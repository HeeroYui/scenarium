Download project in Eclipse
===========================

  - Run Eclipse (select a new workspace)
  - File -> Import
      - Git -> Project From Git (With Smart Import)
      - ```==> Next```
      - Clone URI ```https://gitlab.com/SceDev/scenarium.git```
      - ```==> Next```
      - Select ```master```
      - ```==> Next```
      - Set destination in ```YOUR_WORKSPACE/scenarium```
      - Set ```Clone submodules``` to true
      - ```==> Next```
      - ```==> Finish```


Download in Console (Linux/Mac)
===============================

```{.bash}
export MY_WORKSPACE="workspace"
mkdir ${MY_WORKSPACE}
cd ${MY_WORKSPACE}
git clone https://gitlab.com/SceDev/scenarium.git
```

Run eclipse and open new workspace ${MY_WORKSPACE} folder

