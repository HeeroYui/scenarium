/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario.dataflowdiagram.scheduler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;

import org.scenarium.filemanager.scenario.dataflowdiagram.Block;
import org.scenarium.filemanager.scenario.dataflowdiagram.BlockInput;
import org.scenarium.filemanager.scenario.dataflowdiagram.FlowDiagram;
import org.scenarium.filemanager.scenario.dataflowdiagram.IOComponent;
import org.scenarium.filemanager.scenario.dataflowdiagram.IOData;
import org.scenarium.filemanager.scenario.dataflowdiagram.IOLink;
import org.scenarium.filemanager.scenario.dataflowdiagram.IOLinks;
import org.scenarium.filemanager.scenario.dataflowdiagram.Link;
import org.scenarium.struct.Triple;

public class NoThreadedDiagramScheduler extends DiagramScheduler {
	private ArrayList<OnConsumeTask> onStartTasks;
	private ArrayList<OnConsumeTask> onPauseTasks;
	private ArrayList<OnConsumeTask> onResumeTasks;
	private ArrayList<OnConsumeTask> onStopTasks;
	private BlockRingBuffer todoBlocks = new BlockRingBuffer();
	private HashMap<IOComponent, Triple<Object[], long[], long[]>> blockBuffs = new HashMap<>();
	private boolean isAlive = true;
	private boolean isStarted = false;

	public NoThreadedDiagramScheduler(FlowDiagram fd, ArrayList<Block> schedulableBlocks) {
		super(fd, schedulableBlocks);
		for (Block block : fd.getBlocks())
			createBlock(block);
//		synchronized (this) {
//			if (onStartTasks != null) {
//				onStartTasks.forEach(r -> r.run());
//				onStartTasks = null;
//			}
//		}
		isStarted = true;
	}

	public void createBlock(Block block) {
		DiagramScheduler.initBlock(block, this);
		initOperator(block);
	}

	@Override
	public void destroyBlock(Block block) {
		DiagramScheduler.destroyOperator(block);
	}

	@Override
	public void initNewBlock(Block block) {
		createBlock(block);
	}

	@Override
	public boolean isAlive() {
		return isAlive;
	}

	@Override
	public boolean isStarted() {
		return isStarted;
	}

	@Override
	synchronized public void onStart(Object source, Runnable runnable) {
		if (!isAlive)
			new IllegalAccessError("The scheduler is dead. The started event cannot therefore occur");
		else if (isStarted)
			new IllegalAccessError("The scheduler is already started. The started event cannot happen again");
		onStartTasks.add(new OnConsumeTask(source, runnable));
	}

	private boolean runBlock(Block block) {
		if (!block.isReady())
			return false;
		Triple<Object[], long[], long[]> blockBuff = blockBuffs.get(block);
		Object[] inputs;
		long[] ts;
		long[] toi;
		if (blockBuff == null || blockBuff.getFirst().length != block.getNbInput()) {
			inputs = new Object[block.getNbInput()];
			ts = new long[block.getNbInput()];
			toi = new long[block.getNbInput()];
			blockBuffs.put(block, new Triple<>(inputs, ts, toi));
		} else {
			inputs = blockBuff.getFirst();
			ts = blockBuff.getSecond();
			toi = blockBuff.getThird();
		}
		List<BlockInput> blockInputs = block.getInputs();
		for (int i = 0; i < inputs.length; i++) {
			IOData ioData = blockInputs.get(i).pop();
			if (ioData != null) {
				inputs[i] = ioData.getValue();
				ts[i] = ioData.getTs();
				toi[i] = ioData.getToi();
			} else {
				inputs[i] = null;
				ts[i] = -1;
				toi[i] = -1;
			}
		}
		return DiagramScheduler.processBlock(block, inputs, ts, toi);
	}

	private void stackOutputsAndTrigger(Block block) {
		stackOutputsAndTrigger(block, block.getOutputBuff(), block.getOutputBuffTs());
	}

	private void stackOutputsAndTrigger(IOComponent comp, Object[] outputs, long[] outputsTs) {
		if (!comp.canTriggerOrBeTriggered())
			return;
		long timeOfIssue = System.currentTimeMillis();
		IOLinks[] index = comp.getIndex();
		if (comp instanceof FlowDiagram)
			System.out.println("sortie de diagram de flux");
		for (IOLinks ioLinks : index) {
			IOComponent linkedComp = ioLinks.getComponent();
			if (linkedComp.canTriggerOrBeTriggered()) {
				boolean needToProcess = false;
				for (IOLink ioLink : ioLinks.getLinks()) {
					int outputIndex = ioLink.getOutputIndex();
					Object outputObject = outputs[outputIndex];
					if (outputObject == null)
						continue;
					needToProcess = true;
					if (ioLink.getInput().getBuffer().push(outputsTs[outputIndex], timeOfIssue, ioLink.isNeedToCopy() ? DiagramScheduler.clone(outputObject) : outputObject))
						DiagramScheduler.showBufferOverflow(comp, ioLink.getInput());
					Link link = ioLink.getInput().getLink();
					if (link != null)
						link.consume();
				}
				if (needToProcess)
					todoBlocks.push((Block) ioLinks.getComponent());
			}
		}
	}
	
	@Override
	public void onResume(Object source, Runnable runnable) {
		addOnEvent(source, mds -> mds.onResume(source, runnable));
	}

	@Override
	public void onPause(Object source, Runnable runnable) {
		addOnEvent(source, mds -> mds.onPause(source, runnable));
	}

	@Override
	public void onStop(Block source, Runnable runnable) {
		addOnEvent(source, mds -> mds.onStop(source, runnable));		
	}
	
	private void addOnEvent(Object source, Consumer<NoThreadedDiagramScheduler> consumer) {
		if (!isAlive)
			new IllegalAccessError("The scheduler is dead. The started event cannot therefore occur");
		else
			consumer.accept(this);
	}
	
	@Override
	public void start() {
		consumeOnEventTasks(onStartTasks);
	}

	@Override
	public void resume() {
		consumeOnEventTasks(onResumeTasks);
	}

	@Override
	public void pause() {
		consumeOnEventTasks(onPauseTasks);
	}

	@Override
	public void preStop() {
		consumeOnEventTasks(onStopTasks);
	}
	
	private void consumeOnEventTasks(ArrayList<OnConsumeTask> tasks) {
		if (tasks != null)
			tasks.forEach(t -> triggerTask(t.source, t.runnable));
	}

	@Override
	public void stop() {
		isAlive = false;
		for (Block block : fd.getBlocks())
			destroyBlock(block);
	}

	@Override
	public void triggerOutput(Object source, Object outputValue, long timeStamp) {
		triggerOutput(source, new Object[] { outputValue }, new long[] { timeStamp });
	}

	@Override
	public void triggerOutput(Object source, Object[] outputValues, long timeStamp) {
		long[] timeStamps = new long[outputValues.length];
		for (int i = 0; i < timeStamps.length; i++)
			timeStamps[i] = timeStamp;
		triggerOutput(source, outputValues, timeStamps);
	}

	@Override
	synchronized public void triggerOutput(Object source, Object[] outputValues, long[] timeStamps) {
		stackOutputsAndTrigger((IOComponent) source, outputValues, timeStamps);
		while (!todoBlocks.isEmpty() && isAlive) {
			Block block = todoBlocks.popFirst();
			if (runBlock(block))
				stackOutputsAndTrigger(block);
		}
	}

	@Override
	synchronized public void triggerTask(Object source, Runnable runnable) {
		runnable.run();
	}

	@Override
	protected void blockNameChanged(Block block) {}
}
