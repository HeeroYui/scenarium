/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario.dataflowdiagram.scheduler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.scenarium.filemanager.scenario.dataflowdiagram.Block;
import org.scenarium.filemanager.scenario.dataflowdiagram.BlockInput;
import org.scenarium.filemanager.scenario.dataflowdiagram.FlowDiagram;
import org.scenarium.filemanager.scenario.dataflowdiagram.IOComponent;
import org.scenarium.filemanager.scenario.dataflowdiagram.IOData;
import org.scenarium.filemanager.scenario.dataflowdiagram.IOLink;
import org.scenarium.filemanager.scenario.dataflowdiagram.IOLinks;
import org.scenarium.filemanager.scenario.dataflowdiagram.Link;
import org.scenarium.filemanager.scenario.dataflowdiagram.ManagingCpuUsageListener;
import org.scenarium.filemanager.scenario.dataflowdiagram.Output;
import org.scenarium.filemanager.scenario.dataflowdiagram.ProcessMode;

public class MultiCoreDiagramScheduler extends DiagramScheduler implements ManagingCpuUsageListener {
	private ArrayList<ThreadedBlock> threadedBlocks = new ArrayList<>();
	private HashMap<Block, Condition> incomingDataOrTaskSignals = new HashMap<>();
	private volatile boolean dead = false;
	CPUUsageTask cpuUsageTask;
	Object cpuUsageTaskLock = new Object();
	private boolean started;

	public MultiCoreDiagramScheduler(FlowDiagram fd, ArrayList<Block> schedulableBlocks) {
		super(fd, schedulableBlocks);
		fd.setCpuUsage(-1);
		List<Block> blocks = fd.getBlocks();
		AtomicInteger nbThreadedBlocksLeft = new AtomicInteger(blocks.size());
		fd.addManagingCpuUsage(this);
		ThreadedBlock[] threads = new ThreadedBlock[blocks.size()];
		for (int i = 0; i < blocks.size(); i++)
			threads[i] = createBlock(blocks.get(i), nbThreadedBlocksLeft);

		Arrays.sort(threads, (a, b) -> Integer.compare(b.block.getStartPriority(), a.block.getStartPriority()));
		int i = 0;
		ArrayList<ThreadedBlock> launchedThreadedBlocks = new ArrayList<>(threads.length);
		while (i != threads.length) {
			int priority = threads[i].block.getStartPriority();
			while (i != threads.length && threads[i].block.getStartPriority() == priority) {
				ThreadedBlock t = threads[i++];
				t.start();
				launchedThreadedBlocks.add(t);
			}
			waitThreadedBlock(launchedThreadedBlocks, tb -> tb.isStarted);
			launchedThreadedBlocks.clear();
		}
		started = true;
	}

	private ThreadedBlock createBlock(Block block, AtomicInteger nbThreadedBlocksLeft) {
		if (dead)
			return null;
		DiagramScheduler.initBlock(block, this);
		ThreadedBlock blockThread = new ThreadedBlock(block, nbThreadedBlocksLeft, this);
		incomingDataOrTaskSignals.put(block, blockThread.incomingDataOrTask);
		threadedBlocks.add(blockThread);
		if (fd.isManagingCpuUsage())
			blockThread.initCpuUsageTask();
		return blockThread;
	}

	@Override
	public void destroyBlock(Block block) {
		if (dead)
			return;
		for (ThreadedBlock threadedBlock : threadedBlocks)
			if (block == threadedBlock.getBlock())
				threadedBlock.interrupt();
	}

	@Override
	public void initNewBlock(Block block) {
		if (dead)
			return;
		ThreadedBlock tb = createBlock(block, null);
		tb.start();
		initOperator(block);
	}

	@Override
	public boolean isAlive() {
		return !dead;
	}

	@Override
	public boolean isStarted() {
		return started;
	}

	@Override
	protected void blockNameChanged(Block block) {
		for (ThreadedBlock threadedBlock : threadedBlocks)
			if (threadedBlock.block == block) {
				triggerTask(block, () -> threadedBlock.setName(block.getName()));
				break;
			}
	}

	@Override
	public void managingCpuChanged(boolean managingCpu) {
		if (dead)
			return;
		if (managingCpu)
			for (ThreadedBlock threadedBlock : threadedBlocks)
				threadedBlock.initCpuUsageTask();
		else
			for (ThreadedBlock threadedBlock : threadedBlocks)
				threadedBlock.closeCpuUsageTask();
	}

	@Override
	public void onStart(Object source, Runnable runnable) {
		if (started)
			runnable.run();
		else
			addOnEvent(source, tb -> tb.onStart(runnable));
	}

	@Override
	public void onResume(Object source, Runnable runnable) {
		addOnEvent(source, tb -> tb.onResume(runnable));
	}

	@Override
	public void onPause(Object source, Runnable runnable) {
		addOnEvent(source, tb -> tb.onPause(runnable));
	}

	@Override
	public void onStop(Block source, Runnable runnable) {
		addOnEvent(source, tb -> tb.onStop(runnable));
	}

	private void addOnEvent(Object source, Consumer<ThreadedBlock> consumer) {
		if (dead)
			throw new IllegalAccessError("The scheduler is dead. The stopped event cannot therefore occur");
		for (ThreadedBlock threadedBlock : threadedBlocks)
			if (threadedBlock.getBlock() == source) {
				consumer.accept(threadedBlock);
				return;
			}
		throw new IllegalArgumentException("The source: " + source + " is not managed by this scheduler");
	}
	
	@Override
	public void start() {
		for (ThreadedBlock threadedBlock : threadedBlocks)
			triggerTask(threadedBlock.getBlock(), threadedBlock::consumeOnStartTasks);
	}

	@Override
	public void resume() {
		threadedBlocks.forEach(tb -> tb.consumeOnResumeTasks());
	}

	@Override
	public void pause() {
		threadedBlocks.forEach(tb -> tb.consumeOnPauseTasks());
	}

	@Override
	public void preStop() {
		threadedBlocks.forEach(tb -> tb.consumeOnStopTasks());
	}

	void stackOutputsAndTrigger(IOComponent block, Object[] outputs, long[] outputsTs, FlowDiagram fd) {
		if (!block.canTriggerOrBeTriggered())
			return;
		long timeOfIssue = System.currentTimeMillis();
		IOLinks[] index = block.getIndex();
		if (block instanceof FlowDiagram)
			System.out.println("sortie de diagram de flux");
		for (IOLinks ioLinks : index) {
			IOComponent linkedComp = ioLinks.getComponent();
			if (linkedComp.canTriggerOrBeTriggered()) {
				Condition incomingDataSignal = incomingDataOrTaskSignals.get(linkedComp);
				if (incomingDataSignal != null) { // Sinon, c'est probablement un trigger après la fin d'un thread
					ReentrantLock ioLock = ((Block) linkedComp).getIOLock();
					ioLock.lock();
					try {
						boolean blockReady = false;
						for (IOLink ioLink : ioLinks.getLinks()) {
							int outputIndex = ioLink.getOutputIndex();
							Object outputObject = outputs[outputIndex];
							if (outputObject == null)
								continue;
							blockReady = true;
							if (ioLink.getInput().getBuffer().push(outputsTs[outputIndex], timeOfIssue, ioLink.isNeedToCopy() ? clone(outputObject) : outputObject))
								DiagramScheduler.showBufferOverflow(block, ioLink.getInput());
							Link link = ioLink.getInput().getLink();
							if (link != null)
								link.consume();
						}
						if (blockReady)
							incomingDataSignal.signal();
					} finally {
						ioLock.unlock();
					}
				} else
					System.err.println(block + ": trigger a data after the death of " + linkedComp);
			}
		}
	}

	@Override
	public void stop() {
		if (dead)
			return;
		synchronized (this) {
			dead = true;
			threadedBlocks.sort((a, b) -> Integer.compare(b.block.getStopPriority(), a.block.getStopPriority()));
			int i = 0;
			ArrayList<ThreadedBlock> stoppedThreadedBlocks = new ArrayList<>(threadedBlocks.size());
			while (i != threadedBlocks.size()) {
				int priority = threadedBlocks.get(i).block.getStopPriority();
				while (i != threadedBlocks.size() && threadedBlocks.get(i).block.getStopPriority() == priority) {
					ThreadedBlock t = threadedBlocks.get(i++);
					t.dead = true;
					Block block = t.block;
					if (block.birthFailed())
						block.reset();
					else {
						ReentrantLock lock = block.getIOLock();
						lock.lock();
						try {
							incomingDataOrTaskSignals.get(block).signal();
						} finally {
							lock.unlock();
						}
					}
					stoppedThreadedBlocks.add(t);
				}
				waitThreadedBlock(stoppedThreadedBlocks, tb -> !tb.isAlive());
				stoppedThreadedBlocks.clear();
			}
			threadedBlocks.clear();
			incomingDataOrTaskSignals.clear();
			fd.setCpuUsage(-1);
			fd.removeManagingCpuUsageListener(this);
		}
	}

	@Override
	public void triggerOutput(Object source, Object outputValue, long timeStamp) {
		if (isAlive())
			if (!started)
				System.err.println("Early trigger of: " + source + " data removed");
			else
				stackOutputsAndTrigger((IOComponent) source, new Object[] { outputValue }, new long[] { timeStamp }, fd);
	}

	@Override
	public void triggerOutput(Object source, Object[] outputValues, long timeStamp) {
		if (isAlive()) {
			if (!started)
				System.err.println("Early trigger of: " + source + " data removed");
			boolean isNonNullEntrie = false;
			List<? extends Output> outputs = ((IOComponent) source).getOutputs();
			for (int i = 0; i < outputValues.length; i++)
				if (outputs.get(i).getLinkInputs() == null)
					outputValues[i] = null;
				else if (outputValues[i] != null) {
					outputValues[i] = clone(outputValues[i]);
					isNonNullEntrie = true;
				}
			long[] timeStamps = new long[outputValues.length];
			for (int i = 0; i < timeStamps.length; i++)
				timeStamps[i] = timeStamp;
			if (isNonNullEntrie)
				stackOutputsAndTrigger((IOComponent) source, outputValues, timeStamps, fd);
		}
	}

	@Override
	public void triggerOutput(Object source, Object[] outputValues, long[] timeStamps) {
		if (isAlive()) {
			if (!started)
				System.err.println("Early trigger of: " + source + " data removed");
			boolean isNonNullEntrie = false;
			List<? extends Output> outputs = ((IOComponent) source).getOutputs();
			for (int i = 0; i < outputValues.length; i++)
				if (outputs.get(i).getLinkInputs() == null) {
					outputValues[i] = null;
					timeStamps[i] = -1;
				} else if (outputValues[i] != null) {
					outputValues[i] = clone(outputValues[i]);
					isNonNullEntrie = true;
				}
			if (isNonNullEntrie)
				stackOutputsAndTrigger((IOComponent) source, outputValues, timeStamps, fd);
		}
	}

	@Override
	public void triggerTask(Object source, Runnable task) { // synchronized avant mais on pouvait tout bloquer si le process attendait le relachement de ce synchronized et q'un autre thread attendait
															// la fin de la tache...
		for (ThreadedBlock threadedBlock : threadedBlocks)
			if (threadedBlock.getBlock() == source) {
				if (Thread.currentThread() == threadedBlock
						|| threadedBlock.getBlock().getRemoteOperator() != null && threadedBlock.getBlock().getRemoteOperator().isFromThreadedBlock(threadedBlock.getId()))
					task.run();
				else if (dead || threadedBlock.getBlock().birthFailed()) {
					try {
						threadedBlock.join();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					task.run();
				} else {
					ReentrantLock lock = ((Block) source).getIOLock();
					if (lock.isHeldByCurrentThread())
						task.run();
					else {
						lock.lock();
						Condition taskCompleted = lock.newCondition();
						threadedBlock.tasksCompleted.add(taskCompleted);
						threadedBlock.tasks.add(task);
						try {
							incomingDataOrTaskSignals.get(source).signal();
							try {
								taskCompleted.await();
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						} finally {
							lock.unlock();
						}
					}
				}
				return;
			}
		task.run(); // Impossible de trouver la tache..., je run
	}

	private void waitThreadedBlock(ArrayList<ThreadedBlock> threadedBlocks, Predicate<ThreadedBlock> blockCondition) {
		int timeOutTime = 1000;
		for (ThreadedBlock tb : threadedBlocks) {
			ProcessMode pm = tb.block.getProcessMode();
			if (pm == ProcessMode.ISOLATED) {
				timeOutTime = 2000;
				break;
			} else if (pm == ProcessMode.REMOTE)
				timeOutTime = 1500;
		}
		long endOfTimeout = System.currentTimeMillis() + timeOutTime;
		boolean timeOut = false;
		LinkedList<ThreadedBlock> blockingThreads = new LinkedList<>(threadedBlocks);
		while (!blockingThreads.isEmpty()) {
			long millis = endOfTimeout - System.currentTimeMillis();
			if (millis < 0) {
				timeOut = true;
				break;
			}
			try {
				Thread.sleep(Math.min(50, millis));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			blockingThreads.removeIf(blockCondition);
		}
		if (timeOut) {
			List<Block> aliveSynchroThreads = Collections.synchronizedList(blockingThreads.stream().map(t -> t.getBlock()).collect(Collectors.toList()));
			BiConsumer<List<Block>, Runnable> timeOutTask = fd.getTimeOutTask();
			Thread stopTimeOutThread = null;
			if (timeOutTask != null) {
				stopTimeOutThread = new Thread(() -> timeOutTask.accept(aliveSynchroThreads, () -> blockingThreads.forEach(tb -> tb.interrupt())));
				stopTimeOutThread.start();
			}
			while (!blockingThreads.isEmpty()) {
				try {
					Thread.sleep(50);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				for (Iterator<ThreadedBlock> iterator = blockingThreads.iterator(); iterator.hasNext();) {
					ThreadedBlock tb = iterator.next();
					if (blockCondition.test(tb)) {
						iterator.remove();
						aliveSynchroThreads.remove(tb.getBlock());
					}
				}
			}
			if (stopTimeOutThread != null)
				stopTimeOutThread.interrupt();
		}
	}

	class ThreadedBlock extends Thread {
		private ArrayList<Runnable> onStartTasks;
		private ArrayList<Runnable> onPauseTasks;
		private ArrayList<Runnable> onResumeTasks;
		private ArrayList<Runnable> onStopTasks;
		private Block block;
		private MultiCoreDiagramScheduler scheduler;
		private AtomicInteger nbThreadedBlocksLeft;
		private Object[] inputs = new Object[0];
		private long[] ts;
		private long[] toi;
		private boolean cpuUsageMonitored;
		private final ReentrantLock ioLock;
		private final Condition incomingDataOrTask;
		public ArrayList<Condition> tasksCompleted = new ArrayList<>();
		public ArrayList<Runnable> tasks = new ArrayList<>();
		public boolean isStarted;
		public boolean dead;

		public ThreadedBlock(Block block, AtomicInteger nbThreadedBlocksLeft, MultiCoreDiagramScheduler scheduler) {
			this.block = block;
			ioLock = block.getIOLock();
			incomingDataOrTask = ioLock.newCondition();
			this.scheduler = scheduler;
			this.nbThreadedBlocksLeft = nbThreadedBlocksLeft;
			setPriority(block.getThreadPriority());
			setName(block.getName());
		}

		synchronized public void onStart(Runnable runnable) {
			ArrayList<Runnable> _onStartTasks = onStartTasks;
			if (_onStartTasks == null)
				_onStartTasks = new ArrayList<>();
			_onStartTasks.add(runnable);
			onStartTasks = _onStartTasks;
		}

		synchronized public void onResume(Runnable runnable) {
			ArrayList<Runnable> _onResumeTasks = onResumeTasks;
			if (_onResumeTasks == null)
				_onResumeTasks = new ArrayList<>();
			_onResumeTasks.add(runnable);
			onResumeTasks = _onResumeTasks;
		}

		synchronized public void onPause(Runnable runnable) {
			ArrayList<Runnable> _onPauseTasks = onPauseTasks;
			if (_onPauseTasks == null)
				_onPauseTasks = new ArrayList<>();
			_onPauseTasks.add(runnable);
			onPauseTasks = _onPauseTasks;
		}

		synchronized public void onStop(Runnable runnable) {
			ArrayList<Runnable> _onStopTasks = onStopTasks;
			if (_onStopTasks == null)
				_onStopTasks = new ArrayList<>();
			_onStopTasks.add(runnable);
			onStopTasks = _onStopTasks;
		}

		public void consumeOnStartTasks() {
			consumeOnEventTasks(onStartTasks);
		}

		public void consumeOnResumeTasks() {
			consumeOnEventTasks(onResumeTasks);
		}

		public void consumeOnPauseTasks() {
			consumeOnEventTasks(onPauseTasks);
		}

		public void consumeOnStopTasks() {
			consumeOnEventTasks(onStopTasks);
		}

		private void consumeOnEventTasks(ArrayList<Runnable> tasks) {
			if (tasks != null)
				tasks.forEach(t -> triggerTask(block, t));
		}

		private void consumeTasks() {
			for (int i = 0; i < tasks.size(); i++) {
				Runnable task = tasks.get(i);
				Condition taskCompleted = tasksCompleted.get(i);
				try {
					task.run();
				} catch (Throwable e) {
					block.setDefaulting(true);
					System.err.println("Failed to run task of " + block.getName());
					e.printStackTrace();
				}
				try {
					taskCompleted.signal(); // IllegalMonitorStateException
				} catch (IllegalMonitorStateException e) {
					System.err.println("Happen to " + ProcessHandle.current().pid());
					e.printStackTrace();
				}
			}
			tasks.clear();
			tasksCompleted.clear();
		}

		public Block getBlock() {
			return block;
		}

		public void initCpuUsageTask() {
			if (!cpuUsageMonitored) {
				synchronized (scheduler.cpuUsageTaskLock) {
					if (cpuUsageTask == null)
						try {
							cpuUsageTask = CPUUsageTask.createCPUUsageTask();
						} catch (UnsupportedOperationException e) {
							System.err.println(e.getMessage());
							return;
						}
					cpuUsageTask.addTask(getId(), block);
				}
				cpuUsageMonitored = true;
			}
		}

		public void closeCpuUsageTask() {
			if (cpuUsageMonitored) {
				synchronized (scheduler.cpuUsageTaskLock) {
					if (cpuUsageTask.removeTask(getId()))
						cpuUsageTask = null;
					cpuUsageMonitored = false;
				}
			}
		}

		@Override
		synchronized public void run() {
			scheduler.initOperator(block);
			if (nbThreadedBlocksLeft != null) {
				nbThreadedBlocksLeft.decrementAndGet();
				nbThreadedBlocksLeft = null;
			}
			isStarted = true;
			if (block.birthFailed())
				return;
			block.setThread(Thread.currentThread());
			dead: while (!canBeStopped()) {
				ioLock.lock();
				try {
					while (true) {
						if (canBeStopped())
							break dead;
						if (!tasks.isEmpty())
							consumeTasks();
						if (block.isReady())
							break;
						incomingDataOrTask.await();
					}
					int nbInput = block.getNbInput();
					if (inputs.length != nbInput) {
						inputs = new Object[nbInput];
						ts = new long[nbInput];
						toi = new long[nbInput];
					}
					List<BlockInput> blockInputs = block.getInputs();
					for (int i = 0; i < blockInputs.size(); i++) {
						IOData ioData = blockInputs.get(i).pop();
						if (ioData != null) {
							inputs[i] = ioData.getValue();
							ts[i] = ioData.getTs();
							toi[i] = ioData.getToi();
						} else {
							inputs[i] = null;
							ts[i] = -1;
							toi[i] = -1;
						}
					}
				} catch (InterruptedException e) {
					break;
				} finally {
					ioLock.unlock();
				}
				if (DiagramScheduler.processBlock(block, inputs, ts, toi))
					stackOutputsAndTrigger(block, block.getOutputBuff(), block.getOutputBuffTs(), scheduler.fd);
			}
			DiagramScheduler.destroyOperator(block);
			if (cpuUsageMonitored)
				closeCpuUsageTask();
			ioLock.lock();
			try {
				consumeTasks();
			} finally {
				ioLock.unlock();
			}
		}

		private boolean canBeStopped() {
			return dead && (!block.isConsumesAllDataBeforeDying() || (!block.isReady() && tasks.isEmpty()));
		}

		@Override
		public String toString() {
			return "Thread of: " + block;
		}
	}
}