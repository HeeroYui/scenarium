/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario.dataflowdiagram.scheduler;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import org.scenarium.filemanager.scenario.dataflowdiagram.CpuUsageMeasurable;

public class CPUUsageTask extends TimerTask {
	private final ThreadMXBean bean = ManagementFactory.getThreadMXBean();
	private ArrayList<CPUUsageTaskElement> cpuUsageTaskElements = new ArrayList<>();
	private Timer cpuTimer;

	private CPUUsageTask() {}

	public void close() {
		if (cpuTimer != null) {
			cpuTimer.cancel();
			cpuTimer.purge();
			cpuTimer = null;
		}
	}

	@Override
	public void run() {
		for (CPUUsageTaskElement cpuUsageTaskElement : cpuUsageTaskElements)
			cpuUsageTaskElement.update(bean);
	}
	
	public static CPUUsageTask createCPUUsageTask() throws UnsupportedOperationException{
		ThreadMXBean bean = ManagementFactory.getThreadMXBean();
		if(!bean.isThreadCpuTimeSupported() || !bean.isCurrentThreadCpuTimeSupported() || !bean.isThreadCpuTimeEnabled())
			throw new UnsupportedOperationException("Monitoring threads is not supported by the platform");
		return new CPUUsageTask();
	}
	
	public static CPUUsageTask createCPUUsageTask(long idThread, CpuUsageMeasurable cum) {
		CPUUsageTask cpuUsageTask = createCPUUsageTask();
		cpuUsageTask.addTask(idThread, cum);
		return cpuUsageTask;
	}
	
	public void addTask(long idThread, CpuUsageMeasurable cum) {
		cum.setCpuUsage(0);
		cpuUsageTaskElements.add(new CPUUsageTaskElement(idThread, cum, bean.getThreadCpuTime(idThread)));
		if(cpuTimer == null) {
			cpuTimer = new Timer("CPUUsageTask");
			cpuTimer.schedule(this, 1000, 1000);
		}
	}
	
	public boolean removeTask(long idThread) {
		cpuUsageTaskElements.removeIf(e -> e.id == idThread);
		if(cpuUsageTaskElements.isEmpty()) {
			close();
			return true;
		}
		return false;
	}
}

class CPUUsageTaskElement {
	public final long id;
	public final CpuUsageMeasurable cum;
	public long cpuStartTime;
	public long start;
	
	public CPUUsageTaskElement(long id, CpuUsageMeasurable cum, long cpuStartTime) {
		this.id = id;
		this.cum = cum;
		this.cpuStartTime = cpuStartTime;
		this.start = System.nanoTime();
	}

	public void update(ThreadMXBean bean) {
		cum.setCpuUsage((float) (bean.getThreadCpuTime(id) - cpuStartTime) / (System.nanoTime() - start));
		start = System.nanoTime();
		cpuStartTime = bean.getThreadCpuTime(id);
	}
}
