/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario.dataflowdiagram.scheduler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import org.beanmanager.tools.Tuple;
import org.scenarium.filemanager.scenario.dataflowdiagram.Block;
import org.scenarium.filemanager.scenario.dataflowdiagram.BlockInput;
import org.scenarium.filemanager.scenario.dataflowdiagram.FlowDiagram;
import org.scenarium.filemanager.scenario.dataflowdiagram.IOComponent;
import org.scenarium.filemanager.scenario.dataflowdiagram.IOData;
import org.scenarium.filemanager.scenario.dataflowdiagram.IOLink;
import org.scenarium.filemanager.scenario.dataflowdiagram.IOLinks;
import org.scenarium.filemanager.scenario.dataflowdiagram.Link;
import org.scenarium.filemanager.scenario.dataflowdiagram.ManagingCpuUsageListener;
import org.scenarium.struct.Triple;

public class MonoCoreDiagramScheduler extends DiagramScheduler implements ManagingCpuUsageListener {
	protected HashMap<Block, Object[]> blocksEntries = new HashMap<>();
	protected LinkedBlockingDeque<Long> timePointerConsumptionStack;
	private MonoScheduler ms;
	private CPUUsageTask cpuUsageTask;

	public MonoCoreDiagramScheduler(FlowDiagram fd, ArrayList<Block> schedulableBlocks, CountDownLatch startLock) {
		super(fd, schedulableBlocks);
		timePointerConsumptionStack = new LinkedBlockingDeque<>();
		ms = new MonoScheduler(this, fd, schedulableBlocks, startLock);
		launch(ms);
		managingCpuChanged(fd.isManagingCpuUsage());
		fd.addManagingCpuUsage(this);
		waitMonoSchedulerStart(startLock);
	}

	@Override
	public void destroyBlock(Block block) {
		DiagramScheduler.destroyOperator(block);
	}

	@Override
	public void initNewBlock(Block block) {
		MonoScheduler _mds = ms;
		if (_mds != null)
			ms.createBlock(block);
	}

	@Override
	public boolean isAlive() {
		MonoScheduler _ms = ms;
		return _ms != null && !_ms.dead;
	}

	@Override
	public boolean isStarted() {
		MonoScheduler _ms = ms;
		return _ms != null && _ms.isStarted;
	}

	protected void launch(Thread t) {
		MonoScheduler _ms = ms;
		if (_ms != null)
			_ms.start();
	}

	@Override
	public void managingCpuChanged(boolean managingCpu) {
		if (managingCpu && cpuUsageTask == null)
			try {
				cpuUsageTask = CPUUsageTask.createCPUUsageTask(ms.getId(), fd);
			} catch (UnsupportedOperationException e) {
				System.err.println(e.getMessage());
				return;
			}
		else if (!managingCpu && cpuUsageTask != null) {
			cpuUsageTask.close();
			cpuUsageTask = null;
		}
	}

	@Override
	public void onStart(Object source, Runnable runnable) {
		addOnEvent(source, mds -> {
			if (mds.isStarted)
				runnable.run();
			else
				mds.onStart(source, runnable);
		});
	}

	@Override
	public void onResume(Object source, Runnable runnable) {
		addOnEvent(source, mds -> mds.onResume(source, runnable));
	}

	@Override
	public void onPause(Object source, Runnable runnable) {
		addOnEvent(source, mds -> mds.onPause(source, runnable));
	}

	@Override
	public void onStop(Block source, Runnable runnable) {
		addOnEvent(source, mds -> mds.onStop(source, runnable));
	}

	private void addOnEvent(Object source, Consumer<MonoScheduler> consumer) {
		MonoScheduler _mds = ms;
		if (_mds == null || _mds.dead)
			new IllegalAccessError("The scheduler is dead. The started event cannot therefore occur");
		else
			consumer.accept(_mds);
	}
	
	@Override
	public void start() {
		MonoScheduler _mds = ms;
		if (_mds != null && !_mds.dead)
			_mds.consumeOnStartTasks();
	}

	@Override
	public void resume() {
		MonoScheduler _mds = ms;
		if (_mds != null && !_mds.dead)
			_mds.consumeOnResumeTasks();
	}

	@Override
	public void pause() {
		MonoScheduler _mds = ms;
		if (_mds != null && !_mds.dead)
			_mds.consumeOnPauseTasks();
	}

	@Override
	public void preStop() {
		MonoScheduler _mds = ms;
		if (_mds != null && !_mds.dead)
			_mds.consumeOnStopTasks();
	}

	@Override
	public void stop() {
		// System.err.println("stop " + getClass().getSimpleName() + " id: " + Thread.currentThread().getId());
		fd.removeManagingCpuUsageListener(this);
		if (cpuUsageTask != null) {
			cpuUsageTask.close();
			cpuUsageTask = null;
		}
		fd.setCpuUsage(0);
		if (ms != null) {
			ms.lock.lock();
			ms.dead = true;
			try {
				ms.incomingDataOrTaskSignals.signalAll();
				ms.schedulerAvailable.signalAll();
			} finally {
				ms.lock.unlock();
			}
			try {
				// System.err.println("join: " + Thread.currentThread().getId());
				ms.join(1000);
			} catch (InterruptedException e) {
				// System.err.println("Thread id: " + Thread.currentThread().getId());
				e.printStackTrace(); // TODO Bug Lancer une fois je sais pas pk... une fois de plus en monocore...
			}
			if (ms.isAlive()) {
				BiConsumer<List<Block>, Runnable> timeOutTask = fd.getTimeOutTask();
				Thread stopTimeOutThread = null;
				if (timeOutTask != null) {
					stopTimeOutThread = new Thread(() -> timeOutTask.accept(ms.blockingBlock, () -> {
						ms.isInterrupted = true;
						ms.interrupt();
					}));
					stopTimeOutThread.start();
				}

				try {
					ms.join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if (stopTimeOutThread != null)
					stopTimeOutThread.interrupt();
			}
		}
	}

	@Override
	public void triggerOutput(Object source, Object outputValue, long timeStamp) {
		triggerOutput(source, new Object[] { outputValue }, new long[] { timeStamp });
	}

	@Override
	public void triggerOutput(Object source, Object[] outputValues, long timeStamp) {
		long[] timeStamps = new long[outputValues.length];
		for (int i = 0; i < timeStamps.length; i++)
			timeStamps[i] = timeStamp;
		triggerOutput(source, outputValues, timeStamps);
	}

	@Override
	public void triggerOutput(Object source, Object[] outputValues, long[] timeStamps) {
		MonoScheduler _mds = ms;
		if (_mds != null)
			_mds.triggerOutput((IOComponent) source, outputValues, timeStamps);
	}

	@Override
	public void triggerTask(Object source, Runnable task) {
		MonoScheduler _mds = ms;
		if (_mds != null)
			_mds.triggerTask(source, task);
		else
			task.run();
	}

	private void waitMonoSchedulerStart(CountDownLatch startLock) {
		try {
			boolean isStarted = startLock.await(1, TimeUnit.SECONDS);
			if (!isStarted) {
				BiConsumer<List<Block>, Runnable> timeOutTask = fd.getTimeOutTask();
				Thread stopTimeOutThread = null;
				if (timeOutTask != null) {
					stopTimeOutThread = new Thread(() -> timeOutTask.accept(ms.blockingBlock, () -> {
						ms.isInterrupted = true;
						MonoCoreDiagramScheduler.this.ms.interrupt();
						try {
							MonoCoreDiagramScheduler.this.ms.join();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}));
					stopTimeOutThread.start();
				}
				startLock.await();
				if (stopTimeOutThread != null)
					stopTimeOutThread.interrupt();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void blockNameChanged(Block block) {}
}

class MonoScheduler extends Thread {
	private ArrayList<OnConsumeTask> onStartTasks;
	private ArrayList<OnConsumeTask> onPauseTasks;
	private ArrayList<OnConsumeTask> onResumeTasks;
	private ArrayList<OnConsumeTask> onStopTasks;

	private BlockRingBuffer todoBlocks = new BlockRingBuffer();
	private HashMap<IOComponent, Triple<Object[], long[], long[]>> blockBuffs = new HashMap<>();
	private MonoCoreDiagramScheduler scds;
	private FlowDiagram fd;
	private IOComponent todoSource;
	private Object[] todoOutputValues;
	private long[] todoTimeStamps;
	volatile boolean dataRead = false;
	protected boolean isStarted = false;

	protected boolean dead = false;
	final ReentrantLock lock = new ReentrantLock();
	final Condition incomingDataOrTaskSignals = lock.newCondition();
	final Condition schedulerAvailable = lock.newCondition();
	private CountDownLatch startLock;
	protected List<Block> blockingBlock;
	public boolean isInterrupted = false;
	private long callingThreadId = -1;
	private Thread monoRemoteThread;

	public List<Tuple<Condition, Condition>> tasks = Collections.synchronizedList(new ArrayList<>());

	public MonoScheduler(MonoCoreDiagramScheduler scds, FlowDiagram fd, ArrayList<Block> schedulableBlocks, CountDownLatch startLock2) {
		this.scds = scds;
		this.fd = scds.fd;
		this.startLock = startLock2;
		blockingBlock = Collections.synchronizedList(new LinkedList<>());
	}

	private void consumeTasks() {
		while (!tasks.isEmpty()) {
			Tuple<Condition, Condition> task = tasks.remove(0);
			lock.lock();
			try {
				task.getFirst().signal();
				task.getSecond().await();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} finally {
				lock.unlock();
			}
		}
	}

	public void createBlock(Block block) {
		DiagramScheduler.initBlock(block, scds);
		scds.initOperator(block);
	}

	synchronized public void onStart(Object source, Runnable runnable) {
		ArrayList<OnConsumeTask> _onStartTasks = onStartTasks;
		if (_onStartTasks == null)
			_onStartTasks = new ArrayList<>();
		_onStartTasks.add(new OnConsumeTask(source, runnable));
		onStartTasks = _onStartTasks;
	}

	synchronized public void onResume(Object source, Runnable runnable) {
		ArrayList<OnConsumeTask> _onResumeTasks = onResumeTasks;
		if (_onResumeTasks == null)
			_onResumeTasks = new ArrayList<>();
		_onResumeTasks.add(new OnConsumeTask(source, runnable));
		onResumeTasks = _onResumeTasks;
	}

	synchronized public void onPause(Object source, Runnable runnable) {
		ArrayList<OnConsumeTask> _onPauseTasks = onPauseTasks;
		if (_onPauseTasks == null)
			_onPauseTasks = new ArrayList<>();
		_onPauseTasks.add(new OnConsumeTask(source, runnable));
		onPauseTasks = _onPauseTasks;
	}

	synchronized public void onStop(Object source, Runnable runnable) {
		ArrayList<OnConsumeTask> _onStopTasks = onStopTasks;
		if (_onStopTasks == null)
			_onStopTasks = new ArrayList<>();
		_onStopTasks.add(new OnConsumeTask(source, runnable));
		onStopTasks = _onStopTasks;
	}

	public void consumeOnStartTasks() {
		consumeOnEventTasks(onStartTasks);
	}

	public void consumeOnResumeTasks() {
		consumeOnEventTasks(onResumeTasks);
	}

	public void consumeOnPauseTasks() {
		consumeOnEventTasks(onPauseTasks);
	}

	public void consumeOnStopTasks() {
		consumeOnEventTasks(onStopTasks);
	}

	private void consumeOnEventTasks(ArrayList<OnConsumeTask> tasks) {
		if (tasks != null)
			tasks.forEach(t -> scds.triggerTask(t.source, t.runnable));
	}

	@Override
	public void run() {
		List<Block> blocks = fd.getBlocks();
		blocks.sort((a, b) -> Integer.compare(b.getStartPriority(), a.getStartPriority()));
		for (Block block : blocks) {
			blockingBlock.add(block);
			createBlock(block);
			blockingBlock.remove(block);
		}
		startLock.countDown();
		isStarted = true;
//		synchronized (this) {
//			if (onStartTasks != null) {
//				onStartTasks.forEach(r -> r.runnable.run());
//				onStartTasks = null;
//			}
//		}
		dead: while (!dead) {
			IOComponent source = null;
			Object[] outputValues = null;
			long[] timeStamps = null;
			lock.lock();
			try {
				while (true) {
					if (dead)
						break dead;
					if (!tasks.isEmpty())
						consumeTasks();
					if (this.todoSource != null)
						break;
					incomingDataOrTaskSignals.await();
				}
				if (this.todoSource != null) {
					source = this.todoSource;
					this.todoSource = null;
					outputValues = this.todoOutputValues;
					this.todoOutputValues = null;
					timeStamps = this.todoTimeStamps;
					this.todoTimeStamps = null;
					schedulerAvailable.signal();
				}
			} catch (InterruptedException e) {
				break;
			} finally {
				lock.unlock();
			}
			if (isInterrupted())
				break;
			stackOutputsAndTrigger(source, outputValues, timeStamps);
			while (!todoBlocks.isEmpty()) {
				if (isInterrupted())
					break dead;
				Block block = todoBlocks.popFirst();
				if ((!dead || block.isConsumesAllDataBeforeDying()) && runBlock(block))
					stackOutputsAndTrigger(block);
			}
		}
		lock.lock();
		try {
			schedulerAvailable.signalAll();
			incomingDataOrTaskSignals.signalAll();
		} finally {
			lock.unlock();
		}
		consumeTasks();
		blocks = fd.getBlocks();
		blocks.sort((a, b) -> Integer.compare(b.getStopPriority(), a.getStopPriority()));
		for (Block block : blocks) {
			blockingBlock.add(block);
			if (isInterrupted)
				Thread.currentThread().interrupt();
			DiagramScheduler.destroyOperator(block);
			blockingBlock.remove(block);
		}
	}

	private boolean runBlock(Block block) {
		// if (!block.isReady())
		// return false;
		Triple<Object[], long[], long[]> blockBuff = blockBuffs.get(block);
		Object[] inputs;
		long[] ts;
		long[] toi;
		if (blockBuff == null || blockBuff.getFirst().length != block.getNbInput()) {
			inputs = new Object[block.getNbInput()];
			ts = new long[block.getNbInput()];
			toi = new long[block.getNbInput()];
			blockBuffs.put(block, new Triple<>(inputs, ts, toi));
		} else {
			inputs = blockBuff.getFirst();
			ts = blockBuff.getSecond();
			toi = blockBuff.getThird();
		}
		List<BlockInput> blockInputs = block.getInputs();
		for (int i = 0; i < inputs.length; i++) {
			IOData ioData = blockInputs.get(i).pop();
			if (ioData != null) {
				inputs[i] = ioData.getValue();
				ts[i] = ioData.getTs();
				toi[i] = ioData.getToi();
			} else {
				inputs[i] = null;
				ts[i] = -1;
				toi[i] = -1;
			}
		}
		long oldCallingThreadId = callingThreadId;
		callingThreadId = Thread.currentThread().getId();
		boolean hasReturnValue = DiagramScheduler.processBlock(block, inputs, ts, toi);
		callingThreadId = oldCallingThreadId;
		return hasReturnValue;
	}

	private void stackOutputsAndTrigger(Block block) {
		stackOutputsAndTrigger(block, block.getOutputBuff(), block.getOutputBuffTs());
	}

	private void stackOutputsAndTrigger(IOComponent comp, Object[] outputs, long[] outputsTs) {
		if (!comp.canTriggerOrBeTriggered())
			return;
		long timeOfIssue = System.currentTimeMillis();
		IOLinks[] index = comp.getIndex();
		if (comp instanceof FlowDiagram)
			System.out.println("sortie de diagram de flux");
		for (IOLinks ioLinks : index) {
			IOComponent linkedComp = ioLinks.getComponent();
			if (linkedComp.canTriggerOrBeTriggered()) {
				boolean needToProcess = false;
				for (IOLink ioLink : ioLinks.getLinks()) {
					int outputIndex = ioLink.getOutputIndex();
					Object outputObject = outputs[outputIndex];
					if (outputObject == null)
						continue;
					needToProcess = true;
					if (ioLink.getInput().getBuffer().push(outputsTs[outputIndex], timeOfIssue, ioLink.isNeedToCopy() ? DiagramScheduler.clone(outputObject) : outputObject))
						DiagramScheduler.showBufferOverflow(comp, ioLink.getInput());
					Link link = ioLink.getInput().getLink();
					if (link != null)
						link.consume();
				}
				if (needToProcess)
					todoBlocks.push((Block) linkedComp); // Plante si c'est un flowDiagram
			}
		}
	}

	public void triggerOutput(IOComponent source, Object[] outputValues, long[] timeStamps) {
		Thread currentThread = Thread.currentThread();
		if (currentThread == this || monoRemoteThread == currentThread)
			triggerOutputFromMono(source, outputValues, timeStamps);
		else if (source instanceof Block && ((Block) source).getRemoteOperator() != null && ((Block) source).getRemoteOperator().isFromThreadedBlock(callingThreadId)) {
			Thread oldMonoThread = monoRemoteThread;
			monoRemoteThread = Thread.currentThread();
			triggerOutputFromMono(source, outputValues, timeStamps);
			monoRemoteThread = oldMonoThread;
		} else {
			if (currentThread.isInterrupted()) {
				Thread.currentThread().interrupt();
				return;
			}
			dataSend: while (!dead) {
				lock.lock();
				try {
					while (!isInterrupted()) {
						if (dead)
							return;
						if (this.todoSource == null) {
							this.todoSource = source;
							this.todoOutputValues = outputValues;
							this.todoTimeStamps = timeStamps;
							incomingDataOrTaskSignals.signal();
							break dataSend;
						}
						schedulerAvailable.await();
					}
				} catch (InterruptedException e) {
					Thread.currentThread().interrupt();
					return;
				} finally {
					lock.unlock();
				}
			}
		}
	}

	private void triggerOutputFromMono(IOComponent source, Object[] outputValues, long[] timeStamps) {
		stackOutputsAndTrigger(source, outputValues, timeStamps);
		while (!todoBlocks.isEmpty()) {
			Block block = todoBlocks.popFirst();
			if (runBlock(block))
				stackOutputsAndTrigger(block);
		}
	}

	public void triggerTask(Object source, Runnable task) {
		Thread currentThread = Thread.currentThread();
		if (currentThread == this || monoRemoteThread == currentThread)
			task.run();
		else if (source instanceof Block && ((Block) source).getRemoteOperator() != null && ((Block) source).getRemoteOperator().isFromThreadedBlock(callingThreadId)) {
			Thread oldMonoThread = monoRemoteThread;
			monoRemoteThread = Thread.currentThread();
			task.run();
			monoRemoteThread = oldMonoThread;
		} else {
			if (lock.isHeldByCurrentThread())
				task.run();
			else {
				lock.lock();
				try {
					Condition schedulerAvailable = lock.newCondition();
					Condition taskCompleted = lock.newCondition();
					tasks.add(new Tuple<>(schedulerAvailable, taskCompleted));
					incomingDataOrTaskSignals.signal();
					try {
						schedulerAvailable.await();
						task.run();
						taskCompleted.signal();
					} catch (InterruptedException e) {
						task.run();
					}
				} finally {
					lock.unlock();
				}
			}

		}
	}
}

class BlockRingBuffer {
	private int maxSize;
	private int front;
	private int rear;
	private int bufLen;
	private Block[] blocks;

	public BlockRingBuffer() {
		front = 0;
		rear = 0;
		bufLen = 0;
		this.maxSize = 20;
		blocks = new Block[maxSize];
	}

	public BlockRingBuffer(int maxStackSize, BlockRingBuffer buffer) {
		front = 0;
		rear = 0;
		bufLen = 0;
		this.maxSize = maxStackSize;
		blocks = new Block[maxSize];
		if (buffer != null) {
			Block block;
			while ((block = buffer.popFirst()) != null)
				push(block);
		}
	}

	public boolean isEmpty() {
		return bufLen == 0;
	}

	public Block popFirst() {
		if (bufLen == 0)
			return null;
		Block block = blocks[front];
		blocks[front] = null;
		front += 1;
		if (front == maxSize)
			front = 0;
		bufLen--;
		return block;
	}

	public void push(Block block) {
		if (bufLen == maxSize) {
			BlockRingBuffer newBlockRingBuffer = new BlockRingBuffer(maxSize * 2, this);
			maxSize = newBlockRingBuffer.maxSize;
			front = newBlockRingBuffer.front;
			rear = newBlockRingBuffer.rear;
			bufLen = newBlockRingBuffer.bufLen;
			blocks = newBlockRingBuffer.blocks;
		}
		bufLen++;
		blocks[rear] = block;
		rear += 1;
		if (rear == maxSize)
			rear = 0;
	}
}

//Record
class OnConsumeTask {
	public final Object source;
	public final Runnable runnable;
	
	public OnConsumeTask(Object source, Runnable runnable) {
		this.source = source;
		this.runnable = runnable;
	}
}