/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario.dataflowdiagram;

public abstract class Output extends IO {
	protected Input[] inputs;

	public Output(Class<?> type, String name) {
		super(type, name);
	}

	void addLink(Link link) {
		if (inputs == null)
			inputs = new Input[] { link.getInput() };
		else {
			Input[] newInputs = new Input[inputs.length + 1];
			int i = 0;
			for (; i < inputs.length; i++)
				newInputs[i] = inputs[i];
			newInputs[i] = link.getInput();
			inputs = newInputs;
		}
	}

	public Input[] getLinkInputs() {
		return inputs;
	}

	void removeLink(Link link) {
		Input input = link.getInput();
		if (inputs.length == 1)
			inputs = null;
		else {
			Input[] newInputs = new Input[inputs.length - 1];
			int index = 0;
			for (Input _input : inputs)
				if (_input != input)
					newInputs[index++] = _input;
			inputs = newInputs;
		}
	}
}
