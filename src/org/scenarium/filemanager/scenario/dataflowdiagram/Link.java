/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario.dataflowdiagram;

public class Link {
	private final Input input;
	private final Output output;
	private int nbConsume;

	public Link(Input input, Output output) {
		this.input = input;
		this.output = output;
	}

	public void consume() {
		nbConsume++;
	}

	public int getConsume() {
		return nbConsume;
	}

	public Input getInput() {
		return input;
	}

	public Output getOutput() {
		return output;
	}

	public void resetConsume() {
		nbConsume = 0;
	}

	@Override
	public String toString() {
		return (output == null ? String.valueOf((Object) null) : output.toString()) + " -> " + (input == null ? String.valueOf((Object) null) : input.toString());
	}
}
