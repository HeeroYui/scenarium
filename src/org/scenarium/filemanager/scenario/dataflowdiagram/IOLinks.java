/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario.dataflowdiagram;

import java.util.Arrays;

public class IOLinks {
	private final IOComponent component;
	private final IOLink[] links;

	public IOLinks(IOComponent component, int iOSize) {
		this.component = component;
		this.links = new IOLink[iOSize];
	}

	public IOComponent getComponent() {
		return component;
	}

	public IOLink[] getLinks() {
		return links;
	}

	@Override
	public String toString() {
		return component.toString() + Arrays.toString(links);
	}
}
