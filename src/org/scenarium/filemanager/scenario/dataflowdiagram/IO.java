/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario.dataflowdiagram;

import java.util.Objects;

import javax.swing.event.EventListenerList;

import org.beanmanager.editors.TransientProperty;

import javafx.geometry.Point2D;

abstract public class IO {
	private final EventListenerList listeners = new EventListenerList();
	protected Class<?> type;
	private String name;
	private Point2D position;
	@TransientProperty
	private String errorMessage;
	// protected final Type type;

	public IO(Class<?> type, String name/* , Type type */) {
		this.type = type;
		this.name = name;
		// this.type = type;
	}

	public void addIONameChangeListener(DiagramIONameChangeListener listener) {
		listeners.add(DiagramIONameChangeListener.class, listener);
	}

	public void fireNameChange() {
		for (DiagramIONameChangeListener listener : listeners.getListeners(DiagramIONameChangeListener.class))
			listener.nameChanged(this);
	}

	public String getName() {
		return name;
	}

	public Point2D getPosition() {
		return position;
	}

	public Class<?> getType() {
		return type;
	}

	public void removeIONameChangeListener(DiagramIONameChangeListener listener) {
		listeners.remove(DiagramIONameChangeListener.class, listener);
	}

	public void setName(String name) {
		this.name = name;
		fireNameChange();
	}

	public void setPosition(Point2D position) {
		this.position = position;
	}

	public void setError(String message) {
		if (!Objects.equals(this.errorMessage, message)) {
			this.errorMessage = message;
			fireErrorChange();
		}
	}

	public String getError() {
		return errorMessage;
	}

	public void addErrorChangeListener(InputErrorChangeListener listener) {
		listeners.add(InputErrorChangeListener.class, listener);
	}

	public void removeErrorChangeListener(InputErrorChangeListener listener) {
		listeners.remove(InputErrorChangeListener.class, listener);
	}

	public void fireErrorChange() {
		for (InputErrorChangeListener listener : listeners.getListeners(InputErrorChangeListener.class))
			listener.errorChange();
	}

	@Override
	public String toString() {
		return name;
	}
}

enum Type {
	PROPERTY, STATIC, DYNAMIC, VARARGS
}
