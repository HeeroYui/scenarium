/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario.dataflowdiagram;

import java.util.HashMap;
import java.util.List;

import javax.swing.event.EventListenerList;

import org.beanmanager.BeanRenameListener;
import org.beanmanager.editors.container.BeanEditor;
import org.scenarium.filemanager.scenario.dataflowdiagram.operator.DeclaredInputChangeListener;
import org.scenarium.filemanager.scenario.dataflowdiagram.operator.DeclaredOutputChangeListener;
import org.scenarium.filemanager.scenario.dataflowdiagram.operator.OperatorManager;
import org.scenarium.filemanager.scenario.dataflowdiagram.operator.RemoteBlock;
import org.scenarium.filemanager.scenario.dataflowdiagram.scheduler.MultiCoreDiagramScheduler;
import org.scenarium.filemanager.scenario.dataflowdiagram.scheduler.Trigger;
import org.scenarium.filemanager.scenariomanager.StructChangeListener;

import javafx.scene.layout.Region;

/** EvolvedOperator is the base class for all scenarium operators who needs to perform advanced operations. It allow among other things to:
 * <ul>
 * <li>Manage inputs ans outputs</li>
 * <li>Get timestamp and time of issue information of datas</li>
 * <li>Trigger datas with custom timeStamp</li>
 * <li>Get information from the block link to this operator</li>
 * <li>Prevent any concurrent method call</li>
 * <li>Restart and reload the structure of the block</li>
 * <li>Customize the HMI of the block and allow human interaction</li>
 * <li>Listen events as structure change, block name change, etc...</li>
 * </ul>
 *
 * By default, except if user don't want depedency with Scenarium, all operators must extend this class. An operator must also have a process method to be a valid Scenarium operator. To check if an
 * operator is valid for Scenarium, you can test it with {@link OperatorManager#isOperator(Object) this} method. To add an operator to Scenarium, you can use
 * {@link OperatorManager#addInternOperator(Class) this} method */
public abstract class EvolvedOperator {
	private final EventListenerList listeners = new EventListenerList();
	private Block block;
	private RemoteBlock remoteBlock;
	private int id = -1;
	private Trigger trigger;
	private Object[] additionalInputs;
	private int earlyTrigger;
	private int lateTrigger;
	private HashMap<BeanRenameListener, BeanRenameListener> beanRenameListenersMap;

	/** Adds a BeanRenameListener which will be notified whenever the name of the block changes.
	 *
	 * @param listener the listener to register */
	public void addBlockNameChangeListener(BeanRenameListener listener) {// TODO si set block changer listener
		RemoteBlock _remoteBlock = remoteBlock;
		if (block != null) {
			BeanRenameListener brl = (oldBeanDesc, beanDesc) -> {
				Block _block = block;
				if (_block != null && beanDesc.bean == _block.getOperator())
					listener.beanRename(oldBeanDesc, beanDesc);
			};
			HashMap<BeanRenameListener, BeanRenameListener> _beanRenameListenersMap = beanRenameListenersMap;
			if (_beanRenameListenersMap == null)
				_beanRenameListenersMap = new HashMap<>();
			_beanRenameListenersMap.put(listener, brl);
			beanRenameListenersMap = _beanRenameListenersMap;
			BeanEditor.addStrongRefBeanRenameListener(brl);
		} else if (_remoteBlock != null)
			_remoteBlock.addBlockNameChangeListener(listener);
	};

	/** Adds a DeclaredInputChangeListener which will be notified whenever the structure inputs changes.
	 *
	 * @param listener the listener to register */
	public void addDeclaredInputChangeListener(DeclaredInputChangeListener listener) {
		if (block != null)
			listeners.add(DeclaredInputChangeListener.class, listener);
		else {
			RemoteBlock _remoteBlock = remoteBlock;
			if (_remoteBlock != null)
				_remoteBlock.addDeclaredInputChangeListener(listener);
		}
	}

	/** Adds a DeclaredOutputChangeListener which will be notified whenever the structure outputs changes.
	 *
	 * @param listener the listener to register */
	public void addDeclaredOutputChangeListener(DeclaredOutputChangeListener listener) {
		if (block != null)
			listeners.add(DeclaredOutputChangeListener.class, listener);
		else {
			RemoteBlock _remoteBlock = remoteBlock;
			if (_remoteBlock != null)
				_remoteBlock.addDeclaredOutputChangeListener(listener);
		}
	}

	/** Add listener for input links changed.
	 *
	 * @param listener the listener to register */
	public void addInputLinksChangeListener(InputLinksChangeListener listener) {
		Block _block = block;
		if (_block != null)
			_block.addInputLinksChangeListener(listener);
		else {
			RemoteBlock _remoteBlock = remoteBlock;
			if (_remoteBlock != null)
				_remoteBlock.addInputLinksChangeListener(listener);
		}
	}

	/** Adds a StructChangeListener which will be notified whenever the structure of inputs and/or outputs changes.
	 *
	 * @param listener the listener to register */
	public void addStructChangeListener(StructChangeListener listener) {
		Block _block = block;
		if (_block != null)
			listeners.add(StructChangeListener.class, listener);
		else {
			RemoteBlock _remoteBlock = remoteBlock;
			if (_remoteBlock != null)
				_remoteBlock.addStructChangeListener(listener);
		}
	}

	/** Add listener for variadic input changed.
	 *
	 * @param listener the listener to register */
	public void addVarArgsInputChangeListener(VarArgsInputChangeListener listener) {
		Block _block = block;
		if (_block != null)
			_block.addVarArgsInputChangeListener(listener);
		else {
			RemoteBlock _remoteBlock = remoteBlock;
			if (_remoteBlock != null)
				_remoteBlock.addVarArgsInputChangeListener(listener);
		}
	}

	/** Method call at startup */
	public abstract void birth() throws Exception;

	/** Method call at the end */
	public abstract void death() throws Exception;

	/** Fires declared input changed events with the array of input names and input types. This method is called in the call of {@link #updateInputs(String[], Class[]) updateInputs} and after the
	 * fires of structure changed events
	 *
	 * @param names the names array of new inputs
	 *
	 * @param types the types array of new outputs */
	public void fireDeclaredInputChanged(String[] names, Class<?>[] types) {
		if (block != null)
			for (DeclaredInputChangeListener listener : listeners.getListeners(DeclaredInputChangeListener.class))
				listener.declaredInputChanged(names, types);
		else {
			RemoteBlock _remoteBlock = remoteBlock;
			if (_remoteBlock != null)
				_remoteBlock.fireDeclaredInputChanged(names, types);
		}
	}

	/** Fires declared output changed events with the array of output names and output types. This method is called in the call of {@link #updateOutputs(String[], Class[]) updateOutputs} and after the
	 * fires of structure changed events
	 *
	 * @param names the names array of new outputs
	 *
	 * @param types the types array of new outputs */
	public void fireDeclaredOutputChanged(String[] names, Class<?>[] types) {
		if (block != null)
			for (DeclaredOutputChangeListener listener : listeners.getListeners(DeclaredOutputChangeListener.class))
				listener.declaredOutputChanged(names, types);
		else {
			RemoteBlock _remoteBlock = remoteBlock;
			if (_remoteBlock != null)
				_remoteBlock.fireDeclaredOutputChanged(names, types);
		}

	}

	/** Fires structure changed events with the reference to the block linked to this operator or null otherwise. It will also update the bounds of the block if it exists.
	 *
	 * @param blockName */
	public void fireStructChanged() {
		Block _block = block;
		if (_block != null)
			_block.updateBounds();
		for (StructChangeListener listener : listeners.getListeners(StructChangeListener.class))
			listener.structChanged();
		RemoteBlock _remoteBlock = remoteBlock;
		if (_remoteBlock != null)
			_remoteBlock.fireStructChanged();
	}

	/** Generate a new output array that can be used to trigger datas.
	 *
	 * @return an array of outputs. It size is equal to the number of output of this block */
	protected Object[] generateOuputsVector() {
		int nbOutput = getNbOutput();
		return nbOutput == -1 ? null : new Object[nbOutput];
	}

	// /** Inform Scenarium that the block is ready to be processed.
	// *
	// * @param inputs An array of boolean with a size corresponding to the number of inputs of the block. True for the i-th element corresponds to an input ready to be trigger for the i-th input of
	// the block
	// *
	// * @return true if the block need to be triggered */
	// public boolean isBlockReady(boolean[] inputs) {
	// for (boolean b : inputs)
	// if (b)
	// return true;
	// return false;
	// }

	/** Get the array of additional dynamic inputs. These inputs can be declared with the {@link #updateInputs(String[], Class[]) updateInputs} method. This array is always updated when entering in
	 * the process method of the operator.
	 *
	 * @return the array of additional dynamic inputs. It size is equal to the number of additional dynamic inputs declared and the i-th element corresponds to the data of the i-th input */
	public Object[] getAdditionalInputs() {
		return additionalInputs;
	}

	/** Get the name of the block.
	 *
	 * @return the block name or null if this operator is not embedded in a block */
	public String getBlockName() {
		Block _block = block;
		if (_block != null)
			return _block.getName();
		RemoteBlock _remoteBlock = remoteBlock;
		return _remoteBlock != null ? _remoteBlock.getBlockName() : null;
	}

	/** Get the index of an input with it name.
	 *
	 * @param inputName the name of the desired input
	 *
	 * @return the index of the input */
	public int getInputIndex(String inputName) {
		Block _block = block;
		if (_block != null)
			return _block.getInputIndex(inputName);
		RemoteBlock _remoteBlock = remoteBlock;
		return _remoteBlock != null ? _remoteBlock.getInputIndex(inputName) : -1;
	}

	/** Gets the input names of the block link to this operator.
	 *
	 * @return the input names array. It size is equal to the number of block inputs and the i-th element corresponds to the name of the i-th input */
	public String[] getInputsName() {
		Block _block = block;
		if (_block != null) {
			List<BlockInput> inputs = _block.getInputs();
			String[] inputNames = new String[inputs.size()];
			for (int i = 0; i < inputs.size(); i++)
				inputNames[i] = inputs.get(i).getName();
			return inputNames;
		}
		RemoteBlock _remoteBlock = remoteBlock;
		return _remoteBlock != null ? _remoteBlock.getInputsName() : null;
	}

	/** Get the maximum timestamp of all input datas. The timestamp correspond to the computer time of the original data. This information can be generated and is propaged by Scenarium but can be
	 * override using {@link #triggerOutput(Object[], long[]) triggerOutput} method (or one of these overloaded function).
	 *
	 * @return the maximum timeStamp of all input datas */
	public long getMaxTimeStamp() {
		Block _block = block;
		if (_block != null)
			return _block.getMaxTimeStamp();
		RemoteBlock _remoteBlock = remoteBlock;
		return _remoteBlock != null ? _remoteBlock.getMaxTimeStamp() : -1;
	}

	/** Gets the number of input of the block.
	 *
	 * @return the number of input */
	public int getNbInput() {
		Block _block = block;
		if (_block != null)
			return _block.getNbInput(); // RAS
		RemoteBlock _remoteBlock = remoteBlock;
		return _remoteBlock != null ? _remoteBlock.getNbInput() : -1;
	}

	/** Gets the number of output.
	 *
	 * @return the number of output */
	public int getNbOutput() {
		Block _block = block;
		if (_block != null)
			return _block.getNbOutput();
		RemoteBlock _remoteBlock = remoteBlock;
		return _remoteBlock != null ? _remoteBlock.getNbOutput() : -1;
	}

	/** Return the node property of this operator. This method return null by default and must be overidden in the operator implementation if the user wants to use an specific HMI for this block. It
	 * can be just a representation of the state of this the and/or controls to interract with him.
	 *
	 * @return the node property */
	public Region getNode() {
		return null;
	}

	/** Get the index of an output with it name.
	 *
	 * @param outputName the name of the desired output
	 *
	 * @return the index of the output */
	public int getOutputIndex(String outputName) {
		Block _block = block;
		if (_block != null)
			return _block.getOutputIndex(outputName);
		RemoteBlock _remoteBlock = remoteBlock;
		return _remoteBlock != null ? _remoteBlock.getOutputIndex(outputName) : -1;
	}

	/** Get the names of the outputs link to block inputs.
	 *
	 * @return the outputs names array or null if this operator is not embedded in a block. It size is equal to the number of block input and the i-th element corresponds to the output name link to
	 *         the i-th input of the block */
	public String[] getOutputLinkToInputName() {
		Block _block = block;
		if (_block != null) {
			List<BlockInput> inputs = _block.getInputs();
			String[] outputLinkToInputName = new String[inputs.size()];
			for (int i = 0; i < inputs.size(); i++) {
				Link link = inputs.get(i).getLink();
				if (link != null)
					outputLinkToInputName[i] = link.getOutput().getName();
			}
			return outputLinkToInputName;
		}
		RemoteBlock _remoteBlock = remoteBlock;
		return _remoteBlock != null ? _remoteBlock.getOutputLinkToInputName() : null;
	}

	/** Get the type of the outputs link to block inputs.
	 *
	 * @return the outputs types array or null if this operator is not embedded in a block. It size is equal to the number of block input and the i-th element corresponds to the output name link to
	 *         the i-th input of the block */
	public Class<?>[] getOutputLinkToInputType() {
		Block _block = block;
		if (_block != null) {
			List<BlockInput> inputs = _block.getInputs();
			Class<?>[] outputLinkToInputType = new Class<?>[inputs.size()];
			for (int i = 0; i < inputs.size(); i++) {
				Link link = inputs.get(i).getLink();
				if (link != null)
					outputLinkToInputType[i] = link.getOutput().getType();
			}
			return outputLinkToInputType;
		}
		RemoteBlock _remoteBlock = remoteBlock;
		return _remoteBlock != null ? _remoteBlock.getOutputLinkToInputType() : null;
	}

	/** Gets the outputs names of the block.
	 *
	 * @return the outputs names array. It size is equal to the number of block output and the i-th element corresponds to the name of the i-th output */
	public String[] getOutputsName() {
		Block _block = block;
		if (_block != null) {
			List<BlockOutput> outputs = _block.getOutputs();
			String[] outputNames = new String[outputs.size()];
			for (int i = 0; i < outputs.size(); i++)
				outputNames[i] = outputs.get(i).getName();
			return outputNames;
		}
		RemoteBlock _remoteBlock = remoteBlock;
		return _remoteBlock != null ? _remoteBlock.getOutputsName() : null;
	}

	/** Get the time of issue of the input data. The time of issue correspond to the computer time when the input data was triggered. This information is generated by Scenarium and cannot be override.
	 *
	 * @param indexOfInput the index of the desired input
	 *
	 * @return the time of issue of the input data */
	public long getTimeOfIssue(int indexOfInput) {
		Block _block = block;
		if (_block != null)
			return _block.getTimeOfIssue(indexOfInput);
		RemoteBlock _remoteBlock = remoteBlock;
		return _remoteBlock != null ? _remoteBlock.getTimeOfIssue(indexOfInput) : -1;
	}

	/** Get the timestamp of the input data. The timestamp correspond to the computer time of the original data. This information can be generated and is propaged by Scenarium but can be override
	 * using {@link #triggerOutput(Object[], long[]) triggerOutput} method (or one of these overloaded function).
	 *
	 * @param indexOfInput the index of the desired input
	 *
	 * @return the timeStamp of the input data */
	public long getTimeStamp(int indexOfInput) {
		Block _block = block;
		if (_block != null)
			return _block.getTimeStamp(indexOfInput);
		RemoteBlock _remoteBlock = remoteBlock;
		return _remoteBlock != null ? _remoteBlock.getTimeStamp(indexOfInput) : -1;
	}

	/** Get warning property. Warning means that an event has occurred and requires warning the user.
	 *
	 * @return the description of the block warning or null if there is no warning */
	public String getWarning() {
		Block _block = block;
		if (_block != null)
			return _block.getWarning();
		RemoteBlock _remoteBlock = remoteBlock;
		return _remoteBlock != null ? _remoteBlock.getWarning() : null;
	}

	/** Returns whether this operator is embedded in a block
	 *
	 * @return true if this operator is embedded in a block */
	protected boolean hasBlock() {
		return block != null || remoteBlock != null;
	}

	/** Initialize the dynamique inputs and outputs */
	public void initStruct() throws Exception {}

	/** Returns whether the block is defaulting. Defaulting means that an unexpected error has occurred.
	 *
	 * @return true if the block is defaulting */
	public boolean isDefaulting() {
		Block _block = block;
		if (_block != null)
			return _block.isDefaulting();
		RemoteBlock _remoteBlock = remoteBlock;
		return _remoteBlock != null ? _remoteBlock.isDefaulting() : false;
	}

	/** Return true if input objects are exclusive to this block or false if they are shared by multiple blocks.
	 *
	 * @return true if input objects are exclusive to this block */
	protected boolean isExclusiveObjectInput() {
		if (remoteBlock != null)
			return true;
		Trigger _trigger = trigger;
		return _trigger != null && _trigger instanceof MultiCoreDiagramScheduler;
	}

	/** Returns whether the property is as input.
	 *
	 * @param propertyName the name of the desired property
	 *
	 * @return true if the property is as input */
	public boolean isPropertyAsInput(String propertyName) {
		Block _block = block;
		if (_block != null)
			return _block.isPropertyAsInput(propertyName);
		RemoteBlock _remoteBlock = remoteBlock;
		return _remoteBlock != null ? _remoteBlock.isPropertyAsInput(propertyName) : false;
	}

	/** Returns whether the block is running. A running block is a block with a trigger.
	 *
	 * @return true if the block is running */
	protected boolean isRunning() {
		return trigger != null || remoteBlock != null;
	}

	/** Tell if the trigger is ready to trigger datas. If not, this method increment the earlyTrigger or lateTrigger count.
	 *
	 * @param trigger the trigger link to this operator
	 *
	 * @param block the block link to this operator
	 *
	 * @return true if the trigger is ready */
	private boolean isTriggerReady(Trigger trigger, Block block) {
		if (block != null && !block.canTriggerOrBeTriggered())
			return false;
		if (!trigger.isStarted()) {
			earlyTrigger++;
			if (earlyTrigger == 1 || earlyTrigger == 10 || earlyTrigger == 100 || earlyTrigger == 1000 || earlyTrigger == 10000 || earlyTrigger == 100000)
				System.err.println("Early trigger of: " + block.getName() + " data removed. Occurence: " + earlyTrigger);
		} else if (!trigger.isAlive()) {
			if (lateTrigger == 1 || lateTrigger == 10 || lateTrigger == 100 || lateTrigger == 1000 || lateTrigger == 10000 || lateTrigger == 100000)
				System.err.println("Late trigger of: " + block.getName() + " data removed. Occurence: " + lateTrigger);
		} else
			return true;
		return false;
	}

	/** Warns Scenarium that this block need to be saved if needed. By default, Scenarium saves an operators if it changes one of these properties. But some properties can changed even if scenarium
	 * don't changed them. User can then override this method to warn scenarium that this operator need to be saved. By default, this method return false.
	 *
	 * @return true if the block need to be saved */
	public boolean needToBeSaved() {
		return false;
	}

	/** An action to execute at the start of the diagram, after the end of the execution of all the birth methods of each block, or immediately if the diagram is already started. This method must be
	 * called from the birth method.
	 *
	 * @param runnable the Runnable whose run method will be executed at the start of the diagram
	 * @throws IllegalAccessError if this operator is not link to a block or if the block is already started or dead */
	public void onStart(Runnable runnable) {
		Block _block = block;
		Trigger _trigger = trigger;
		if (_block == null || _trigger == null) {
			RemoteBlock _remoteBlock = remoteBlock;
			if (_remoteBlock != null)
				_remoteBlock.onStart(runnable);
			else
				new IllegalAccessError("This operator is not link to a " + (_trigger == null ? "scheduler" : block) + ". The started event cannot therefore occur");
		} else
			_trigger.onStart(_block, runnable);
	}

	/** An action to execute each time the diagram is resumed.
	 *
	 * @param runnable the Runnable whose run method will be executed when the diagram is resumed */
	public void onResume(Runnable runnable) {
		Block _block = block;
		Trigger _trigger = trigger;
		if (_block == null || _trigger == null) {
			RemoteBlock _remoteBlock = remoteBlock;
			if (_remoteBlock != null)
				_remoteBlock.onResume(runnable);
			else
				new IllegalAccessError("This operator is not link to a " + (_trigger == null ? "scheduler" : block) + ". The started event cannot therefore occur");
		} else
			_trigger.onResume(_block, runnable);
	}

	/** An action to execute each time the diagram is paused.
	 *
	 * @param runnable the Runnable whose run method will be executed when the diagram is paused */
	public void onPause(Runnable runnable) {
		Block _block = block;
		Trigger _trigger = trigger;
		if (_block == null || _trigger == null) {
			RemoteBlock _remoteBlock = remoteBlock;
			if (_remoteBlock != null)
				_remoteBlock.onPause(runnable);
			else
				new IllegalAccessError("This operator is not link to a " + (_trigger == null ? "scheduler" : block) + ". The started event cannot therefore occur");
		} else
			_trigger.onPause(_block, runnable);
	}

	/** An action to execute at the stop of the diagram, before the start of the execution of all the death methods of each block. This method must be called before the death of the operator.
	 *
	 * @param runnable the Runnable whose run method will be executed at the stop of the diagram
	 * @throws IllegalAccessError if this operator is not link to a block or if the block is already dead */
	public void onStop(Runnable runnable) {
		Block _block = block;
		Trigger _trigger = trigger;
		if (_block == null || _trigger == null) {
			RemoteBlock _remoteBlock = remoteBlock;
			if (_remoteBlock != null)
				_remoteBlock.onStop(runnable);
			else
				new IllegalAccessError("This operator is not link to a " + (_trigger == null ? "scheduler" : block) + ". The started event cannot therefore occur");
		} else
			_trigger.onStop(_block, runnable);
	}

	/** Removes the given listener from the list of listeners, that are notified whenever the name of the block changes.
	 *
	 * @param listener the listener to remove */
	public void removeBlockNameChangeListener(BeanRenameListener listener) {
		BeanRenameListener brl;
		HashMap<BeanRenameListener, BeanRenameListener> _beanRenameListenersMap = beanRenameListenersMap;
		if (_beanRenameListenersMap != null && (brl = _beanRenameListenersMap.remove(listener)) != null) {
			BeanEditor.removeStrongRefBeanRenameListener(brl);
			if (_beanRenameListenersMap.isEmpty())
				_beanRenameListenersMap = null;
		} else {
			RemoteBlock _remoteBlock = remoteBlock;
			if (_remoteBlock != null)
				_remoteBlock.removeBlockNameChangeListener(listener);
		}
	}

	/** Removes the given listener from the list of listeners, that are notified whenever the structure inputs changes.
	 *
	 * @param listener the listener to remove */
	public void removeDeclaredInputChangeListener(DeclaredInputChangeListener listener) {
		if (block != null)
			listeners.remove(DeclaredInputChangeListener.class, listener);
		else {
			RemoteBlock _remoteBlock = remoteBlock;
			if (_remoteBlock != null)
				_remoteBlock.removeDeclaredInputChangeListener(listener);
		}
	}

	/** Removes the given listener from the list of listeners, that are notified whenever the structure outputs changes.
	 *
	 * @param listener the listener to remove */
	public void removeDeclaredOutputChangeListener(DeclaredOutputChangeListener listener) {
		if (block != null)
			listeners.remove(DeclaredOutputChangeListener.class, listener);
		else {
			RemoteBlock _remoteBlock = remoteBlock;
			if (_remoteBlock != null)
				_remoteBlock.removeDeclaredOutputChangeListener(listener);
		}
	}

	/** Remove listener for input links changed.
	 *
	 * @param listener the listener to remove */
	public void removeInputLinksChangeListener(InputLinksChangeListener listener) {
		Block _block = block;
		if (_block != null)
			_block.removeInputLinksChangeListener(listener);
		else {
			RemoteBlock _remoteBlock = remoteBlock;
			if (_remoteBlock != null)
				_remoteBlock.removeInputLinksChangeListener(listener);
		}
	}

	/** Removes the given listener from the list of listeners, that are notified whenever the structure of inputs and/or outputs changes.
	 *
	 * @param listener the listener to remove */
	public void removeStructChangeListener(StructChangeListener listener) {
		Block _block = block;
		if (_block != null)
			listeners.remove(StructChangeListener.class, listener);
		else {
			RemoteBlock _remoteBlock = remoteBlock;
			if (_remoteBlock != null)
				_remoteBlock.removeStructChangeListener(listener);
		}
	}

	/** Remove listener for variadic input changed.
	 *
	 * @param listener the listener to remove */
	public void removeVarArgsInputChangeListener(VarArgsInputChangeListener listener) {
		Block _block = block;
		if (_block != null)
			_block.removeVarArgsInputChangeListener(listener);
		else {
			RemoteBlock _remoteBlock = remoteBlock;
			if (_remoteBlock != null)
				_remoteBlock.removeVarArgsInputChangeListener(listener);
		}
	}

	/** This method restart this operator only if it is schedule. In that case, this method call the {@link #death() death} method and then the {@link #birth() birth} method. It is preferable to use
	 * the restartLater method if this operator is sensitive to concurrent method call of {@link #birth() birth}, process and {@link #death() death} method. */
	public void restart() {
		if (isRunning())
			try {
				death();
				birth();
			} catch (Exception e) {
				e.printStackTrace();
			}
	}

	/** This method restart and reload the structure of this operator. The reload is always done but the restart is only done if this operator is schedule. The restart corresponds to the call of the
	 * {@link #death() death} method and then the {@link #birth() birth} method. The reload corresponds to the call of the {@link #initStruct() initStruct}, done between the {@link #death() death} and
	 * the {@link #birth() birth} method in the case of a schedule operator or alone otherwise. It is preferable to use the {@link #restartAndReloadStructLater() restartAndReloadStructLater} method if
	 * this operator is sensitive to concurrent method call of {@link #birth() birth}, process and {@link #death() death} method. */
	public void restartAndReloadStruct() {
		try {
			if (isRunning()) {
				death();
				initStruct();
				birth();
			} else
				initStruct();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/** This method call the {@link #restartAndReloadStruct() restartAndReloadStruct} in a {@link #runLater(Runnable) runLater} method to avoid any concurrent method call of {@link #birth() birth},
	 * process and {@link #death() death} method. */
	public void restartAndReloadStructLater() {
		if (isRunning())
			runLater(() -> restartAndReloadStruct());
		else
			restartAndReloadStruct();
	}

	/** This method call the {@link #restart() restart} method in a {@link #runLater(Runnable) runLater} method to avoid any concurrent method call of {@link #birth() birth}, process and
	 * {@link #death() death} method. */
	public void restartLater() {
		if (isRunning())
			runLater(() -> restart());
		else
			restart();
	}

	/** Run the specified Runnable on the block's thread if it exists or immediately otherwise. This method will return only after the execution of the Runnable. It prevent any concurrent method call
	 * of {@link #birth() birth}, process, {@link #death() death} method and the specified Runnable. The process is slightly different if this method is called from the scenarium process or from an
	 * isolated/remote process block. In the first case, if this method is called from the block's thread, the Runnable will be call immediately. In the second case, if this method is called from the
	 * birth, process or death method, the Runnable will be call immediately. The result is very close except in the case where another thread than the Scenarium thread calls the birth, process or
	 * death method. But this case means that we are already in a concurrent method call context and therefore this method is useless.
	 *
	 * @param runnable the Runnable whose run method will be executed on the block's thread if it exists or immediately otherwise */
	public void runLater(Runnable runnable) {
		Block _block = block;
		Trigger _trigger = trigger;
		if (_block == null || _trigger == null) {
			RemoteBlock _remoteBlock = remoteBlock;
			if (_remoteBlock != null)
				_remoteBlock.runLater(runnable);
			else
				runnable.run();
		} else
			_trigger.triggerTask(_block, runnable);
	}

	/** Set the array of additional dynamic inputs. Take care to not set it if this operator is already managed by Scenarium.
	 *
	 * @param additionalInputs the array of additional dynamic inputs */
	public void setAdditionalInputs(Object[] additionalInputs) {
		this.additionalInputs = additionalInputs;
	}

	/** Link this operator to the specified block. Take care to not set it if this operator is already managed by Scenarium.
	 *
	 * @param block the block linked to this operator */
	public void setBlock(Block block) {
		this.block = block;
	}

	/** Set defaulting property. Defaulting means that an unexpected error has occurred.
	 *
	 * @param defaulting true if the block is defaulting */
	public void setDefaulting(boolean defaulting) {
		Block _block = block;
		if (_block != null)
			_block.setDefaulting(true);
		else {
			RemoteBlock _remoteBlock = remoteBlock;
			if (_remoteBlock != null)
				remoteBlock.setDefaulting(defaulting);
		}
	}

	/** Set the id of this operator. This id is used by the {@link #triggerOutput(Object[], long[]) triggerOutput} method (or one of these overloaded function) to identify the source of the data if
	 * this operator is not linked to a block.
	 *
	 * @param id the id of this operator. This identifier is useful to identify the source of a data. */
	public void setId(int id) {
		this.id = id;
	}

	/** Set the property as input.
	 *
	 * @param propertyName the name of the desired property
	 *
	 * @param asInput true if the property must be an input or false otherwise
	 *
	 * @return true if the change is made successfully */
	public boolean setPropertyAsInput(String propertyName, boolean asInput) {
		boolean success = false;
		Block _block = block;
		if (_block != null) {
			if (_block.setPropertyAsInput(propertyName, asInput)) {
				fireStructChanged();
				success = true;
			}
		} else {
			RemoteBlock _remoteBlock = remoteBlock;
			if (_remoteBlock != null && _remoteBlock.setPropertyAsInput(propertyName, asInput)) {
				fireStructChanged();
				success = true;
			}
		}
		return success;
	}

	/** Set the remote block reference. This property is used by Scenarium if the instance of the block link to this operator and the instance of this operator is not in the same process. In this
	 * case, this property is used to define its remote block reference. Thus, most calls made by this operator to the block will be done using RMI with this remote block reference. Take care to not
	 * set it if this operator is already managed by Scenarium.
	 *
	 * @param remoteBlock the remote block reference used by RMI to get or send information from the operator to the block */
	public void setRemoteBlock(RemoteBlock remoteBlock) {
		this.remoteBlock = remoteBlock;
	}

	/** Set the trigger for the block. Take care to not set it if this operator is already schedule by Scenarium.
	 *
	 * @param trigger the trigger for this operator. */
	public void setTrigger(Trigger trigger) {
		earlyTrigger = 0;
		lateTrigger = 0;
		this.trigger = trigger;
	}

	/** Set warning property. Warning means that an event has occurred and requires warning the user.
	 *
	 * @param warning a description of the warning or null if there is no warning */
	public void setWarning(String warning) {
		Block _block = block;
		if (_block != null)
			_block.setWarning(warning);
		else {
			RemoteBlock _remoteBlock = remoteBlock;
			if (_remoteBlock != null)
				remoteBlock.setWarning(warning);
		}
	}

	/** Get the name of this operator. This method returns {@link #getBlockName() getBlockName} if non null or {@link Object#toString() toString} otherwise.
	 *
	 * @return a string representation of the operator */
	@Override
	public String toString() {
		String name = getBlockName();
		return name != null ? name : id != -1 ? Integer.toString(id) : super.toString();
	}

	/** Trigger one output with one timeStamp which correspond to the maximum timestamp of inputs or current time if there is no input.
	 *
	 * @param outputValue the output to trigger
	 *
	 * @return true if the output is successfully triggered
	 *
	 * @throws IllegalArgumentException if outputValue is null */
	public boolean triggerOutput(Object outputValue) {
		if (outputValue == null)
			throw new IllegalArgumentException(getBlockName() + ": OutputValue cannot be null");
		Trigger _trigger = trigger;
		if (_trigger != null) {
			Block _block = block;
			long maxTimeStamp = _block == null ? System.currentTimeMillis() : _block.getMaxTimeStamp();
			long[] timeStamps = new long[1];
			for (int i = 0; i < timeStamps.length; i++)
				timeStamps[i] = maxTimeStamp;
			if (isTriggerReady(_trigger, _block)) {
				_trigger.triggerOutput(_block == null ? id : _block, outputValue, maxTimeStamp);
				return true;
			}
		} else {
			RemoteBlock _remoteBlock = remoteBlock;
			if (_remoteBlock != null)
				_remoteBlock.triggerOutput(outputValue);
		}
		return false;
	}

	/** Trigger one output with one custom timestamp.
	 *
	 * @param outputValue the outputValue to trigger
	 *
	 * @return true if the outputValue is successfully triggered
	 *
	 * @throws IllegalArgumentException if outputValue is null */
	public boolean triggerOutput(Object outputValue, long timeStamp) {
		if (outputValue == null)
			throw new IllegalArgumentException(getBlockName() + ": OutputValue cannot be null");
		Block _block = block;
		Trigger _trigger = trigger;
		if (_trigger != null) {
			if (isTriggerReady(_trigger, _block)) {
				_trigger.triggerOutput(_block == null ? id : _block, outputValue, timeStamp);
				return true;
			}
		} else {
			RemoteBlock _remoteBlock = remoteBlock;
			if (_remoteBlock != null)
				_remoteBlock.triggerOutput(outputValue, timeStamp);
		}
		return false;
	}

	/** Trigger outputs with one timeStamp which correspond to the maximum timestamp of inputs, or current time if there is no input.
	 *
	 * @param outputValues the outputValues to trigger
	 *
	 * @return true if outputValues is successfully triggered
	 *
	 * @throws IllegalArgumentException if outputValues is null */
	public boolean triggerOutput(Object[] outputValues) {
		if (outputValues == null)
			throw new IllegalArgumentException(getBlockName() + ": OutputValues cannot be null");
		Block _block = block;
		if (_block != null && outputValues.length != _block.getNbOutput())
			throw new IllegalArgumentException(getBlockName() + ": The number of outputs: " + _block.getNbOutput() + " does not correspond to the size of the output vector: " + outputValues.length);
		Trigger _trigger = trigger;
		if (_trigger != null) {
			if (isTriggerReady(_trigger, _block)) {
				long maxTimeStamp = _block == null ? System.currentTimeMillis() : _block.getMaxTimeStamp();
				long[] timeStamps = new long[outputValues.length];
				for (int i = 0; i < timeStamps.length; i++)
					timeStamps[i] = maxTimeStamp;
				_trigger.triggerOutput(_block == null ? id : _block, outputValues, maxTimeStamp);
				return true;
			}
		} else {
			RemoteBlock _remoteBlock = remoteBlock;
			if (_remoteBlock != null)
				_remoteBlock.triggerOutput(outputValues);
		}
		return false;
	}

	/** Trigger outputs with one custom timestamp.
	 *
	 * @param outputValues the outputValues to trigger
	 *
	 * @param timeStamp the timestamp of all outputValues
	 *
	 * @return true if outputValues is successfully triggered
	 *
	 * @throws IllegalArgumentException if outputValues is null */
	public boolean triggerOutput(Object[] outputValues, long timeStamp) {
		if (outputValues == null)
			throw new IllegalArgumentException(getBlockName() + ": OutputValues cannot be null");
		Block _block = block;
		if (_block != null && outputValues.length != _block.getNbOutput()) {
			System.out.println(trigger + " " + ProcessHandle.current().pid());
			throw new IllegalArgumentException(getBlockName() + ": The number of outputs: " + _block.getNbOutput() + " does not correspond to the size of the output vector: " + outputValues.length);
		}
		Trigger _trigger = trigger;
		if (_trigger != null) {
			if (isTriggerReady(_trigger, _block)) {
				_trigger.triggerOutput(_block == null ? id : _block, outputValues, timeStamp);
				return true;
			}
		} else {
			RemoteBlock _remoteBlock = remoteBlock;
			if (_remoteBlock != null)
				_remoteBlock.triggerOutput(outputValues, timeStamp);
		}
		return false;
	}

	// /**
	// * Adds a NameChangeListener which will be notified whenever an input or an output name changes.
	// *
	// * @param listener the listener to register
	// */
	// public void addIONameChangeListener(NameChangeListener listener) {
	// if (block != null)
	// listeners.add(NameChangeListener.class, listener);
	// else {
	// RemoteBlock _remoteBlock = remoteBlock;
	// if (_remoteBlock != null)
	// _remoteBlock.addIONameChangeListener(listener);
	// }
	// }

	// /**
	// * Removes the given listener from the list of listeners, that are notified whenever an input or an output name changes.
	// *
	// * @param listener the listener to register
	// */
	// public void removeIONameChangeListener(NameChangeListener listener) {
	// if (block != null)
	// listeners.remove(NameChangeListener.class, listener);
	// else {
	// RemoteBlock _remoteBlock = remoteBlock;
	// if (_remoteBlock != null)
	// _remoteBlock.removeIONameChangeListener(listener);
	// }
	// }

	// /**
	// * Fires IO name change events.
	// *
	// * @param input true if the IO is an input or false if it is an output
	// *
	// * @param indexOfInput the index of the input to rename
	// *
	// * @param newName the new name for this input
	// */
	// private void fireIONameChanged(boolean input, int indexOfInput, String newName) {
	// if (block != null)
	// for (NameChangeListener listener : listeners.getListeners(NameChangeListener.class))
	// listener.nameChanged(input, indexOfInput, newName);
	// else {
	// RemoteBlock _remoteBlock = remoteBlock;
	// if (_remoteBlock != null)
	// _remoteBlock.fireIONameChanged(input, indexOfInput, newName);
	// }
	// }

	/** Trigger outputs with multiple custom timestamp. Prefer this method instead of other triggerOutput methods to avoid object instanciation. The outputValues and timeStamps array parameters
	 * corresponds to the array of outputs with their corresponding timestamps and they must therefore have the same size.
	 *
	 * @param outputValues the outputValues to trigger
	 *
	 * @param timeStamps the timeStamps array where the i-th element corresponds to the timestamp of the i-th element in the outputValues array
	 *
	 * @return true if outputValues is successfully triggered
	 *
	 * @throws IllegalArgumentException if outputValues or timeStamps is null or if outputValues and timeStamps does not have the same size */
	public boolean triggerOutput(Object[] outputValues, long[] timeStamps) {
		if (timeStamps == null)
			throw new IllegalArgumentException(getBlockName() + ": TimeStamps cannot be null");
		if (outputValues == null)
			throw new IllegalArgumentException(getBlockName() + ": OutputValues cannot be null");
		if (timeStamps.length != outputValues.length)
			throw new IllegalArgumentException(getBlockName() + ": TimeStamps and outputValues does not have the same size");
		Block _block = block;
		if (_block != null && outputValues.length != _block.getNbOutput())
			throw new IllegalArgumentException(getBlockName() + ": The number of outputs: " + _block.getNbOutput() + " does not correspond to the size of the output vector: " + outputValues.length);
		Trigger _trigger = trigger;
		if (_trigger != null) {
			if (isTriggerReady(_trigger, _block)) {
				_trigger.triggerOutput(_block == null ? id : _block, outputValues, timeStamps);
				return true;
			}
		} else {
			RemoteBlock _remoteBlock = remoteBlock;
			if (_remoteBlock != null)
				_remoteBlock.triggerOutput(outputValues, timeStamps);
		}
		return false;
	}

	/** Update dynamic inputs of the block. The names and types array parameters corresponds to the array of names and types of new inputs and they must therefore have the same size
	 *
	 * @param names the names array of new inputs
	 *
	 * @param types the types array of new inputs
	 *
	 * @return true if the dynamic inputs evolved
	 *
	 * @throws IllegalArgumentException if names or types is null or if one of their elements is null or if names and types does not have the same size */
	public boolean updateInputs(String[] names, Class<?>[] types) { // Pk synchronized avant???
		Block _block = block;
		if (_block != null) {
			if (_block.updateDynamicInputs(names, types)) {
				fireStructChanged();
				fireDeclaredInputChanged(names, types);
				return true;
			}
		} else {
			RemoteBlock _remoteBlock = remoteBlock;
			if (_remoteBlock != null)
				_remoteBlock.updateInputs(names, types);
		}
		return false;
	}

	/** Update dynamic outputs. The names and types array parameters corresponds to the array of names and types of new outputs and they must therefore have the same size
	 *
	 * @param names the names array of new outputs
	 *
	 * @param types the types array of new outputs
	 *
	 * @return true if the dynamic outputs evolved
	 *
	 * @throws IllegalArgumentException if names or types is null or if one of their elements is null or if names and types does not have the same size */
	public boolean updateOutputs(String[] names, Class<?>[] types) {
		Block _block = block;
		if (_block != null) {
			if (_block.updateDynamicOuputs(names, types)) {
				fireStructChanged();
				fireDeclaredOutputChanged(names, types);
				return true;
			}
		} else {
			RemoteBlock _remoteBlock = remoteBlock;
			if (_remoteBlock != null)
				return _remoteBlock.updateOutputs(names, types);
		}
		return false;
	}
}
