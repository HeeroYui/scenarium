/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario.dataflowdiagram;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyDescriptor;
import java.lang.annotation.Annotation;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;

import javax.swing.event.EventListenerList;

import org.beanmanager.BeanManager;
import org.beanmanager.BeanRenameListener;
import org.beanmanager.editors.DynamicVisibleBean;
import org.beanmanager.editors.PropertyInfo;
import org.beanmanager.editors.TransientProperty;
import org.beanmanager.editors.container.BeanEditor;
import org.beanmanager.editors.primitive.number.ControlType;
import org.beanmanager.editors.primitive.number.NumberInfo;
import org.scenarium.filemanager.scenario.dataflowdiagram.operator.EvolvedVarArgsOperator;
import org.scenarium.filemanager.scenario.dataflowdiagram.operator.OperatorManager;
import org.scenarium.filemanager.scenario.dataflowdiagram.operator.rmi.RemoteOperator;
import org.scenarium.filemanager.scenario.dataflowdiagram.scheduler.DiagramScheduler;
import org.scenarium.filemanager.scenariomanager.StructChangeListener;
import org.scenarium.struct.Unit;

import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;

public class Block implements IOComponent, CpuUsageMeasurable, LocalisedObject, DynamicVisibleBean {
	public static final String methodOutputPrefix = "getOutput";
	private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);
	public final Object operator;
	private List<BlockInput> inputs = Collections.unmodifiableList(new ArrayList<>()); // Le nombre de patte peux avoir changé entre un getNbInput et l'accès...
	private List<BlockOutput> outputs = Collections.unmodifiableList(new ArrayList<>());
	private ReentrantLock ioLock = new ReentrantLock();
	private Rectangle2D rectangle;
	@TransientProperty
	private boolean defaulting;
	@TransientProperty
	private String warning;
	@TransientProperty
	private boolean birthFailed = false;
	@PropertyInfo(index = 1, info = "Specify if the block is enable and is executable at runtime")
	private boolean enable;
	@PropertyInfo(index = 2, info = "Indicates whether all data should be consumed before the operator's death")
	private boolean consumesAllDataBeforeDying = false;
	@PropertyInfo(index = 3, info = "Block priority when starting a diagram. Blocks with a higher priority will be initialized before blocks with a lower priority")
	@NumberInfo(controlType = ControlType.SPINNER)
	private int startPriority;
	@PropertyInfo(index = 4, info = "Block priority when stopping a diagram. Blocks with a higher priority will be stopped before blocks with a lower priority")
	@NumberInfo(controlType = ControlType.SPINNER)
	private int stopPriority;
	@PropertyInfo(index = 5, info = "Priority of block's thread. Only used for diagram in multicore mode")
	@NumberInfo(min = Thread.MIN_PRIORITY, max = Thread.MAX_PRIORITY, controlType = ControlType.SPINNER)
	private int threadPriority = Thread.NORM_PRIORITY;

	@PropertyInfo(index = 13, info = "Specify the execution mode:\nLOCAL: The block is executed in the Scenarium process\nISOLATED: The block is executed in a separate process\nREMOTE: The block is executed on a remote machine")
	private ProcessMode processMode;
	@PropertyInfo(index = 14, info = "Socket port used for the communication between isolate process and Scenarium process")
	private int isolatePort;
	@PropertyInfo(index = 15, info = "Synchronize properties between the local and the remote operator")
	private boolean synchronizeProperties;
	@PropertyInfo(index = 16, info = "Ip and port of the remote machine used to process the block")
	private InetSocketAddress remoteIp;
	@PropertyInfo(index = 17, info = "Create an remote operator on the remote machine and request properties on this one.\nThis mode must be used if the choice of some properties depends of the remote machine (ex. serial communication port available).")
	private boolean remoteProperty;

	private final EventListenerList listeners = new EventListenerList();
	private int nbPropertyInput;
	private int nbStaticInput;
	private int nbDynamicInput;
	private int nbVarArgsInput = -1;
	private int nbStaticOutput;
	@TransientProperty
	private float cpuUsage = -1;
	// private LinkedList<IODatas> entrieConsumptionStack;
	private boolean processMethodReturnVoid;
	private long nbConsume;
	private SetterPropertyHandler[] propertyMethodHandles;
	private MethodHandle processMethodHandler;
	private Object[] varArgsBuff;
	private Object[] outputBuff;
	private long[] outputBuffTs;
	private long[] outputBuffToi;
	private long[] inputTs;
	private long[] inputTOI;
	private IOLinks[] index;
	@TransientProperty
	private Thread thread;
	@TransientProperty
	private RemoteOperator remoteOperator;
	private PropertyChangeListener runTimePropertyChangeListener;
	// Function used for non-EvolvedOperator to change inputs or outputs without synchronization issue
	private Consumer<Runnable> runLaterFunction;
	private final BeanRenameListener brl;

	public Block(Object operator, Point2D location, boolean isMiddlePoint) throws IllegalInputArgument {
		brl = (oldBeanDesc, beanDesc) -> {
			if (beanDesc.bean == operator)
				pcs.firePropertyChange("name", oldBeanDesc.name, beanDesc.name);
		};
		BeanEditor.addStrongRefBeanRenameListener(brl);
		if (!OperatorManager.isOperator(operator))
			throw new IllegalArgumentException("the object operator don't respect operator rules");
		if (operator == null)
			new IllegalArgumentException("operator cannot be null");
		if (location == null)
			location = new Point2D(0, 0);
		resetProperties();
		this.operator = operator;
		populate();
		int width = 100;
		int height = computeHeight();
		rectangle = new Rectangle2D(isMiddlePoint ? location.getX() - width / 2 : location.getX(), isMiddlePoint ? location.getY() - height / 2 : location.getY(), width, height);
		// System.out.println("create block: " + this);
	}

	public boolean birthFailed() {
		return birthFailed;
	}

	@Override
	public boolean canTriggerOrBeTriggered() {
		return enable && !birthFailed;
	}

	private void clearIOPos() {
		for (BlockInput input : inputs)
			input.setPosition(null);
		for (BlockOutput output : outputs)
			output.setPosition(null);
	}

	public void clearTemporaryBuffer() {
		if (varArgsBuff != null)
			Arrays.fill(varArgsBuff, null);
		Arrays.fill(outputBuff, null);
		Arrays.fill(outputBuffTs, -1);
		Arrays.fill(outputBuffToi, -1);
	}

	private int computeHeight() {
		return Math.max(120, (getNbOutput() + getNbInput()) * 25); // Re event si rechange
	}

	public void consume() {
		nbConsume++;
	}

	public long getConsume() {
		return nbConsume;
	}

	public float getCpuUsage() {
		return cpuUsage;
	}

	@Override
	public IOLinks[] getIndex() {
		if (index != null)
			return index;
		int[] iOList = new int[50]; // 50 est juste une taille de cache, il peut grossir tant que necessaire
		int indexIOList = 0;
		ArrayList<IOComponent> compoList = new ArrayList<>(50);
		int nbOutput = getNbOutput();
		for (int i = 0; i < nbOutput; i++) {
			Input[] inputs = outputs.get(i).getLinkInputs();
			if (inputs == null)
				continue;
			for (int j = 0; j < inputs.length; j++) {
				boolean success = false;
				Input nextInput = inputs[j];
				IOComponent nextComp = nextInput.getComponent();
				for (int k = 0; k < compoList.size(); k++) {
					IOComponent _ioComp = compoList.get(k);
					if (nextComp == _ioComp) {
						iOList[k]++;
						success = true;
						break;
					}
				}
				if (!success) {
					if (indexIOList == iOList.length) {
						int[] _iOList = new int[iOList.length * 2];
						System.arraycopy(iOList, 0, _iOList, 0, iOList.length);
						iOList = _iOList;
					}
					iOList[indexIOList++]++;
					compoList.add(nextComp);
				}
			}
		}
		IOLinks[] index = new IOLinks[compoList.size()];
		for (int i = 0; i < index.length; i++)
			index[i] = new IOLinks(compoList.get(i), iOList[i]);
		for (int i = 0; i < nbOutput; i++) {
			Input[] inputs = outputs.get(i).getLinkInputs();
			if (inputs == null)
				continue;
			boolean isFirst = true;
			for (int j = 0; j < inputs.length; j++) {
				IOLinks ioLink = null;
				Input nextInput = inputs[j];
				IOComponent nextComp = nextInput.getComponent();
				for (IOLinks _ioLink : index)
					if (nextComp == _ioLink.getComponent()) {
						ioLink = _ioLink;
						break;
					}
				IOLink[] blockLinks = ioLink.getLinks();
				int l = 0;
				while (blockLinks[l] != null)
					l++;
				blockLinks[l] = new IOLink(i, j, nextInput, isFirst);
				if (isFirst)
					isFirst = false;
			}
		}
		this.index = index;
		return index;
	}

	public int getInputIndex(String inputName) {
		for (int i = 0; i < inputs.size(); i++)
			if (inputs.get(i).getName().equals(inputName))
				return i - nbPropertyInput;
		throw new IllegalArgumentException(inputName + " is not an input of " + getName());
	}

	@Override
	public List<BlockInput> getInputs() {
		return inputs;
	}

	public ReentrantLock getIOLock() {
		return ioLock;
	}

	public int getIsolatePort() {
		return isolatePort;
	}

	/** Get the maximum timestamp of inputs or current time if there is no input.
	 *
	 * @param outputValue the output to trigger
	 *
	 * @return true if the output is successfully triggered
	 *
	 * @throws IllegalArgumentException if outputValue is null */
	public long getMaxTimeStamp() {
		if (inputTs == null)
			return System.currentTimeMillis();
		long maxTimeStamp = Long.MIN_VALUE;
		for (long ts : inputTs)
			if (ts > maxTimeStamp)
				maxTimeStamp = ts;
		return maxTimeStamp;
	}

	@PropertyInfo(index = 0, info = "Name of the block")
	public String getName() {
		return BeanEditor.getBeanDesc(operator).name;
	}

	public int getNbDynamicInput() {
		return nbDynamicInput;
	}

	public int getNbInput() {
		return inputs.size();
	}

	public int getNbOutput() {
		if (outputs == null)
			return 0;
		return outputs.size();
	}

	public int getNbPropertyInput() {
		return nbPropertyInput;
	}

	public int getNbStaticInput() {
		return nbStaticInput;
	}

	public int getNbStaticOutput() {
		return nbStaticOutput;
	}

	public int getNbVarArgsInput() {
		return nbVarArgsInput;
	}

	public Object getOperator() {
		return operator;
	}

	@Override
	public Object[] getOutputBuff() {
		return outputBuff;
	}

	public long[] getOutputBuffToi() {
		return outputBuffToi;
	}

	@Override
	public long[] getOutputBuffTs() {
		return outputBuffTs;
	}

	public int getOutputIndex(String outputName) {
		for (int i = 0; i < outputs.size(); i++)
			if (outputs.get(i).getName().equals(outputName))
				return i;
		throw new IllegalArgumentException(outputName + " is not an output of " + getName());
	}

	@Override
	public List<BlockOutput> getOutputs() {
		return outputs;
	}

	@Override
	public Point2D getPosition() {
		return new Point2D((float) (rectangle.getMinX() + rectangle.getWidth() / 2.0), (float) (rectangle.getMinY() + rectangle.getHeight() / 2.0));
	}

	public MethodHandle getProcessHandler() {
		return processMethodHandler;
	}

	public ProcessMode getProcessMode() {
		return this.processMode;
	}

	public SetterPropertyHandler getPropertyMethodHandles(int inputIndex) {
		return propertyMethodHandles[inputIndex];
	}

	public Rectangle2D getRectangle() {
		return rectangle;
	}

	public InetSocketAddress getRemoteIp() {
		return remoteIp;
	}

	public RemoteOperator getRemoteOperator() {
		return remoteOperator;
	}

	public Thread getThread() {
		return thread;
	}

	public long getTimeOfIssue(int i) {
		return inputTOI[i + nbPropertyInput];
	}

	public long getTimeStamp(int i) {
		return inputTs[i + nbPropertyInput];
	}

	public Object[] getVarArgsBuff() {
		return varArgsBuff;
	}

	public String getWarning() {
		return warning;
	}

	public int indexOfInput(BlockInput input) {
		return inputs.indexOf(input);
	}

	public boolean isDefaulting() {
		return defaulting;
	}

	public boolean isEnable() {
		return enable;
	}

	public boolean isProcessMethodReturnVoid() {
		return processMethodReturnVoid;
	}

	public boolean isPropertyAsInput(String propertyName) {
		if (propertyName == null)
			throw new IllegalArgumentException("PropertyName cannot be null");
		for (int i = 0; i < nbPropertyInput; i++)
			if (inputs.get(i).getName().equals(propertyName))
				return true;
		return false;
	}

	@Override
	public boolean isReady() {
		for (int k = 0; k < inputs.size(); k++)
			if (!inputs.get(k).getBuffer().isEmpty())
				return true;
		return false;
	}

	public boolean isRemoteProperty() {
		return remoteProperty;
	}

	public boolean isRunTimePropertyChangeListener() {
		return runTimePropertyChangeListener != null;
	}

	public boolean isSynchronizeProperties() {
		return synchronizeProperties;
	}

	private void populate() throws IllegalInputArgument {
		inputs = populateInputs();
		outputs = populateOutputs();
		processMethodReturnVoid = true;
		if (outputs != null)
			for (BlockOutput output : outputs)
				if (output.getMethodHandle() == null)
					processMethodReturnVoid = false;
		nbStaticOutput = outputs != null ? outputs.size() : 0;
		outputBuff = new Object[nbStaticOutput];
		outputBuffTs = new long[nbStaticOutput];
		outputBuffToi = new long[nbStaticOutput];
		if (nbVarArgsInput != -1)
			varArgsBuff = (Object[]) Array.newInstance(inputs.get(inputs.size() - 1).getType(), nbVarArgsInput);
		if (operator instanceof EvolvedOperator) {
			((EvolvedOperator) operator).setBlock(this);
			try {
				((EvolvedOperator) operator).initStruct();
			} catch (Exception e) {
				System.err.println("error while init the structure of the operator: " + operator + "\n" + e.getClass().getSimpleName() + ": " + e.getMessage());
				e.printStackTrace();
			}
		}
	}

	private List<BlockInput> populateInputs() throws IllegalInputArgument {
		if (operator == null)
			return null;
		nbVarArgsInput = -1;
		ArrayList<BlockInput> inputs = new ArrayList<>();
		HashSet<String> inputsName = new HashSet<>();
		for (Method method : operator.getClass().getMethods())
			if (method.getName().equals("process")) {
				method.setAccessible(true);
				try {
					processMethodHandler = MethodHandles.lookup().unreflect(method).bindTo(operator);
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
				String[] names = null;
				for (Annotation anno : method.getAnnotations())
					if (anno instanceof ParamInfo) {
						names = ((ParamInfo) anno).in();
						break;
					}
				int i = 0;
				Parameter[] parameters = method.getParameters();
				int nbParam = parameters.length;
				for (int j = 0; j < nbParam; j++) {
					Parameter param = parameters[j];
					Class<?> type = param.getType();
					if (type.isPrimitive())
						throw new IllegalInputArgument(
								"Process method of the block: " + this + " contains forbidden primitive type: " + type + ". It must be replace by " + BeanManager.toWrapper(type).getSimpleName());
					boolean isVarArgs = method.isVarArgs() && j == parameters.length - 1;
					String inputName = names != null && i < names.length ? names[i++] : param.isNamePresent() ? param.getName() : type.getSimpleName();
					if (isVarArgs) {
						inputName += "-0";
						nbVarArgsInput = 0;
					}
					if (!inputsName.contains(inputName)) {
						inputs.add(new BlockInput(this, isVarArgs ? type.getComponentType() : type, inputName, isVarArgs));
						inputsName.add(inputName);
					} else
						throw new IllegalInputArgument("The block: " + this + " contains multiple intputs " + inputName + "\nAdd or change input name of input to avoid confusion");
				}
				break;
			}
		nbStaticInput = inputs.size();
		if (nbVarArgsInput == 0)
			nbStaticInput--;
		return Collections.unmodifiableList(inputs);
	}

	private List<BlockOutput> populateOutputs() {
		if (operator == null)
			return null;
		ArrayList<BlockOutput> outputs = new ArrayList<>();
		Method[] methods = operator.getClass().getMethods();
		HashSet<String> outputsName = new HashSet<>();
		for (Method method : methods)
			if (method.getDeclaringClass() != EvolvedOperator.class && method.getName().startsWith(methodOutputPrefix) && method.getParameterTypes().length == 0
					&& !method.getReturnType().equals(Void.TYPE))
				try {
					String outputName = method.getName().substring(methodOutputPrefix.length());
					method.setAccessible(true);
					outputs.add(new BlockOutput(this, MethodHandles.lookup().unreflect(method).bindTo(operator), method.getReturnType(), outputName));
					outputsName.add(outputName);
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
		Collections.sort(outputs, (o1, o2) -> o1.getName().compareTo(o2.getName()));
		Method youngestProcessMethod = null; // PK???
		for (Method method : methods)
			if (method.getName().equals("process") && !method.getReturnType().equals(Void.TYPE)
					&& (youngestProcessMethod == null || !method.getReturnType().isAssignableFrom(youngestProcessMethod.getReturnType())))
				youngestProcessMethod = method;
		if (youngestProcessMethod != null) {
			String outputName = null;
			for (Annotation anno : youngestProcessMethod.getAnnotations())
				if (anno instanceof ParamInfo) {
					outputName = ((ParamInfo) anno).out();
					break;
				}
			Class<?> returnType = youngestProcessMethod.getReturnType();
			if (DiagramScheduler.isCloneable(BeanManager.toWrapper(returnType))) {
				if (outputName == null)
					outputName = returnType.getSimpleName();
				if (!outputsName.contains(outputName)) {
					BlockOutput output = new BlockOutput(this, null, returnType, outputName);
					outputs.add(0, output);
					outputsName.add(outputName);
				} else
					System.err.println("The block: " + this + " contains multiple outputs" + outputName);
			} else
				System.err.println("The block: " + this + " contains output" + outputName + " of type: " + returnType.getSimpleName() + " that is not cloneable");
		}
		return Collections.unmodifiableList(outputs);
	}

	public void reset() {
		cpuUsage = -1;
		nbConsume = 0;
		if (operator instanceof EvolvedOperator)
			((EvolvedOperator) operator).setAdditionalInputs(null);
		for (BlockInput input : inputs) {
			input.getBuffer().clear();
			Link link = input.getLink();
			if (link != null)
				link.resetConsume();
		}
		setWarning(null);
	}

	public void resetIndex() {
		index = null;
	}

	public void resetProperties() {
		setEnable(true);
		setConsumesAllDataBeforeDying(false);
		setStartPriority(0);
		setStopPriority(0);
		setThreadPriority(Thread.NORM_PRIORITY);
		setProcessMode(ProcessMode.LOCAL);
		setIsolatePort(1099);
		setSynchronizeProperties(true);
		setRemoteIp(new InetSocketAddress(0));
		setRemoteProperty(false);
	}

	public void setBirthFailed(boolean birthFailed) {
		this.birthFailed = birthFailed;
	}

	@Override
	public void setCpuUsage(float cpuUsage) {
		if (cpuUsage > 1)
			cpuUsage = 1;
		this.cpuUsage = cpuUsage;
	}

	public void setDefaulting(boolean defaulting) {
		if (this.defaulting != defaulting) {
			this.defaulting = defaulting;
			fireRunningStateChange();
		}
	}

	public void setEnable(boolean enable) {
		boolean oldEnable = this.enable;
		this.enable = enable;
		pcs.firePropertyChange("enable", oldEnable, enable);
	}

	public boolean isConsumesAllDataBeforeDying() {
		return consumesAllDataBeforeDying;
	}

	public void setConsumesAllDataBeforeDying(boolean consumesAllDataBeforeDying) {
		boolean oldConsumesAllDataBeforeDying = this.consumesAllDataBeforeDying;
		this.consumesAllDataBeforeDying = consumesAllDataBeforeDying;
		pcs.firePropertyChange("consumesAllDataBeforeDying", oldConsumesAllDataBeforeDying, consumesAllDataBeforeDying);
	}

	public int getStartPriority() {
		return startPriority;
	}

	public void setStartPriority(int startPriority) {
		int oldStartPriority = this.startPriority;
		this.startPriority = startPriority;
		pcs.firePropertyChange("startPriority", oldStartPriority, startPriority);
	}

	public int getStopPriority() {
		return stopPriority;
	}

	public void setStopPriority(int stopPriority) {
		int oldStopPriority = this.stopPriority;
		this.stopPriority = stopPriority;
		pcs.firePropertyChange("stopPriority", oldStopPriority, stopPriority);
	}

	public int getThreadPriority() {
		return threadPriority;
	}

	public void setThreadPriority(int threadPriority) {
		int oldValue = this.threadPriority;
		this.threadPriority = threadPriority;
		pcs.firePropertyChange("threadPriority", oldValue, threadPriority);
	}

	public void setInputTs(long[] ts) {
		this.inputTs = ts;
	}

	public void setIsolatePort(int isolatePort) {
		int oldIsolatePort = this.isolatePort;
		this.isolatePort = isolatePort;
		pcs.firePropertyChange("isolatePort", oldIsolatePort, isolatePort);
	}

	public void setName(String name) {
		String oldName = BeanEditor.getBeanDesc(operator).name;
		BeanEditor.renameBean(BeanEditor.getBeanDesc(operator), name);
		pcs.firePropertyChange("name", oldName, name);
	}

	@Override
	public void setPosition(double x, double y) {
		rectangle = new Rectangle2D((int) (x - rectangle.getWidth() / 2.0), (int) (y - rectangle.getHeight() / 2.0), rectangle.getWidth(), rectangle.getHeight());
	}

	public void setProcessMode(ProcessMode processMode) {
		if (this.processMode != processMode) {
			ProcessMode oldProcessMode = this.processMode;
			this.processMode = processMode;
			pcs.firePropertyChange("processMode", oldProcessMode, processMode);
			setVisible();
		}
		if (processMode != ProcessMode.REMOTE)
			setRemoteProperty(false);
	}

	public static void main(String[] args) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		var object = new Object() {
			@SuppressWarnings("unused")
			public Object getNull() {
				return null;
			}
		};
		Method m = object.getClass().getMethod("getNull");
		System.out.println("res: " + m.invoke(object));
	}

	public boolean setPropertyAsInput(String propertyName, boolean asInput) {
		if (propertyName == null)
			throw new IllegalArgumentException("PropertyName cannot be null");
		Unit<Boolean> success = new Unit<>(true);
		runTaskLater(() -> {
			for (int i = 0; i < nbPropertyInput; i++) {
				BlockInput input = inputs.get(i);
				if (input.getName().equals(propertyName)) {
					if (!asInput) {
						input.setLink(null, false);
						ArrayList<BlockInput> _inputs = new ArrayList<>(inputs);
						_inputs.remove(i);
						nbPropertyInput--;
						if (nbPropertyInput != 0) {
							SetterPropertyHandler[] newPropertyMethodHandles = new SetterPropertyHandler[nbPropertyInput];
							int index = 0;
							for (int j = 0; j < propertyMethodHandles.length; j++)
								if (i != j)
									newPropertyMethodHandles[index++] = propertyMethodHandles[j];
							propertyMethodHandles = newPropertyMethodHandles;
						} else
							propertyMethodHandles = null;
						inputs = Collections.unmodifiableList(_inputs);
						firePropertyStructChange();
						return;// true;
					}
					success.element = false;
					return;// false;
				}
			}
			if (asInput)
				try {
					String[] propertiesNameSequence = propertyName.split("-");
					Object[] propertiesobjectSequence = new Object[propertiesNameSequence.length - 1];
					GetterPropertyHandler[] propertiesGetterHandlerSequence = new GetterPropertyHandler[propertiesobjectSequence.length];
					Object po = operator;
					for (int j = 0; j < propertiesNameSequence.length; j++) {
						String pn = propertiesNameSequence[j];
						PropertyDescriptor ppd = findProperty(po.getClass(), pn);
						if (ppd == null) { // Cannot find the property...
							success.element = false;
							return;
						}
						if (j == propertiesNameSequence.length - 1) {
							ArrayList<BlockInput> _inputs = new ArrayList<>(inputs);
							_inputs.add(nbPropertyInput, new BlockInput(this, ppd.getPropertyType(), propertyName, false));
							nbPropertyInput++;
							try {
								SetterPropertyHandler[] newPropertyMethodHandles = new SetterPropertyHandler[nbPropertyInput];
								if (propertyMethodHandles != null)
									for (int i = 0; i < propertyMethodHandles.length; i++)
										newPropertyMethodHandles[i] = propertyMethodHandles[i];
								Method wm = ppd.getWriteMethod();
								wm.setAccessible(true);
								var sphc = new Object() {
									SetterPropertyHandler sph;
								};
								SetterPropertyHandler sph = new SetterPropertyHandler(po, wm, j == 1 ? null : () -> {
									for (int i = 0; i < propertiesGetterHandlerSequence.length; i++) {
										GetterPropertyHandler propertiesGetterHandler = propertiesGetterHandlerSequence[i];
										try {
											Object currentProperty = propertiesGetterHandler.get();
											Object registerProperty = propertiesobjectSequence[i];
											if (registerProperty == null)
												System.err.println("Error in SetterPropertyHandler");
											if (currentProperty != registerProperty) {
												if (currentProperty == null)
													throw new BrokerPropertyException(propertyName, "The property: " + propertiesNameSequence[i] + " of the "
															+ (i == 0 ? "block : " + getName() : "sub bean: " + propertiesobjectSequence[i - 1]) + " is null ");
												else if (currentProperty.getClass() == registerProperty.getClass()) {
													propertiesobjectSequence[i] = currentProperty;
													if (i + 1 < propertiesGetterHandlerSequence.length)
														propertiesGetterHandlerSequence[i + 1].reBindTo(currentProperty);
													else
														sphc.sph.reBindTo(propertiesobjectSequence[propertiesobjectSequence.length - 1]);
												} else {
													int k = i;
													while (true) {
														String pnn = propertiesNameSequence[k + 1];
														// Recherche si une propriété à le même nom
														PropertyDescriptor _ppd = findProperty(currentProperty.getClass(), pnn);
														if (_ppd == null)
															throw new BrokerPropertyException(propertyName, "The class: " + currentProperty.getClass() + " of the property: " + currentProperty
																	+ " does not contains the property with the name: " + pn);
														propertiesobjectSequence[k] = currentProperty;
														k++;
														if (k == propertiesGetterHandlerSequence.length) {
															sphc.sph.setMethodHandler(_ppd.getWriteMethod(), currentProperty);
															break;
														}
														GetterPropertyHandler rmh = new GetterPropertyHandler(currentProperty, _ppd.getReadMethod(), null);
														propertiesGetterHandlerSequence[k] = rmh;
														currentProperty = rmh.get();
														if (currentProperty == null)
															throw new BrokerPropertyException(propertyName, "The property: " + propertiesNameSequence[i] + " of the "
																	+ (i == 0 ? "block : " + getName() : "sub bean: " + propertiesobjectSequence[k - 1]) + " is null ");
														if (currentProperty == propertiesobjectSequence[k])
															break;
														System.out.println("");
													}
												}
											}
										} catch (Throwable e) { // Le getter de l'utilisateur a chié
											if (e instanceof BrokerPropertyException)
												throw e;
											e.printStackTrace();
										}
									}
								});
								sphc.sph = sph;
								newPropertyMethodHandles[newPropertyMethodHandles.length - 1] = sph;
								propertyMethodHandles = newPropertyMethodHandles;
							} catch (IllegalAccessException e) {
								e.printStackTrace();
							}
							inputs = Collections.unmodifiableList(_inputs);
							firePropertyStructChange();
							return;// true;
						}
						try {
							GetterPropertyHandler rmh = new GetterPropertyHandler(po, ppd.getReadMethod(), null);
							po = rmh.get();
							if (po == null) {
								success.element = false;
								return;
							}
							propertiesobjectSequence[j] = po;
							propertiesGetterHandlerSequence[j] = rmh;
						} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
							e.printStackTrace();
						}
					}
					success.element = false;
				} catch (Throwable e) {
					e.printStackTrace();
				}
		});
		return success.element;
	}

	private void runTaskLater(Runnable task) {
		if (operator instanceof EvolvedOperator)
			((EvolvedOperator) operator).runLater(task);
		else {
			Consumer<Runnable> _runLaterFunction = runLaterFunction;
			if (_runLaterFunction != null)
				runLaterFunction.accept(task);
			else
				task.run();
		}
	}

	private static PropertyDescriptor findProperty(Class<? extends Object> _class, String propernyName) throws IntrospectionException {
		for (PropertyDescriptor pd : Introspector.getBeanInfo(_class).getPropertyDescriptors()) // Search property...
			if (pd.getWriteMethod() != null && pd.getName().equals(propernyName))
				return pd;
		return null;
	}

	public void setRemoteIp(InetSocketAddress remoteIp) {
		InetSocketAddress oldRemoteIp = this.remoteIp;
		this.remoteIp = remoteIp;
		pcs.firePropertyChange("remoteIp", oldRemoteIp, remoteIp);
	}

	public void setRemoteOperator(RemoteOperator remoteOperator) {
		this.remoteOperator = remoteOperator;
	}

	public void setRemoteProperty(boolean remoteProperty) {
		if (processMode != ProcessMode.REMOTE)
			remoteProperty = false;
		boolean oldRemoteProperty = this.remoteProperty;
		this.remoteProperty = remoteProperty;
		pcs.firePropertyChange("remoteProperty", oldRemoteProperty, remoteProperty);
	}

	public void setRunTimePropertyChangeListener(PropertyChangeListener listener) {
		if (runTimePropertyChangeListener != null)
			removePropertyChangeListener(runTimePropertyChangeListener);
		if (listener != null)
			addPropertyChangeListener(listener);
		runTimePropertyChangeListener = listener;
	}

	public void setSynchronizeProperties(boolean synchronizeProperties) {
		boolean oldSynchronizeProperties = this.synchronizeProperties;
		this.synchronizeProperties = synchronizeProperties;
		pcs.firePropertyChange("synchronizeProperties", oldSynchronizeProperties, synchronizeProperties);
	}

	public void setThread(Thread thread) {
		this.thread = thread;
	}

	public void setTimeOfIssue(long[] toi) {
		this.inputTOI = toi;
	}

	@Override
	public void setVisible() {
		fireSetPropertyVisible(this, "isolatePort", processMode == ProcessMode.ISOLATED);
		fireSetPropertyVisible(this, "synchronizeProperties", processMode != ProcessMode.LOCAL);
		fireSetPropertyVisible(this, "remoteIp", processMode == ProcessMode.REMOTE);
		fireSetPropertyVisible(this, "remoteProperty", processMode == ProcessMode.REMOTE);
	}

	public void setWarning(String warning) {
		if (!Objects.equals(this.warning, warning)) {
			this.warning = warning;
			fireRunningStateChange();
		}
	}

	@Override
	public String toString() {
		return BeanEditor.getBeanDesc(operator).name;
	}

	public void updateBounds() {
		if (rectangle != null)
			rectangle = new Rectangle2D((int) rectangle.getMinX(), (int) rectangle.getMinY(), 100, computeHeight());
	}

	boolean updateDynamicInputs(String[] names, Class<?>[] types) {
		if (names == null || types == null)
			throw new IllegalArgumentException("names and types musts be non null");
		if (names.length != types.length)
			throw new IllegalArgumentException("names and types must have the same size");
		for (int i = 0; i < types.length; i++)
			if (names[i] == null || types[i] == null)
				throw new IllegalArgumentException("All elements in names and types must be non null");
		if (!(operator instanceof EvolvedOperator))
			throw new IllegalArgumentException("This feature is only available for block linked to an EvolvedOperator");
		Unit<Boolean> evolved = new Unit<>(true);
		runTaskLater(() -> {
			LinkedList<BlockInput> checkInputs = new LinkedList<>();
			int start = nbPropertyInput + nbStaticInput;
			int end = start + nbDynamicInput;
			for (int j = start; j < end; j++)
				checkInputs.add(inputs.get(j));
			boolean hasChanged = false;
			ArrayList<Integer> toAdd = new ArrayList<>();
			nextInput: for (int i = 0; i < names.length; i++) {
				String inName = names[i];
				Class<?> inType = types[i];
				for (int j = 0; j < end; j++) {
					BlockInput input = inputs.get(j);
					if (input.getName().equals(inName) && input.getType() == inType) {
						checkInputs.remove(input);
						continue nextInput;
					}
				}
				toAdd.add(i);
			}
			if (checkInputs.isEmpty() && toAdd.isEmpty()) {
				evolved.element = false;
				return;// false;
			}
			ArrayList<BlockInput> _inputs = new ArrayList<>(inputs);
			for (BlockInput blockInput : checkInputs) {
				Link link = blockInput.getLink();
				if (link != null)
					blockInput.setLink(null);
				if (_inputs.remove(blockInput))
					nbDynamicInput--;
			}
			nextInputToAdd: for (Integer i : toAdd) {
				String name = names[i];
				Class<?> type = types[i];
				for (BlockInput input : _inputs)
					if (input.getName().equals(name)) {
						System.err.println("The block: " + this + " already contains the inputs: " + name);
						continue nextInputToAdd;
					}
				_inputs.add(nbPropertyInput + nbStaticInput + nbDynamicInput, new BlockInput(this, type, name, false));
				hasChanged = true;
				nbDynamicInput++;
			}
			evolved.element = hasChanged || !checkInputs.isEmpty();
			if (evolved.element) {
				clearIOPos();
				outputBuff = new Object[outputs.size()];
				outputBuffTs = new long[outputs.size()];
				outputBuffToi = new long[outputs.size()];
				inputs = Collections.unmodifiableList(_inputs);
			}
		});
		return evolved.element;
	}

	// TODO evolve data générateur -> comment ne pas enlever les lien si les entré/sortie sont encore compatible?
	boolean updateDynamicOuputs(String[] names, Class<?>[] types) {
		if (names == null || types == null)
			throw new IllegalArgumentException("names and types must be non null");
		if (names.length != types.length)
			throw new IllegalArgumentException("names and types must have the same size");
		for (int i = 0; i < types.length; i++)
			if (names[i] == null || types[i] == null)
				throw new IllegalArgumentException("All elements in names and types must be non null");
		if (new HashSet<>(Arrays.asList(names)).size() != names.length)
			throw new IllegalArgumentException("All elements in names must be different");
		if (!(operator instanceof EvolvedOperator))
			throw new IllegalArgumentException("This feature is only available for block linked to an EvolvedOperator");
		Unit<Boolean> evolved = new Unit<>(true);
		runTaskLater(() -> {
			LinkedList<BlockOutput> checkOutputs = new LinkedList<>();
			int nbOutput = getNbOutput();
			for (int j = getNbStaticOutput(); j < nbOutput; j++)
				checkOutputs.add(outputs.get(j));
			boolean hasChanged = false;
			ArrayList<Integer> toAdd = new ArrayList<>();
			nextOuput: for (int i = 0; i < names.length; i++) {
				String outName = names[i];
				Class<?> outType = types[i];
				for (int j = 0; j < nbOutput; j++) {
					BlockOutput output = outputs.get(j);
					if (output.getName().equals(outName) && output.getType() == outType) {
						checkOutputs.remove(output);
						continue nextOuput;
					}
				}
				toAdd.add(i);
			}
			if (checkOutputs.isEmpty() && toAdd.isEmpty()) {
				evolved.element = false;
				return;// false;
			}
			ArrayList<BlockOutput> _outputs = new ArrayList<>(outputs);
			for (BlockOutput blockOutput : checkOutputs) {
				newtOutput: while (true) {
					Input[] linkinputs = blockOutput.getLinkInputs();
					if (linkinputs != null)
						for (Input input : linkinputs) {
							input.setLink(null);
							continue newtOutput;
						}
					break;
				}
				_outputs.remove(blockOutput);
			}
			nextOuputToAdd: for (Integer i : toAdd) {
				String name = names[i];
				Class<?> type = types[i];
				for (BlockOutput output : _outputs)
					if (output.getName().equals(name)) {
						System.err.println("The block: " + this + " already contains the outputs: " + name);
						continue nextOuputToAdd;
					}
				_outputs.add(nbStaticOutput + i, new BlockOutput(this, null, type, name));
				hasChanged = true;
			}
			evolved.element = hasChanged || !checkOutputs.isEmpty();
			if (evolved.element) {
				clearIOPos();
				outputBuff = new Object[_outputs.size()];
				outputBuffTs = new long[_outputs.size()];
				outputBuffToi = new long[_outputs.size()];
				outputs = Collections.unmodifiableList(_outputs);
			}
		});
		return evolved.element;
	}

	public void varArgsInputChanged(BlockInput input) {
		runTaskLater(() -> {
			ArrayList<BlockInput> _inputs = new ArrayList<>(inputs);
			int fireVarArgsInputChangeIndexOfInput = -1;
			int fireVarArgsInputChangeTypeOfChange = -1;
			if (input.getLink() == null) {
				int indexOfVarArgs = nbPropertyInput + nbStaticInput + nbDynamicInput;
				if (indexOfVarArgs == _inputs.size() - 1)
					return;
				if (!(operator instanceof EvolvedVarArgsOperator && _inputs.indexOf(input) == _inputs.size() - 1)) {// Je suis sur le dernier d'un drawer
					if (_inputs.get(_inputs.size() - 1).getLink() == null) { // Il faut pas retirer le canaddinput
						_inputs.remove(_inputs.size() - 1);
						nbVarArgsInput--;
						varArgsBuff = (Object[]) Array.newInstance(_inputs.get(_inputs.size() - 1).getType(), nbVarArgsInput);
					}
					for (; indexOfVarArgs < _inputs.size(); indexOfVarArgs++)
						if (_inputs.get(indexOfVarArgs).getLink() == null)
							break;
					BlockInput currInput = input;
					for (; indexOfVarArgs < _inputs.size() - 1; indexOfVarArgs++) {
						currInput = _inputs.get(indexOfVarArgs);
						Link link = _inputs.get(indexOfVarArgs + 1).getLink();
						Link newLink = link == null ? null : new Link(currInput, link.getOutput());
						_inputs.get(indexOfVarArgs).setLink(newLink, false);
					}
					if (indexOfVarArgs < _inputs.size())
						_inputs.get(indexOfVarArgs).setLink(null, false);
					fireVarArgsInputChangeIndexOfInput = _inputs.indexOf(input);
					fireVarArgsInputChangeTypeOfChange = VarArgsInputChangeListener.REMOVE;
					// fireVarArgsInputChange(_inputs.indexOf(input), VarArgsInputChangeListener.REMOVE);
				}
			} else if (_inputs.get(_inputs.size() - 1) == input) {
				nbVarArgsInput = _inputs.size() - nbPropertyInput - nbStaticInput - nbDynamicInput;
				// nbVarArgsInput++; //Ajout du 23/08/17 on ne rajoute une patte que si on peut canAddInput
				if (varArgsBuff.length != nbVarArgsInput)
					varArgsBuff = (Object[]) Array.newInstance(_inputs.get(_inputs.size() - 1).getType(), nbVarArgsInput);
				if (operator instanceof EvolvedVarArgsOperator) {
					Class<?>[] inputsType = new Class[_inputs.size() - nbPropertyInput];
					for (int i = nbPropertyInput; i < _inputs.size(); i++) {
						Link link = _inputs.get(i).getLink();
						inputsType[i - nbPropertyInput] = link == null ? null : link.getOutput().getType();
					}
					if (!((EvolvedVarArgsOperator) operator).canAddInput(inputsType))
						return;
				}
				String inputName = input.getName();
				int indexOf = inputName.lastIndexOf("-");
				int index = Integer.parseInt(inputName.substring(indexOf + 1)) + 1;
				BlockInput newInput = new BlockInput(this, input.getType(), inputName.substring(0, indexOf) + "-" + Integer.toString(index), input.isVarArgs());
				for (InputLinkChangeListener lcl : input.getLinkChangeListener())
					newInput.addLinkChangeListener(lcl);
				_inputs.add(newInput);
				fireVarArgsInputChangeIndexOfInput = _inputs.indexOf(newInput);
				fireVarArgsInputChangeTypeOfChange = VarArgsInputChangeListener.NEW;
				// fireVarArgsInputChange(_inputs.indexOf(newInput), VarArgsInputChangeListener.NEW);
			} else {
				fireVarArgsInputChangeIndexOfInput = _inputs.indexOf(input);
				fireVarArgsInputChangeTypeOfChange = VarArgsInputChangeListener.CHANGED;
				fireVarArgsInputChange(_inputs.indexOf(input), VarArgsInputChangeListener.CHANGED);
				return;
			}
			inputs = Collections.unmodifiableList(_inputs);
			clearIOPos();
			if (fireVarArgsInputChangeIndexOfInput != -1)
				fireVarArgsInputChange(fireVarArgsInputChangeIndexOfInput, fireVarArgsInputChangeTypeOfChange);
		});
	}

	public void addPropertyStructChangeListener(StructChangeListener listener) {
		listeners.add(StructChangeListener.class, listener);
	}

	public void removePropertyStructChangeListener(StructChangeListener listener) {
		listeners.remove(StructChangeListener.class, listener);
	}

	public void firePropertyStructChange() {
		for (StructChangeListener listener : listeners.getListeners(StructChangeListener.class))
			listener.structChanged();
	}

	public void addInputLinksChangeListener(InputLinksChangeListener listener) {
		listeners.add(InputLinksChangeListener.class, listener);
	}

	public void removeInputLinksChangeListener(InputLinksChangeListener listener) {
		listeners.remove(InputLinksChangeListener.class, listener);
	}

	void fireInputLinksChangeListener(Input input) {
		inputs.indexOf(input);
		for (InputLinksChangeListener listener : listeners.getListeners(InputLinksChangeListener.class))
			listener.inputLinkChanged(inputs.indexOf(input));
	}

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		pcs.addPropertyChangeListener(listener);
	}

	public void addPropertyChangeListenerIfNotPresent(PropertyChangeListener listener) {
		for (PropertyChangeListener list : pcs.getPropertyChangeListeners())
			if (listener == list)
				return;
		addPropertyChangeListener(listener);
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		pcs.removePropertyChangeListener(listener);
	}

	public void addRunningStateChangeListener(RunningStateChangeListener listener) {
		listeners.add(RunningStateChangeListener.class, listener);
	}

	public void removeRunningStateChangeListener(RunningStateChangeListener listener) {
		listeners.remove(RunningStateChangeListener.class, listener);
	}

	public void fireRunningStateChange() {
		for (RunningStateChangeListener listener : listeners.getListeners(RunningStateChangeListener.class))
			listener.stateChange();
	}

	public void addVarArgsInputChangeListener(VarArgsInputChangeListener listener) {
		listeners.add(VarArgsInputChangeListener.class, listener);
	}

	public void removeVarArgsInputChangeListener(VarArgsInputChangeListener listener) {
		listeners.remove(VarArgsInputChangeListener.class, listener);
	}

	private void fireVarArgsInputChange(int indexOfInput, int typeOfChange) {
		updateBounds();
		for (VarArgsInputChangeListener listener : listeners.getListeners(VarArgsInputChangeListener.class))
			listener.varArgsInputChanged(indexOfInput, typeOfChange);
	}

	public static Rectangle2D getBlockMinimumBounds(Point2D location, boolean isMiddlePoint) {
		int width = 100;
		int height = 120;
		return new Rectangle2D(isMiddlePoint ? location.getX() - width / 2 : location.getX(), isMiddlePoint ? location.getY() - height / 2 : location.getY(), width, height);
	}

	public void setInputError(int ip, String message) {
		inputs.get(ip).setError(message);
	}

	public void setRunLaterFunction(Consumer<Runnable> runLaterFunction) {
		this.runLaterFunction = runLaterFunction;
	}

	public void dispose() {
		BeanEditor.removeStrongRefBeanRenameListener(brl);
	}
}
