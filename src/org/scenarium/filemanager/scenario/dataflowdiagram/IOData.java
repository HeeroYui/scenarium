/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario.dataflowdiagram;

public class IOData {
	private long ts;
	private long toi;
	private Object value;

	public long getToi() {
		return toi;
	}

	public long getTs() {
		return ts;
	}

	public Object getValue() {
		return value;
	}

	public void setToi(long toi) {
		this.toi = toi;
	}

	public void setTs(long ts) {
		this.ts = ts;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "ts: " + ts + " toi: " + toi + " value: " + value;
	}
}