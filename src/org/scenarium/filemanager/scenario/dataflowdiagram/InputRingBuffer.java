/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario.dataflowdiagram;

import java.util.Arrays;

public class InputRingBuffer {
	public static void main(String[] args) {
		InputRingBuffer cb = new InputRingBuffer(3, false);
		cb.push(2, 2, Integer.valueOf(5));
		cb.push(4, 2, Integer.valueOf(8));
		cb.push(6, 2, Integer.valueOf(10));
		cb.push(8, 2, Integer.valueOf(15));
		cb.push(9, 2, Integer.valueOf(19));
		System.out.println(cb.popFirst());
		System.out.println(cb.popFirst());
		System.out.println(cb.popFirst());
		System.out.println(cb.popFirst());
		System.out.println(cb.popFirst());
		cb.push(17, 2, Integer.valueOf(17));
		System.out.println(cb.popLast());
		System.out.println(cb.popFirst());
		System.out.println(cb.popLast());
		System.out.println(cb.popFirst());
		cb.push(17, 2, Integer.valueOf(19));
		System.out.println(cb.popLast());
		System.out.println(cb.isEmpty());
	}

	private int maxSize;
	private boolean eraseDataIfFull;
	private int front = 0;
	private int rear = 0;
	private int bufLen = 0;
	private final Object[] datas;
	private final long[] ts;
	private final long[] toi;
	private final IOData ioData = new IOData();

	private long nbMissedData;

	public InputRingBuffer() {
		this(10, false);
	}

	public InputRingBuffer(int maxSize, boolean eraseDataIfFull) {
		front = 0;
		rear = 0;
		bufLen = 0;
		nbMissedData = 0;
		datas = new Object[maxSize];
		this.maxSize = maxSize;
		this.eraseDataIfFull = eraseDataIfFull;
		ts = new long[maxSize];
		toi = new long[maxSize];
	}

	public InputRingBuffer(int maxStackSize, boolean eraseDataIfFull, InputRingBuffer buffer, Class<?> type) {
		front = 0;
		rear = 0;
		bufLen = 0;
		nbMissedData = 0;
		this.maxSize = maxStackSize;
		this.eraseDataIfFull = eraseDataIfFull;
		datas = new Object[maxSize];
		ts = new long[maxSize];
		toi = new long[maxSize];
		if (buffer != null) {
			IOData data;
			while ((data = buffer.popFirst()) != null)
				push(data.getTs(), data.getToi(), data.getValue());
		}
	}

	public void clear() {
		front = 0;
		rear = 0;
		bufLen = 0;
		nbMissedData = 0;
		Arrays.fill(datas, null);
	}

	public int getMaxSize() {
		return maxSize;
	}

	public long getNbMissedData() {
		return nbMissedData;
	}

	public boolean isEmpty() {
		return bufLen == 0;
	}

	public boolean isFull() {
		return bufLen == maxSize;
	}

	public IOData popFirst() {
		if (bufLen == 0)
			return null;
		ioData.setValue(datas[front]);
		datas[front] = null;
		ioData.setTs(ts[front]);
		ioData.setToi(toi[front]);
		front += 1;
		if (front == maxSize)
			front = 0;
		bufLen--;
		return ioData;
	}

	public IOData popLast() {
		if (bufLen == 0)
			return null;
		bufLen--;
		rear = rear == 0 ? maxSize - 1 : rear - 1;
		ioData.setValue(datas[rear]);
		datas[rear] = null;
		ioData.setTs(ts[rear]);
		ioData.setToi(toi[rear]);
		return ioData;
	}

	public boolean push(long ts, long toi, Object data) {
		if (isFull()) {
			nbMissedData++;
			if (eraseDataIfFull) {
				datas[front] = null;
				front += 1;
				if (front == maxSize)
					front = 0;
				datas[rear] = data;
				this.ts[rear] = ts;
				this.toi[rear] = toi;
				rear += 1;
				if (rear == maxSize)
					rear = 0;
			}
			return true;
		}
		bufLen++;
		datas[rear] = data;
		this.ts[rear] = ts;
		this.toi[rear] = toi;
		rear += 1;
		if (rear == maxSize)
			rear = 0;
		return false;
	}

	public boolean setEraseDataIfFull() {
		return eraseDataIfFull;
	}

	public int size() {
		return bufLen;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < datas.length; i++)
			sb.append(datas[i] + " ");
		return sb.toString();
	}
}
