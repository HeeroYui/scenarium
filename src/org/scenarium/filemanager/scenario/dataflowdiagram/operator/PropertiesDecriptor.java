/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario.dataflowdiagram.operator;

import java.io.File;

public class PropertiesDecriptor {
	public static final int UNKNOW = -1;
	public static final int BOOLEAN = 0;
	public static final int INTEGER = 1;
	public static final int DOUBLE = 2;
	public static final int STRING = 3;
	public static final int FOLDER = 4;
	public static final int FILE = 5;

	public static int getType(Class<?> c) {
		if (c == Boolean.class || c == boolean.class)
			return PropertiesDecriptor.BOOLEAN;
		else if (c == Double.class || c == double.class)
			return PropertiesDecriptor.DOUBLE;
		else if (c == File.class)
			return PropertiesDecriptor.FILE;
		else if (c == File.class)
			return PropertiesDecriptor.FOLDER;
		else if (c == Integer.class || c == int.class)
			return PropertiesDecriptor.INTEGER;
		else if (c == String.class)
			return PropertiesDecriptor.STRING;
		return PropertiesDecriptor.UNKNOW;
	}

	public static int getType(String type) {
		switch (type) {
		case "%b":
			return BOOLEAN;
		case "%i":
			return INTEGER;
		case "%d":
			return DOUBLE;
		case "%s":
			return STRING;
		case "%folder":
			return FOLDER;
		case "%file":
			return FILE;
		default:
			return -1;
		}
	}

	public final int type;

	public final String name;

	public PropertiesDecriptor(final int type, final String name) {
		this.type = type;
		this.name = name;
	}

	public String getStringType() {
		switch (type) {
		case BOOLEAN:
			return "Boolean";
		case INTEGER:
			return "Integer";
		case DOUBLE:
			return "Double";
		case STRING:
			return "String";
		case FOLDER:
			return "Folder";
		case FILE:
			return "File";
		default:
			return "Unknow";
		}
	}

	@Override
	public String toString() {
		return getStringType() + " " + name;
	}
}
