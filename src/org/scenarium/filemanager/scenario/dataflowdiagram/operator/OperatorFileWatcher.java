/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario.dataflowdiagram.operator;

import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_DELETE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;

import java.io.IOException;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.HashMap;

public class OperatorFileWatcher {
	class OperatorWeakReference extends WeakReference<Object> {
		public OperatorWeakReference(Object operator) {
			super(operator);
		}

		@Override
		public boolean equals(Object obj) {
			if (obj instanceof Reference)
				return get().equals(((Reference<?>) obj).get());
			return get().equals(obj);
		}

		@Override
		public int hashCode() {
			if (get() == null)
				return 0;
			return get().hashCode();
		}
	}

	private static OperatorFileWatcher instance;

	public static OperatorFileWatcher getInstance() {
		if (instance == null)
			instance = new OperatorFileWatcher();
		return instance;
	}

	private HashMap<WeakReference<Object>, HashMap<String, Path>> operatorManagerFiles;

	private Thread synchroThread;

	private OperatorFileWatcher() {
		operatorManagerFiles = new HashMap<>();
	}

	public void addWatcher(Object operator, String propertyName, final Path synchonizedFile) {
		WeakReference<Object> op = new OperatorWeakReference(operator) {};
		if (operatorManagerFiles.get(op) == null)
			operatorManagerFiles.put(op, new HashMap<String, Path>());
		operatorManagerFiles.get(op).put(propertyName, synchonizedFile);
		updateSynchro();
	}

	public void removeWatcher(Object operator, String propertyName, Path synchonizedFile) {
		WeakReference<Object> op = new OperatorWeakReference(operator);
		if (operatorManagerFiles != null || operatorManagerFiles.get(op) == null || operatorManagerFiles.get(operator).get(propertyName) == null)
			return;
		operatorManagerFiles.get(op).remove(propertyName);
		if (operatorManagerFiles.get(op).isEmpty())
			operatorManagerFiles.remove(op);
		if (operatorManagerFiles.isEmpty()) {
			synchroThread.interrupt();
			synchroThread = null;
			instance = null;
			return;
		}
		updateSynchro();
	}

	private void updateSynchro() {
		if (synchroThread != null)
			synchroThread.interrupt();
		synchroThread = new Thread(new Runnable() {
			private long timer;

			@Override
			public void run() {
				final FileSystem fileSystem = FileSystems.getDefault();
				try (final WatchService watchService = fileSystem.newWatchService()) {
					final HashMap<WatchKey, Path> keyMap = new HashMap<>();
					for (HashMap<String, Path> operatorFiles : operatorManagerFiles.values())
						for (Path path : operatorFiles.values()) {
							if (path.getParent() == null)
								continue;
							keyMap.put(path.getParent().register(watchService, ENTRY_CREATE, ENTRY_MODIFY, ENTRY_DELETE), path.getParent());
						}
					WatchKey watchKey;
					timer = System.currentTimeMillis();
					do {
						watchKey = watchService.take();
						final Path eventDir = keyMap.get(watchKey);
						for (final WatchEvent<?> event : watchKey.pollEvents())
							if (System.currentTimeMillis() - timer > 100) {
								Thread.sleep(100);
								timer = System.currentTimeMillis();
								Path eventPath = eventDir.resolve((Path) event.context());
								operatorManagerFiles.forEach((operator, operatorFiles) -> {
									for (String propertyName : operatorFiles.keySet())
										if (operatorFiles.get(propertyName).equals(eventPath) && operator.get() != null)
											;
									// TODO faire la chose autrement
									// try {
									//
									// operator.get().firePropertyChanged(propertyName, event.kind() + " " + eventPath.toFile().getAbsolutePath());
									// } catch (NativeOperatorAccessException e) {
									// }
								});
							}
					} while (watchKey.reset());
				} catch (InterruptedException e) {} catch (IOException e) {}
			}
		});
		synchroThread.start();
	}
}
