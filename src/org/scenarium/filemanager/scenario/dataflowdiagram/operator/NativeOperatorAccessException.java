/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario.dataflowdiagram.operator;

public class NativeOperatorAccessException extends Exception {
	private static final long serialVersionUID = 1L;
	private String operatorPath;
	private String methodeName;

	public NativeOperatorAccessException(String operatorPath, String methodeName) {
		this.operatorPath = operatorPath.substring(0, operatorPath.lastIndexOf("."));
		this.methodeName = methodeName;
	}

	@Override
	public String getMessage() {
		return "Cannot access to " + methodeName + " method from operator: " + operatorPath;
	}
}
