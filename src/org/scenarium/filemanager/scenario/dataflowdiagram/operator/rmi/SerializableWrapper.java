/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario.dataflowdiagram.operator.rmi;

import java.awt.image.BufferedImage;
import java.io.Externalizable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;

import org.beanmanager.BeanManager;
import org.beanmanager.editors.PropertyEditorManager;
import org.beanmanager.editors.container.ArrayEditor;
import org.scenarium.communication.can.CanTrame;
import org.scenarium.editors.can.CanTrameEditor;
import org.scenarium.filemanager.datastream.DataStreamManager;
import org.scenarium.filemanager.datastream.input.DataFlowInputStream;
import org.scenarium.filemanager.datastream.output.DataFlowOutputStream;

public class SerializableWrapper implements Externalizable {
	private static final long serialVersionUID = 1L;
	private static final HashMap<String, Class<?>> classMap = new HashMap<>();
	private Class<?> type;
	private DataFlowOutputStream<?> dfos;
	private DataFlowInputStream<?> dfis;
	private Object value;

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		Object[] data = new Object[3];
		data[0] = new BufferedImage[] { null, null/* new BufferedImage(10, 10, BufferedImage.TYPE_3BYTE_BGR), new BufferedImage(10, 10, BufferedImage.TYPE_3BYTE_BGR) */ };
		data[1] = new BufferedImage[0];
		data[2] = new BufferedImage[][] { new BufferedImage[] { new BufferedImage(10, 10, BufferedImage.TYPE_3BYTE_BGR), new BufferedImage(10, 10, BufferedImage.TYPE_3BYTE_BGR) }, new BufferedImage[0], new BufferedImage[] { null, null } };
		int serializedData = serialize(data, new ArrayList<SerializableWrapper>());
		try(FileOutputStream fos = new FileOutputStream(new File("Externalizable.txt"))){
			new ObjectOutputStream(fos).writeObject(data);
		}
		try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(new File("Externalizable.txt")))){
			System.out.println(ois.readObject());
		}
		System.out.println(serializedData);
		deserialize(data);

		PropertyEditorManager.registerEditor(CanTrame.class, CanTrameEditor.class);
		SerializableWrapper sw = new SerializableWrapper(new CanTrame(new byte[8]));
		sw.setValue(new CanTrame(new byte[3]));
		try(FileOutputStream fos = new FileOutputStream(new File("Externalizable.txt"))) {
			new ObjectOutputStream(fos).writeObject(sw);
		}
		try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(new File("Externalizable.txt")))){
			System.out.println(ois.readObject());
		}
		
	}

	public SerializableWrapper() {}

	public SerializableWrapper(Object object) {
		setValue(object);
	}

	public Object getValue() {
		return value;
	}

	// avant synchronized
	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		String className = in.readUTF();
		Class<?> _type = classMap.get(className);
		if (_type == null) {
			_type = BeanManager.getClassFromDescriptor(className);
			classMap.put(className, _type);
		}
		if (_type != type) {
			type = _type;
			dfis = DataStreamManager.getInputStream(type);
			if (dfis == null)
				throw new IllegalArgumentException("Cannot read: " + type + " type, no input stream associated with this kind of data");
		}
		dfis.setDataInput(in);
		value = dfis.pop();
	}

	public void setValue(Object value) {
		this.value = value;
		if (value.getClass() != type) {
			type = value.getClass();
			dfos = null;
		}
	}

	@Override
	public String toString() {
		return "Wrapper on: " + (value == null ? null : value.toString());
	}

	// avant synchronized
	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		if (dfos == null) {
			dfos = DataStreamManager.getOutputStream(type); // Eviter de toujours faire des streams....
			if (dfos == null)
				throw new IllegalArgumentException("Cannot write: " + value.getClass() + " type, no output stream associated with this kind of data");
		}
		dfos.setDataOutput(out);
		out.writeUTF(BeanManager.getDescriptorFromClass(value.getClass()));
		dfos.pushObject(value);
	}

	static void deserialize(Object[] datas) {
		for (int i = 0; i < datas.length; i++) {
			Object data = datas[i];
			if (data != null) {
				Class<?> type;
				if (data instanceof SerializableWrapper)
					datas[i] = ((SerializableWrapper) data).getValue();
				else if ((type = data.getClass()).isArray() && !Serializable.class.isAssignableFrom(type.getComponentType()) && !type.getComponentType().isPrimitive()) {
					if (((Object[]) data).length != 0 && ArrayEditor.getComponentType(data.getClass()) == Object.class)
						datas[i] = deserializeArray((Object[]) data);
				}
			}
		}
	}

	private static Object[] deserializeArray(Object[] datas) {
		int level = 0;
		Object subDataType = datas;
		while (subDataType.getClass().isArray()) {
			if (((Object[]) subDataType).length == 0)
				break;
			level++;
			subDataType = ((Object[]) subDataType)[0];
		}
		if (subDataType instanceof SerializableWrapper)
			subDataType = ((SerializableWrapper) subDataType).getValue();
		level--;
		Class<?> type = subDataType.getClass();
		for (int i = 0; i < level; i++)
			type = type.arrayType();
		Object[] newArray = (Object[]) Array.newInstance(type, datas.length);
		for (int i = 0; i < datas.length; i++) {
			Object data = datas[i];
			if (data != null)
				newArray[i] = data instanceof SerializableWrapper ? ((SerializableWrapper) data).getValue()
						: data.getClass().isArray() && !Serializable.class.isAssignableFrom(data.getClass().getComponentType()) && !data.getClass().getComponentType().isPrimitive()
								? (((Object[]) data).length == 0 || ArrayEditor.getComponentType(data.getClass()) != Object.class) ? datas[i] : deserializeArray((Object[]) data)
								: datas[i];
		}
		return newArray;
	}

	private static SerializableWrapper getSerializableWrapper(Object value, ArrayList<SerializableWrapper> serializableWrapperPool, int indexInPool) {
		if (!DataStreamManager.hasOutputStream(value.getClass()))
			return null;
		if (indexInPool == serializableWrapperPool.size()) {
			SerializableWrapper sw = new SerializableWrapper(value);
			serializableWrapperPool.add(sw);
			indexInPool++;
			return sw;
		}
		SerializableWrapper sw = serializableWrapperPool.get(indexInPool);
		sw.setValue(value);
		indexInPool++;
		return sw;
	}

	static Object serialize(Object data, ArrayList<SerializableWrapper> serializableWrapperPool) {
		int indexInPool = 0;
		if (data != null)
			if (!(data instanceof Serializable)) {
				Object serializableWrapper = getSerializableWrapper(data, serializableWrapperPool, indexInPool);
				if (serializableWrapper != null)
					return serializableWrapper;
				throw new IllegalArgumentException("The data type: " + data.getClass() + " is not streamable");
			} else if (data.getClass().isArray() && !Serializable.class.isAssignableFrom(data.getClass().getComponentType()) && !data.getClass().getComponentType().isPrimitive()) {
				Object[] dataArray = (Object[]) data;
				Object[] outputArray = new Object[dataArray.length];
				indexInPool += serializeArray(dataArray, outputArray, serializableWrapperPool, indexInPool);
			}
		System.err.println("null return for data: " + data);
		return null;
	}

	static int serialize(Object[] datas, ArrayList<SerializableWrapper> serializableWrapperPool) {
		return serialize(datas, serializableWrapperPool, 0);
	}

	static int serialize(Object[] datas, ArrayList<SerializableWrapper> serializableWrapperPool, int indexInPool) {
		for (int i = 0; i < datas.length; i++) {
			Object data = datas[i];
			if (data != null) {
				Class<?> type;
				if (!(data instanceof Serializable)) {
					Object serializableWrapper = getSerializableWrapper(data, serializableWrapperPool, indexInPool);
					if (serializableWrapper != null)
						indexInPool++;
					else
						throw new IllegalArgumentException("The data type: " + data.getClass() + " is not streamable");
					datas[i] = serializableWrapper;
				} else if (data.getClass().isArray() && !Serializable.class.isAssignableFrom(type = ArrayEditor.getComponentType(data.getClass())) && !type.isPrimitive()) {
					Object[] dataArray = (Object[]) data;
					if (dataArray.length != 0) {
						Object[] outputArray = new Object[dataArray.length];
						int oldIndexInPool = indexInPool;
						indexInPool = serializeArray(dataArray, outputArray, serializableWrapperPool, indexInPool);
						if (oldIndexInPool != indexInPool)
							datas[i] = outputArray;
					}
				}
			}
		}
		return indexInPool;
	}

	static int serializeArray(Object[] datas, Object[] outputArray, ArrayList<SerializableWrapper> serializableWrapperPool, int indexInPool) {
		for (int i = 0; i < datas.length; i++) {
			Object data = datas[i];
			if (data != null)
				if (!(data instanceof Serializable)) {
					Object serializableWrapper = getSerializableWrapper(data, serializableWrapperPool, indexInPool);
					if (serializableWrapper != null)
						indexInPool++;
					else
						throw new IllegalArgumentException("The data type: " + data.getClass() + " is not streamable");
					outputArray[i] = serializableWrapper;
				} else if (data.getClass().isArray() && !Serializable.class.isAssignableFrom(ArrayEditor.getComponentType(data.getClass())) && !ArrayEditor.getComponentType(data.getClass()).isPrimitive()) {
					Object[] dataSubArray = (Object[]) data;
					if (dataSubArray.length != 0) {
						Object[] outputSubArray = new Object[dataSubArray.length];
						int oldIndexInPool = indexInPool;
						indexInPool = serializeArray(dataSubArray, outputSubArray, serializableWrapperPool, indexInPool);
						if (oldIndexInPool != indexInPool) {
							outputArray[i] = outputSubArray;
							continue;
						}
					}
					outputArray[i] = data;
				}
		}
		return indexInPool;
	}
}
