/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario.dataflowdiagram.operator.rmi;

import java.rmi.RemoteException;
import java.util.HashMap;

import org.beanmanager.BeanRenameListener;
import org.beanmanager.rmi.server.RMIBeanCallBack;
import org.scenarium.filemanager.scenario.dataflowdiagram.Block;
import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;
import org.scenarium.filemanager.scenario.dataflowdiagram.InputLinksChangeListener;
import org.scenarium.filemanager.scenario.dataflowdiagram.VarArgsInputChangeListener;
import org.scenarium.filemanager.scenario.dataflowdiagram.operator.DeclaredInputChangeListener;
import org.scenarium.filemanager.scenario.dataflowdiagram.operator.DeclaredOutputChangeListener;
import org.scenarium.filemanager.scenariomanager.StructChangeListener;

public class RMIOperatorCallBack extends RMIBeanCallBack implements RMIOperatorCallBackImpl {
	private static final long serialVersionUID = 5L;
	private Block block;
	private HashMap<Integer, BeanRenameListener> beanRenameListenersMap;
	private HashMap<Integer, StructChangeListener> structChangeListenersMap;
	private HashMap<Integer, VarArgsInputChangeListener> varArgsInputChangeListenersMap;
	private HashMap<Integer, InputLinksChangeListener> inputLinksChangeListenersMap;
	private HashMap<Integer, DeclaredInputChangeListener> declaredInputChangeListenersMap;
	private HashMap<Integer, DeclaredOutputChangeListener> declaredOutputChangeListenersMap;
	// private ArrayList<SerializableWrapper> serializableWrapperPool = new ArrayList<>();

	public RMIOperatorCallBack(Block block) throws RemoteException {
		super();
		this.block = block;
	}

	@Override
	public void addBlockNameChangeListener(int listenerId) throws RemoteException {
		BeanRenameListener brl = (oldBeanDesc, beanDesc) -> {
			try {
				RemoteOperator remoteOp = block.getRemoteOperator();
				if (remoteOp != null)
					remoteOp.fireBlockNameChanged(beanDesc.name, beanDesc.local, oldBeanDesc.name, listenerId);
			} catch (Throwable e) {
				e.printStackTrace();
			}
		};
		if (beanRenameListenersMap == null)
			beanRenameListenersMap = new HashMap<>();
		beanRenameListenersMap.put(listenerId, brl);
		((EvolvedOperator) block.operator).addBlockNameChangeListener(brl);
	}

	@Override
	public void addDeclaredInputChangeListener(int listenerId) throws RemoteException {
		DeclaredInputChangeListener dicl = (names, types) -> {
			try {
				RemoteOperator remoteOp = block.getRemoteOperator();
				if (remoteOp != null)
					remoteOp.fireDeclaredInputChanged(names, types);
			} catch (Throwable e) {
				e.printStackTrace();
			}
		};
		if (declaredInputChangeListenersMap == null)
			declaredInputChangeListenersMap = new HashMap<>();
		declaredInputChangeListenersMap.put(listenerId, dicl);
		((EvolvedOperator) block.operator).addDeclaredInputChangeListener(dicl);
	}

	@Override
	public void addDeclaredOutputChangeListener(int listenerId) throws RemoteException {
		DeclaredOutputChangeListener dicl = (names, types) -> {
			try {
				RemoteOperator remoteOp = block.getRemoteOperator();
				if (remoteOp != null)
					remoteOp.fireDeclaredOutputChanged(names, types);
			} catch (Throwable e) {
				e.printStackTrace();
			}
		};
		if (declaredOutputChangeListenersMap == null)
			declaredOutputChangeListenersMap = new HashMap<>();
		declaredOutputChangeListenersMap.put(listenerId, dicl);
		((EvolvedOperator) block.operator).addDeclaredOutputChangeListener(dicl);
	}

	@Override
	public void addInputLinksChangeListener(int listenerId) throws RemoteException {
		InputLinksChangeListener ilcl = indexOfInput -> {
			try {
				RemoteOperator remoteOp = block.getRemoteOperator();
				if (remoteOp != null)
					remoteOp.fireInputLinksChangeChanged(indexOfInput);
			} catch (Throwable e) {
				e.printStackTrace();
			}
		};
		if (inputLinksChangeListenersMap == null)
			inputLinksChangeListenersMap = new HashMap<>();
		inputLinksChangeListenersMap.put(listenerId, ilcl);
		((EvolvedOperator) block.operator).addInputLinksChangeListener(ilcl);
	}

	@Override
	public void addStructChangeListener(int listenerId) throws RemoteException {
		StructChangeListener scl = () -> {
			try {
				RemoteOperator remoteOp = block.getRemoteOperator();
				if (remoteOp != null)
					remoteOp.fireStructChanged();
			} catch (Throwable e) {
				e.printStackTrace();
			}
		};
		if (structChangeListenersMap == null)
			structChangeListenersMap = new HashMap<>();
		structChangeListenersMap.put(listenerId, scl);
		((EvolvedOperator) block.operator).addStructChangeListener(scl);
	}

	@Override
	public void addVarArgsInputChangeListener(int listenerId) throws RemoteException {
		VarArgsInputChangeListener vaicl = (indexOfInput, typeOfChange) -> {
			try {
				RemoteOperator remoteOp = block.getRemoteOperator();
				if (remoteOp != null)
					remoteOp.fireVarArgsInputChanged(indexOfInput, typeOfChange);
			} catch (Throwable e) {
				e.printStackTrace();
			}
		};
		if (varArgsInputChangeListenersMap == null)
			varArgsInputChangeListenersMap = new HashMap<>();
		varArgsInputChangeListenersMap.put(listenerId, vaicl);
		((EvolvedOperator) block.operator).addVarArgsInputChangeListener(vaicl);
	}

	@Override
	public String getBlockName() throws RemoteException {
		return ((EvolvedOperator) block.operator).getBlockName();
	}

	@Override
	public int getInputIndex(String inputName) throws RemoteException {
		return ((EvolvedOperator) block.operator).getInputIndex(inputName);
	}

	@Override
	public String[] getInputsName() throws RemoteException {
		return ((EvolvedOperator) block.operator).getInputsName();
	}

	@Override
	public long getMaxTimeStamp() throws RemoteException {
		return ((EvolvedOperator) block.operator).getMaxTimeStamp();
	}

	@Override
	public int getNbInput() throws RemoteException {
		return ((EvolvedOperator) block.operator).getNbInput();
	}

	@Override
	public int getNbOutput() throws RemoteException {
		return ((EvolvedOperator) block.operator).getNbOutput();
	}

	@Override
	public int getOutputIndex(String outputName) throws RemoteException {
		return ((EvolvedOperator) block.operator).getOutputIndex(outputName);
	}

	@Override
	public String[] getOutputLinkToInputName() throws RemoteException {
		return ((EvolvedOperator) block.operator).getOutputLinkToInputName();
	}

	@Override
	public Class<?>[] getOutputLinkToInputType() throws RemoteException {
		return ((EvolvedOperator) block.operator).getOutputLinkToInputType();
	}

	@Override
	public String[] getOutputsName() throws RemoteException {
		return ((EvolvedOperator) block.operator).getOutputsName();
	}

	@Override
	public long getTimeOfIssue(int indexOfInput) throws RemoteException {
		return ((EvolvedOperator) block.operator).getTimeOfIssue(indexOfInput);
	}

	@Override
	public long getTimeStamp(int indexOfInput) throws RemoteException {
		return ((EvolvedOperator) block.operator).getTimeStamp(indexOfInput);
	}

	@Override
	public String getWarning() throws RemoteException {
		return ((EvolvedOperator) block.operator).getWarning();
	}

	@Override
	public boolean isDefaulting() throws RemoteException {
		return ((EvolvedOperator) block.operator).isDefaulting();
	}

	@Override
	public boolean isPropertyAsInput(String propertyName) throws RemoteException {
		return ((EvolvedOperator) block.operator).isPropertyAsInput(propertyName);
	}

	@Override
	public void onStart(long callingThreadId, int taskId) throws RemoteException {
		updateCallingThread(callingThreadId);
		((EvolvedOperator) block.operator).onStart(() -> {
			try {
				RemoteOperator remoteOp = block.getRemoteOperator();
				if (remoteOp != null)
					remoteOp.onStartCallBack(taskId);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		});
	}
	
	@Override
	public void onResume(long callingThreadId, int taskId) throws RemoteException {
		updateCallingThread(callingThreadId);
		((EvolvedOperator) block.operator).onResume(() -> {
			try {
				RemoteOperator remoteOp = block.getRemoteOperator();
				if (remoteOp != null)
					remoteOp.onResumeCallBack(taskId);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		});
	}
	
	@Override
	public void onPause(long callingThreadId, int taskId) throws RemoteException {
		updateCallingThread(callingThreadId);
		((EvolvedOperator) block.operator).onPause(() -> {
			try {
				RemoteOperator remoteOp = block.getRemoteOperator();
				if (remoteOp != null)
					remoteOp.onPauseCallBack(taskId);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		});
	}
	
	@Override
	public void onStop(long callingThreadId, int taskId) throws RemoteException {
		updateCallingThread(callingThreadId);
		((EvolvedOperator) block.operator).onStop(() -> {
			try {
				RemoteOperator remoteOp = block.getRemoteOperator();
				if (remoteOp != null)
					remoteOp.onStopCallBack(taskId);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		});
	}

	@Override
	public void removeBlockNameChangeListener(int listenerId) throws RemoteException {
		if (beanRenameListenersMap == null)
			return;
		BeanRenameListener brl = beanRenameListenersMap.get(listenerId);
		if (brl == null)
			return;
		((EvolvedOperator) block.operator).removeBlockNameChangeListener(brl);
	}

	@Override
	public void removeDeclaredInputChangeListener(int listenerId) throws RemoteException {
		if (declaredInputChangeListenersMap == null)
			return;
		DeclaredInputChangeListener dicl = declaredInputChangeListenersMap.get(listenerId);
		if (dicl == null)
			return;
		((EvolvedOperator) block.operator).removeDeclaredInputChangeListener(dicl);
	}

	@Override
	public void removeDeclaredOutputChangeListener(int listenerId) throws RemoteException {
		if (declaredOutputChangeListenersMap == null)
			return;
		DeclaredOutputChangeListener docl = declaredOutputChangeListenersMap.get(listenerId);
		if (docl == null)
			return;
		((EvolvedOperator) block.operator).removeDeclaredOutputChangeListener(docl);
	}

	@Override
	public void removeInputLinksChangeListener(int listenerId) throws RemoteException {
		if (inputLinksChangeListenersMap == null)
			return;
		InputLinksChangeListener ilcl = inputLinksChangeListenersMap.get(listenerId);
		if (ilcl == null)
			return;
		((EvolvedOperator) block.operator).removeInputLinksChangeListener(ilcl);
	}

	@Override
	public void removeStructChangeListener(int listenerId) throws RemoteException {
		if (structChangeListenersMap == null)
			return;
		StructChangeListener scl = structChangeListenersMap.get(listenerId);
		if (scl == null)
			return;
		((EvolvedOperator) block.operator).removeStructChangeListener(scl);
	}

	@Override
	public void removeVarArgsInputChangeListener(int listenerId) throws RemoteException {
		if (varArgsInputChangeListenersMap == null)
			return;
		VarArgsInputChangeListener vaicl = varArgsInputChangeListenersMap.get(listenerId);
		if (vaicl == null)
			return;
		((EvolvedOperator) block.operator).removeVarArgsInputChangeListener(vaicl);
	}

	@Override
	public void runLater(long callingThreadId, int taskId) throws RemoteException {
		updateCallingThread(callingThreadId);
		((EvolvedOperator) block.operator).runLater(() -> {
			try {
				RemoteOperator remoteOp = block.getRemoteOperator();
				if (remoteOp != null)
					remoteOp.runLaterCallBack(taskId);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		});
	}

	@Override
	public void setDefaulting(boolean defaulting) throws RemoteException {
		((EvolvedOperator) block.operator).setDefaulting(defaulting);
	}

	@Override
	public boolean setPropertyAsInput(long callingThreadId, String propertyName, boolean asInput) throws RemoteException {
		updateCallingThread(callingThreadId);
		return ((EvolvedOperator) block.operator).setPropertyAsInput(propertyName, asInput);
	}

	@Override
	public void setWarning(String warning) throws RemoteException {
		((EvolvedOperator) block.operator).setWarning(warning);
	}

	@Override
	public boolean triggerOutput(long callingThreadId, Object outputValue) throws RemoteException {
		updateCallingThread(callingThreadId);
		return ((EvolvedOperator) block.operator).triggerOutput(outputValue instanceof SerializableWrapper ? ((SerializableWrapper) outputValue).getValue() : outputValue);
	}

	@Override
	public boolean triggerOutput(long callingThreadId, Object outputValue, long timeStamp) throws RemoteException {
		updateCallingThread(callingThreadId);
		return ((EvolvedOperator) block.operator).triggerOutput(outputValue instanceof SerializableWrapper ? ((SerializableWrapper) outputValue).getValue() : outputValue, timeStamp);
	}

	@Override
	public boolean triggerOutput(long callingThreadId, Object[] outputValues) throws RemoteException {
		updateCallingThread(callingThreadId);
		SerializableWrapper.deserialize(outputValues);
		return ((EvolvedOperator) block.operator).triggerOutput(outputValues);
	}

	@Override
	public boolean triggerOutput(long callingThreadId, Object[] outputValues, long timeStamp) throws RemoteException {
		updateCallingThread(callingThreadId);
		SerializableWrapper.deserialize(outputValues);
		return ((EvolvedOperator) block.operator).triggerOutput(outputValues, timeStamp);
	}

	@Override
	public boolean triggerOutput(long callingThreadId, Object[] outputValues, long[] timeStamps) throws RemoteException, IllegalArgumentException {
		updateCallingThread(callingThreadId);
		SerializableWrapper.deserialize(outputValues);
		return ((EvolvedOperator) block.operator).triggerOutput(outputValues, timeStamps);
	}

	// @Override
	// public void addIONameChangeListener(int listenerId) throws RemoteException {
	// NameChangeListener dicl = (input, indexOfInput, newName) -> {
	// try {
	// RemoteOperator remoteOp = block.getRemoteOperator();
	// if(remoteOp != null)
	// remoteOp.fireIONameChanged(input, indexOfInput, newName);
	// } catch (Throwable e) {
	// e.printStackTrace();
	// }
	// };
	// if(nameChangeChangeListenersMap == null)
	// nameChangeChangeListenersMap = new HashMap<>();
	// nameChangeChangeListenersMap.put(listenerId, dicl);
	// ((EvolvedOperator) block.operator).addIONameChangeListener(dicl);
	// }
	//
	// @Override
	// public void removeIONameChangeListener(int listenerId) throws RemoteException {
	// if(nameChangeChangeListenersMap == null)
	// return;
	// NameChangeListener ncl = nameChangeChangeListenersMap.get(listenerId);
	// if(ncl == null)
	// return;
	// ((EvolvedOperator) block.operator).removeIONameChangeListener(ncl);
	// }

	private void updateCallingThread(long callingThreadId) {
		RemoteOperator remoteOp = block.getRemoteOperator();
		if (remoteOp != null)
			remoteOp.setCallingThread(callingThreadId);
	}

	@Override
	public boolean updateInputs(long callingThreadId, String[] names, Class<?>[] types) throws RemoteException {
		updateCallingThread(callingThreadId);
		return ((EvolvedOperator) block.operator).updateInputs(names, types);
	}

	@Override
	public boolean updateOutputs(long callingThreadId, String[] names, Class<?>[] types) throws RemoteException {
		updateCallingThread(callingThreadId);
		return ((EvolvedOperator) block.operator).updateOutputs(names, types);
	}
}
