/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario.dataflowdiagram.operator.rmi;

import java.lang.reflect.InvocationTargetException;
import java.rmi.RemoteException;

import org.beanmanager.rmi.server.RMIBeanCallBackImpl;
import org.scenarium.display.ViewerAnimationTimer;
import org.scenarium.timescheduler.VisuableSchedulable;

public class RMIVisuableSchedulableOperator extends RMIOperator implements RMIVisuableSchedulableOperatorImpl {
	private static final long serialVersionUID = 1L;
	private ViewerAnimationTimer viewerAnimationTimer;

	public RMIVisuableSchedulableOperator(Class<?> _class, String identifier, RMIBeanCallBackImpl callBack)
			throws RemoteException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		super(_class, identifier, callBack);
	}

	@Override
	public void setAnimated(long callingThreadId, boolean animated) throws RemoteException {
		// System.err.println("set animated: " + bean.toString());
		mapCallingThreadIdToRmiThread(callingThreadId);
		// System.err.println("start of setAnimated: " + animated + " " + ProcessHandle.current().pid());
		((VisuableSchedulable) bean).setAnimated(animated);
		if (animated && viewerAnimationTimer == null) {
			viewerAnimationTimer = new ViewerAnimationTimer();
			viewerAnimationTimer.register((VisuableSchedulable) bean);
			viewerAnimationTimer.start();
		} else if (!animated && viewerAnimationTimer != null) {
			viewerAnimationTimer.stop();
			viewerAnimationTimer = null;
		}
		unMapCallingThreadIdToRmiThread();
		// System.err.println("set animated: " + bean.toString() + " done");
	}
}
