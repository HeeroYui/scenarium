/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario.dataflowdiagram.operator.rmi;

import java.beans.IntrospectionException;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodHandles.Lookup;
import java.lang.invoke.MethodType;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.EventListener;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import org.beanmanager.BeanDesc;
import org.beanmanager.BeanManager;
import org.beanmanager.BeanRenameListener;
import org.beanmanager.rmi.server.RMIBean;
import org.beanmanager.rmi.server.RMIBeanCallBackImpl;
import org.scenarium.filemanager.scenario.dataflowdiagram.Block;
import org.scenarium.filemanager.scenario.dataflowdiagram.BlockOutput;
import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;
import org.scenarium.filemanager.scenario.dataflowdiagram.IllegalInputArgument;
import org.scenarium.filemanager.scenario.dataflowdiagram.InputLinksChangeListener;
import org.scenarium.filemanager.scenario.dataflowdiagram.VarArgsInputChangeListener;
import org.scenarium.filemanager.scenario.dataflowdiagram.operator.DeclaredInputChangeListener;
import org.scenarium.filemanager.scenario.dataflowdiagram.operator.DeclaredOutputChangeListener;
import org.scenarium.filemanager.scenario.dataflowdiagram.operator.RemoteBlock;
import org.scenarium.filemanager.scenariomanager.StructChangeListener;

public class RMIOperator extends RMIBean implements RMIOperatorImpl, RemoteBlock {
	private static final long serialVersionUID = 1L;
	private MethodHandle processMethodHandle;
	private long mainThreadId;
	private Class<?>[] inputsClass = new Class<?>[0];
	private Class<?>[] additionalInputsClass = new Class<?>[0];
	private Object[] staticOutputsArray;
	private MethodHandle[] staticOutputMethodHandles;
	private boolean isProcessMethodReturnVoid;
	private final HashMap<Integer, BeanRenameListener> beanRenameListenersMap = new HashMap<>();
	private final HashMap<Integer, StructChangeListener> structChangeListenersMap = new HashMap<>();
	private final HashMap<Integer, VarArgsInputChangeListener> varArgsInputChangeListenersMap = new HashMap<>();
	private final HashMap<Integer, InputLinksChangeListener> inputLinksChangeListenersMap = new HashMap<>();
	private final HashMap<Integer, DeclaredInputChangeListener> declaredInputChangeListenersMap = new HashMap<>();
	private final HashMap<Integer, DeclaredOutputChangeListener> declaredOutputChangeListenersMap = new HashMap<>();
	private ConcurrentHashMap<Integer, Runnable> runLaterRunnableMap = new ConcurrentHashMap<>();
	private AtomicInteger runLaterTaskIdCpt = new AtomicInteger(0);
	private ConcurrentHashMap<Integer, Runnable> startedTaskRunnableMap = new ConcurrentHashMap<>();
	private ConcurrentHashMap<Integer, Runnable> resumedTaskRunnableMap = new ConcurrentHashMap<>();
	private ConcurrentHashMap<Integer, Runnable> pausedTaskRunnableMap = new ConcurrentHashMap<>();
	private ConcurrentHashMap<Integer, Runnable> stoppedTaskRunnableMap = new ConcurrentHashMap<>();

	private AtomicInteger startedTaskIdCpt = new AtomicInteger(0);
	private AtomicInteger resumedTaskIdCpt = new AtomicInteger(0);
	private AtomicInteger pausedTaskIdCpt = new AtomicInteger(0);
	private AtomicInteger stoppedTaskIdCpt = new AtomicInteger(0);

	private ArrayList<SerializableWrapper> serializableWrapperPool = new ArrayList<>();

	private ConcurrentHashMap<Thread, Long> callingThreadId = new ConcurrentHashMap<>();

	public RMIOperator(Class<?> _class, String identifier, RMIBeanCallBackImpl callBack)
			throws RemoteException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		super(_class, identifier, callBack);
		try {
			Block block = new Block(bean, null, false);
			staticOutputsArray = new Object[block.getNbStaticOutput()];
			staticOutputMethodHandles = new MethodHandle[block.getNbStaticOutput()];
			List<BlockOutput> outputs = block.getOutputs();
			for (int i = 0; i < staticOutputMethodHandles.length; i++)
				staticOutputMethodHandles[i] = outputs.get(i).getMethodHandle();
			isProcessMethodReturnVoid = block.isProcessMethodReturnVoid();
			if (bean instanceof EvolvedOperator)
				((EvolvedOperator) bean).setBlock(null);
		} catch (IllegalInputArgument e) {
			e.printStackTrace();
		}
	}

	@Override
	public void addBlockNameChangeListener(BeanRenameListener listener) {
		int id = beanRenameListenersMap.size();
		beanRenameListenersMap.put(id, listener);
		try {
			((RMIOperatorCallBackImpl) callBack).addBlockNameChangeListener(id);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void addDeclaredInputChangeListener(DeclaredInputChangeListener listener) {
		int id;
		synchronized (declaredInputChangeListenersMap) {
			id = declaredInputChangeListenersMap.size();
			declaredInputChangeListenersMap.put(id, listener);
		}
		try {
			((RMIOperatorCallBackImpl) callBack).addDeclaredInputChangeListener(id);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void addDeclaredOutputChangeListener(DeclaredOutputChangeListener listener) {
		int id;
		synchronized (declaredOutputChangeListenersMap) {
			id = declaredOutputChangeListenersMap.size();
			declaredOutputChangeListenersMap.put(id, listener);
		}
		try {
			((RMIOperatorCallBackImpl) callBack).addDeclaredOutputChangeListener(id);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void addInputLinksChangeListener(InputLinksChangeListener listener) {
		int id;
		synchronized (inputLinksChangeListenersMap) {
			id = inputLinksChangeListenersMap.size();
			inputLinksChangeListenersMap.put(id, listener);
		}
		try {
			((RMIOperatorCallBackImpl) callBack).addInputLinksChangeListener(id);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void addStructChangeListener(StructChangeListener listener) {
		int id;
		synchronized (structChangeListenersMap) {
			id = structChangeListenersMap.size();
			structChangeListenersMap.put(id, listener);
		}
		try {
			((RMIOperatorCallBackImpl) callBack).addStructChangeListener(id);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void addVarArgsInputChangeListener(VarArgsInputChangeListener listener) {
		int id;
		synchronized (varArgsInputChangeListenersMap) {
			id = varArgsInputChangeListenersMap.size();
			varArgsInputChangeListenersMap.put(id, listener);
		}
		try {
			((RMIOperatorCallBackImpl) callBack).addVarArgsInputChangeListener(id);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void birth(long callingThreadId) throws Throwable {
		mapCallingThreadIdToRmiThread(callingThreadId);
		Lookup lookUp = MethodHandles.lookup();
		if (bean instanceof EvolvedOperator)
			((EvolvedOperator) bean).setRemoteBlock(this);
		for (Method method : bean.getClass().getMethods())
			if (method.getName().equals("process"))
				try {
					method.setAccessible(true);
					processMethodHandle = MethodHandles.lookup().unreflect(method).bindTo(bean);
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
		lookUp.bind(bean, "birth", MethodType.methodType(Void.TYPE, new Class<?>[0])).invokeExact();
		unMapCallingThreadIdToRmiThread();
	}

	@Override
	public void death(long callingThreadId) throws Throwable { // TODO je détruit pas la BDD de bean de remote
		mapCallingThreadIdToRmiThread(callingThreadId);
		MethodHandles.lookup().bind(bean, "death", MethodType.methodType(Void.TYPE, new Class<?>[0])).invokeExact();
		if (bean instanceof EvolvedOperator)
			((EvolvedOperator) bean).setRemoteBlock(null);
		processMethodHandle = null;
		unMapCallingThreadIdToRmiThread();
	}

	@Override
	public void fireBlockNameChanged(long callingThreadId, String name, String local, String oldName, int id) {
		mapCallingThreadIdToRmiThread(callingThreadId);
		BeanRenameListener beanRenameListener;
		synchronized (beanRenameListenersMap) {
			beanRenameListener = beanRenameListenersMap.get(id);
		}
		if (beanRenameListener != null)
			beanRenameListener.beanRename(new BeanDesc<>(bean, name, local), new BeanDesc<>(bean, oldName, local));
		unMapCallingThreadIdToRmiThread();
	}

	@Override
	public void fireDeclaredInputChanged(long callingThreadId, String[] names, Class<?>[] types) {
		mapCallingThreadIdToRmiThread(callingThreadId);
		ArrayList<DeclaredInputChangeListener> values;
		synchronized (declaredInputChangeListenersMap) {
			values = new ArrayList<>(declaredInputChangeListenersMap.values());
		}
		for (DeclaredInputChangeListener listener : values)
			listener.declaredInputChanged(names, types);
		unMapCallingThreadIdToRmiThread();
	}

	@Override
	public void fireDeclaredOutputChanged(long callingThreadId, String[] names, Class<?>[] types) {
		mapCallingThreadIdToRmiThread(callingThreadId);
		ArrayList<DeclaredOutputChangeListener> values;
		synchronized (declaredOutputChangeListenersMap) {
			values = new ArrayList<>(declaredOutputChangeListenersMap.values());
		}
		for (DeclaredOutputChangeListener listener : values)
			listener.declaredOutputChanged(names, types);
		unMapCallingThreadIdToRmiThread();
	}

	@Override
	public void fireInputLinksChanged(long callingThreadId, int indexOfInput) {
		mapCallingThreadIdToRmiThread(callingThreadId);
		ArrayList<InputLinksChangeListener> values;
		synchronized (inputLinksChangeListenersMap) {
			values = new ArrayList<>(inputLinksChangeListenersMap.values());
		}
		for (InputLinksChangeListener listener : values)
			listener.inputLinkChanged(indexOfInput);
		unMapCallingThreadIdToRmiThread();
	}

	@Override
	public void fireStructChanged(long callingThreadId) {
		mapCallingThreadIdToRmiThread(callingThreadId);
		ArrayList<StructChangeListener> values;
		synchronized (structChangeListenersMap) {
			values = new ArrayList<>(structChangeListenersMap.values());
		}
		for (StructChangeListener listener : values)
			listener.structChanged();
		unMapCallingThreadIdToRmiThread();
	}

	@Override
	public void fireVarArgsInputChanged(long callingThreadId, int indexOfInput, int typeOfChange) {
		mapCallingThreadIdToRmiThread(callingThreadId);
		ArrayList<VarArgsInputChangeListener> values;
		synchronized (varArgsInputChangeListenersMap) {
			values = new ArrayList<>(varArgsInputChangeListenersMap.values());
		}
		for (VarArgsInputChangeListener listener : values)
			listener.varArgsInputChanged(indexOfInput, typeOfChange);
		unMapCallingThreadIdToRmiThread();
	}

	@Override
	public String getBlockName() {
		try {
			return ((RMIOperatorCallBackImpl) callBack).getBlockName();
		} catch (RemoteException e) {
			e.printStackTrace();
			return "";
		}
	}

	private long getCallingThreadId() {
		return callingThreadId.getOrDefault(Thread.currentThread(), -1L);
	}

	@Override
	public int getInputIndex(String inputName) {
		try {
			return ((RMIOperatorCallBackImpl) callBack).getInputIndex(inputName);
		} catch (RemoteException e) {
			e.printStackTrace();
			return -1;
		}
	}

	@Override
	public String[] getInputsName() {
		try {
			return ((RMIOperatorCallBackImpl) callBack).getInputsName();
		} catch (RemoteException e) {
			e.printStackTrace();
			return null;
		}
	}

	private static Integer getKeyFromValue(HashMap<Integer, ?> map, EventListener listener) {
		if (map == null)
			return -1;
		int listenerId = -1;
		for (Integer _listenerId : map.keySet())
			if (map.get(_listenerId) == listener) {
				listenerId = _listenerId;
				break;
			}
		return listenerId;
	}

	@Override
	public long getMaxTimeStamp() {
		try {
			return ((RMIOperatorCallBackImpl) callBack).getMaxTimeStamp();
		} catch (RemoteException e) {
			e.printStackTrace();
			return -1;
		}
	}

	@Override
	public int getNbInput() {
		try {
			return ((RMIOperatorCallBackImpl) callBack).getNbInput();
		} catch (RemoteException e) {
			e.printStackTrace();
			return -1;
		}
	}

	@Override
	public int getNbOutput() {
		try {
			return ((RMIOperatorCallBackImpl) callBack).getNbOutput();
		} catch (RemoteException e) {
			e.printStackTrace();
			return -1;
		}
	}

	@Override
	public int getOutputIndex(String outputName) {
		try {
			return ((RMIOperatorCallBackImpl) callBack).getOutputIndex(outputName);
		} catch (RemoteException e) {
			e.printStackTrace();
			return -1;
		}
	}

	@Override
	public String[] getOutputLinkToInputName() {
		try {
			return ((RMIOperatorCallBackImpl) callBack).getOutputLinkToInputName();
		} catch (RemoteException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Class<?>[] getOutputLinkToInputType() {
		try {
			return ((RMIOperatorCallBackImpl) callBack).getOutputLinkToInputType();
		} catch (RemoteException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public String[] getOutputsName() {
		try {
			return ((RMIOperatorCallBackImpl) callBack).getOutputsName();
		} catch (RemoteException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public long getTimeOfIssue(int indexOfInput) {
		try {
			return ((RMIOperatorCallBackImpl) callBack).getTimeOfIssue(indexOfInput);
		} catch (RemoteException e) {
			e.printStackTrace();
			return -1;
		}
	}

	@Override
	public long getTimeStamp(int indexOfInput) {
		try {
			return ((RMIOperatorCallBackImpl) callBack).getTimeStamp(indexOfInput);
		} catch (RemoteException e) {
			e.printStackTrace();
			return -1;
		}
	}

	@Override
	public String getWarning() {
		try {
			return ((RMIOperatorCallBackImpl) callBack).getWarning();
		} catch (RemoteException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public boolean isDefaulting() {
		try {
			return ((RMIOperatorCallBackImpl) callBack).isDefaulting();
		} catch (RemoteException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean isPropertyAsInput(String propertyName) {
		try {
			return ((RMIOperatorCallBackImpl) callBack).isPropertyAsInput(propertyName);
		} catch (RemoteException e) {
			e.printStackTrace();
			return false;
		}
	}

	protected void mapCallingThreadIdToRmiThread(long callingThreadId) {
		this.callingThreadId.put(Thread.currentThread(), callingThreadId);
	}

	@Override
	public boolean needToBeSaved(long callingThreadId) {
		mapCallingThreadIdToRmiThread(callingThreadId);
		boolean returnVal = ((EvolvedOperator) bean).needToBeSaved();
		unMapCallingThreadIdToRmiThread();
		return returnVal;
	}

	@Override
	public void onStart(Runnable runnable) {
		int taskId = startedTaskIdCpt.getAndIncrement();
		startedTaskRunnableMap.put(taskId, runnable);
		try {
			((RMIOperatorCallBackImpl) callBack).onStart(getCallingThreadId(), taskId);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onStartCallBack(long callingThreadId, int taskId) {
		mapCallingThreadIdToRmiThread(callingThreadId);
		startedTaskRunnableMap.remove(taskId).run();
		unMapCallingThreadIdToRmiThread();
	}
	
	@Override
	public void onResume(Runnable runnable) {
		int taskId = resumedTaskIdCpt.getAndIncrement();
		resumedTaskRunnableMap.put(taskId, runnable);
		try {
			((RMIOperatorCallBackImpl) callBack).onResume(getCallingThreadId(), taskId);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onResumeCallBack(long callingThreadId, int taskId) {
		mapCallingThreadIdToRmiThread(callingThreadId);
		resumedTaskRunnableMap.remove(taskId).run();
		unMapCallingThreadIdToRmiThread();
	}
	
	@Override
	public void onPause(Runnable runnable) {
		int taskId = pausedTaskIdCpt.getAndIncrement();
		pausedTaskRunnableMap.put(taskId, runnable);
		try {
			((RMIOperatorCallBackImpl) callBack).onPause(getCallingThreadId(), taskId);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onPauseCallBack(long callingThreadId, int taskId) {
		mapCallingThreadIdToRmiThread(callingThreadId);
		pausedTaskRunnableMap.remove(taskId).run();
		unMapCallingThreadIdToRmiThread();
	}
	
	@Override
	public void onStop(Runnable runnable) {
		int taskId = stoppedTaskIdCpt.getAndIncrement();
		stoppedTaskRunnableMap.put(taskId, runnable);
		try {
			((RMIOperatorCallBackImpl) callBack).onStop(getCallingThreadId(), taskId);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onStopCallBack(long callingThreadId, int taskId) {
		mapCallingThreadIdToRmiThread(callingThreadId);
		stoppedTaskRunnableMap.remove(taskId).run();
		unMapCallingThreadIdToRmiThread();
	}

	@Override
	public Object[] process(long callingThreadId, Object[] inputs, Object[] additionalInputs, boolean[] linkedStaticOutput) throws Throwable {
		mapCallingThreadIdToRmiThread(callingThreadId);
		if (additionalInputs != null) {
			SerializableWrapper.deserialize(additionalInputs);
			((EvolvedOperator) bean).setAdditionalInputs(additionalInputs);
		}
		SerializableWrapper.deserialize(inputs);
		Object val = processMethodHandle.invokeWithArguments(inputs);
		int i;
		if (isProcessMethodReturnVoid)
			i = 0;
		else {
			staticOutputsArray[0] = linkedStaticOutput[0] ? val : null;
			i = 1;
		}
		for (; i < staticOutputsArray.length; i++)
			staticOutputsArray[i] = linkedStaticOutput[i] ? staticOutputMethodHandles[i].invoke() : null;
		SerializableWrapper.serialize(staticOutputsArray, serializableWrapperPool);
		unMapCallingThreadIdToRmiThread();
		return staticOutputsArray;
	}

	@Override
	public Object[] processWithNewInputs(long callingThreadId, String[] inputsClassName, Object[] inputs, String[] additionalInputsClassName, Object[] additionalInputs, boolean[] linkedStaticOutput) throws Throwable {
		if (inputsClassName.length != inputsClass.length)
			inputsClass = new Class<?>[inputsClassName.length];
		for (int i = 0; i < inputsClass.length; i++)
			if (inputsClassName[i] != null)
				inputsClass[i] = BeanManager.getClassFromDescriptor(inputsClassName[i]);
		if (additionalInputsClassName.length != additionalInputsClass.length)
			additionalInputsClass = new Class<?>[additionalInputsClassName.length];
		for (int i = 0; i < additionalInputsClass.length; i++)
			if (additionalInputsClassName[i] != null)
				additionalInputsClass[i] = BeanManager.getClassFromDescriptor(additionalInputsClassName[i]);
		return process(callingThreadId, inputs, additionalInputs, linkedStaticOutput);
	}

	@Override
	public void removeBlockNameChangeListener(BeanRenameListener listener) {
		int listenerId;
		synchronized (beanRenameListenersMap) {
			listenerId = getKeyFromValue(beanRenameListenersMap, listener);
		}
		if (listenerId != -1)
			try {
				((RMIOperatorCallBackImpl) callBack).removeBlockNameChangeListener(listenerId);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
	}

	@Override
	public void removeDeclaredInputChangeListener(DeclaredInputChangeListener listener) {
		int listenerId;
		synchronized (declaredInputChangeListenersMap) {
			listenerId = getKeyFromValue(declaredInputChangeListenersMap, listener);
		}
		if (listenerId != -1)
			try {
				((RMIOperatorCallBackImpl) callBack).removeDeclaredInputChangeListener(listenerId);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
	}

	@Override
	public void removeDeclaredOutputChangeListener(DeclaredOutputChangeListener listener) {
		int listenerId;
		synchronized (declaredOutputChangeListenersMap) {
			listenerId = getKeyFromValue(declaredOutputChangeListenersMap, listener);
		}
		if (listenerId != -1)
			try {
				((RMIOperatorCallBackImpl) callBack).removeDeclaredOutputChangeListener(listenerId);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
	}

	@Override
	public void removeInputLinksChangeListener(InputLinksChangeListener listener) {
		int listenerId;
		synchronized (inputLinksChangeListenersMap) {
			listenerId = getKeyFromValue(inputLinksChangeListenersMap, listener);
		}
		if (listenerId != -1)
			try {
				((RMIOperatorCallBackImpl) callBack).removeInputLinksChangeListener(listenerId);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
	}

	@Override
	public void removeStructChangeListener(StructChangeListener listener) {
		int listenerId;
		synchronized (structChangeListenersMap) {
			listenerId = getKeyFromValue(structChangeListenersMap, listener);
		}
		if (listenerId != -1)
			try {
				((RMIOperatorCallBackImpl) callBack).removeStructChangeListener(listenerId);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
	}

	@Override
	public void removeVarArgsInputChangeListener(VarArgsInputChangeListener listener) {
		int listenerId;
		synchronized (varArgsInputChangeListenersMap) {
			listenerId = getKeyFromValue(varArgsInputChangeListenersMap, listener);
		}
		if (listenerId != -1)
			try {
				((RMIOperatorCallBackImpl) callBack).removeVarArgsInputChangeListener(listenerId);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
	}

	@Override
	public void runLater(Runnable runnable) {
		if (getCallingThreadId() == mainThreadId)
			runnable.run();
		else {
			int taskId = runLaterTaskIdCpt.getAndIncrement();
			runLaterRunnableMap.put(taskId, runnable);
			try {
				((RMIOperatorCallBackImpl) callBack).runLater(getCallingThreadId(), taskId);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void runLaterCallBack(long callingThreadId, int taskId) {
		mapCallingThreadIdToRmiThread(callingThreadId);
		runLaterRunnableMap.remove(taskId).run();
		unMapCallingThreadIdToRmiThread();
	}

	@Override
	public void setDefaulting(boolean defaulting) {
		try {
			((RMIOperatorCallBackImpl) callBack).setDefaulting(defaulting);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void setMainThreadId(long mainThreadId) throws RemoteException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, IntrospectionException {
		this.mainThreadId = mainThreadId;
	}

	@Override
	public boolean setPropertyAsInput(String propertyName, boolean asInput) {
		try {
			return ((RMIOperatorCallBackImpl) callBack).setPropertyAsInput(getCallingThreadId(), propertyName, asInput);
		} catch (RemoteException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public void setWarning(String warning) {
		try {
			((RMIOperatorCallBackImpl) callBack).setWarning(warning);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	// public void addIONameChangeListener(NameChangeListener listener) {
	// int id;
	// synchronized (ioNameChangeListenersMap) {
	// id = ioNameChangeListenersMap.size();
	// ioNameChangeListenersMap.put(id, listener);
	// }
	// try {
	// callBack.addIONameChangeListener(id);
	// } catch (RemoteException e) {
	// e.printStackTrace();
	// }
	// }
	//
	// public void removeIONameChangeListener(NameChangeListener listener) {
	// int listenerId;
	// synchronized (ioNameChangeListenersMap) {
	// listenerId = getKeyFromValue(ioNameChangeListenersMap, listener);
	// }
	// if (listenerId != -1)
	// try {
	// callBack.removeIONameChangeListener(listenerId);
	// } catch (RemoteException e) {
	// e.printStackTrace();
	// }
	// }
	//
	// public void fireIONameChanged(long callingThreadId, boolean input, int indexOfInput, String newName) {
	// mapCallingThreadIdToRmiThread(callingThreadId);
	// ArrayList<NameChangeListener> values;
	// synchronized (ioNameChangeListenersMap) {
	// values = new ArrayList<>(ioNameChangeListenersMap.values());
	// }
	// for (NameChangeListener listener : values)
	// listener.nameChanged(input, indexOfInput, newName);
	// unMapCallingThreadIdToRmiThread();
	// }

	// @Override
	// public boolean isBlockReady(long callingThreadId, boolean[] inputs) {
	// mapCallingThreadIdToRmiThread(callingThreadId);
	// boolean returnVal = ((EvolvedOperator) bean).isBlockReady(inputs);
	// unMapCallingThreadIdToRmiThread();
	// return returnVal;
	// }

	@Override
	public boolean triggerOutput(Object outputValue) {
		try {
			return ((RMIOperatorCallBackImpl) callBack).triggerOutput(getCallingThreadId(), SerializableWrapper.serialize(outputValue, serializableWrapperPool));
		} catch (RemoteException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean triggerOutput(Object outputValue, long timeStamp) {
		try {
			return ((RMIOperatorCallBackImpl) callBack).triggerOutput(getCallingThreadId(), SerializableWrapper.serialize(outputValue, serializableWrapperPool), timeStamp);
		} catch (RemoteException e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean triggerOutput(Object[] outputValues) {
		try {
			SerializableWrapper.serialize(outputValues, serializableWrapperPool);
			return ((RMIOperatorCallBackImpl) callBack).triggerOutput(getCallingThreadId(), outputValues);
		} catch (RemoteException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean triggerOutput(Object[] outputValues, long timeStamp) {
		try {
			SerializableWrapper.serialize(outputValues, serializableWrapperPool);
			return ((RMIOperatorCallBackImpl) callBack).triggerOutput(getCallingThreadId(), outputValues, timeStamp);
		} catch (RemoteException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean triggerOutput(Object[] outputValues, long[] timeStamps) {
		try {
			SerializableWrapper.serialize(outputValues, serializableWrapperPool);
			return ((RMIOperatorCallBackImpl) callBack).triggerOutput(getCallingThreadId(), outputValues, timeStamps);
		} catch (RemoteException e) {
			e.printStackTrace();
			return false;
		}
	}

	protected void unMapCallingThreadIdToRmiThread() {
		this.callingThreadId.remove(Thread.currentThread());
	}

	@Override
	public boolean updateInputs(String[] names, Class<?>[] types) {
		try {
			return ((RMIOperatorCallBackImpl) callBack).updateInputs(getCallingThreadId(), names, types);
		} catch (RemoteException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean updateOutputs(String[] names, Class<?>[] types) {
		try {
			return ((RMIOperatorCallBackImpl) callBack).updateOutputs(getCallingThreadId(), names, types);
		} catch (RemoteException e) {
			e.printStackTrace();
			return false;
		}
	}
}
