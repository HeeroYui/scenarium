/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario.dataflowdiagram.operator.rmi;

import java.io.EOFException;
import java.rmi.RemoteException;

import org.beanmanager.rmi.server.RMIBeanImpl;

public class ScenarioRemoteOperator extends RemoteOperator {

	public ScenarioRemoteOperator(Class<?> operatorClass, String identifier, String hostName, int port, boolean createRemoteOperator, RMIOperatorCallBack callBack, boolean eraseCallBackIfPresent) throws Exception {
		super(operatorClass, identifier, hostName, port, createRemoteOperator, callBack, eraseCallBackIfPresent);
	}

	public ScenarioRemoteOperator(Class<?> operatorClass, String identifier, String hostName, int port, boolean createRemoteOperator, RMIOperatorCallBack callBack, boolean eraseCallBackIfPresent, int timeout) throws Exception {
		super(operatorClass, identifier, hostName, port, createRemoteOperator, callBack, eraseCallBackIfPresent, timeout);
	}

	public void update(int taskId, long timePointer, long timeStamp) {
		RMIBeanImpl _remoteBean = remoteBean;
		if (_remoteBean != null)
			try {
				((RMIScenarioOperatorImpl) _remoteBean).update(Thread.currentThread().getId(), taskId, timePointer, timeStamp);
			} catch (RemoteException e) {
				if (!(e.detail instanceof EOFException))
					e.printStackTrace();
			}
	}
}
