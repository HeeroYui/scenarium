/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario.dataflowdiagram.operator.rmi;

import java.lang.reflect.InvocationTargetException;
import java.rmi.RemoteException;

import org.beanmanager.rmi.server.RMIBean;
import org.beanmanager.rmi.server.RMIBeanCallBackImpl;
import org.beanmanager.rmi.server.RMIBeanServer;
import org.scenarium.ModuleManager;
import org.scenarium.filemanager.scenariomanager.Scenario;
import org.scenarium.timescheduler.VisuableSchedulable;

public class RMIOperatorServer extends RMIBeanServer {
	private static final long serialVersionUID = 1L;

	public static void main(String[] args) throws RemoteException {
		ModuleManager.loadEmbeddedAndInternalModules();
		new RMIOperatorServer(args);
	}

	protected RMIOperatorServer() throws RemoteException {
		super();
	}

	public RMIOperatorServer(String[] args) throws RemoteException {
		super(args);
	}

	@Override
	protected RMIBean getRMIObject(Class<?> _class, String identifier, RMIBeanCallBackImpl callBack)
			throws RemoteException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		if (VisuableSchedulable.class.isAssignableFrom(_class))
			return new RMIVisuableSchedulableOperator(_class, identifier, callBack);
		else if (Scenario.class.isAssignableFrom(_class))
			return new RMIScenarioOperator(_class, identifier, callBack);
		else
			return new RMIOperator(_class, identifier, callBack);
	}

	@Override
	protected RMIBeanServer getRMIServer() throws RemoteException {
		return new RMIOperatorServer();
	}
}
