/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario.dataflowdiagram.operator;

public interface EvolvedVarArgsOperator {
	public boolean canAddInput(Class<?>[] inputsType);

	public boolean isValidInput(Class<?>[] inputsType, Class<?> additionalInput);
}
