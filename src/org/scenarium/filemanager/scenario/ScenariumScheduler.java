/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Objects;

import org.beanmanager.editors.DynamicVisibleBean;
import org.scenarium.display.drawer.TheaterPanel;
import org.scenarium.filemanager.scenario.dataflowdiagram.scheduler.Trigger;
import org.scenarium.filemanager.scenariomanager.LocalScenario;
import org.scenarium.filemanager.scenariomanager.Scenario;
import org.scenarium.filemanager.scenariomanager.ScenarioException;
import org.scenarium.filemanager.scenariomanager.ScenarioManager;
import org.scenarium.filemanager.scenariomanager.TimedScenario;
import org.scenarium.timescheduler.Schedulable;
import org.scenarium.timescheduler.ScheduleTask;
import org.scenarium.timescheduler.Scheduler;

public class ScenariumScheduler extends TimedScenario implements /* MetaScenario, */Trigger, DynamicVisibleBean {
	private ArrayList<TimedScenario> scenarios;

	// private ArrayList<ScheduleTask> schedulableTasks;
	private long beginTime = -1;
	private long endTime = -1;
	private boolean[] valideScenario;

	private int[] scenarioIds;

	@Override
	public boolean canCreateDefault() {
		return false;
	}

	@Override
	synchronized protected boolean close() {
		if (scenarios != null) // Attention, bien garder les scénarios, juste les fermer
			for (LocalScenario scenario : scenarios)
				scenario.death();
		return true;
	}

	// @Override
	// public Scenario[] getAdditionalScenario() {
	// Scenario[] additionalScenarioData = new Scenario[scenarios.size() - 1];
	// for (int i = 0; i < additionalScenarioData.length; i++)
	// additionalScenarioData[i] = scenarios.get(i + 1);
	// return additionalScenarioData;
	// }

	// @Override
	// public boolean needToBeScheduled() {
	// return false;
	// }

	@Override
	public long getBeginningTime() {
		return beginTime;
	}

	@Override
	public Class<?> getDataType() {
		return null;
	}

	@Override
	public long getEndTime() {
		return endTime;
	}

	@Override
	public String[] getReaderFormatNames() {
		return new String[] { "psf" };
	}

	public String[] getScenariosName() {
		return scenarios == null ? null : scenarios.stream().map(s -> getScenarioName(s.getFile().getName())).toArray(String[]::new);
	}

	@Override
	public int getSchedulerType() {
		return Scheduler.EVENTSCHEDULER;
	}

	@Override
	public Schedulable getTaskFromId(int id) {
		return scenarios.get(id);
	}

	@Override
	public int getTaskId(Schedulable task) {
		return scenarios.indexOf(task);
	}

	@Override
	public void initOrdo() {
		if (file == null)
			return;
		ArrayList<ScheduleTask> schedulableTasks = new ArrayList<>();
		schedulableTasks = readScheduleFile();
		// Déplacement en amont, et si le set scheduler ajoute des taches, beginTime et endTime vont changer
		setSchedulerInterfaceToSubScenario();
		if (schedulableTasks != null) {
			schedulerInterface.addTasks(schedulableTasks, (beginTime, endTime) -> {
				this.beginTime = beginTime;
				this.endTime = endTime;
			});
			schedulableTasks = null; // Pour vider les objets
		} else {
			beginTime = -1;
			endTime = -1;
		}
		// updateStartStopTime();
	}

	// private void updateStartStopTime() {
	// schedulerInterface.setStartTime(getStartTime().getTime());
	// schedulerInterface.setStopTime(getStopTime().getTime());
	// }

	@Override
	public void initStruct() throws Exception {
		// System.out.println("init struct de: " + (getBlockName().length() != 0 ? getBlockName() : "") + " " + file);
		// ArrayList<Scenario> _scenarios = scenarios; //Pk???
		if (scenarios == null)
			readScheduleFileHeader();
		ArrayList<String> names = new ArrayList<>();
		ArrayList<Class<?>> types = new ArrayList<>();
		if (scenarios != null)
			for (LocalScenario scenario : scenarios) {
				File lfile = scenario.getFile();
				if (lfile != null) {
					Class<?> type = scenario.getDataType();
					if (type != null) {
						names.add(getScenarioName(lfile.getName()));
						types.add(scenario.getDataType());
					}
				}
			}
		updateOutputs(names.toArray(new String[0]), types.toArray(new Class<?>[0]));
	}

	// @Override
	// public boolean isTimeRepresentation() {
	// return true;
	// }

	@Override
	synchronized public void load(File scenarioFile, boolean backgroundLoading) throws IOException, ScenarioException {
		file = scenarioFile;
		if (scenarios == null) {
			readScheduleFileHeader();
			setSchedulerInterfaceToSubScenario();
		}
		// throw new IllegalArgumentException("c'est bien utile, a voir pk?");
		for (int i = 0; i < scenarios.size(); i++) {
			LocalScenario scenario = scenarios.get(i);
			try {
				scenario.birth();
				scenario.setTrigger(this);
				scenario.setId(i);
			} catch (Exception e) {
				System.err.println("Cannot load the scenario: " + scenario);
				e.printStackTrace();
			}
		}
		if (scenarios.size() != 0)
			scenarioData = scenarios.get(0).getScenarioData();
		fireLoadChanged();
	}

	@Override
	public void populateInfo(LinkedHashMap<String, String> info) throws IOException {

	}

	@Override
	public void process(Long timePointer) throws Exception {
		throw new IllegalAccessError(getClass().getSimpleName() + " can never be call");
	}

	private void readCompactHeader(MappedByteBuffer buffer, String scenarioPath, HashSet<String> scenarioNames) {
		buffer.get();
		scenarios = new ArrayList<>();
		int nbInput = buffer.getInt();
		valideScenario = new boolean[nbInput];
		scenarioIds = new int[nbInput];
		for (int i = 0; i < nbInput; i++) {
			String filePath = getString(buffer);
			String scenarioName = getScenarioName(filePath);
			if (scenarioNames.contains(scenarioName)) {
				System.err.println("This scenario already include the outputs: " + scenarioName);
				continue;
			}
			scenarioNames.add(scenarioName);
			filePath = scenarioPath + File.separator + filePath;
			if (!new File(filePath).exists())
				continue;
			Class<? extends Scenario> scenarioType = ScenarioManager.getScenarioType(new File(filePath));
			if (scenarioType == null) {
				System.err.println("No loader for the file: " + filePath);
				continue;
			} else if (!LocalScenario.class.isAssignableFrom(scenarioType)) {
				System.err.println("Not a LocalScenario type");
				continue;
			}
			try {
				TimedScenario scenario = (TimedScenario) scenarioType.getConstructor().newInstance();
				scenario.setFile(new File(filePath));
				if (scenario.getDataType() == null)
					continue;
				scenario.setStartTime(getStartTime());
				scenario.setStopTime(getStopTime());
				scenario.setPeriod(1);
				scenarios.add(scenario);
				scenarioIds[i] = scenarios.size() - 1;
			} catch (IllegalArgumentException | InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
				System.err.println("Cannot create the scenario: " + scenarioType.getSimpleName());
				continue;
			}
			valideScenario[i] = true;
		}
	}

	private void readHeader(BufferedReader br, String scenarioPath, HashSet<String> scenarioNames) throws NumberFormatException, IOException {
		scenarios = new ArrayList<>();
		int nbInput = Integer.parseInt(br.readLine());
		valideScenario = new boolean[nbInput];
		scenarioIds = new int[nbInput];
		for (int i = 0; i < nbInput; i++) {
			String filePath = br.readLine();
			filePath = filePath.substring(0, filePath.lastIndexOf(":"));
			String scenarioName = getScenarioName(filePath);
			if (scenarioNames.contains(scenarioName)) {
				System.err.println("This scenario already include the outputs: " + scenarioName);
				continue;
			}
			scenarioNames.add(scenarioName);
			filePath = scenarioPath + File.separator + filePath;
			if (!new File(filePath).exists())
				continue;
			Class<? extends Scenario> scenarioType = ScenarioManager.getScenarioType(new File(filePath));
			if (scenarioType == null) {
				System.err.println("no loader for the file: " + filePath);
				continue;
			} else if (!LocalScenario.class.isAssignableFrom(scenarioType)) {
				System.err.println("Not a LocalScenario type");
				continue;
			}
			try {
				TimedScenario scenario = (TimedScenario) scenarioType.getConstructor().newInstance();
				scenario.setFile(new File(filePath));
				if (scenario.getDataType() == null)
					continue;
				scenario.setStartTime(getStartTime());
				scenario.setStopTime(getStopTime());
				scenario.setPeriod(1);
				scenarios.add(scenario);
				scenarioIds[i] = scenarios.size() - 1;
			} catch (IllegalArgumentException | InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
				System.err.println("cannot create the scenario: " + scenarioType.getSimpleName());
				continue;
			}
			valideScenario[i] = true;
		}
	}

	private ArrayList<ScheduleTask> readScheduleFile() {
		// System.err.println("readScheduleFile: " + Thread.currentThread().getId());
		if (file == null || !file.getName().endsWith(getReaderFormatNames()[0]) || !file.exists())
			return null;
		String ligne = null;
		String scenarioPath = file.getParent();
		ArrayList<ScheduleTask> schedulableTasks = new ArrayList<>();
		HashSet<String> scenarioNames = new HashSet<>();
		try (FileInputStream fis = new FileInputStream(file)) {
			if (fis.read() == 0) { // Lecture normale
				try (BufferedReader br = new BufferedReader(new InputStreamReader(fis))) {
					if (scenarios == null)
						readHeader(br, scenarioPath, scenarioNames);
					else
						skipHeader(br);
					Scenario[] scenariosArray = new Scenario[scenarios.size()];
					for (int i = 0; i < scenariosArray.length; i++)
						scenariosArray[i] = scenarios.get(i);
					int valideScenarioLength = valideScenario.length;
					while ((ligne = br.readLine()) != null) {
						int i1 = ligne.indexOf(":");
						int i2 = ligne.indexOf(":", i1 + 1);
						int i3 = ligne.indexOf(":", i2 + 1);
						int scenarioIndex = Integer.parseInt(ligne.substring(i2 + 1, i3));
						if (scenarioIndex < valideScenarioLength && valideScenario[scenarioIndex] == true)
							schedulableTasks.add(new ScheduleTask(Long.parseLong(ligne.substring(0, i1)), Long.parseLong(ligne.substring(i1 + 1, i2)), scenariosArray[scenarioIds[scenarioIndex]],
									Integer.parseInt(ligne.substring(i3 + 1))));
					}
				}
			} else {
				FileChannel inChannel = fis.getChannel();
				MappedByteBuffer buffer = inChannel.map(FileChannel.MapMode.READ_ONLY, 0, inChannel.size());
				buffer.load();
				if (scenarios == null)
					readCompactHeader(buffer, scenarioPath, scenarioNames);
				else
					skipCompactHeader(buffer);
				Scenario[] scenariosArray = new Scenario[scenarios.size()];
				for (int i = 0; i < scenariosArray.length; i++)
					scenariosArray[i] = scenarios.get(i);
				int valideScenarioLength = valideScenario.length;
				try {
					while (buffer.hasRemaining()) {
						long toi = buffer.getLong();
						long ts = buffer.getLong();
						int scenarioIndex = buffer.getInt();
						long seekIndex = buffer.getLong();
						if (scenarioIndex >= 0 && scenarioIndex < valideScenarioLength && valideScenario[scenarioIndex] == true)
							schedulableTasks.add(new ScheduleTask(toi, ts, scenariosArray[scenarioIds[scenarioIndex]], seekIndex));
					}
				} catch (BufferUnderflowException e) {
					System.err.println("The end of the schedule file: " + file + " is corrupted, some data may be missing");
				}
			}
		} catch (NumberFormatException | StringIndexOutOfBoundsException | IOException e) {
			System.err.println("Erreur while reading Schedule File: " + e.getClass().getSimpleName() + ": " + e.getMessage());
		}
		return schedulableTasks;
	}

	private void readScheduleFileHeader() {
		scenarios = new ArrayList<>();
		if (file == null || !file.getName().endsWith(getReaderFormatNames()[0]) || !file.exists())
			return;
		String scenarioPath = file.getParent();
		HashSet<String> scenarioNames = new HashSet<>();
		try (FileInputStream fis = new FileInputStream(file);) {
			if (fis.read() == 0) { // Lecture normale
				try(BufferedReader br = new BufferedReader(new InputStreamReader(fis))){
					readHeader(br, scenarioPath, scenarioNames);
				}
			} else {
				FileChannel inChannel = fis.getChannel();
				MappedByteBuffer buffer = inChannel.map(FileChannel.MapMode.READ_ONLY, 0, inChannel.size());
				buffer.load();
				readCompactHeader(buffer, scenarioPath, scenarioNames);
			}
		} catch (NumberFormatException | IOException e) {
			System.err.println("Erreur while reading Schedule File: " + e.getClass().getSimpleName() + ": " + e.getMessage());
		}
	}

	@Override
	public void setStartTime(Date startTime) {
		super.setStartTime(startTime);
		if (scenarios != null)
			for (TimedScenario scenario : scenarios)
				scenario.setStartTime(startTime);
	}

	@Override
	public void setStopTime(Date stopTime) {
		super.setStopTime(stopTime);
		if (scenarios != null)
			for (TimedScenario scenario : scenarios)
				scenario.setStopTime(stopTime);
	}

	@Override
	public void save(File file) throws IOException {}

	@Override
	public void setFile(File file) {
		if (Objects.equals(file, this.file))
			return;
		scenarios = null;
		super.setFile(file);
		if (hasBlock())
			try {
				initStruct();
			} catch (Exception e) {
				e.printStackTrace();
				throw new IllegalArgumentException("Cannot init the struct, " + file.getName());
			}
		// fireStructChanged();
	}

	private void setSchedulerInterfaceToSubScenario() {
		if (scenarios != null)
			for (Scenario scenario : scenarios)
				scenario.setScheduler(schedulerInterface);
	}

	@Override
	public void setTheaterPanel(TheaterPanel theaterPanel) {
		super.setTheaterPanel(theaterPanel);
		scenarios.get(0).setTheaterPanel(theaterPanel);
	}

	@Override
	public void setTrigger(Trigger trigger) {
		super.setTrigger(trigger);
		if (trigger != null && scenarios == null) // Ajout pour ne pas lire le fichier quand on arrête le diagramme
			readScheduleFileHeader(); // 14/09/17 -> changement de readScheduleFile vers readScheduleFileHeader. On a besoin que des scenarios...
		if (scenarios == null)
			return;
	}

	private static void skipCompactHeader(MappedByteBuffer buffer) throws NumberFormatException {
		buffer.get();
		int nbInput = buffer.getInt();
		for (int i = 0; i < nbInput; i++)
			getString(buffer);
	}

	private static void skipHeader(BufferedReader br) throws NumberFormatException, IOException {
		int nbInput = Integer.parseInt(br.readLine());
		for (int i = 0; i < nbInput; i++)
			br.readLine();
	}

	@Override
	public void triggerOutput(Object source, Object outputValue, long timeStamp) {
		triggerOutput(outputValue, timeStamp);
	}

	@Override
	public void triggerOutput(Object source, Object[] outputValues, long timeStamp) {
		Object[] ouputsVector = generateOuputsVector();
		if (ouputsVector == null)
			return;
		ouputsVector[(Integer) source] = outputValues[0];
		triggerOutput(ouputsVector, timeStamp);
	}

	@Override
	public void triggerOutput(Object source, Object[] outputValues, long[] timeStamps) {
		Object[] ouputsVector = generateOuputsVector();
		if (ouputsVector == null)
			return;
		ouputsVector[(Integer) source] = outputValues[0];
		if (ouputsVector.length != timeStamps.length) {
			long[] newTimeStamps = new long[ouputsVector.length];
			newTimeStamps[(Integer) source] = timeStamps[0];
			timeStamps = newTimeStamps;
		}
		triggerOutput(ouputsVector, timeStamps);
	}

	@Override
	public void triggerTask(Object source, Runnable runnable) {
		runnable.run();
	}

	@Override
	public void setVisible() {
		fireSetPropertyVisible(this, "period", false);
	}

	public static String getScenarioName(String filePath) {
		return filePath.substring(0, filePath.lastIndexOf("."))/* .substring(filePath.indexOf("_") + 1) */;
	}

	private static String getString(ByteBuffer bb) {
		byte[] stringBytes = new byte[bb.getInt()];
		bb.get(stringBytes);
		return new String(stringBytes, StandardCharsets.UTF_8);
	}
}