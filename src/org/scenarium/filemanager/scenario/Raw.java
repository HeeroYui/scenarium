/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario;

import java.awt.Transparency;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.ComponentColorModel;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferUShort;
import java.awt.image.WritableRaster;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.LinkedHashMap;
import java.util.Objects;

import org.scenarium.filemanager.RasterInfo;
import org.scenarium.filemanager.scenariomanager.ScenarioException;
import org.scenarium.filemanager.scenariomanager.TimedScenario;
import org.scenarium.struct.ScenariumProperties;
import org.scenarium.struct.YUVColorSpace;
import org.scenarium.struct.raster.BufferedImageStrategy;
import org.scenarium.timescheduler.Scheduler;

public class Raw extends TimedScenario {
	private static final int GRAY = 0;
	private static final int RGB = 1;
	private static final int BGR = 2;
	private static final int YCBCR = 3;
	private static final int GRAY16 = 4;
	public static final String[] EXTENSIONS = { "raw", "inf" };
	private RandomAccessFile movieFile;
	private int channelsSequence;
	private int nbFrame;
	private int rasterType;
	private int width;
	private int height;

	@Override
	public boolean canCreateDefault() {
		return false;
	}

	@Override
	public boolean close() {
		try {
			if (movieFile != null)
				movieFile.close();
			movieFile = null;
			return true;
		} catch (IOException e) {
			return false;
		}
	}

	@Override
	public long getBeginningTime() {
		return 0;
	}

	@Override
	public Class<?> getDataType() {
		return BufferedImage.class;
	}

	@Override
	public long getEndTime() {
		return getNbGap() * getPeriod();
	}

	public long getNbGap() {
		if (nbFrame == -1 && file != null)
			try {
				readHeader(file);
			} catch (IOException | ScenarioException e) {}
		return nbFrame;
	}

	@Override
	public String[] getReaderFormatNames() {
		return EXTENSIONS;
	}

	@Override
	public int getSchedulerType() {
		return Scheduler.TIMERSCHEDULER;
	}

	// @Override
	// public boolean isTimeRepresentation() {
	// return false;
	// }

	@Override
	public void load(File file, boolean backgroundLoading) throws IOException, ScenarioException {
		if (movieFile == null)
			readHeader(file);
		if (width == 0 || height == 0)
			return;
		BufferedImage raster;
		if (rasterType == BufferedImage.TYPE_CUSTOM) {
			ColorModel cm = new ComponentColorModel(new YUVColorSpace(), new int[] { 8, 8, 8 }, false, false, Transparency.OPAQUE, DataBuffer.TYPE_BYTE);
			raster = buildBufferedImage(cm, cm.createCompatibleWritableRaster(width, height), false);
		} else
			raster = buildBufferedImage(width, height, rasterType);
		if (rasterType == BufferedImage.TYPE_USHORT_GRAY) {
			short[] pixels = ((DataBufferUShort) raster.getRaster().getDataBuffer()).getData();
			byte[] pixelsByte = new byte[pixels.length * 2];
			movieFile.read(pixelsByte, 0, pixelsByte.length);
			ByteBuffer.wrap(pixelsByte).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().get(pixels);
		} else {
			byte[] pixels = ((DataBufferByte) raster.getRaster().getDataBuffer()).getData();
			movieFile.read(pixels, 0, pixels.length);
			if (channelsSequence == RGB)
				for (int i = 0; i < pixels.length;) {
					byte temp = pixels[i];
					pixels[i++] = pixels[++i];
					pixels[i++] = temp;
				}
		}
		scenarioData = new BufferedImageStrategy(ScenariumProperties.get().isPageFlipping());
		((BufferedImageStrategy) scenarioData).setComputeElement(raster);
		fireLoadChanged();
	}

	protected BufferedImage buildBufferedImage(int width, int height, int rasterType) {
		return new BufferedImage(width, height, rasterType);
	}

	protected BufferedImage buildBufferedImage(ColorModel cm, WritableRaster raster, boolean isRasterPremultiplied) {
		return new BufferedImage(cm, raster, isRasterPremultiplied, null);
	}

	@Override
	public void populateInfo(LinkedHashMap<String, String> info) throws IOException {
		try {
			load(file, false);
			RasterInfo.populate(info, ((BufferedImageStrategy) getScenarioData()).getElement());
			info.put("Number of frames", Integer.toString(nbFrame));
			close();
		} catch (ScenarioException e) {
			e.printStackTrace();
		}
		info.put("Data coding format", "RAW");
	}

	@Override
	public void process(Long timePointer) {
		// System.out.println("Play raw from process: " + ProcessHandle.current().pid());
		if (timePointer < 0) {
			System.err.println("Negative framePointer : " + timePointer);
			return;
		}
		timePointer /= getPeriod();
		if (timePointer >= nbFrame) {
			System.err.println("error");
		}
		BufferedImage raster = ((BufferedImageStrategy) scenarioData).getComputeElement();
		if (raster.getWidth() != width || raster.getHeight() != height || raster.getType() != rasterType) {
			if (rasterType == BufferedImage.TYPE_CUSTOM) {
				ColorModel cm = new ComponentColorModel(new YUVColorSpace(), new int[] { 8, 8, 8 }, false, false, Transparency.OPAQUE, DataBuffer.TYPE_BYTE);
				raster = buildBufferedImage(cm, cm.createCompatibleWritableRaster(width, height), false);
			} else
				raster = buildBufferedImage(width, height, rasterType);
			((BufferedImageStrategy) scenarioData).setComputeElement(raster);
		}
		if (channelsSequence == GRAY16) {
			short[] pixels = ((DataBufferUShort) raster.getRaster().getDataBuffer()).getData();
			byte[] pixelsByte = new byte[pixels.length * 2];
			if (movieFile == null)
				return;
			try {
				movieFile.seek(timePointer * pixelsByte.length);
				movieFile.read(pixelsByte, 0, pixelsByte.length);
				ByteBuffer.wrap(pixelsByte).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().get(pixels);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			byte[] pixels = ((DataBufferByte) raster.getRaster().getDataBuffer()).getData();
			if (movieFile == null)
				return;
			try {
				movieFile.seek(timePointer * pixels.length);
				int nbBytesRead = movieFile.read(pixels, 0, pixels.length);
				if (nbBytesRead < 0)
					throw new IllegalArgumentException("Cannot read raw file");
				if (channelsSequence == RGB)
					for (int i = 0; i < pixels.length;) {
						byte temp = pixels[i];
						pixels[i++] = pixels[++i];
						pixels[i++] = temp;
					}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void readHeader(File file) throws IOException, ScenarioException {
		if (file == null || file.toString().isEmpty())
			return;
		String filePath = file.getAbsolutePath();
		int extIndex = filePath.lastIndexOf(".");
		String ext = filePath.substring(extIndex + 1, filePath.length());
		movieFile = new RandomAccessFile(ext.equals("raw") ? file : new File(filePath.substring(0, extIndex).concat(".raw")), "r");
		if (!ext.equals("inf"))
			file = new File(file.getAbsolutePath().substring(0, extIndex).concat(".inf"));
		this.file = file;
		try (BufferedReader infReader = new BufferedReader(new FileReader(file))) {
			String ligne;
			ligne = infReader.readLine();
			nbFrame = Integer.parseInt(ligne.substring(0, ligne.indexOf(" ")).trim());
			ligne = infReader.readLine();
			width = Integer.parseInt(ligne.substring(0, ligne.indexOf(" ")).trim());
			ligne = infReader.readLine();
			height = Integer.parseInt(ligne.substring(0, ligne.indexOf(" ")).trim());
			ligne = infReader.readLine();
			String codingFormat = ligne.substring(0, ligne.indexOf(" ")).trim();
			ligne = infReader.readLine();
			String channelsSequence = ligne.substring(0, ligne.indexOf(" ")).trim();
			infReader.readLine(); // depth
			ligne = infReader.readLine();
			int imageSize = Integer.parseInt(ligne.substring(0, ligne.indexOf(" ")).trim());
			int nbFrame2 = (int) (movieFile.length() / imageSize);
			if (nbFrame2 != nbFrame)
				System.err.println("Corrupt raw file: " + file + "\nThe inf file indicated: " + nbFrame + " frames and the raw file contain: " + nbFrame2 + " frames");
			if (!codingFormat.equals("RAW"))
				throw new ScenarioException("Scenarium only support raw files");
			rasterType = -1;
			if (channelsSequence.equals("GRAY")) {
				this.channelsSequence = GRAY;
				rasterType = BufferedImage.TYPE_BYTE_GRAY; // Raster.GRAY;
			} else if (channelsSequence.equals("RGB")) {
				this.channelsSequence = RGB;
				rasterType = BufferedImage.TYPE_3BYTE_BGR; // Raster.RGB;
			} else if (channelsSequence.equals("BGR")) {
				this.channelsSequence = BGR;
				rasterType = BufferedImage.TYPE_3BYTE_BGR; // Raster.BGR;
			} else if (channelsSequence.equals("YCbCr")) {
				this.channelsSequence = YCBCR;
				rasterType = BufferedImage.TYPE_CUSTOM; // Raster.BGR;
			} else if (channelsSequence.equals("GRAY16")) {
				this.channelsSequence = GRAY16;
				rasterType = BufferedImage.TYPE_USHORT_GRAY; // Raster.BGR;
			} else
				throw new ScenarioException("The channelsSequence: " + channelsSequence + " is not supported");

		}
	}

	@Override
	public void save(File file) throws IOException {
		// ImageStreamRecorder isr = new ImageStreamRecorder();
		// isr.setRecordPath(file.getParent());
		// isr.pop(((BufferedImageStrategy) scenarioData).getDrawElement());
	}

	@Override
	public void setFile(File file) {
		if (Objects.equals(file, this.file))
			return;
		nbFrame = -1;
		super.setFile(file);
	}
}
