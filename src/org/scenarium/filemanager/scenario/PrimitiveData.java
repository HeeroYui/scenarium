/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario;

import java.io.DataInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.RandomAccessFile;
import java.nio.channels.ClosedByInterruptException;
import java.nio.channels.FileChannel;
import java.util.LinkedHashMap;
import java.util.Objects;

import org.beanmanager.BeanManager;
import org.beanmanager.editors.PropertyEditor;
import org.beanmanager.editors.PropertyEditorManager;
import org.scenarium.communication.can.CanTrame;
import org.scenarium.filemanager.scenariomanager.ScenarioException;
import org.scenarium.filemanager.scenariomanager.TimedScenario;
import org.scenarium.timescheduler.Scheduler;

public class PrimitiveData extends TimedScenario/* implements IOTypeChanger */ {
	private long nbDatas = -1;
	private DataInputStream dataDis;
	private FileChannel dataChannel;
	private FileInputStream dataFis;
	private DataInputStream indexDis;
	private FileChannel indexFc;
	private ObjectInputStream ois;
	private PropertyEditor<?> sEditor;
	private Class<?> type;
	// private final EventListenerList listeners = new EventListenerList();
	private long beginFilePointeur;
	private int cpt = 100;

	@Override
	public boolean canCreateDefault() {
		return false;
	}

	// @Override
	// public boolean needToBeScheduled() {
	// return true;
	// }

	@Override
	synchronized protected boolean close() {
		try {
			if (dataDis != null)
				dataDis.close();
			if (dataFis != null)
				dataFis.close();
			if (ois != null)
				ois.close();
			scenarioData = null;
			dataDis = null;
			return true;
		} catch (IOException e) {
			return false;
		}
	}

	@Override
	public long getBeginningTime() {
		return 0;
	}

	@Override
	public Class<?> getDataType() {
		return type;
	}

	@Override
	public long getEndTime() {
		return getNbGap()* getPeriod();
	}

	public long getNbGap() {
		if (nbDatas == -1 && file != null)
			try {
				load(file, false);
				close();
			} catch (IOException | ScenarioException e) {
				return -1;
			}
		return nbDatas;
	}

	@Override
	public String[] getReaderFormatNames() {
		return new String[] { "ppd" };
	}

	@Override
	public int getSchedulerType() {
		return Scheduler.TIMERSCHEDULER;
	}

//	@Override
//	public boolean isTimeRepresentation() {
//		return false;
//	}

	@Override
	synchronized public void load(File scenarioFile, boolean backgroundLoading) throws IOException, ScenarioException {
		String ligne;
		try(RandomAccessFile _rafData = new RandomAccessFile(scenarioFile, "r")){
			ligne = _rafData.readLine(); // TODO dangereux... il faut passer par read string plutot
			beginFilePointeur = _rafData.getFilePointer();
		}
		FileInputStream rafDataFis = new FileInputStream(scenarioFile);
		dataChannel = rafDataFis.getChannel();
		try {
			if (ligne.equals("dataStructure.can.CanTrame"))
				type = CanTrame.class;
			else
				type = BeanManager.getClassFromDescriptor(ligne);
			PropertyEditor<?> editor = PropertyEditorManager.findEditor(type, "");
			file = scenarioFile;
			dataDis = new DataInputStream(rafDataFis);
			sEditor = editor;
			int pitch = sEditor.getPitch();
			if (pitch != -1)
				nbDatas = (scenarioFile.length() - beginFilePointeur) / pitch;
			else {
				FileInputStream rafIndexFis = getFileIndexInputStream(scenarioFile);
				indexFc = rafIndexFis.getChannel();
				indexDis = new DataInputStream(rafIndexFis);
				nbDatas = new File(getFileIndexPath(scenarioFile.getAbsolutePath())).length() / Long.BYTES;
			}
			dataChannel.position(beginFilePointeur);
			scenarioData = sEditor.readValue(dataDis);
		} catch (IOException | ClassNotFoundException e) {
			if (e instanceof IOException)
				System.err.println("Error while loading: " + scenarioFile + "\nCaused by: " + e.getClass().getSimpleName() + ": " + e.getMessage());
			else
				System.err.println("Cannot find the class: " + ligne);
		}
		fireLoadChanged();
	}

	@Override
	public void populateInfo(LinkedHashMap<String, String> info) throws IOException {}

	@Override
	synchronized public void process(Long timePointer) throws IOException, ClassNotFoundException, ScenarioException {
 		timePointer /= getPeriod();
		try {
			if (timePointer >= nbDatas) {
				scenarioData = null;
				return;
			}
			if (!dataChannel.isOpen() || (indexFc != null && !indexFc.isOpen())) {
				dataChannel.close();
				FileInputStream rafDataFis = new FileInputStream(file);
				dataChannel = rafDataFis.getChannel();
				if (ois == null) {
					dataDis = new DataInputStream(rafDataFis);
					if (indexFc != null) {
						indexFc.close();
						FileInputStream rafIndexFis = getFileIndexInputStream(file);
						indexFc = rafIndexFis.getChannel();
						indexDis = new DataInputStream(rafIndexFis);
					}
				} else {
					indexDis = new DataInputStream(getFileIndexInputStream(file));
					dataFis = rafDataFis;
					dataFis.skip(beginFilePointeur);
					ois = new ObjectInputStream(dataFis);
				}
			}
			if (indexDis == null) {
				dataChannel.position(beginFilePointeur + timePointer * sEditor.getPitch());
				scenarioData = sEditor.readValue(dataDis);// ((PrimitiveCell) scenarioData).setValue(sEditor.readValue(raf));
			} else {
				indexFc.position(timePointer * Long.BYTES);
				if (ois == null) { // Pour les objects flux
					dataChannel.position(indexDis.readLong());
					scenarioData = sEditor.readValue(dataDis);
				} else { // Pour les objects flux
					dataFis.getChannel().position(indexDis.readLong());
					scenarioData = ois.readUnshared();
					// Memory leak sinon...
					if (cpt == 0) {
						ois.close();
						dataFis = new FileInputStream(file);
						dataFis.skip(beginFilePointeur);
						dataChannel = dataFis.getChannel();
						ois = new ObjectInputStream(dataFis);
						cpt = 100;
					} else
						cpt--;
				}
			}
		} catch (ClosedByInterruptException e) {
			// death();
			// load(file, false); //Pk c'est normal une ClosedByInterruptException si interruptedException...
		} catch (EOFException e) {
			e.printStackTrace();
		}
	}

	private static FileInputStream getFileIndexInputStream(File file) throws FileNotFoundException {
		return new FileInputStream(getFileIndexPath(file.getAbsolutePath()));
	}

	private static String getFileIndexPath(String filePath) {
		return filePath.substring(0, filePath.lastIndexOf(".") + 1) + "pindex";
	}

	@Override
	public void save(File file) throws IOException {
		return;
	}

	@Override
	public void setFile(File file) {
		if (Objects.equals(file, this.file))
			return;
		if (file == null) {
			super.setFile(null);
			return;
		}
		Class<?> newType = null;
		try (RandomAccessFile _raf = new RandomAccessFile(file, "r")){
			String ligne = _raf.readLine();
			try {
				newType = BeanManager.getClassFromDescriptor(ligne);
			} catch (ClassNotFoundException e) {
				if (ligne.equals("dataStructure.can.CanTrame"))
					newType = CanTrame.class;
				else
					System.err.println("Cannot find the class: " + ligne + " indicated in the primitive data file: " + file.getCanonicalPath());
			}
		} catch (IOException e1) {
			if (!file.toString().isEmpty())
				System.err.println("Error while setting file: " + file + " caused by: " + e1.getClass().getSimpleName());
		}
		boolean isNewType = false;
		if (newType != type) {
			type = newType;
			isNewType = true;
		}
		if (isNewType)
			try {
				initStruct();
			} catch (Exception e) {
				e.printStackTrace();
				throw new IllegalArgumentException("Cannot init the struct, " + file.getName());
			}
		super.setFile(file);
	}
}
