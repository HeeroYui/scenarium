/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenario;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import org.scenarium.filemanager.scenariomanager.ScenarioException;
import org.scenarium.filemanager.scenariomanager.TimedScenario;
import org.scenarium.struct.CSVData;
import org.scenarium.timescheduler.ScheduleTask;
import org.scenarium.timescheduler.Scheduler;

public class CSV extends TimedScenario {
	private static final String DELIMITER = ",";
	public static final HashMap<String, Class<?>> typeOfVariable = new HashMap<>();
	static {
		typeOfVariable.put("Index", Integer.class);
		typeOfVariable.put("lon_gps", Double.class);
		typeOfVariable.put("long_gps", Double.class);
		typeOfVariable.put("lat_gps", Double.class);
		typeOfVariable.put("elev_gps", Double.class);
		typeOfVariable.put("accuracy_gps", Double.class);
		typeOfVariable.put("highWay", String.class);
		typeOfVariable.put("tunnel", Boolean.class);
		typeOfVariable.put("pke", Double.class);
		typeOfVariable.put("timestamp", Long.class);
	}
	private ArrayList<ScheduleTask> schedulableTasks;
	public Class<?>[] typeOfColumn;
	private RandomAccessFile raf;
	private String info;

	private long[] lineIndex;

	@Override
	public boolean canCreateDefault() {
		return false;
	}

	@Override
	protected boolean close() {
		schedulableTasks = null;
		typeOfColumn = null;
		try {
			if (raf != null)
				raf.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		raf = null;
		info = null;
		lineIndex = null;
		return true;
	}

	@Override
	public long getBeginningTime() {
		return 0;
	}

	@Override
	public Class<?> getDataType() {
		return CSVData.class;
	}

	@Override
	public long getEndTime() {
		if (lineIndex == null)
			try {
				init(file);
			} catch (IOException e) {
				System.err.println("Error while reading file: " + e.getMessage());
			}
		return lineIndex != null ? lineIndex.length - 1 : -1;
	}

	@Override
	public String[] getReaderFormatNames() {
		return new String[] { "csv" };
	}

	@Override
	public int getSchedulerType() {
		return Scheduler.EVENTSCHEDULER;
	}

	// @Override
	// public boolean needToBeScheduled() {
	// return false;
	// }

	private static Object getValue(Class<?> class1, String value) {
		if (value.isEmpty())
			return null;
		if (class1 == null)
			return value;
		if (class1.equals(Integer.class))
			return Integer.parseInt(value);
		if (class1.equals(Double.class))
			return Double.parseDouble(value);
		if (class1.equals(Boolean.class))
			return Boolean.parseBoolean(value);
		if (class1.equals(Long.class))
			return Long.parseLong(value);
		return value;
	}

	private void init(File file) throws IOException {
		if (file == null || !file.exists())
			return;
		List<String> lines = Files.readAllLines(file.toPath());
		int lineIndex = 0;
		long ptr = 0;
		CSVData cSVData = new CSVData();
		LinkedHashMap<String, Integer> typeIndex = new LinkedHashMap<>();
		int i = 0;
		for (String headerName : lines.get(lineIndex++).split(DELIMITER)) {
			typeIndex.put(headerName.trim(), i++);
			ptr += headerName.length() + 1;
		}
		typeOfColumn = new Class<?>[i];
		i = 0;
		for (String typeName : typeIndex.keySet())
			typeOfColumn[i++] = typeOfVariable.get(typeName);
		cSVData.initStruct(typeIndex);
		scenarioData = cSVData;
		ArrayList<Long> lineIndexArray = new ArrayList<>();
		lineIndexArray.add(ptr);
		Integer indexTS = cSVData.getIndexFromName("timestamp");
		int indexOfTimeStamp = indexTS == null ? 0 : cSVData.getIndexFromName("timestamp");
		schedulableTasks = new ArrayList<>();
		i = 0;
		String line;
		try {
			while ((line = lines.get(lineIndex++)) != null && line.contains(DELIMITER)) {
				ptr += line.length() + 1;
				lineIndexArray.add(ptr);
				readTrame(line);
				Long timeStamp = (Long) ((CSVData) scenarioData).getData()[indexOfTimeStamp];
				if (timeStamp != null)
					schedulableTasks.add(new ScheduleTask(System.currentTimeMillis(), timeStamp, this, i++));
			}
		} catch (Exception e) {}
		int nbElement = lineIndexArray.size();
		this.lineIndex = new long[nbElement];
		for (i = 0; i < nbElement; i++)
			this.lineIndex[i] = lineIndexArray.get(i);
	}

	@Override
	public void initOrdo() {
		if (schedulableTasks == null)
			try {
				init(file);
			} catch (IOException e) {
				System.err.println("Error while reading file: " + e.getMessage());
			}
		if (schedulableTasks != null)
			schedulerInterface.addTasks(schedulableTasks, null);
	}


	@Override
	public void load(File scenarioFile, boolean backgroundLoading) throws IOException, ScenarioException {
		if (raf == null) {
			if (lineIndex == null)
				init(scenarioFile);
			raf = new RandomAccessFile(file, "r");
		}
		raf.seek(lineIndex[0]);
		byte[] lb = new byte[(int) (lineIndex[1] - lineIndex[0] - 1)];
		raf.readFully(lb);
		readTrame(new String(lb));
	}

	@Override
	public void populateInfo(LinkedHashMap<String, String> info) throws IOException {
		info.put("info", this.info);
		info.put("Number of Datas", Integer.toString(lineIndex.length - 1));
	}

	@Override
	public void process(Long timePointer) throws Exception {
		if (raf == null)
			return;
		long begin = lineIndex[timePointer.intValue()];
		raf.seek(begin);
		byte[] lb = new byte[(int) (lineIndex[(int) (timePointer + 1)] - begin - 1)];
		raf.readFully(lb);
		readTrame(new String(lb));
	}

	private void readTrame(String line) {
		CSVData cSVData = (CSVData) scenarioData;
		Object[] data = new Object[cSVData.getNbElements()];
		int i = 0;
		String[] element = line.split(DELIMITER);
		for (int j = 0; j < data.length; j++) {
			String value = element[j];
			if (value.startsWith("\"")) {
				value = value.substring(1);
				String val = "";
				j++;
				while (!val.endsWith("\"")) {
					value += " " + element[j];
					j++;
					val = element[j];
				}
				value += " " + element[j].substring(0, element[j].length() - 1);
			}
			data[i] = getValue(typeOfColumn[i++], value.trim());
		}
		cSVData.setData(data);
	}

	@Override
	public void save(File file) throws IOException {}
}
