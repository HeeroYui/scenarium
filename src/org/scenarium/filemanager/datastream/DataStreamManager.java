/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.datastream;

import java.awt.image.BufferedImage;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

import org.beanmanager.editors.PropertyEditor;
import org.beanmanager.editors.PropertyEditorManager;
import org.scenarium.filemanager.datastream.input.BufferedImageInputStream;
import org.scenarium.filemanager.datastream.input.ByteRasterInputStream;
import org.scenarium.filemanager.datastream.input.DataFlowInputStream;
import org.scenarium.filemanager.datastream.input.IntegerRasterInputStream;
import org.scenarium.filemanager.datastream.input.PropertyInputStream;
import org.scenarium.filemanager.datastream.input.ShortRasterInputStream;
import org.scenarium.filemanager.datastream.output.BufferedImageOutputStream;
import org.scenarium.filemanager.datastream.output.ByteRasterOutputStream;
import org.scenarium.filemanager.datastream.output.DataFlowOutputStream;
import org.scenarium.filemanager.datastream.output.DoubleRasterOutputStream;
import org.scenarium.filemanager.datastream.output.FloatRasterOutputStream;
import org.scenarium.filemanager.datastream.output.IntegerRasterOutputStream;
import org.scenarium.filemanager.datastream.output.PropertyOutputStream;
import org.scenarium.filemanager.datastream.output.ShortRasterOutputStream;
import org.scenarium.struct.raster.ByteRaster;
import org.scenarium.struct.raster.DoubleRaster;
import org.scenarium.struct.raster.FloatRaster;
import org.scenarium.struct.raster.IntegerRaster;
import org.scenarium.struct.raster.ShortRaster;

public final class DataStreamManager {
	private static final ConcurrentHashMap<Long, DataFlowInputMap> dataInputStreamMap = new ConcurrentHashMap<>();
	private static final ConcurrentHashMap<Long, DataFlowOutputMap> dataOutputStreamMap = new ConcurrentHashMap<>();
	private static final ConcurrentHashMap<Class<?>, Class<? extends DataFlowInputStream<?>>> inputStreamMap = new ConcurrentHashMap<>();
	private static final ConcurrentHashMap<Class<?>, Class<? extends DataFlowOutputStream<?>>> outputStreamMap = new ConcurrentHashMap<>();
	private static HashMap<Class<?>, Boolean> hasOutputStream = new HashMap<>();

	private DataStreamManager() {}

	static {
		registerDataStream(BufferedImage.class, BufferedImageInputStream.class, BufferedImageOutputStream.class);
		registerDataStream(ByteRaster.class, ByteRasterInputStream.class, ByteRasterOutputStream.class);
		registerDataStream(ShortRaster.class, ShortRasterInputStream.class, ShortRasterOutputStream.class);
		registerDataStream(IntegerRaster.class, IntegerRasterInputStream.class, IntegerRasterOutputStream.class);
		registerDataStream(FloatRaster.class, FloatRasterInputStream.class, FloatRasterOutputStream.class);
		registerDataStream(DoubleRaster.class, DoubleRasterInputStream.class, DoubleRasterOutputStream.class);
	}

	public static <T> DataFlowInputStream<T> getInputStream(Class<T> type) {
		DataFlowInputMap threadDataInputStreamMap = dataInputStreamMap.get(Thread.currentThread().getId());
		if (threadDataInputStreamMap == null) {
			threadDataInputStreamMap = new DataFlowInputMap();
			dataInputStreamMap.put(Thread.currentThread().getId(), threadDataInputStreamMap);
		}
		DataFlowInputStream<T> inputStream = threadDataInputStreamMap.get(type);
		if (inputStream != null)
			return inputStream;
		@SuppressWarnings("unchecked")
		Class<? extends DataFlowInputStream<T>> inputStreamClass = (Class<? extends DataFlowInputStream<T>>) inputStreamMap.get(type);
		if (inputStreamClass != null)
			try {
				inputStream = inputStreamClass.getConstructor().newInstance();
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
				return null;
			}
		if (inputStream == null) {
			DataFlowOutputMap threadDataOutputStreamMap = dataOutputStreamMap.get(Thread.currentThread().getId());
			if (threadDataOutputStreamMap == null) {
				threadDataOutputStreamMap = new DataFlowOutputMap();
				dataOutputStreamMap.put(Thread.currentThread().getId(), threadDataOutputStreamMap);
			}
			DataFlowOutputStream<T> dos = threadDataOutputStreamMap.get(type);
			if (dos != null && dos instanceof PropertyOutputStream)
				inputStream = new PropertyInputStream<>(((PropertyOutputStream<T>) dos).getEditor());
			else {
				PropertyEditor<T> pe = PropertyEditorManager.findEditor(type, "");
				if (pe != null)
					inputStream = new PropertyInputStream<>(pe);
			}
		}
		if (inputStream != null)
			threadDataInputStreamMap.put(type, inputStream);
		return inputStream;
	}

	public static <T> DataFlowOutputStream<T> getOutputStream(Class<T> type) {
		DataFlowOutputMap threadDataOutputStreamMap = dataOutputStreamMap.get(Thread.currentThread().getId());
		if (threadDataOutputStreamMap == null) {
			threadDataOutputStreamMap = new DataFlowOutputMap();
			dataOutputStreamMap.put(Thread.currentThread().getId(), threadDataOutputStreamMap);
		}
		DataFlowOutputStream<T> outputStream = threadDataOutputStreamMap.get(type);
		if (outputStream != null)
			return outputStream;
		@SuppressWarnings("unchecked")
		Class<? extends DataFlowOutputStream<T>> outputStreamClass = (Class<? extends DataFlowOutputStream<T>>) outputStreamMap.get(type);
		if (outputStreamClass != null)
			try {
				outputStream = outputStreamClass.getConstructor().newInstance();
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
				System.err.println("No default constructor for the DataFlowOutputStream class: " + outputStreamClass);
				return null;
			}
		if (outputStream == null) {
			DataFlowInputMap threadDataInputStreamMap = dataInputStreamMap.get(Thread.currentThread().getId());
			if (threadDataInputStreamMap == null) {
				threadDataInputStreamMap = new DataFlowInputMap();
				dataInputStreamMap.put(Thread.currentThread().getId(), threadDataInputStreamMap);
			}
			DataFlowInputStream<T> dis = threadDataInputStreamMap.get(type);
			if (dis != null && dis instanceof PropertyInputStream)
				outputStream = new PropertyOutputStream<>(((PropertyInputStream<T>) dis).getEditor());
			else {
				PropertyEditor<T> pe = PropertyEditorManager.findEditor(type, "");
				if (pe != null)
					outputStream = new PropertyOutputStream<>(pe);
			}
		}
		if (outputStream != null)
			threadDataOutputStreamMap.put(type, outputStream);
		return outputStream;
	}

	public static boolean hasOutputStream(Class<? extends Object> _class) {
		Boolean isProperty = hasOutputStream.get(_class);
		if (isProperty == null) {
			isProperty = DataStreamManager.getOutputStream(_class) != null;
			hasOutputStream.put(_class, isProperty);
		}
		return isProperty;
	}

	public static <T> void registerDataStream(Class<T> type, Class<? extends DataFlowInputStream<T>> inputStream, Class<? extends DataFlowOutputStream<T>> outputStream) {
		if (type == null || inputStream == null || outputStream == null)
			throw new IllegalArgumentException("Type, inputStream and outputStream cannot be null");
		inputStreamMap.put(type, inputStream);
		outputStreamMap.put(type, outputStream);
	}
	
	public static void unregisterDataStream(Class<?> type) {
		inputStreamMap.remove(type);
		outputStreamMap.remove(type);
	}
	
	public static void purgeDataStream(Module module) {
		inputStreamMap.keySet().removeIf(c -> c.getModule().equals(module));
		outputStreamMap.keySet().removeIf(c -> c.getModule().equals(module));
	}
}

class DataFlowInputMap {
	private final HashMap<Class<?>, DataFlowInputStream<?>> map = new HashMap<>();

	@SuppressWarnings("unchecked")
	public <T> DataFlowInputStream<T> get(Class<T> key) {
		return (DataFlowInputStream<T>) map.get(key);
	}

	public <T> void put(Class<T> key, DataFlowInputStream<T> editor) {
		map.put(key, editor);
	}
}

class DataFlowOutputMap {
	private final HashMap<Class<?>, DataFlowOutputStream<?>> map = new HashMap<>();

	@SuppressWarnings("unchecked")
	public <T> DataFlowOutputStream<T> get(Class<T> key) {
		return (DataFlowOutputStream<T>) map.get(key);
	}

	public <T> void put(Class<T> key, DataFlowOutputStream<T> editor) {
		map.put(key, editor);
	}
}