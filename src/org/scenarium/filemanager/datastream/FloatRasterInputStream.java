/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.datastream;

import java.io.IOException;
import java.nio.ByteBuffer;

import org.scenarium.filemanager.datastream.input.DataFlowInputStream;
import org.scenarium.struct.raster.FloatRaster;

public class FloatRasterInputStream extends DataFlowInputStream<FloatRaster> {
	private byte[] tempData;

	@Override
	public FloatRaster pop() throws IOException {
		FloatRaster rb = new FloatRaster(dataInput.readInt(), dataInput.readInt(), dataInput.readInt());
		if (tempData == null || tempData.length != rb.getSize() * Float.BYTES)
			tempData = new byte[rb.getSize() * Float.BYTES];
		dataInput.readFully(tempData);
		ByteBuffer.wrap(tempData).asFloatBuffer().put(rb.getData());
		return rb;
	}
}
