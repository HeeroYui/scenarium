/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.datastream.output;

import java.io.IOException;
import java.nio.ByteBuffer;

import org.scenarium.struct.raster.FloatRaster;

public class FloatRasterOutputStream extends DataFlowOutputStream<FloatRaster> {
	private byte[] tempData;

	@Override
	public void push(FloatRaster raster) throws IOException {
		dataOutput.writeInt(raster.getWidth());
		dataOutput.writeInt(raster.getHeight());
		dataOutput.writeInt(raster.getType());
		float[] pixels = raster.getData();
		if (tempData == null || tempData.length != pixels.length * Float.BYTES)
			tempData = new byte[pixels.length * Float.BYTES];
		ByteBuffer.wrap(tempData).asFloatBuffer().put(pixels);
		dataOutput.write(tempData);
	}
}
