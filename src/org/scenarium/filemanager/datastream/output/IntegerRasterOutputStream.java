/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.datastream.output;

import java.io.IOException;
import java.nio.ByteBuffer;

import org.scenarium.struct.raster.IntegerRaster;

public class IntegerRasterOutputStream extends DataFlowOutputStream<IntegerRaster> {
	private byte[] tempData;

	@Override
	public void push(IntegerRaster raster) throws IOException {
		dataOutput.writeInt(raster.getWidth());
		dataOutput.writeInt(raster.getHeight());
		dataOutput.writeInt(raster.getType());
		int[] pixels = raster.getData();
		if (tempData == null || tempData.length != pixels.length * Integer.BYTES)
			tempData = new byte[pixels.length * Integer.BYTES];
		ByteBuffer.wrap(tempData).asIntBuffer().put(pixels);
		dataOutput.write(tempData);
	}
}
