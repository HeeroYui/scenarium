/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.datastream.input;

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferDouble;
import java.awt.image.DataBufferFloat;
import java.awt.image.DataBufferInt;
import java.awt.image.DataBufferShort;
import java.awt.image.DataBufferUShort;
import java.io.DataInput;
import java.io.IOException;
import java.nio.ByteBuffer;

public class BufferedImageInputStream extends DataFlowInputStream<BufferedImage> {
	private byte[] tempData;

	@Override
	public BufferedImage pop() throws IOException {
		BufferedImage bi = buildObject(dataInput);
		DataBuffer db = bi.getRaster().getDataBuffer();
		if (db instanceof DataBufferByte)
			dataInput.readFully(((DataBufferByte) db).getData());
		else if (db instanceof DataBufferShort) {
			short[] pixels = ((DataBufferShort) db).getData();
			if (tempData == null || tempData.length != pixels.length * Short.BYTES)
				tempData = new byte[pixels.length * Short.BYTES];
			dataInput.readFully(tempData);
			ByteBuffer.wrap(tempData).asShortBuffer().get(pixels);
		} else if (db instanceof DataBufferUShort) {
			short[] pixels = ((DataBufferUShort) db).getData();
			if (tempData == null || tempData.length != pixels.length * Short.BYTES)
				tempData = new byte[pixels.length * Short.BYTES];
			dataInput.readFully(tempData);
			ByteBuffer.wrap(tempData).asShortBuffer().get(pixels);
		} else if (db instanceof DataBufferDouble) {
			double[] pixels = ((DataBufferDouble) db).getData();
			if (tempData == null || tempData.length != pixels.length * Double.BYTES)
				tempData = new byte[pixels.length * Double.BYTES];
			dataInput.readFully(tempData);
			ByteBuffer.wrap(tempData).asDoubleBuffer().get(pixels);
		} else if (db instanceof DataBufferFloat) {
			float[] pixels = ((DataBufferFloat) db).getData();
			if (tempData == null || tempData.length != pixels.length * Float.BYTES)
				tempData = new byte[pixels.length * Float.BYTES];
			dataInput.readFully(tempData);
			ByteBuffer.wrap(tempData).asFloatBuffer().get(pixels);
		} else if (db instanceof DataBufferInt) {
			int[] pixels = ((DataBufferInt) db).getData();
			if (tempData == null || tempData.length != pixels.length * Integer.BYTES)
				tempData = new byte[pixels.length * Integer.BYTES];
			dataInput.readFully(tempData);
			ByteBuffer.wrap(tempData).asIntBuffer().get(pixels);
		} else
			throw new IllegalArgumentException("Image with an DataBuffer: " + db.getClass() + " is not supported");
		return bi;
	}

	protected BufferedImage buildObject(DataInput dataInput) throws IOException {
		return new BufferedImage(dataInput.readInt(), dataInput.readInt(), dataInput.readInt());
	}

}
