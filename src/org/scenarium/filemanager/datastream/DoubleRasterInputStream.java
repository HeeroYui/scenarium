/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.datastream;

import java.io.IOException;
import java.nio.ByteBuffer;

import org.scenarium.filemanager.datastream.input.DataFlowInputStream;
import org.scenarium.struct.raster.DoubleRaster;

public class DoubleRasterInputStream extends DataFlowInputStream<DoubleRaster> {
	private byte[] tempData;

	@Override
	public DoubleRaster pop() throws IOException {
		DoubleRaster rb = new DoubleRaster(dataInput.readInt(), dataInput.readInt(), dataInput.readInt());
		if (tempData == null || tempData.length != rb.getSize() * Double.BYTES)
			tempData = new byte[rb.getSize() * Double.BYTES];
		dataInput.readFully(tempData);
		ByteBuffer.wrap(tempData).asDoubleBuffer().put(rb.getData());
		return rb;
	}
}
