/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.function.Supplier;

import javax.swing.event.EventListenerList;

import org.beanmanager.BeanManager;
import org.scenarium.MainApp;
import org.scenarium.Scenarium;
import org.scenarium.display.LanguageManager;
import org.scenarium.filemanager.playlist.PlayListManager;
import org.scenarium.filemanager.scenariomanager.Scenario;
import org.scenarium.filemanager.scenariomanager.ScenarioDescriptor;
import org.scenarium.filemanager.scenariomanager.ScenarioException;
import org.scenarium.filemanager.scenariomanager.ScenarioManager;
import org.scenarium.struct.ScenariumProperties;

import javafx.application.Application;
import javafx.beans.property.ReadOnlyDoubleProperty;

public class DataLoader {
	public static final int SCENARIO = 0;
	public static final int PLAYLIST = 1;
	public static final int UNSUPPORTED = 2;
	private static final List<Supplier<Boolean>> backgroundLoadingTask = Collections.synchronizedList(new ArrayList<Supplier<Boolean>>());

	public static void addToBackgroundloadingtask(Supplier<Boolean> backgroundTask) {
		backgroundLoadingTask.add(backgroundTask);
	}

	public static int getFileType(File file) {
		String fileName = file.getName();
		String fileExt = fileName.substring(fileName.lastIndexOf(".") + 1, file.getName().length());
		if (ScenarioManager.getScenarioType(file) != null)
			return DataLoader.SCENARIO;
		else if (fileExt.equals(PlayListManager.ext))
			return DataLoader.PLAYLIST;
		return DataLoader.UNSUPPORTED;
	}

	private File scenariumConfigFile;
	private Object reloadLock;
	public PlayListManager playListManager = new PlayListManager();
	public Integer currentIndexInRecord = 0;
	public int recordCount;
	public HashMap<String, String> language = new HashMap<>();
	private MainApp mainApp;
	private final EventListenerList listeners = new EventListenerList();
	private Thread synchroThread;

	private Scenario scenario;

	public final List<Object> recentScenario = Collections.synchronizedList(new ArrayList<>());

	public DataLoader(MainApp mainapp) {
		this.scenariumConfigFile = new File(Scenarium.getLocalDir() + File.separator + "ScenariumConfig.txt");
		this.mainApp = mainapp;
		this.reloadLock = new Object();
	}

	public void addOpenListener(OpenListener listener) {
		listeners.add(OpenListener.class, listener);
	}

	private void fireOpened(Scenario oldScenario) {
		for (OpenListener listener : listeners.getListeners(OpenListener.class))
			listener.opened(oldScenario, scenario);
	}

	public List<Object> getRecentScenario() {
		return recentScenario;
	}

	public Scenario getScenario() {
		return scenario;
	}

	public boolean goToNextScenario(boolean reverse) {
		Object newScenario = playListManager.goToNextScenario(scenario.getSource(), reverse);
		if (newScenario == null)
			return false;
		return reload(newScenario, false, false); // Implique un close de l'ancien diagram
	}

	public Object loadSofwareConfig() throws IOException {
		if (!scenariumConfigFile.exists()) {
			LanguageManager.loadDefaultLanguage();
			throw new IOException();
		}
		ScenariumProperties pp = ScenariumProperties.get();
		new BeanManager(pp, Scenarium.getLocalDir()).load(scenariumConfigFile);
		ArrayList<Object> _recentScenario = new ArrayList<>();
		if (pp.getRecentScenarios() != null)
			for (ScenarioDescriptor rs : pp.getRecentScenarios())
				if (rs != null)
					_recentScenario.add(ScenarioManager.getSource(rs.sourceType, rs.scenarioPath));
		recentScenario.addAll(_recentScenario);
		Object scenarioSource = null;
		if (pp.getPlayList() != null) {
			for (ScenarioDescriptor ple : pp.getPlayList())
				if (ple != null)
					playListManager.addToPlayList(-1, ScenarioManager.getSource(ple.sourceType, ple.scenarioPath));
			try {
				scenarioSource = playListManager.getPlayList().get(pp.getPlayListIndex());
			} catch (IndexOutOfBoundsException e) {}
		}
		String look = pp.getLookAndFeel();
		String[] lookAndFeels = new String[] { Application.STYLESHEET_MODENA, Application.STYLESHEET_CASPIAN };
		for (String lookAndFeel : lookAndFeels)
			if (lookAndFeel.equals(look)) {
				if (!Application.STYLESHEET_MODENA.equals(look))
					Application.setUserAgentStylesheet(look);
				break;
			}
		if (pp.getLanguage() == null || pp.getLanguage().isEmpty() || !LanguageManager.loadLanguage(pp.getLanguage()))
			LanguageManager.loadDefaultLanguage();
		BeanManager.isHiddenFieldVisible = pp.isHiddenFieldVisible();
		BeanManager.isExpertFieldVisible = pp.isExpertFieldVisible();
		return scenarioSource;
	}

	public boolean reload(Object scenarioSource, boolean resetPlayList, boolean backgroundLoading) {
		synchronized (reloadLock) {
			try {
				secureLoad(scenarioSource, resetPlayList, backgroundLoading);
				ReadOnlyDoubleProperty progressProperty = scenario.getProgressProperty();
				if (progressProperty != null)
					mainApp.loadingOperation(progressProperty);
				return true;
			} catch (LoadingException e) {
				System.err.println("Cannot load the scenario: " + scenarioSource + "\nCause: " + e.getMessage());
				return false;
			}
		}
	}

	// @SuppressWarnings("unchecked")
	// public DataLoader clone(Scenario scenario) {
	// DataLoader dataLoader = new DataLoader(mainApp, reloadLock);
	// dataLoader.scenario = scenario;
	// dataLoader.recentScenario.addAll(recentScenario);
	// dataLoader.language = (HashMap<String, String>) language.clone();
	// return dataLoader;
	// }

	public void removeOpenListener(OpenListener listener) {
		listeners.remove(OpenListener.class, listener);
	}

	public void save(File scenarioFile) throws IOException {
		boolean sync = ScenariumProperties.get().isSynchronization();
		if (sync)
			setSynchronized(false);
		System.out.println("save scenario: " + scenarioFile);
		scenario.save(scenarioFile);
		if (sync)
			setSynchronized(true);
	}

	public void saveSoftwareConfig() {
		ScenariumProperties pp = ScenariumProperties.get();
		int i = 0;
		ScenarioDescriptor[] recentScenarios = new ScenarioDescriptor[recentScenario.size()];
		for (Object rs : recentScenario)
			if (rs != null)
				recentScenarios[i++] = ScenarioManager.getDescriptor(rs);
		pp.setRecentScenarios(recentScenarios);
		i = 0;
		ArrayList<Object> pl = playListManager.getPlayList();
		ScenarioDescriptor[] playList = new ScenarioDescriptor[pl.size()];
		for (Object sc : pl) {
			if (sc == null)
				continue;
			if (scenario != null && pl.equals(scenario.getSource()))
				pp.setPlayListIndex(i);
			playList[i++] = ScenarioManager.getDescriptor(sc);
		}
		pp.setPlayList(playList);
		pp.setHiddenFieldVisible(BeanManager.isHiddenFieldVisible);
		pp.setExpertFieldVisible(BeanManager.isExpertFieldVisible);
		new BeanManager(pp, Scenarium.getLocalDir()).save(scenariumConfigFile, false);
	}

	public void secureLoad(Object scenarioSource, boolean resetPlayList, boolean backgroundLoading) throws LoadingException {
		if (scenarioSource == null)
			throw new LoadingException("Scenario source is null");
		if (!(scenarioSource instanceof Class<?>) && !ScenarioManager.isAvailable(scenarioSource))
			throw new LoadingException("Cannot find the scenario : " + ScenarioManager.getPath(scenarioSource));
		try {
			Scenario newScenario = ScenarioManager.createScenario(scenarioSource);
			if (newScenario == null)
				throw new LoadingException("Cannot find scenario manager for the kind of scenario: " + scenarioSource.getClass());
			// StreamRecorder sr = null;
			if (scenario != null) {
				scenario.synchroClose(); // synchroClose puis setScheduler null sinon je me désinscrit pas dans scheduler
				scenario.setScheduler(null);
				scenario.synchroClose();
				mainApp.removeToSchedule(scenario);
				// sr = scenario.getStreamRecorder();
			}
			Scenario oldScenario = scenario;
			scenario = newScenario;
			// StreamRecorder _sr = sr;
			newScenario.addLoadListener(() -> {
				if (ScenariumProperties.get().isSynchronization())
					updateSynchro();
				mainApp.addToScheduleAndShow(scenario);
				// newScenario.setStreamRecorder(_sr);
				fireOpened(oldScenario);
			});
			newScenario.load(scenarioSource instanceof Class<?> ? null : scenarioSource, backgroundLoading);
			scenarioSource = scenario.getSource();
			if (scenarioSource != null) {
				if (recentScenario.contains(scenarioSource))
					recentScenario.remove(scenarioSource);
				recentScenario.add(0, scenarioSource);
			}
			while (recentScenario.size() > 20)
				recentScenario.remove(recentScenario.size() - 1);
			if (resetPlayList)
				playListManager.reset(scenarioSource);
		} catch (IOException | OutOfMemoryError | ScenarioException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
			throw new LoadingException(ScenarioManager.getErrorMessage(e));

		}
		Iterator<Supplier<Boolean>> it = backgroundLoadingTask.iterator();
		while (it.hasNext()) {
			Boolean needToRemove = it.next().get();
			if (needToRemove != null && needToRemove == true)
				it.remove();
		}
	}

	public void setSynchronized(boolean synchro) {
		ScenariumProperties scenariumProperties = ScenariumProperties.get();
		if (scenariumProperties.isSynchronization() == synchro)
			return;
		if (synchro)
			updateSynchro();
		else
			synchroThread.interrupt();
		scenariumProperties.setSynchronization(synchro);
	}

	private void updateSynchro() {
		if (synchroThread != null)
			synchroThread.interrupt();
		if (scenario == null || !(scenario.getSource() instanceof File))
			return;
		synchroThread = new Thread(new Runnable() {
			private long timer;

			@Override
			public void run() {
				WatchKey watckKey = null;
				try {
					File scenarioFile = (File) scenario.getSource();
					Path myDir = Paths.get(scenarioFile.getParent());
					WatchService watcher = myDir.getFileSystem().newWatchService();
					myDir.register(watcher, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_MODIFY);
					timer = System.currentTimeMillis();
					while (true) {
						watckKey = watcher.take();
						for (WatchEvent<?> event : watckKey.pollEvents())
							if (scenarioFile.getName().equals(event.context().toString()) && System.currentTimeMillis() - timer > 100) {
								Thread.sleep(100);
								timer = System.currentTimeMillis();
								new Thread(() -> reload(scenarioFile, false, true)).start();
								return;
							}
						watckKey.reset();
					}
				} catch (InterruptedException e) {
					if (watckKey != null)
						watckKey.cancel();
				} catch (IOException e) {
					System.err.println("Cannot synchronized the scenario: " + e.toString());
				}
			}
		});
		synchroThread.start();
	}
//
//	public void unregisterExternModule(Module module) {
//		mainApp.unregisterExternModule(module);
//	}
//
//	public void registerExternModules(Path moduleFile) {
//		mainApp.registerExternModules(moduleFile);
//	}
//
//	public boolean isInternModule(Module module) {
//		return mainApp.isInternModule(module);
//	}
//
//	public String getModulePath(Module module) {
//		return mainApp.getModulePath(module);
//	}
}
