/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenariomanager;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.concurrent.Callable;

import javax.swing.event.EventListenerList;

import org.beanmanager.BeanPropertiesInheritanceLimit;
import org.beanmanager.editors.TransientProperty;
import org.scenarium.display.drawer.TheaterPanel;
import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;
import org.scenarium.struct.BufferedStrategy;
import org.scenarium.timescheduler.Schedulable;
import org.scenarium.timescheduler.SchedulerInterface;

import javafx.beans.InvalidationListener;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.collections.ObservableList;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.StrokeLineCap;
import javafx.scene.shape.StrokeLineJoin;

@BeanPropertiesInheritanceLimit
public abstract class Scenario extends EvolvedOperator implements Schedulable {
	public static String MINUTEPATTERN = "ss.SSS";
	public static String HOURPATTERN = "mm:ss.SSS";
	public static String DAYPATTERN = "HH:mm:ss.SSS";
	public static String DATEPATTERN = "yyyy-MM-dd HH:mm:ss.SSS";
	public static final int LOADED = 0;
	public static final int SCHEDULED = 1;
	protected ReadOnlyDoubleProperty progressProperty;
	private final EventListenerList listeners = new EventListenerList();
	protected Object scenarioData;
	@TransientProperty
	protected SchedulerInterface schedulerInterface;
	private TheaterPanel theaterPanel;
	private Polygon polygon;

	public void addLoadListener(LoadListener listener) {
		listeners.add(LoadListener.class, listener);
	}

	public void addScheduleChangeListener(ScheduleChangeListener listener) {
		listeners.add(ScheduleChangeListener.class, listener);
	}

	public void addSourceChangeListener(SourceChangeListener listener) {
		for (Object list : listeners.getListenerList())
			if (listener == list)
				return;
		listeners.add(SourceChangeListener.class, listener);
	}

	public void addSourceChangeListenerIfNotPresent(SourceChangeListener listener) {
		for (Object list : listeners.getListenerList())
			if (listener == list)
				return;
		addSourceChangeListener(listener);
	}

	public boolean canModulateSpeed() {
		return true;
	}

	public boolean canReverse() {
		return true;
	}

	protected abstract boolean close();

	@Override
	public void death() {
		close();
		scenarioData = null;
	}

	protected void fireLoadChanged() {
		for (LoadListener listener : listeners.getListeners(LoadListener.class))
			listener.scenarioLoaded();
	}

	protected void fireScheduleChanged() {
		for (ScheduleChangeListener listener : listeners.getListeners(ScheduleChangeListener.class))
			listener.scheduleChange();
	}

	protected void fireSourceChanged() {
		for (SourceChangeListener listener : listeners.getListeners(SourceChangeListener.class))
			listener.sourceChanged();
	}

	public abstract long getBeginningTime();

	public abstract long getEndTime();

	public abstract Date getStartTime();

	public abstract Date getStopTime();

	public abstract Class<?> getDataType();

	public LinkedHashMap<String, String> getInfo() throws IOException {
		LinkedHashMap<String, String> info = new LinkedHashMap<>();
		getInfo(info);
		return info;
	}

	public abstract void getInfo(LinkedHashMap<String, String> info) throws IOException;

	@Override
	public Region getNode() {
		polygon = new Polygon();
		polygon.setFill(Color.TRANSPARENT);
		polygon.setStrokeLineCap(StrokeLineCap.ROUND);
		polygon.setStrokeLineJoin(StrokeLineJoin.ROUND);
		StackPane sp = new StackPane(polygon);
		sp.setMouseTransparent(true);
		sp.setFocusTraversable(false);
		InvalidationListener il = e -> updatePolyPoints(polygon, sp.getWidth(), sp.getHeight());
		sp.heightProperty().addListener(il);
		sp.widthProperty().addListener(il);
		updateColor();
		return sp;
	}

	protected void updateColor() {
		if (polygon != null) {
			Color color = getColor();
			Stop[] stops = new Stop[] { new Stop(0, new Color(color.getRed(), color.getGreen(), color.getBlue(), 130 / 255.0)),
					new Stop(1, new Color(color.getRed(), color.getGreen(), color.getBlue(), 30 / 255.0)) };
			LinearGradient lg = new LinearGradient(0, 0, 1, 0, true, CycleMethod.NO_CYCLE, stops);
			polygon.setStroke(lg);
		}
	}

	protected Color getColor() {
		return new Color(0, 0.7, 0, 0);
	}

	public ReadOnlyDoubleProperty getProgressProperty() {
		return progressProperty;
	}

	protected abstract String[] getReaderFormatNames();

	public Object getScenarioData() {
		return scenarioData;
	}

	public SchedulerInterface getSchedulerInterface() {
		return schedulerInterface;
	}

	public abstract int getSchedulerType();

	public abstract Object getSource();

	public Schedulable getTaskFromId(int id) {
		return this;
	}

	public int getTaskId(Schedulable task) {
		return 0;
	}

	public abstract void initOrdo();

	@Override
	public void initStruct() throws Exception {
		Class<?> type = getDataType();
		if (type != null)
			updateOutputs(new String[] { type.getSimpleName() }, new Class<?>[] { type });
		else
			updateOutputs(new String[] {}, new Class<?>[] {});
	}

	public boolean isPaintableProperties() {
		return false;
	}

	public boolean isStreamScenario() {
		return this instanceof StreamScenario;
	}

	// protected abstract boolean isTimeRepresentation();

	public abstract void load(Object scenarioSource, boolean backgroundLoading) throws IOException, ScenarioException;

	public abstract void populateInfo(LinkedHashMap<String, String> info) throws IOException;

	public abstract void process(Long timePointer) throws Exception;

	protected void reload() {
		try {
			synchroLoad(getSource(), false);
			// fireLoaded();
		} catch (IOException | ScenarioException e) {
			e.printStackTrace();
		}
	}

	public void removeLoadListener(LoadListener listener) {
		listeners.remove(LoadListener.class, listener);
	}

	public void removeScheduleChangeListener(ScheduleChangeListener listener) {
		listeners.remove(ScheduleChangeListener.class, listener);
	}

	public void removeSourceChangeListener(SourceChangeListener listener) {
		listeners.remove(SourceChangeListener.class, listener);
	}

	public abstract void save(File file) throws IOException;

	public void setScheduler(SchedulerInterface schedulerInterface) {
		this.schedulerInterface = schedulerInterface;
		if (schedulerInterface != null)
			initOrdo();
	}

	public abstract void setSource_(Object source);

	// public Scheduler getScheduler() {
	// return scheduler;
	// }

	public void setTheaterPanel(TheaterPanel theaterPanel) {
		this.theaterPanel = theaterPanel;
	}

	public boolean stopAndWait() {
		return schedulerInterface.stopAndWait();
	}

	public void synchroClose() {
		Callable<Boolean> closeTask = () -> {
			synchronized (this) {
				return close();
			}
		};
		try {
			if (schedulerInterface != null) {
				schedulerInterface.synchronisedCall(closeTask); // Il ne faut pas que le scheduler s'arrête pendant le rechargement
				schedulerInterface.removeScheduleElement(this);
			} else
				closeTask.call();
		} catch (Exception e) {} // Impossible normalement

	}

	public void synchroLoad(Object scenarioSource, boolean backgroundLoading) throws IOException, ScenarioException {
		Callable<Boolean> loadTask = () -> {
			synchronized (this) {
				load(scenarioSource, backgroundLoading);
				return true;
			}
		};
		try {
			if (schedulerInterface != null)
				schedulerInterface.synchronisedCall(loadTask);
			else
				loadTask.call();
		} catch (IOException | ScenarioException e) {
			throw e;
		} catch (Exception e) {} // Impossible normalement

	}

	private boolean synchroUpdate(long timePointer) {
		Object oldScenarioData = scenarioData;
		try {
			process(timePointer);
			if (scenarioData instanceof BufferedStrategy)
				((BufferedStrategy<?>) scenarioData).flip();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		if (theaterPanel != null) {
			if (oldScenarioData != scenarioData)
				theaterPanel.setDrawableElement(scenarioData);
			theaterPanel.repaint(true);
		}
		return true;
	}

	@Override
	synchronized public void update(long timePointer) {
		if (scenarioData == null) {
			System.err.println("no scenario data for " + this);
			return;
		}
		boolean success;
		if (scenarioData instanceof BufferedStrategy<?>) {
			BufferedStrategy<?> rasterStrategy = (BufferedStrategy<?>) scenarioData;
			if (rasterStrategy.isPageFlipping())
				success = synchroUpdate(timePointer);
			else
				synchronized (rasterStrategy.getDrawElement()) {
					success = synchroUpdate(timePointer);
				}
		} else
			synchronized (scenarioData) {
				success = synchroUpdate(timePointer);
			}
		if (success && isRunning())
			triggerOutput(new Object[] { scenarioData instanceof BufferedStrategy<?> ? ((BufferedStrategy<?>) scenarioData).getDrawElement() : scenarioData }, schedulerInterface.getTimeStamp());
	}

	private static void updatePolyPoints(Polygon polygon, double width, double height) {
		ObservableList<Double> points = polygon.getPoints();
		double gapx = 0.85f;
		double gapy = 0.95f;
		double roundSize = width / 4;
		double minX = (1 - gapx) * width + roundSize;
		points.clear();
		points.addAll(minX, (1 - gapy) * width + roundSize, width - roundSize, width / 2, minX, gapy * width - roundSize);
		polygon.setStrokeWidth(roundSize);
	}

	public String getTimePattern() {
		LocalDateTime endDate = Instant.ofEpochMilli(getEndTime()).atZone(ZoneId.systemDefault()).toLocalDateTime();
		LocalDateTime beginningDate = Instant.ofEpochMilli(getBeginningTime()).atZone(ZoneId.systemDefault()).toLocalDateTime();
		if(!endDate.truncatedTo(ChronoUnit.DAYS).equals(beginningDate.truncatedTo(ChronoUnit.DAYS)))
			return DATEPATTERN;
		return getEndTime() < 1000*60 ? MINUTEPATTERN : getEndTime() < 1000*60 * 60 ? HOURPATTERN : DAYPATTERN;
	}

	public boolean canRecord() {
		return false;
	}

	public boolean isRecording() {
		return false;
	}

	public void setRecording(boolean recording) {}

	public void addRecordingListener(RecordingListener listener) {
		listeners.add(RecordingListener.class, listener);
	}

	public void removeRecordingListener(RecordingListener listener) {
		listeners.remove(RecordingListener.class, listener);
	}

	protected void fireRecordingChanged(boolean recording) {
		for (RecordingListener listener : listeners.getListeners(RecordingListener.class))
			listener.recordingPropertyChanged(recording);
	}
}
