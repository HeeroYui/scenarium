/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenariomanager;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.beanmanager.editors.basic.AbstractPathEditor;
import org.scenarium.filemanager.scenario.DataFlowDiagram;
import org.scenarium.struct.ScenariumProperties;

import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Window;

public class ScenarioFileChooserFx {

	private static FileChooser baseFileChooser(Window window, ArrayList<ExtensionFilter> extensionFilters, File path) {
		FileChooser fc = new FileChooser();
		fc.setTitle("Scenario file chooser");
		File cp = ScenariumProperties.get().getScenarioChooserPath();
		fc.setInitialDirectory(path != null && path.exists() ? path : cp != null && cp.exists() ? cp : new File(System.getProperty("user.dir")));
		fc.getExtensionFilters().setAll(extensionFilters != null ? extensionFilters : AbstractPathEditor.getExtensionFilters(ScenarioManager.getReaderFormatNames()));
		fc.getExtensionFilters().sort((a, b) -> a.getDescription().compareTo(b.getDescription()));
		for (ExtensionFilter extensionFilter : fc.getExtensionFilters())
			if(extensionFilter.getDescription().equals(DataFlowDiagram.class.getSimpleName())) {
				fc.setSelectedExtensionFilter(extensionFilter);
				break;
			}
		return fc;
	}

	public static File showOpenDialog(Window window, File path) {
		return baseFileChooser(window, null, path).showOpenDialog(window);
	}

	public static List<File> showOpenMultipleDialog(Window window, File path) {
		return baseFileChooser(window, null, path).showOpenMultipleDialog(window);
	}

	public static File showSaveDialog(Window window, Scenario scenario, File path) {
		File file = baseFileChooser(window, AbstractPathEditor.getExtensionFilters(((LocalScenario)scenario).getFilters()), path).showSaveDialog(window);
		if (file == null)
			return null;
		String[] rfn = scenario.getReaderFormatNames();
		if (rfn.length != 0) {
			String extension = scenario.getReaderFormatNames()[0];
			if (!extension.isEmpty() && !file.getName().toLowerCase().endsWith("." + extension))
				file = new File(file.getAbsoluteFile() + "." + extension);
		}
		ScenariumProperties.get().setScenarioChooserPath(file.getParentFile());
		return file;
	}
}
