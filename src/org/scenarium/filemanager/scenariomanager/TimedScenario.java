/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.scenariomanager;

import java.io.File;
import java.util.Date;

import org.beanmanager.editors.PropertyInfo;
import org.beanmanager.editors.primitive.number.NumberInfo;
import org.beanmanager.editors.time.DynamicDateInfo;
import org.scenarium.editors.NotChangeableAtRuntime;

public abstract class TimedScenario extends LocalScenario {
	@PropertyInfo(index = 1)
	@NumberInfo(min = 1)
	@NotChangeableAtRuntime
	private int period = 40;
	@PropertyInfo(index = 2)
	@NotChangeableAtRuntime
	@DynamicDateInfo(minMethodName = "getMinStartTime", maxMethodName = "getMaxStartTime", timePatternMethodName = "getTimePattern")
	private Long startTime = null;
	@PropertyInfo(index = 3)
	@DynamicDateInfo(minMethodName = "getMinStopTime", maxMethodName = "getMaxStopTime", timePatternMethodName = "getTimePattern")
	@NotChangeableAtRuntime
	private Long stopTime = null;

	private long testedStartTime = Long.MIN_VALUE;
	private long testedStopTime = Long.MAX_VALUE;

	@Override
	public void setFile(File file) {
		startTime = null;
		stopTime = null;
		testedStartTime = Long.MIN_VALUE;
		testedStopTime = Long.MAX_VALUE;
		super.setFile(file);
	}

	public int getPeriod() {
		return period;
	}

	public void setPeriod(int period) {
		if (this.period != period) {
			if (period < 1)
				throw new IllegalArgumentException("The period must be strictly positive");
			this.period = period;
			firePeriodChanged();
		}
	}

	private void firePeriodChanged() {
		for (PeriodListener listener : listeners.getListeners(PeriodListener.class))
			listener.periodChanged(period);
	}

	public Date getStartTime() {
		return startTime == null ? null : new Date(startTime);
	}

	public Long getMinStartTime() {
		long time = getBeginningTime();
		return time == -1 ? Long.MIN_VALUE : time;
	}

	public Long getMaxStartTime() {
		long time = stopTime == null ? getEndTime() : Math.min(getEndTime(), stopTime);
		return time == -1 ? Long.MAX_VALUE : time;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime == null ? null : startTime.getTime();
		testedStartTime = startTime == null ? Long.MIN_VALUE : startTime.getTime();
		fireStartStopTimeChanged();
		fireAnnotationChanged(this, "stopTime");
	}

	public Date getStopTime() {
		return stopTime == null ? null : new Date(stopTime);
	}

	public Long getMinStopTime() {
		long time = startTime == null ? getBeginningTime() : Math.max(getBeginningTime(), startTime);
		return time == -1 ? Long.MIN_VALUE : time;
	}

	public Long getMaxStopTime() {
		long time = getEndTime();
		return time == -1 ? Long.MAX_VALUE : time;
	}

	public void setStopTime(Date stopTime) {
		this.stopTime = stopTime == null ? null : stopTime.getTime();
		testedStopTime = stopTime == null ? Long.MAX_VALUE : stopTime.getTime();
		fireAnnotationChanged(this, "startTime");
		fireStartStopTimeChanged();
	}

	@Override
	public boolean canTrigger(long time) {
		if (!(time >= testedStartTime && time <= testedStopTime))
			System.err.println("error");
		return time >= testedStartTime && time <= testedStopTime;
	}
}
