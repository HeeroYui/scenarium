/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.playlist;

import org.scenarium.filemanager.scenariomanager.ScenarioManager;

public class ScenarioDesc {
	public final Object scenarioSource;
	public final boolean isAvailable;
	public final int lenght;

	public ScenarioDesc(Object scenarioSource) {
		this.scenarioSource = scenarioSource;
		isAvailable = ScenarioManager.isAvailable(scenarioSource);
		lenght = ScenarioManager.getLenght(scenarioSource);
	}
	// Retirer car bug playlist fx sinon, mais je sais pas pk il �tait la...
	// public boolean equals(Object e) {
	// return e instanceof ScenarioDesc && ((ScenarioDesc) e).scenarioSource.equals(scenarioSource);
	// }

	@Override
	public String toString() {
		return scenarioSource.toString();
	}
}
