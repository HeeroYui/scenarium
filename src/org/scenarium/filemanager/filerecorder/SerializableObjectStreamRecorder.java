/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.filerecorder;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.channels.FileChannel;

import org.beanmanager.BeanManager;

//TODO toujouts utile pour les objets uniquement serializable
public class SerializableObjectStreamRecorder extends FileStreamRecorder {
	private ObjectOutputStream oos;
	private DataOutputStream pitchDos;
	private DataOutputStream dataDos;
	private File file;
	private FileChannel channel;

	public SerializableObjectStreamRecorder() {}

	@Override
	public void close() throws IOException {
		IOException exception = null;
		if (pitchDos != null)
			try {
				pitchDos.close();
			} catch (IOException e) {
				e.printStackTrace();
				exception = e;
			}
		if (oos != null)
			try {
				oos.close();
			} catch (IOException e) {
				e.printStackTrace();
				exception = e;
			}
		channel = null;
		if (exception != null)
			throw exception;
	}

	@Override
	public File getFile() {
		if (file == null && recordPath != null)
			file = new File(recordPath.endsWith(".ppsr") ? recordPath : recordPath + File.separator + recordName + ".ppd");
		return file;
	}

	@Override
	public void pop(Object value) throws IOException, FileNotFoundException {
		if (oos == null) {
			file = getFile();
			String filePath = file.getAbsolutePath().substring(0, file.getAbsolutePath().length() - 4);
			FileOutputStream fos = new FileOutputStream(file);
			channel = fos.getChannel();
			dataDos = new DataOutputStream(fos);
			// StringEditor.writeString(dataDos, value.getClass().getName()); //TODO je dois faire ca normalement...
			dataDos.writeBytes(BeanManager.getDescriptorFromClass(value.getClass()) + System.lineSeparator());
			oos = new ObjectOutputStream(dataDos);
			pitchDos = new DataOutputStream(new FileOutputStream(new File(filePath + ".pindex")));
		}
		if (pitchDos != null)
			pitchDos.writeLong(channel.position());		//dataDos.size() marche, pas, il est en int...
		oos.writeUnshared(value);
		oos.reset();
		timePointer++;
	}
}
