/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.filerecorder;

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferUShort;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import org.scenarium.operator.image.conversion.TypeConverter;
import org.scenarium.struct.YUVColorSpace;
import org.scenarium.struct.raster.ByteRaster;
import org.scenarium.struct.raster.Raster;
import org.scenarium.struct.raster.ShortRaster;

public class RawStreamRecorder extends FileStreamRecorder {
	private FileOutputStream fos;
	private String rawPath;
	private int width;
	private int height;
	private String stringType;
	private int size;
	private int depth;
	private File file;

	@Override
	public void close() throws IOException {
		if (fos != null) {
			fos.flush();
			fos.close();
			fos = null;
			writeHeader(false);
		}
	}

	@Override
	public File getFile() {
		return getInfFile();
	}
	
	public File getInfFile() {
		if (file == null && recordPath != null)
			file = new File(recordPath.endsWith(".inf") ? recordPath : recordPath.endsWith(".raw") ? recordPath.substring(0, recordPath.length() - 3) + "inf" : recordPath + File.separator + recordName + ".inf");
		return file;
	}

	protected void initStream(int width, int height, String stringType, int size, int depth) throws IOException {
		File infFile = getFile();
		String filePath = infFile.getAbsolutePath();
		rawPath = filePath.substring(0, filePath.lastIndexOf(".")).concat(".raw");
		File f = new File(rawPath);
		f.getParentFile().mkdirs();
		fos = new FileOutputStream(f);
		this.width = width;
		this.height = height;
		this.stringType = stringType;
		this.size = size;
		this.depth = depth;
		writeHeader(true);
	}

	@Override
	public void pop(Object object) throws IOException {
		int type = object instanceof ByteRaster ? 0 : object instanceof BufferedImage && ((BufferedImage) object).getData().getDataBuffer() instanceof DataBufferByte ? 1 : object instanceof ShortRaster ? 2 : 3;
		if (type <= 1) {
			ByteRaster raster = getByteRaster(object, type);
			byte[] pixels = raster.getData();
			if (fos == null)
				initStream(raster.getWidth(), raster.getHeight(), raster.getStringType(), pixels.length, 8);
			else if (raster.getWidth() != width || raster.getHeight() != height || !raster.getStringType().equals(stringType) || pixels.length != size)
				throw new IllegalArgumentException("Stream error: Raster property change");
			fos.write(pixels, 0, pixels.length);
		} else {
			ShortRaster raster = getShortRaster(object, type);
			short[] pixels = raster.getData();
			if (fos == null)
				initStream(raster.getWidth(), raster.getHeight(), raster.getStringType(), pixels.length * 2, 8 * 2);
			else if (raster.getWidth() != width || raster.getHeight() != height || !raster.getStringType().equals(stringType) || pixels.length * 2 != size)
				throw new IllegalArgumentException("Stream error: Raster property change");
			byte[] bytes = new byte[pixels.length * 2];
			ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN).asShortBuffer().put(pixels);
			fos.write(bytes, 0, bytes.length);
		}
		timePointer++;
	}

	private void writeHeader(boolean init) throws IOException {
		try(BufferedWriter bw = new BufferedWriter(new FileWriter(getInfFile()))){
			bw.write((init ? "-1" : timePointer) + " // number of frames");
			bw.newLine();
			bw.write(width + " // horizontal frame size");
			bw.newLine();
			bw.write(height + " // vertical frame size");
			bw.newLine();
			bw.write("RAW" + " // Data coding format");
			bw.newLine();
			bw.write(stringType + " // Channels sequence");
			bw.newLine();
			bw.write(depth + " // depth");
			bw.newLine();
			bw.write(size + " // imageSize");
		}
	}
	
	public static ByteRaster getByteRaster(Object object, int type) {
		if (type == 0)
			return (ByteRaster) object;
		BufferedImage img = (BufferedImage) object;
		DataBuffer db = img.getRaster().getDataBuffer();
		if (!(db instanceof DataBufferByte)) {
			img = new TypeConverter().process(img);
			db = img.getRaster().getDataBuffer();
		}
		byte[] pixels = ((DataBufferByte) db).getData();
		int imgType = img.getType();
		int imgRasterType = imgType == BufferedImage.TYPE_BYTE_GRAY ? Raster.GRAY : imgType == BufferedImage.TYPE_3BYTE_BGR ? Raster.BGR : -1;
		if (imgRasterType == -1 && imgType == BufferedImage.TYPE_CUSTOM && img.getColorModel().getColorSpace() instanceof YUVColorSpace)
			imgRasterType = Raster.YUV;
		return new ByteRaster(img.getWidth(), img.getHeight(), imgRasterType, pixels);
	}

	public static ShortRaster getShortRaster(Object object, int type) {
		if (type == 2)
			return (ShortRaster) object;
		BufferedImage img = (BufferedImage) object;
		DataBufferUShort db = (DataBufferUShort) img.getRaster().getDataBuffer();
		short[] pixels = db.getData();
		return new ShortRaster(img.getWidth(), img.getHeight(), Raster.GRAY, pixels);
	}
}
