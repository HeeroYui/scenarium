/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.filemanager.filerecorder;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

import org.beanmanager.BeanManager;
import org.beanmanager.editors.PropertyEditor;

public class PropertyStreamRecorder extends FileStreamRecorder {
	private PropertyEditor<?> streamableEditor;
	private DataOutputStream dataDos;
	private DataOutputStream pitchDos;
	private File file;
	private FileChannel channel;

	public PropertyStreamRecorder(PropertyEditor<?> streamableEditor) {
		this.streamableEditor = streamableEditor;
	}

	@Override
	public void close() {
		if (dataDos != null)
			try {
				dataDos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		if (pitchDos != null)
			try {
				pitchDos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		channel = null;
	}

	@Override
	public File getFile() {
		if (file == null && recordPath != null)
			file = new File(recordPath.endsWith(".ppsr") ? recordPath : recordPath + File.separator + recordName + ".ppd");
		return file;
	}

	@Override
	public void pop(Object object) throws IOException, FileNotFoundException {
		if (dataDos == null) {
			file = getFile();
			FileOutputStream fos = new FileOutputStream(file);
			channel = fos.getChannel();
			dataDos = new DataOutputStream(fos);
			// StringEditor.writeString(dataDos, object.getClass().getName()); //TODO je dois faire ca normalement...
			dataDos.writeBytes(BeanManager.getDescriptorFromClass(object.getClass()) + System.lineSeparator());// dépend des plateformes!!!!
			if (streamableEditor.getPitch() <= 0)
				pitchDos = new DataOutputStream(new FileOutputStream(file.getAbsolutePath().substring(0, file.getAbsolutePath().length() - 4) + ".pindex"));
		}
		if (pitchDos != null)
			pitchDos.writeLong(channel.position());		//dataDos.size() marche, pas, il est en int...

		streamableEditor.writeValueFromObj(dataDos, object);
		timePointer++;
	}
}
