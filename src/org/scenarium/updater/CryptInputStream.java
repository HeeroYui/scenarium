/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.updater;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class CryptInputStream extends FilterInputStream {
	private InputStream input;
	private byte[] ibuffer = new byte[512];
	private boolean done = false;
	private byte[] obuffer;
	private int ostart = 0;
	private int ofinish = 0;
	private Encryptor encryptor;

	public CryptInputStream(InputStream is, EncryptionAlgorithm encryptionAlgorithm) {
		super(is);
		input = is;
		encryptor = new Encryptor(encryptionAlgorithm);
	}

	@Override
	public int available() throws IOException {
		return ofinish - ostart;
	};

	@Override
	public void close() throws IOException {
		input.close();
		encryptor.doFinal(null, 0, 0);
		ostart = 0;
		ofinish = 0;
	}

	private int getMoreData() throws IOException {
		if (done)
			return -1;
		int readin = input.read(ibuffer);
		if (readin == -1) {
			done = true;
			obuffer = encryptor.doFinal(null, 0, 0);
			if (obuffer == null)
				return -1;
			ostart = 0;
			ofinish = obuffer.length;
			return ofinish;
		}
		try {
			obuffer = encryptor.update(ibuffer, 0, readin);
		} catch (IllegalStateException e) {
			obuffer = null;
		}
		;
		ostart = 0;
		if (obuffer == null)
			ofinish = 0;
		else
			ofinish = obuffer.length;
		return ofinish;
	}

	@Override
	public boolean markSupported() {
		return false;
	}

	@Override
	public int read() throws IOException {
		if (ostart >= ofinish) {
			// we loop for new data as the spec says we are blocking
			int i = 0;
			while (i == 0)
				i = getMoreData();
			if (i == -1)
				return -1;
		}
		return obuffer[ostart++] & 0xff;
	}

	@Override
	public int read(byte b[]) throws IOException {
		return read(b, 0, b.length);
	}

	@Override
	public int read(byte b[], int off, int len) throws IOException {
		if (ostart >= ofinish) {
			// we loop for new data as the spec says we are blocking
			int i = 0;
			while (i == 0)
				i = getMoreData();
			if (i == -1)
				return -1;
		}
		if (len <= 0)
			return 0;
		int available = ofinish - ostart;
		if (len < available)
			available = len;
		if (b != null)
			System.arraycopy(obuffer, ostart, b, off, available);
		ostart = ostart + available;
		return available;
	}

	@Override
	public long skip(long n) throws IOException {
		int available = ofinish - ostart;
		if (n > available)
			n = available;
		if (n < 0)
			return 0;
		ostart += n;
		return n;
	}
}
