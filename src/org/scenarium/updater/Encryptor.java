/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.updater;

public class Encryptor {
	private byte[] buffer;
	private int blockSize;
	private int buffered = 0;
	private boolean decrypting = false;
	private EncryptionAlgorithm ea;

	public Encryptor(EncryptionAlgorithm ea) {
		buffer = new byte[ea.getBlockSize() * 2];
		blockSize = ea.getBlockSize();
		decrypting = ea.isDecrypting();
		this.ea = ea;
	}

	void decrypt(byte[] in, int inOff, int len, byte[] out, int outOff) {
		while (len >= blockSize) {
			ea.decryptBlock(in, inOff, out, outOff);
			len -= blockSize;
			inOff += blockSize;
			outOff += blockSize;
		}
	}

	byte[] doFinal(byte[] input, int inputOffset, int inputLen) {
		byte[] output = null;
		byte[] out = null;
		output = new byte[getOutputSize(inputLen)];
		int len = doFinal(input, inputOffset, inputLen, output, 0);
		if (len < output.length) {
			out = new byte[len];
			if (len != 0)
				System.arraycopy(output, 0, out, 0, len);
		} else
			out = output;
		return out;
	}

	int doFinal(byte[] input, int inputOffset, int inputLen, byte[] output, int outputOffset) {
		int totalLen = buffered + inputLen;
		int paddedLen = totalLen;
		int paddingLen = padLength(totalLen);
		if (paddingLen > 0 && paddingLen != blockSize && decrypting)
			throw new IllegalArgumentException("Input length must be multiple of " + blockSize + " when decrypting with padded cipher");
		if (!decrypting)
			paddedLen += paddingLen;
		if (output == null)
			throw new IllegalArgumentException("Output buffer is null");
		int outputCapacity = output.length - outputOffset;
		if ((!decrypting || false) && outputCapacity < paddedLen || decrypting && outputCapacity < paddedLen - blockSize)
			throw new IllegalArgumentException("Output buffer too short: " + outputCapacity + " bytes given, " + paddedLen + " bytes needed");
		byte[] finalBuf = input;
		int finalOffset = inputOffset;
		if (buffered != 0 || !decrypting) {
			finalOffset = 0;
			finalBuf = new byte[paddedLen];
			if (buffered != 0)
				System.arraycopy(buffer, 0, finalBuf, 0, buffered);
			if (inputLen != 0)
				System.arraycopy(input, inputOffset, finalBuf, buffered, inputLen);
			if (!decrypting)
				padWithLen(finalBuf, totalLen, paddingLen);
		}
		if (decrypting) {
			byte[] outWithPadding = new byte[totalLen];
			totalLen = finalNoPadding(finalBuf, finalOffset, outWithPadding, 0, totalLen);
			int padStart = unpad(outWithPadding, 0, totalLen);
			if (padStart < 0)
				throw new IllegalArgumentException("Given final block not " + "properly padded");
			totalLen = padStart;
			if (output.length - outputOffset < totalLen)
				throw new IllegalArgumentException("Output buffer too short: " + (output.length - outputOffset) + " bytes given, " + totalLen + " bytes needed");
			for (int i = 0; i < totalLen; i++)
				output[outputOffset + i] = outWithPadding[i];
		} else
			totalLen = finalNoPadding(finalBuf, finalOffset, output, outputOffset, paddedLen);
		buffered = 0;
		return totalLen;
	}

	void encrypt(byte[] in, int inOff, int len, byte[] out, int outOff) {
		while (len >= blockSize) {
			ea.encryptBlock(in, inOff, out, outOff);
			len -= blockSize;
			inOff += blockSize;
			outOff += blockSize;
		}
	}

	private int finalNoPadding(byte[] in, int inOff, byte[] out, int outOff, int len) {
		if (in == null || len == 0)
			return 0;
		if (decrypting)
			decrypt(in, inOff, len, out, outOff);
		else
			encrypt(in, inOff, len, out, outOff);
		return len;
	}

	int getOutputSize(int inputLen) {
		int totalLen = buffered + inputLen;
		return decrypting ? totalLen : totalLen + blockSize - totalLen % blockSize;
	}

	public int padLength(int len) {
		return blockSize - len % blockSize;
	}

	public void padWithLen(byte[] in, int off, int len) {
		if (in == null)
			return;
		if (off + len > in.length)
			throw new IllegalArgumentException("Buffer too small to hold padding");
		byte paddingOctet = (byte) (len & 0xff);
		for (int i = 0; i < len; i++)
			in[i + off] = paddingOctet;
	}

	public int unpad(byte[] in, int off, int len) {
		if (in == null || len == 0)
			return 0;
		byte lastByte = in[off + len - 1];
		int padValue = lastByte & 0x0ff;
		if (padValue < 0x01 || padValue > blockSize)
			return -1;
		int start = off + len - (lastByte & 0x0ff);
		if (start < off)
			return -1;
		for (int i = 0; i < (lastByte & 0x0ff); i++)
			if (in[start + i] != lastByte)
				return -1;
		return start;
	}

	byte[] update(byte[] input, int inputOffset, int inputLen) {
		byte[] output = null;
		byte[] out = null;
		output = new byte[getOutputSize(inputLen)];
		int len = update(input, inputOffset, inputLen, output, 0);
		if (len == output.length)
			out = output;
		else {
			out = new byte[len];
			System.arraycopy(output, 0, out, 0, len);
		}
		return out;
	}

	int update(byte[] input, int inputOffset, int inputLen, byte[] output, int outputOffset) {
		int len = buffered + inputLen;
		if (decrypting)
			len -= blockSize;
		len = len > 0 ? len - len % blockSize : 0;
		if (len != 0) {
			byte[] in = new byte[len];
			int inputConsumed = len - buffered;
			int bufferedConsumed = buffered;
			if (inputConsumed < 0) {
				inputConsumed = 0;
				bufferedConsumed = len;
			}
			if (buffered != 0)
				System.arraycopy(buffer, 0, in, 0, bufferedConsumed);
			if (inputConsumed > 0)
				System.arraycopy(input, inputOffset, in, bufferedConsumed, inputConsumed);
			if (decrypting)
				decrypt(in, 0, len, output, outputOffset);
			else
				encrypt(in, 0, len, output, outputOffset);
			inputLen -= inputConsumed;
			inputOffset += inputConsumed;
			outputOffset += len;
			buffered -= bufferedConsumed;
			if (buffered > 0)
				System.arraycopy(buffer, bufferedConsumed, buffer, 0, buffered);
		}
		if (inputLen > 0)
			System.arraycopy(input, inputOffset, buffer, buffered, inputLen);
		buffered += inputLen;
		return len;
	}

	public final byte[] update1(byte[] input, int inputOffset, int inputLen) {
		if (input == null || inputOffset < 0 || inputLen > input.length - inputOffset || inputLen < 0)
			throw new IllegalArgumentException("Bad arguments");
		if (inputLen == 0)
			return null;
		return update(input, inputOffset, inputLen);
	}
}
