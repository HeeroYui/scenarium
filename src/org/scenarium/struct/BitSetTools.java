/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.struct;

import java.util.BitSet;

public class BitSetTools {
	public static byte[] getDataFromBitSet(BitSet bitSet, int size) {
		if (bitSet == null || bitSet.length() == 0)
			return new byte[size];
		bitSet = (BitSet) bitSet.clone();
		bitSet.clear(bitSet.length() - 1);
		byte[] byteArray = bitSet.toByteArray();
		// for (int i = 0; i < byteArray.length / 2; i++) {
		// byte temp = byteArray[i];
		// byteArray[i] = byteArray[byteArray.length - i - 1];
		// byteArray[byteArray.length - i - 1] = temp;
		// }
		if (byteArray.length != size) {
			byte[] temp = new byte[size];
			System.arraycopy(byteArray, 0, temp, temp.length - byteArray.length, byteArray.length);
			byteArray = temp;
		}
		return byteArray;
	}
}
