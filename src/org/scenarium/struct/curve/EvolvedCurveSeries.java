/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.struct.curve;

import java.util.ArrayList;

public class EvolvedCurveSeries extends CurveSeries {
	private static final long serialVersionUID = 1L;
	private ArrayList<BoxMarker> boxmarkers;
	private String name;
	private String xLabel;
	private String yLabel;
	private double begin;
	private double end;

	public EvolvedCurveSeries(ArrayList<BoxMarker> boxmarkers, ArrayList<Curve> curves) {
		super(curves);
		this.boxmarkers = boxmarkers;
	}

	public EvolvedCurveSeries(ArrayList<BoxMarker> boxmarkers, Curve... curves) {
		super(curves);
		this.boxmarkers = boxmarkers;
	}

	public EvolvedCurveSeries(ArrayList<BoxMarker> boxmarkers, String name, String xLabel, String yLabel, double begin, double end, ArrayList<Curve> curves) {
		super(curves);
		this.name = name;
		this.xLabel = xLabel;
		this.yLabel = yLabel;
		this.boxmarkers = boxmarkers;
		addIntervalMarker(begin, end);
	}

	public EvolvedCurveSeries(ArrayList<BoxMarker> boxmarkers, String name, String xLabel, String yLabel, double begin, double end, Curve... curves) {
		super(curves);
		this.name = name;
		this.xLabel = xLabel;
		this.yLabel = yLabel;
		this.boxmarkers = boxmarkers;
		addIntervalMarker(begin, end);
	}

	public EvolvedCurveSeries(ArrayList<Curve> curves) {
		super(curves);
	}

	public EvolvedCurveSeries(Curve... curves) {
		super(curves);
	}

	public EvolvedCurveSeries(String name, String xLabel, String yLabel, ArrayList<Curve> curves) {
		super(curves);
		this.name = name;
		this.xLabel = xLabel;
		this.yLabel = yLabel;
	}

	public EvolvedCurveSeries(String name, String xLabel, String yLabel, Curve... curves) {
		super(curves);
		this.name = name;
		this.xLabel = xLabel;
		this.yLabel = yLabel;
	}

	public void addBoxMarker(BoxMarker boxmarker) {
		if (boxmarkers == null)
			boxmarkers = new ArrayList<>();
		boxmarkers.add(boxmarker);
	}

	public void addIntervalMarker(double begin, double end) {
		this.begin = begin;
		this.end = end;
	}

	@Override
	public EvolvedCurveSeries clone() {
		EvolvedCurveSeries ecs = new EvolvedCurveSeries(super.clone().curves);
		ecs.begin = begin;
		ecs.end = end;
		ecs.name = name;
		ecs.xLabel = xLabel;
		ecs.yLabel = yLabel;
		if (boxmarkers != null)
			ecs.boxmarkers = new ArrayList<>(boxmarkers);
		return ecs;
	}

	public double getBegin() {
		return begin;
	}

	public ArrayList<BoxMarker> getBoxmarkers() {
		return boxmarkers;
	}

	public double getEnd() {
		return end;
	}

	public String getName() {
		return name;
	}

	public String getxLabel() {
		return xLabel;
	}

	public String getyLabel() {
		return yLabel;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setxLabel(String xLabel) {
		this.xLabel = xLabel;
	}

	public void setyLabel(String yLabel) {
		this.yLabel = yLabel;
	}
}
