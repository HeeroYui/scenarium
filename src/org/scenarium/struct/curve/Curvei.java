/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.struct.curve;

import java.util.ArrayList;

public class Curvei extends Curve {
	private static final long serialVersionUID = 1L;

	public static Curvei getCurve(ArrayList<Integer> values) {
		int size = values.size();
		int[][] datas = new int[2][size];
		for (int i = 0; i < size; i++) {
			datas[0][i] = i;
			datas[1][i] = values.get(i);
		}
		return new Curvei(datas);
	}

	private int[][] data;

	private int[] values;

	public Curvei(int[][] data) {
		if (data == null)
			throw new IllegalArgumentException("The data may not be null");
		if (data.length <= 1)
			throw new IllegalArgumentException("The data array must have length <= 1");
		if (data[0].length != data[1].length)
			throw new IllegalArgumentException("The data array array must contain two arrays with equal length");
		this.data = data;
	}

	public Curvei(int[][] data, int[] values) {
		this(data);
		if (values != null && values.length != data[0].length)
			throw new IllegalArgumentException("The values array must have length == data array");
		this.values = values;
	}

	public Curvei(int[][] data, int[] values, String name) {
		this(data, values);
		this.name = name;
	}

	public Curvei(int[][] data, String name) {
		this(data);
		this.name = name;
	}

	@Override
	public Curvei clone() {
		int[][] temp = data.clone();
		for (int i = 0; i < temp.length; i++)
			temp[i] = data[i].clone();
		return new Curvei(temp, values != null ? values.clone() : null, name);
	}

	public int[][] getData() {
		return data;
	}

	@Override
	public double[][] getDatad() {
		int l1 = data.length;
		int l2 = data[0].length;
		double[][] datad = new double[l1][l2];
		for (int i = 0; i < l1; i++) {
			double[] subd = datad[i];
			int[] subi = data[i];
			for (int j = 0; j < l2; j++)
				subd[j] = subi[j];
		}
		return datad;
	}

	public int[] getValues() {
		return values;
	}

	@Override
	public double[] getValuesd() {
		if (values == null)
			return null;
		int l = values.length;
		double[] valuesd = new double[l];
		for (int i = 0; i < l; i++)
			valuesd[i] = values[l];
		return valuesd;
	}

	public int nbPoints() {
		return data[0].length;
	}
}