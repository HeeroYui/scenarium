/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/

/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.struct;

import java.io.Serializable;
import java.util.Objects;

public class GeographicalPose extends GeographicCoordinate implements Cloneable, Serializable {
	private static final long serialVersionUID = 1L;
	public final double heading;
	public final double roll;
	public final double pitch;
	public final double stdHeading;
	public final double stdRoll;
	public final double stdPitch;

	public GeographicalPose(double latitude, double longitude, double altitude, double heading, double roll, double pitch) {
		this(latitude, longitude, altitude, null, Double.NaN, heading, roll, pitch, heading, roll, pitch);
	}

	public GeographicalPose(double latitude, double longitude, double altitude, double posVar[], double stdAltitude, double heading, double roll, double pitch, double stdHeading, double stdRoll, double stdPitch) {
		super(latitude, longitude, altitude, posVar, stdAltitude);
		this.heading = heading;
		this.roll = roll;
		this.pitch = pitch;
		this.stdHeading = stdHeading;
		this.stdRoll = stdRoll;
		this.stdPitch = stdPitch;
	}

	public GeographicalPose(double latitude, double longitude, double altitude, double stdLatitude, double stdLongitude, double stdAltitude, double heading, double roll, double pitch, double stdHeading, double stdRoll, double stdPitch) {
		this(latitude, longitude, altitude, new double[] { stdLatitude, 0, 0, stdLongitude }, stdAltitude, heading, roll, pitch, stdHeading, stdRoll, stdPitch);
	}

	@Override
	public GeographicalPose clone() {
		return new GeographicalPose(latitude, longitude, altitude, posVar, stdAltitude, heading, roll, pitch, stdHeading, stdRoll, stdPitch);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (!(obj instanceof GeographicalPose))
			return false;
		if (obj == this)
			return true;
		GeographicalPose gc = (GeographicalPose) obj;
		return super.equals(obj) && Double.doubleToLongBits(heading) == Double.doubleToLongBits(gc.heading) && Double.doubleToLongBits(roll) == Double.doubleToLongBits(gc.roll) && Double.doubleToLongBits(pitch) == Double.doubleToLongBits(gc.pitch)
				&& Double.doubleToLongBits(stdHeading) == Double.doubleToLongBits(gc.stdHeading) && Double.doubleToLongBits(stdRoll) == Double.doubleToLongBits(gc.stdRoll) && Double.doubleToLongBits(stdPitch) == Double.doubleToLongBits(gc.stdPitch);
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), latitude, longitude, altitude, posVar, stdAltitude);
	}
	
	/** Increment meters to a geographical coordinate
	 *
	 * @param dx the number of meters along the x axis
	 * @param dy the number of meters along the y axis
	 * @return */
	@Override
	public GeographicalPose incrementFast(double dx, double dy) {
		return new GeographicalPose(latitude + dy / earthRadius * (180 / Math.PI), longitude + dx / earthRadius * (180 / Math.PI) / Math.cos(latitude * Math.PI / 180), altitude, posVar, stdAltitude, heading, roll, pitch, stdHeading, stdRoll,
				stdPitch);
	}

	@Override
	public String toString() {
		return "GeographicalPose [latitude=" + latitude + ", longitude=" + longitude + (!Double.isNaN(altitude) ? ", altitude=" + altitude : "") + (posVar != null ? ", posVar=" + posVar : "")
				+ (!Double.isNaN(stdAltitude) ? ", stdAltitude=" + stdAltitude : "") + ", heading=" + heading + ", roll=" + roll + ", pitch=" + pitch + (!Double.isNaN(stdHeading) ? ", stdHeading=" + stdHeading : "")
				+ (!Double.isNaN(stdRoll) ? ", stdRoll=" + stdRoll : "") + (!Double.isNaN(stdPitch) ? ", stdPitch=" + stdPitch + "]" : "]");
	}
}
