/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.struct;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/******************************************************************************* Copyright (c) 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019 Revilloud Marc. All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 *
 * Contributors: Revilloud Marc - initial API and implementation ******************************************************************************/
public class CSVData implements Serializable {
	private static final long serialVersionUID = 1L;
	private LinkedHashMap<String, Integer> typeIndex = new LinkedHashMap<>();
	private int nbElements;
	private Object[] data;

	@SuppressWarnings("unchecked")
	@Override
	public CSVData clone() {
		CSVData newCvsData = new CSVData();
		newCvsData.typeIndex = (LinkedHashMap<String, Integer>) typeIndex.clone();
		newCvsData.nbElements = nbElements;
		if (data != null)
			newCvsData.data = data.clone();
		return newCvsData;
	}

	public CSVData cloneData() {
		CSVData newCvsData = new CSVData();
		newCvsData.typeIndex = typeIndex;
		newCvsData.nbElements = nbElements;
		if (data != null)
			newCvsData.data = data.clone();
		return newCvsData;
	}

	public CSVData cloneData(String... excludeFilter) {
		CSVData newCvsData = new CSVData();
		newCvsData.typeIndex = typeIndex;
		newCvsData.nbElements = nbElements;
		ArrayList<Integer> exclud = new ArrayList<>();
		for (String filter : excludeFilter)
			if (typeIndex.containsKey(filter))
				exclud.add(getIndexFromName(filter));
		if (data != null)
			newCvsData.data = data.clone();
		for (Integer id : exclud)
			newCvsData.data[id] = null;
		return newCvsData;
	}

	@Override
	public boolean equals(Object e) {
		if (!(e instanceof CSVData))
			return false;
		Object[] oData = ((CSVData) e).getData();
		for (int i = 0; i < data.length; i++)
			if (!(oData[i] == null && data[i] == null || oData[i].equals(data[i])))
				return false;
		return true;
	}

	public Object[] getData() {
		return data;
	}

	public Integer getIndexFromName(String name) {
		return typeIndex.get(name);
	}

	public String getNameFromIndex(int i) {
		for (String name : typeIndex.keySet())
			if (typeIndex.get(name) == i)
				return name;
		return null;
	}

	public int getNbElements() {
		return nbElements;
	}

	public void initStruct(LinkedHashMap<String, Integer> typeIndex) {
		this.typeIndex = typeIndex;
		nbElements = typeIndex.values().size();
	}

	public void setData(Object[] data) {
		this.data = data;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		int i = 0;
		for (String name : typeIndex.keySet()) {
			sb.append(name);
			sb.append("= ");
			sb.append(data[i++]);
			if (i >= data.length)
				break;
			sb.append("; ");
		}
		return sb.toString();
	}
}
