/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.struct;

import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import javax.swing.SwingUtilities;

import com.jogamp.opengl.awt.GLCanvas;

public class Camera implements MouseWheelListener, KeyListener, MouseListener, MouseMotionListener {
	private GLCanvas canvas;
	private double distance;
	private double angleX;
	private double angleY;
	private double x;
	private double y;
	private Point anchorPoint = new Point();
	private boolean translate;
	private boolean rotate;

	public Camera(GLCanvas canvas, double distance, double x, double y, double angleX, double angleY, double angleZ) {
		this.canvas = canvas;
		canvas.addMouseWheelListener(this);
		canvas.addKeyListener(this);
		canvas.addMouseMotionListener(this);
		canvas.addMouseListener(this);
		this.distance = distance;
		this.x = x;
		this.y = y;
		this.angleX = angleX;
		this.angleY = angleY;
	}

	public double getAngleX() {
		return angleX;
	}

	public double getAngleY() {
		return angleY;
	}

	public double getDistance() {
		return distance;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	@Override
	public void keyPressed(KeyEvent e) {
		rotation(e.getKeyCode());
		canvas.display();
	}

	@Override
	public void keyReleased(KeyEvent arg0) {}

	@Override
	public void keyTyped(KeyEvent arg0) {}

	@Override
	public void mouseClicked(MouseEvent e) {}

	@Override
	public void mouseDragged(MouseEvent e) {
		if (translate)
			translate((anchorPoint.x - e.getX()) / (float) canvas.getWidth() * distance, (-anchorPoint.y + e.getY()) / (float) canvas.getHeight() * distance);
		else if (rotate)
			rotate(anchorPoint.x - e.getX(), -anchorPoint.y + e.getY());
		canvas.display();
	}

	@Override
	public void mouseEntered(MouseEvent e) {}

	@Override
	public void mouseExited(MouseEvent e) {}

	@Override
	public void mouseMoved(MouseEvent e) {}

	@Override
	public void mousePressed(MouseEvent e) {
		if (SwingUtilities.isRightMouseButton(e)) {
			anchorPoint.setLocation(e.getX() + x * canvas.getWidth() / distance, e.getY() - y * canvas.getHeight() / distance);
			translate = true;
			rotate = false;
		} else if (SwingUtilities.isLeftMouseButton(e)) {
			anchorPoint.setLocation(e.getX() - angleY, e.getY() - angleX);
			rotate = true;
			translate = false;
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		translate = false;
		rotate = false;
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		zoom(e.getWheelRotation() > 0);
		canvas.display();
	}

	public void rotate(int i, int j) {
		angleY = -i;
		angleX = j;
	}

	public void rotation(int keyCode) {
		switch (keyCode) {
		case KeyEvent.VK_LEFT:
			angleY += 5;
			break;
		case KeyEvent.VK_RIGHT:
			angleY -= 5;
			break;
		case KeyEvent.VK_UP:
			angleX += 5;
			break;
		case KeyEvent.VK_DOWN:
			angleX -= 5;
			break;
		}
	}

	public void setAngleX(double angleX) {
		this.angleX = angleX;
	}

	public void setAngleY(double angleY) {
		this.angleY = angleY;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public void setX(double x) {
		this.x = x;
	}

	public void setY(double y) {
		this.y = y;
	}

	public void translate(double dx, double dy) {
		x = dx;
		y = dy;
	}

	public void zoom(boolean pos) {
		if (pos)
			distance += distance / 10.f;
		else
			distance -= distance / 10.f;
	}
}
