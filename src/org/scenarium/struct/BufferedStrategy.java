/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.struct;

public abstract class BufferedStrategy<T> {
	protected boolean isPageFlipping;
	protected T e1;
	protected T e2;

	public BufferedStrategy(boolean isPageFlipping) {
		this.isPageFlipping = isPageFlipping;
	}

	@Override
	public Object clone() {
		return clone();
	}

	protected abstract T clone(T element);

	public boolean flip() {
		if (!isPageFlipping)
			return false;
		T de = getDrawElement();
		if (de != null)
			synchronized (de) {
				synchoFlip();
			}
		else
			synchoFlip();
		if (e1 == null) {
			if (e2 == null)
				return false;
			e1 = clone(e2);// new BufferedImage(raster2.getWidth(), raster2.getHeight(), raster2.getType());
		}
		return true;
	}

	public T getComputeElement() {
		return e1;
	}

	public T getDrawElement() {
		return isPageFlipping ? e2 : e1;
	}

	public T getElement() {
		T raster = getDrawElement();
		if (raster == null)
			raster = getComputeElement();
		return raster;
	}

	public boolean isPageFlipping() {
		return isPageFlipping;
	}

	public void setComputeElement(T raster) {
		e1 = raster;
	}

	public void setPageFlipping(boolean isPageFlipping) {
		if (this.isPageFlipping == isPageFlipping)
			return;
		this.isPageFlipping = isPageFlipping;
		if (isPageFlipping)
			flip();
		else {
			synchoFlip();
			e2 = null;
		}
	}

	private void synchoFlip() {
		T temp = e1;
		e1 = e2;
		e2 = temp;
	}
}
