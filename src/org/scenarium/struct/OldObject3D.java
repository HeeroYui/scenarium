/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.struct;

import static com.jogamp.opengl.GL.GL_FLOAT;
import static com.jogamp.opengl.GL.GL_TEXTURE_2D;
import static com.jogamp.opengl.fixedfunc.GLPointerFunc.GL_VERTEX_ARRAY;

import java.nio.FloatBuffer;

import javax.vecmath.Color3f;

import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.fixedfunc.GLPointerFunc;
import com.jogamp.opengl.util.gl2.GLUT;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.awt.AWTTextureIO;

import javafx.scene.paint.Color;

public class OldObject3D implements DrawableObject3D {
	public static final int POINTS = 0;
	public static final int LINES = 1;
	public static final int TEXT = -1;
	public static final int TEXTURE = -2;
	public int elementType;
	public float[] data;
	public float[] colors;
	public Color3f color;
	public int pointSize;

	public OldObject3D(int elementType, float[] data, Color3f color, int pointSize) {
		if (elementType < -2 || elementType > 1)
			throw new IllegalArgumentException("The elementType is unknow");
		if (data == null || data.length == 0)
			throw new IllegalArgumentException("Data cannot be null or wrong data size");
		if (color == null)
			throw new IllegalArgumentException("Color cannot be null");
		if (pointSize != 2 && pointSize != 3)
			throw new IllegalArgumentException("PointSize must be 2 or 3");
		this.elementType = elementType;
		this.data = data;
		this.colors = null;
		this.color = color;
		this.pointSize = pointSize;
	}

	public OldObject3D(int elementType, float[] data, float[] colors, int pointSize) {
		if (elementType < -2 || elementType > 1)
			throw new IllegalArgumentException("The elementType is unknow");
		if (data == null || data.length % pointSize != 0)
			throw new IllegalArgumentException("Data cannot be null or wrong data size");
		if (colors == null || colors.length != data.length)
			throw new IllegalArgumentException("Color cannot be null or colors size is not equal to data size");
		if (pointSize != 2 && pointSize != 3)
			throw new IllegalArgumentException("PointSize must be 2 or 3");
		this.elementType = elementType;
		this.data = data;
		this.colors = colors;
		this.color = null;
		this.pointSize = pointSize;
	}

	public OldObject3D(int elementType, float[] data, int pointSize) {
		if (elementType < -2 || elementType > 1)
			throw new IllegalArgumentException("The elementType is unknow");
		if (data == null)
			throw new IllegalArgumentException("Data cannot be null or wrong data size");
		if (pointSize != 2 && pointSize != 3)
			throw new IllegalArgumentException("PointSize must be 2 or 3");
		this.elementType = elementType;
		this.data = data;
		this.colors = null;
		this.color = null;
		this.pointSize = pointSize;
	}

	@Override
	public OldObject3D clone() {
		return colors != null ? new OldObject3D(elementType, data.clone(), colors.clone(), pointSize)
				: color != null ? new OldObject3D(elementType, data.clone(), color.clone(), pointSize) : new OldObject3D(elementType, data.clone(), pointSize);
	}

	@Override
	public void draw(GL2 gl, Color color) {
		if (elementType >= 0 && data.length != 0) {
			gl.glLineWidth(1);
			FloatBuffer dataBuffer = Buffers.newDirectFloatBuffer(data);
			dataBuffer.rewind();
			gl.glPointSize(pointSize);
			gl.glEnableClientState(GL_VERTEX_ARRAY);
			gl.glVertexPointer(3, GL_FLOAT, 0, dataBuffer);
			if (colors != null) {
				gl.glEnableClientState(GLPointerFunc.GL_COLOR_ARRAY);
				FloatBuffer colors = Buffers.newDirectFloatBuffer(this.colors);
				colors.rewind();
				gl.glColorPointer(3, GL_FLOAT, 0, colors);
				gl.glDrawArrays(elementType, 0, data.length / pointSize);
				gl.glDisableClientState(GLPointerFunc.GL_COLOR_ARRAY);
			} else if (this.color != null) {
				gl.glColor3f(this.color.x, this.color.y, this.color.z);
				gl.glDrawArrays(elementType, 0, data.length / pointSize);
			} else {
				gl.glColor3f((float) color.getRed(), (float) color.getGreen(), (float) color.getBlue());
				gl.glDrawArrays(elementType, 0, data.length / pointSize);
			}
			gl.glDisableClientState(GL_VERTEX_ARRAY);
		} else if (elementType == TEXT && this instanceof Text3D) {
			Text3D text3D = (Text3D) this;
			float[] data = text3D.data;
			for (int i = 0; i < data.length; i += 3) {
				gl.glPushMatrix();
				gl.glTranslated(data[i], data[i + 1], data[i + 2]);
				gl.glScalef(0.005f, 0.003f, 0.003f);
				GLUT glut = new GLUT();
				gl.glColor3f((float) color.getRed(), (float) color.getGreen(), (float) color.getBlue());
				glut.glutStrokeString(GLUT.STROKE_ROMAN, text3D.text[i / 3]);
				gl.glPopMatrix();
			}
		} else if (elementType == TEXTURE && this instanceof Texture3D) {
			Texture3D texture3D = (Texture3D) this;
			Texture texture = AWTTextureIO.newTexture(gl.getGLProfile(), texture3D.img, true);
			gl.glBindTexture(GL_TEXTURE_2D, texture.getTextureObject());

			gl.glEnable(GL_TEXTURE_2D);
			float[] data = texture3D.data;
			float trap_wide = data[5] - data[1];
			float trap_narrow = data[7] - data[3];

			int indexData = 0;
			gl.glBegin(GL.GL_TRIANGLE_STRIP);
			gl.glColor3f(1, 1, 1);

			// glTexCoord4f(0,0,0,1);
			gl.glTexCoord4f(trap_wide, trap_wide, 0, trap_wide);
			gl.glVertex3f(data[indexData++], data[indexData++], 0.01f);

			// glTexCoord4f(1,0,0,1);
			gl.glTexCoord4f(trap_narrow, 0, 0, trap_narrow);
			gl.glVertex3f(data[indexData++], data[indexData++], 0.01f);

			// glTexCoord4f(0,1,0,1);
			gl.glTexCoord4f(0, trap_wide, 0, trap_wide);
			gl.glVertex3f(data[indexData++], data[indexData++], 0.01f);

			// glTexCoord4f(1,1,0,1);
			gl.glTexCoord4f(0, 0, 0, trap_narrow);
			gl.glVertex3f(data[indexData++], data[indexData++], 0.01f);

			gl.glEnd();
			gl.glDisable(GL_TEXTURE_2D);
		}
	}
	/// TODO
	// @Override
	// public void writeExternal(ObjectOutput out) throws IOException {
	// out.writeInt(elementType);
	// float[] array = (float[]) data;
	// ByteBuffer buff = ByteBuffer.allocate(array.length * Float.BYTES);
	// for (float val : (float[]) data)
	// buff.putFloat(val);
	// out.writeInt(array.length);
	// out.write(buff.array());
	//
	// //TODO
	// }
	//
	// @Override
	// public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
	// // TODO Auto-generated method stub
	// byte[] data = new byte[data.length * Float.BYTES];
	// raf.readFully(data);
	// int times = Float.SIZE / Byte.SIZE;
	// float[] floats = new float[data.length / times];
	// for (int i = 0; i < floats.length; i++) {
	// floats[i] = ByteBuffer.wrap(data, i * times, times).getFloat();
	// }
	// }

	@Override
	public String toString() {
		return "ElementType: " + (elementType == 0 ? "Points" : "Lines") + " Data size: " + data.length + " Point size: " + pointSize;
	}
}
