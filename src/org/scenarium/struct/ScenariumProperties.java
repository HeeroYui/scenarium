/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.struct;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;

import javax.vecmath.Point2d;

import org.beanmanager.BeanManager;
import org.scenarium.display.toolbarclass.ToolBarDescriptor;
import org.scenarium.filemanager.scenariomanager.ScenarioDescriptor;

public class ScenariumProperties {
	private static ScenariumProperties scenariumProperties;
	private ScenarioDescriptor[] recentScenarios;
	private ScenarioDescriptor[] playList;
	private int playListIndex = 0;
	private boolean synchronization = false;
	private File scenarioChooserPath;
	private File recordChooserPath;
	private File playListChooserPath;
	private int videoFrequencie = 40;
	private String operator;
	private String lookAndFeel;
	private boolean askBeforeQuit = false;
	public boolean checkUpdatesAtStarup = true;
	private String language;
	private int[][] displayMode;
	private boolean hiddenFieldVisible = false;
	private boolean expertFieldVisible = false;
	private Point2d mainFramePosition;
	private Point2d mainFrameDimension;
	private boolean mainFrameAlwaysOnTop;
	private ToolBarDescriptor[] visibleToolsDesc;
	private boolean pageFlipping = false;
	private boolean showHiddenProperties = false;
	private boolean showExpertProperties = true;
	private String mapTempPath;
	private String[] externModules;

	private ScenariumProperties() {
		mapTempPath = System.getProperty("java.io.tmpdir");
		if (!mapTempPath.endsWith(File.separator))
			mapTempPath += File.separator;
		mapTempPath += "mapCache" + File.separator;
	}

	public static ScenariumProperties get() {
		if (scenariumProperties == null)
			scenariumProperties = new ScenariumProperties();
		return scenariumProperties;
	}

	public int[][] getDisplayMode() {
		return displayMode;
	}

	public String getLanguage() {
		return language;
	}

	public String getLookAndFeel() {
		return lookAndFeel;
	}

	public Point2d getMainFrameDimension() {
		return mainFrameDimension;
	}

	public Point2d getMainFramePosition() {
		return mainFramePosition;
	}

	public String getMapTempPath() {
		return mapTempPath;
	}

	public String getOperator() {
		return operator;
	}

	public ScenarioDescriptor[] getPlayList() {
		return playList;
	}

	public File getPlayListChooserPath() {
		return playListChooserPath;
	}

	public int getPlayListIndex() {
		return playListIndex;
	}

	public ScenarioDescriptor[] getRecentScenarios() {
		return recentScenarios;
	}

	public File getRecordChooserPath() {
		return recordChooserPath;
	}

	public File getScenarioChooserPath() {
		return scenarioChooserPath;
	}

	public int getVideoFrequencie() {
		return videoFrequencie;
	}

	public ToolBarDescriptor[] getVisibleToolsDesc() {
		return visibleToolsDesc;
	}

	public boolean isAskBeforeQuit() {
		return askBeforeQuit;
	}

	public boolean isCheckUpdatesAtStarup() {
		return checkUpdatesAtStarup;
	}

	public boolean isExpertFieldVisible() {
		return expertFieldVisible;
	}

	public boolean isHiddenFieldVisible() {
		return hiddenFieldVisible;
	}

	public boolean isMainFrameAlwaysOnTop() {
		return mainFrameAlwaysOnTop;
	}

	public boolean isPageFlipping() {
		return pageFlipping;
	}

	public boolean isShowExpertProperties() {
		return showExpertProperties;
	}

	public boolean isShowHiddenProperties() {
		return showHiddenProperties;
	}

	public boolean isSynchronization() {
		return synchronization;
	}

	public void setAskBeforeQuit(boolean askBeforeQuit) {
		this.askBeforeQuit = askBeforeQuit;
	}

	public void setCheckUpdatesAtStarup(boolean checkUpdatesAtStarup) {
		this.checkUpdatesAtStarup = checkUpdatesAtStarup;
	}

	public void setDisplayMode(int[][] displayMode) {
		this.displayMode = displayMode;
	}

	public void setExpertFieldVisible(boolean expertFieldVisible) {
		this.expertFieldVisible = expertFieldVisible;
	}

	public void setHiddenFieldVisible(boolean hiddenFieldVisible) {
		this.hiddenFieldVisible = hiddenFieldVisible;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public void setLookAndFeel(String lookAndFeel) {
		this.lookAndFeel = lookAndFeel;
	}

	public void setMainFrameAlwaysOnTop(boolean mainFrameAlwaysOnTop) {
		this.mainFrameAlwaysOnTop = mainFrameAlwaysOnTop;
	}

	public void setMainFrameDimension(Point2d mainFrameDimension) {
		this.mainFrameDimension = mainFrameDimension;
	}

	public void setMainFramePosition(Point2d mainFramePosition) {
		this.mainFramePosition = mainFramePosition;
	}

	public void setMapTempPath(String mapTempPath) {
		Path parentPath = Paths.get(mapTempPath).getParent();
		if (parentPath != null && Files.exists(parentPath)) {
			if (!mapTempPath.endsWith(File.separator))
				mapTempPath += File.separator;
			this.mapTempPath = mapTempPath;
		}
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public void setPageFlipping(boolean pageFlipping) {
		this.pageFlipping = pageFlipping;
	}

	public void setPlayList(ScenarioDescriptor[] playList) {
		this.playList = playList;
	}

	public void setPlayListChooserPath(File playListChooserPath) {
		this.playListChooserPath = playListChooserPath;
	}

	public void setPlayListIndex(int playListIndex) {
		this.playListIndex = playListIndex;
	}

	public void setRecentScenarios(ScenarioDescriptor[] recentScenarios) {
		this.recentScenarios = recentScenarios;
	}

	public void setRecordChooserPath(File recordChooserPath) {
		this.recordChooserPath = recordChooserPath;
	}

	public void setScenarioChooserPath(File scenarioChooserPath) {
		if (scenarioChooserPath != null && !scenarioChooserPath.isDirectory())
			scenarioChooserPath = scenarioChooserPath.getParentFile();
		this.scenarioChooserPath = scenarioChooserPath;
	}

	public void setShowExpertProperties(boolean showExpertProperties) {
		this.showExpertProperties = showExpertProperties;
		BeanManager.isExpertFieldVisible = showExpertProperties;
	}

	public void setShowHiddenProperties(boolean showHiddenProperties) {
		this.showHiddenProperties = showHiddenProperties;
		BeanManager.isHiddenFieldVisible = showHiddenProperties;
	}

	public void setSynchronization(boolean synchronization) {
		this.synchronization = synchronization;
	}

	public void setVideoFrequencie(int videoFrequencie) {
		this.videoFrequencie = videoFrequencie;
	}

	public void setVisibleToolsDesc(ToolBarDescriptor[] visibleToolsDesc) {
		this.visibleToolsDesc = visibleToolsDesc;
	}

	public String[] getExternModules() {
		return externModules;
	}

	public void setExternModules(String[] externModules) {
		this.externModules = externModules;
	}

	public void addExternModule(Path moduleFile) {
		if (moduleFile == null)
			return;
		String moduleFileName = moduleFile.toAbsolutePath().toString();
		if (externModules == null)
			externModules = new String[] { moduleFileName };
		else if (Arrays.asList(externModules).contains(moduleFileName))
			return;
		else {
			String[] newExternModules = new String[externModules.length + 1];
			System.arraycopy(externModules, 0, newExternModules, 0, externModules.length);
			newExternModules[externModules.length] = moduleFileName;
			externModules = newExternModules;
		}
	}

	public void removeModule(Path path) {
		if (path == null || externModules == null)
			return;
		ArrayList<String> em = new ArrayList<>(Arrays.asList(externModules));
		em.remove(path.toString());
		externModules = em.isEmpty() ? null : em.toArray(new String[em.size()]);
	}
}
