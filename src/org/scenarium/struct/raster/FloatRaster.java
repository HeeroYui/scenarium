/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.struct.raster;

import java.util.Arrays;

public class FloatRaster implements Raster{
	private final float[] raster;
	private final int width, height, depth, widDep;

	private int type;

	public FloatRaster(int width, int height, int type) {
		this(width, height, type, null);
	}

	public FloatRaster(int width, int height, int type, float[] pixels) {
		if(width < 0)
			throw new IllegalArgumentException("The width of the raster must be >= 0");
		if(height < 0)
			throw new IllegalArgumentException("The height of the raster must be >= 0");
		if(type < GRAY || type > YUV)
			throw new IllegalArgumentException("The type of the raster does not exists");
		this.width = width;
		this.height = height;
		this.depth = type == GRAY ? 1 : 3;
		this.type = type;
		widDep = width * depth;
		if(pixels == null)
			raster = new float[width * height * depth];
		else {
			if (pixels.length != getSize())
				throw new IllegalArgumentException("The size of the pixel array" + pixels.length + " do not corresponds to the size of this kind of image: " + getSize());
			raster = pixels;
		}
	}
	
	public void set(int x, int y, int z, float value) {
		raster[y * widDep + x * depth + z] = value;
	}

	public void setSecure(int x, int y, int z, float value) {
		if (isOnRaster(x, y, z))
			raster[y * widDep + x * depth + z] = value;
	}

	public void clear() {
		Arrays.fill(raster, 0);
	}

	public void decrement(int x, int y, int z, float dec) {
		raster[y * widDep + x * depth + z] -= dec;
	}

	public float get(int x, int y, int z) {
		return raster[y * widDep + x * depth + z];
	}

	public float[] getData() {
		return raster;
	}

	public int getDepth() {
		return depth;
	}
	
	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}
	
	public int getType() {
		return type;
	}
	
	public String getStringType() {
		if (type == GRAY)
			return "GRAY";
		else if (type == RGB)
			return "RGBFLOAT";
		else if (type == BGR)
			return "BGRFLOAT";
		else if (type == YUV)
			return "YCBCRFLOAT";
		return "UNKNOW";
	}

	public float getIntValue(int x, int y, int z) {
		return raster[y * widDep + x * depth + z];
	}

	public float getIntValueSecure(int x, int y, int z) {
		if (x < 0 || x >= width || y < 0 || y >= height)
			throw new IllegalArgumentException("out of bound");
		return raster[y * widDep + x * depth + z];
	}

	public int getSize() {
		return widDep * height;
	}

	public void increment(int x, int y, int z, float inc) {
		raster[y * widDep + x * depth + z] += inc;
	}

	public boolean isOnRaster(int x, int y) {
		return x >= 0 && x < width && y >= 0 && y < height;
	}
	
	public boolean isOnRaster(int x, int y, int z) {
		return x >= 0 && x < width && y >= 0 && y < height && z >= 0 && z < depth;
	}

	public boolean isSameTypeAndSize(FloatRaster r) {
		return r != null && width == r.getWidth() && height == r.getHeight() && type == r.getType();
	}
	
	@Override
	public FloatRaster clone() {
		FloatRaster r = new FloatRaster(width, height, type);
		System.arraycopy(raster, 0, r.raster, 0, raster.length);
		return r;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + ":" + width + "*" + height + "*" + depth + "_Hash:" + hashCode();
	}
}
