/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.tools;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.function.Function;

import org.beanmanager.tools.Logger;

public class LibraryUtils {
	public static boolean loadLibraryFromRessource(String... librariesNames) {
		return loadLibraryFromRessource(LibraryUtils.class, librariesNames);
	}
	
	public static boolean loadLibraryFromRessource(Class<?> resourcesClass, String... librariesNames) {
		return loadLibraryFromRessource(resourcesClass, null, librariesNames);
	}
	
	public static boolean loadLibraryFromRessource(Class<?> resourcesClass, Function<IOException, String> loadingErrorMessageProvider, String... librariesNames) {
		boolean isAllAvailable = true;
		for (String libName : librariesNames) {
			String libPat = System.mapLibraryName("");
			int iP = libPat.indexOf(".");
			String libCompleteName = libPat.substring(0, iP) + libName + libPat.substring(iP);
			InputStream libRessource = resourcesClass.getResourceAsStream("/" + libCompleteName);
			if (libRessource == null) {
				Logger.logError(Logger.LIBRARYLOADING, "Cannot find library " + libCompleteName + " in ressource");
				isAllAvailable = false;
				break;
			}
			try {
				Path libPath = Files.createTempFile(libName, System.mapLibraryName(""));
				Files.copy(libRessource, libPath, StandardCopyOption.REPLACE_EXISTING);
				System.load(libPath.toString());
				libPath.toFile().deleteOnExit();
				Logger.log(Logger.LIBRARYLOADING, libName + " library loaded");
			} catch (IOException e) {
				isAllAvailable = false;
				Logger.logError(Logger.LIBRARYLOADING, "Cannot load library: " + e.getMessage() + (loadingErrorMessageProvider == null ? "" : loadingErrorMessageProvider.apply(e)));
				System.err.println(e.getClass().getSimpleName() + ": " + e.getMessage() + "\nDrivers of the Peak's card probably not installed");
			}
		}
		return isAllAvailable;
	}
}
