/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.display;

import java.util.ArrayList;

import org.scenarium.timescheduler.VisuableSchedulable;

import javafx.animation.AnimationTimer;

public class ViewerAnimationTimer {
	ArrayList<VisuableSchedulable> needToBeScheduleList = new ArrayList<>();
	private ArrayList<VisuableSchedulable> visuableSchedulables = new ArrayList<>();
	private AnimationTimer animationTimer;

	synchronized public void register(VisuableSchedulable visuableSchedulable) {
		// System.out.println("register: " + visuableSchedulable);
		if (visuableSchedulables.contains(visuableSchedulable))
			return;
		visuableSchedulables.add(visuableSchedulable);
		if (animationTimer != null) {
			visuableSchedulable.setAnimated(true);
			if(visuableSchedulable.needToBeSchedule()) {
				ArrayList<VisuableSchedulable> _needToBeScheduleList = new ArrayList<>(needToBeScheduleList);
				_needToBeScheduleList.add(visuableSchedulable);
				needToBeScheduleList = _needToBeScheduleList;
			}
		}
	}
	
	synchronized public void unRegister(VisuableSchedulable visuableSchedulable) {
		// System.out.println("unRegister: " + visuableSchedulable);
		if(visuableSchedulables.remove(visuableSchedulable)) {
			if (animationTimer != null) {
				visuableSchedulable.setAnimated(false);
				ArrayList<VisuableSchedulable> _needToBeScheduleList = new ArrayList<>(needToBeScheduleList);
				_needToBeScheduleList.remove(visuableSchedulable);
				needToBeScheduleList = _needToBeScheduleList;
			}
		}
	}

//	 private int frameCount = 0;
//	 private long time = System.currentTimeMillis();

	synchronized public void start() {
		// System.err.println("start of ViewerAnimationTimer " + ProcessHandle.current().pid());
		needToBeScheduleList.clear();
		for (VisuableSchedulable visuableSchedulable : visuableSchedulables) {
			visuableSchedulable.setAnimated(true);
			if (visuableSchedulable.needToBeSchedule())
				needToBeScheduleList.add(visuableSchedulable);
		}
		// System.out.println("nb Element to schedule: " + needToBeScheduleList.size());
		animationTimer = new AnimationTimer() {
			@Override
			public void handle(long now) {
				// if(System.currentTimeMillis() - time > 1000) {
				// System.out.println(frameCount);
				// time = System.currentTimeMillis();
				// frameCount=0;
				// }
//				 frameCount++;
//				 if(frameCount%100 == 0)
//					 System.out.println("frameCount: " + frameCount + " time: " + (System.currentTimeMillis() - time));
				// long _time = System.currentTimeMillis();
				for (VisuableSchedulable vs : needToBeScheduleList)
					vs.paint();
			}
		};
		animationTimer.start();
	}

	synchronized public void stop() {
		// System.err.println("stop ViewerAnimationTimer " + ProcessHandle.current().pid());
		for (VisuableSchedulable visuableSchedulable : visuableSchedulables)
			visuableSchedulable.setAnimated(false);
		animationTimer.stop(); // NullPointerException double stop
		animationTimer = null;
	}
}
