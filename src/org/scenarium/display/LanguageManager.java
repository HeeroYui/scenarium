/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.display;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class LanguageManager {
	private static final String english = "English";
	private static final String french = "French";
	private static final String german = "German";
	private static final String italian = "Italian";
	public static final ArrayList<String> availableLanguage = new ArrayList<>(Arrays.asList(english, french, german, italian));
	private static final HashMap<String, String> dictionnary = new HashMap<>();

	public static String getText(String text) {
		String lText = dictionnary.get(text);
		return lText != null ? lText : text;
	}

	public static void loadDefaultLanguage() {
		String language = System.getProperty("user.language");
		String languageType = null;
		if (language.equals("fr"))
			languageType = french;
		else if (language.equals("de"))
			languageType = german;
		else if (language.equals("it"))
			languageType = italian;
		else
			languageType = english;
		loadLanguage(languageType);
	}

	public static boolean loadLanguage(String languageType) {
		dictionnary.clear();
		dictionnary.put("language", languageType);
		if (!languageType.equals(english))
			try {
				BufferedReader br = new BufferedReader(new InputStreamReader(LanguageManager.class.getResourceAsStream("/" + languageType + ".txt"), StandardCharsets.UTF_8));
				String line;
				while ((line = br.readLine()) != null) {
					int i = line.indexOf(":");
					dictionnary.put(line.substring(0, i), line.substring(i + 1, line.length()));
				}
			} catch (IOException e) {
				return false;
			}
		return true;
	}
}
