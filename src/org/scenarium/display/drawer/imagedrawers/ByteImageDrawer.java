/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.display.drawer.imagedrawers;

import java.util.Arrays;

import org.scenarium.display.drawer.ImageDrawer;

public class ByteImageDrawer {

	public static void drawFullImage(Object imageData, int imageType, int[] viewerData) {
		byte[] src = (byte[]) imageData;
		int offset = 0;
		switch (imageType) {
		case ImageDrawer.GRAY:
			for (int i = 0; i < src.length; i++) {
				byte gray = src[i];
				viewerData[i] = (255 << 24) + (gray << 16) + (gray << 8) + ((gray));
			}
			break;
		case ImageDrawer.RGB:
			for (int i = 0; i < src.length; i += 3)
				viewerData[offset++] = (255 << 24) + ((src[i] & 0xFF) << 16) + ((src[i + 1] & 0xFF) << 8) + (((src[i + 2] & 0xFF)));
			break;
		case ImageDrawer.BGR:
			for (int i = 0; i < src.length; i += 3)
				viewerData[offset++] = (255 << 24) + ((src[i + 2] & 0xFF) << 16) + ((src[i + 1] & 0xFF) << 8) + (((src[i] & 0xFF)));
			break;
		case ImageDrawer.ABGR:
			for (int i = 0; i < src.length; i += 4)
				viewerData[offset++] = (255 << 24) + ((src[i + 3] & 0xFF) << 16) + ((src[i + 2] & 0xFF) << 8) + (((src[i + 1] & 0xFF)));
			break;
		case ImageDrawer.YUV:
			for (int i = 0; i < src.length; i += 3) {
				int yi = src[i] & 0xFF;
				int ui = src[i + 1] & 0xFF;
				int vi = (src[i + 2]) & 0xFF;
				viewerData[offset++] = (255 << 24) + ((int) (yi + 1.13983f * vi) << 16) + ((int) (yi - 0.39465f * ui - 0.58060f * vi) << 8) + ((int) (yi + 2.03211f * ui) << 16);
			}
			break;
		}
	}

	public static void drawImage(Object imageData, int imageType, int imageWidth, int imageHeight, int[] viewerData, int startX, int startY, int width, int height, double startXOnRaster, double startYOnRaster, double invScale, int viewerWidth) {
		byte[] src = (byte[]) imageData;
		switch (imageType) {
		case ImageDrawer.GRAY:
			for (int y = height; y-- != startY;) {
				int yFIndex = y * viewerWidth;
				int yOnRaster = (int) startYOnRaster;
				if (startX > 0)
					Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
				if (yOnRaster < imageHeight) {
					int yIndex = yOnRaster * imageWidth;
					double xOnRaster = startXOnRaster;
					for (int x = width; x-- != startX;) {
						if (yIndex + (int) xOnRaster == 576000) {
							System.err.println();
							continue;
						}
						byte gray = src[yIndex + (int) xOnRaster];
						viewerData[yFIndex + x] = (255 << 24) + (gray << 16) + (gray << 8) + ((gray));
						xOnRaster -= invScale;
					}
				}
				startYOnRaster -= invScale;
				if (viewerWidth != width)
					Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
			}
			break;
		case ImageDrawer.RGB:
			for (int y = height; y-- != startY;) {
				int yFIndex = y * viewerWidth;
				int yOnRaster = (int) startYOnRaster;
				if (startX > 0)
					Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
				if (yOnRaster < imageHeight) {
					int yIndex = yOnRaster * imageWidth * 3;
					double xOnRaster = startXOnRaster;
					for (int x = width; x-- != startX;) {
						int index = yIndex + 3 * (int) xOnRaster;
						viewerData[yFIndex + x] = (255 << 24) + ((src[index] & 0xFF) << 16) + ((src[index + 1] & 0xFF) << 8) + (src[index + 2] & 0xFF);
						xOnRaster -= invScale;
					}
				}
				startYOnRaster -= invScale;
				if (viewerWidth != width)
					Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
			}
			break;
		case ImageDrawer.BGR:
			for (int y = height; y-- != startY;) {
				int yFIndex = y * viewerWidth;
				int yOnRaster = (int) startYOnRaster;
				if (startX > 0)
					Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
				if (yOnRaster < imageHeight) {
					int yIndex = yOnRaster * imageWidth * 3;
					double xOnRaster = startXOnRaster;
					for (int x = width; x-- != startX;) {
						int index = yIndex + 3 * (int) xOnRaster;
						viewerData[yFIndex + x] = (255 << 24) + ((src[index + 2] & 0xFF) << 16) + ((src[index + 1] & 0xFF) << 8) + (src[index] & 0xFF);
						xOnRaster -= invScale;
					}
				}
				startYOnRaster -= invScale;
				if (viewerWidth != width)
					Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
			}
			break;
		case ImageDrawer.ABGR:
			for (int y = height; y-- != startY;) {
				int yFIndex = y * viewerWidth;
				int yOnRaster = (int) startYOnRaster;
				if (startX > 0)
					Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
				if (yOnRaster < imageHeight) {
					int yIndex = yOnRaster * imageWidth * 4;
					double xOnRaster = startXOnRaster;
					for (int x = width; x-- != startX;) {
						int index = yIndex + 4 * (int) xOnRaster;
						viewerData[yFIndex + x] = (255 << 24) + ((src[index + 3] & 0xFF) << 16) + ((src[index + 2] & 0xFF) << 8) + ((src[index + 1]) & 0xFF);
						xOnRaster -= invScale;
					}
				}
				startYOnRaster -= invScale;
				if (viewerWidth != width)
					Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
			}
			break;
		case ImageDrawer.YUV:
			for (int y = height; y-- != startY;) {
				int yFIndex = y * viewerWidth;
				int yOnRaster = (int) startYOnRaster;
				if (startX > 0)
					Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
				if (yOnRaster < imageHeight) {
					int yIndex = yOnRaster * imageWidth * 3;
					double xOnRaster = startXOnRaster;
					for (int x = width; x-- != startX;) {
						int index = yIndex + 3 * (int) xOnRaster;
						int yi = src[index] & 0xFF;
						int ui = src[index + 1] & 0xFF;
						int vi = src[index + 2] & 0xFF;
						viewerData[yFIndex + x] = (255 << 24) + ((int) (yi + 1.13983f * vi) << 16) + ((int) (yi - 0.39465f * ui - 0.58060f * vi) << 8) + ((int) (yi + 2.03211f * ui) << 16);
						xOnRaster -= invScale;
					}
				}
				startYOnRaster -= invScale;
				if (viewerWidth != width)
					Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
			}
			break;
		}
	}

	public static Number getRGBValue(Object imageData, int imageWidth, int imageType, int x, int y, int z) {
		byte[] src = (byte[]) imageData;
		switch (imageType) {
		case ImageDrawer.GRAY:
			return src[y * imageWidth + x] & 0xFF;
		case ImageDrawer.RGB:
			return src[(y * imageWidth + x) * 3 + z] & 0xFF;
		case ImageDrawer.BGR:
			return src[(y * imageWidth + x) * 3 + (2 - z)] & 0xFF;
		case ImageDrawer.ABGR:
			return src[(y * imageWidth + x) * 4 + 1 + (2 - z)] & 0xFF;
		case ImageDrawer.YUV:
			return src[(y * imageWidth + x) * 3 + z] & 0xFF;
		}
		return -1;
	}
}
