/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.display.drawer.imagedrawers;

import java.util.Arrays;

import org.scenarium.display.drawer.ImageDrawer;

public class FloatImageDrawer {

	public static void drawFullImage(Object imageData, int imageType, int[] viewerData) {
		float[] src = (float[]) imageData;
		int offset = 0;
		switch (imageType) {
		case ImageDrawer.GRAY:
			for (int i = 0; i < src.length; i++) {
				byte gray = (byte) (src[i] * 255);
				viewerData[i] = (255 << 24) + (gray << 16) + (gray << 8) + ((gray));
			}
			break;
		case ImageDrawer.RGB:
			for (int i = 0; i < src.length; i += 3)
				viewerData[offset++] = (255 << 24) + ((byte) (src[i] * 255) << 16) + ((byte) (src[i + 1] * 255) << 8) + ((byte) (src[i + 2] * 255));
			break;
		case ImageDrawer.BGR:
			for (int i = 0; i < src.length; i += 3)
				viewerData[offset++] = (255 << 24) + ((byte) (src[i + 2] * 255) << 16) + ((byte) (src[i + 1] * 255) << 8) + ((byte) (src[i] * 255));
			break;
		case ImageDrawer.YUV:
			for (int i = 0; i < src.length; i += 3) {
				int yi = (int) src[i];
				int ui = (int) src[i + 1];
				int vi = (int) (src[i + 2]);
				viewerData[offset++] = (255 << 24) + ((int) (yi + 1.13983f * vi) << 16) + ((int) (yi - 0.39465f * ui - 0.58060f * vi) << 8) + ((int) (yi + 2.03211f * ui) << 16);
			}
			break;
		}
	}

	public static void drawImage(Object imageData, int imageType, int imageWidth, int imageHeight, int[] viewerData, int startX, int startY, int width, int height, double startXOnRaster, double startYOnRaster, double invScale, int viewerWidth) {
		float[] src = (float[]) imageData;
		switch (imageType) {
		case ImageDrawer.GRAY:
			for (int y = height; y-- != startY;) {
				int yFIndex = y * viewerWidth;
				int yOnRaster = (int) startYOnRaster;
				if (startX > 0)
					Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
				if (yOnRaster < imageHeight) {
					int yIndex = yOnRaster * imageWidth;
					double xOnRaster = startXOnRaster;
					for (int x = width; x-- != startX;) {
						byte gray = (byte) (src[yIndex + (int) xOnRaster] * 255);
						viewerData[yFIndex + x] = (255 << 24) + (gray << 16) + (gray << 8) + ((gray));
						xOnRaster -= invScale;
					}
				}
				startYOnRaster -= invScale;
				if (viewerWidth != width)
					Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
			}
			break;
		case ImageDrawer.RGB:
			for (int y = height; y-- != startY;) {
				int yFIndex = y * viewerWidth;
				int yOnRaster = (int) startYOnRaster;
				if (startX > 0)
					Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
				if (yOnRaster < imageHeight) {
					int yIndex = yOnRaster * imageWidth * 3;
					double xOnRaster = startXOnRaster;
					for (int x = width; x-- != startX;) {
						int index = yIndex + 3 * (int) xOnRaster;
						viewerData[yFIndex + x] = (255 << 24) + ((byte)(src[index] * 255) << 16) + ((byte)(src[index + 1] * 255) << 8) + ((int) (src[index + 2] * 255));
						xOnRaster -= invScale;
					}
				}
				startYOnRaster -= invScale;
				if (viewerWidth != width)
					Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
			}
			break;
		case ImageDrawer.BGR:
			for (int y = height; y-- != startY;) {
				int yFIndex = y * viewerWidth;
				int yOnRaster = (int) startYOnRaster;
				if (startX > 0)
					Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
				if (yOnRaster < imageHeight) {
					int yIndex = yOnRaster * imageWidth * 3;
					double xOnRaster = startXOnRaster;
					for (int x = width; x-- != startX;) {
						int index = yIndex + 3 * (int) xOnRaster;
						viewerData[yFIndex + x] = (255 << 24) + ((byte)(src[index + 2] * 255) << 16) + ((byte)(src[index + 1] * 255) << 8) + ((int) (src[index] * 255));
						xOnRaster -= invScale;
					}
				}
				startYOnRaster -= invScale;
				if (viewerWidth != width)
					Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
			}
			break;
		case ImageDrawer.YUV:
			for (int y = height; y-- != startY;) {
				int yFIndex = y * viewerWidth;
				int yOnRaster = (int) startYOnRaster;
				if (startX > 0)
					Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
				if (yOnRaster < imageHeight) {
					int yIndex = yOnRaster * imageWidth * 3;
					double xOnRaster = startXOnRaster;
					for (int x = width; x-- != startX;) {
						int index = yIndex + 3 * (int) xOnRaster;
						float yi = src[index] * 255;
						float ui = src[index + 1] * 255;
						float vi = (src[index + 2]) * 255;
						viewerData[yFIndex + x] = (255 << 24) + ((int) (yi + 1.13983f * vi) << 16) + ((int) (yi - 0.39465f * ui - 0.58060f * vi) << 8) + ((int) (yi + 2.03211f * ui) << 16);
						xOnRaster -= invScale;
					}
				}
				startYOnRaster -= invScale;
				if (viewerWidth != width)
					Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
			}
			break;
		}
	}
	
	public static Number getRGBValue(Object imageData, int imageWidth, int imageType, int x, int y, int z) {
		float[] src = (float[]) imageData;
		switch (imageType) {
		case ImageDrawer.GRAY:
			return src[y * imageWidth + x];
		case ImageDrawer.RGB:
			return src[(y * imageWidth + x) * 3 + z];
		case ImageDrawer.BGR:
			return src[(y * imageWidth + x) * 3 + (2 - z)];
		case ImageDrawer.ABGR:
			return src[(y * imageWidth + x) * 4 + 1 + (2 - z)];
		case ImageDrawer.YUV:
			return src[(y * imageWidth + x) * 3 + z];
		}
		return -1;
	}
}
