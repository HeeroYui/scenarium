/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.display.drawer.imagedrawers;

import java.util.Arrays;

import org.scenarium.display.drawer.ImageDrawer;

public class ShortImageDrawer {

	public static void drawFullImage(Object imageData, int imageType, int[] viewerData) {
		short[] src = (short[]) imageData;
		int offset = 0;
		switch (imageType) {
		case ImageDrawer.GRAY:
			for (int i = 0; i < src.length; i++) {
				byte gray = (byte) (src[i] >> 8);
				viewerData[i] = (255 << 24) + (gray << 16) + (gray << 8) + ((gray));
			}
			break;
		case ImageDrawer.RGB:
			for (int i = 0; i < src.length; i += 3)
				viewerData[offset++] = (255 << 24) + ((byte) (src[i] >> 8) << 16) + ((byte) (src[i + 1] >> 8) << 8) + ((byte) (src[i + 2] >> 8));
			break;
		case ImageDrawer.BGR:
			for (int i = 0; i < src.length; i += 3)
				viewerData[offset++] = (255 << 24) + ((byte) (src[i + 2] >> 8) << 16) + ((byte) (src[i + 1] >> 8) << 8) + ((byte) (src[i] >> 8));
			break;
		case ImageDrawer.YUV:
			for (int i = 0; i < src.length; i += 3) {
				int yi = src[i];
				int ui = src[i + 1];
				int vi = (src[i + 2]);
				viewerData[offset++] = (255 << 24) + ((int) (yi + 1.13983f * vi) << 16) + ((int) (yi - 0.39465f * ui - 0.58060f * vi) << 8) + ((int) (yi + 2.03211f * ui) << 16);
			}
			break;
		}
	}

	public static void drawImage(Object imageData, int imageType, int imageWidth, int imageHeight, int[] viewerData, int startX, int startY, int width, int height, double startXOnRaster, double startYOnRaster, double invScale, int viewerWidth) {
		short[] src = (short[]) imageData;
		switch (imageType) {
		case ImageDrawer.GRAY:
			for (int y = height; y-- != startY;) {
				int yFIndex = y * viewerWidth;
				int yOnRaster = (int) startYOnRaster;
				if (startX > 0)
					Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
				if (yOnRaster < imageHeight) {
					int yIndex = yOnRaster * imageWidth;
					double xOnRaster = startXOnRaster;
					for (int x = width; x-- != startX;) {
						byte gray = (byte) (src[yIndex + (int) xOnRaster] >> 8);
						viewerData[yFIndex + x] = (255 << 24) + (gray << 16) + (gray << 8) + ((gray));
						xOnRaster -= invScale;
					}
				}
				startYOnRaster -= invScale;
				if (viewerWidth != width)
					Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
			}
			break;
		case ImageDrawer.RGB:
			for (int y = height; y-- != startY;) {
				int yFIndex = y * viewerWidth;
				int yOnRaster = (int) startYOnRaster;
				if (startX > 0)
					Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
				if (yOnRaster < imageHeight) {
					int yIndex = yOnRaster * imageWidth * 3;
					double xOnRaster = startXOnRaster;
					for (int x = width; x-- != startX;) {
						int index = yIndex + 3 * (int) xOnRaster;
						viewerData[yFIndex + x] = (255 << 24) + ((src[index] >> 8) << 16) + ((src[index + 1] >> 8) << 8) + (src[index + 2] >> 8);
						xOnRaster -= invScale;
					}
				}
				startYOnRaster -= invScale;
				if (viewerWidth != width)
					Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
			}
			break;
		case ImageDrawer.BGR:
			for (int y = height; y-- != startY;) {
				int yFIndex = y * viewerWidth;
				int yOnRaster = (int) startYOnRaster;
				if (startX > 0)
					Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
				if (yOnRaster < imageHeight) {
					int yIndex = yOnRaster * imageWidth * 3;
					double xOnRaster = startXOnRaster;
					for (int x = width; x-- != startX;) {
						int index = yIndex + 3 * (int) xOnRaster;
						viewerData[yFIndex + x] = (255 << 24) + ((src[index + 2] >> 8) << 16) + ((src[index + 1] >> 8) << 8) + (src[index] >> 8);
						xOnRaster -= invScale;
					}
				}
				startYOnRaster -= invScale;
				if (viewerWidth != width)
					Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
			}
			break;
		case ImageDrawer.YUV:
			for (int y = height; y-- != startY;) {
				int yFIndex = y * viewerWidth;
				int yOnRaster = (int) startYOnRaster;
				if (startX > 0)
					Arrays.fill(viewerData, yFIndex, yFIndex + startX, 0);
				if (yOnRaster < imageHeight) {
					int yIndex = yOnRaster * imageWidth * 3;
					double xOnRaster = startXOnRaster;
					for (int x = width; x-- != startX;) {
						int index = yIndex + 3 * (int) xOnRaster;
						int yi = src[index];
						int ui = src[index + 1];
						int vi = (src[index + 2]);
						viewerData[yFIndex + x] = (255 << 24) + ((int) (yi + 1.13983f * vi) << 16) + ((int) (yi - 0.39465f * ui - 0.58060f * vi) << 8) + ((int) (yi + 2.03211f * ui) << 16);
						xOnRaster -= invScale;
					}
				}
				startYOnRaster -= invScale;
				if (viewerWidth != width)
					Arrays.fill(viewerData, yFIndex + width, yFIndex + viewerWidth, 0);
			}
			break;
		}
	}
	
	public static Number getRGBValue(Object imageData, int imageWidth, int imageType, int x, int y, int z) {
		short[] src = (short[]) imageData;
		switch (imageType) {
		case ImageDrawer.GRAY:
			return src[y * imageWidth + x] & 0xFFFF;
		case ImageDrawer.RGB:
			return src[(y * imageWidth + x) * 3 + z] & 0xFFFF;
		case ImageDrawer.BGR:
			return src[(y * imageWidth + x) * 3 + (2 - z)] & 0xFFFF;
		case ImageDrawer.ABGR:
			return src[(y * imageWidth + x) * 4 + 1 + (2 - z)] & 0xFFFF;
		case ImageDrawer.YUV:
			return src[(y * imageWidth + x) * 3 + z] & 0xFFFF;
		}
		return -1;
	}
}
