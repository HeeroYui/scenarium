/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.display.drawer;

import static org.scenarium.display.drawer.geo.OsmMercator.DEFAULT_TILE_SIZE;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

import javax.imageio.ImageIO;
import javax.vecmath.Point2d;

import org.beanmanager.editors.PropertyInfo;
import org.beanmanager.editors.primitive.number.ControlType;
import org.beanmanager.editors.primitive.number.NumberInfo;
import org.beanmanager.struct.BooleanProperty;
import org.beanmanager.struct.TreeNode;
import org.ejml.data.DMatrix2x2;
import org.ejml.data.DMatrixRMaj;
import org.ejml.dense.row.factory.DecompositionFactory_DDRM;
import org.ejml.interfaces.decomposition.EigenDecomposition_F64;
import org.scenarium.display.ScenariumContainer;
import org.scenarium.display.StackableDrawer;
import org.scenarium.display.drawer.geo.BingAerialTileSource;
import org.scenarium.display.drawer.geo.CycleMapTileSource;
import org.scenarium.display.drawer.geo.GeographicalMarker;
import org.scenarium.display.drawer.geo.GoogleHybridTileSource;
import org.scenarium.display.drawer.geo.GoogleRoadsOnlyTileSource;
import org.scenarium.display.drawer.geo.GoogleSatelliteOnlyTileSource;
import org.scenarium.display.drawer.geo.GoogleSomehowAlteredRoadmapTileSource;
import org.scenarium.display.drawer.geo.GoogleStandardRoadmapTileSource;
import org.scenarium.display.drawer.geo.GoogleTerrainOnlyTileSource;
import org.scenarium.display.drawer.geo.GoogleTerrainTileSource;
import org.scenarium.display.drawer.geo.LandscapeTileSource;
import org.scenarium.display.drawer.geo.MapQuestOSMAerialTileSource;
import org.scenarium.display.drawer.geo.MapQuestOSMTileSource;
import org.scenarium.display.drawer.geo.MapnikTileSource;
import org.scenarium.display.drawer.geo.MappyPhotoTileSource;
import org.scenarium.display.drawer.geo.MappyStandardTileSource;
import org.scenarium.display.drawer.geo.OsmMercator;
import org.scenarium.display.drawer.geo.TileSource;
import org.scenarium.struct.GeographicCoordinate;
import org.scenarium.struct.GeographicalPose;
import org.scenarium.struct.ScenariumProperties;
import org.scenarium.timescheduler.Scheduler;

import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Dimension2D;
import javafx.geometry.Point2D;
import javafx.scene.Cursor;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.transform.Affine;

public class GeographicalDrawer extends TheaterPanel implements StackableDrawer {
	private static final int maxMercatorTileZoom = 22;
	protected static final String GENERALFILTERS = "General";
	protected static final String BORDER = "Border";
	protected static final String GRID = "Grid";
	private static final String MAPNIK = "Mapnik";
	private static final String OSMCYCLEMAP = "OSM Cycle Map";
	private static final String BINGAERIALMAPS = "Bing Aerial Maps";
	private static final String MAPQUESTOSM = "MapQuest-OSM";
	private static final String MAPQUESTOPENAERIAL = "MapQuest Open Aerial";
	private static final String GOOGLEROADSONLY = "Google Roads Only";
	private static final String GOOGLESTANDARDROADMAP = "Google Standard Roadmap";
	private static final String GOOGLETERRAIN = "Google Terrain";
	private static final String GOOGLESOMEHOWALTEREDROADMAP = "Google Somehow Altered Roadmap";
	private static final String GOOGLESATELLITEONLY = "Google Satellite Only";
	private static final String GOOGLETERRAINONLY = "Google Terrain Only";
	private static final String GOOGLEHYBRID = "Google Hybrid";
	private static final String LANDSCAPE = "Landscape Maps";
	private static final String MAPPYPHOTO = "Mappy Photo Maps";
	private static final String MAPPYSTANDARD = "Mappy Standard Maps";
	private static final String GEODATAINFO = "Geographical Data Information";
	private static final Image waitingTile = new Image(GeographicalDrawer.class.getResourceAsStream("/waiting_tile.png"));
	private static final Image errorTile = new Image(GeographicalDrawer.class.getResourceAsStream("/error_tile.png"));

	public static void drawPose(GraphicsContext gc, Function<GeographicCoordinate, Point2D> geoToScreen, double zoom, Color color, GeographicCoordinate coord) {
		Point2D screenPos = geoToScreen.apply(coord);
		double[] posVar = coord.posVar;
		if (posVar != null)
			if (posVar[1] == 0 && posVar[2] == 0) {
				gc.setFill(new Color(0.5, 0.5, 0.5, 0.5));
				double w = zoom * posVar[0];
				double h = zoom * posVar[3];
				double x = screenPos.getX() - w / 2.0;
				double y = screenPos.getY() - h / 2.0;
				gc.fillOval(x, y, w, h); // lon lat
				gc.setStroke(color.darker());
				gc.strokeOval(x, y, w, h);
			} else {
				EigenDecomposition_F64<DMatrixRMaj> eigenDecomposition = DecompositionFactory_DDRM.eig(3, true);
				DMatrix2x2 mat = new DMatrix2x2(posVar[0], posVar[1], posVar[2], posVar[3]);
				eigenDecomposition.decompose(new DMatrixRMaj(mat));
				double axesWidth = zoom * Math.sqrt(eigenDecomposition.getEigenvalue(1).real);
				double axesHeight = zoom * Math.sqrt(eigenDecomposition.getEigenvalue(0).real);

				double angle = Math.atan2(mat.a21, mat.a11);
				Affine transform = gc.getTransform();
				Affine oldTranform = transform.clone();
				transform.appendTranslation(screenPos.getX(), screenPos.getY());
				transform.appendRotation(Math.toDegrees(-angle) - 90);
				gc.setTransform(transform);
				gc.setFill(new Color(0.5, 0.5, 0.5, 0.5));
				gc.fillOval(-axesWidth / 2.0, -axesHeight / 2.0, axesWidth, axesHeight); // lon lat
				gc.setStroke(color.darker());
				gc.strokeOval(-axesWidth / 2.0, -axesHeight / 2.0, axesWidth, axesHeight);
				gc.setTransform(oldTranform);
			}
		gc.setFill(color);
		double radius = 10;
		gc.fillOval(screenPos.getX() - radius / 2, screenPos.getY() - radius / 2, radius, radius);
		gc.setStroke(Color.BLACK);
		gc.strokeOval(screenPos.getX() - radius / 2, screenPos.getY() - radius / 2, radius, radius);
		if (coord instanceof GeographicalPose) {
			double lineLength = 20;
			double headingRad = Math.toRadians(((GeographicalPose) coord).heading);
			gc.strokeLine(screenPos.getX(), screenPos.getY(), screenPos.getX() + Math.sin(headingRad) * lineLength, screenPos.getY() - Math.cos(headingRad) * lineLength);
		}
	}

	public boolean filterROI = false;
	protected boolean filterBorder = true;
	protected boolean filterGrid = true;
	private boolean filterMapnik;
	private boolean filterOSMCycleMap;
	private boolean filterBingAerialMaps;
	private boolean filterMapQuestOSM;
	private boolean filterMapquestOpenAerial;
	private boolean filterGoogleRoadsOnly;
	private boolean filterGoogleStandardRoadmap;
	private boolean filterGoogleTerrain;
	private boolean filterGoogleSomehowAlteredRoadmap;
	private boolean filterGoogleSatelliteOnly;
	private boolean filterGoogleTerrainOnly;
	private boolean filterGoogleHybrid;
	private boolean filterLandscapeMaps;
	private boolean filterMappyPhoto;
	private boolean filterMappyStandard;

	private boolean filterGeoDataInfo;
	@PropertyInfo(index = 0, info = "Zoom level")
	@NumberInfo(min = 0, max = maxMercatorTileZoom, controlType = ControlType.SPINNER)
	private int zoom = 15;
	@PropertyInfo(index = 1, info = "Center the view around the coordinate of the scenario for each new data")
	private boolean followCenterPoint = true;
	@PropertyInfo(index = 2, info = "Center point of the view")
	protected GeographicCoordinate centerPoint;
	@PropertyInfo(index = 3, info = "Display tile loadings errors message")
	protected boolean displayTileLoadingMessageError;
	private TileSource tileSource;
	private ConcurrentHashMap<TileId, TileImage> loadingImg = new ConcurrentHashMap<>();
	private HashMap<Integer, HashMap<Integer, TileImage>> cache = new HashMap<>();
	protected OsmMercator om;
	private boolean dragg;
	private GeographicCoordinate anchorPointGeo;
	private Point2d anchorPoint;
	private Point2d mousTruePos = new Point2d();
	protected GraphicsContext gc;
	protected GraphicsContext gInfo;
	private GeographicCoordinate oldCoord;

	private int maxTile;

	@Override
	public boolean canAddInputToRenderer(Class<?>[] class1) {
		return true;
	}

	protected GeographicCoordinate cartoToGeo(Point2D pointXY) {
		return new GeographicCoordinate(om.yToLat(pointXY.getY(), zoom), om.xToLon(pointXY.getX(), zoom));
	}

	private void clearImageCache() {
		for (TileImage dImg : loadingImg.values()) // TODO garder celle que l'on aura encore, map pour la structure les contenant
			dImg.image.cancel();
		loadingImg.clear();
		cache.clear();
	}

	@Override
	public void death() {
		super.death();
		clearImageCache();
	}

	private static void drawImage(GraphicsContext g, TileImage tileImage, boolean upScaling, int i, int j, int deltaZoom) {
		if (tileImage == null || tileImage.isCancel)
			System.err.println("cannot drawImage, the image is null or cancelled");
		Image image = tileImage.image.isError() ? errorTile : tileImage.image.getProgress() != 1 ? waitingTile : tileImage.image;
		if (upScaling) {
			int deltaX = i % deltaZoom == 0 ? 0 : DEFAULT_TILE_SIZE * (i % deltaZoom) / deltaZoom;
			int deltaY = j % deltaZoom == 0 ? 0 : DEFAULT_TILE_SIZE * (j % deltaZoom) / deltaZoom;
			g.drawImage(image, deltaX, deltaY, DEFAULT_TILE_SIZE / deltaZoom, DEFAULT_TILE_SIZE / deltaZoom, tileImage.xDraw, tileImage.yDraw, 256, 256);
		} else
			g.drawImage(image, tileImage.xDraw, tileImage.yDraw);
	}

	@Override
	public void fitDocument() {
		repaint(false);
	}

	protected Point2D geoToCarto(GeographicCoordinate geo) {
		return new Point2D(om.lonToX(geo.longitude, zoom), om.latToY(geo.latitude, zoom));
	}

	protected Point2D geoToScreen(GeographicCoordinate geo) {
		double coordX = om.lonToX(geo.longitude, zoom) - om.lonToX(centerPoint.longitude, zoom) + getWidth() / 2; // je suis en tuille
		double coordY = om.latToY(geo.latitude, zoom) - om.latToY(centerPoint.latitude, zoom) + getHeight() / 2; // je suis en tuille
		return new Point2D(coordX, coordY);
	}

	public GeographicCoordinate getCenterPoint() {
		return centerPoint;
	}

	protected GeographicCoordinate getCoordinate() {
		return (GeographicCoordinate) getDrawableElement();// .clone()
	}

	@Override
	public Dimension2D getDimension() {
		return new Dimension2D(800, 600);
	}

	protected ArrayList<GeographicalMarker> getGeoMarkers() {
		Object[] add = getAdditionalDrawableElement();
		if (add == null)
			return null;
		ArrayList<GeographicalMarker> geoMarkers = new ArrayList<>();
		for (Object addInput : add)
			if (addInput instanceof GeographicalMarker[])
				for (GeographicalMarker o : (GeographicalMarker[]) addInput)
					geoMarkers.add(o);
			else
				geoMarkers.add((GeographicalMarker) addInput);
		return geoMarkers;
	}

	protected double getMeterPerPixel() {
		Point2D origin = new Point2D(5, 5);
		Point2D center = new Point2D(getWidth() / 2, getHeight() / 2);
		double pDistance = center.distance(origin);
		GeographicCoordinate originCoord = screenToGeo(origin);
		GeographicCoordinate centerCoord = screenToGeo(center);
		double mDistance = om.getDistance(originCoord.latitude, originCoord.longitude, centerCoord.latitude, centerCoord.longitude);
		return mDistance / pDistance;
	}

	@Override
	public String[] getStatusBarInfo() {
		GeographicCoordinate geopPos = screenToGeo(new Point2D(mousTruePos.x, mousTruePos.y));
//		double[] cartoPos = EarthProjectionLambert93WGS84.geoToCarto(Math.toRadians(geopPos.latitude), Math.toRadians(geopPos.longitude));
		return new String[] {/* "x: " + String.format("%f", cartoPos[0]) + "m", "y: " + String.format("%f", cartoPos[1]) + "m",*/ "lat: " + geopPos.latitude + "°", "lon: " + geopPos.longitude + "°", "Zoom: " + zoom,
				"TileX: " + (int) om.lonToX(geopPos.longitude, zoom) / DEFAULT_TILE_SIZE, "TileY: " + (int) om.latToY(geopPos.latitude, zoom) / DEFAULT_TILE_SIZE };
	}

	public int getZoom() {
		return zoom;
	}

	@Override
	public void initialize(ScenariumContainer container, Scheduler scheduler, Object dataElement, boolean autoFitIfResize) {
		super.initialize(container, scheduler, dataElement, autoFitIfResize);
		Canvas canvas = new Canvas();
		getChildren().add(canvas);
		canvas.widthProperty().bind(widthProperty());
		canvas.heightProperty().bind(heightProperty());
		gc = canvas.getGraphicsContext2D();
		Canvas infoCanvas = new Canvas();
		infoCanvas.setManaged(false);
		infoCanvas.widthProperty().bind(widthProperty());
		infoCanvas.heightProperty().bind(heightProperty());
		infoCanvas.setFocusTraversable(false);
		infoCanvas.setPickOnBounds(false);
		gInfo = infoCanvas.getGraphicsContext2D();
		getChildren().addAll(infoCanvas);
		if (followCenterPoint) {
			GeographicCoordinate newCoord = getCoordinate();
			if (newCoord != null)
				centerPoint = newCoord;
		}
		if (tileSource == null)
			tileSource = new MappyPhotoTileSource();
		updateZoom();
		File cacheTilePath = new File(ScenariumProperties.get().getMapTempPath() + tileSource.getPrefix());
		if (!cacheTilePath.exists())
			cacheTilePath.mkdirs();
		om = new OsmMercator();
		maxTile = (int) om.lonToX(180, zoom) / DEFAULT_TILE_SIZE;
		setOnScroll(e -> {
			if (e.isControlDown() && e.getDeltaY() != 0)
				zoom(e.getDeltaY() < 0, e.getX(), e.getY());
		});

		addEventHandler(MouseEvent.MOUSE_PRESSED, e -> {
			MouseButton button = e.getButton();
			if (button == MouseButton.SECONDARY) {
				// anchorPoint = getPosition(new Point2D(e.getX(), e.getY()));
				anchorPoint = new Point2d(e.getX(), e.getY());
				anchorPointGeo = screenToGeo(new Point2D(getWidth() / 2, getHeight() / 2));
				// anchorPoint = new GeographicCoordinate(-anchorPoint.latitude + centerPoint.latitude, -anchorPoint.longitude + centerPoint.longitude);
				dragg = true;
			}
		});

		addEventHandler(MouseEvent.MOUSE_DRAGGED, e -> {
			MouseButton button = e.getButton();
			if (button == MouseButton.SECONDARY) {
				if (!dragg) {
					setCursor(Cursor.MOVE);
					dragg = true;
				}
				double coordX = om.lonToX(anchorPointGeo.longitude, zoom);
				double coordY = om.latToY(anchorPointGeo.latitude, zoom);
				coordX -= e.getX() - anchorPoint.x;
				coordY -= e.getY() - anchorPoint.y;
				centerPoint = new GeographicCoordinate(om.yToLat(coordY, zoom), om.xToLon(coordX, zoom));
				repaint(false);
				e.consume();
			}
		});
		addEventHandler(MouseEvent.MOUSE_RELEASED, e -> {
			MouseButton button = e.getButton();
			if (button == MouseButton.SECONDARY) {
				dragg = false;
				anchorPoint = null;
				e.consume();
			}
		});
		addEventHandler(MouseEvent.MOUSE_MOVED, e -> {
			mousTruePos.x = (int) e.getX();
			mousTruePos.y = (int) e.getY();
		});
	}

	public boolean isFollowCenterPoint() {
		return followCenterPoint;
	}

	@Override
	public boolean isValidAdditionalInput(Class<?>[] inputsType, Class<?> additionalInput) {
		return additionalInput == null ? false : GeographicalMarker.class.isAssignableFrom(additionalInput) || GeographicalMarker[].class.isAssignableFrom(additionalInput);
	}

	@Override
	public void paint(Object dataElement) {
		GeographicCoordinate newCoord = getCoordinate();
		if (followCenterPoint && newCoord != null && !newCoord.equals(oldCoord))
			centerPoint = newCoord;
		GraphicsContext g = gc;
		g.setFill(Color.WHITE);
		g.fillRect(0, 0, getWidth(), getHeight());
		double coordX = om.lonToX(centerPoint.longitude, zoom) / DEFAULT_TILE_SIZE; // je suis en tuille
		double coordY = om.latToY(centerPoint.latitude, zoom) / DEFAULT_TILE_SIZE;
		int beginX = (int) coordX;
		int beginY = (int) coordY;
		int xCenter = (int) (getWidth() / 2) - (int) ((coordX - beginX) * DEFAULT_TILE_SIZE);
		int yCenter = (int) (getHeight() / 2) - (int) ((coordY - beginY) * DEFAULT_TILE_SIZE);
		int beginTileX = -(xCenter + DEFAULT_TILE_SIZE - 1) / DEFAULT_TILE_SIZE;
		int beginTileY = -(yCenter + DEFAULT_TILE_SIZE - 1) / DEFAULT_TILE_SIZE;
		int endTileX = ((int) getWidth() - (xCenter + DEFAULT_TILE_SIZE) + DEFAULT_TILE_SIZE - 1) / DEFAULT_TILE_SIZE + 1;
		int endTileY = ((int) getHeight() - (yCenter + DEFAULT_TILE_SIZE) + DEFAULT_TILE_SIZE - 1) / DEFAULT_TILE_SIZE + 1;
		int idScale = zoom;
		boolean upScaling = zoom > tileSource.getMaxZoom();
		int deltaZoom = 0;
		if (upScaling) {
			idScale = tileSource.getMaxZoom();
			deltaZoom = (int) Math.pow(2, zoom - tileSource.getMaxZoom());
		}
		int minX = beginX + beginTileX;
		int maxX = beginX + endTileX;
		int minY = beginY + beginTileY;
		int maxY = beginY + endTileY;

		// Cancel des anciennes tuiles
		for (TileId tileId : loadingImg.keySet()) {
			TileImage tileImage = loadingImg.get(tileId);
			tileImage.usedCpt = 0;
			if (tileId.scale != idScale || tileId.x < minX || tileId.x > maxX || tileId.y < minY || tileId.y > maxY) {
				tileImage.image.cancel();
				tileImage.isCancel = true;
				loadingImg.remove(tileId);
			} else { // Je garde et recalcul la position
				int i = tileId.x - beginX;
				int j = tileId.y - beginY;
				int xc = xCenter + i * DEFAULT_TILE_SIZE;
				int yc = yCenter + j * DEFAULT_TILE_SIZE;
				tileImage.xDraw = xc;
				tileImage.yDraw = yc;
			}
		}
		cache.values().forEach(a -> a.values().forEach(b -> b.usedCpt = 0));
		for (int i = beginTileX; i < endTileX; i++)
			for (int j = beginTileY; j < endTileY; j++) {
				int idX = beginX + i;
				int idY = beginY + j;
				if (idX < 0 || idY < 0 || idX >= maxTile || idY >= maxTile)
					continue;
				if (upScaling) {
					idX /= deltaZoom;
					idY /= deltaZoom;
				}
				int xc = xCenter + i * DEFAULT_TILE_SIZE;
				int yc = yCenter + j * DEFAULT_TILE_SIZE;
				// Chargement à partir du cache ram
				HashMap<Integer, TileImage> idc = cache.get(idX);
				if (idc != null) {
					TileImage img = idc.get(idY);
					if (img != null)
						if (img.isCancel)
							idc.remove(idY);
						else {
							// System.out.println("charge du cache: " + idX + " " + idY);
							img.usedCpt++;
							TileImage tileImage = img;
							tileImage.xDraw = xc;
							tileImage.yDraw = yc;
							drawImage(g, tileImage, upScaling, beginX + i, beginY + j, deltaZoom);
							continue;
						}
				}
				// Chargement à partir des images en cours de chargement
				TileImage tileImage = loadingImg.get(new TileId(idX, idY, idScale));
				if (tileImage != null) {
					if (tileImage.usedCpt == 0) {
						tileImage.xDraw = xc;
						tileImage.yDraw = yc;
						drawImage(g, tileImage, upScaling, beginX + i, beginY + j, deltaZoom);
					} else {
						tileImage.addPlaceToDraw(xc, yc, beginX + i, beginY + j, deltaZoom);
						drawImage(g, new TileImage(tileImage.image, xc, yc), upScaling, beginX + i, beginY + j, deltaZoom);
					}
					continue;
				}
				// Chargement à partir du cache disque dur
				File imgFile = new File(ScenariumProperties.get().getMapTempPath() + tileSource.getPrefix() + "/" + idScale + "_" + idX + "_" + idY + ".png");
				if (imgFile.exists()) {
					tileImage = new TileImage(new Image(imgFile.toURI().toString()), xc, yc);
					if (!tileImage.image.isError())
						putImageInCache(idX, idY, tileImage);
					drawImage(g, tileImage, upScaling, beginX + i, beginY + j, deltaZoom);
					// System.out.println("charge du disque dur: " + idX + " " + idY);
				} else
					try {
						// Chargement à partir d'internet
						// System.out.println("charge d'internet: " + idX + " " + idY);
						TileImage loadingTileImage = new TileImage(new Image(tileSource.getTileUrl(idScale, idX, idY), true), xc, yc);
						TileId tileId = new TileId(idX, idY, idScale);
						loadingImg.put(tileId, loadingTileImage);
						drawImage(g, loadingTileImage, upScaling, beginX + i, beginY + j, deltaZoom);
						int _idX = idX;
						int _idy = idY;
						int _i = i;
						int _j = j;
						int _deltaZoom = deltaZoom;
						loadingTileImage.image.progressProperty().addListener((a, b, progress) -> {
							if ((Double) progress == 1.0) {
								if (loadingTileImage.image.getException() instanceof CancellationException)
									return;
								drawImage(g, loadingTileImage, upScaling, beginX + _i, beginY + _j, _deltaZoom);
								if (loadingTileImage.additionalPlaceToDraw != null)
									for (int[] p : loadingTileImage.additionalPlaceToDraw) {
										loadingTileImage.xDraw = p[0];
										loadingTileImage.yDraw = p[1];
										drawImage(g, loadingTileImage, upScaling, p[2], p[3], p[4]);
									}
								if (!loadingTileImage.image.isError()) {
									putImageInCache(_idX, _idy, loadingTileImage);
									new Thread(() -> {
										BufferedImage bImage = SwingFXUtils.fromFXImage(loadingTileImage.image, null);
										try {
											ImageIO.write(bImage, "png", imgFile);
											loadingImg.remove(tileId);
										} catch (Exception e) {
											e.printStackTrace();
										}
									}).start();
								} else if (displayTileLoadingMessageError)
									System.err.println(loadingTileImage.image.getException());
							}
						});
					} catch (IOException e1) {
						g.drawImage(errorTile, xc, yc);
						if (displayTileLoadingMessageError)
							System.err.println(e1);
					}
			}

		ArrayList<Integer> toRemoveX = new ArrayList<>();
		ArrayList<Integer> toRemoveY = new ArrayList<>();
		cache.forEach((idX, idXs) -> {
			toRemoveY.clear();
			idXs.forEach((idY, idYs) -> {
				if (idYs.usedCpt == 0)
					toRemoveY.add(idY);
			});
			for (Integer idY : toRemoveY)
				idXs.remove(idY);
			if (idXs.isEmpty())
				toRemoveX.add(idX);
		});
		for (Integer idX : toRemoveX)
			cache.remove(idX);

		gInfo.clearRect(0, 0, getWidth(), getHeight());
		if (filterGrid) {
			gInfo.setStroke(Color.BLACK);
			int end = xCenter + endTileX * DEFAULT_TILE_SIZE;
			for (int i = xCenter + beginTileX * DEFAULT_TILE_SIZE; i <= end; i += DEFAULT_TILE_SIZE)
				gInfo.strokeLine(i + 0.5, 0 + 0.5, i + 0.5, getHeight() + 0.5);
			end = yCenter + endTileY * DEFAULT_TILE_SIZE;
			for (int i = yCenter + beginTileY * DEFAULT_TILE_SIZE; i <= end; i += DEFAULT_TILE_SIZE)
				gInfo.strokeLine(0 + 0.5, i + 0.5, getWidth() + 0.5, i + 0.5);
		}
		GeographicalDrawer.drawPose(gInfo, this::geoToScreen, 1 / getMeterPerPixel(), Color.YELLOW, getCoordinate());
		if (filterGeoDataInfo) {
			ArrayList<String> infos = new ArrayList<>();
			infos.add("latitude:\t" + newCoord.latitude + (newCoord.posVar != null && newCoord.posVar[1] == 0 && newCoord.posVar[2] == 0 ? "\t+-" + newCoord.posVar[0] : ""));
			infos.add("longitude:\t" + newCoord.longitude + (newCoord.posVar != null && newCoord.posVar[1] == 0 && newCoord.posVar[2] == 0 ? "\t+-" + newCoord.posVar[3] : ""));
			if (!Double.isNaN(newCoord.altitude))
				infos.add("altitude:\t" + newCoord.altitude + (!Double.isNaN(newCoord.altitude) ? "\t+-" + newCoord.stdAltitude : ""));
			if (newCoord instanceof GeographicalPose) {
				GeographicalPose newPose = (GeographicalPose) newCoord;
				if (!Double.isNaN(newPose.heading))
					infos.add("heading:\t" + newPose.heading + (!Double.isNaN(newPose.stdHeading) ? "\t+-" + newPose.stdHeading : ""));
				if (!Double.isNaN(newPose.roll))
					infos.add("roll:  \t" + newPose.roll + (!Double.isNaN(newPose.stdRoll) ? "\t+-" + newPose.stdRoll : ""));
				if (!Double.isNaN(newPose.altitude))
					infos.add("pitch:\t" + newPose.pitch + (!Double.isNaN(newPose.stdPitch) ? "\t+-" + newPose.stdPitch : ""));
			}
			double height = new Text("").getLayoutBounds().getHeight();
			double maxWidth = 0;
			for (String info : infos) {
				double width = new Text(info).getLayoutBounds().getWidth();
				if (width > maxWidth)
					maxWidth = width;
			}
			gInfo.setFill(new Color(1, 1, 1, 0.6));
			gInfo.fillRect(0, 0, maxWidth + 10, infos.size() * height + 8);
			gInfo.setFill(Color.BLACK);
			for (int i = 0; i < infos.size(); i++)
				gInfo.fillText(infos.get(i), 5, height + i * height);
		}
		ArrayList<GeographicalMarker> geoMarkers = getGeoMarkers();
		if (geoMarkers != null)
			for (GeographicalMarker geoMarker : geoMarkers)
				if (geoMarker != null)
					geoMarker.draw(gInfo, this::geoToScreen);
		oldCoord = newCoord;
	}

	@Override
	public void populateExclusiveFilter(ArrayList<List<String>> exclusiveFilters) {
		exclusiveFilters.add(List.of(MAPNIK, OSMCYCLEMAP, BINGAERIALMAPS, MAPQUESTOSM, MAPQUESTOPENAERIAL, GOOGLEROADSONLY, GOOGLESTANDARDROADMAP, GOOGLETERRAIN, GOOGLESOMEHOWALTEREDROADMAP, GOOGLESATELLITEONLY, GOOGLETERRAINONLY, GOOGLEHYBRID,
				LANDSCAPE, MAPPYSTANDARD, MAPPYPHOTO));
	}

	@Override
	protected void populateTheaterFilter() {
		super.populateTheaterFilter();

		TreeNode<BooleanProperty> filtersMap = new TreeNode<>(new BooleanProperty(GENERALFILTERS, true));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(BORDER, true)));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(GRID, true)));
		theaterFilter.addChild(filtersMap);
		filtersMap = new TreeNode<>(new BooleanProperty("Maps", true));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(MAPNIK, false)));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(OSMCYCLEMAP, false)));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(BINGAERIALMAPS, false)));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(MAPQUESTOSM, false)));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(MAPQUESTOPENAERIAL, false)));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(GOOGLEROADSONLY, false)));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(GOOGLESTANDARDROADMAP, false)));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(GOOGLETERRAIN, false)));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(GOOGLESOMEHOWALTEREDROADMAP, false)));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(GOOGLESATELLITEONLY, false)));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(GOOGLETERRAINONLY, false)));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(GOOGLEHYBRID, false)));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(LANDSCAPE, false)));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(MAPPYPHOTO, true)));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(MAPPYSTANDARD, false)));
		theaterFilter.addChild(filtersMap);
		theaterFilter.addChild(new TreeNode<>(new BooleanProperty(GEODATAINFO, false)));
	}

	private void putImageInCache(int idX, int idY, TileImage tileImage) {
		HashMap<Integer, TileImage> idca = cache.get(idX);
		if (idca == null) {
			idca = new HashMap<>();
			cache.put(idX, idca);
		}
		idca.put(idY, tileImage);
	}

	protected Point2D screenToCarto(Point2D point) {
		double coordX = om.lonToX(centerPoint.longitude, zoom) - (getWidth() / 2 - point.getX());
		double coordY = om.latToY(centerPoint.latitude, zoom) - (getHeight() / 2 - point.getY());
		return new Point2D(coordX, coordY);
	}

	protected GeographicCoordinate screenToGeo(Point2D point2d) {
		return cartoToGeo(screenToCarto(point2d));
	}

	public void setCenterPoint(GeographicCoordinate centerPoint) {
		this.centerPoint = centerPoint;
		repaint(false);
	}

	public void setFollowCenterPoint(boolean followCenterPoint) {
		this.followCenterPoint = followCenterPoint;
		repaint(false);
	}

	public void setZoom(int zoom) {
		if (tileSource != null && zoom >= Math.min(tileSource.getMaxZoom() + 10, maxMercatorTileZoom))
			zoom = Math.min(tileSource.getMaxZoom() + 10, maxMercatorTileZoom);
		else if (zoom <= 0)
			zoom = 0;
		this.zoom = zoom;
		clearImageCache();
		if (om != null)
			maxTile = (int) om.lonToX(180, zoom) / DEFAULT_TILE_SIZE;
		repaint(false);
	}

	@Override
	public boolean updateFilterWithPath(String[] filterPath, boolean value) {
		String filterName = filterPath[filterPath.length - 1];
		if (filterName.equals(GRID))
			filterGrid = value;
		else if (filterName.equals(BORDER))
			filterBorder = value;
		else if (filterName.equals(MAPNIK)) {
			filterMapnik = value;
			updateTileSource();
		} else if (filterName.equals(OSMCYCLEMAP)) {
			filterOSMCycleMap = value;
			updateTileSource();
		} else if (filterName.equals(BINGAERIALMAPS)) {
			filterBingAerialMaps = value;
			updateTileSource();
		} else if (filterName.equals(MAPQUESTOSM)) {
			filterMapQuestOSM = value;
			updateTileSource();
		} else if (filterName.equals(MAPQUESTOPENAERIAL)) {
			filterMapquestOpenAerial = value;
			updateTileSource();
		} else if (filterName.equals(GOOGLEROADSONLY)) {
			filterGoogleRoadsOnly = value;
			updateTileSource();
		} else if (filterName.equals(GOOGLESTANDARDROADMAP)) {
			filterGoogleStandardRoadmap = value;
			updateTileSource();
		} else if (filterName.equals(GOOGLETERRAIN)) {
			filterGoogleTerrain = value;
			updateTileSource();
		} else if (filterName.equals(GOOGLESOMEHOWALTEREDROADMAP)) {
			filterGoogleSomehowAlteredRoadmap = value;
			updateTileSource();
		} else if (filterName.equals(GOOGLESATELLITEONLY)) {
			filterGoogleSatelliteOnly = value;
			updateTileSource();
		} else if (filterName.equals(GOOGLETERRAINONLY)) {
			filterGoogleTerrainOnly = value;
			updateTileSource();
		} else if (filterName.equals(GOOGLEHYBRID)) {
			filterGoogleHybrid = value;
			updateTileSource();
		} else if (filterName.equals(LANDSCAPE)) {
			filterLandscapeMaps = value;
			updateTileSource();
		} else if (filterName.equals(MAPPYSTANDARD)) {
			filterMappyStandard = value;
			updateTileSource();
		} else if (filterName.equals(MAPPYPHOTO)) {
			filterMappyPhoto = value;
			updateTileSource();
		} else if (filterName.equals(GEODATAINFO))
			filterGeoDataInfo = value;
		/* else if (filterName.equals(FITTOMARKER)) { filterFitToMarker = value; if (filterFitToMarker && map != null) map.setDisplayToFitMapMarkers(); } else if (filterName.equals(INFO)) { filterInfo = value; repaint(); } */ else
			return false;
		repaint(false);
		return true;
	}

	private void updateTileSource() {
		TileSource tileSource = null;
		if (filterMapnik)
			tileSource = new MapnikTileSource();
		else if (filterOSMCycleMap)
			tileSource = new CycleMapTileSource();
		else if (filterBingAerialMaps)
			tileSource = new BingAerialTileSource();
		else if (filterMapQuestOSM)
			tileSource = new MapQuestOSMTileSource();
		else if (filterMapquestOpenAerial)
			tileSource = new MapQuestOSMAerialTileSource();
		else if (filterGoogleRoadsOnly)
			tileSource = new GoogleRoadsOnlyTileSource();
		else if (filterGoogleStandardRoadmap)
			tileSource = new GoogleStandardRoadmapTileSource();
		else if (filterGoogleTerrain)
			tileSource = new GoogleTerrainTileSource();
		else if (filterGoogleSomehowAlteredRoadmap)
			tileSource = new GoogleSomehowAlteredRoadmapTileSource();
		else if (filterGoogleSatelliteOnly)
			tileSource = new GoogleSatelliteOnlyTileSource();
		else if (filterGoogleTerrainOnly)
			tileSource = new GoogleTerrainOnlyTileSource();
		else if (filterGoogleHybrid)
			tileSource = new GoogleHybridTileSource();
		else if (filterLandscapeMaps)
			tileSource = new LandscapeTileSource();
		else if (filterMappyStandard)
			tileSource = new MappyStandardTileSource();
		else if (filterMappyPhoto)
			tileSource = new MappyPhotoTileSource();
		if (tileSource != null) {
			this.tileSource = tileSource;
			clearImageCache();
			File cacheTilePath = new File(ScenariumProperties.get().getMapTempPath() + tileSource.getPrefix());
			if (!cacheTilePath.exists())
				cacheTilePath.mkdirs();
			updateZoom();
			// cache.clear();
		}
	}

	private void updateZoom() {
		if(zoom < this.tileSource.getMinZoom())
			setZoom(this.tileSource.getMinZoom());
		else if(zoom > this.tileSource.getMaxZoom())
			setZoom(this.tileSource.getMaxZoom());
	}

	// public static void main(String[] args) {
	// for (int i = 0; i < 100000000; i++) {
	// if(i%10000 == 0)
	// System.out.println(i);
	// double a = -1000 + Math.random() * 2000;// 1;
	// double b = -1000 + Math.random() * 2000;//2;
	// double c = -1000 + Math.random() * 2000;//3;
	// double d = -1000 + Math.random() * 2000;//2;
	//
	// EigenDecomposition_F64<DMatrixRMaj> eigenDecomposition = DecompositionFactory_DDRM.eig(true);
	// DMatrix2x2 mat2 = new DMatrix2x2(a, b, c, d); //res: 4, -1
	// double[] res = PolynomialsTools.root(new double[] {a * d - c * b, -(a + d), 1});//-4, -3, 1
	// eigenDecomposition.decompose(new DMatrixRMaj(mat2));
	//// System.out.println(eigenDecomposition.getEigenvalue(1).real);
	//// System.out.println(eigenDecomposition.getEigenvalue(0).real);
	// double res0 = eigenDecomposition.getEigenvalue(0).real;
	// double res1 = eigenDecomposition.getEigenvalue(1).real;
	//
	//
	// double[] res2 = new double[2];
	// double max = Math.max(Math.max(Math.abs(a), Math.abs(b)), Math.max(Math.abs(c), Math.abs(d)));
	// value2x2(a, b, c, d, res2);
	//// res2[0] *= max;
	//// res2[1] *= max;
	//
	// try {
	// if(Math.abs(res0 - res2[0]) > 0.000001 || Math.abs(res1 - res2[1]) > 0.000001)
	// System.err.println("fail");
	// }catch(ArrayIndexOutOfBoundsException e) {
	// System.out.println("res0: " + res0 + " res1: " + res1);
	// double[] resBis = PolynomialsTools.root(new double[] {a * d - c * b, -(a + d), 1});//-4, -3, 1
	// e.printStackTrace();
	// }
	// }
	// }
	//
	// public static void value2x2( double a11 , double a12, double a21 , double a22, double[] res){
	// double c,s;
	// if( a12 + a21 == 0 )
	// c = s = 1.0 / Math.sqrt(2);
	// else {
	// double aa = (a11-a22);
	// double bb = (a12+a21);
	// double t_hat = aa/bb;
	// double t = t_hat/(1.0 + Math.sqrt(1.0+t_hat*t_hat));
	// c = 1.0/ Math.sqrt(1.0+t*t);
	// s = c*t;
	// }
	// double c2 = c*c;
	// double s2 = s*s;
	// double cs = c*s;
	// double b11 = c2*a11 + s2*a22 - cs*(a12+a21);
	// double b12 = c2*a12 - s2*a21 + cs*(a11-a22);
	// double b21 = c2*a21 - s2*a12 + cs*(a11-a22);
	// if( b21*b12 >= 0 ) {
	// if( b12 == 0 ) {
	// c = 0;
	// s = 1;
	// } else {
	// s = Math.sqrt(b21/(b12+b21));
	// c = Math.sqrt(b12/(b12+b21));
	// }
	// cs = c*s;
	// a11 = b11 - cs*(b12 + b21);
	// a22 = b11 + cs*(b12 + b21);
	// res[0] = a11;
	// res[1] = a22;
	// } else {
	// res[0] = b11;
	// res[1] = b11;
	// }
	// }

	private void zoom(boolean in, double x, double y) {
		int newZoom = zoom;
		int minScale = tileSource.getMinZoom();
		int maxScale = Math.min(tileSource.getMaxZoom() + 10, maxMercatorTileZoom); // 23 Maximum available zoom
		if (in)
			newZoom--;
		else
			newZoom++;
		if (newZoom < minScale)
			newZoom = minScale;
		else if (newZoom > maxScale)
			newZoom = maxScale;
		if (zoom != newZoom)
			setZoom(newZoom);
	}

	public boolean isDisplayTileLoadingMessageError() {
		return displayTileLoadingMessageError;
	}

	public void setDisplayTileLoadingMessageError(boolean displayTileLoadingMessageError) {
		this.displayTileLoadingMessageError = displayTileLoadingMessageError;
	}
}

class TileImage {
	public ArrayList<int[]> additionalPlaceToDraw;
	public boolean isCancel;
	public Image image;
	public int xDraw;
	public int yDraw;
	public int usedCpt;

	public TileImage(Image image, int xDraw, int yDraw) {
		this.image = image;
		this.usedCpt = 1;
		this.xDraw = xDraw;
		this.yDraw = yDraw;
	}

	public void addPlaceToDraw(int xc, int yc, int i, int j, int deltaZoom) {
		if (additionalPlaceToDraw == null)
			additionalPlaceToDraw = new ArrayList<>();
		additionalPlaceToDraw.add(new int[] { xc, yc, i, j, deltaZoom });
	}

	@Override
	public String toString() {
		return "";
	}
}
