/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.display.drawer.geo;

import java.io.IOException;

public interface TileSource {
	public int getMaxZoom();

	public int getMinZoom();

	public String getPrefix();

	public String getTileUrl(int zoom, int tilex, int tiley) throws IOException;
}
