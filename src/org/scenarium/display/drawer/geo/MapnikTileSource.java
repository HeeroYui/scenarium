/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.display.drawer.geo;

public class MapnikTileSource implements TileSource {
	private int serveurNumber = 0;

	@Override
	public int getMaxZoom() {
		return 19;
	}

	@Override
	public int getMinZoom() {
		return 0;
	}

	@Override
	public String getPrefix() {
		return "Mapnik";
	}

	@Override
	public String getTileUrl(int zoom, int tilex, int tiley) {
		serveurNumber++;
		if (serveurNumber == 3)
			serveurNumber = 0;
		return "https://" + (serveurNumber == 0 ? "a" : serveurNumber == 1 ? "b" : "c") + ".tile.openstreetmap.org/" + zoom + "/" + tilex + "/" + tiley + ".png";
	}
}
