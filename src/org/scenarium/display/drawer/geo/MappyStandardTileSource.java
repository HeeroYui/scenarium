/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.display.drawer.geo;

public class MappyStandardTileSource implements TileSource {
	private int serveurNumber = 1;

	@Override
	public int getMaxZoom() {
		return 19;
	}

	@Override
	public int getMinZoom() {
		return 0;
	}

	@Override
	public String getPrefix() {
		return "MappyStandard";
	}

	@Override
	public String getTileUrl(int zoom, int tilex, int tiley) {
		serveurNumber++;
		if (serveurNumber == 10)
			serveurNumber = 1;
		return "https://map" + serveurNumber + ".mappy.net/map/1.0/slab/standard/256/" + zoom + "/" + tilex + "/" + tiley;
	}
}
