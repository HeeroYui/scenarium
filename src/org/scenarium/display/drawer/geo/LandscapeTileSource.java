/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.display.drawer.geo;

public class LandscapeTileSource implements TileSource {

	@Override
	public int getMaxZoom() {
		return 19;
	}

	@Override
	public int getMinZoom() {
		return 0;
	}

	@Override
	public String getPrefix() {
		return "Landscape";
	}

	@Override
	public String getTileUrl(int zoom, int tilex, int tiley) {
		return "http://c.tile.thunderforest.com/landscape/" + zoom + "/" + tilex + "/" + tiley + ".png";
	}
}
