/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.display.drawer;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Set;
import java.util.function.BiConsumer;

import javax.vecmath.Point2d;
import javax.vecmath.Point2i;

import org.beanmanager.editors.DynamicEnableBean;
import org.beanmanager.editors.PropertyInfo;
import org.beanmanager.editors.container.ArrayInfo;
import org.beanmanager.struct.BooleanProperty;
import org.beanmanager.struct.TreeNode;
import org.scenarium.display.ColorProvider;
import org.scenarium.display.ScenariumContainer;
import org.scenarium.display.StackableDrawer;
import org.scenarium.struct.curve.BoxMarker;
import org.scenarium.struct.curve.Curve;
import org.scenarium.struct.curve.CurveSeries;
import org.scenarium.struct.curve.Curved;
import org.scenarium.struct.curve.Curvei;
import org.scenarium.struct.curve.EvolvedCurveSeries;
import org.scenarium.timescheduler.Scheduler;

import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Dimension2D;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

public class ChartDrawer extends TheaterPanel implements StackableDrawer, DynamicEnableBean {
	protected static final String LINKS = "Links";
	protected static final String SYMBOLS = "Symbols";
	protected static final String COORDINATES = "Coordinates";
	protected static final String INDICES = "Indices";
	private ColorProvider colorProvider = new ColorProvider();
	private Data<Number, Number> first = new LineChart.Data<>(0, 0);
	private Data<Number, Number> last = new LineChart.Data<>(1, 1);
	private Canvas chartCanvas;
	private Canvas selectionCanvas;
	private Region drawRegion;
	private double ratioX;
	private double ratioY;
	private double offsetX;
	private double offsetY;
	private Point2i anchorPoint;
	private Point2i draggPoint;
	private NumberAxis xAxis;
	private NumberAxis yAxis;
	private double minX = Double.NaN;
	private double maxX = Double.NaN;
	private double minY = Double.NaN;
	private double maxY = Double.NaN;
	private LineChart<Number, Number> chart;
	private Point2i mousePostion = new Point2i();
	private LineChart.Series<Number, Number> series;
	private Point2D oldFirst;
	private Point2D oldLast;
	private ArrayList<Curve> curves;
	@PropertyInfo(index = 0, info = "Auto compute x bounds based on curves bounds")
	private boolean autoFitXaxis = true;
	@PropertyInfo(index = 1, info = "x bounds for the chart")
	private Point2d xBounds = new Point2d(0, 1);
	@PropertyInfo(index = 2, info = "Auto compute y bounds based on curves bounds")
	private boolean autoFitYaxis = true;
	@PropertyInfo(index = 3, info = "y bounds for the chart")
	private Point2d yBounds = new Point2d(0, 1);
	@PropertyInfo(index = 4, info = "Auto compute color bounds based on curves values bounds")
	private boolean autoFitValuesBounds = true;
	@PropertyInfo(index = 5, info = "bounds for the values")
	private Point2d valuesBounds = new Point2d(0, 1);
	private boolean boundsChanged;
	private double margin = 0.0;
	private ArrayList<double[]> previousTransform = new ArrayList<>();
	private ChartLabel tempChartLabel;
	private ArrayList<ChartLabel> chartLabels;
	private EventHandler<KeyEvent> keyReleasedHandler;
	private EventHandler<KeyEvent> keyPressedHandler;

	@Override
	public boolean canAddInputToRenderer(Class<?>[] class1) {
		return true;
	}

	private static void computeBounds(Curve curve, double[] res) {
		double[][] datas = curve.getDatad();
		double[] xs = datas[0];
		double[] ys = datas[1];
		if (xs.length == 0) {
			res[0] = 0;
			res[1] = 1;
			res[2] = 0;
			res[3] = 1;
			return;
		}
		double minX = xs[0];
		double maxX = minX;
		double minY = ys[0];
		double maxY = minY;
		for (int i = xs.length; i-- != 0;) {
			double val = xs[i];
			if (val < minX)
				minX = val;
			else if (val > maxX)
				maxX = val;
			val = ys[i];
			if (val < minY)
				minY = val;
			else if (val > maxY)
				maxY = val;
		}
		res[0] = minX;
		res[1] = maxX;
		res[2] = minY;
		res[3] = maxY;
	}

	private static void computeValueBounds(double[] values, double[] bounds) {
		double minZ = values[0];
		double maxZ = minZ;
		for (int i = values.length; i-- != 0;) {
			double val = values[i];
			if (val < minZ)
				minZ = val;
			else if (val > maxZ)
				maxZ = val;
		}
		bounds[0] = minZ;
		bounds[1] = maxZ;
	}

	private static void computeValueBounds(int[] values, int[] bounds) {
		int minZ = values[0];
		int maxZ = minZ;
		for (int i = values.length; i-- != 0;) {
			int val = values[i];
			if (val < minZ)
				minZ = val;
			else if (val > maxZ)
				maxZ = val;
		}
		bounds[0] = minZ;
		bounds[1] = maxZ;
	}

	private static void computeXBounds(Curve curve, double[] res) {
		double[][] datas = curve.getDatad();
		double[] xs = datas[0];
		double minX = xs[0];
		double maxX = minX;
		for (int i = xs.length; i-- != 0;) {
			double val = xs[i];
			if (val < minX)
				minX = val;
			else if (val > maxX)
				maxX = val;
		}
		res[0] = minX;
		res[1] = maxX;
	}

	private static void computeYBounds(Curve curve, double[] res) {
		double[][] datas = curve.getDatad();
		double[] ys = datas[1];
		double minY = ys[0];
		double maxY = minY;
		for (int i = ys.length; i-- != 0;) {
			double val = ys[i];
			if (val < minY)
				minY = val;
			else if (val > maxY)
				maxY = val;
		}
		res[0] = minY;
		res[1] = maxY;
	}

	@Override
	public void death() {
		super.death();
		getChildren().clear();
		if (keyPressedHandler != null)
			removeEventHandler(KeyEvent.KEY_PRESSED, keyPressedHandler);
		if (keyReleasedHandler != null)
			removeEventHandler(KeyEvent.KEY_RELEASED, keyReleasedHandler);
		keyPressedHandler = null;
		keyReleasedHandler = null;
	}

	private void drawChart() {
		GraphicsContext g = chartCanvas.getGraphicsContext2D();
		g.clearRect(0, 0, chartCanvas.getWidth(), chartCanvas.getHeight());
		if (true/* filterChart */) {
			colorProvider.resetIndex();
			ArrayList<TreeNode<BooleanProperty>> sons = theaterFilter.getChildren();
			double width = getWidth();
			double height = getHeight();
			for (int i = 0; i < curves.size(); i++) {
				Curve curve = curves.get(i);
				String name = curve.getName();
				if (name == null)
					name = Integer.toString(i);
				ArrayList<TreeNode<BooleanProperty>> curveFilters = sons.get(i).getChildren();
				boolean filterLinks = curveFilters.get(0).getValue().value;
				boolean filterSymbols = curveFilters.get(1).getValue().value;
				boolean filterCoordinates = curveFilters.get(2).getValue().value;
				boolean filterIndices = curveFilters.get(3).getValue().value;

				boolean additionalFilter = filterSymbols || filterIndices || filterCoordinates;
				double textHeight = filterCoordinates ? new Text("").getLayoutBounds().getHeight() : 0;
				Boolean isVisible = additionalFilter || filterLinks;
				if (!isVisible) {
					colorProvider.incrementColorIndex();
					continue;
				}
				if (curve instanceof Curved) {
					double[][] datas = ((Curved) curve).getData();
					double[] xDatas = datas[0];
					double[] yDatas = datas[1];
					int dataLength = xDatas.length;
					Color color = colorProvider.getNextColor();
					if (dataLength != 0) {
						double oldX = (xDatas[dataLength - 1] - offsetX) * ratioX;
						double oldY = (yDatas[dataLength - 1] - offsetY) * ratioY;
						double[] values = ((Curved) curve).getValues();
						if (values == null) {
							if (filterLinks) {
								g.beginPath();
								g.moveTo(oldX, oldY);
							}
							g.setStroke(color);
							for (int k = dataLength; k-- != 0;) {
								double x = (xDatas[k] - offsetX) * ratioX;
								double y = (yDatas[k] - offsetY) * ratioY;
								// g.strokeLine(oldX, oldY, x, y);
								if (filterLinks)
									g.lineTo(x, y);
								if (additionalFilter && x >= 0 && x <= width && y >= 0 && y <= height) {
									if (filterSymbols) {
										g.setFill(g.getStroke());
										drawShape(g, colorProvider.getCurrentIndex(), x, y);
									}
									if (filterIndices) {
										g.setFill(Color.BLACK);
										g.fillText(Integer.toString(k), x, y);
									}
									if (filterCoordinates) {
										g.setFill(Color.BLACK);
										double yPos = y;
										if (filterIndices)
											yPos += textHeight;
										g.fillText(Double.toString(xDatas[k]), x, yPos);
										yPos += textHeight;
										g.fillText(Double.toString(yDatas[k]), x, yPos);
									}
								}
								// oldX = x;
								// oldY = y;
							}

							double x = (xDatas[dataLength - 1] - offsetX) * ratioX;
							double y = (yDatas[dataLength - 1] - offsetY) * ratioY;
							// g.strokeLine(oldX, oldY, x, y);
							if (additionalFilter && x >= 0 && x <= width && y >= 0 && y <= height) {
								if (filterSymbols) {
									g.setFill(g.getStroke());
									drawShape(g, colorProvider.getCurrentIndex(), x, y);
								}
								if (filterIndices) {
									g.setFill(Color.BLACK);
									g.fillText(Integer.toString(dataLength - 1), x, y);
								}
								if (filterCoordinates) {
									g.setFill(Color.BLACK);
									double yPos = y;
									if (filterIndices)
										yPos += textHeight;
									g.fillText(Double.toString(xDatas[dataLength - 1]), x, yPos);
									yPos += textHeight;
									g.fillText(Double.toString(yDatas[dataLength - 1]), x, yPos);
								}
							}
							if (filterLinks)
								g.stroke();
						} else {
							double min, max;
							if (autoFitValuesBounds) {
								double[] bounds = new double[2];
								computeValueBounds(((Curved) curve).getValues(), bounds);
								min = bounds[0];
								max = bounds[1];
							} else {
								min = valuesBounds.x;
								max = valuesBounds.y;
							}
							double offsetZ = max == min ? max - 1 : min;
							double ratioZ = max == min ? 1 : 1 / (max - min);
							double red = color.getRed();
							double green = color.getGreen();
							double blue = color.getBlue();
							for (int k = dataLength - 1; k-- != 0;) {
								double x = (xDatas[k] - offsetX) * ratioX;
								double y = (yDatas[k] - offsetY) * ratioY;
								double z = (values[k] - offsetZ) * ratioZ;
								if (z < 0)
									z = 0;
								else if (z > 1)
									z = 1;
								g.setStroke(new Color(z * red, z * green, z * blue, 1));
								g.strokeLine(oldX, oldY, x, y);
								if (additionalFilter && x >= 0 && x <= width && y >= 0 && y <= height) {
									if (filterSymbols) {
										g.setFill(g.getStroke());
										drawShape(g, colorProvider.getCurrentIndex(), x, y);
									}
									if (filterIndices) {
										g.setFill(Color.BLACK);
										g.fillText(Integer.toString(k), x, y);
									}
									if (filterCoordinates) {
										g.setFill(Color.BLACK);
										double yPos = y;
										if (filterIndices)
											yPos += textHeight;
										g.fillText(Double.toString(xDatas[k]), x, yPos);
										yPos += textHeight;
										g.fillText(Double.toString(yDatas[k]), x, yPos);
										yPos += textHeight;
										g.fillText(Double.toString(values[k]), x, yPos);
									}
								}
								oldX = x;
								oldY = y;
							}

							double x = (xDatas[dataLength - 1] - offsetX) * ratioX;
							double y = (yDatas[dataLength - 1] - offsetY) * ratioY;
							double z = (values[dataLength - 1] - offsetZ) * ratioZ;
							if (z < 0)
								z = 0;
							else if (z > 1)
								z = 1;
							g.setStroke(new Color(z * red, z * green, z * blue, 1));
							if (additionalFilter && x >= 0 && x <= width && y >= 0 && y <= height) {
								if (filterSymbols) {
									g.setFill(g.getStroke());
									drawShape(g, colorProvider.getCurrentIndex(), x, y);
								}
								if (filterIndices) {
									g.setFill(Color.BLACK);
									g.fillText(Integer.toString(dataLength - 1), x, y);
								}
								if (filterCoordinates) {
									g.setFill(Color.BLACK);
									double yPos = y;
									if (filterIndices)
										yPos += textHeight;
									g.fillText(Double.toString(xDatas[dataLength - 1]), x, yPos);
									yPos += textHeight;
									g.fillText(Double.toString(yDatas[dataLength - 1]), x, yPos);
									yPos += textHeight;
									g.fillText(Double.toString(values[dataLength - 1]), x, yPos);
								}
							}
							oldX = x;
							oldY = y;
						}
					}
				} else {
					int[][] datas = ((Curvei) curve).getData();
					int[] xDatas = datas[0];
					int[] yDatas = datas[1];
					int dataLength = xDatas.length;
					Color color = colorProvider.getNextColor();
					if (dataLength != 0) {
						double oldX = (xDatas[dataLength - 1] - offsetX) * ratioX;
						double oldY = (yDatas[dataLength - 1] - offsetY) * ratioY;
						int[] values = ((Curvei) curve).getValues();
						if (values == null) {
							if (filterLinks) {
								g.beginPath();
								g.moveTo(oldX, oldY);
							}
							g.setStroke(color);
							for (int k = dataLength - 1; k-- != 0;) {
								double x = (xDatas[k] - offsetX) * ratioX;
								double y = (yDatas[k] - offsetY) * ratioY;
								// g.strokeLine(oldX, oldY, x, y);
								if (filterLinks)
									g.lineTo(x, y);
								if (additionalFilter && x >= 0 && x <= width && y >= 0 && y <= height) {
									if (filterSymbols) {
										g.setFill(g.getStroke());
										drawShape(g, colorProvider.getCurrentIndex(), x, y);
									}
									if (filterIndices) {
										g.setFill(Color.BLACK);
										g.fillText(Integer.toString(k), x, y);
									}
									if (filterCoordinates) {
										g.setFill(Color.BLACK);
										double yPos = y;
										if (filterIndices)
											yPos += textHeight;
										g.fillText(Double.toString(xDatas[k]), x, yPos);
										yPos += textHeight;
										g.fillText(Double.toString(yDatas[k]), x, yPos);
									}
								}
								// oldX = x;
								// oldY = y;
							}

							double x = (xDatas[dataLength - 1] - offsetX) * ratioX;
							double y = (yDatas[dataLength - 1] - offsetY) * ratioY;
							if (additionalFilter && x >= 0 && x <= width && y >= 0 && y <= height) {
								if (filterSymbols) {
									g.setFill(g.getStroke());
									drawShape(g, colorProvider.getCurrentIndex(), x, y);
								}
								if (filterIndices) {
									g.setFill(Color.BLACK);
									g.fillText(Integer.toString(dataLength - 1), x, y);
								}
								if (filterCoordinates) {
									g.setFill(Color.BLACK);
									double yPos = y;
									if (filterIndices)
										yPos += textHeight;
									g.fillText(Double.toString(xDatas[dataLength - 1]), x, yPos);
									yPos += textHeight;
									g.fillText(Double.toString(yDatas[dataLength - 1]), x, yPos);
								}
							}
							// oldX = x;
							// oldY = y;
							if (filterLinks)
								g.stroke();
						} else {
							double min, max;
							if (autoFitValuesBounds) {
								int[] bounds = new int[2];
								computeValueBounds(((Curvei) curve).getValues(), bounds);
								min = bounds[0];
								max = bounds[1];
							} else {
								min = valuesBounds.x;
								max = valuesBounds.y;
							}
							double offsetZ = max == min ? max - 1 : min;
							double ratioZ = max == min ? 1 : 1 / (max - min);
							double red = color.getRed();
							double green = color.getGreen();
							double blue = color.getBlue();
							for (int k = dataLength - 1; k-- != 0;) {
								double x = (xDatas[k] - offsetX) * ratioX;
								double y = (yDatas[k] - offsetY) * ratioY;
								double z = (values[k] - offsetZ) * ratioZ;
								if (z < 0)
									z = 0;
								else if (z > 1)
									z = 1;
								g.setStroke(new Color(z * red, z * green, z * blue, 1));
								g.strokeLine(oldX, oldY, x, y);
								if (additionalFilter && x >= 0 && x <= width && y >= 0 && y <= height) {
									if (filterSymbols) {
										g.setFill(g.getStroke());
										drawShape(g, colorProvider.getCurrentIndex(), x, y);
									}
									if (filterIndices) {
										g.setFill(Color.BLACK);
										g.fillText(Integer.toString(k), x, y);
									}
									if (filterCoordinates) {
										g.setFill(Color.BLACK);
										double yPos = y;
										if (filterIndices)
											yPos += textHeight;
										g.fillText(Double.toString(xDatas[k]), x, yPos);
										yPos += textHeight;
										g.fillText(Double.toString(yDatas[k]), x, yPos);
										yPos += textHeight;
										g.fillText(Double.toString(values[k]), x, yPos);
									}
								}
								oldX = x;
								oldY = y;
							}

							double x = (xDatas[dataLength - 1] - offsetX) * ratioX;
							double y = (yDatas[dataLength - 1] - offsetY) * ratioY;
							double z = (values[dataLength - 1] - offsetZ) * ratioZ;
							if (z < 0)
								z = 0;
							else if (z > 1)
								z = 1;
							g.setStroke(new Color(z * red, z * green, z * blue, 1));
							if (additionalFilter && x >= 0 && x <= width && y >= 0 && y <= height) {
								if (filterSymbols) {
									g.setFill(g.getStroke());
									drawShape(g, colorProvider.getCurrentIndex(), x, y);
								}
								if (filterIndices) {
									g.setFill(Color.BLACK);
									g.fillText(Integer.toString(dataLength - 1), x, y);
								}
								if (filterCoordinates) {
									g.setFill(Color.BLACK);
									double yPos = y;
									if (filterIndices)
										yPos += textHeight;
									g.fillText(Double.toString(xDatas[dataLength - 1]), x, yPos);
									yPos += textHeight;
									g.fillText(Double.toString(yDatas[dataLength - 1]), x, yPos);
									yPos += textHeight;
									g.fillText(Double.toString(values[dataLength - 1]), x, yPos);
								}
							}
							oldX = x;
							oldY = y;
						}
					}
				}
			}
		}
		Object dataElement = getDrawableElement();
		if (dataElement instanceof EvolvedCurveSeries) {
			EvolvedCurveSeries ecs = (EvolvedCurveSeries) dataElement;
			ArrayList<BoxMarker> boxMarkers = ecs.getBoxmarkers();
			if (boxMarkers != null) {
				g.setStroke(Color.BLACK);
				g.setFill(Color.BLACK);
				g.setTextAlign(TextAlignment.CENTER);
				g.setFont(new Font(10));
				for (BoxMarker boxMarker : boxMarkers) {
					Rectangle rect = new Rectangle(boxMarker.x, boxMarker.y, boxMarker.width, boxMarker.height);
					double x = (rect.getX() - offsetX) * ratioX;
					double y = (rect.getY() + rect.getHeight() - offsetY) * ratioY;
					double width = rect.getWidth() * ratioX;
					double height = -rect.getHeight() * ratioY;
					g.strokeRect(x, y, width, height);
					String[] lines = boxMarker.name.split("\n");
					for (int j = 0; j < lines.length; j++)
						g.fillText(lines[j], x + width / 2, y + height / 2 + j * 10);
				}
				double begin = ecs.getBegin();
				double end = ecs.getEnd();
				if (begin != end) {
					g.setFill(new Color(0.5, 0.5, 0.5, 0.7));
					begin = (begin - offsetX) * ratioX;
					g.fillRect((begin - offsetX) * ratioX, 0, (end - offsetX) * ratioX - begin, getHeight());
				}
			}
		}
	}

	private void drawSelection() {
		GraphicsContext g = selectionCanvas.getGraphicsContext2D();
		g.clearRect(0, 0, selectionCanvas.getWidth(), selectionCanvas.getHeight());
		if (anchorPoint != null && draggPoint != null) {
			g.setStroke(Color.BLACK);
			g.strokeRect(anchorPoint.getX() - 0.5, anchorPoint.getY() - 0.5, draggPoint.getX() - anchorPoint.getX(), draggPoint.getY() - anchorPoint.getY());
		}
		if (tempChartLabel != null)
			tempChartLabel.updatePos(chartCanvas, (tempChartLabel.curve.getDatad()[0][tempChartLabel.index] - offsetX) * ratioX, (tempChartLabel.curve.getDatad()[1][tempChartLabel.index] - offsetY) * ratioY);
		if (chartLabels != null)
			for (ChartLabel cl : chartLabels)
				cl.updatePos(chartCanvas, (cl.curve.getDatad()[0][cl.index] - offsetX) * ratioX, (cl.curve.getDatad()[1][cl.index] - offsetY) * ratioY);
	}

	private static void drawShape(GraphicsContext g, int colorIndex, double x, double y) {
		switch (colorIndex) {
		case 0:
			g.fillRect(x - 5, y - 5, 10, 10);
			break;
		case 1:
			g.fillOval(x - 5, y - 5, 10, 10);
			break;
		case 2:
			g.fillPolygon(new double[] { x - 5, x, x + 5, x }, new double[] { y, y + 5, y, y - 5 }, 4);
			break;
		case 3:
			g.fillPolygon(new double[] { x - 5, x, x + 5 }, new double[] { y + 5, y - 5, y + 5 }, 3); // tri bas
			break;
		case 4:
			g.fillRect(x - 5, y - 2, 10, 5);
			break;
		case 5:
			g.fillPolygon(new double[] { x - 5, x, x + 5 }, new double[] { y - 5, y + 5, y - 5 }, 3); // tri droite
			break;
		case 6:
			g.fillRect(x - 2, y - 5, 5, 10);
			break;
		case 7:
			g.fillPolygon(new double[] { x - 5, x + 5, x - 5 }, new double[] { y + 5, y, y - 5 }, 3); // tri droite
			break;
		default:
			break;
		}
	}

	@Override
	public void fitDocument() {
		if (Double.isNaN(minX) || Double.isNaN(minY) || Double.isNaN(maxX) || Double.isNaN(maxY))
			return;
		if (first.getXValue().doubleValue() != minX)
			first.setXValue(minX);
		if (first.getYValue().doubleValue() != minY)
			first.setYValue(minY);
		if (last.getXValue().doubleValue() != maxX)
			last.setXValue(maxX);
		if (last.getYValue().doubleValue() != maxY)
			last.setYValue(maxY);
	}

	private Object[] getClosestPoint(double xp, double yp) {
		double minDist = Double.MAX_VALUE;
		int indexOfMin = 0;
		Curve closestCurve = null;
		ArrayList<TreeNode<BooleanProperty>> sons = theaterFilter.getChildren();
		for (int i = 0; i < curves.size(); i++) {
			Curve curve = curves.get(i);
			String name = curve.getName();
			if (name == null)
				name = Integer.toString(i);
			ArrayList<TreeNode<BooleanProperty>> curveFilters = sons.get(i).getChildren();
			boolean filterLinks = curveFilters.get(0).getValue().value;
			boolean filterSymbols = curveFilters.get(1).getValue().value;
			boolean filterCoordinates = curveFilters.get(2).getValue().value;
			boolean filterIndices = curveFilters.get(3).getValue().value;

			boolean additionalFilter = filterSymbols || filterIndices || filterCoordinates;
			if (additionalFilter || filterLinks) {
				double[][] data = curve.getDatad();
				double[] xs = data[0];
				double[] ys = data[1];
				for (int j = 0; j < data[0].length; j++) {
					double x = Math.abs(xs[j] - xp) * ratioX;
					double y = Math.abs(ys[j] - yp) * ratioY;
					double dist = x * x + y * y;
					if (dist < minDist) {
						indexOfMin = j;
						closestCurve = curve;
						minDist = dist;
					}
				}
			}
		}
		return closestCurve == null ? null : new Object[] { closestCurve, indexOfMin };
	}

	@Override
	public Dimension2D getDimension() {
		return new Dimension2D(640, 480);
	}

	public double getMargin() {
		return margin;
	}

	@Override
	public String[] getStatusBarInfo() {
		return new String[] { xAxis.getLabel() + ": " + (mousePostion.x / ratioX + offsetX), yAxis.getLabel() + ": " + (mousePostion.y / ratioY + offsetY) };
	}

	public Point2d getValuesBounds() {
		return valuesBounds;
	}

	public Point2d getxBounds() {
		return xBounds;
	}

	public Point2d getyBounds() {
		return yBounds;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void initialize(ScenariumContainer container, Scheduler scheduler, Object drawableElement, boolean autoFitIfResize) {
		super.initialize(container, scheduler, drawableElement, autoFitIfResize);
		if (!autoFitXaxis) {
			first.setXValue(xBounds.x);
			last.setXValue(xBounds.y);
		}
		if (!autoFitYaxis) {
			first.setYValue(yBounds.x);
			last.setYValue(yBounds.y);
		}
		series = new LineChart.Series<>();
		series.setName("");
		series.getData().addAll(first, last);

		curves = new ArrayList<>();
		// Création du graphique.
		xAxis = new NumberAxis(first.getXValue().doubleValue(), last.getXValue().doubleValue(), 1);
		xAxis.setLabel("x");
		xAxis.setLowerBound(first.getXValue().doubleValue());
		xAxis.setUpperBound(last.getXValue().doubleValue());
		yAxis = new NumberAxis(first.getYValue().doubleValue(), last.getYValue().doubleValue(), 1);
		yAxis.setLabel("y");
		yAxis.setLowerBound(first.getYValue().doubleValue());
		yAxis.setUpperBound(last.getYValue().doubleValue());
		// yAxis.prefWidthProperty().bind(widthprop);

		chart = new LineChart<>(xAxis, yAxis);
		chart.setAnimated(false);
		chart.setCreateSymbols(false);
		chart.getData().add(series);
		chart.setStyle("CHART_COLOR_1: transparent;");

		Pane drawPane = (Pane) chart.lookup(".chart-content");
		drawPane.setPadding(new Insets(5, 20, 0, 0));
		drawRegion = (Region) drawPane.getChildren().get(0);

		chart.setPadding(new Insets(5, 0, 0, 0));
		Node chartBackground = chart.lookup(".chart-plot-background");
		chartBackground.setStyle("-fx-background-color: #C0C0C0;");
		chart.lookup(".chart-vertical-grid-lines").setStyle("-fx-stroke: #DEDEDE;");
		chart.lookup(".chart-horizontal-grid-lines").setStyle("-fx-stroke: #DEDEDE;");

		final StackPane root = new StackPane();
		chartCanvas = new Canvas();
		chartCanvas.setManaged(false);
		chartCanvas.getGraphicsContext2D().setTextAlign(TextAlignment.CENTER);
		chartCanvas.widthProperty().bind(drawRegion.widthProperty());
		chartCanvas.heightProperty().bind(drawRegion.heightProperty());
		selectionCanvas = new Canvas();
		selectionCanvas.setManaged(false);
		selectionCanvas.widthProperty().bind(drawRegion.widthProperty());
		selectionCanvas.heightProperty().bind(drawRegion.heightProperty());
		selectionCanvas.setVisible(true);
		selectionCanvas.setFocusTraversable(false);
		selectionCanvas.setPickOnBounds(false);
		selectionCanvas.setMouseTransparent(true);
		root.getChildren().addAll(chart, chartCanvas, selectionCanvas);

		chartCanvas.setOnMousePressed(e -> {
			if (e.getButton() == MouseButton.PRIMARY && e.isControlDown()) {
				Object[] closestData = getClosestPoint(e.getX() / ratioX + offsetX, e.getY() / ratioY + offsetY);
				if (closestData == null)
					return;
				if (tempChartLabel == null || tempChartLabel.curve != closestData[0] || tempChartLabel.index != (int) closestData[1]) {
					Curve selectedCurve = (Curve) closestData[0];
					int selectedIndex = (int) closestData[1];
					drawSelection();
					double[][] selectedCurveData = selectedCurve.getDatad();
					double valX = selectedCurveData[0][selectedIndex];
					double valY = selectedCurveData[1][selectedIndex];
					double x = (valX - offsetX) * ratioX;
					double y = (valY - offsetY) * ratioY;
					root.getChildren().remove(tempChartLabel);
					Point2D pt = chartCanvas.localToParent(new Point2D(x, y));

					tempChartLabel = new ChartLabel(pt.getX(), pt.getY(), 10, selectedCurve, selectedIndex, xAxis.getLabel(), yAxis.getLabel(), (chartLabel, isPin) -> {
						if (isPin) {
							if (chartLabels == null)
								chartLabels = new ArrayList<>();
							chartLabels.add(chartLabel);
							if (chartLabel == tempChartLabel)
								tempChartLabel = null;
						} else {
							if (tempChartLabel != null)
								root.getChildren().remove(tempChartLabel);
							tempChartLabel = chartLabel;
						}
					});
					tempChartLabel.curve = selectedCurve;
					tempChartLabel.index = selectedIndex;
					tempChartLabel.updatePos(chartCanvas, x, y);
					tempChartLabel.updateInfo();
					StackPane.setAlignment(tempChartLabel, Pos.TOP_LEFT);
					root.getChildren().add(tempChartLabel);
				}
			} else if (e.getButton() == MouseButton.PRIMARY || e.getButton() == MouseButton.SECONDARY) {
				anchorPoint = new Point2i((int) e.getX(), (int) e.getY());
				if (e.getButton() == MouseButton.SECONDARY) {
					oldFirst = new Point2D(first.getXValue().doubleValue(), first.getYValue().doubleValue());
					oldLast = new Point2D(last.getXValue().doubleValue(), last.getYValue().doubleValue());
				}
			}
			selectionCanvas.setVisible(true);
		});
		chartCanvas.setOnMouseDragged(e -> {
			if (e.getButton() == MouseButton.PRIMARY && e.isControlDown()) {
				Object[] closestData = getClosestPoint(e.getX() / ratioX + offsetX, e.getY() / ratioY + offsetY);
				tempChartLabel.curve = (Curve) closestData[0];
				tempChartLabel.index = (int) closestData[1];
				drawSelection();
				// }
			} else if (e.getButton() == MouseButton.PRIMARY) {
				draggPoint = new Point2i((int) e.getX(), (int) e.getY());
				drawSelection();
			} else if (e.getButton() == MouseButton.SECONDARY && anchorPoint != null) {
				setCursor(Cursor.MOVE);
				double dx = anchorPoint.x / ratioX - e.getX() / ratioX;
				double dy = anchorPoint.y / ratioY - e.getY() / ratioY;
				first.setXValue(oldFirst.getX() + dx);
				first.setYValue(oldFirst.getY() + dy);
				last.setXValue(oldLast.getX() + dx);
				last.setYValue(oldLast.getY() + dy);
				updateCanvasParam();
				repaint(false);
			}
		});
		chartCanvas.setOnMouseMoved(e -> mousePostion = new Point2i((int) e.getX(), (int) e.getY()));
		chartCanvas.setOnMouseReleased(e -> {
			if (getCursor() != Cursor.DEFAULT)
				setCursor(Cursor.DEFAULT);
			if (anchorPoint != null && draggPoint != null) {
				double beginX = anchorPoint.x / ratioX + offsetX;
				double endX = draggPoint.x / ratioX + offsetX;
				double beginY = draggPoint.y / ratioY + offsetY;
				double endY = anchorPoint.y / ratioY + offsetY;
				boolean isReinit = false;
				boolean hasChanged = false;
				if (endX - beginX < 0) {
					if (first.getXValue().doubleValue() != minX || last.getXValue().doubleValue() != maxX) {
						first.setXValue(minX);
						last.setXValue(maxX);
						hasChanged = true;
					}
					isReinit = true;
				}
				if (endY - beginY < 0) {
					if (first.getYValue().doubleValue() != minY || last.getYValue().doubleValue() != maxY) {
						first.setYValue(minY);
						last.setYValue(maxY);
						hasChanged = true;
					}
					isReinit = true;
				}
				if (!isReinit) {
					if (endX - beginX > 1E-5) {
						if (first.getXValue().doubleValue() != beginX || last.getXValue().doubleValue() != endX) {
							first.setXValue(beginX);
							last.setXValue(endX);
							hasChanged = true;
						}
					} else
						showMessage("Too small X", true);
					if (endY - beginY > 1E-5) {
						if (first.getYValue().doubleValue() != beginY || last.getYValue().doubleValue() != endY) {
							first.setYValue(beginY);
							last.setYValue(endY);
						}
					} else
						showMessage("Too small Y", true);
				}
				if (hasChanged) {
					updateCanvasParam();
					repaint(false);
				}
			}
			anchorPoint = null;
			draggPoint = null;
			if (tempChartLabel != null)
				selectionCanvas.setVisible(true);
			if (e.getButton() == MouseButton.PRIMARY && !e.isControlDown()) {
				root.getChildren().remove(tempChartLabel);
				tempChartLabel = null;
			}
			pushTransform();
		});
		chartCanvas.setOnScroll(e -> {
			if(e.getDeltaY() != 0) {
				double xf = first.getXValue().doubleValue();
				double xl = last.getXValue().doubleValue();
				double yf = first.getYValue().doubleValue();
				double yl = last.getYValue().doubleValue();
				double distX = xl - xf;
				double distY = yl - yf;
				double ratioX = (chartCanvas.getWidth() - e.getX()) / chartCanvas.getWidth();
				double invRatioX = 1 - ratioX;
				double ratioY = (chartCanvas.getHeight() - e.getY()) / chartCanvas.getHeight();
				double invRatioY = 1 - ratioY;
				distX = distX / 10;
				distY = distY / 10;
				if (e.getDeltaY() > 0) {
					xf += distX * invRatioX;
					xl -= distX * ratioX;
					yf += distY * ratioY;
					yl -= distY * invRatioY;
				} else {
					xf -= distX * invRatioX;
					xl += distX * ratioX;
					yf -= distY * ratioY;
					yl += distY * invRatioY;
				}
				boolean hasChanged = false;
				if (xl - xf > 1E-5) {
					first.setXValue(xf);
					last.setXValue(xl);
					hasChanged = true;
				}
				if (yl - yf > 1E-5) {
					first.setYValue(yf);
					last.setYValue(yl);
					hasChanged = true;
				}
				if (hasChanged) {
					updateCanvasParam();
					repaint(false);
				}
			}
		});
		keyReleasedHandler = e -> pushTransform();
		addEventHandler(KeyEvent.KEY_RELEASED, keyReleasedHandler);
		keyPressedHandler = e -> {
			if (e.isConsumed())
				return;
			switch (e.getCode()) {
			case A:
				fitDocument();
				updateCanvasParam();
				repaint(false);
				break;
			case RIGHT:
				if (e.isControlDown())
					moveCurveSelection(true);
				else {
					double dx = Math.abs(last.getXValue().doubleValue() - first.getXValue().doubleValue()) / 10.0;
					first.setXValue(first.getXValue().doubleValue() + dx);
					last.setXValue(last.getXValue().doubleValue() + dx);
					updateCanvasParam();
				}
				repaint(false);
				break;
			case LEFT:
				if (e.isControlDown())
					moveCurveSelection(false);
				else {
					double _dx = Math.abs(last.getXValue().doubleValue() - first.getXValue().doubleValue()) / 10.0;
					first.setXValue(first.getXValue().doubleValue() - _dx);
					last.setXValue(last.getXValue().doubleValue() - _dx);
					updateCanvasParam();
				}
				repaint(false);
				break;
			case UP:
				if (e.isControlDown())
					moveCurveSelection(true);
				else {
					double dy = Math.abs(last.getYValue().doubleValue() - first.getYValue().doubleValue()) / 10.0;
					first.setYValue(first.getYValue().doubleValue() + dy);
					last.setYValue(last.getYValue().doubleValue() + dy);
					updateCanvasParam();
				}
				repaint(false);
				break;
			case DOWN:
				if (e.isControlDown())
					moveCurveSelection(false);
				else {
					double _dy = Math.abs(last.getYValue().doubleValue() - first.getYValue().doubleValue()) / 10.0;
					first.setYValue(first.getYValue().doubleValue() - _dy);
					last.setYValue(last.getYValue().doubleValue() - _dy);
					updateCanvasParam();
				}
				repaint(false);
				break;
			default:
				break;
			}
		};
		addEventHandler(KeyEvent.KEY_PRESSED, keyPressedHandler);
		Parent parent = drawRegion;
		while (parent != chart) {
			parent.localToParentTransformProperty().addListener(e -> updateCanvasPos());
			parent = parent.getParent();
		}
		chart.localToParentTransformProperty().addListener(e -> updateCanvasPos());
		chart.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));

		chartCanvas.widthProperty().addListener(e -> Platform.runLater(() -> updateCanvasPos()));
		chartCanvas.heightProperty().addListener(e -> Platform.runLater(() -> updateCanvasPos()));
		root.prefWidthProperty().bind(widthProperty());
		root.prefHeightProperty().bind(heightProperty());
		getChildren().add(root);

		last.XValueProperty().addListener(e -> {
			double distX = Math.abs(last.getXValue().doubleValue() - first.getXValue().doubleValue());
			double d = distX;
			String pat = "###";
			if (d != 0) {
				if (d < 10)
					pat += ".";
				while (d < 10) {
					d *= 10;
					pat += "0";
				}
				pat += "0";
			}
			double absValue = Math.abs(last.getXValue().doubleValue());
			if (absValue > 100000)
				pat += "E0";
			yAxis.setPrefWidth(
					Math.max(new Text(new DecimalFormat(pat).format(first.getXValue().doubleValue())).getBoundsInLocal().getWidth(), new Text(new DecimalFormat(pat).format(last.getXValue().doubleValue())).getBoundsInLocal().getWidth()) + 30);
		});
	}

	public boolean isAutoFitValuesBounds() {
		return autoFitValuesBounds;
	}

	public boolean isAutoFitXaxis() {
		return autoFitXaxis;
	}

	public boolean isAutoFitYaxis() {
		return autoFitYaxis;
	}

	@Override
	public boolean isValidAdditionalInput(Class<?>[] inputsType, Class<?> additionalInput) {
		return additionalInput == null ? false : Curved.class.isAssignableFrom(additionalInput) || CurveSeries.class.isAssignableFrom(additionalInput);
	}

	private void moveCurveSelection(boolean up) {
		Curve selectedCurve = tempChartLabel.curve;
		if (selectedCurve == null)
			return;
		if (up) {
			if (tempChartLabel.index >= selectedCurve.getDatad()[0].length - 1)
				return;
			tempChartLabel.index++;
		} else {
			if (tempChartLabel.index <= 0)
				return;
			tempChartLabel.index--;
		}
		tempChartLabel.updateInfo();
	}

	@Override
	public void paint(Object dataElement) {
		ArrayList<Object> des = new ArrayList<>();
		des.add(dataElement);
		if (getAdditionalDrawableElement() != null)
			for (Object de : getAdditionalDrawableElement())
				des.add(de);
		curves.clear();
		for (Object de : des) {
			if (de == null)
				continue;
			if (de instanceof Curved)
				curves.add((Curved) de);
			else if (de instanceof Curvei)
				curves.add((Curve) de);
			else
				curves.addAll(((CurveSeries) de).getCurves());
		}
		// if (hasTheaterFilterStructChanged())
		updateCurvesTheaterFilter();
		ObservableList<Series<Number, Number>> chartData = chart.getData();

		boolean hasChanged = false;
		if (chartData.size() != curves.size()) {
			int diff = curves.size() - chart.getData().size();
			if (diff > 0) {
				ArrayList<LineChart.Series<Number, Number>> series = new ArrayList<>();
				for (int i = 0; i < diff; i++)
					series.add(new LineChart.Series<>());
				chartData.addAll(series);
			} else
				while (chartData.size() != curves.size())
					chartData.remove(chartData.size() - 1);
			hasChanged = true;
		}
		if (dataElement instanceof EvolvedCurveSeries) {
			EvolvedCurveSeries ecs = (EvolvedCurveSeries) dataElement;
			String chartName = ecs.getName();
			if (chartName != null && !chartName.equals(chart.getTitle()))
				chart.setTitle(chartName);
			String xLabel = ecs.getxLabel();
			if (xLabel != null && !xLabel.equals(xAxis.getLabel()))
				xAxis.setLabel(xLabel);
			String yLabel = ecs.getyLabel();
			if (yLabel != null && !yLabel.equals(yAxis.getLabel()))
				yAxis.setLabel(yLabel);
		} else if (curves.size() == 1) {
			String chartName = curves.get(0).getName();
			if (chartName == null) {
				if (chart.getTitle() != null)
					chart.setTitle(null);
			} else if (!chartName.equals(chart.getTitle()))
				chart.setTitle(chartName);
		}
		for (int i = 0; i < curves.size(); i++) {
			String name = curves.get(i).getName();
			if (name == null)
				name = Integer.toString(i);
			if (name != chartData.get(i).getName()) {
				chartData.get(i).setName(name);
				hasChanged = true;
			}
		}
		if (hasChanged) {
			Set<Node> items = chart.lookupAll("Label.chart-legend-item");
			colorProvider.resetIndex();
			for (Node item : items) {
				Label label = (Label) item;
				Line line = new Line(0, 0, 6, 0);
				line.setStroke(colorProvider.getNextColor());
				line.setStrokeWidth(2);
				label.setGraphic(line);
				label.setOnMouseClicked(e -> {
					updateCurvesTheaterFilter();
					String name = label.getText();
					TreeNode<BooleanProperty> curveElement = getTheaterSon(theaterFilter, name);
					TreeNode<BooleanProperty> linksNode = getTheaterSon(curveElement, LINKS);
					boolean isVisible = linksNode.getValue().value;

					if (e.getClickCount() == 1)
						updateFilterWithPath(new String[] { curveElement.getValue().name, linksNode.getValue().name }, !isVisible, true);
					else
						for (TreeNode<BooleanProperty> node : theaterFilter.getChildren())
							if (!node.getValue().name.equals(name))
								if (!isVisible) {
									updateFilterWithPath(new String[] { node.getValue().name, getTheaterSon(node, LINKS).getValue().name }, !isVisible, true);
									updateFilterWithPath(new String[] { node.getValue().name, getTheaterSon(node, SYMBOLS).getValue().name }, isVisible, true);
									// updateFilterWithPath(new String[] { node.getLeaf().name, getTheaterSon(node, COORDINATES).getLeaf().name }, isVisible, true);
									// updateFilterWithPath(new String[] { node.getLeaf().name, getTheaterSon(node, INDICES).getLeaf().name }, isVisible, true);
								} else
									updateFilterWithPath(new String[] { node.getValue().name, getTheaterSon(node, LINKS).getValue().name }, !isVisible, true);
					// updateFilterWithPath(new String[] { node.getLeaf().name, getTheaterSon(node, SYMBOLS).getLeaf().name }, !isVisible, true);
					// updateFilterWithPath(new String[] { node.getLeaf().name, getTheaterSon(node, COORDINATES).getLeaf().name }, !isVisible, true);
					// updateFilterWithPath(new String[] { node.getLeaf().name, getTheaterSon(node, INDICES).getLeaf().name }, !isVisible, true);
				});
			}
		}
		if (boundsChanged && (autoFitXaxis || autoFitYaxis)) {
			double[] bounds;
			if (autoFitXaxis && autoFitYaxis) {
				bounds = new double[4];
				minX = Double.MAX_VALUE;
				maxX = -Double.MAX_VALUE;
				minY = Double.MAX_VALUE;
				maxY = -Double.MAX_VALUE;
				for (Curve curve : curves) {
					computeBounds(curve, bounds);
					if (bounds[0] < minX)
						minX = bounds[0];
					if (bounds[1] > maxX)
						maxX = bounds[1];
					if (bounds[2] < minY)
						minY = bounds[2];
					if (bounds[3] > maxY)
						maxY = bounds[3];
				}
				double dx = Math.abs(maxX - minX);
				double dy = Math.abs(maxY - minY);
				minX -= dx * margin;
				maxX += dx * margin;
				minY -= dy * margin;
				maxY += dy * margin;
				boolean hasBoundChanged = false;
				if (first.getXValue().doubleValue() != minX) {
					first.setXValue(minX);
					hasBoundChanged = true;
				}
				if (last.getXValue().doubleValue() != maxX) {
					last.setXValue(maxX);
					hasBoundChanged = true;
				}
				if (first.getYValue().doubleValue() != minY) {
					first.setYValue(minY);
					hasBoundChanged = true;
				}
				if (last.getYValue().doubleValue() != maxY) {
					last.setYValue(maxY);
					hasBoundChanged = true;
				}
				if (hasBoundChanged)
					updateCanvasParam();
			} else if (autoFitXaxis) {
				bounds = new double[2];
				minX = Double.MAX_VALUE;
				maxX = -Double.MAX_VALUE;
				for (Curve curve : curves) {
					computeXBounds(curve, bounds);
					if (bounds[0] < minX)
						minX = bounds[0];
					if (bounds[1] > maxX)
						maxX = bounds[1];
				}
				double dx = Math.abs(maxX - minX);
				minX -= dx * margin;
				maxX += dx * margin;
				boolean hasBoundChanged = false;
				if (first.getXValue().doubleValue() != minX) {
					first.setXValue(minX);
					hasBoundChanged = true;
				}
				if (last.getXValue().doubleValue() != maxX) {
					last.setXValue(maxX);
					hasBoundChanged = true;
				}
				if (hasBoundChanged)
					updateCanvasParam();
			} else if (autoFitYaxis) {
				bounds = new double[2];
				minY = Double.MAX_VALUE;
				maxY = -Double.MAX_VALUE;
				for (Curve curve : curves) {
					computeYBounds(curve, bounds);
					if (bounds[0] < minY)
						minY = bounds[0];
					if (bounds[1] > maxY)
						maxY = bounds[1];
				}
				double dy = Math.abs(maxY - minY);
				minY -= dy * margin;
				maxY += dy * margin;
				boolean hasBoundChanged = false;
				if (first.getYValue().doubleValue() != minY) {
					first.setYValue(minY);
					hasBoundChanged = true;
				}
				if (last.getYValue().doubleValue() != maxY) {
					last.setYValue(maxY);
					hasBoundChanged = true;
				}
				if (hasBoundChanged)
					updateCanvasParam();
			}
		}
		drawChart();
		boundsChanged = false;
		if (selectionCanvas.isVisible())
			drawSelection();
	}

	private void pushTransform() {
		double[] t = new double[] { first.getXValue().doubleValue(), first.getYValue().doubleValue(), last.getXValue().doubleValue(), last.getYValue().doubleValue() };
		double[] lastT = previousTransform.isEmpty() ? null : previousTransform.get(previousTransform.size() - 1);
		if (lastT == null || t[0] != lastT[0] || t[1] != lastT[1] || t[2] != lastT[2] || t[3] != lastT[3]) {
			if (previousTransform.size() == 10)
				previousTransform.remove(0);
			previousTransform.add(new double[] { first.getXValue().doubleValue(), first.getYValue().doubleValue(), last.getXValue().doubleValue(), last.getYValue().doubleValue() });
		}
	}

	public void setAutoFitValuesBounds(boolean autoFitValuesBounds) {
		if (this.autoFitValuesBounds == autoFitValuesBounds)
			return;
		this.autoFitValuesBounds = autoFitValuesBounds;
		fireSetPropertyEnable(this, "valuesBounds", !autoFitValuesBounds);
		repaint(false);
	}

	public void setAutoFitXaxis(boolean autoFitXaxis) {
		if (this.autoFitXaxis == autoFitXaxis)
			return;
		this.autoFitXaxis = autoFitXaxis;
		updateXAxisBounds();
		fireSetPropertyEnable(this, "xBounds", !autoFitXaxis);
		repaint(false);
	}

	public void setAutoFitYaxis(boolean autoFitYaxis) {
		if (this.autoFitYaxis == autoFitYaxis)
			return;
		this.autoFitYaxis = autoFitYaxis;
		updateYAxisBounds();
		fireSetPropertyEnable(this, "yBounds", !autoFitYaxis);
		repaint(false);
	}

	public void setColors(Color[] colors) {
		if(colors != null)
			this.colorProvider = new ColorProvider(colors);
		repaint(false);
	}
	
	@ArrayInfo(prefHeight = 26 * 8 + 2)
	@PropertyInfo(nullable = false, info = "Color for the additional input point cloud")
	public Color[] getChartsColor() {
		return colorProvider.getColors();
	}

	public void setChartsColor(Color[] pointCloudColor) {
		colorProvider = new ColorProvider(pointCloudColor);
		repaint(false);
	}

	@Override
	public void setDrawableElement(Object drawableElement) {
		super.setDrawableElement(drawableElement);
		boundsChanged = true;
	}

	@Override
	public void setEnable() {
		fireSetPropertyEnable(this, "xBounds", !autoFitXaxis);
		fireSetPropertyEnable(this, "yBounds", !autoFitYaxis);
		fireSetPropertyEnable(this, "valuesBounds", !autoFitValuesBounds);
	}

	public void setMargin(double margin) {
		if (this.margin == margin)
			return;
		this.margin = margin;
		boundsChanged = true;
		if (xAxis != null)
			repaint(false);
	}

	public void setValuesBounds(Point2d valuesBounds) {
		this.valuesBounds = valuesBounds;
		repaint(false);
	}

	public void setxBounds(Point2d xBounds) {
		if (this.xBounds == xBounds)
			return;
		this.xBounds = xBounds;
		updateXAxisBounds();
		repaint(false);
	}

	public void setyBounds(Point2d yBounds) {
		if (this.yBounds == yBounds)
			return;
		this.yBounds = yBounds;
		updateYAxisBounds();
		repaint(false);
	}

	private void updateCanvasParam() {
		double distX = Math.abs(last.getXValue().doubleValue() - first.getXValue().doubleValue());
		double distY = Math.abs(last.getYValue().doubleValue() - first.getYValue().doubleValue());
		xAxis.setTickUnit(distX / 10.0);
		yAxis.setTickUnit(distY / 10.0);
		xAxis.setTickLabelFormatter(new NumberAxis.DefaultFormatter(xAxis) {
			@Override
			public String toString(Number object) {
				double value = object.doubleValue();
				double d = distX;
				String pat = "##0";
				if (d != 0) {
					if (d < 10)
						pat += ".";
					while (d < 10) {
						d *= 10;
						pat += "0";
					}
					pat += "0";
				}
				double absValue = Math.abs(object.doubleValue());
				if (absValue > 100000)
					pat += "E0";
				return new DecimalFormat(pat).format(value);
			}
		});
		yAxis.setTickLabelFormatter(new NumberAxis.DefaultFormatter(xAxis) {
			@Override
			public String toString(Number object) {
				double d = distY;
				String pat = "##0";
				if (d != 0) {
					if (d < 10)
						pat += ".";
					while (d < 10) {
						d *= 10;
						pat += "0";
					}
					pat += "0";
				}
				double value = object.doubleValue();
				double absValue = Math.abs(value);
				if (absValue > 100000)
					pat += "E0";
				return new DecimalFormat(pat).format(value);
			}
		});

		xAxis.setLowerBound(first.getXValue().doubleValue());
		xAxis.setUpperBound(last.getXValue().doubleValue());
		yAxis.setLowerBound(first.getYValue().doubleValue());
		yAxis.setUpperBound(last.getYValue().doubleValue());

		ratioX = drawRegion.getWidth() / (last.getXValue().doubleValue() - first.getXValue().doubleValue());
		ratioY = -drawRegion.getHeight() / (last.getYValue().doubleValue() - first.getYValue().doubleValue());
		offsetX = first.getXValue().doubleValue();
		offsetY = first.getYValue().doubleValue() - chartCanvas.getHeight() / ratioY;
	}

	private void updateCanvasPos() {
		Point2D zero = new Point2D(0, 0);
		Point2D pos = drawRegion.localToParent(zero);
		Parent parent = drawRegion.getParent();
		while (parent != chart) {
			pos = pos.add(parent.localToParent(zero));
			parent = parent.getParent();
		}
		pos = pos.add(chart.parentToLocal(zero));
		if (chartCanvas.getLayoutX() != pos.getX())
			chartCanvas.setLayoutX(pos.getX());
		if (chartCanvas.getLayoutY() != pos.getY())
			chartCanvas.setLayoutY(pos.getY());
		if (selectionCanvas.getLayoutX() != pos.getX())
			selectionCanvas.setLayoutX(pos.getX());
		if (selectionCanvas.getLayoutY() != pos.getY())
			selectionCanvas.setLayoutY(pos.getY());
		updateCanvasParam();
		repaint(false);
	}

	private void updateCurvesTheaterFilter() {
		LinkedList<String> toRemove = new LinkedList<>();
		if (theaterFilter.getChildren() != null)
			for (TreeNode<BooleanProperty> son : theaterFilter.getChildren())
				toRemove.add(son.getValue().name);
		boolean hasChanged = false;
		for (int i = 0; i < curves.size(); i++) {
			String name = curves.get(i).getName();
			if (name == null)
				name = Integer.toString(i);
			if (!toRemove.contains(name)) {
				TreeNode<BooleanProperty> tfcp = new TreeNode<>(new BooleanProperty(name, false));
				tfcp.addChild(new TreeNode<>(new BooleanProperty(LINKS, true)));
				tfcp.addChild(new TreeNode<>(new BooleanProperty(SYMBOLS, false)));
				tfcp.addChild(new TreeNode<>(new BooleanProperty(COORDINATES, false)));
				tfcp.addChild(new TreeNode<>(new BooleanProperty(INDICES, false)));
				theaterFilter.addChild(tfcp);
				hasChanged = true;
			} else
				toRemove.remove(name);
		}
		if (!toRemove.isEmpty()) {
			hasChanged = true;
			for (String curveName : toRemove)
				theaterFilter.getChildren().removeIf((son) -> son.getValue().name.equals(curveName));
		}
		if (hasChanged)
			fireTheaterFilterStructChanged();
	}

	@Override
	public boolean updateFilterWithPath(String[] filterPath, boolean value) {
		if (chart == null)
			return true;
		String filterName = filterPath[0];
		boolean isVisible = false;
		for (TreeNode<BooleanProperty> node : getTheaterSon(theaterFilter, filterName).getChildren())
			if (node.getValue().value) {
				isVisible = true;
				break;
			}
		for (Node item : chart.lookupAll("Label.chart-legend-item")) {
			Label label = (Label) item;
			if (label.getText().equals(filterName))
				label.setTextFill(isVisible ? Color.BLACK : Color.DARKGRAY);
		}
		repaint(false);
		return true;
	}

	private void updateXAxisBounds() {
		if (!autoFitXaxis) {
			first.setXValue(xBounds.x);
			minX = xBounds.x;
			last.setXValue(xBounds.y);
			maxX = xBounds.y;
			if (xAxis != null) {
				updateCanvasParam();
				repaint(false);
			}
		}
		if (xAxis != null) {
			boundsChanged = true;
			repaint(false);
		}
	}

	private void updateYAxisBounds() {
		if (!autoFitYaxis) {
			first.setYValue(yBounds.x);
			minY = yBounds.x;
			last.setYValue(yBounds.y);
			maxY = yBounds.y;
			if (yAxis != null) {
				updateCanvasParam();
				repaint(false);
			}
		}
		if (yAxis != null) {
			boundsChanged = true;
			repaint(false);
		}
	}
}

class ChartLabel extends VBox {
	private static int radius = 10;
	public static final int width = 210 + radius;
	public static double height = 90;
	private static final Color color = new Color(0, 0, 0, 0.5);
	public Curve curve;
	public int index;
	private String xLabelName;
	private String yLabelName;
	private ChartLabelOrientation orientation;
	private double posX;
	private double posY;
	private VBox labelBox;
	private Label idLabel;
	private Label xLabel;
	private Label yLabel;
	private Label valueLabel;

	public ChartLabel(double posX, double posY, double radius, Curve curve, int index, String xLabelName, String yLabelName, BiConsumer<ChartLabel, Boolean> isPin) {
		this.curve = curve;
		this.index = index;
		this.xLabelName = xLabelName;
		this.yLabelName = yLabelName;
		setTranslateX(posX);
		setTranslateY(posY);
		setBackground(new Background(new BackgroundFill(color, null, null)));
		setPrefSize(width, height);
		setMaxSize(width, height);
		setPickOnBounds(false);
		focusedProperty().addListener(e -> setBorder(isFocused() ? new Border(new BorderStroke(Color.LIME, BorderStrokeStyle.SOLID, null, null)) : null));
		// focusedProperty().addListener(e -> {
		// System.out.println("cool: " + isFocused());
		// if(isFocused() == false)
		// System.err.println("pk");
		// setBorder(isFocused() ? new Border(new BorderStroke(Color.LIME, BorderStrokeStyle.SOLID, null, null)) : null);
		// });

		final ImageView toggleImage = new ImageView(new Image(getClass().getResourceAsStream("/pin2.png")));
		ToggleButton pinButton = new ToggleButton();
		pinButton.setGraphic(toggleImage);
		pinButton.selectedProperty().addListener(e -> {
			if (pinButton.isSelected()) {
				toggleImage.setRotate(-90);
				pinButton.setStyle("-fx-background-color: transparent;-fx-border-width: 1px;-fx-border-color: #303030 #B0B0B0 #B0B0B0 #303030;"); // 3 bas 4 gauche
			} else {
				toggleImage.setRotate(0);
				pinButton.setStyle("-fx-background-color: transparent;-fx-border-width: 1px;-fx-border-color: #B0B0B0 #303030 #303030 #B0B0B0;"); // 3 bas 4 gauche
			}
			isPin.accept(this, pinButton.isSelected());
		});

		idLabel = new Label();
		idLabel.setTooltip(new Tooltip());
		idLabel.setTextFill(Color.WHITE);
		xLabel = new Label();
		xLabel.setTooltip(new Tooltip());
		xLabel.setTextFill(Color.WHITE);
		yLabel = new Label();
		yLabel.setTextFill(Color.WHITE);
		yLabel.setTooltip(new Tooltip());
		// if (value != null) {
		// valueLabel = new Label();
		// valueLabel.setTooltip(new Tooltip());
		// valueLabel.setTextFill(Color.WHITE);
		// }
		// setinfo(selectedIndex, x, y, value);

		BorderPane idPinBox = new BorderPane();
		idPinBox.setLeft(idLabel);
		idPinBox.setRight(pinButton);
		BorderPane.setAlignment(idLabel, Pos.BOTTOM_LEFT);
		BorderPane.setAlignment(idPinBox, Pos.TOP_RIGHT);
		HBox.setHgrow(idLabel, Priority.ALWAYS);
		labelBox = new VBox(idPinBox, xLabel, yLabel);
		// if (valueLabel != null)
		// labelBox.getChildren().add(valueLabel);
		labelBox.setPadding(new Insets(5, 5, 1, 5));
		labelBox.setAlignment(Pos.CENTER_LEFT);
		labelBox.setPickOnBounds(false);
		// labelBox.setMouseTransparent(true);
		getChildren().add(labelBox);

		setOnKeyPressed(e -> {
			if (e.isControlDown()) {
				ChartLabelOrientation oldOrientation = this.orientation;
				if (e.getCode() == KeyCode.RIGHT) {
					if (this.orientation == ChartLabelOrientation.TOP_LEFT)
						this.orientation = ChartLabelOrientation.TOP_RIGHT;
					else if (this.orientation == ChartLabelOrientation.BOTTOM_LEFT)
						this.orientation = ChartLabelOrientation.BOTTOM_RIGHT;
					e.consume();
				} else if (e.getCode() == KeyCode.LEFT) {
					if (this.orientation == ChartLabelOrientation.TOP_RIGHT)
						this.orientation = ChartLabelOrientation.TOP_LEFT;
					else if (this.orientation == ChartLabelOrientation.BOTTOM_RIGHT)
						this.orientation = ChartLabelOrientation.BOTTOM_LEFT;
					e.consume();
				} else if (e.getCode() == KeyCode.UP) {
					if (this.orientation == ChartLabelOrientation.BOTTOM_RIGHT)
						this.orientation = ChartLabelOrientation.TOP_RIGHT;
					else if (this.orientation == ChartLabelOrientation.BOTTOM_LEFT)
						this.orientation = ChartLabelOrientation.TOP_LEFT;
					e.consume();
				} else if (e.getCode() == KeyCode.DOWN) {
					if (this.orientation == ChartLabelOrientation.TOP_RIGHT)
						this.orientation = ChartLabelOrientation.BOTTOM_RIGHT;
					else if (this.orientation == ChartLabelOrientation.TOP_LEFT)
						this.orientation = ChartLabelOrientation.BOTTOM_LEFT;
					e.consume();
				}
				if (oldOrientation != this.orientation) {
					updateOrientation();
					setTranslate(new Point2D(this.posX, this.posY));
				}
			} else
				System.out.println("je déplace");
		});
		pinButton.setStyle("-fx-background-color: transparent;-fx-border-width: 1px;-fx-border-color: #B0B0B0 #303030 #303030 #B0B0B0;");
		pinButton.setBackground(null);
		pinButton.setPadding(new Insets(1));
		// updateOrientation();
		// setOnMousePressed(e -> requestFocus());
		setOnMousePressed(e -> {
			requestFocus();
			e.consume();
		});

		heightProperty().addListener(e -> {
			height = getPrefHeight();
			updateOrientation();
			setTranslate(new Point2D(this.posX, this.posY));
		});
	}

	public void setTranslate(Point2D pos) {
		this.posX = pos.getX();
		this.posY = pos.getY();
		updateTranslate();
	}

	public void updateInfo() {
		String ids = Integer.toString(index);
		idLabel.setText("id: " + ids);
		idLabel.getTooltip().setText(ids);
		String xs = Double.toString(curve.getDatad()[0][index]);
		xLabel.setText(xs + " " + xLabelName);
		xLabel.getTooltip().setText(xs);
		String ys = Double.toString(curve.getDatad()[1][index]);
		yLabel.setText(ys + " " + yLabelName);
		yLabel.getTooltip().setText(ys);
		Double value = curve.getValuesd() != null ? curve.getValuesd()[index] : null;
		if (valueLabel != null && value == null) {
			labelBox.getChildren().remove(valueLabel);
			valueLabel = null;
		} else if (valueLabel == null && value != null) {
			valueLabel = new Label();
			valueLabel.setTooltip(new Tooltip());
			valueLabel.setTextFill(Color.WHITE);
			labelBox.getChildren().add(valueLabel);
		}
		if (valueLabel != null) {
			String sValue = value.toString();
			valueLabel.setText("value: " + sValue);
			valueLabel.getTooltip().setText(sValue);
		}
	}

	// public void setinfo(int selectedIndex, double x, double y, Double value) {
	// String ids = Integer.toString(selectedIndex);
	// idLabel.setText("id: " + ids);
	// idLabel.getTooltip().setText(ids);
	// String xs = Double.toString(x);
	// xLabel.setText("x: " + xs);
	// xLabel.getTooltip().setText(xs);
	// String ys = Double.toString(y);
	// yLabel.setText("y: " + ys);
	// yLabel.getTooltip().setText(ys);
	// if (valueLabel != null && value == null) {
	// labelBox.getChildren().remove(valueLabel);
	// valueLabel = null;
	// } else if (valueLabel == null && value != null) {
	// valueLabel = new Label();
	// valueLabel.setTooltip(new Tooltip());
	// valueLabel.setTextFill(Color.WHITE);
	// labelBox.getChildren().add(valueLabel);
	// }
	// if (valueLabel != null) {
	// String values = value.toString();
	// valueLabel.setText("value: " + values);
	// valueLabel.getTooltip().setText(values);
	// }
	// }

	private void updateOrientation() {
		Shape shape;
		switch (orientation) {
		case BOTTOM_RIGHT:
			shape = Shape.union(new Rectangle(radius, radius, width - radius, height - radius), new Circle(radius, radius, radius));
			shape = Shape.subtract(shape, new Circle(radius, radius, 6));
			shape = Shape.union(shape, new Line(radius, 2, radius, radius * 2 - 2));
			shape = Shape.union(shape, new Line(2, radius, radius * 2 - 2, radius));
			break;
		case BOTTOM_LEFT:
			shape = Shape.union(new Rectangle(0, radius, width - radius, height - radius), new Circle(width - radius, radius, radius));
			shape = Shape.subtract(shape, new Circle(width - radius, radius, 6));
			shape = Shape.union(shape, new Line(width - radius * 2 - 2, radius, width - 2, radius));
			shape = Shape.union(shape, new Line(width - radius, 2, width - radius, radius * 2 - 2));
			break;
		case TOP_RIGHT:
			shape = Shape.union(new Rectangle(radius, 0, width - radius, height - radius), new Circle(radius, height - radius, radius));
			shape = Shape.subtract(shape, new Circle(radius, height - radius, 6));
			shape = Shape.union(shape, new Line(2, height - radius, radius * 2 - 2, height - radius));
			shape = Shape.union(shape, new Line(radius, height - radius * 2 - 2, radius, height - 2));
			break;
		case TOP_LEFT:
			shape = Shape.union(new Rectangle(0, 0, width - radius, height - radius), new Circle(width - radius, height - radius, radius));
			shape = Shape.subtract(shape, new Circle(width - radius, height - radius, 6));
			shape = Shape.union(shape, new Line(width - radius * 2 - 2, height - radius, width - 2, height - radius));
			shape = Shape.union(shape, new Line(width - radius, height - radius * 2 - 2, width - radius, height - 2));
			break;
		default:
			return;
		}
		setShape(shape);
	}

	public void updatePos(Canvas canvas, double x, double y) {
		// System.out.println("x: " + x);
		boolean isVisible = x >= 0 && y >= 0 && x < canvas.getWidth() && y < canvas.getHeight();
		if (isVisible != isVisible())
			setVisible(isVisible);
		ChartLabelOrientation orientation;
		if (x - ChartLabel.width + radius < 0)
			orientation = y - ChartLabel.height + radius < 0 ? ChartLabelOrientation.BOTTOM_RIGHT : ChartLabelOrientation.TOP_RIGHT;
		else
			orientation = y - ChartLabel.height + radius < 0 ? ChartLabelOrientation.BOTTOM_LEFT : ChartLabelOrientation.TOP_LEFT;
		if (this.orientation != orientation) {
			this.orientation = orientation;
			updateOrientation();
		}
		setTranslate(canvas.localToParent(new Point2D(x, y)));
	}

	private void updateTranslate() {
		Insets insets;
		switch (orientation) {
		case BOTTOM_RIGHT:
			insets = new Insets(radius + 5, 5, 1, 5 + radius);
			setTranslateX(posX - radius);
			setTranslateY(posY - radius);
			break;
		case BOTTOM_LEFT:
			insets = new Insets(radius + 5, 5 + radius, 1, 5);
			setTranslateX(posX - width + radius);
			setTranslateY(posY - radius);
			break;
		case TOP_RIGHT:
			insets = new Insets(5, 5, 5, 5 + radius);
			setTranslateX(posX - radius);
			setTranslateY(posY - height + radius);
			break;
		case TOP_LEFT:
			insets = new Insets(5, 5 + radius, 5, 5);
			setTranslateX(posX - width + radius);
			setTranslateY(posY - height + radius);
			break;
		default:
			return;
		}
		labelBox.setPadding(insets);
	}
}

enum ChartLabelOrientation {
	BOTTOM_RIGHT, BOTTOM_LEFT, TOP_RIGHT, TOP_LEFT
}
