/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.display.drawer;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

import org.beanmanager.struct.BooleanProperty;
import org.beanmanager.struct.TreeNode;
import org.scenarium.display.ScenariumContainer;
import org.scenarium.display.StackableDrawer;
import org.scenarium.struct.GeographicCoordinate;
import org.scenarium.timescheduler.Scheduler;

import javafx.geometry.Point2D;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import net.sf.marineapi.nmea.parser.DataNotAvailableException;
import net.sf.marineapi.nmea.sentence.GGASentence;
import net.sf.marineapi.nmea.sentence.PositionSentence;
import net.sf.marineapi.nmea.util.Position;

public class GPSDrawer extends GeographicalDrawer implements StackableDrawer {
	private static final String ONLYGGATRAME = "Only GGA Trame";
	private static final String INFO = "Infos";
	private boolean filterOnlyGGATRrame;
	private boolean filterInfo;
	private PositionSentence lastSentence;

	@Override
	public boolean canAddInputToRenderer(Class<?>[] class1) {
		return true;
	}

	@Override
	protected GeographicCoordinate getCoordinate() {
		GeographicCoordinate posGeo = null;
		Object dataElement = getDrawableElement();
		if (dataElement instanceof PositionSentence) {
			Position pos = ((PositionSentence) dataElement).getPosition();
			posGeo = new GeographicCoordinate(pos.getLatitude(), pos.getLongitude());
		}
		return posGeo;
	}

	@Override
	public void initialize(ScenariumContainer container, Scheduler scheduler, Object dataElement, boolean autoFitIfResize) {
		super.initialize(container, scheduler, dataElement, autoFitIfResize);
	}

	@Override
	public boolean isValidAdditionalInput(Class<?>[] inputsType, Class<?> additionalInput) {
		return additionalInput == null ? false : !filterOnlyGGATRrame || GGASentence.class.isAssignableFrom(additionalInput);
	}

	@Override
	public void paint(Object dataElement) {
		if(!(dataElement instanceof PositionSentence) || (filterOnlyGGATRrame && !(dataElement instanceof GGASentence)))
			return;
		Position pos;
		try {
			pos = ((PositionSentence) dataElement).getPosition();
		}catch(DataNotAvailableException e) {
			return;
		}
		GeographicCoordinate posGeo = new GeographicCoordinate(pos.getLatitude(), pos.getLongitude());
		super.paint(posGeo);
		if (filterOnlyGGATRrame)
			if (dataElement instanceof GGASentence)
				lastSentence = (PositionSentence) dataElement;
			else
				dataElement = lastSentence;
		gInfo.clearRect(0, 0, getWidth(), getHeight());
		if (dataElement == null)
			return;
		Point2D screenPos = geoToScreen(posGeo);
		if (dataElement instanceof GGASentence) {
			gInfo.setFill(new Color(0.5, 0.5, 0.5, 0.5));
			double radius = ((GGASentence) dataElement).getHorizontalDOP() / getMeterPerPixel() * 10;
			gInfo.fillOval(screenPos.getX() - radius / 2, screenPos.getY() - radius / 2, radius, radius);
			gInfo.setStroke(new Color(1, 0.784313725, 0, 1));
			gInfo.strokeOval(screenPos.getX() - radius / 2, screenPos.getY() - radius / 2, radius, radius);
		}
		gInfo.setFill(Color.YELLOW);
		double radius = 10;
		gInfo.fillOval(screenPos.getX() - radius / 2, screenPos.getY() - radius / 2, radius, radius);
		gInfo.setStroke(Color.BLACK);
		gInfo.strokeOval(screenPos.getX() - radius / 2, screenPos.getY() - radius / 2, radius, radius);
		if (filterInfo)
			try {
				ArrayList<String> infos = new ArrayList<>();
				for (PropertyDescriptor pd : Introspector.getBeanInfo(dataElement.getClass()).getPropertyDescriptors())
					try {
						String name = pd.getName();
						if (!name.equals("beginChar") && !name.equals("fieldCount") && !name.equals("class"))
							infos.add(name + ": " + pd.getReadMethod().invoke(dataElement, new Object[0]));
					} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {}
				double height = new Text("").getLayoutBounds().getHeight();
				double maxWidth = 0;
				for (String info : infos) {
					double width = new Text(info).getLayoutBounds().getWidth();
					if (width > maxWidth)
						maxWidth = width;
				}
				gInfo.setFill(new Color(1, 1, 1, 0.6));
				gInfo.fillRect(0, 0, maxWidth + 10, height + infos.size() * height);
				gInfo.setFill(Color.BLACK);
				for (int i = 0; i < infos.size(); i++)
					gInfo.fillText(infos.get(i), 5, height + i * height);
			} catch (IntrospectionException e) {}
	}

	@Override
	protected void populateTheaterFilter() {
		theaterFilter.addChild(new TreeNode<>(new BooleanProperty(ONLYGGATRAME, true)));
		theaterFilter.addChild(new TreeNode<>(new BooleanProperty(INFO, true)));
	}

	@Override
	public boolean updateFilterWithPath(String[] filterPath, boolean value) {
		String filterName = filterPath[filterPath.length - 1];
		if (filterName.equals(ONLYGGATRAME))
			filterOnlyGGATRrame = value;
		else if (filterName.equals(INFO))
			filterInfo = value;
		else
			return super.updateFilterWithPath(filterPath, value);
		repaint(false);
		return true;
	}
}
