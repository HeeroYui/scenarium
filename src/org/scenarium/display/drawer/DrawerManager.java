/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.display.drawer;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;

import javax.swing.event.EventListenerList;

import org.beanmanager.editors.PropertyEditor;
import org.beanmanager.editors.PropertyEditorManager;
import org.scenarium.display.LoadDrawerListener;
import org.scenarium.filemanager.scenario.dataflowdiagram.FlowDiagram;
import org.scenarium.struct.GeographicCoordinate;
import org.scenarium.struct.OldObject3D;
import org.scenarium.struct.curve.Curve;
import org.scenarium.struct.curve.CurveSeries;
import org.scenarium.struct.raster.BufferedImageStrategy;
import org.scenarium.struct.raster.ByteRaster;
import org.scenarium.struct.raster.RasterStrategy;

import net.sf.marineapi.nmea.sentence.Sentence;

public class DrawerManager {
	private static LinkedHashMap<Class<?>, Class<? extends TheaterPanel>> drawerManager = new LinkedHashMap<>();
	private static final EventListenerList drawerListeners = new EventListenerList();
	static {
		drawerManager.put(ByteRaster.class, ImageDrawer.class);
		drawerManager.put(RasterStrategy.class, ImageDrawer.class);
		drawerManager.put(BufferedImageStrategy.class, ImageDrawer.class);
		drawerManager.put(BufferedImage.class, ImageDrawer.class);
		drawerManager.put(FlowDiagram.class, DiagramDrawer.class);
		drawerManager.put(GeographicCoordinate.class, GeographicalDrawer.class);
		drawerManager.put(Curve.class, ChartDrawer.class);
		drawerManager.put(CurveSeries.class, ChartDrawer.class);
		drawerManager.put(OldObject3D.class, Drawer3D.class);
		drawerManager.put(Sentence.class, GPSDrawer.class);
	}

	public static boolean registerDrawer(Class<? extends Object> drawerClass, Class<? extends TheaterPanel> theaterPanel) {
		if(drawerManager.put(drawerClass, theaterPanel) == null) {
			fireDrawerLoaded(theaterPanel);
			return true;
		}
		return false;
	}

	public static void replaceDrawer(Class<? extends Object> drawableElement, Class<? extends TheaterPanel> theaterPanel) {
		drawerManager.put(drawableElement, theaterPanel);
	}

	public static void purgeDrawers(Module module) {
		for (Iterator<Class<? extends TheaterPanel>> iterator = drawerManager.values().iterator(); iterator.hasNext();) {
			Class<? extends TheaterPanel> drawerClass = iterator.next();
			if (drawerClass.getModule().equals(module)) {
				iterator.remove();
				fireDrawerUnloaded(drawerClass);
			}
		}
	}
	
	public static void addLoadDrawerListener(LoadDrawerListener listener) {
		drawerListeners.add(LoadDrawerListener.class, listener);
	}

	public static void removeLoadDrawerListener(LoadDrawerListener listener) {
		drawerListeners.remove(LoadDrawerListener.class, listener);
	}
	
	private static void fireDrawerLoaded(Class<? extends TheaterPanel> drawerClass) {
		for (LoadDrawerListener listener : drawerListeners.getListeners(LoadDrawerListener.class))
			listener.loaded(drawerClass);
	}
	
	private static void fireDrawerUnloaded(Class<? extends TheaterPanel> drawerClass) {
		for (LoadDrawerListener listener : drawerListeners.getListeners(LoadDrawerListener.class))
			listener.unloaded(drawerClass);		
	}

	public static boolean isCompatibleRenderPanel(TheaterPanel theaterPane, Class<? extends Object> drawableElement) {
		Class<? extends TheaterPanel> rpt = drawerManager.get(drawableElement);
		if (rpt == null) {
			PropertyEditor<?> editor = PropertyEditorManager.findEditor(drawableElement, "");
			if (editor != null)
				rpt = PrimitiveDrawer.class;
			if (theaterPane.getClass().equals(rpt))
				return ((PrimitiveDrawer) theaterPane).getEditor().getClass().equals(editor.getClass());
			return false;
		}
		return theaterPane.getClass().equals(rpt);
	}

	public static Class<? extends TheaterPanel> getRenderPanelTypeBase(Class<?> drawableElement) {
		Class<? extends TheaterPanel> rpt = drawerManager.get(drawableElement);
		if (rpt == null) {
			PropertyEditor<?> editor = PropertyEditorManager.findEditor(drawableElement, "");
			if (editor != null && editor.hasCustomEditor())
				return PrimitiveDrawer.class;
		}
		return rpt;
	}
	
	public static void main(String[] args) {
		LinkedHashMap<Class<?>, String> map = new LinkedHashMap<>();
		map.put(Object.class, "Object");
		map.put(Number.class, "Number");
//		map.put(Integer.class, "Integer");
		System.out.println(getRenderPanelParentObjectTest(map, Integer.class));
		
		map = new LinkedHashMap<>();
//		map.put(Number.class, "Number");
//		map.put(Object.class, "Object");
		map.put(Integer.class, "Integer");
		System.out.println(getRenderPanelParentObjectTest(map, Integer.class));
	}
	
	private static Class<? extends Object> getRenderPanelParentObjectTest(LinkedHashMap<Class<?>, String> map, Class<?> drawableElement) {
		Class<?> _drawableElement = null;
		for (Class<? extends Object> key : map.keySet())
			if (key.isAssignableFrom(drawableElement) && (_drawableElement == null || _drawableElement.isAssignableFrom(key)))
				_drawableElement = key;
		return _drawableElement;
	}

	/**
	 * Get the more specialized drawable object class from the registered drawer map correponding to the given drawable object class. The output class must either be the input class or a parent class.
	 * @param drawableObjectClass the drawable object class
	 * @return the more specialized drawable class correponding to the drawableObjectClass
	 */
	public static Class<?> getSpecializedDrawableObjectClass(Class<?> drawableObjectClass) {
		Class<?> _drawableElement = null;
		for (Class<? extends Object> key : drawerManager.keySet())
			if (key.isAssignableFrom(drawableObjectClass) && (_drawableElement == null || _drawableElement.isAssignableFrom(key)))
				_drawableElement = key;
		return _drawableElement == null ? drawableObjectClass : _drawableElement;
	}

	/**
	 * Get the render panel class corresponding to a given drawable object class.
	 * @param drawableObjectClass the drawable object class 
	 * @return the render panel class associated to the drawableObjectClass
	 */
	public static Class<? extends TheaterPanel> getRenderPanelClass(Class<? extends Object> drawableObjectClass) {
		return getRenderPanelTypeBase(getSpecializedDrawableObjectClass(drawableObjectClass));
	}

	public static ArrayList<Class<? extends TheaterPanel>> getDrawers() {
		return new ArrayList<>(drawerManager.values());
	}
}
