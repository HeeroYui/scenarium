/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.display.drawer.camera3D;

import javax.vecmath.Vector3d;

import com.jogamp.opengl.GL2;

import javafx.scene.Node;

public interface Camera {

	void drawCameraInfo(GL2 gl);

	double getDistance();

	void getMatrix(double[] matrix);

	public Vector3d getViewCenter();

	void init(CameraChangeListener listener);

	void reset();

	void setBounds(float width, float height);

	void setGl(GL2 gl);

	void setNode(Node node);
}
