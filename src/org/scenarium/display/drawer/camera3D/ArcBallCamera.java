/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.display.drawer.camera3D;

import static com.jogamp.opengl.GL.GL_LINES;

import java.awt.Point;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point2d;
import javax.vecmath.Quat4d;
import javax.vecmath.Vector3d;

import org.beanmanager.editors.PropertyInfo;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.fixedfunc.GLMatrixFunc;
import com.jogamp.opengl.glu.GLU;

import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.input.MouseButton;

public class ArcBallCamera implements Camera {
	private static final double Epsilon = 1.0e-5f;

	public static void matToDouble(Matrix4d mat, double[] dest) {
		dest[0] = mat.m00;
		dest[1] = mat.m10;
		dest[2] = mat.m20;
		dest[3] = mat.m30;
		dest[4] = mat.m01;
		dest[5] = mat.m11;
		dest[6] = mat.m21;
		dest[7] = mat.m31;
		dest[8] = mat.m02;
		dest[9] = mat.m12;
		dest[10] = mat.m22;
		dest[11] = mat.m32;
		dest[12] = mat.m03;
		dest[13] = mat.m13;
		dest[14] = mat.m23;
		dest[15] = mat.m33;
	}

	Vector3d stVec; // Saved click vector
	Vector3d enVec; // Saved drag vector
	double adjustWidth; // Mouse bounds width
	double adjustHeight; // Mouse bounds height
	private Matrix4d lastRot = new Matrix4d();
	private boolean dragMode;
	@PropertyInfo(nullable = false)
	private Matrix4d rot = new Matrix4d();
	public double deltaX;
	public double deltaY;
	public double distance = -50;
	public double centerRotX = 0;
	public double centerRotY = 0;
	private Point2D anchorPoint;
	private CameraChangeListener listener;

	private GL2 gl;

	public ArcBallCamera() {
		stVec = new Vector3d();
		enVec = new Vector3d();
		lastRot.setIdentity();
		rot.setIdentity();
	}

	public void drag(Point newPt, Quat4d newRot) {
		mapToSphere(newPt, enVec);
		if (newRot != null) {
			Vector3d perp = new Vector3d();
			perp.cross(stVec, enVec);
			if (perp.length() > Epsilon) {
				newRot.x = perp.x;
				newRot.y = perp.y;
				newRot.z = perp.z;
				newRot.w = stVec.dot(enVec);
			} else
				newRot.x = newRot.y = newRot.z = newRot.w = 0.0f;
		}
	}

	public void drawAxis(GL2 gl) {
		gl.glLineWidth(3);
		gl.glBegin(GL_LINES);
		// x axis
		gl.glColor3f(1.0f, 0.0f, 0.0f);
		gl.glVertex3f(0.0f, 0.0f, 0.0f);
		gl.glVertex3f(1.0f, 0.0f, 0.0f);
		// y axis
		gl.glColor3f(0.0f, 1.0f, 0.0f);
		gl.glVertex3f(0.0f, 0.0f, 0.0f);
		gl.glVertex3f(0.0f, 1.0f, 0.0f);
		// Z axis
		gl.glColor3f(0.0f, 0.0f, 1.0f);
		gl.glVertex3f(0.0f, 0.0f, 0.0f);
		gl.glVertex3f(0.0f, 0.0f, 1.0f);
		gl.glEnd();
	}

	@Override
	public void drawCameraInfo(GL2 gl) {
		gl.glTranslated(centerRotX, centerRotY, 0);
		drawAxis(gl);
		gl.glTranslated(-centerRotX, -centerRotY, 0);
	}

	public double getCenterRotX() {
		return centerRotX;
	}

	public double getCenterRotY() {
		return centerRotY;
	}

	public double getDeltaX() {
		return deltaX;
	}

	public double getDeltaY() {
		return deltaY;
	}

	@Override
	public double getDistance() {
		return distance;
	}

	@Override
	public void getMatrix(double[] matrix) {
		Matrix4d trans = new Matrix4d(rot);
		trans.m03 = deltaX + centerRotX;
		trans.m13 = deltaY + centerRotY;
		trans.m23 = distance;
		Matrix4d t = new Matrix4d();
		t.setIdentity();
		t.m03 = -centerRotX;
		t.m13 = -centerRotY;
		trans.mul(t);
		matToDouble(trans, matrix);
	}

	public Matrix4d getRot() {
		return rot;
	}

	@Override
	public Vector3d getViewCenter() {
		return new Vector3d(centerRotX, centerRotY, 0);
	}

	@Override
	public void init(CameraChangeListener listener) {
		this.listener = listener;
	}

	public void mapToSphere(Point point, Vector3d vector) {
		Point2d tempPoint = new Point2d(point.x, point.y);
		tempPoint.x = tempPoint.x * this.adjustWidth - 1;
		tempPoint.y = 1 - tempPoint.y * this.adjustHeight;
		double length = tempPoint.x * tempPoint.x + tempPoint.y * tempPoint.y;
		if (length > 1.0f) {
			double norm = 1.0 / Math.sqrt(length);
			vector.x = tempPoint.x * norm;
			vector.y = tempPoint.y * norm;
			vector.z = 0;
		} else {
			vector.x = tempPoint.x;
			vector.y = tempPoint.y;
			vector.z = Math.sqrt(1.0f - length);
		}

	}

	@Override
	public void reset() {
		lastRot.setIdentity();
		rot.setIdentity();
		deltaX = 0;
		deltaY = 0;
		distance = -50;
		centerRotX = 0;
		centerRotY = 0;
		listener.cameraChange();
	}

	@Override
	public void setBounds(float NewWidth, float NewHeight) {
		adjustWidth = 1.0f / ((NewWidth - 1.0f) * 0.5f);
		adjustHeight = 1.0f / ((NewHeight - 1.0f) * 0.5f);
	}

	public void setCenterRotX(double centerRotX) {
		this.centerRotX = centerRotX;
	}

	public void setCenterRotY(double centerRotY) {
		this.centerRotY = centerRotY;
	}

	public void setDeltaX(double deltaX) {
		this.deltaX = deltaX;
	}

	public void setDeltaY(double deltaY) {
		this.deltaY = deltaY;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	@Override
	public void setGl(GL2 gl) {
		this.gl = gl;
	}

	@Override
	public void setNode(Node node) {
		node.setOnMouseDragged(e -> {
			if (e.getButton() == MouseButton.PRIMARY) {
				if (dragMode == false) {
					dragMode = true;
					lastRot.set(rot);
					mapToSphere(new Point((int) e.getX(), (int) e.getY()), this.stVec);
				}
				Quat4d quat = new Quat4d();
				drag(new Point((int) e.getX(), (int) e.getY()), quat);
				setRotation(rot, quat);
				rot.mul(rot, lastRot);
				listener.cameraChange();
			} else if (e.getButton() == MouseButton.SECONDARY) {
				deltaX = anchorPoint.getX() * 0.01f + e.getX() * 0.01f;
				deltaY = anchorPoint.getY() * 0.01f - e.getY() * 0.01f;
				listener.cameraChange();
			}
		});
		node.setOnMousePressed(e -> {
			anchorPoint = null;
			if (e.getButton() == MouseButton.SECONDARY)
				anchorPoint = new Point2D(-e.getX() + deltaX * 100, e.getY() + deltaY * 100);
			else if (e.getButton() == MouseButton.PRIMARY && e.isControlDown()) {
				gl.glLoadIdentity();
				double[] matrix = new double[16];
				getMatrix(matrix);
				gl.glMultMatrixd(matrix, 0);

				GLU glu = new GLU();
				int viewport[] = new int[4];
				double mvmatrix[] = new double[16];
				double projmatrix[] = new double[16];
				int realy = 0;
				double wcoord[] = new double[4];
				gl.glGetIntegerv(GL.GL_VIEWPORT, viewport, 0);
				gl.glGetDoublev(GLMatrixFunc.GL_MODELVIEW_MATRIX, mvmatrix, 0);
				gl.glGetDoublev(GLMatrixFunc.GL_PROJECTION_MATRIX, projmatrix, 0);
				realy = viewport[3] - (int) e.getY() - 1;
				// position de la camera
				glu.gluUnProject((viewport[2] - viewport[0]) / 2.0, (viewport[3] - viewport[1]) / 2.0, 0, mvmatrix, 0, projmatrix, 0, viewport, 0, wcoord, 0);
				// System.out.println("cam pos: x:" + wcoord[0] + "y:" + wcoord[1] + "z:" + wcoord[2]);
				double Ax = wcoord[0];
				double Ay = wcoord[1];
				double Az = wcoord[2];
				glu.gluUnProject(e.getX(), realy, 1, mvmatrix, 0, projmatrix, 0, viewport, 0, wcoord, 0);
				double Bx = wcoord[0];
				double By = wcoord[1];
				double Bz = wcoord[2];
				double t = -Az / (Bz - Az); // on calcule t en fonction de la position de la camera(Az) et de (Bz)
				double Mx = t * (Bx - Ax) + Ax; // on calcule les positions de M avec t
				double My = t * (By - Ay) + Ay;
				if (Double.isNaN(Mx) || Double.isNaN(My))
					return;
				centerRotX = Mx;
				centerRotY = My;
				deltaX = -centerRotX;
				deltaY = -centerRotY;
				listener.cameraChange();
			}
		});
		node.setOnScroll(e -> {
			if (e.getDeltaY() != 0) {
				distance += distance * (e.getDeltaX() + e.getDeltaY() <= 0 ? 1.0 : -1.0) / (e.isShiftDown() && e.isControlDown() ? 100 : e.isShiftDown() ? 1000 : e.isControlDown() ? 30 : 10);
				listener.cameraChange();
			}
		});
		node.setOnMouseReleased(e -> dragMode = false);
	}

	public void setRot(Matrix4d rot) {
		if (rot == null)
			throw new IllegalArgumentException("Cannot be set to null for a nullable");
		this.rot = rot;
	}

	private static void setRotation(Matrix4d mat, Quat4d q1) {
		double n, s;
		double xs, ys, zs;
		double wx, wy, wz;
		double xx, xy, xz;
		double yy, yz, zz;
		n = q1.x * q1.x + q1.y * q1.y + q1.z * q1.z + q1.w * q1.w;
		s = n > 0.0f ? 2.0f / n : 0.0f;
		xs = q1.x * s;
		ys = q1.y * s;
		zs = q1.z * s;
		wx = q1.w * xs;
		wy = q1.w * ys;
		wz = q1.w * zs;
		xx = q1.x * xs;
		xy = q1.x * ys;
		xz = q1.x * zs;
		yy = q1.y * ys;
		yz = q1.y * zs;
		zz = q1.z * zs;
		mat.m00 = 1.0f - (yy + zz);
		mat.m01 = xy - wz;
		mat.m02 = xz + wy;
		mat.m03 = 0f;
		mat.m10 = xy + wz;
		mat.m11 = 1.0f - (xx + zz);
		mat.m12 = yz - wx;
		mat.m13 = 0f;
		mat.m20 = xz - wy;
		mat.m21 = yz + wx;
		mat.m22 = 1.0f - (xx + yy);
		mat.m23 = 0f;
		mat.m30 = 0f;
		mat.m31 = 0f;
		mat.m32 = 0f;
		mat.m33 = 1f;
	}
}
