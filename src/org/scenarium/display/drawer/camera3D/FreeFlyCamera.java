/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.display.drawer.camera3D;

import javax.vecmath.Vector3d;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.fixedfunc.GLMatrixFunc;
import com.jogamp.opengl.glu.GLU;

import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.input.MouseButton;

public class FreeFlyCamera implements Camera {
	private static final Vector3d m_axeVertical = new Vector3d(0, 0, 1);

	private static Vector3d mul(Vector3d v1, double v2) {
		Vector3d result = new Vector3d();
		result.x = v1.x * v2;
		result.y = v1.y * v2;
		result.z = v1.z * v2;
		return result;
	}

	private CameraChangeListener listener;
	float phi = -20;
	float theta = 90;
	float sensibility = 0.5f;
	float speed = 1f;
	public Vector3d orientation = new Vector3d();
	public Vector3d lateralDisplacement = new Vector3d();
	public Vector3d position = new Vector3d(0, -6, 6);
	public Vector3d targetPoint = new Vector3d();

	private Point2D anchorPoint;

	@Override
	public void drawCameraInfo(GL2 gl) {
		gl.glMatrixMode(GLMatrixFunc.GL_PROJECTION);
		gl.glPushMatrix();
		gl.glLoadIdentity();
		new GLU().gluOrtho2D(-1, 1, -1, 1);
		gl.glMatrixMode(GLMatrixFunc.GL_MODELVIEW);
		gl.glPushMatrix();
		gl.glLoadIdentity();
		gl.glBegin(GL.GL_LINE_LOOP);
		gl.glVertex2d(0.02, -0);
		gl.glVertex2d(-0.02, -0);
		gl.glEnd();
		gl.glBegin(GL.GL_LINE_LOOP);
		gl.glVertex2d(0, 0.02);
		gl.glVertex2d(0, -0.02);
		gl.glEnd();
		gl.glMatrixMode(GLMatrixFunc.GL_PROJECTION);
		gl.glPopMatrix();
		gl.glMatrixMode(GLMatrixFunc.GL_MODELVIEW);
		gl.glPopMatrix();
	}

	@Override
	public double getDistance() {
		// TODO Auto-generated method stub
		return position.z;
	}

	public Vector3d getLateralDisplacement() {
		return lateralDisplacement;
	}

	@Override
	public void getMatrix(double[] matrix) {
		Vector3d f = new Vector3d(targetPoint);
		f.sub(position);
		f.normalize();
		Vector3d s = new Vector3d();
		s.cross(f, m_axeVertical);
		s.normalize();
		Vector3d u = new Vector3d(m_axeVertical);
		u.cross(s, f);
		u.normalize();
		matrix[0] = s.x;
		matrix[1] = u.x;
		matrix[2] = -f.x;
		matrix[3] = 0.0f;
		matrix[4] = s.y;
		matrix[5] = u.y;
		matrix[6] = -f.y;
		matrix[7] = 0.0f;
		matrix[8] = s.z;
		matrix[9] = u.z;
		matrix[10] = -f.z;
		matrix[11] = 0.0f;
		matrix[12] = -s.dot(position);
		matrix[13] = -u.dot(position);
		matrix[14] = f.dot(position);
		matrix[15] = 1.0f;
	}

	public Vector3d getOrientation() {
		return orientation;
	}

	public Vector3d getPosition() {
		return position;
	}

	public float getSensibility() {
		return sensibility;
	}

	public float getSpeed() {
		return speed;
	}

	public Vector3d getTargetPoint() {
		return targetPoint;
	}

	@Override
	public Vector3d getViewCenter() {
		return targetPoint;
	}

	@Override
	public void init(CameraChangeListener listener) {
		this.listener = listener;
		orienter(0, 0);
	}

	private void orienter(float xRel, float yRel) {
		phi += -yRel * sensibility;
		theta += -xRel * sensibility;
		if (phi > 89.0)
			phi = 89.0f;
		else if (phi < -89.0)
			phi = -89.0f;
		float phiRadian = (float) (phi * Math.PI / 180);
		float thetaRadian = (float) (theta * Math.PI / 180);
		if (m_axeVertical.x == 1.0) {
			orientation.x = (float) Math.sin(phiRadian);
			orientation.y = (float) Math.cos(phiRadian) * (float) Math.cos(thetaRadian);
			orientation.z = (float) Math.cos(phiRadian) * (float) Math.sin(thetaRadian);
		} else if (m_axeVertical.y == 1.0) {
			orientation.x = (float) Math.cos(phiRadian) * (float) Math.sin(thetaRadian);
			orientation.y = (float) Math.sin(phiRadian);
			orientation.z = (float) Math.cos(phiRadian) * (float) Math.cos(thetaRadian);
		} else {
			orientation.x = (float) Math.cos(phiRadian) * (float) Math.cos(thetaRadian);
			orientation.y = (float) Math.cos(phiRadian) * (float) Math.sin(thetaRadian);
			orientation.z = (float) Math.sin(phiRadian);
		}
		lateralDisplacement.cross(m_axeVertical, orientation);
		lateralDisplacement.normalize();
		targetPoint = new Vector3d(position);
		targetPoint.add(orientation);
	}

	@Override
	public void reset() {
		orientation = new Vector3d();
		lateralDisplacement = new Vector3d();
		position = new Vector3d(0, -6, 6);
		targetPoint = new Vector3d();
		orienter(0, 0);
		listener.cameraChange();
	}

	@Override
	public void setBounds(float width, float height) {}

	@Override
	public void setGl(GL2 gl) {}

	public void setLateralDisplacement(Vector3d lateralDisplacement) {
		this.lateralDisplacement = lateralDisplacement;
	}

	@Override
	public void setNode(Node node) {
		node.setOnKeyPressed(e -> {
			float vitesse = this.speed;
			if (e.isShiftDown() && e.isControlDown())
				vitesse /= 100;
			else if (e.isShiftDown())
				vitesse /= 1000;
			else if (e.isControlDown())
				vitesse /= 10;

			switch (e.getCode()) {
			case UP:
			case Z:
				position.add(mul(orientation, vitesse));
				targetPoint.set(position);
				targetPoint.add(orientation);
				break;
			case DOWN:
			case S:
				position.sub(mul(orientation, vitesse));
				targetPoint.set(position);
				targetPoint.add(orientation);
				break;
			case LEFT:
			case Q:
				position.add(mul(lateralDisplacement, vitesse));
				targetPoint.set(position);
				targetPoint.add(orientation);
				break;
			case RIGHT:
			case D:
				position.sub(mul(lateralDisplacement, vitesse));
				targetPoint.set(position);
				targetPoint.add(orientation);
				break;
			case NUMPAD3:
				position.z -= vitesse;
				targetPoint.z -= vitesse;
				break;
			case NUMPAD9:
				position.z += vitesse;
				targetPoint.z += vitesse;
				break;
			case NUMPAD2:
				position.y -= vitesse;
				targetPoint.y -= vitesse;
				break;
			case NUMPAD8:
				position.y += vitesse;
				targetPoint.y += vitesse;
				break;
			default:
				return;
			}
			listener.cameraChange();
		});

		node.setOnMouseDragged(e -> {
			if (anchorPoint != null) {
				float dx = (float) (e.getX() - anchorPoint.getX());
				float dy = (float) (e.getY() - anchorPoint.getY());
				anchorPoint = new Point2D(e.getX(), e.getY());
				orienter(dx, dy);
				listener.cameraChange();
			}
		});
		node.setOnMousePressed(e -> {
			anchorPoint = null;
			if (e.getButton() == MouseButton.SECONDARY)
				anchorPoint = new Point2D(e.getX(), e.getY());
		});
		node.setOnScroll(e -> {
			if (e.getDeltaY() != 0) {
				float vitesse = this.speed;
				if (e.isShiftDown() && e.isControlDown())
					vitesse /= 100;
				else if (e.isShiftDown())
					vitesse /= 1000;
				else if (e.isControlDown())
					vitesse /= 10;
				if (e.getDeltaY() < 0) {
					position.sub(mul(orientation, vitesse));
					targetPoint.set(position);
					targetPoint.add(orientation);
				} else {
					position.add(mul(orientation, vitesse));
					targetPoint.set(position);
					targetPoint.add(orientation);
				}
				listener.cameraChange();
			}
		});
	}

	public void setOrientation(Vector3d orientation) {
		this.orientation = orientation;
	}

	public void setPosition(Vector3d position) {
		this.position = position;
	}

	public void setSensibility(float sensibility) {
		this.sensibility = sensibility;
	}

	public void setSpeed(float speed) {
		this.speed = speed;
	}

	public void setTargetPoint(Vector3d targetPoint) {
		this.targetPoint = targetPoint;
	}
}
