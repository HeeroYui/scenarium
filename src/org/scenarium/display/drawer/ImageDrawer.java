/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.display.drawer;

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferInt;
import java.awt.image.DataBufferShort;
import java.awt.image.DataBufferUShort;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import javax.vecmath.Point2d;
import javax.vecmath.Point2i;

import org.beanmanager.editors.PropertyInfo;
import org.beanmanager.editors.container.ArrayInfo;
import org.beanmanager.editors.primitive.number.NumberInfo;
import org.beanmanager.struct.BooleanProperty;
import org.beanmanager.struct.TreeNode;
import org.scenarium.display.ColorProvider;
import org.scenarium.display.ScenariumContainer;
import org.scenarium.display.StackableDrawer;
import org.scenarium.display.drawer.imagedrawers.ByteImageDrawer;
import org.scenarium.display.drawer.imagedrawers.DoubleImageDrawer;
import org.scenarium.display.drawer.imagedrawers.FloatImageDrawer;
import org.scenarium.display.drawer.imagedrawers.IntegerImageDrawer;
import org.scenarium.display.drawer.imagedrawers.ShortImageDrawer;
import org.scenarium.operator.image.ImageType;
import org.scenarium.operator.image.conversion.TypeConverter;
import org.scenarium.struct.BufferedStrategy;
import org.scenarium.struct.curve.CurveSeries;
import org.scenarium.struct.curve.Curved;
import org.scenarium.struct.raster.BufferedImageStrategy;
import org.scenarium.struct.raster.ByteRaster;
import org.scenarium.struct.raster.IntegerRaster;
import org.scenarium.struct.raster.Raster;
import org.scenarium.struct.raster.RasterStrategy;
import org.scenarium.timescheduler.Scheduler;

import javafx.beans.InvalidationListener;
import javafx.geometry.Dimension2D;
import javafx.geometry.Rectangle2D;
import javafx.geometry.VPos;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelBuffer;
import javafx.scene.image.PixelFormat;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.scene.transform.Affine;

public class ImageDrawer extends GeometricDrawer implements StackableDrawer {
	public static final int GRAY = 0;
	public static final int RGB = 1;
	public static final int BGR = 2;
	public static final int YUV = 3;
	public static final int ARGB = 4;
	public static final int ABGR = 5;
	public static final int INT_RGB = 6;
	public static final int INT_BGR = 7;
	private static final HashMap<Class<?>, ImageDrawerInfo> imageDrawers = new HashMap<>();
	private static final String RASTERFILTERS = "Raster";
	private static final String RASTER = "Raster";
	private static final String VALUE = "Value";
	private static final int[] grayColorMap;
	static {
		grayColorMap = new int[256];
		for (int i = 0; i < grayColorMap.length; i++)
			grayColorMap[i] = 0xff000000 | i << 16 | i << 8 | i;
	}
	// Element to draw image
	private PixelBuffer<IntBuffer> pixelBuffer;
	private ImageView imageView;
	private Rectangle2D imageRegion;

	// Filter of Theater
	private boolean filterRaster = true;
	private boolean filterValue = true;
	private ArrayList<Point2i> selectedCells = new ArrayList<>();
	private int[] viewWindow = new int[2 * 2];
	protected int[] getViewWindow() {
		return viewWindow.clone();
	}

	// Level of details
	protected int lodGrid = 5;
	protected int lodRasterValue1 = 15;
	protected int lodRasterValue3 = 25;
	// Image of Theater
	public boolean computeRaster = true;
	private boolean isRasterDrawn;
	private ColorProvider colorProvider = new ColorProvider();
	@PropertyInfo(index = 0, info = "Line width for each point of the point cloud")
	@NumberInfo(min = 1)
	private double pointCloudLineWidth = 1;
	@PropertyInfo(index = 1, info = "Line size for each point of the point cloud")
	@NumberInfo(min = 0)
	private double pointCloudLineSize = 5;
	protected boolean needToRefreshGc;

	static {
		imageDrawers.put(byte[].class, new ImageDrawerInfo(ByteImageDrawer::drawFullImage, ByteImageDrawer::drawImage, ByteImageDrawer::getRGBValue));
		imageDrawers.put(short[].class, new ImageDrawerInfo(ShortImageDrawer::drawFullImage, ShortImageDrawer::drawImage, ShortImageDrawer::getRGBValue));
		imageDrawers.put(int[].class, new ImageDrawerInfo(IntegerImageDrawer::drawFullImage, IntegerImageDrawer::drawImage, IntegerImageDrawer::getRGBValue));
		imageDrawers.put(float[].class, new ImageDrawerInfo(FloatImageDrawer::drawFullImage, FloatImageDrawer::drawImage, FloatImageDrawer::getRGBValue));
		imageDrawers.put(double[].class, new ImageDrawerInfo(DoubleImageDrawer::drawFullImage, DoubleImageDrawer::drawImage, DoubleImageDrawer::getRGBValue));
	}

	@Override
	public void initialize(ScenariumContainer container, Scheduler scheduler, Object drawableElement, boolean autoFitIfResize) {
		super.initialize(container, scheduler, drawableElement, autoFitIfResize);
		imageView = new ImageView();
		InvalidationListener il = (b) -> {
			int width = (int) getWidth();
			int height = (int) getHeight();
			if (width <= 0 || height <= 0)
				return;
			pixelBuffer = new PixelBuffer<>(width, height, IntBuffer.wrap(new int[width * height]), PixelFormat.getIntArgbPreInstance());
			imageRegion = new Rectangle2D(0, 0, width, height);
			imageView.setImage(new WritableImage(pixelBuffer));
		};
		il.invalidated(null);
		widthProperty().addListener(il);
		heightProperty().addListener(il);
		imageView.fitWidthProperty().bind(widthProperty());
		imageView.fitHeightProperty().bind(heightProperty());
		getChildren().add(0, imageView);
	}

	@Override
	public void scale1AndReplace() {
		super.scale1AndReplace();
		updateTransform(1, 0, 0);
	}

	@Override
	public Dimension2D getDimension() {
		Object de = getDrawableElement();
		if (de instanceof RasterStrategy)
			return ((RasterStrategy) getDrawableElement()).getDimension();
		else if (de instanceof Raster)
			return new Dimension2D(((Raster) de).getWidth(), ((Raster) de).getHeight());
		else if (de instanceof BufferedImageStrategy)
			return ((BufferedImageStrategy) de).getDimension();
		else
			return new Dimension2D(((BufferedImage) de).getWidth(), ((BufferedImage) de).getHeight());
	}

	@Override
	protected void paint(Object _dataElement) {
		Object dataElement = _dataElement instanceof BufferedStrategy<?> ? ((BufferedStrategy<?>) _dataElement).getDrawElement() : _dataElement;
		if (dataElement instanceof BufferedImage) {
			BufferedImage image = (BufferedImage) dataElement;
			int imgType = image.getType();
			if (imgType == BufferedImage.TYPE_BYTE_BINARY || imgType == BufferedImage.TYPE_BYTE_INDEXED || imgType == BufferedImage.TYPE_USHORT_555_RGB || imgType == BufferedImage.TYPE_USHORT_565_RGB
					|| imgType == BufferedImage.TYPE_CUSTOM) {
				paint(new TypeConverter(imgType == BufferedImage.TYPE_BYTE_BINARY ? ImageType.TYPE_BYTE_GRAY : ImageType.TYPE_INT_ARGB).process(image));
				return;
			}
		}

		int imageWidth, imageHeight, imageType;
		Object imageData;
		if (dataElement instanceof BufferedImage) {
			BufferedImage image = (BufferedImage) dataElement;
			imageWidth = image.getWidth();
			imageHeight = image.getHeight();
			DataBuffer db = image.getRaster().getDataBuffer();
			imageData = db instanceof DataBufferByte ? ((DataBufferByte) db).getData()
					: db instanceof DataBufferShort ? ((DataBufferShort) db).getData() : db instanceof DataBufferUShort ? ((DataBufferUShort) db).getData() : ((DataBufferInt) db).getData();
			switch (image.getType()) {
			case BufferedImage.TYPE_BYTE_GRAY:
			case BufferedImage.TYPE_USHORT_GRAY:
				imageType = GRAY;
				break;
			case BufferedImage.TYPE_3BYTE_BGR:
				imageType = BGR;
				break;
			case BufferedImage.TYPE_4BYTE_ABGR:
				imageType = ABGR;
				break;
			case BufferedImage.TYPE_4BYTE_ABGR_PRE:
				imageType = ABGR;
				break;
			case BufferedImage.TYPE_INT_ARGB:
				imageType = ARGB;
				break;
			case BufferedImage.TYPE_INT_ARGB_PRE:
				imageType = ARGB;
				break;
			case BufferedImage.TYPE_INT_BGR:
				imageType = INT_BGR;
				break;
			case BufferedImage.TYPE_INT_RGB:
				imageType = INT_RGB;
				break;
			default:
				imageType = 0;
			}
		} else {
			Raster image = (Raster) dataElement;
			imageWidth = image.getWidth();
			imageHeight = image.getHeight();
			imageData = image.getData();
			switch (image.getType()) {
			case Raster.GRAY:
				imageType = GRAY;
				break;
			case Raster.BGR:
				imageType = BGR;
				break;
			case Raster.RGB:
				imageType = RGB;
				break;
			case Raster.YUV:
				imageType = YUV;
				break;
			case IntegerRaster.ARGB8:
				imageType = ARGB;
				break;
			case IntegerRaster.ABGR8:
				imageType = ABGR;
				break;
			default:
				imageType = 0;
			}

		}
		double scale = getScale();
		double tx = getxTranslate();
		double ty = getyTranslate();
		if (pixelBuffer != null && (filterRaster || isRasterDrawn))
			pixelBuffer.updateBuffer(pb -> {
				int[] viewerData = pb.getBuffer().array();
				if (!filterRaster) {
					Arrays.fill(viewerData, 0);
					isRasterDrawn = false;
					return imageRegion;
				}
				int viewerHeight = (int) getHeight();
				int viewerWidth = (int) getWidth();

				if (scale == 1 && imageWidth == viewerWidth && imageHeight == viewerHeight && tx == 0 && ty == 0)
					imageDrawers.get(imageData.getClass()).getFirst().drawImage(imageData, imageType, viewerData);
				else {
					double invScale = 1 / scale;
					double xTranslate = -tx * invScale;
					double yTranslate = -ty * invScale;

					int pos = (viewerWidth - 1 + (viewerHeight - 1) * viewerWidth);
					int nbToSkipX = 0;
					double boundX = viewerWidth * invScale + xTranslate;
					if (boundX > imageWidth)
						nbToSkipX = (int) Math.round((boundX - imageWidth) / invScale);
					int width = viewerWidth - nbToSkipX;
					int endY = 0;
					double boundY = viewerHeight * invScale + yTranslate;
					if (boundY > imageHeight)
						endY = (int) ((boundY - imageHeight) * scale);
					int startX = (int) Math.ceil(-xTranslate * scale);
					int startY = (int) Math.ceil(-yTranslate * scale);
					if (startY > 0)
						Arrays.fill(viewerData, 0, startY * viewerWidth, 0);
					int height = viewerHeight;
					if (endY != 0) {
						height -= endY;
						pos -= endY * viewerWidth;
						Arrays.fill(viewerData, pos, pos + endY * viewerWidth, 0);
					}
					double startXOnRaster = (width - 1) * invScale + xTranslate;
					double startYOnRaster = (height - 1) * invScale + yTranslate;
					imageDrawers.get(imageData.getClass()).getSecond().drawImage(imageData, imageType, imageWidth, imageHeight, viewerData, Math.max(0, startX), Math.max(0, startY), width, height,
							startXOnRaster, startYOnRaster, invScale, viewerWidth);
					isRasterDrawn = true;
				}
				return imageRegion;
			});

		boolean viewWindowsComputed = false;
		// Draw values
		GraphicsContext g = gc;
		Object[] adds = getAdditionalDrawableElement();
		boolean needToRefresh = needToRefreshGc || (adds != null && adds.length != 0) || filterROI;
		needToRefreshGc = false;
		boolean clearRectDone = false;
		if (scale > lodRasterValue1 && filterValue) {
			int imageDepth = dataElement instanceof BufferedImage
					? ((((BufferedImage) dataElement).getType() == BufferedImage.TYPE_BYTE_GRAY || ((BufferedImage) dataElement).getType() == BufferedImage.TYPE_USHORT_GRAY) ? 1 : 3)
					: ((Raster) dataElement).getDepth();
			g.setFill(Color.BLACK);
			if ((imageDepth == 1 || (imageDepth == 3 && scale > lodRasterValue3))) {
				if (clearRectDone == false) {
					clearScreen(g);
					clearRectDone = true;
				}
				if (viewWindowsComputed == false) {
					computeViewWindow();
					viewWindowsComputed = true;
				}
				int xTranslate = (int) getxTranslate();
				int yTranslate = (int) getyTranslate();
				Affine oldTransform = g.getTransform();
				g.setTransform(1, 0, 0, 1, 0, 0);
				g.setTextAlign(TextAlignment.CENTER);
				g.setTextBaseline(VPos.CENTER);
				// if(imageData.getClass() == byte[].class)
				ImageValueProvider ivp = imageDrawers.get(imageData.getClass()).getImageValueProvider();
				Number testVal = ivp.getRGBValue(imageData, imageWidth, imageType, 0, 0, 0);
				if (testVal instanceof Float || testVal instanceof Double) {
					g.setFont(new Font(12 + (imageDepth == 1 ? 0.52 * (scale - lodRasterValue1) : 0.26 * (scale - lodRasterValue3))));
					for (int i = viewWindow[0]; i <= viewWindow[2]; i++)
						for (int j = viewWindow[1]; j <= viewWindow[3]; j++) {
							if (imageDepth == 1) {
								double value = ivp.getRGBValue(imageData, imageWidth, imageType, i, j, 0).doubleValue();
								g.setFill(value > 0.5 ? Color.BLACK : Color.WHITE);
								g.fillText(Double.toString(value), (int) Math.ceil(scale * (i + 0.5) + xTranslate), (int) Math.ceil(scale * (j + 0.5) + yTranslate));
							} else if (imageDepth == 3) {
								float gap = 1.0f / (imageDepth + 1);
								double value = ivp.getRGBValue(imageData, imageWidth, imageType, i, j, 0).doubleValue();
								g.setFill(value > 0.5 ? new Color(0.5, 0, 0, 1) : Color.RED);
								g.fillText(Double.toString(value), (int) Math.ceil(scale * (i + 0.5) + xTranslate), (int) Math.ceil(scale * (j + gap * 1) + yTranslate));
								value = ivp.getRGBValue(imageData, imageWidth, imageType, i, j, 1).doubleValue();
								g.setFill(value > 0.5 ? new Color(0, 0.5, 0, 1) : Color.LIME);
								g.fillText(Double.toString(value), (int) Math.ceil(scale * (i + 0.5) + xTranslate), (int) Math.ceil(scale * (j + gap * 2) + yTranslate));
								value = ivp.getRGBValue(imageData, imageWidth, imageType, i, j, 2).doubleValue();
								g.setFill(value > 0.5 ? new Color(0, 0, 0.5, 1) : Color.BLUE);
								g.fillText(Double.toString(value), (int) Math.ceil(scale * (i + 0.5) + xTranslate), (int) Math.ceil(scale * (j + gap * 3) + yTranslate));
							}
						}
				} else {
					if (imageData instanceof byte[] || (imageData instanceof int[] && (imageType == ARGB || imageType == ABGR || imageType == INT_RGB || imageType == INT_BGR)))
						g.setFont(new Font(7 + (imageDepth == 1 ? 0.4 * (scale - lodRasterValue1) : 0.3 * (scale - lodRasterValue3))));
					else
						g.setFont(new Font(5 + (imageDepth == 1 ? 0.2 * (scale - lodRasterValue1) : 0.1 * (scale - lodRasterValue3))));
					double typeThreshold = imageData instanceof byte[] ? Byte.MAX_VALUE / 2
							: imageData instanceof short[] ? Short.MAX_VALUE / 2
									: (imageData instanceof int[] && (imageType != ARGB && imageType != ABGR && imageType != INT_RGB && imageType != INT_BGR)) ? Integer.MAX_VALUE / 2 : 0.5;
					for (int i = viewWindow[0]; i <= viewWindow[2]; i++)
						for (int j = viewWindow[1]; j <= viewWindow[3]; j++) {
							if (imageDepth == 1) {
								long value = ivp.getRGBValue(imageData, imageWidth, imageType, i, j, 0).longValue();
								g.setFill(value > typeThreshold ? Color.BLACK : Color.WHITE);
								g.fillText(Long.toString(value), (int) Math.ceil(scale * (i + 0.5) + xTranslate), (int) Math.ceil(scale * (j + 0.5) + yTranslate));
							} else if (imageDepth == 3) {
								float gap = 1.0f / (imageDepth + 1);
								long value = ivp.getRGBValue(imageData, imageWidth, imageType, i, j, 0).longValue();
								g.setFill(value > typeThreshold ? new Color(0.5, 0, 0, 1) : Color.RED);
								g.fillText(Long.toString(value), (int) Math.ceil(scale * (i + 0.5) + xTranslate), (int) Math.ceil(scale * (j + gap * 1) + yTranslate));
								value = ivp.getRGBValue(imageData, imageWidth, imageType, i, j, 1).longValue();
								g.setFill(value > typeThreshold ? new Color(0, 0.5, 0, 1) : Color.LIME);
								g.fillText(Long.toString(value), (int) Math.ceil(scale * (i + 0.5) + xTranslate), (int) Math.ceil(scale * (j + gap * 2) + yTranslate));
								value = ivp.getRGBValue(imageData, imageWidth, imageType, i, j, 2).longValue();
								g.setFill(value > typeThreshold ? new Color(0, 0, 0.5, 1) : Color.BLUE);
								g.fillText(Long.toString(value), (int) Math.ceil(scale * (i + 0.5) + xTranslate), (int) Math.ceil(scale * (j + gap * 3) + yTranslate));
							}
						}
				}
				g.setTransform(oldTransform);
				needToRefresh = true;
			}
		}
		if(clearRectDone == false && needToRefresh) {
			clearScreen(g);
		}
		if (needToRefresh) { // TODO marche pas
			if (filterGrid && scale > lodGrid) {
				if (viewWindowsComputed == false)
					computeViewWindow();
				g.setStroke(Color.BLACK);
				g.setLineWidth(1 / getScale());
				int beginX = viewWindow[0];
				int endX = viewWindow[2] + 1;
				int beginY = viewWindow[1];
				int endY = viewWindow[3] + 1;
				int end = endY;
				for (int i = beginX; i <= endX; i++)
					g.strokeLine(i, beginY, i, end);
				end = endX;
				for (int i = beginY; i <= endY; i++)
					g.strokeLine(beginX, i, end, i);
			} else if (filterBorder) {
				g.setStroke(Color.BLACK);
				g.setLineWidth(1 / getScale());
				g.strokeRect(0, 0, sWid, sHei);
			}
			if (filterROI) {
				g.setStroke(Color.RED);
				g.strokeRect(roi[0], roi[1], roi[2], roi[3]);
			}
			if (adds != null) {
				colorProvider.resetIndex();
				g.setLineWidth(pointCloudLineWidth / getScale());
				double lineSize = pointCloudLineSize;
				for (Object add : adds)
					if (add instanceof Point2d[]) {
						g.setStroke(colorProvider.getNextColor());
						for (Point2d point : (Point2d[]) add) {
							double u = point.x;
							double v = point.y;
							g.strokeLine(u + lineSize, v, u - lineSize, v);
							g.strokeLine(u, v - lineSize, u, v + lineSize);
						}
					}
			}
			if (selectAreaRect != null) {
				g.setStroke(Color.LIME);
				g.setLineWidth(1 / getScale());
				int x = (int) (selectAreaRect[0].x + 0.5);
				int y = (int) (selectAreaRect[0].y + 0.5);
				int width = (int) Math.round(selectAreaRect[1].x + 0.5);
				int height = (int) Math.round(selectAreaRect[1].y + 0.5);
				g.strokeRect(x, y, width, height);
			}
		}

	}

	protected void clearScreen(GraphicsContext g) {
		Affine oldTransform = g.getTransform();
		g.setTransform(1, 0, 0, 1, 0, 0);
		g.clearRect(0, 0, getWidth(), getHeight());
		g.setTransform(oldTransform);
	}

	private void computeViewWindow() {
		double xTranslate = -getxTranslate();
		double yTranslate = -getyTranslate();
		double scale = getScale();
		int value = (int) (xTranslate / scale);
		viewWindow[0] = value >= 0 ? value : 0;
		value = (int) (yTranslate / scale);
		viewWindow[1] = value >= 0 ? value : 0;
		value = (int) ((xTranslate + getWidth() - 1) / scale);
		viewWindow[2] = value < sWid ? value : sWid - 1;
		value = (int) ((yTranslate + getHeight() - 1) / scale);
		viewWindow[3] = value < sHei ? value : sHei - 1;
	}

	public static <T> void addCloner(Class<T> _class, Object src, Object val) {
		imageDrawers.get(_class).getFirst().drawImage(src, 1, null);
	}

	protected int getLodRasterValue() {
		Object dataElement = getDrawableElement();
		if (dataElement instanceof BufferedStrategy<?>)
			dataElement = ((BufferedStrategy<?>) dataElement).getDrawElement();
		return (dataElement instanceof ByteRaster ? ((ByteRaster) dataElement).getDepth() : ((BufferedImage) dataElement).getColorModel().getPixelSize() / 8) == 1 ? lodRasterValue1 : lodRasterValue3;
	}

	@Override
	protected void populateTheaterFilter() {
		super.populateTheaterFilter();
		TreeNode<BooleanProperty> rasNode = new TreeNode<>(new BooleanProperty(RASTERFILTERS, true));
		rasNode.addChild(new TreeNode<>(new BooleanProperty(RASTER, true)));
		rasNode.addChild(new TreeNode<>(new BooleanProperty(VALUE, true)));
		theaterFilter.addChild(rasNode);
	}

	@Override
	protected double adjustScale(double scale) {
		return scale > lodGrid ? Math.round(scale) : scale;
	}

	@Override
	protected void updateTransform(double scale, double tx, double ty) {
		if (scale != getScale() || tx != getxTranslate() || ty != getyTranslate())
			needToRefreshGc = true;
		super.updateTransform(scale, tx, ty);
	}

	@Override
	public float getIntelligentZoomThreshold() {
		return lodGrid;
	}

	@Override
	public float getZoomScale() {
		return getLodRasterValue() + 1;
	}

	@Override
	protected boolean isAreaSelectionPoint(int x, int y) {
		return true;
	}

	@Override
	protected void selectAreaChanged(boolean isCtrlDown) {
		if (selectArea && !isCtrlDown)
			selectedCells.clear();
		needToRefreshGc = true;
	}

	public double getPointCloudLineWidth() {
		return pointCloudLineWidth;
	}

	public void setPointCloudLineWidth(double pointCloudLineWidth) {
		this.pointCloudLineWidth = pointCloudLineWidth;
		repaint(false);
	}

	public double getPointCloudLineSize() {
		return pointCloudLineSize;
	}

	public void setPointCloudLineSize(double pointCloudLineSize) {
		this.pointCloudLineSize = pointCloudLineSize;
		repaint(false);
	}

	@ArrayInfo(prefHeight = 26 * 8 + 2)
	@PropertyInfo(index = 2, nullable = false, info = "Color for the additional input point cloud")
	public Color[] getPointCloudsColor() {
		return colorProvider.getColors();
	}

	public void setPointCloudsColor(Color[] pointCloudColor) {
		colorProvider = new ColorProvider(pointCloudColor);
		repaint(false);
	}

	@Override
	public boolean updateFilterWithPath(String[] filterPath, boolean value) {
		needToRefreshGc = true;
		if (filterPath[filterPath.length - 1].equals(RASTER))
			filterRaster = value;
		else if (filterPath[filterPath.length - 1].equals(VALUE))
			filterValue = value;
		else
			return super.updateFilterWithPath(filterPath, value);
		repaint(false);
		return true;
	}

	@Override
	public boolean isValidAdditionalInput(Class<?>[] inputsType, Class<?> additionalInput) {
		return additionalInput == null ? false
				: Curved.class.isAssignableFrom(additionalInput) || CurveSeries.class.isAssignableFrom(additionalInput) || Point2d[].class.isAssignableFrom(additionalInput);
	}

	@Override
	public boolean canAddInputToRenderer(Class<?>[] class1) {
		return true;
	}
}

@FunctionalInterface
interface NominalDrawer {
	public void drawImage(Object src, int srcType, int[] dest);
}

@FunctionalInterface
interface Drawer {
	public void drawImage(Object src, int srcType, int srcWid, int srcHei, int[] dest, int startX, int startY, int width, int height, double startXOnRaster, double startYOnRaster, double invScale,
			int frameWidht);
}

@FunctionalInterface
interface ImageValueProvider {
	public Number getRGBValue(Object imageData, int imageWidth, int imageType, int x, int y, int z);
}

class ImageDrawerInfo {
	private final NominalDrawer fullImageDrawer;
	private final Drawer drawer;
	private final ImageValueProvider imageValueProvider;

	public ImageDrawerInfo(NominalDrawer first, Drawer second, ImageValueProvider imageValueProvider) {
		this.fullImageDrawer = first;
		this.drawer = second;
		this.imageValueProvider = imageValueProvider;
	}

	public NominalDrawer getFirst() {
		return fullImageDrawer;
	}

	public Drawer getSecond() {
		return drawer;
	}

	public ImageValueProvider getImageValueProvider() {
		return imageValueProvider;
	}
}
