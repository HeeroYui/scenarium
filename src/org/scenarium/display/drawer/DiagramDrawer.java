/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.display.drawer;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyChangeListener;
import java.beans.PropertyDescriptor;
import java.io.File;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;

import org.beanmanager.BeanDesc;
import org.beanmanager.BeanManager;
import org.beanmanager.SubBeanExpansionListener;
import org.beanmanager.editors.DynamicSizeEditorChangeListener;
import org.beanmanager.editors.container.BeanEditor;
import org.beanmanager.editors.primitive.number.NumberInfo;
import org.beanmanager.rmi.RMIBeanManager;
import org.beanmanager.rmi.client.RemoteBean;
import org.beanmanager.struct.BooleanProperty;
import org.beanmanager.struct.TreeNode;
import org.beanmanager.tools.FxUtils;
import org.beanmanager.tools.Tuple;
import org.scenarium.display.AnimationTimerConsumer;
import org.scenarium.display.RenderPane;
import org.scenarium.display.ScenariumContainer;
import org.scenarium.display.toolBar.Operators;
import org.scenarium.editors.NotChangeableAtRuntime;
import org.scenarium.filemanager.scenario.dataflowdiagram.Block;
import org.scenarium.filemanager.scenario.dataflowdiagram.BlockIO;
import org.scenarium.filemanager.scenario.dataflowdiagram.BlockInfo;
import org.scenarium.filemanager.scenario.dataflowdiagram.BlockInput;
import org.scenarium.filemanager.scenario.dataflowdiagram.BlockOutput;
import org.scenarium.filemanager.scenario.dataflowdiagram.DynamicFlowDiagramDepictionChangeListener;
import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;
import org.scenarium.filemanager.scenario.dataflowdiagram.FlowDiagram;
import org.scenarium.filemanager.scenario.dataflowdiagram.FlowDiagramChangeListener;
import org.scenarium.filemanager.scenario.dataflowdiagram.FlowDiagramInput;
import org.scenarium.filemanager.scenario.dataflowdiagram.FlowDiagramOutput;
import org.scenarium.filemanager.scenario.dataflowdiagram.IO;
import org.scenarium.filemanager.scenario.dataflowdiagram.IllegalInputArgument;
import org.scenarium.filemanager.scenario.dataflowdiagram.Input;
import org.scenarium.filemanager.scenario.dataflowdiagram.Link;
import org.scenarium.filemanager.scenario.dataflowdiagram.LocalisedObject;
import org.scenarium.filemanager.scenario.dataflowdiagram.ModificationType;
import org.scenarium.filemanager.scenario.dataflowdiagram.Output;
import org.scenarium.filemanager.scenario.dataflowdiagram.ProcessMode;
import org.scenarium.filemanager.scenario.dataflowdiagram.RunningStateChangeListener;
import org.scenarium.filemanager.scenario.dataflowdiagram.VarArgsInputChangeListener;
import org.scenarium.filemanager.scenario.dataflowdiagram.operator.EvolvedVarArgsOperator;
import org.scenarium.filemanager.scenario.dataflowdiagram.operator.rmi.RemoteOperator;
import org.scenarium.filemanager.scenario.dataflowdiagram.scheduler.DiagramScheduler;
import org.scenarium.filemanager.scenariomanager.LocalScenario;
import org.scenarium.filemanager.scenariomanager.Scenario;
import org.scenarium.filemanager.scenariomanager.ScenarioManager;
import org.scenarium.timescheduler.Scheduler;
import org.scenarium.timescheduler.SchedulerPropertyChangeListener;
import org.scenarium.timescheduler.SchedulerState;
import org.scenarium.timescheduler.VisuableSchedulable;

import javafx.animation.AnimationTimer;
import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.geometry.Bounds;
import javafx.geometry.Dimension2D;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.geometry.VPos;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Circle;
import javafx.scene.shape.CubicCurve;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.scene.shape.StrokeType;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.transform.Affine;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class DiagramDrawer extends GeometricDrawer implements DynamicFlowDiagramDepictionChangeListener, SchedulerPropertyChangeListener, FlowDiagramChangeListener {
	private static final String STATICDIAGRAMFILTERS = "Static Diagram Info";
	private static final String DYNAMICDIAGRAMFILTERS = "Dynamic Diagram Info";
	private static final String BLOCK = "Block";
	private static final String SHADOW = "Shadow";
	private static final String LINK = "Link";

	private static final String BLOCKNAME = "Block Name";
	private static final String IO = "I/O";
	private static final String STACKSTATE = "Stack state";
	private static final String CPUUSAGE = "CPU usage";
	private static final String HEATLINK = "Heat link";
	private static final String MISSEDDATA = "Missed Data";
	private static final String COUNT = "Count";
	private static final String IONAME = "I/O Name";

	private boolean filterBlock;
	private boolean filterShadow;
	private boolean filterLink;
	private boolean filterIO;

	private boolean filterStackState;
	private boolean filterCPUUsage;
	private boolean filterHeatLink;
	private boolean filterMissedData;
	private boolean filterCount;
	private boolean filterIOName;
	private boolean filterBlockName;
	private Color diagramBackground = new Color(0.450980392, 0.450980392, 0.450980392, 1);
	private Color shadow = Color.GRAY.darker();// new Color(0.392156863, 0.392156863, 0.392156863, 0.68627451);
	private Color grid = new Color(0.411764706, 0.411764706, 0.411764706, 1);
	private Color component = new Color(0.588235294, 0.588235294, 0.588235294, 1);
	private Color componentHeaderLocal = new Color(0.5, 0.5, 0.5, 1);

	private Color componentHeaderIsolated = new Color(0.4, 0.4, 0.6, 1);
	private Color componentHeaderRemote = new Color(0.6, 0.6, 0.4, 1);
	private Color componentBorder = new Color(0.341176471, 0.341176471, 0.341176471, 1);
	private Color componentPropertyIO = new Color(0.39215687, 0.58431375, 0.92941177, 1);
	private Color componentStaticIO = new Color(0.784313725, 0.784313725, 0.156862745, 1);

	private Color componentDynamicIO = new Color(0.784313725, 0.156862745, 0.156862745, 1);
	private Color componentVarArgsIO = new Color(0.156862745, 0.784313725, 0.156862745, 1);
	private Color componentEntrie = new Color(0.564705882, 0.784313725, 0.784313725, 1);
	private Color link = Color.WHITE;
	private Color cpuBarBackground = new Color(0.5, 0.5, 0.5, 0.5);
	private Color cpuBarForeground = new Color(0, 0.5, 0, 0.5);
	private Color countForeground = new Color(0, 0, 0, 1);
	private Color selectionColor = Color.LIME;
	private Color ioTextColor = Color.BLACK;
	@NumberInfo(min = 0)
	private float headerSize = 10;
	@NumberInfo(min = 1)
	private int ioRoundRadius = 5;

	private int ioFlowDiagramSize = 8;
	private float shadowRadius = 15;
	@NumberInfo(min = 0)
	private int componentCornerRadius = 12;
	private float linkSelectionDistance = 8;
	private int cpuBarHeight = 10;
	private int cpuBarVerticalGap = 10;
	@NumberInfo(min = 1)
	private float gridLenght = 30;
	private HashMap<Object, Stage> visibleProperties = new HashMap<>();
	private FlowDiagram oldFd = null;
	private Group drawingElementGroup;
	private double oldScale;
	private HashMap<Block, DrawableBlock> drawingBlocks;

	private LinkedHashMap<LinkDescriptor, CubicCurve> drawingLinks;
	private HashMap<FlowDiagramInput, DrawableInput> drawingInput;
	private HashMap<FlowDiagramOutput, DrawableOutput> drawingOutput;
	private Link tempLink;
	private Rectangle selectAreaRectNode;
	private Rectangle drawingRoi;
	private CanvasInfo canvasInfo;
	private HashSet<Object> selectedElements = new HashSet<>();
	private HashSet<Object> areaSelectedElements = new HashSet<>();
	private Object selection;

	private Point2D anchorPoint;
	private HashMap<Object, Point2D> selectedElementsPosition = new HashMap<>();
	private ContextMenu contextMenu;
	private AnimationTimerConsumer animationTimerConsumer;

	private ArrayList<Node> notChangeableAtRuntimeNodes;

	private boolean isRunning;

	private boolean dragging;

	// Properties link to diagram stop
	private Alert alert;

	private AnimationTimer at;

	private ArrayList<Block> oldAlivedBlocks;

	private boolean interruptedMode = false;
	private AnimationTimer stoppingAnimationTimer; // Used to see waitingdata while stopping a diagram

	@Override
	protected void adaptViewToScenario() {
		Dimension2D dim = getDimension();
		sWid = (int) dim.getWidth();
		sHei = (int) dim.getHeight();
		super.adaptViewToScenario();
	}

	private void addAsInputMenuToEditor(Block block, String propertyNameSequence, Object operator, BeanManager beanManager, GridPane editor) {
		Function<String, String> propertyNameFullSequenceFunction = pns -> propertyNameSequence == null ? pns : propertyNameSequence + "-" + pns;
		try {
			PropertyDescriptor[] beanInfo = Introspector.getBeanInfo(operator.getClass()).getPropertyDescriptors();
			Arrays.sort(beanInfo, 0, beanInfo.length, (a, b) -> a.getName().compareTo(b.getName()));
			ArrayList<Label> propertyLabels = new ArrayList<>();
			for (Node node : editor.getChildren())
				if (GridPane.getColumnIndex(node) == 0) {
					Label propertyNameLabel = null;
					if (node instanceof Label)
						propertyNameLabel = (Label) node;
					else if (node instanceof HBox) {
						ObservableList<Node> children = ((HBox) node).getChildren();
						if (children.size() > 1) {
							Node secondChild = children.get(1);
							if (secondChild instanceof Label)
								propertyNameLabel = (Label) secondChild;
						}
					}
					if (propertyNameLabel != null) {
						int indexOfNameEnd = propertyNameLabel.getText().indexOf(":");
						if (indexOfNameEnd != -1)
							propertyLabels.add(propertyNameLabel);
					}
				}
			ArrayList<Node> _notChangeableAtRuntimeNodes = new ArrayList<>();
			if (!propertyLabels.isEmpty()) {
				propertyLabels.sort((a, b) -> BeanManager.getPropertyNameFromLabelName(a.getText()).compareTo(BeanManager.getPropertyNameFromLabelName(b.getText())));
				int propertyLabelsIndex = 0;
				for (int j = 0; j < beanInfo.length; j++) {
					if (BeanManager.getPropertyNameFromLabelName(propertyLabels.get(propertyLabelsIndex).getText()).equals(beanInfo[j].getName())) {
						if (BeanManager.getAnno(NotChangeableAtRuntime.class, operator.getClass(), beanInfo[j]) != null) {
							Integer ci = GridPane.getRowIndex(propertyLabels.get(propertyLabelsIndex));
							for (Node node : editor.getChildren())
								if (GridPane.getColumnIndex(node) == 1 && GridPane.getRowIndex(node) == ci)
									_notChangeableAtRuntimeNodes.add(node);
						}
						if (DiagramScheduler.isCloneable(BeanManager.toWrapper(beanInfo[j].getPropertyType())) && beanInfo[j].getWriteMethod() != null) {
							String propertyName = beanInfo[j].getName();
							boolean forbidden = false;
							List<BlockInput> inputs = block.getInputs();
							for (int i = block.getNbPropertyInput(); i < inputs.size(); i++) // OK pas grave, juste pour menu
								if (inputs.get(i).getName().equals(propertyName)) {
									forbidden = true;
									break;
								}
							if (!forbidden) {
								String propertyNameFullSequence = propertyNameFullSequenceFunction.apply(propertyName);
								CheckMenuItem menu = new CheckMenuItem(propertyNameFullSequence + " as input");
								Object blockOperator = block.getOperator();
								if (blockOperator instanceof EvolvedOperator) {
									menu.setSelected(((EvolvedOperator) blockOperator).isPropertyAsInput(propertyNameFullSequence));
									menu.setOnAction(e1 -> ((EvolvedOperator) blockOperator).setPropertyAsInput(propertyNameFullSequence, menu.isSelected()));
								} else {
									menu.setSelected(block.isPropertyAsInput(propertyNameFullSequence));
									menu.setOnAction(e1 -> block.setPropertyAsInput(propertyNameFullSequence, menu.isSelected())); // Plus d'event de
								}
								propertyLabels.get(propertyLabelsIndex).setContextMenu(new ContextMenu(menu));
							}
						}
						propertyLabelsIndex++;
						if (propertyLabelsIndex == propertyLabels.size())
							break;
					}
				}
			}
			synchronized (DiagramDrawer.this) {
				if (notChangeableAtRuntimeNodes == null)
					notChangeableAtRuntimeNodes = _notChangeableAtRuntimeNodes;
				else
					notChangeableAtRuntimeNodes.addAll(_notChangeableAtRuntimeNodes);
				_notChangeableAtRuntimeNodes.forEach(node -> node.setDisable(isRunning)); // ne pas reenable des éléments disable par fire
			}
		} catch (IntrospectionException e2) {
			e2.printStackTrace();
		}
		var listenerHandler = new Object() {
			SubBeanExpansionListener sbel;
		};
		listenerHandler.sbel = new SubBeanExpansionListener() {

			@Override
			public void subBeanExpanded(boolean expanded, String subBeanName, BeanManager subBeanManager, GridPane subBeanGridpane) {
				if (subBeanManager != null) {
					if (expanded)
						addAsInputMenuToEditor(block, propertyNameFullSequenceFunction.apply(subBeanName), subBeanManager.getBean(), subBeanManager, subBeanGridpane);
					else {
						subBeanManager.forEachExpendedSubView((name, sbm, sbgp) -> subBeanManager.removeSubBeanExpandedListener(listenerHandler.sbel));
						subBeanManager.removeSubBeanExpandedListener(listenerHandler.sbel);
					}
				}
			}
		};
		beanManager.forEachExpendedSubView(
				(subBeanName, subBeanManager, subBeanGridpane) -> listenerHandler.sbel.subBeanExpanded(true, propertyNameFullSequenceFunction.apply(subBeanName), subBeanManager, subBeanGridpane));
		beanManager.addSubBeanExpandedListener(listenerHandler.sbel);
	}

	@Override
	protected double adjustScale(double scale) {
		return scale;
	}

	public void clearSelectedElements() {
		for (Object element : selectedElements)
			if (element instanceof Block)
				drawingBlocks.get(element).resetCoreStroke();
			else if (element instanceof DrawableIOComponent<?>)
				((DrawableIOComponent<?>) element).resetCoreStroke();
			else if (element instanceof BlockIO) {
				BlockIO blockIO = (BlockIO) element;
				drawingBlocks.get(blockIO.getBlock()).resetStroke((IO) blockIO);
			} else if (element instanceof FlowDiagramInput) {
				FlowDiagramInput fdInput = (FlowDiagramInput) element;
				drawingInput.get(fdInput).resetStroke(fdInput);
			} else if (element instanceof FlowDiagramOutput) {
				FlowDiagramOutput fdOutput = (FlowDiagramOutput) element;
				drawingOutput.get(fdOutput).resetStroke(fdOutput);
			} else if (canvasInfo == null)
				drawingLinks.get(element).setStroke(Color.WHITE);
			else
				canvasInfo.paint();
		selectedElements.clear();
	}

	private void createDiagramContextMenu() {
		MenuItem miaop = new MenuItem("Add Operator");
		miaop.setOnAction(e -> showTool(Operators.class));
		MenuItem miai = new MenuItem("Add Input");
		miai.setOnAction(e1 -> {
			Point2D localPos = DiagramDrawer.this.screenToLocal(new Point2D(contextMenu.getAnchorX(), contextMenu.getAnchorY()));
			Point2D pos = toGeometricCoordinate(localPos.getX(), localPos.getY());
			FlowDiagram fd = (FlowDiagram) getDrawableElement();
			List<FlowDiagramOutput> outputs = fd.getOutputs();
			String baseName = "in-";
			String name;
			boolean isValidName;
			int id = 0;
			do {
				isValidName = true;
				name = baseName + id++;
				for (FlowDiagramOutput output : outputs)
					if (output.getName().equals(name)) {
						isValidName = false;
						break;
					}
			} while (!isValidName || id == Integer.MAX_VALUE);
			fd.addOutput(new FlowDiagramOutput(null, name, pos));
		});
		MenuItem miao = new MenuItem("Add Output");
		miao.setOnAction(e1 -> {
			Point2D localPos = DiagramDrawer.this.screenToLocal(new Point2D(contextMenu.getAnchorX(), contextMenu.getAnchorY()));
			Point2D pos = toGeometricCoordinate(localPos.getX(), localPos.getY());
			FlowDiagram fd = (FlowDiagram) getDrawableElement();
			List<FlowDiagramInput> inputs = fd.getInputs();
			String baseName = "out-";
			String name;
			boolean isValidName;
			int id = 0;
			do {
				isValidName = true;
				name = baseName + id++;
				for (FlowDiagramInput input : inputs)
					if (input.getName().equals(name)) {
						isValidName = false;
						break;
					}
			} while (!isValidName || id == Integer.MAX_VALUE);
			fd.addInput(new FlowDiagramInput(fd, null, name, pos));
		});
		contextMenu = new ContextMenu(miaop, miai, miao);
	}

	private Node createDrawingborder() {
		Rectangle drawingBorder = new Rectangle(0.5, 0.5, sWid * getScale() + 0.5, sHei * getScale() + 0.5);
		drawingBorder.setId(BORDER);
		drawingBorder.setStroke(grid);
		drawingBorder.setFill(null);
		return drawingBorder;
	}

	private CubicCurve createDrawingLink(LinkDescriptor linkDesc) {
		IoDescriptor inputDesc = linkDesc.input;
		IoDescriptor outputDesc = linkDesc.output;
		CubicCurve cubic = new CubicCurve();
		if (inputDesc.io instanceof BlockIO)
			drawingBlocks.get(linkDesc.inputBlock).setPosition(inputDesc.io, cubic);
		else if (inputDesc.io instanceof FlowDiagramInput) {
			DrawableInput dinput = drawingInput.get(inputDesc.io);
			dinput.setPosition(inputDesc.io, cubic);
			dinput.updateIOInfo(inputDesc.name, inputDesc.dataType);
		}
		if (outputDesc.io instanceof BlockIO)
			drawingBlocks.get(linkDesc.outputBlock).setPosition(outputDesc.io, cubic);
		else if (outputDesc.io instanceof FlowDiagramOutput) {
			DrawableOutput doutput = drawingOutput.get(outputDesc.io);
			doutput.setPosition(outputDesc.io, cubic);
			doutput.updateIOInfo(outputDesc.name, outputDesc.dataType); // output input input avant
		}
		InvalidationListener list = e -> updateCurveControlPoint(cubic);
		cubic.startXProperty().addListener(list);
		cubic.startYProperty().addListener(list);
		cubic.endXProperty().addListener(list);
		cubic.endYProperty().addListener(list);
		updateCurveControlPoint(cubic);
		cubic.setStroke(link);
		cubic.setFill(null);
		drawingLinks.put(linkDesc, cubic);
		cubic.setVisible(filterLink);
		return cubic;
	}

	private Group creatGrid() {
		Group g = new Group();
		g.setId(GRID);
		ObservableList<Node> gridChildren = g.getChildren();
		double scale = getScale();
		int ls = (int) Math.round(sWid * getScale());
		int hs = (int) Math.round(sHei * scale);
		int ls1 = ls + 1;
		int hs1 = hs + 1;
		for (float i = 0; i < ls1; i += scale * gridLenght) {
			Line line = new Line((int) i + 0.5, 0 + 0.5, (int) i + 0.5, hs + 0.5);
			line.setStroke(grid);
			line.setStrokeWidth(1);
			gridChildren.add(line);
		}
		for (float i = 0; i < hs1; i += scale * gridLenght) {
			Line line = new Line(0 + 0.5, (int) i + 0.5, ls + 0.5, (int) i + 0.5);
			line.setStroke(grid);
			line.setStrokeWidth(1);
			gridChildren.add(line);
		}
		return g;
	}

	@Override
	public void death() {
		FlowDiagram de = (FlowDiagram) getDrawableElement();
		if (de != null)
			de.removeFlowDiagramChangeListener(this);
		if (scheduler != null)
			scheduler.removePropertyChangeListener(this);
		for (Stage stage : visibleProperties.values())
			stage.close();
		visibleProperties.clear();
		super.death();
	}

	private void deleteElementFromSelectionAndPropertiesStage(Object element) {
		selectedElements.remove(element);
		Stage propertiesStage = visibleProperties.remove(element);
		if (propertiesStage != null)
			propertiesStage.close();
	}

	private static double distance(CubicCurve curve, Point2D point) {
		float flatness = 0.01f;
		FlatteningPathIterator pit = new FlatteningPathIterator(new CubicIterator(curve), flatness);
		float[] coords = new float[2];
		float[] oldCoords = null;
		double minDist = Double.MAX_VALUE;
		while (!pit.isDone()) {
			pit.currentSegment(coords);
			if (oldCoords == null)
				oldCoords = new float[2];
			else {
				double dist = squaredDistancePointToSegment(oldCoords, coords, point);
				if (dist < minDist)
					minDist = dist;
			}
			oldCoords[0] = coords[0];
			oldCoords[1] = coords[1];
			pit.next();
		}
		return minDist;
	}

	@Override
	public void dynamicFlowDiagramChanged() {
		// if (canvasInfo != null)
		// canvasInfo.needToRepaint = true;
	}

	// Attention!!! pas de FxUtils.runLaterIfNeeded, cela peut changer l'ordre des tache à exécuter...
	@Override
	public void flowDiagramChanged(Object element, String property, ModificationType modificationType) {
		// System.out.println("Element: " + element + " ModificationType: " + modificationType);
		if (element instanceof Link) { // removeLink OK createDrawingLink input output
			LinkDescriptor linkDescriptor = new LinkDescriptor((Link) element);
			Platform.runLater(() -> {
				int index = (filterGrid ? 1 : 0) + (filterBorder ? 1 : 0);
				if (modificationType == ModificationType.NEW)
					drawingElementGroup.getChildren().add(index, createDrawingLink(linkDescriptor));
				else if (modificationType == ModificationType.DELETE)
					removeLink(linkDescriptor);
				else if (modificationType == ModificationType.CHANGE) {
					LinkDescriptor linkToRemove = null;
					for (LinkDescriptor _link : drawingLinks.keySet())
						if (_link.input.io == linkDescriptor.input.io) {
							linkToRemove = _link;
							break;
						}
					if (linkToRemove != null)
						removeLink(linkToRemove);
					drawingElementGroup.getChildren().add(index, createDrawingLink(linkDescriptor));
				}
			});
		}
		if (element instanceof Block) {
			if (modificationType == ModificationType.CHANGE && property == "name") {
				String name = ((Block) element).getName();
				Platform.runLater(() -> drawingBlocks.get(element).updateBlockName(name));
			} else {
				IoDescriptor[] ioDescriptors = modificationType == ModificationType.DELETE ? null : getIODescriptors((Block) element);
				Platform.runLater(() -> {
					Block block = (Block) element;
					if (modificationType == ModificationType.NEW)
						drawingElementGroup.getChildren().add(new DrawableBlock(block, getScale(), ioDescriptors));
					else if (modificationType == ModificationType.DELETE) {
						drawingElementGroup.getChildren().remove(drawingBlocks.get(block));
						drawingBlocks.remove(block).delete();
						deleteElementFromSelectionAndPropertiesStage(block);
					} else if (modificationType == ModificationType.CHANGE)
						drawingBlocks.get(block).updateIOStruct(ioDescriptors);
					if (canvasInfo != null)
						canvasInfo.repaint();
				});
			}
		} else if (element instanceof FlowDiagramInput) {
			IoDescriptor[] ioDescriptors = modificationType == ModificationType.NEW ? getIODescriptors((FlowDiagramInput) element) : null;
			Platform.runLater(() -> {
				if (modificationType == ModificationType.NEW)
					drawingElementGroup.getChildren().add(new DrawableInput((FlowDiagramInput) element, getScale(), ioDescriptors));
				else if (modificationType == ModificationType.DELETE) {
					drawingElementGroup.getChildren().remove(drawingInput.get(element));
					drawingInput.remove(element);
					deleteElementFromSelectionAndPropertiesStage(element);
				} else if (modificationType == ModificationType.CHANGE)
					((DrawableInput) drawingElementGroup.getChildren().get(drawingElementGroup.getChildren().indexOf(drawingOutput.get(element))))
							.nameChange((org.scenarium.filemanager.scenario.dataflowdiagram.IO) element);
				if (canvasInfo != null)
					canvasInfo.repaint();
			});
		} else if (element instanceof FlowDiagramOutput) {
			IoDescriptor[] ioDescriptors = modificationType == ModificationType.NEW ? getIODescriptors((FlowDiagramOutput) element) : null;
			Platform.runLater(() -> {
				if (modificationType == ModificationType.NEW)
					drawingElementGroup.getChildren().add(new DrawableOutput((FlowDiagramOutput) element, getScale(), ioDescriptors));
				else if (modificationType == ModificationType.DELETE) {
					drawingElementGroup.getChildren().remove(drawingOutput.get(element));
					drawingOutput.remove(element);
					deleteElementFromSelectionAndPropertiesStage(element);
				} else if (modificationType == ModificationType.CHANGE)
					((DrawableOutput) drawingElementGroup.getChildren().get(drawingElementGroup.getChildren().indexOf(drawingOutput.get(element))))
							.nameChange((org.scenarium.filemanager.scenario.dataflowdiagram.IO) element);
				if (canvasInfo != null)
					canvasInfo.repaint();
			});
		}
	}

	private void forceRepaint() {
		if (getDrawableElement() != null) {
			oldFd = null;
			repaint(false);
		}
	}

	public Color getComponent() {
		return component;
	}

	public Color getComponentBorder() {
		return componentBorder;
	}

	public int getComponentCornerRadius() {
		return componentCornerRadius;
	}

	public Color getComponentDynamicIO() {
		return componentDynamicIO;
	}

	public Color getComponentEntrie() {
		return componentEntrie;
	}

	public Color getComponentHeaderIsolated() {
		return componentHeaderIsolated;
	}

	public Color getComponentHeaderLocal() {
		return componentHeaderLocal;
	}

	public Color getComponentHeaderRemote() {
		return componentHeaderRemote;
	}

	public Color getComponentPropertyIO() {
		return componentPropertyIO;
	}

	public Color getComponentStaticIO() {
		return componentStaticIO;
	}

	public Color getComponentVarArgsIO() {
		return componentVarArgsIO;
	}

	public Color getCountForeground() {
		return countForeground;
	}

	public Color getCpuBarBackground() {
		return cpuBarBackground;
	}

	public Color getCpuBarForeground() {
		return cpuBarForeground;
	}

	public int getCpuBarHeight() {
		return cpuBarHeight;
	}

	public int getCpuBarVerticalGap() {
		return cpuBarVerticalGap;
	}

	public Color getDiagramBackground() {
		return diagramBackground;
	}

	@Override
	public Dimension2D getDimension() {
		double minX = 800;
		double minY = 600;
		FlowDiagram fd = (FlowDiagram) getDrawableElement();
		for (Block block : fd.getBlocks()) {
			Rectangle2D rect = block.getRectangle();
			if (rect.getMaxX() + 10 > minX)
				minX = rect.getMaxX() + 10;
			if (rect.getMaxY() + 10 > minY)
				minY = rect.getMaxY() + 10;
		}
		for (FlowDiagramInput fdInput : fd.getInputs()) {
			Point2D inputPos = fdInput.getPosition();
			if (inputPos.getX() + 20 + 1 + ioRoundRadius > minX)
				minX = inputPos.getX() + 20 + 1 + ioRoundRadius;
			if (inputPos.getY() + 10 > minY)
				minY = inputPos.getY() + 10;
		}
		for (FlowDiagramOutput fdOutput : fd.getOutputs()) {
			Point2D inputPos = fdOutput.getPosition();
			if (inputPos.getX() + 20 + 1 + ioRoundRadius > minX)
				minX = inputPos.getX() + 20 + 1 + ioRoundRadius;
			if (inputPos.getY() + 10 > minY)
				minY = inputPos.getY() + 10;
		}
		return new Dimension2D(minX, minY);
	}

	public Color getGrid() {
		return grid;
	}

	public float getGridLenght() {
		return gridLenght;
	}

	public float getHeaderSize() {
		return headerSize;
	}

	@Override
	public float getIntelligentZoomThreshold() {
		return 10;
	}

	private static IoDescriptor[] getIODescriptors(Block block) {
		ArrayList<IoDescriptor> ioDescriptors = new ArrayList<>();
		int index = 0;
		List<BlockInput> inputs = block.getInputs();
		for (int j = 0; j < block.getNbPropertyInput(); j++)
			ioDescriptors.add(new IoDescriptor(inputs.get(index), index++, IoDescriptor.PROPERTY));
		for (int j = 0; j < block.getNbStaticInput(); j++)
			ioDescriptors.add(new IoDescriptor(inputs.get(index), index++, IoDescriptor.STATIC));
		for (int j = 0; j < block.getNbDynamicInput(); j++)
			ioDescriptors.add(new IoDescriptor(inputs.get(index), index++, IoDescriptor.DYNAMIC));
		for (int j = 0; j < block.getNbVarArgsInput(); j++)
			ioDescriptors.add(new IoDescriptor(inputs.get(index), index++, IoDescriptor.VARARGS));
		if (inputs.size() != index)
			ioDescriptors.add(new IoDescriptor(inputs.get(index), index++, IoDescriptor.VARARGS));
		List<BlockOutput> outputs = block.getOutputs();
		index = 0;
		for (int k = 0; k < block.getNbStaticOutput(); k++)
			ioDescriptors.add(new IoDescriptor(outputs.get(index), index++, IoDescriptor.STATIC));
		int end = block.getNbOutput() - index;
		for (int k = 0; k < end; k++)
			ioDescriptors.add(new IoDescriptor(outputs.get(index), index++, IoDescriptor.DYNAMIC));
		return ioDescriptors.toArray(new IoDescriptor[ioDescriptors.size()]);
	}

	private static IoDescriptor[] getIODescriptors(IO io) {
		return new IoDescriptor[] { new IoDescriptor(io, 0, IoDescriptor.STATIC) };
	}

	public int getIoFlowDiagramSize() {
		return ioFlowDiagramSize;
	}

	public int getIoRoundRadius() {
		return ioRoundRadius;
	}

	private static String getIOToolTipInfo(String name, Class<?> _class, String errorMessage) {
		return "Name: " + name + "\nType: " + (_class == null ? "not defined" : _class.getSimpleName()) + "\nPath: " + (_class == null ? String.valueOf((Object) null) : _class)
				+ (errorMessage == null ? "" : "\nerror: " + errorMessage);
	}

	public Color getLink() {
		return link;
	}

	public float getLinkSelectionDistance() {
		return linkSelectionDistance;
	}

	public Color getShadow() {
		return shadow;
	}

	public float getShadowRadius() {
		return shadowRadius;
	}

	public Color getSelectionColor() {
		return selectionColor;
	}

	public void setSelectionColor(Color selectionColor) {
		this.selectionColor = selectionColor;
		forceRepaint();
	}

	public Color getIoTextColor() {
		return ioTextColor;
	}

	public void setIoTextColor(Color ioTextColor) {
		this.ioTextColor = ioTextColor;
		forceRepaint();
	}

	@Override
	public float getZoomScale() {
		return 2;
	}

	@Override
	public void initialize(ScenariumContainer container, Scheduler scheduler, Object dataElement, boolean autoFitIfResize) {
		setOnKeyPressed(e -> {
			KeyCode code = e.getCode();
			switch (code) {
			case DELETE:
				if (selectedElements.size() != 0) {
					ArrayList<Object> removeList = new ArrayList<>(selectedElements);
					for (int i = 0; i < removeList.size(); i++) {
						Object ei = removeList.get(i);
						if (ei instanceof DrawableDiagramIO)
							removeList.set(i, ((DrawableDiagramIO<?>) ei).io);
						else if (ei instanceof LinkDescriptor)
							removeList.set(i, ((LinkDescriptor) ei).link);
					}
					for (Object element : ((FlowDiagram) getDrawableElement()).removeElements(removeList, isRunning(), e.isShiftDown())) {
						if (element instanceof DrawableDiagramIO)
							selectedElements.removeIf(ele -> ele instanceof DrawableDiagramIO && ((DrawableDiagramIO<?>) ele).io == element);
						else if (element instanceof Link)
							selectedElements.removeIf(ele -> ele instanceof LinkDescriptor && ((LinkDescriptor) ele).link == element);
						else
							selectedElements.remove(element);
					}
					clearSelectedElements();
				}
				break;
			case UP:
			case DOWN:
			case RIGHT:
			case LEFT:
				if (!e.isControlDown())
					for (Object selectedElement : selectedElements)
						if (selectedElement instanceof LocalisedObject) {
							LocalisedObject element = (LocalisedObject) selectedElement;
							double invScale = 1 / getScale();
							Point2D position = element.getPosition();
							double newX = position.getX() + (code == KeyCode.RIGHT ? invScale : code == KeyCode.LEFT ? -invScale : 0);
							double newY = position.getY() + (code == KeyCode.DOWN ? invScale : code == KeyCode.UP ? -invScale : 0);
							if (!iscollision(element, newX, newY)) {
								element.setPosition(newX, newY);
								drawingBlocks.get(selectedElement).updatePosition();
							}
						}
				break;
			case D:
			case I:
			case R:
				if (selectedElements.size() != 0)
					for (Object selectedElement : selectedElements)
						if (selectedElement instanceof Block) {
							Block block = (Block) selectedElement;
							if (code == KeyCode.D)
								block.setEnable(!block.isEnable());
							else {
								ProcessMode mode = code == KeyCode.I ? ProcessMode.ISOLATED : ProcessMode.REMOTE;
								block.setProcessMode(block.getProcessMode() == mode ? ProcessMode.LOCAL : mode);
							}
							e.consume();
						}
				break;
			case P:
				if (selectedElements.size() == 1) {
					Object selectedElement = selectedElements.iterator().next();
					if (selectedElement instanceof Block) {
						showBlockProperties((Block) selectedElement);
						e.consume();
					}
				}
			default:
				break;
			}
		});
		addEventHandler(DragEvent.DRAG_OVER, e -> {
			if (e.isConsumed())
				return;
			Dragboard db = e.getDragboard();
			if (db.hasContent(Operators.operatorFormat) || db.hasContent(Operators.operatorClassFormat)) {
				Point2D pos = toGeometricCoordinate(e.getX(), e.getY());
				if (!iscollision(null, Block.getBlockMinimumBounds(pos, true), pos.getX(), pos.getY()))
					e.acceptTransferModes(TransferMode.MOVE);
				e.consume();
			}
		});
		addEventHandler(DragEvent.DRAG_DROPPED, e -> {
			// System.out.println("dragg ok");
			Dragboard db = e.getDragboard();
			if (db.hasContent(Operators.operatorFormat) || db.hasContent(Operators.operatorClassFormat)) {
				Point2D pos = toGeometricCoordinate(e.getX(), e.getY());
				if (!iscollision(null, Block.getBlockMinimumBounds(pos, true), pos.getX(), pos.getY())) {
					e.acceptTransferModes(TransferMode.MOVE);
					FlowDiagram fd = (FlowDiagram) getDrawableElement();
					BeanDesc<?> beanDesc = null;
					if (db.hasContent(Operators.operatorClassFormat))
						try {
							beanDesc = BeanEditor.registerCreatedBeanAndCreateSubBean(
									BeanManager.getClassFromDescriptor((String) e.getDragboard().getContent(Operators.operatorClassFormat)).getConstructor().newInstance(), BeanManager.DEFAULTDIR);
						} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException
								| ClassNotFoundException e1) {
							e1.printStackTrace();
						}
					else
						try {
							beanDesc = BeanEditor.getBeanDescWithAlias((String) e.getDragboard().getContent(Operators.operatorFormat));
						} catch (ClassNotFoundException e2) {
							e2.printStackTrace();
						}
					if (beanDesc == null)
						return;
					int index;
					if ((index = fd.contains(beanDesc.bean)) != -1) {
						showMessage("The block: " + beanDesc + " is already in the diagram", true);
						clearSelectedElements();
						selectedElements.add(drawingBlocks.get(fd.getBlocks().get(index)));
					} else
						try {
							fd.addBlock(new Block(beanDesc.bean, pos, true));
						} catch (IllegalInputArgument e1) {
							showMessage("Cannot place the Block: " + e1.getMessage(), true);
						}
					clearSelectedElements();
				}
				e.setDropCompleted(true);
				e.consume();
			}
		});
		addEventFilter(MouseEvent.MOUSE_DRAGGED, e -> {
			dragging = true;
			if (contextMenu != null) {
				contextMenu.hide();
				contextMenu = null;
			}
			if (e.isConsumed())
				return;
			for (Object selectedElement : selectedElements) {
				Point2D selectedElementPosition = selectedElementsPosition.get(selectedElement);
				if (selectedElement != null && selectedElement instanceof LocalisedObject && anchorPoint != null) {
					LocalisedObject element = (LocalisedObject) selectedElement;
					double newX = (e.getX() - getxTranslate()) / getScale() - anchorPoint.getX() + selectedElementPosition.getX();
					double newY = (e.getY() - getyTranslate()) / getScale() - anchorPoint.getY() + selectedElementPosition.getY();
					if (!iscollision(element, newX, newY))
						updatePosition(newX, newY, element);
					else {
						Point2D oldPos = element.getPosition();
						double oldX = oldPos.getX();
						double oldy = oldPos.getY();
						if (!iscollision(element, oldX, newY))
							updatePosition(oldX, newY, element);
						else if (!iscollision(element, newX, oldy))
							updatePosition(newX, oldy, element);
					}
				}
			}
		});
		addEventHandler(MouseEvent.MOUSE_RELEASED, e -> {
			selectedElementsPosition.clear();
			anchorPoint = null;
			if (selectAreaRect != null) {
				selectAreaRect = null;
				repaint(false);
			}
			areaSelectedElements.clear();
		});
		addEventHandler(MouseEvent.MOUSE_PRESSED, e -> {
			dragging = false;
			if (contextMenu != null)
				contextMenu.hide();
			if (selection != null) {
				if (!selectedElements.contains(selection)) {
					if (!e.isControlDown())
						clearSelectedElements();
					selectedElements.add(selection);
				}
				selectedElementsPosition.clear();
				anchorPoint = toGeometricCoordinate(e.getX(), e.getY());// new Point2D((e.getX() - getxTranslate()) / getScale(), (e.getY() - getyTranslate()) / getScale());
				for (Object obj : selectedElements)
					if (obj instanceof LocalisedObject)
						selectedElementsPosition.put(obj, ((LocalisedObject) obj).getPosition());
				selection = null;
				e.consume();
				return;
			}
			if (e.isConsumed())
				return;
			if (e.getButton() == MouseButton.PRIMARY) {
				if (!e.isControlDown()) // Que si MouseButton.PRIMARY, sinon perte des blocks et autres quand déplace
					clearSelectedElements();
				Point2D mousePoint = new Point2D(e.getX() - getxTranslate(), e.getY() - getyTranslate());
				LinkDescriptor closestLink = null;
				double minDist = Double.MAX_VALUE;
				for (LinkDescriptor link : drawingLinks.keySet()) {
					CubicCurve _curve = drawingLinks.get(link);
					Bounds cb = _curve.getBoundsInLocal();
					float lsd = linkSelectionDistance;
					if (new Rectangle2D(cb.getMinX() - lsd, cb.getMinY() - lsd, cb.getWidth() + lsd * 2, cb.getHeight() + lsd * 2).contains(mousePoint)) {
						double dist = distance(_curve, mousePoint);
						if (dist < minDist) {
							minDist = dist;
							closestLink = link;
						}
					}
				}
				if (minDist < linkSelectionDistance * linkSelectionDistance) {
					selectedElements.add(closestLink);
					drawingLinks.get(closestLink).setStroke(selectionColor);
					e.consume();
				}
			}
		});
		setOnContextMenuRequested(e -> {
			if (!dragging && tempLink == null)
				showDiagramContextMenu(e);
			e.consume();
		});
		super.initialize(container, scheduler, dataElement, autoFitIfResize);
		if (scheduler != null) {
			scheduler.addPropertyChangeListener(this);
			isRunning = scheduler.isRunning();
		} else
			isRunning = false;
		widthProperty().addListener(e -> setClip(new Rectangle(getWidth(), getHeight())));
		heightProperty().addListener(e -> setClip(new Rectangle(getWidth(), getHeight())));
		setBackground(new Background(new BackgroundFill(diagramBackground, null, null)));
	}

	@Override
	protected boolean isAreaSelectionPoint(int x, int y) {
		return true;
	}

	private static boolean isAssignable(Input input, Output output) {
		if (input instanceof FlowDiagramInput && output instanceof FlowDiagramOutput)
			return false;
		Class<?> inputType = input instanceof FlowDiagramInput ? null : input.getType();
		Class<?> outputType = output.getType();
		if (inputType != null) {
			if (outputType != null && !BeanManager.isAssignable(inputType, outputType))
				return false;
			if (input instanceof BlockInput) {
				Block inputBlock = ((BlockInput) input).getBlock();
				if (((BlockInput) input).isVarArgs && inputBlock.getOperator() instanceof EvolvedVarArgsOperator) {
					// Accès à des données
					List<BlockInput> inputs = inputBlock.getInputs();
					int nbPropertyInput = inputBlock.getNbPropertyInput();
					Class<?>[] inputsType = new Class[inputs.size() - nbPropertyInput];
					for (int i = nbPropertyInput; i < inputs.size(); i++) {
						Link link = inputs.get(i).getLink();
						inputsType[i - nbPropertyInput] = link == null ? null : link.getOutput().getType();
					}
					return ((EvolvedVarArgsOperator) inputBlock.getOperator()).isValidInput(inputsType, outputType);
				}
			}
			return true;
		}
		return true;
	}

	private boolean iscollision(LocalisedObject selectedElement, double newX, double newY) {
		Rectangle2D rect;
		if (selectedElement instanceof Block)
			rect = ((Block) selectedElement).getRectangle();
		else {
			DrawableDiagramIO<?> fdio = (DrawableDiagramIO<?>) selectedElement;
			rect = new Rectangle2D(fdio.getPosition().getX() - ioFlowDiagramSize, fdio.getPosition().getY() - ioFlowDiagramSize, ioFlowDiagramSize * 2, ioFlowDiagramSize * 2);
		}
		rect = new Rectangle2D(newX - rect.getWidth() / 2.0, newY - rect.getHeight() / 2.0, rect.getWidth(), rect.getHeight());
		return iscollision(selectedElement, rect, newX, newY);
	}

	private boolean iscollision(LocalisedObject selectedElement, Rectangle2D rect, double newX, double newY) {
		double[] x = new double[4];
		double[] y = new double[4];
		x[0] = rect.getMinX();
		x[1] = x[0];
		x[2] = x[0] + rect.getWidth();
		x[3] = x[2];
		y[0] = rect.getMinY();
		y[1] = y[0] + rect.getHeight();
		y[2] = y[1];
		y[3] = y[0];
		ArrayList<Rectangle2D> collisionShape = new ArrayList<>();
		FlowDiagram fd = (FlowDiagram) getDrawableElement();
		for (Block block : fd.getBlocks())
			if (block != selectedElement)
				collisionShape.add(block.getRectangle());
		for (FlowDiagramInput fdi : fd.getInputs()) {
			if (selectedElement instanceof DrawableDiagramIO ? ((DrawableDiagramIO<?>) selectedElement).io == fdi : selectedElement == fdi)
				continue;
			collisionShape.add(new Rectangle2D(fdi.getPosition().getX() - ioFlowDiagramSize, fdi.getPosition().getY() - ioFlowDiagramSize, ioFlowDiagramSize * 2, ioFlowDiagramSize * 2));
		}
		for (FlowDiagramOutput fdo : fd.getOutputs()) {
			if (selectedElement instanceof DrawableDiagramIO ? ((DrawableDiagramIO<?>) selectedElement).io == fdo : selectedElement == fdo)
				continue;
			collisionShape.add(new Rectangle2D(fdo.getPosition().getX() - ioFlowDiagramSize, fdo.getPosition().getY() - ioFlowDiagramSize, ioFlowDiagramSize * 2, ioFlowDiagramSize * 2));
		}
		for (Rectangle2D rectOther : collisionShape) {
			for (int j = 0; j < x.length; j++)
				if (rectOther.getMinX() <= x[j] && rectOther.getMinY() <= y[j] && rectOther.getMaxX() >= x[j] && rectOther.getMaxY() >= y[j])
					return true;
			double[] xo = new double[4];
			double[] yo = new double[4];
			xo[0] = rectOther.getMinX();
			xo[1] = xo[0];
			xo[2] = xo[0] + rectOther.getWidth();
			xo[3] = xo[2];
			yo[0] = rectOther.getMinY();
			yo[1] = yo[0] + rectOther.getHeight();
			yo[2] = yo[1];
			yo[3] = yo[0];
			for (int j = 0; j < xo.length; j++)
				if (rect.getMinX() <= xo[j] && rect.getMinY() <= yo[j] && rect.getMaxX() >= xo[j] && rect.getMaxY() >= yo[j])
					return true;
		}
		return false;
	}

	@Override
	public void paint(Object dataElement) {
		FlowDiagram fd = (FlowDiagram) dataElement;
		double scale = getScale();
		if (scale == 0)
			return;
		if (oldFd == null || oldFd != fd) {
			clearSelectedElements();
			if (drawingElementGroup != null)
				for (Node node : drawingElementGroup.getChildren())
					if (node instanceof DrawableBlock)
						((DrawableBlock) node).delete();
			getChildren().remove(drawingElementGroup);
			drawingBlocks = new HashMap<>();
			ArrayList<Node> drawingElements = new ArrayList<>();
			if (filterGrid)
				drawingElements.add(creatGrid());
			if (filterBorder)
				drawingElements.add(createDrawingborder());
			int index = drawingElements.size();
			for (Block block : fd.getBlocks())
				drawingElements.add(new DrawableBlock(block, getScale(), getIODescriptors(block)));
			drawingInput = new HashMap<>();
			fd.getInputs().forEach(fdi -> drawingElements.add(index, new DrawableInput(fdi, getScale(), getIODescriptors(fdi))));
			drawingOutput = new HashMap<>();
			fd.getOutputs().forEach(fdo -> drawingElements.add(index, new DrawableOutput(fdo, getScale(), getIODescriptors(fdo))));
			drawingLinks = new LinkedHashMap<>();
			for (Link link : fd.getAllLinks())
				drawingElements.add(index, createDrawingLink(new LinkDescriptor(link)));
			drawingElementGroup = new Group(drawingElements);
			getChildren().add(drawingElementGroup);
		}
		drawingElementGroup.setTranslateX((int) getxTranslate());
		drawingElementGroup.setTranslateY((int) getyTranslate());
		if (getScale() != oldScale)
			if (drawingElementGroup != null) {
				ArrayList<Runnable> taskAfterScaleChanged = null;
				for (Node node : drawingElementGroup.getChildren())
					if (node instanceof DrawableIOComponent<?>) {
						Runnable runTaskAfterScaleChanged = ((DrawableIOComponent<?>) node).setScale(getScale());
						if (runTaskAfterScaleChanged != null) {
							if (taskAfterScaleChanged == null)
								taskAfterScaleChanged = new ArrayList<>();
							taskAfterScaleChanged.add(runTaskAfterScaleChanged);
						}
					} else if (node.getId() == GRID)
						setGridScale((Group) node, scale);
					else if (node.getId() == BORDER)
						setBorderScale((Rectangle) node, scale);
				if (taskAfterScaleChanged != null)
					taskAfterScaleChanged.forEach(Runnable::run);
			}
		if (filterROI) {
			if (drawingRoi == null) {
				drawingRoi = new Rectangle();
				drawingRoi.setFill(null);
				drawingRoi.setStroke(Color.RED);
				drawingElementGroup.getChildren().add(drawingRoi);
			}
			drawingRoi.setX(roi[0] * scale + 0.5);
			drawingRoi.setY(roi[1] * scale + 0.5);
			drawingRoi.setWidth(roi[2] * scale + 0.5);
			drawingRoi.setHeight(roi[3] * scale + 0.5);
		} else if (drawingRoi != null) {
			drawingElementGroup.getChildren().remove(drawingRoi);
			drawingRoi = null;
		}
		if (selectAreaRect == null ^ selectAreaRectNode == null) { // ou exclusif changement dans la selection
			if (selectAreaRect == null) {
				drawingElementGroup.getChildren().remove(selectAreaRectNode);
				selectAreaRectNode = null;
			} else if (selectAreaRectNode == null) {
				selectAreaRectNode = new Rectangle(selectAreaRect[0].x * scale + 0.5, selectAreaRect[0].y * scale + 0.5, selectAreaRect[1].x * scale + 0.5, selectAreaRect[1].y * scale + 0.5);
				selectAreaRectNode.setStroke(selectionColor);
				selectAreaRectNode.setFill(null);
				drawingElementGroup.getChildren().add(selectAreaRectNode);
			}
		} else if (selectAreaRect != null && selectAreaRectNode != null) {
			selectAreaRectNode.setX(selectAreaRect[0].x * scale + 0.5);
			selectAreaRectNode.setY(selectAreaRect[0].y * scale + 0.5);
			selectAreaRectNode.setWidth(selectAreaRect[1].x * scale + 0.5);
			selectAreaRectNode.setHeight(selectAreaRect[1].y * scale + 0.5);
		}
		if (canvasInfo != null)
			canvasInfo.repaint();
		oldFd = fd;
		oldScale = scale;
	}

	@Override
	protected void populateTheaterFilter() {
		super.populateTheaterFilter();
		// LinkedHashMap<String, Boolean> filtersMap = new LinkedHashMap<String, Boolean>();

		TreeNode<BooleanProperty> filtersMap = new TreeNode<>(new BooleanProperty(STATICDIAGRAMFILTERS, true));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(BLOCK, true)));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(SHADOW, true)));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(LINK, true)));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(BLOCKNAME, true)));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(IO, true)));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(IONAME, true)));
		theaterFilter.addChild(filtersMap);
		filtersMap = new TreeNode<>(new BooleanProperty(DYNAMICDIAGRAMFILTERS, true));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(STACKSTATE, false)));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(CPUUSAGE, false)));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(HEATLINK, false)));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(COUNT, false)));
		filtersMap.addChild(new TreeNode<>(new BooleanProperty(MISSEDDATA, false)));
		theaterFilter.addChild(filtersMap);
	}

	private void removeLink(LinkDescriptor linkDesc) {
		drawingElementGroup.getChildren().remove(drawingLinks.get(linkDesc));
		drawingLinks.remove(linkDesc);
		if (linkDesc.input.io instanceof FlowDiagramInput) {
			IoDescriptor input = linkDesc.input;
			drawingInput.get(input.io).updateIOInfo(input.name, input.dataType);
		} else if (linkDesc.output.io instanceof FlowDiagramOutput) {
			IoDescriptor output = linkDesc.output;
			drawingOutput.get(output.io).updateIOInfo(output.name, output.dataType);
		}
	}

	private void resetDrawingLinksColor() {
		FlowDiagram fd = (FlowDiagram) getDrawableElement();
		if (fd == null || drawingLinks == null)
			return;
		for (CubicCurve cc : drawingLinks.values())
			cc.setStroke(this.link);
		// for (Block block : fd.getBlocks())
		// for (BlockInput input : block.getInputs()) {
		// Link link = input.getLink();
		// if (link != null)
		// drawingLinks.get(link).setStroke(this.link);
		// }
	}

	@Override
	protected void selectAreaChanged(boolean isCtrlDown) {
		FlowDiagram fd = (FlowDiagram) getDrawableElement();
		// clearSelectedElements();
		Rectangle areaRect = new Rectangle(selectAreaRect[0].x, selectAreaRect[0].y, selectAreaRect[1].x, selectAreaRect[1].y);
		for (Iterator<Object> iterator = areaSelectedElements.iterator(); iterator.hasNext();) {
			Object areaElement = iterator.next();
			if (areaElement instanceof Block) {
				Block block = (Block) areaElement;
				if (!areaRect.contains(block.getPosition())) {
					iterator.remove();
					drawingBlocks.get(block).resetCoreStroke();
					selectedElements.remove(areaElement);
				}
			} else if (areaElement instanceof DrawableIOComponent<?>) {
				DrawableDiagramIO<?> fdi = (DrawableDiagramIO<?>) areaElement;
				if (!areaRect.contains(fdi.getPosition())) {
					iterator.remove();
					fdi.resetCoreStroke();
					selectedElements.remove(areaElement);
				}
			}
		}
		for (Block block : fd.getBlocks())
			if (areaRect.contains(block.getPosition()) && !selectedElements.contains(block)) {
				selectedElements.add(block);
				areaSelectedElements.add(block);
				drawingBlocks.get(block).core.setStroke(selectionColor);
			}
		for (FlowDiagramInput fdInput : fd.getInputs())
			if (areaRect.contains(fdInput.getPosition()) && !selectedElements.contains(fdInput)) {
				DrawableInput element = drawingInput.get(fdInput);
				selectedElements.add(element);
				areaSelectedElements.add(element);
				element.core.setStroke(selectionColor);
			}
		for (FlowDiagramOutput fdOutput : fd.getOutputs())
			if (areaRect.contains(fdOutput.getPosition()) && !selectedElements.contains(fdOutput)) {
				DrawableOutput element = drawingOutput.get(fdOutput);
				selectedElements.add(element);
				areaSelectedElements.add(element);
				element.core.setStroke(selectionColor);
			}
	}

	@Override
	public void setAnimated(boolean animated, AnimationTimerConsumer animationTimerConsumer) {
		stateChanged(SchedulerState.STOPPED);
		this.animationTimerConsumer = animationTimerConsumer;
		stateChanged(SchedulerState.STARTED);
	}

	private void setBorderScale(Rectangle node, double scale) {
		node.setWidth(sWid * getScale() + 0.5);
		node.setHeight(sHei * getScale() + 0.5);
	}

	public void setComponent(Color component) {
		this.component = component;
		forceRepaint();
	}

	public void setComponentBorder(Color componentBorder) {
		this.componentBorder = componentBorder;
		forceRepaint();
	}

	public void setComponentCornerRadius(int componentCornerRadius) {
		this.componentCornerRadius = componentCornerRadius;
		forceRepaint();
	}

	public void setComponentDynamicIO(Color componentDynamicIO) {
		this.componentDynamicIO = componentDynamicIO;
		forceRepaint();
	}

	public void setComponentEntrie(Color componentEntrie) {
		this.componentEntrie = componentEntrie;
		forceRepaint();
	}

	public void setComponentHeaderIsolated(Color componentHeaderIsolated) {
		this.componentHeaderIsolated = componentHeaderIsolated;
		forceRepaint();
	}

	public void setComponentHeaderLocal(Color componentHeaderLocal) {
		this.componentHeaderLocal = componentHeaderLocal;
		forceRepaint();
	}

	public void setComponentHeaderRemote(Color componentHeaderRemote) {
		this.componentHeaderRemote = componentHeaderRemote;
		forceRepaint();
	}

	public void setComponentPropertyIO(Color componentPropertyIO) {
		this.componentPropertyIO = componentPropertyIO;
		forceRepaint();
	}

	public void setComponentStaticIO(Color componentStaticIO) {
		this.componentStaticIO = componentStaticIO;
		forceRepaint();
	}

	public void setComponentVarArgsIO(Color componentVarArgsIO) {
		this.componentVarArgsIO = componentVarArgsIO;
		forceRepaint();
	}

	public void setCountForeground(Color countForeground) {
		this.countForeground = countForeground;
		forceRepaint();
	}

	public void setCpuBarBackground(Color cpuBarBackground) {
		this.cpuBarBackground = cpuBarBackground;
		forceRepaint();
	}

	public void setCpuBarForeground(Color cpuBarForeground) {
		this.cpuBarForeground = cpuBarForeground;
		forceRepaint();
	}

	public void setCpuBarHeight(int cpuBarHeight) {
		this.cpuBarHeight = cpuBarHeight;
		forceRepaint();
	}

	public void setCpuBarVerticalGap(int cpuBarVerticalGap) {
		this.cpuBarVerticalGap = cpuBarVerticalGap;
		forceRepaint();
	}

	public void setDiagramBackground(Color diagramBackground) {
		this.diagramBackground = diagramBackground;
		setBackground(new Background(new BackgroundFill(diagramBackground, null, null)));
		forceRepaint();
	}

	@Override
	public void setDrawableElement(Object drawableElement) {
		for (Stage stage : visibleProperties.values())
			stage.close();
		visibleProperties.clear();
		FlowDiagram oldDrawableElement = (FlowDiagram) getDrawableElement();
		if (oldDrawableElement != null)
			oldDrawableElement.removeFlowDiagramChangeListener(this);
		super.setDrawableElement(drawableElement);
		((FlowDiagram) getDrawableElement()).setManagingCpuUsage(filterCPUUsage);
		((FlowDiagram) getDrawableElement()).addFlowDiagramChangeListener(this);
		((FlowDiagram) getDrawableElement()).setStopTimeOutTask((waitingBlocks, interruptProcess) -> {
			interruptedMode = false;
			Platform.runLater(() -> {
				ButtonType interruptButton = new ButtonType(interruptProcess != null ? "Interrupt them" : "Ok");
				alert = new Alert(AlertType.WARNING, null, interruptButton);
				updateStopAlert(waitingBlocks, true);
				alert.setHeaderText("Blocks below seems blocked");
				alert.resultProperty().addListener(e -> {
					if (alert.getResult() == interruptButton) {
						interruptProcess.run();
						alert = null;
						interruptedMode = true;
					}
				});
				alert.show();
				at = new AnimationTimer() {
					private long oltTime;
					private boolean phase = true;
					private int timeEllapseInInterruptedMode = 0;

					@Override
					public void handle(long time) {
						if (time - oltTime > 500000000) {
							phase = !phase;
							updateStopAlert(waitingBlocks, phase);
							// canvasInfo.paint(); //TODO HERE redéclencher dans un AnimationTimer le canvasInfo
							oltTime = time;
							if (interruptedMode) {
								timeEllapseInInterruptedMode++;
								if (timeEllapseInInterruptedMode == 4) {
									ButtonType restartButton = new ButtonType("Quit");
									ButtonType saveButton = new ButtonType("Save and Quit");
									alert = new Alert(AlertType.ERROR, null, saveButton, restartButton);
									alert.setHeaderText("Blocks below seems blocked while interrupted\nThis prevent Scenarium from continuing");
									alert.resultProperty().addListener(e -> {
										if (alert.getResult() == saveButton)
											saveScenario();
										if (alert.getResult() == saveButton || alert.getResult() == restartButton)
											System.exit(-1);
									});
									updateStopAlert(waitingBlocks, phase);
									alert.show();
								}
							}
						}
					}
				};
				at.start();
			});
			synchronized (this) {
				try {
					wait();
				} catch (InterruptedException e2) {
					if (at != null) {
						at.stop();
						at = null;
					}
					Platform.runLater(() -> {
						if (alert != null) {
							alert.getButtonTypes().add(new ButtonType("", ButtonData.CANCEL_CLOSE));
							alert.close();
							alert = null;
						}
						updateStopAlert(waitingBlocks, false);
						oldAlivedBlocks = null;
					});
				}
			}
		});
	}

	public void setGrid(Color grid) {
		this.grid = grid;
		forceRepaint();
	}

	public void setGridLenght(float gridLenght) {
		this.gridLenght = gridLenght;
		forceRepaint();
	}

	public void setGridScale(Group g, double scale) { // TODO bug si trop petite, revoir cette grille
		ObservableList<Node> gridChildren = g.getChildren();
		int ls = (int) Math.round(sWid * scale);
		int hs = (int) Math.round(sHei * scale);
		int ls1 = ls + 1;
		int hs1 = hs + 1;
		int index = 0;
		for (float i = 0; i < ls1; i += scale * gridLenght) {
			if (index == gridChildren.size()) {
				Line line = new Line();
				line.setStroke(grid);
				gridChildren.add(line);
			}
			Line line = (Line) gridChildren.get(index++);
			line.setStartX((int) i + 0.5);
			line.setStartY(0 + 0.5);
			line.setEndX((int) i + 0.5);
			line.setEndY(hs + 0.5);
		}
		for (float i = 0; i < hs1; i += scale * gridLenght) {
			if (index == gridChildren.size()) {
				Line line = new Line();
				line.setStroke(grid);
				gridChildren.add(line);
			}
			Line line = (Line) gridChildren.get(index++);
			line.setStartX(0 + 0.5);
			line.setStartY((int) i + 0.5);
			line.setEndX(ls + 0.5);
			line.setEndY((int) i + 0.5);
		}
		if (index != gridChildren.size())
			gridChildren.remove(index, gridChildren.size());
	}

	public void setHeaderSize(float headerSize) {
		this.headerSize = headerSize;
		forceRepaint();
	}

	public void setIoFlowDiagramSize(int ioFlowDiagramSize) {
		this.ioFlowDiagramSize = ioFlowDiagramSize;
		forceRepaint();
	}

	public void setIoRoundRadius(int ioRoundRadius) {
		this.ioRoundRadius = ioRoundRadius;
		forceRepaint();
	}

	public void setLink(Color link) {
		this.link = link;
		forceRepaint();
	}

	public void setLinkSelectionDistance(float linkSelectionDistance) {
		this.linkSelectionDistance = linkSelectionDistance;
	}

	public void setShadow(Color shadow) {
		this.shadow = shadow;
		forceRepaint();
	}

	public void setShadowRadius(float shadowRadius) {
		this.shadowRadius = shadowRadius;
		forceRepaint();
	}

	private void showBlockProperties(Block block) {
		Stage dia = visibleProperties.remove(block);
		if (dia != null) {
			dia.requestFocus();
			return;
		}
		Stage dialog = new Stage(StageStyle.DECORATED);
		dialog.initOwner(getScene().getWindow());
		dialog.setTitle(block.getName());
		final BeanManager bmo;
		if (block.isRemoteProperty()) {
			BeanManager bmTemp = null;
			try {
				BeanManager bmLocal = new BeanManager(block.operator, BeanManager.DEFAULTDIR);
				RemoteOperator remoteOperator = null;
				try {
					remoteOperator = RemoteOperator.createRemoteOperator(block, false, 1000);
				} catch (Exception e) {
					System.err.println(e.getMessage());
				}
				if (remoteOperator == null || !remoteOperator.isConnected()) {
					alert = new Alert(AlertType.ERROR, "Cannot reach " + block.getName() + " " + block.getProcessMode().toString().toLowerCase() + " bean.\nSwitch block to local process mode?",
							ButtonType.CANCEL, ButtonType.OK);
					Optional<ButtonType> result = alert.showAndWait();
					if (result.isPresent() && result.get() == ButtonType.OK) {
						block.setProcessMode(ProcessMode.LOCAL);
						showBlockProperties(block);
					}
					return;
				}
				bmTemp = new RMIBeanManager(remoteOperator, BeanManager.DEFAULTDIR);
				bmLocal.copyTo(bmTemp);
			} finally {
				bmo = bmTemp;
			}
		} else
			bmo = new BeanManager(block.operator, BeanManager.DEFAULTDIR);
		final BeanManager bmb = new BeanManager(block, "");
		LinkedList<PropertyDescriptor> propDesc = new LinkedList<>();
		ArrayList<Object> blockBackup = new ArrayList<>();

		GridPane editor = bmo.getEditor();
		addAsInputMenuToEditor(block, null, bmo.getBean(), bmo, editor);
		ScrollPane sp = new ScrollPane(editor);
		sp.setFitToWidth(true);
		sp.setFitToHeight(true);
		sp.setHbarPolicy(ScrollBarPolicy.NEVER);

		Tab operatorPropertiesTab = new Tab("Operator properties", sp);
		operatorPropertiesTab.setClosable(false);
		Tab blockPropertiesTab = new Tab("Block properties");
		blockPropertiesTab.setClosable(false);
		TabPane tabPane = new TabPane(operatorPropertiesTab, blockPropertiesTab);
		tabPane.getSelectionModel().selectedItemProperty().addListener((obs, oldTab, newTab) -> {
			GridPane _editor;
			if (newTab == blockPropertiesTab) {
				_editor = bmb.getEditor();
				if (propDesc.isEmpty()) {
					propDesc.addAll(BeanManager.getPropertyDescriptors(Block.class));
					for (final PropertyDescriptor pd : propDesc)
						try {
							blockBackup.add(pd.getReadMethod().invoke(block));
						} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
							e.printStackTrace();
						}
				}
			} else {
				_editor = bmo.getEditor();
				addAsInputMenuToEditor(block, null, bmo.getBean(), bmo, editor);
			}
			newTab.setContent(_editor);
			oldTab.setContent(null);
			tabPane.requestLayout(); // TODO JDK-8145490
			dialog.sizeToScene();
		});

		BorderPane bp = new BorderPane(tabPane);
		Button okButton = new Button("Ok");
		BorderPane.setAlignment(okButton, Pos.CENTER);
		Runnable closingAction = () -> {
			Object bean = bmo.getBean();
			if (bean instanceof RemoteBean)
				((RemoteBean) bean).destroy();
			if (tabPane.getSelectionModel().getSelectedItem() == operatorPropertiesTab)
				bmo.saveIfChanged();
			else
				saveScenario();
			visibleProperties.remove(block);
		};
		// block.addPropertyStructChangeListener(() -> {
		//
		// System.out.println("changed");
		// });
		okButton.setOnAction(e1 -> {
			dialog.close();
			closingAction.run();
		});

		Button resetButton = new Button("Reset");
		resetButton.setOnAction(e1 -> {
			if (tabPane.getSelectionModel().getSelectedItem() == operatorPropertiesTab) {
				bmo.reset();
				GridPane _editor = bmo.getEditor();
				operatorPropertiesTab.setContent(_editor);
				addAsInputMenuToEditor(block, null, bmo.getBean(), bmo, _editor);
			} else {
				block.resetProperties();
				blockPropertiesTab.setContent(bmb.getEditor());
			}
			tabPane.requestLayout(); // JDK-8145490
			dialog.sizeToScene();
		});
		Button refreshButton = new Button("Refresh");
		refreshButton.setOnAction(e1 -> {
			if (tabPane.getSelectionModel().getSelectedItem() == operatorPropertiesTab) {
				GridPane _editor = bmo.getEditor();
				operatorPropertiesTab.setContent(_editor);
				addAsInputMenuToEditor(block, null, bmo.getBean(), bmo, _editor);
			} else
				blockPropertiesTab.setContent(bmb.getEditor());
			tabPane.requestLayout(); // JDK-8145490
			dialog.sizeToScene();
		});
		Button saveButton = new Button("Save");
		saveButton.setOnAction(e1 -> {
			bmo.saveIfChanged();
			if (bmb.isEdited)
				saveScenario();
		});
		Button cancelButton = new Button("Cancel");
		cancelButton.setOnAction(e1 -> {
			if (tabPane.getSelectionModel().getSelectedItem() == operatorPropertiesTab) {
				bmo.load();
				bmo.isEdited = false;
				BeanManager.createSubBeans(bmo.getBean(), BeanManager.DEFAULTDIR);
				operatorPropertiesTab.setContent(bmo.getEditor());
			} else {
				int j = 0;
				for (final PropertyDescriptor pd : propDesc)
					try {
						pd.getWriteMethod().invoke(block, blockBackup.get(j++));
					} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
						e.printStackTrace();
					}
				blockPropertiesTab.setContent(bmb.getEditor());
			}
			tabPane.requestLayout(); // JDK-8145490
			dialog.sizeToScene();
		});
		HBox vBox = new HBox(10, resetButton, refreshButton, saveButton, cancelButton, okButton);
		vBox.setAlignment(Pos.CENTER);
		vBox.setPadding(new Insets(3, 3, 3, 3));
		bp.setBottom(vBox);
		Scene _scene = new Scene(bp, -1, -1);
		dialog.setScene(_scene);
		dialog.sizeToScene();
		dialog.setOnCloseRequest(e1 -> closingAction.run());
		dialog.show();
		visibleProperties.put(block, dialog);
		DynamicSizeEditorChangeListener scl = () -> {
			tabPane.requestLayout(); // JDK-8145490
			dialog.sizeToScene();
		};
		bmo.addSizeChangeListener(scl);
		bmb.addSizeChangeListener(scl);
		// ScenicView.show(_scene);
	}

	private void showDiagramContextMenu(ContextMenuEvent e) {
		ArrayList<DrawableIOComponent<?>> l = new ArrayList<>();
		l.addAll(drawingBlocks.values());
		l.addAll(drawingInput.values());
		l.addAll(drawingOutput.values());
		for (DrawableIOComponent<?> db : l) {
			Point2D p = db.core.sceneToLocal(e.getSceneX(), e.getSceneY(), true /* rootScene */);
			if (db.core.isVisible() && !db.core.isMouseTransparent() && db.core.contains(p))
				return;
		}
		if (contextMenu != null && contextMenu.isShowing()) {
			contextMenu.setAnchorX(e.getScreenX());
			contextMenu.setAnchorY(e.getScreenY());
		} else {
			createDiagramContextMenu();
			Task<Void> sleeper = new Task<>() {
				@Override
				protected Void call() throws Exception {
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {}
					return null;
				}
			};
			sleeper.setOnSucceeded(event -> {
				ContextMenu _contextMenu = contextMenu;
				if (_contextMenu != null)
					_contextMenu.show(DiagramDrawer.this, e.getScreenX(), e.getScreenY());
			});
			new Thread(sleeper).start();
		}
	}

	/* Renvoie la distance de p à la droite (ps, pe) au carrée */
	private static double squaredDistancePointToSegment(float[] ps, float[] pe, Point2D p) {
		if (ps[0] == pe[0] && ps[1] == pe[1])
			return Math.pow(p.getX() - ps[0], 2) + Math.pow(p.getY() - ps[1], 2);
		double sx = pe[0] - ps[0];
		double sy = pe[1] - ps[1];
		double ux = p.getX() - ps[0];
		double uy = p.getY() - ps[1];
		double dp = sx * ux + sy * uy;
		if (dp < 0)
			return Math.pow(p.getX() - ps[0], 2) + Math.pow(p.getY() - ps[1], 2);
		double sn2 = sx * sx + sy * sy;
		if (dp > sn2)
			return Math.pow(p.getX() - pe[0], 2) + Math.pow(p.getY() - pe[1], 2);
		return Math.pow(ux, 2) + Math.pow(uy, 2) - dp * dp / sn2;
	}

	@Override
	public void stateChanged(SchedulerState state) {
		FxUtils.runLaterIfNeeded(() -> { // OK
			if (state == SchedulerState.STARTED) {
				isRunning = true;
				if (notChangeableAtRuntimeNodes != null)
					notChangeableAtRuntimeNodes.forEach(node -> node.setDisable(isRunning));
				if (animationTimerConsumer == null)
					return;
				if ((filterStackState || filterCPUUsage || filterHeatLink || filterCount || filterMissedData) && canvasInfo == null) {
					canvasInfo = new CanvasInfo();
					canvasInfo.setManaged(false);
					canvasInfo.widthProperty().bind(widthProperty());
					canvasInfo.heightProperty().bind(heightProperty());
					canvasInfo.setMouseTransparent(true);
					canvasInfo.setFocusTraversable(false);
					getChildren().add(canvasInfo);
					if (animationTimerConsumer != null)
						animationTimerConsumer.register(canvasInfo);
					canvasInfo.repaint();
				}
			} else if (state == SchedulerState.PRESTOP) {
				if (stoppingAnimationTimer != null)
					stoppingAnimationTimer.stop();
				stoppingAnimationTimer = new AnimationTimer() {

					@Override
					public void handle(long now) {
						CanvasInfo _canvasInfo = canvasInfo;
						if (_canvasInfo != null)
							_canvasInfo.paint();

					}
				};
				stoppingAnimationTimer.start();
			} else if (state == SchedulerState.STOPPED) {
				isRunning = false;
				if (notChangeableAtRuntimeNodes != null)
					notChangeableAtRuntimeNodes.forEach(node -> node.setDisable(isRunning));
				if (canvasInfo != null) {
					if (animationTimerConsumer != null)
						animationTimerConsumer.unRegister(canvasInfo);
					getChildren().remove(canvasInfo);
					// ((FlowDiagram) getDrawableElement()).removeDynamicFlowDiagramDepictionChangeListener(this);
					canvasInfo = null;
					resetDrawingLinksColor();
				}
				if (stoppingAnimationTimer != null) {
					stoppingAnimationTimer.stop();
					stoppingAnimationTimer = null;
				}
			}
		});
	}

	private static void updateCurveControlPoint(CubicCurve cubic) {
		double y1 = cubic.getStartY();
		double y2 = cubic.getEndY();
		double x1 = cubic.getStartX();
		double x2 = cubic.getEndX();
		double deltaValueX = x2 - x1;
		if (deltaValueX < 0)
			deltaValueX = 0;
		double deltaValueY = y2 > y1 ? deltaValueX : -deltaValueX;
		double mid = (x1 + x2) / 2.0;
		cubic.setControlX1(mid - deltaValueX);
		cubic.setControlY1(y1 + deltaValueY);
		cubic.setControlX2(mid + deltaValueX);
		cubic.setControlY2(y2 - deltaValueY);
	}

	@Override
	public boolean updateFilterWithPath(String[] filterPath, boolean value) {
		String filterName = filterPath[filterPath.length - 1];
		if (filterName.equals(BLOCK)) {
			filterBlock = value;
			if (drawingBlocks != null)
				for (DrawableBlock db : drawingBlocks.values())
					db.setVisible(value);
		} else if (filterName.equals(LINK)) {
			filterLink = value;
			if (drawingLinks != null)
				for (CubicCurve cc : drawingLinks.values())
					cc.setVisible(value);
		} else if (filterName.equals(IO)) {
			filterIO = value;
			if (drawingBlocks != null)
				for (DrawableBlock db : drawingBlocks.values())
					for (DrawableIOComponent<? extends Shape>.DrawingIo di : db.drawingIOs.values())
						di.circle.setVisible(value);
		} else if (filterName.equals(STACKSTATE)) {
			filterStackState = value;
			updatePaneInfo();
		} else if (filterName.equals(CPUUSAGE)) {
			filterCPUUsage = value;
			updatePaneInfo();
			if (getDrawableElement() != null)
				((FlowDiagram) getDrawableElement()).setManagingCpuUsage(filterCPUUsage);
		} else if (filterName.equals(HEATLINK)) {
			filterHeatLink = value;
			if (!filterHeatLink)
				resetDrawingLinksColor();
			updatePaneInfo();
		} else if (filterName.equals(MISSEDDATA)) {
			filterMissedData = value;
			updatePaneInfo();
		} else if (filterName.equals(COUNT)) {
			filterCount = value;
			updatePaneInfo();
		} else if (filterName.equals(IONAME)) {
			filterIOName = value;
			if (drawingBlocks != null)
				for (DrawableBlock db : drawingBlocks.values())
					for (DrawableIOComponent<? extends Shape>.DrawingIo di : db.drawingIOs.values())
						di.text.setVisible(value);
		} else if (filterName.equals(BLOCKNAME)) {
			filterBlockName = value;
			if (drawingBlocks != null)
				for (DrawableBlock db : drawingBlocks.values())
					db.blockName.setVisible(value);
		} else if (filterName.equals(SHADOW)) {
			filterShadow = value;
			if (drawingBlocks != null)
				for (DrawableBlock db : drawingBlocks.values())
					db.core.setEffect(value ? new DropShadow(shadowRadius, shadow) : null);
		} else {
			if (filterName.equals(GRID) && drawingElementGroup != null)
				if (value)
					drawingElementGroup.getChildren().add(0, creatGrid());
				else
					drawingElementGroup.getChildren().removeIf(e -> e.getId() == GRID);
			if (filterName.equals(BORDER) && drawingElementGroup != null)
				if (value)
					drawingElementGroup.getChildren().add(0, createDrawingborder());
				else
					drawingElementGroup.getChildren().removeIf(e -> e.getId() == BORDER);
			return super.updateFilterWithPath(filterPath, value);
		}
		repaint(false);
		return true;
	}

	private void updateLinkIO() {
		for (DrawableBlock drawableBlock : drawingBlocks.values())
			drawableBlock.updateIOStroke();
		for (DrawableInput drawableInput : drawingInput.values())
			drawableInput.updateIOStroke();
		for (DrawableOutput drawableOutput : drawingOutput.values())
			drawableOutput.updateIOStroke();
	}

	private void updatePaneInfo() {
		if (!isStopped())
			stateChanged(filterStackState || filterCPUUsage || filterHeatLink || filterCount || filterMissedData ? SchedulerState.STARTED : SchedulerState.PRESTOP);
	}

	private void updatePosition(double newX, double newY, LocalisedObject selectedElement) {
		selectedElement.setPosition(newX, newY);
		if (selectedElement instanceof Block)
			drawingBlocks.get(selectedElement).updatePosition();
		else if (selectedElement instanceof DrawableInput)
			((DrawableInput) selectedElement).updatePosition();
		else
			((DrawableOutput) selectedElement).updatePosition();

	}

	private void updateStopAlert(List<Block> waitingBlocks, boolean phase) {
		if (alert != null) {
			StringBuilder sb = new StringBuilder();
			for (Iterator<Block> iterator = waitingBlocks.iterator(); iterator.hasNext();) {
				Block block = iterator.next();
				sb.append(block.getName());
				if (iterator.hasNext())
					sb.append("\n");
			}
			String waitingBlocksText = sb.toString();
			if (!waitingBlocksText.equals(alert.getContentText()))
				alert.setContentText(sb.toString());
		}
		if (oldAlivedBlocks != null)
			for (Block block : oldAlivedBlocks) {
				DrawableBlock drawingBlock = drawingBlocks.get(block);
				if (drawingBlock != null)
					if (!waitingBlocks.contains(block))
						drawingBlock.resetCoreStroke();
					else
						drawingBlock.updateDrawableWithStopWarning(phase, interruptedMode);
			}
		else
			for (Block block : waitingBlocks) {
				DrawableBlock drawingBlock = drawingBlocks.get(block);
				if (drawingBlock != null)
					drawingBlock.updateDrawableWithStopWarning(phase, interruptedMode);
			}
		oldAlivedBlocks = new ArrayList<>(waitingBlocks);
	}

	class CanvasInfo extends Canvas implements VisuableSchedulable {

		// private volatile boolean needToRepaint;
		private int fpsDivider = 6;
		private int fpsCounter = 0;
		private HashMap<Link, ArrayList<Tuple<Long, Integer>>> linkConsumeCpt = new HashMap<>();
		private HashMap<Link, Integer> linkConsume = new HashMap<>();
		private Text text = new Text();

		@Override
		public void paint() {
			fpsCounter++;
			if (fpsCounter == fpsDivider) {
				fpsCounter = 0;
				if (true) {// needToRepaint
					FlowDiagram fd = (FlowDiagram) getDrawableElement();
					if (fd == null)
						return;
					GraphicsContext g = getGraphicsContext2D();
					g.setTransform(1, 0, 0, 1, 0, 0);
					g.clearRect(0, 0, getWidth(), getHeight());
					double scale = getScale();
					g.setTransform(1, 0, 0, 1, (int) getxTranslate(), (int) getyTranslate());
					if (filterMissedData) {
						g.setFill(Color.RED.darker());
						g.setTextAlign(TextAlignment.RIGHT);
						g.setTextBaseline(VPos.CENTER);
						int scaleIORadius = (int) (ioRoundRadius * scale);
						for (Block block : fd.getBlocks()) {
							Rectangle2D rectangle = block.getRectangle();
							double x = rectangle.getMinX();
							double y = rectangle.getMinY();
							for (BlockInput input : block.getInputs())
								if (input.getPosition() != null) {
									Point2D iPos = input.getPosition();
									int xRound = (int) ((x + iPos.getX()) * scale - scaleIORadius);
									int yRound = (int) ((y + iPos.getY()) * scale - scaleIORadius);
									long nbMissedData = input.getBuffer().getNbMissedData();
									if (nbMissedData != 0)
										g.fillText(Long.toString(input.getBuffer().getNbMissedData()), xRound - 4, yRound);
								}
						}
					}
					if (filterStackState) {
						int scaleIORadius = (int) (ioRoundRadius * scale);
						g.setFill(Color.BLACK);
						for (Block block : fd.getBlocks())
							if (block.isEnable()) {
								Rectangle2D rectangle = block.getRectangle();
								double x = rectangle.getMinX();
								double y = rectangle.getMinY();
								for (BlockInput input : block.getInputs())
									if (input.getPosition() != null) {
										Point2D iPos = input.getPosition();
										int xRound = (int) ((x + iPos.getX()) * scale - scaleIORadius);
										int yRound = (int) ((y + iPos.getY()) * scale - scaleIORadius);
										int stackSize = input.getBuffer().size();
										int xBegin = xRound - 4;
										if (filterMissedData) {
											long nbMissedData = input.getBuffer().getNbMissedData();
											if (nbMissedData != 0) {
												text.setText(Long.toString(nbMissedData));
												xBegin -= (int) (text.getLayoutBounds().getWidth() + 8);
											}
										}
										int yBegin = yRound - 2;
										for (int m = 0; m < stackSize; m++)
											g.fillRect(xBegin - m * 5, yBegin, 4, 4);
									}
							}
					}
					if (filterCPUUsage)
						if (fd.getCpuUsage() != -1) {
							Affine oldTransform = g.getTransform();
							g.setTransform(1, 0, 0, 1, 0, 0);
							int cpuWidth = (int) getWidth() / 5;
							int cpux = ((int) getWidth() - cpuWidth) / 2;
							int cpuy = (int) getHeight() - cpuBarHeight - cpuBarVerticalGap;
							g.setFill(cpuBarBackground);
							g.fillRect(cpux, cpuy, cpuWidth, cpuBarHeight);
							g.setFill(cpuBarForeground);
							g.fillRect(cpux, cpuy, cpuWidth * fd.getCpuUsage(), cpuBarHeight);
							g.setTransform(oldTransform);
						} else
							for (Block block : fd.getBlocks())
								if (block.isEnable()) {
									Rectangle2D rectangle = block.getRectangle();
									double x = rectangle.getMinX();
									double y = rectangle.getMinY();
									int xRect = (int) (x * scale);
									int wRect = (int) (rectangle.getWidth() * scale);
									int hRect = (int) (rectangle.getHeight() * scale);
									int yRect = (int) (y * scale) + hRect + 2;
									g.setFill(cpuBarBackground);
									g.fillRect(xRect, yRect, wRect, cpuBarHeight);
									float cpuUsage = block.getCpuUsage();
									if (cpuUsage < 0)
										cpuUsage = 0;
									g.setFill(cpuBarForeground);
									g.fillRect(xRect, yRect, (int) (wRect * cpuUsage), cpuBarHeight);
								}
					if (filterCount) {
						g.setTextAlign(TextAlignment.CENTER);
						g.setTextBaseline(VPos.CENTER);
						text.setText("");
						double textHeight = text.getLayoutBounds().getHeight();
						for (Block block : fd.getBlocks())
							if (block.isEnable()) {
								long nbConsume = block.getConsume();
								Rectangle2D rectangle = block.getRectangle();
								double x = rectangle.getMinX();
								double y = rectangle.getMinY();
								int xRect = (int) (x * scale);
								int wRect = (int) (rectangle.getWidth() * scale);
								int hRect = (int) (rectangle.getHeight() * scale);
								int yRect = (int) (y * scale) + hRect + 2;
								if (filterCPUUsage && fd.getCpuUsage() == -1)
									yRect += cpuBarHeight + 2;
								g.setFill(cpuBarBackground);
								g.fillRect(xRect, yRect, wRect, textHeight);
								g.setFill(countForeground);
								g.fillText(Long.toString(nbConsume), xRect + wRect / 2, yRect + textHeight / 2);
							}
					}
					if (filterHeatLink) {
						linkConsume.clear();
						long time = System.currentTimeMillis();
						int maxCons = Integer.MIN_VALUE;
						for (Block block : fd.getBlocks())
							for (BlockInput input : block.getInputs()) {
								Link link = input.getLink();
								if (link != null) {
									ArrayList<Tuple<Long, Integer>> consumeStack = linkConsumeCpt.get(link);
									if (consumeStack == null) {
										consumeStack = new ArrayList<>();
										linkConsumeCpt.put(link, consumeStack);
									}
									consumeStack.add(new Tuple<>(time, link.getConsume()));
									for (int k = 0; k < consumeStack.size(); k++)
										if (time > consumeStack.get(k).getFirst() + 1000)
											consumeStack.remove(k--);
									int sum = consumeStack.get(consumeStack.size() - 1).getSecond() - consumeStack.get(0).getSecond();
									linkConsume.put(link, sum);
									if (sum > maxCons)
										maxCons = sum;
								}
							}
						float _maxCons = maxCons;
						drawingLinks.forEach((ld, cc) -> {
							if (!selectedElements.contains(ld)) {
								Integer consume = linkConsume.get(ld.link);
								if (consume != null) {
									float cons = consume / _maxCons;
									if (cons < 0)
										cons = 0;
									else if (cons > 1)
										cons = 1;
									cc.setStroke(cons == 0 ? new Color(0, 0, 0, 1) : new Color(cons, 0, 1 - cons, 1));
								}
							}
						});
					}
				}
			}
		}

		public void repaint() {
			fpsCounter = fpsDivider - 1;
		}

		@Override
		public void setAnimated(boolean animated) {}
	}

	class DrawableBlock extends DrawableIOComponent<Rectangle> {
		private Block block;
		private Region oldOperatorRegion = null;
		private Region operatorRegion = null;
		private Text blockName;
		private VarArgsInputChangeListener varArgsInputChangeListener;
		private RunningStateChangeListener stateChangeChangeListener;
		private PropertyChangeListener propertyChangeListener;
		private ContextMenu contextMenu;
		private Tooltip toolTip;
		private boolean isDefaulting;
		private boolean isWarning;

		// To watch -> warning info rectangle, input, outputs, name, defaulting, enable, processMode
		// Changer updateDrawingLinksPosition pour prendre autre chose
		public DrawableBlock(Block block, double scale, IoDescriptor[] ioDescriptors) {
			super(scale);
			this.block = block;
			// Mise en place du core
			core = new Rectangle();
			core.setViewOrder(0);

			// Mise en place du ToolTip

			String toolTipText;
			Object operator = block.operator;
			if (operator == null)
				toolTipText = null;
			else {
				toolTipText = new String("Name: " + block.getName() + "\nType: " + operator.getClass().getSimpleName() + "\nPath: " + operator.getClass());
				for (Annotation anno : operator.getClass().getAnnotations())
					if (anno instanceof BlockInfo) {
						toolTipText = toolTipText + "\nAuthor(s): " + ((BlockInfo) anno).author() + "\n" + ((BlockInfo) anno).info();
						break;
					}
				String warning = block.getWarning();
				if (warning != null)
					toolTipText += "\nWarning: " + warning;
				toolTip = new Tooltip(toolTipText); // Dépend de warning et de name
				toolTip.setShowDelay(Duration.seconds(1));
				toolTip.setShowDuration(Duration.minutes(1));
				if (warning != null)
					toolTip.setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/dialog-warning.png"))));
				Tooltip.install(core, toolTip);
			}
			core.setFill(
					new LinearGradient(0, 0, 0, 1, true, CycleMethod.NO_CYCLE, new Stop[] { new Stop(0, component), new Stop(0.25, component), new Stop(0.5, component), new Stop(1, component) }));
			initBlockCore(ioDescriptors);
			core.setOnMouseDragged(e -> e.consume()); // éviter dragg clic gauche
			core.setOnContextMenuRequested(e -> {
				if (tempLink == null) {
					if (contextMenu != null && contextMenu.isShowing()) {
						contextMenu.setAnchorX(e.getScreenX());
						contextMenu.setAnchorY(e.getScreenY());
					} else {
						boolean blockDisable = !block.isEnable();
						CheckMenuItem menuDisable = new CheckMenuItem("Disable");
						menuDisable.setSelected(blockDisable);
						menuDisable.setAccelerator(new KeyCodeCombination(KeyCode.D));
						menuDisable.setOnAction(e1 -> block.setEnable(!block.isEnable()));
						CheckMenuItem menuIsolate = new CheckMenuItem("Isolate in separated process");
						menuIsolate.setSelected(block.getProcessMode() == ProcessMode.ISOLATED);
						menuIsolate.setAccelerator(new KeyCodeCombination(KeyCode.I));
						menuIsolate.setDisable(blockDisable);
						menuIsolate.setOnAction(e1 -> block.setProcessMode(menuIsolate.isSelected() ? ProcessMode.ISOLATED : ProcessMode.LOCAL));
						CheckMenuItem menuRemote = new CheckMenuItem("Run in an remote machine");
						menuRemote.setSelected(block.getProcessMode() == ProcessMode.REMOTE);
						menuRemote.setAccelerator(new KeyCodeCombination(KeyCode.R));
						menuRemote.setDisable(blockDisable);
						menuRemote.setOnAction(e1 -> block.setProcessMode(menuRemote.isSelected() ? ProcessMode.REMOTE : ProcessMode.LOCAL));
						CheckMenuItem menuProperties = new CheckMenuItem("Properties");
						menuProperties.setAccelerator(new KeyCodeCombination(KeyCode.P));
						menuProperties.setOnAction(e1 -> showBlockProperties(block));
						contextMenu = new ContextMenu(menuDisable, menuIsolate, menuRemote, menuProperties);
						contextMenu.show(this, e.getScreenX(), e.getScreenY());
					}
				}
				e.consume();
			});

			// Entete du core
			blockName = new Text();
			blockName.setViewOrder(-1);
			updateBlockName(block.getName());
			blockName.setVisible(filterBlockName);
			blockName.setOnMousePressed(core.getOnMousePressed());
			if (block.operator instanceof LocalScenario) {
				addEventFilter(DragEvent.DRAG_OVER, e -> {
					Dragboard db = e.getDragboard();
					if (db.hasFiles() && ScenarioManager.getScenarioType(db.getFiles().get(0)) != null) {
						if (!selectedElements.contains(block) || selectedElements.size() != 1) {
							clearSelectedElements();
							selectedElements.add(block);
							drawingBlocks.get(block).core.setStroke(selectionColor);
							e.acceptTransferModes(TransferMode.MOVE);
							e.consume();
						}
					} else if (selectedElements.size() != 0)
						clearSelectedElements();
				});
				addEventHandler(DragEvent.DRAG_DROPPED, e -> {
					Dragboard db = e.getDragboard();
					if (db.hasFiles()) {
						File scenario = db.getFiles().get(0);
						Class<? extends Scenario> scenarioType = ScenarioManager.getScenarioType(scenario);
						if (scenarioType != null) {
							e.consume();
							if (!scenarioType.equals(block.operator.getClass())) {
								showMessage("The scenario is not fit for the " + block.operator.getClass().getSimpleName() + " player", true);
								return;
							}
							new Thread(new Task<>() {
								@Override
								protected Boolean call() throws Exception {
									scheduler.stop(); // Opération potentiellement bloquante...
									LocalScenario localScenario = (LocalScenario) block.operator;
									// localScenario.synchroClose(); //Pas besoin, le setFile le fait
									// Platform.runLater(() -> localScenario.setFile(scenario)); //Pb de synchro ici
									localScenario.setFile(scenario);
									new BeanManager(localScenario, RenderPane.drawerPropertiesDir).save(false);
									try {
										localScenario.birth();
										block.setDefaulting(false);
									} catch (Exception e2) {
										block.setDefaulting(true);
										showMessage(ScenarioManager.getErrorMessage(e2), true);
									}
									return true;
								}
							}).start();
							e.setDropCompleted(true);
						}
						e.consume();
					}
				});
			}
			blockName.setOnMouseClicked(e -> {
				if (e.getClickCount() == 2 && e.getButton() == MouseButton.PRIMARY) {
					getChildren().remove(blockName);
					TextField tf = new TextField(blockName.getText());
					tf.setFont(blockName.getFont());
					tf.setAlignment(Pos.CENTER);
					tf.setBackground(new Background(new BackgroundFill(null, null, null)));
					tf.setStyle("-fx-text-fill: white;");
					tf.setPadding(new Insets(0));
					int halfArcLenghtRect = (int) (componentCornerRadius * scale + 0.5);
					tf.setPrefWidth((int) core.getWidth() - halfArcLenghtRect / 2);
					tf.focusedProperty().addListener((ov, oldValue, newValue) -> {
						if (newValue)
							tf.selectAll();
						else {
							try {
								BeanEditor.renameBean(BeanEditor.getBeanDesc(block.getOperator()), tf.getText());
							} catch (IllegalArgumentException e1) {
								System.err.println(e1.getMessage());
							}
							getChildren().remove(tf);
							getChildren().add(blockName);
						}
					});
					tf.setOnKeyPressed(e1 -> {
						if (e1.getCode() == KeyCode.ENTER)
							FxUtils.traverse(); // RT-21596
					});
					tf.setTranslateX(halfArcLenghtRect / 4);
					tf.setTranslateY(blockName.getTranslateY() - blockName.getLayoutBounds().getHeight() + 1);
					getChildren().add(tf);
					tf.requestFocus();
				}
				e.consume();
			});

			core.setOnMouseClicked(e -> {
				if (e.getClickCount() == 2 && e.getButton() == MouseButton.PRIMARY)
					showBlockProperties(block);
				if (contextMenu != null && e.getButton() == MouseButton.PRIMARY) {
					contextMenu.hide();
					contextMenu = null;
				}
				e.consume();
			});
			getChildren().addAll(core, blockName);
			for (DrawableIOComponent<? extends Shape>.DrawingIo di : drawingIOs.values()) {
				getChildren().add(di.circle);
				getChildren().add(di.text);
			}
			if (operatorRegion != null)
				operatorRegion.toFront();
			layoutChildren();
			varArgsInputChangeListener = (indexOfInput, type) -> {
				if (type != VarArgsInputChangeListener.CHANGED) {
					IoDescriptor[] newIoDescriptors = getIODescriptors(block);
					FxUtils.runLaterIfNeeded(() -> updateIOStruct(newIoDescriptors));
				}
			};
			block.addVarArgsInputChangeListener(varArgsInputChangeListener);
			stateChangeChangeListener = () -> {
				String warning = block.getWarning();
				boolean isDefaulting = block.isDefaulting();
				FxUtils.runLaterIfNeeded(() -> {
					isWarning = warning != null;
					this.isDefaulting = isDefaulting;
					if (toolTip != null) {
						String newToolTipText = toolTip.getText();
						int waningIndex = newToolTipText.indexOf("\nWarning: ");
						if (waningIndex != -1)
							newToolTipText = newToolTipText.substring(0, waningIndex);
						if (warning != null)
							newToolTipText += "\nWarning: " + warning;
						toolTip.setText(newToolTipText);
					}
					resetCoreStroke();
				});
			};
			block.addRunningStateChangeListener(stateChangeChangeListener);
			propertyChangeListener = (evt) -> {
				if (evt.getPropertyName().equals("enable") || evt.getPropertyName().equals("processMode")) {
					boolean enable = block.isEnable();
					ProcessMode processMode = block.getProcessMode();
					FxUtils.runLaterIfNeeded(() -> updateEffect(enable, processMode));
				}
			};
			block.addPropertyChangeListener(propertyChangeListener);
			updateEffect(block.isEnable(), block.getProcessMode());
			drawingBlocks.put(block, this);
			setVisible(filterBlock);
		}

		public void delete() {
			block.removeVarArgsInputChangeListener(varArgsInputChangeListener);
			block.removeRunningStateChangeListener(stateChangeChangeListener);
			block.removePropertyChangeListener(propertyChangeListener);
		}

		private double[] getFontSize(double scale, String bName, int maxWidthAvailable, int maxHeightAvailable, int minFontSize, int maxFontSize) {
			Text t = new Text(bName);
			double sMaxFontSize = maxFontSize;
			t.setFont(new Font(Math.max(sMaxFontSize, minFontSize)));
			Bounds b = t.getLayoutBounds();
			double tw = b.getWidth();
			double th = b.getHeight();
			while ((tw > maxWidthAvailable || th > maxHeightAvailable) && sMaxFontSize > minFontSize) {
				sMaxFontSize--;
				t.setFont(new Font(sMaxFontSize));
				b = t.getLayoutBounds();
				tw = b.getWidth();
				th = b.getHeight();
			}
			return new double[] { tw, sMaxFontSize };
		}

		protected float getGapBetweenIO() {
			return (float) ((block.getRectangle().getHeight() - headerSize - componentCornerRadius * 2) / ((double) block.getNbInput() + block.getNbOutput()));
		}

		@Override
		protected LocalisedObject getSeletecion() {
			return block;
		}

		@Override
		protected void initIOPos(IoDescriptor[] ioDescriptors) { // Utilise rectangle Ok inputs et outputs pas ok, set position dans inputs, outputs...
			int arcLenght = componentCornerRadius * 2;
			int headerWithArcSize = (int) (arcLenght / 2.0f + headerSize + 0.5);
			Rectangle2D rectangle = block.getRectangle();
			float gap = getGapBetweenIO();
			int nbInput = 0;
			for (; nbInput < ioDescriptors.length; nbInput++)
				if (!(ioDescriptors[nbInput].io instanceof Input))
					break;
			for (int j = 0; j < nbInput; j++)
				ioDescriptors[j].io.setPosition(new Point2D(0, headerWithArcSize + j * gap + gap / 2.0));
			for (int j = nbInput; j < ioDescriptors.length; j++)
				ioDescriptors[j].io.setPosition(new Point2D(rectangle.getWidth(), headerWithArcSize + j * gap + gap / 2.0));
		}

		@Override
		public void populateIO(IoDescriptor[] ioDescriptors) {
			initIOPos(ioDescriptors);
			for (IoDescriptor ioDescriptor : ioDescriptors)
				createIO(ioDescriptor);
		}

		@Override
		public void resetCoreStroke() {
			core.setStroke(isDefaulting ? Color.RED : isWarning ? Color.YELLOW : componentBorder);
		}

		@Override
		public void setIOPos(IO io, Circle drawingIO) { // Utilise position de io...
			Point2D pos = io.getPosition();
			if (pos != null) { // Peut être null si changement asynchone
				drawingIO.setCenterX((int) (pos.getX() * scale + 0.5));
				drawingIO.setCenterY((int) (pos.getY() * scale + 0.5));
			}
		}

		@Override
		public Runnable setScale(double scale) {
			Runnable task = super.setScale(scale);
			updateBlockNameStyle();
			return task;
		}

		@Override
		protected void updateBlockCoreSize() { // Utilise bloc rectangle Ok processmode, pas ok
			Rectangle2D rectangle = block.getRectangle();
			core.setWidth(rectangle.getWidth() * scale);
			int arcLenght = componentCornerRadius * 2;
			int headerWithArcSize = (int) (arcLenght / 2.0f + headerSize + 0.5);
			double headRatioPos = headerWithArcSize / rectangle.getHeight();
			LinearGradient lg = (LinearGradient) core.getFill();
			List<Stop> stops = lg.getStops();
			core.setFill(new LinearGradient(0, 0, 0, 1, true, CycleMethod.NO_CYCLE, new Stop[] { new Stop(0, stops.get(0).getColor()), new Stop(headRatioPos, stops.get(1).getColor()),
					new Stop(headRatioPos, stops.get(2).getColor()), new Stop(1, stops.get(3).getColor()) }));
			core.setHeight(rectangle.getHeight() * scale);
			core.setArcHeight((int) (arcLenght * scale + 0.5));
			core.setArcWidth((int) (arcLenght * scale + 0.5));
			operatorRegion = block.operator instanceof EvolvedOperator ? ((EvolvedOperator) block.operator).getNode() : null;
			if (operatorRegion != null) {
				operatorRegion.setOnMouseClicked(core.getOnMouseClicked());
				operatorRegion.setOnMousePressed(core.getOnMousePressed());
				operatorRegion.setPrefSize(rectangle.getWidth() * scale, (rectangle.getHeight() - headerWithArcSize) * scale);
				operatorRegion.setMaxSize(rectangle.getWidth() * scale, (rectangle.getHeight() - headerWithArcSize) * scale);
				operatorRegion.setMinSize(rectangle.getWidth() * scale, (rectangle.getHeight() - headerWithArcSize) * scale);
				operatorRegion.setTranslateY(headerWithArcSize * scale);
			}
			if (operatorRegion != oldOperatorRegion) {
				if (oldOperatorRegion != null)
					getChildren().remove(oldOperatorRegion);
				if (operatorRegion != null) {
					operatorRegion.setViewOrder(-2);
					getChildren().add(operatorRegion);
				}
			}
			oldOperatorRegion = operatorRegion;
		}

		public void updateBlockName(String name) {
			blockName.setText(name);
			String toolTipText = toolTip.getText();
			toolTip.setText("Name: " + name + toolTipText.substring(toolTipText.indexOf("\nType: ")));
			updateBlockNameStyle();
		}

		private void updateBlockNameStyle() {
			int halfArcLenghtRect = (int) (componentCornerRadius * scale + 0.5);
			int maxWidthAvailable = (int) core.getWidth() - halfArcLenghtRect / 2;
			int arcLenght = componentCornerRadius * 2;
			int maxHeightAvailable = (int) ((arcLenght / 2.0f + headerSize) * scale);
			double[] textInfo = getFontSize(1, blockName.getText(), maxWidthAvailable, maxHeightAvailable, 9, 14);
			blockName.setFont(new Font(textInfo[1]));
			Bounds textBounds = blockName.getLayoutBounds();
			if (textBounds.getWidth() < maxWidthAvailable) {
				blockName.setTranslateX(core.getWidth() / 2 - textBounds.getWidth() / 2);
				blockName.setFill(Color.WHITE);
			} else {
				blockName.setTranslateX(halfArcLenghtRect / 4);
				// blockName.setClip(new Rectangle(0, -textBounds.getHeight(), maxWidthAvailable, textBounds.getHeight())); //Il sert a quoi, bug clip scale quand il est la
				double end = maxWidthAvailable / textBounds.getWidth();
				Stop[] _stops = new Stop[] { new Stop(0, Color.WHITE), new Stop(0.9 * end, Color.WHITE), new Stop(1 * end, Color.TRANSPARENT) };
				blockName.setFill(new LinearGradient(0, 0, 1, 0, true, CycleMethod.NO_CYCLE, _stops));
			}
			blockName.setTranslateY(textBounds.getHeight() / 2 + (halfArcLenghtRect / 2 + headerSize * scale) / 2);// -textBounds.getHeight() / 2 + (halfArcLenghtRect + headerSize * scale) / 2
		}

		public void updateDrawableWithStopWarning(boolean phase, boolean interruptedMode) {
			core.setStroke(phase ? interruptedMode ? Color.RED : Color.YELLOW : componentBorder);
		}

		@Override
		protected void updateDrawingLinksPosition() {
			Set<LinkDescriptor> linksDesc = drawingLinks.keySet();
			drawingIOs.keySet().stream().filter(e -> e instanceof Input).forEach(input -> {
				for (LinkDescriptor linkDesc : linksDesc)
					if (linkDesc.input.io == input)
						setPosition(input, drawingLinks.get(linkDesc));
			});
			drawingIOs.keySet().stream().filter(e -> e instanceof Output).forEach(output -> {
				for (LinkDescriptor linkDesc : linksDesc)
					if (linkDesc.output.io == output)
						setPosition(output, drawingLinks.get(linkDesc));
			});
		}

		private void updateEffect(boolean enable, ProcessMode processMode) {
			setEffect(enable ? null : new ColorAdjust(0, 0, 0, -0.8));
			double headRatioPos = (int) (componentCornerRadius * 2 / 2.0f + headerSize + 0.5) / block.getRectangle().getHeight();
			Color headerColor = processMode == ProcessMode.LOCAL ? componentHeaderLocal : processMode == ProcessMode.REMOTE ? componentHeaderIsolated : componentHeaderRemote;
			core.setFill(new LinearGradient(0, 0, 0, 1, true, CycleMethod.NO_CYCLE,
					new Stop[] { new Stop(0, headerColor), new Stop(headRatioPos, headerColor), new Stop(headRatioPos, component), new Stop(1, component) }));
		}

		@Override
		protected void updateIONameStyle(IO io) {
			DrawableIOComponent<? extends Shape>.DrawingIo di = drawingIOs.get(io);
			Text drawingIOName = di.text;
			Circle drawingIO = di.circle;
			int marge = (int) (3 * scale + 0.5);
			int maxWidthAvailable = (int) (core.getWidth() - ioRoundRadius * scale - marge);
			drawingIOName.setFont(new Font(getFontSize(scale, drawingIOName.getText(), maxWidthAvailable, (int) (getGapBetweenIO() * scale), 8, 12)[1]));
			Bounds textBounds = drawingIOName.getLayoutBounds();
			if (io instanceof Input)
				drawingIOName.setTranslateX(drawingIO.getCenterX() + drawingIO.getRadius() + marge);
			else
				drawingIOName.setTranslateX(
						drawingIO.getCenterX() + (textBounds.getWidth() <= maxWidthAvailable ? -(int) (textBounds.getWidth() + 0.5) - drawingIO.getRadius() - marge : marge - core.getWidth()));
			if (textBounds.getWidth() > maxWidthAvailable) {
				drawingIOName.setClip(new Rectangle(0, -textBounds.getHeight(), maxWidthAvailable, textBounds.getHeight()));
				double end = maxWidthAvailable / textBounds.getWidth();
				Stop[] _stops = new Stop[] { new Stop(0, Color.BLACK), new Stop(0.9 * end, Color.BLACK), new Stop(1 * end, Color.TRANSPARENT) };
				drawingIOName.setFill(new LinearGradient(0, 0, 1, 0, true, CycleMethod.NO_CYCLE, _stops));
			} else {
				drawingIOName.setClip(null);
				drawingIOName.setFill(ioTextColor);
			}
			drawingIOName.setTranslateY((int) (drawingIO.getCenterY() + drawingIOName.getBaselineOffset() / 2));
		}

		@Override
		public void updateIOStruct(IoDescriptor[] ioDescriptors) {
			updateBlockCoreSize();
			super.updateIOStruct(ioDescriptors);
		}

		@Override
		public void updatePosition() {
			Rectangle2D rectangle = block.getRectangle();
			setTranslateX((int) (rectangle.getMinX() * scale + 0.5));
			setTranslateY((int) (rectangle.getMinY() * scale + 0.5));
			if (canvasInfo != null)
				canvasInfo.repaint();
		}

		@Override
		public String toString() {
			return getClass().getSimpleName() + " of " + block;
		}
	}

	abstract class DrawableDiagramIO<T extends IO> extends DrawableIOComponent<Polygon> implements LocalisedObject {
		protected final T io;

		public DrawableDiagramIO(T io, double scale, IoDescriptor[] ioDescriptors) {
			super(scale);
			this.io = io;
			core = new Polygon();
			updateBlockCoreSize();
			core.setFill(componentStaticIO);
			initBlockCore(ioDescriptors);
			core.setOnMouseClicked(e -> e.consume());
			getChildren().add(core);
			for (DrawableIOComponent<? extends Shape>.DrawingIo di : drawingIOs.values()) {
				getChildren().add(di.circle);
				getChildren().add(di.text);
			}
		}

		@Override
		public Point2D getPosition() {
			return io.getPosition();
		}

		@Override
		protected LocalisedObject getSeletecion() {
			return this;
		}

		@Override
		protected void initIOPos(IoDescriptor[] ioDescriptors) {}

		protected void nameChange(IO io) {
			drawingIOs.get(io).text.setText(io.getName());
		}

		@Override
		public void populateIO(IoDescriptor[] ioDescriptors) {
			initIOPos(ioDescriptors);
			createIO(ioDescriptors[0]);
			Text ioText = drawingIOs.get(io).text;
			ioText.setMouseTransparent(false);
			ioText.setOnMouseClicked(e -> {
				if (e.getClickCount() == 2 && e.getButton() == MouseButton.PRIMARY) {
					Text text = drawingIOs.get(io).text;
					getChildren().remove(text);
					TextField tf = new TextField(io.getName());
					tf.setFont(text.getFont());
					tf.setBackground(new Background(new BackgroundFill(null, null, null)));
					tf.setStyle("-fx-text-fill: black;");
					tf.setPadding(new Insets(0));
					tf.focusedProperty().addListener((ov, oldValue, newValue) -> {
						if (newValue)
							tf.selectAll();
						else {
							getChildren().remove(tf);
							String newName = tf.getText();
							FlowDiagram fd = (FlowDiagram) getDrawableElement();
							boolean isValid = true;
							for (FlowDiagramInput fdInput : fd.getInputs()) {
								if (fdInput.getName().equals(newName))
									isValid = false;
								break;
							}
							if (isValid)
								for (FlowDiagramOutput fdOutput : fd.getOutputs())
									if (fdOutput.getName().equals(newName)) {
										isValid = false;
										break;
									}
							if (isValid)
								io.setName(newName);
							else
								showMessage("The name: " + newName + " is already used by an other input or output", true);
							getChildren().add(text);
						}
					});
					tf.setOnKeyPressed(e1 -> {
						if (e1.getCode() == KeyCode.ENTER)
							FxUtils.traverse();
					});
					tf.setTranslateX(text.getTranslateX());
					tf.setTranslateY(text.getTranslateY() - text.getLayoutBounds().getHeight() + 1);
					getChildren().add(tf);
					tf.requestFocus();
				}
			});
		}

		@Override
		public void resetCoreStroke() {
			core.setStroke(componentBorder);
		}

		@Override
		public void setIOPos(IO io, Circle drawingIO) {
			drawingIO.setCenterX(0);
			drawingIO.setCenterY(0);
		}

		@Override
		public void setPosition(double x, double y) {
			io.setPosition(new Point2D(x, y));
		}

		public void updateDrawableStrokeWithMissedData() {
			core.setStroke(Color.BLACK);
		}

		public void updateIOInfo(String name, Class<?> _class) {
			Tooltip.install(drawingIOs.get(io).circle, new Tooltip(getIOToolTipInfo(name, _class, io.getError())));
		}

		@Override
		protected void updateIONameStyle(IO io) {
			DrawableIOComponent<? extends Shape>.DrawingIo di = drawingIOs.get(io);
			Text drawingIOName = di.text;
			Circle drawingIO = di.circle;
			if (io instanceof Input) {
				drawingIOName.setTranslateX(drawingIO.getCenterX() + (ioRoundRadius + 1) * scale);
				drawingIOName.setTranslateY((int) (drawingIO.getCenterY() + (10 + 1) * scale + drawingIOName.getBaselineOffset()));
			} else {
				drawingIOName.setTranslateX(drawingIO.getCenterX() - (ioRoundRadius + 1 + 20) * scale);
				drawingIOName.setTranslateY((int) (drawingIO.getCenterY() - (10 + 1) * scale));
			}
		}

		@Override
		public void updatePosition() {
			Point2D pos = io.getPosition();
			setTranslateX((int) (pos.getX() * scale + 0.5));
			setTranslateY((int) (pos.getY() * scale + 0.5));
			if (canvasInfo != null)
				canvasInfo.repaint();
		}
	}

	class DrawableInput extends DrawableDiagramIO<FlowDiagramInput> implements LocalisedObject {

		public DrawableInput(FlowDiagramInput input, double scale, IoDescriptor[] ioDescriptors) {
			super(input, scale, ioDescriptors);
			drawingInput.put(input, this);
		}

		@Override
		protected void updateBlockCoreSize() {
			core.getPoints().setAll((ioRoundRadius + 1 + 20) * scale, 0.0, (ioRoundRadius + 1) * scale, -10.0 * scale, (ioRoundRadius + 1) * scale, 10.0 * scale);
		}

		@Override
		protected void updateDrawingLinksPosition() {
			for (LinkDescriptor ld : drawingLinks.keySet())
				if (ld.input.io == io)
					setPosition(io, drawingLinks.get(ld));
		}
	}

	abstract class DrawableIOComponent<T extends Shape> extends Group {
		protected T core;
		protected LinkedHashMap<IO, DrawableIOComponent<? extends Shape>.DrawingIo> drawingIOs = new LinkedHashMap<>();
		protected double scale;
		private CubicCurve tempLinkCurve;
		private InvalidationListener tempLinkFocusListener;
		private Circle pickIO;
		private Point2D mousePointInScene;

		class DrawingIo {
			public Circle circle;
			public Text text;

			public DrawingIo(Circle circle, Text text) {
				this.circle = circle;
				this.text = text;
				this.circle.setViewOrder(-3);
				this.text.setViewOrder(-3);
			}
		}

		public DrawableIOComponent(double scale) {
			this.scale = scale;
		}

		private void clearTempLink() {
			DiagramDrawer.this.focusedProperty().removeListener(tempLinkFocusListener);
			tempLinkFocusListener = null;
			tempLink = null;
			DiagramDrawer.this.drawingElementGroup.getChildren().remove(tempLinkCurve);
			clearSelectedElements();
			updateLinkIO();
			mousePointInScene = null;
			tempLinkCurve = null;
		}

		protected void createIO(IoDescriptor ioDesc) {
			Circle drawableIO = new Circle();
			drawableIO.setStroke(componentBorder);
			drawableIO.setStrokeType(StrokeType.OUTSIDE);
			drawableIO.setFill(ioDesc.type == IoDescriptor.PROPERTY ? componentPropertyIO
					: ioDesc.type == IoDescriptor.STATIC ? componentStaticIO : ioDesc.type == IoDescriptor.DYNAMIC ? componentDynamicIO : componentVarArgsIO);
			Text drawableIOName = new Text(ioDesc.name);
			// drawableIOName.setMouseTransparent(true);
			drawingIOs.put(ioDesc.io, new DrawingIo(drawableIO, drawableIOName));
			drawableIO.setVisible(filterIO);
			drawableIOName.setVisible(filterIOName);
			updateIO(ioDesc.io);
			updateIONameStyle(ioDesc.io);
			Tooltip toolTip = new Tooltip(getIOToolTipInfo(ioDesc.name, ioDesc.dataType, ioDesc.io.getError()));
			Tooltip.install(drawableIO, toolTip);
			drawableIO.setOnContextMenuRequested(e -> {
				if (ioDesc.type == IoDescriptor.PROPERTY && (ioDesc.io instanceof BlockInput || ioDesc.io instanceof BlockOutput)) {
					if (contextMenu != null && contextMenu.isShowing()) {
						contextMenu.setAnchorX(e.getScreenX());
						contextMenu.setAnchorY(e.getScreenY());
					} else {
						CheckMenuItem menuProperties = new CheckMenuItem("Remove");
						menuProperties.setAccelerator(new KeyCodeCombination(KeyCode.DELETE));
						menuProperties.setOnAction(e1 -> {
							Block block = ioDesc.io instanceof BlockInput ? ((BlockInput) ioDesc.io).getBlock() : ((BlockOutput) ioDesc.io).getBlock();
							Object blockOperator = block.operator;
							if (blockOperator instanceof EvolvedOperator)
								((EvolvedOperator) blockOperator).setPropertyAsInput(ioDesc.name, false);
							else
								block.setPropertyAsInput(ioDesc.name, false);
							selectedElements.remove(ioDesc.io);
						});
						contextMenu = new ContextMenu(menuProperties);
						contextMenu.show(this, e.getScreenX(), e.getScreenY());
					}
				}
				e.consume();
			});
			drawableIO.setCursor(Cursor.HAND);
			drawableIOName.setCursor(Cursor.HAND);
			ioDesc.io.addErrorChangeListener(() -> {
				toolTip.setText(getIOToolTipInfo(ioDesc.name, ioDesc.dataType, ioDesc.io.getError()));
				resetStroke(ioDesc.io);
			});
			drawableIO.setOnMousePressed(e -> {
				if (e.getButton() == MouseButton.PRIMARY) {
					if (!selectedElements.contains(ioDesc.io)) {
						if (!e.isControlDown())
							clearSelectedElements();
						selectedElements.add(ioDesc.io);
						drawableIO.setStroke(selectionColor);
					}
					e.consume();
				}
			});
			drawableIOName.setOnMousePressed(drawableIO.getOnMousePressed());
			drawableIO.setOnMouseDragged(e -> {
				if (tempLink == null) {
					if (e.getButton() == MouseButton.PRIMARY) {
						tempLinkCurve = new CubicCurve();
						tempLink = ioDesc.io instanceof Input ? new Link((Input) ioDesc.io, null) : new Link(null, (Output) ioDesc.io);
						tempLinkFocusListener = e1 -> {
							if (!DiagramDrawer.this.isFocused() && tempLink != null)
								clearTempLink();
						};
						DiagramDrawer.this.focusedProperty().addListener(tempLinkFocusListener);
						setPosition(ioDesc.io, tempLinkCurve);
						drawingElementGroup.getChildren().add((filterGrid ? 1 : 0) + (filterBorder ? 1 : 0), tempLinkCurve);
						tempLinkCurve.setStroke(selectionColor);
						tempLinkCurve.setFill(null);
						updateLinkIO();
					} else
						return;
				}
				Point2D p = new Point2D(e.getX(), e.getY());
				Node src = (Node) e.getSource();
				mousePointInScene = src.localToScene(p);
				updateTempLinkWithMousePos(src.getParent().localToParent(src.localToParent(p)));
			});
			drawableIOName.setOnMouseDragged(drawableIO.getOnMouseDragged());
			drawableIO.setOnMouseReleased(e -> {
				if (e.getButton() == MouseButton.PRIMARY) {
					if (tempLink != null) {
						Object element = null;
						if (pickIO != null)
							pickIOFound: for (int i = 0; i < 3; i++) {
								Collection<? extends DrawableIOComponent<? extends Shape>> l;
								if (i == 0)
									l = drawingBlocks.values();
								else if (i == 1)
									l = drawingInput.values();
								else
									l = drawingOutput.values();
								for (DrawableIOComponent<? extends Shape> db : l) {
									HashMap<IO, DrawableIOComponent<? extends Shape>.DrawingIo> dio = db.drawingIOs;
									for (IO _io : dio.keySet())
										if (pickIO == dio.get(_io).circle) {
											element = _io;
											break pickIOFound;
										}
								}
							}
						if (tempLink.getInput() == null && element instanceof Input) {
							if (isAssignable((Input) element, tempLink.getOutput()))
								tempLink = new Link((Input) element, tempLink.getOutput());
						} else if (tempLink.getOutput() == null && element instanceof Output) {
							Output output = (Output) element;
							if (isAssignable(tempLink.getInput(), output))
								tempLink = new Link(tempLink.getInput(), output);
						}
						if (tempLink.getInput() != null && tempLink.getOutput() != null)
							tempLink.getInput().setLink(tempLink);
						clearTempLink();
					}
					e.consume();
				}
			});
			drawableIOName.setOnMouseReleased(drawableIO.getOnMouseReleased());
			drawableIO.setOnMouseClicked(e -> {
				if (e.getClickCount() == 2 && e.getButton() == MouseButton.PRIMARY) {
					Stage dia = visibleProperties.get(ioDesc.io);
					if (dia != null) {
						dia.requestFocus();
						return;
					}
					Stage dialog = new Stage(StageStyle.DECORATED);
					dialog.initOwner(getScene().getWindow());
					dialog.setTitle(ioDesc.io.getName());
					final BeanManager bm = new BeanManager(ioDesc.io, BeanManager.DEFAULTDIR);
					GridPane editor = bm.getEditor();
					editor.widthProperty().addListener(e1 -> dialog.sizeToScene());
					editor.heightProperty().addListener(e1 -> dialog.sizeToScene());
					BorderPane bp = new BorderPane(bm.getEditor());
					Button okButton = new Button("Ok");
					BorderPane.setAlignment(okButton, Pos.CENTER);
					okButton.setOnAction(e1 -> {
						bm.saveIfChanged();
						visibleProperties.remove(ioDesc.io);
						dialog.close();
					});
					bp.setBottom(okButton);
					dialog.setScene(new Scene(bp, -1, -1));
					dialog.sizeToScene();
					dialog.setOnCloseRequest(e1 -> {
						bm.saveIfChanged();
						visibleProperties.remove(ioDesc.io);
					});
					dialog.show();
					visibleProperties.put(ioDesc.io, dialog);
					// bm.addSizeChangeListener(() -> dialog.sizeToScene());
				}
				e.consume();
			});
			drawableIOName.setOnMouseClicked(drawableIO.getOnMouseClicked());
		}

		private void updateTempLinkWithMousePos(Point2D mousePosInTheaterPane) {
			Object io = tempLink.getInput() != null ? tempLink.getInput() : tempLink.getOutput();
			pickIO = null;
			Point2D pickPos = null;
			pickIOFound: for (int i = 0; i < 3; i++) {
				Collection<? extends DrawableIOComponent<?>> l;
				if (i == 0)
					l = drawingBlocks.values();
				else if (i == 1)
					l = drawingInput.values();
				else
					l = drawingOutput.values();
				for (DrawableIOComponent<?> db : l)
					for (IO _io : db.drawingIOs.keySet()) {
						DrawableIOComponent<?>.DrawingIo di = db.drawingIOs.get(_io);
						Circle circle = di.circle;
						Point2D pCircle = db.localToParent(circle.getCenterX(), circle.getCenterY());
						Text text = di.text;
						Point2D ori = db.localToParent(text.getX() + text.getTranslateX(), text.getY() + text.getTranslateY());
						if (pCircle.distance(mousePosInTheaterPane) < circle.getRadius()
								|| new Rectangle2D(ori.getX(), ori.getY() - text.getLayoutBounds().getHeight(), text.getLayoutBounds().getWidth(), text.getLayoutBounds().getHeight())
										.contains(mousePosInTheaterPane)) {
							if (circle.getStroke() != Color.BLACK) {
								pickIO = circle;
								pickPos = pCircle;
							}
							break pickIOFound;
						}
					}
			}
			double x;
			double y;
			if (pickPos != null) {
				tempLinkCurve.setStroke(Color.WHITE);
				x = pickPos.getX();
				y = pickPos.getY();
			} else {
				tempLinkCurve.setStroke(selectionColor);
				x = mousePosInTheaterPane.getX();
				y = mousePosInTheaterPane.getY();
			}
			if (io instanceof Input) {
				tempLinkCurve.setEndX(x);
				tempLinkCurve.setEndY(y);
			} else {
				tempLinkCurve.setStartX(x);
				tempLinkCurve.setStartY(y);
			}
			updateCurveControlPoint(tempLinkCurve);
		}

		protected abstract LocalisedObject getSeletecion();

		protected void initBlockCore(IoDescriptor[] ioDescriptors) {
			updateBlockCoreSize();
			core.setStrokeType(StrokeType.OUTSIDE);
			updatePosition();
			// On met les IO
			populateIO(ioDescriptors);
			if (filterShadow)
				core.setEffect(new DropShadow(shadowRadius, shadow));
			InvalidationListener drawingLinksPositionUpdate = e -> updateDrawingLinksPosition();
			translateXProperty().addListener(drawingLinksPositionUpdate);
			translateYProperty().addListener(drawingLinksPositionUpdate);
			resetCoreStroke();

			core.setOnMousePressed(e -> {
				if (e.isConsumed())
					return;
				if (e.getButton() == MouseButton.PRIMARY) {
					if (core.getStroke() != selectionColor)
						core.setStroke(selectionColor);
					selection = getSeletecion();
				}
			});
		}

		protected abstract void initIOPos(IoDescriptor[] ioDescriptors); // dépend de la géométrie

		public abstract void populateIO(IoDescriptor[] ioDescriptors); // Of course

		public abstract void resetCoreStroke();

		public abstract void setIOPos(IO io, Circle drawingIO);

		public void setPosition(IO io, CubicCurve cubic) {
			Circle circle = null;
			try {
				circle = drawingIOs.get(io).circle;
			} catch (NullPointerException e) {
				System.err.println("Cannot get drawingIO from: " + io);
				return;
			}
			if (io instanceof Input) {
				Point2D newPos = this.localToParent(new Point2D(circle.getCenterX(), circle.getCenterY()));
				cubic.setStartX((int) newPos.getX());
				cubic.setStartY((int) newPos.getY());
			} else {
				Point2D newPos = this.localToParent(new Point2D(circle.getCenterX(), circle.getCenterY()));
				cubic.setEndX((int) newPos.getX());
				cubic.setEndY((int) newPos.getY());
			}
		}

		public Runnable setScale(double scale) {
			this.scale = scale;
			updateBlockCoreSize();
			updateIOLook();
			updatePosition();
			if (tempLinkCurve != null && tempLink != null)
				return () -> {
					IO io = tempLink.getInput() != null ? tempLink.getInput() : tempLink.getOutput();
					setPosition(io, tempLinkCurve);
					updateTempLinkWithMousePos(localToParent(sceneToLocal(mousePointInScene)));
				};
			return null;
		}

		public void resetStroke(IO io) {
			drawingIOs.get(io).circle.setStroke(io.getError() == null ? componentBorder : Color.RED);
		}

		protected abstract void updateBlockCoreSize();

		protected abstract void updateDrawingLinksPosition(); // dépend de la config IO

		private void updateIO(IO io) {
			Circle drawingIO = drawingIOs.get(io).circle;
			setIOPos(io, drawingIO);
			drawingIO.setRadius((int) (ioRoundRadius * scale + 0.5));
		}

		protected void updateIOLook() { // Pas Thread safe, il faut le link
			for (IO io : drawingIOs.keySet()) {
				updateIO(io);
				updateIONameStyle(io);
			}
			updateDrawingLinksPosition();
		}

		protected abstract void updateIONameStyle(IO io); // dépend de la géométrie

		public void updateIOStroke() {
			Output output = tempLink == null ? null : tempLink.getOutput();
			Input input = tempLink == null ? null : tempLink.getInput();
			for (IO io : drawingIOs.keySet()) {
				Input in;
				Output out;
				boolean oio;
				if (io instanceof Input) {
					in = (Input) io;
					out = output;
					oio = output == null;
				} else {
					in = input;
					out = (Output) io;
					oio = input == null;
				}
				drawingIOs.get(io).circle
						.setStroke(selectedElements.contains(io) ? selectionColor : tempLink == null ? componentBorder : oio ? Color.BLACK : isAssignable(in, out) ? Color.WHITE : Color.BLACK);
			}
		}

		public void updateIOStruct(IoDescriptor[] ioDescriptors) {
			for (DrawableIOComponent<? extends Shape>.DrawingIo di : drawingIOs.values()) {
				getChildren().remove(di.circle);
				getChildren().remove(di.text);
			}
			drawingIOs.clear();
			populateIO(ioDescriptors);
			for (DrawableIOComponent<? extends Shape>.DrawingIo di : drawingIOs.values()) {
				getChildren().add(di.circle);
				getChildren().add(di.text);
			}
			updateDrawingLinksPosition();
		}

		public abstract void updatePosition();
	}

	class DrawableOutput extends DrawableDiagramIO<FlowDiagramOutput> implements LocalisedObject {

		public DrawableOutput(FlowDiagramOutput output, double scale, IoDescriptor[] ioDescriptors) {
			super(output, scale, ioDescriptors);
			drawingOutput.put(output, this);
		}

		@Override
		protected void initIOPos(IoDescriptor[] ioDescriptors) {}

		@Override
		protected void updateBlockCoreSize() {
			core.getPoints().setAll(-(ioRoundRadius + 1) * scale, 0.0, -(ioRoundRadius + 20 + 1) * scale, -10.0 * scale, -(ioRoundRadius + 20 + 1) * scale, 10.0 * scale);
		}

		@Override
		protected void updateDrawingLinksPosition() {
			for (LinkDescriptor linkDesc : drawingLinks.keySet())
				if (linkDesc.output.io == io)
					setPosition(io, drawingLinks.get(linkDesc));
		}
	}

	// @SuppressWarnings("unused")
	// private static Node pick(Node node, double sceneX, double sceneY) {
	// Point2D p = node.sceneToLocal(sceneX, sceneY, true);
	// if (!node.contains(p))
	// return null;
	// if (node instanceof Parent) {
	// Node bestMatchingChild = null;
	// List<Node> children = ((Parent) node).getChildrenUnmodifiable();
	// for (int i = children.size() - 1; i >= 0; i--) {
	// Node child = children.get(i);
	// p = child.sceneToLocal(sceneX, sceneY, true /* rootScene */);
	// if (child.isVisible() && !child.isMouseTransparent() && child.contains(p)) {
	// bestMatchingChild = child;
	// break;
	// }
	// }
	// if (bestMatchingChild != null)
	// return pick(bestMatchingChild, sceneX, sceneY);
	// }
	// return node;
	// }
}

class FlatteningPathIterator {
	public static final int WIND_EVEN_ODD = 0;
	public static final int WIND_NON_ZERO = 1;
	public static final int SEG_MOVETO = 0;
	public static final int SEG_LINETO = 1;
	public static final int SEG_QUADTO = 2;
	public static final int SEG_CUBICTO = 3;
	public static final int SEG_CLOSE = 4;
	static final int GROW_SIZE = 24;

	public static float ptSegDistSq(float x1, float y1, float x2, float y2, float px, float py) {
		x2 -= x1;
		y2 -= y1;
		px -= x1;
		py -= y1;
		float dotprod = px * x2 + py * y2;
		float projlenSq;
		if (dotprod <= 0f)
			projlenSq = 0f;
		else {
			px = x2 - px;
			py = y2 - py;
			dotprod = px * x2 + py * y2;
			if (dotprod <= 0f)
				projlenSq = 0f;
			else
				projlenSq = dotprod * dotprod / (x2 * x2 + y2 * y2);
		}
		float lenSq = px * px + py * py - projlenSq;
		if (lenSq < 0f)
			lenSq = 0f;
		return lenSq;
	}

	private static void subdivide(float src[], int srcoff, int leftoff) {
		float x1 = src[srcoff];
		float y1 = src[srcoff + 1];
		float ctrlx1 = src[srcoff + 2];
		float ctrly1 = src[srcoff + 3];
		float ctrlx2 = src[srcoff + 4];
		float ctrly2 = src[srcoff + 5];
		float x2 = src[srcoff + 6];
		float y2 = src[srcoff + 7];
		src[leftoff] = x1;
		src[leftoff + 1] = y1;
		src[srcoff + 6] = x2;
		src[srcoff + 7] = y2;
		x1 = (x1 + ctrlx1) / 2f;
		y1 = (y1 + ctrly1) / 2f;
		x2 = (x2 + ctrlx2) / 2f;
		y2 = (y2 + ctrly2) / 2f;
		float centerx = (ctrlx1 + ctrlx2) / 2f;
		float centery = (ctrly1 + ctrly2) / 2f;
		ctrlx1 = (x1 + centerx) / 2f;
		ctrly1 = (y1 + centery) / 2f;
		ctrlx2 = (x2 + centerx) / 2f;
		ctrly2 = (y2 + centery) / 2f;
		centerx = (ctrlx1 + ctrlx2) / 2f;
		centery = (ctrly1 + ctrly2) / 2f;
		src[leftoff + 2] = x1;
		src[leftoff + 3] = y1;
		src[leftoff + 4] = ctrlx1;
		src[leftoff + 5] = ctrly1;
		src[leftoff + 6] = centerx;
		src[leftoff + 7] = centery;
		src[srcoff] = centerx;
		src[srcoff + 1] = centery;
		src[srcoff + 2] = ctrlx2;
		src[srcoff + 3] = ctrly2;
		src[srcoff + 4] = x2;
		src[srcoff + 5] = y2;
	}

	CubicIterator src;
	float squareflat;
	int limit;
	volatile float hold[] = new float[14];
	float curx, cury;
	float movx, movy;
	int holdType;
	int holdEnd;
	int holdIndex;
	int levels[];

	int levelIndex;

	boolean done;

	public FlatteningPathIterator(CubicIterator src, float flatness) {
		this(src, flatness, 10);
	}

	public FlatteningPathIterator(CubicIterator src, float flatness, int limit) {
		if (flatness < 0f)
			throw new IllegalArgumentException("flatness must be >= 0");
		if (limit < 0)
			throw new IllegalArgumentException("limit must be >= 0");
		this.src = src;
		this.squareflat = flatness * flatness;
		this.limit = limit;
		this.levels = new int[limit + 1];
		// prime the first path segment
		next(false);
	}

	public int currentSegment(float[] coords) {
		if (isDone())
			throw new NoSuchElementException("flattening iterator out of bounds");
		int type = holdType;
		if (type != SEG_CLOSE) {
			coords[0] = hold[holdIndex];
			coords[1] = hold[holdIndex + 1];
			if (type != SEG_MOVETO)
				type = SEG_LINETO;
		}
		return type;
	}

	void ensureHoldCapacity(int want) {
		if (holdIndex - want < 0) {
			int have = hold.length - holdIndex;
			int newsize = hold.length + GROW_SIZE;
			float newhold[] = new float[newsize];
			System.arraycopy(hold, holdIndex, newhold, holdIndex + GROW_SIZE, have);
			hold = newhold;
			holdIndex += GROW_SIZE;
			holdEnd += GROW_SIZE;
		}
	}

	public float getFlatness() {
		return (float) Math.sqrt(squareflat);
	}

	private static float getFlatnessSq(float[] coords, int offset) {
		return Math.max(ptSegDistSq(coords[offset], coords[offset + 1], coords[offset + 6], coords[offset + 7], coords[offset + 2], coords[offset + 3]),
				ptSegDistSq(coords[offset], coords[offset + 1], coords[offset + 6], coords[offset + 7], coords[offset + 4], coords[offset + 5]));
	}

	public int getRecursionLimit() {
		return limit;
	}

	public int getWindingRule() {
		return src.getWindingRule();
	}

	public boolean isDone() {
		return done;
	}

	public void next() {
		next(true);
	}

	private void next(boolean doNext) {
		int level;

		if (holdIndex >= holdEnd) {
			if (doNext)
				src.next();
			if (src.isDone()) {
				done = true;
				return;
			}
			holdType = src.currentSegment(hold);
			levelIndex = 0;
			levels[0] = 0;
		}

		switch (holdType) {
		case SEG_MOVETO:
		case SEG_LINETO:
			curx = hold[0];
			cury = hold[1];
			if (holdType == SEG_MOVETO) {
				movx = curx;
				movy = cury;
			}
			holdIndex = 0;
			holdEnd = 0;
			break;
		case SEG_CLOSE:
			curx = movx;
			cury = movy;
			holdIndex = 0;
			holdEnd = 0;
			break;
		case SEG_QUADTO:
			if (holdIndex >= holdEnd) {
				holdIndex = hold.length - 6;
				holdEnd = hold.length - 2;
				hold[holdIndex] = curx;
				hold[holdIndex + 1] = cury;
				hold[holdIndex + 2] = hold[0];
				hold[holdIndex + 3] = hold[1];
				hold[holdIndex + 4] = curx = hold[2];
				hold[holdIndex + 5] = cury = hold[3];
			}

			level = levels[levelIndex];
			while (level < limit) {
				if (getFlatnessSq(hold, holdIndex) < squareflat)
					break;
				ensureHoldCapacity(4);
				subdivide(hold, holdIndex, holdIndex - 4);
				holdIndex -= 4;
				level++;
				levels[levelIndex] = level;
				levelIndex++;
				levels[levelIndex] = level;
			}
			holdIndex += 4;
			levelIndex--;
			break;
		case SEG_CUBICTO:
			if (holdIndex >= holdEnd) {
				holdIndex = hold.length - 8;
				holdEnd = hold.length - 2;
				hold[holdIndex] = curx;
				hold[holdIndex + 1] = cury;
				hold[holdIndex + 2] = hold[0];
				hold[holdIndex + 3] = hold[1];
				hold[holdIndex + 4] = hold[2];
				hold[holdIndex + 5] = hold[3];
				hold[holdIndex + 6] = curx = hold[4];
				hold[holdIndex + 7] = cury = hold[5];
			}

			level = levels[levelIndex];
			while (level < limit) {
				if (getFlatnessSq(hold, holdIndex) < squareflat)
					break;
				ensureHoldCapacity(6);
				subdivide(hold, holdIndex, holdIndex - 6);
				holdIndex -= 6;
				level++;
				levels[levelIndex] = level;
				levelIndex++;
				levels[levelIndex] = level;
			}
			holdIndex += 6;
			levelIndex--;
			break;
		}
	}
}

class IoDescriptor {
	public static final int PROPERTY = 0;
	public static final int STATIC = 1;
	public static final int DYNAMIC = 2;
	public static final int VARARGS = 3;
	public final IO io;
	public final int index;
	public final int type;
	public final String name;
	public final Class<?> dataType;

	public IoDescriptor(IO io, int index, int type) {
		this.io = io;
		this.index = index;
		this.type = type;
		this.name = io.getName();
		this.dataType = io.getType();
	}

	@Override
	public boolean equals(Object obj) {
		return io == ((IoDescriptor) obj).io;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(io);
	}

	@Override
	public String toString() {
		String errorMessage = io.getError();
		return io.getClass().getSimpleName() + " index: " + index + " type: " + type + " name: " + name + " dataType: " + dataType + (errorMessage == null ? "" : "error: " + errorMessage);
	}
}

class LinkDescriptor {
	public final Link link;
	public final Block inputBlock;
	public final Block outputBlock;
	public final IoDescriptor input;
	public final IoDescriptor output;

	public LinkDescriptor(Link link) {
		this.link = link;
		Input in = link.getInput();
		this.input = new IoDescriptor(in, 0, 1);
		this.inputBlock = in instanceof BlockIO ? ((BlockIO) in).getBlock() : null;
		Output out = link.getOutput();
		this.output = new IoDescriptor(out, 0, 1);
		this.outputBlock = out instanceof BlockIO ? ((BlockIO) out).getBlock() : null;
	}

	@Override
	public boolean equals(Object obj) {
		return obj instanceof LinkDescriptor ? link == ((LinkDescriptor) obj).link : false;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(link);
	}

	@Override
	public String toString() {
		return "input: " + input + " output: " + output;
	}
}

class CubicIterator {
	public static final int WIND_EVEN_ODD = 0;
	public static final int WIND_NON_ZERO = 1;
	public static final int SEG_MOVETO = 0;
	public static final int SEG_LINETO = 1;
	public static final int SEG_QUADTO = 2;
	public static final int SEG_CUBICTO = 3;
	public static final int SEG_CLOSE = 4;
	CubicCurve cubicCurve;
	int index;

	CubicIterator(CubicCurve cubicCurve) {
		this.cubicCurve = cubicCurve;
	}

	public int currentSegment(double[] coords) {
		if (isDone())
			throw new NoSuchElementException("cubic iterator iterator out of bounds");
		int type;
		if (index == 0) {
			coords[0] = cubicCurve.getStartX();
			coords[1] = cubicCurve.getStartY();
			type = SEG_MOVETO;
		} else {
			coords[0] = cubicCurve.getControlX1();
			coords[1] = cubicCurve.getControlY1();
			coords[2] = cubicCurve.getControlX2();
			coords[3] = cubicCurve.getControlY2();
			coords[4] = cubicCurve.getEndX();
			coords[5] = cubicCurve.getEndY();
			type = SEG_CUBICTO;
		}
		return type;
	}

	public int currentSegment(float[] coords) {
		if (isDone())
			throw new NoSuchElementException("cubic iterator iterator out of bounds");
		int type;
		if (index == 0) {
			coords[0] = (float) cubicCurve.getStartX();
			coords[1] = (float) cubicCurve.getStartY();
			type = SEG_MOVETO;
		} else {
			coords[0] = (float) cubicCurve.getControlX1();
			coords[1] = (float) cubicCurve.getControlY1();
			coords[2] = (float) cubicCurve.getControlX2();
			coords[3] = (float) cubicCurve.getControlY2();
			coords[4] = (float) cubicCurve.getEndX();
			coords[5] = (float) cubicCurve.getEndY();
			type = SEG_CUBICTO;
		}
		return type;
	}

	public int getWindingRule() {
		return WIND_NON_ZERO;
	}

	public boolean isDone() {
		return index > 1;
	}

	public void next() {
		index++;
	}
}