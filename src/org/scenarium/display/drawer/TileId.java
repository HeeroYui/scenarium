/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.display.drawer;

public class TileId {
	public final int x;
	public final int y;
	public final int scale;

	public TileId(int x, int y, int scale) {
		this.x = x;
		this.y = y;
		this.scale = scale;
	}

	@Override
	public boolean equals(Object obj) {
		return ((TileId) obj).scale == scale && ((TileId) obj).x == x && ((TileId) obj).y == y;
	}

	@Override
	public int hashCode() {
		return Integer.hashCode(x) + Integer.hashCode(y) + Integer.hashCode(scale);
	}

	@Override
	public String toString() {
		return "x: " + x + "y: " + y + "scale: " + scale;
	}
}