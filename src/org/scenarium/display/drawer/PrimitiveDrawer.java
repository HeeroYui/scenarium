/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.display.drawer;

import org.beanmanager.editors.PropertyEditor;
import org.beanmanager.editors.PropertyEditorManager;
import org.scenarium.display.ScenariumContainer;
import org.scenarium.timescheduler.Scheduler;

import javafx.collections.ObservableList;
import javafx.geometry.Dimension2D;
import javafx.scene.Node;
import javafx.scene.layout.Region;

public class PrimitiveDrawer extends TheaterPanel {
	private PropertyEditor<?> editor;
	private Object oldDataElement;

	int cpt = 0;

	private Dimension2D dim;

	private static <T> void createEditor(T dataElement) {
		PropertyEditor<T> _editor = PropertyEditorManager.findEditorWithValue(dataElement, null);
		_editor.setValue(dataElement);
	}

	@Override
	public void fitDocument() {}

	@Override
	public Dimension2D getDimension() {
		if (editor == null)
			editor = PropertyEditorManager.findEditor(getDrawableElement().getClass(), "");
		Region ce = editor.getNoSelectionEditor();
		dim = new Dimension2D(ce.getPrefWidth(), ce.getPrefHeight());
		return dim;
	}

	public PropertyEditor<?> getEditor() {
		return editor;
	}

	@Override
	public String[] getStatusBarInfo() {
		return null;
	}

	@Override
	public void initialize(ScenariumContainer container, Scheduler scheduler, Object dataElement, boolean autoFitIfResize) {
		super.initialize(container, scheduler, dataElement, autoFitIfResize);
		createEditor(dataElement);
		Region editorComponenet = editor.getNoSelectionEditor();
		ObservableList<Node> children = getChildren();
		children.clear();
		children.add(editorComponenet);
		editorComponenet.prefWidthProperty().bind(widthProperty());
		editorComponenet.prefHeightProperty().bind(heightProperty());
	}

	@Override
	public void paint(Object dataElement) {
		if (dataElement != oldDataElement) {
			if (dataElement != editor.getValue()) {
				editor.setValueFromObj(dataElement);
				editor.updateGUI();
			}
			oldDataElement = dataElement;
		}
	}

	@Override
	protected boolean updateFilterWithPath(String[] filterPath, boolean value) {
		return false;
	}
}
