/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.display.drawer;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.swing.event.EventListenerList;

import org.beanmanager.BeanPropertiesInheritanceLimit;
import org.beanmanager.editors.PropertyInfo;
import org.beanmanager.editors.TransientProperty;
import org.beanmanager.struct.BooleanProperty;
import org.beanmanager.struct.TreeNode;
import org.beanmanager.struct.TreeRoot;
import org.beanmanager.tools.FxUtils;
import org.scenarium.display.AnimationTimerConsumer;
import org.scenarium.display.FilterChangeListener;
import org.scenarium.display.PaintListener;
import org.scenarium.display.ScenariumContainer;
import org.scenarium.display.StackableDrawer;
import org.scenarium.display.TheaterFilterPropertyChange;
import org.scenarium.display.TheaterFilterStructChange;
import org.scenarium.display.toolbarclass.Tool;
import org.scenarium.display.toolbarclass.ToolBoxItem;
import org.scenarium.operator.basic.Viewer;
import org.scenarium.struct.BufferedStrategy;
import org.scenarium.timescheduler.Scheduler;

import javafx.beans.value.ChangeListener;
import javafx.geometry.Dimension2D;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.Window;

@BeanPropertiesInheritanceLimit
public abstract class TheaterPanel extends Pane implements FilterChangeListener {
	@TransientProperty
	private Object drawableElement;
	private Object[] additionalDrawableElement;
	private ScenariumContainer container;
	protected Scheduler scheduler;
	protected int sWid; // largeur du scenario
	protected int sHei; // hauteur du scenario
	// protected final LinkedHashMap<String, Object> theaterFilter = new LinkedHashMap<>();
	@PropertyInfo(hidden = true)
	protected TreeRoot<BooleanProperty> theaterFilter;
	private final EventListenerList listeners = new EventListenerList();
	private Dimension2D oldDataSize;
	public float[] roi = new float[4];
	@TransientProperty
	private int theaterEditorSelection;

	public boolean ignoreRepaint = true;

	public boolean animated = false;

	public boolean needToBeRefresh;

	public void addPaintListener(PaintListener listener) {
		listeners.add(PaintListener.class, listener);
	}

	public void addTheaterFilterPropertyChangeListener(TheaterFilterPropertyChange listener) {
		listeners.add(TheaterFilterPropertyChange.class, listener);
	}

	public void addTheaterFilterStructChangeListener(TheaterFilterStructChange listener) {
		listeners.add(TheaterFilterStructChange.class, listener);
	}

	protected void changeFullScreen() {
		Window windows = getScene().getWindow();
		if (windows instanceof Stage) {
			Stage stage = (Stage) windows;
			stage.setFullScreen(!stage.isFullScreen());
		}
	}

	public void death() {
		container = null;
		scheduler = null;
		drawableElement = null;
		getChildren().clear();
		// widthProperty().removeListener(listener);
		// heightProperty().removeListener(listener);
	}

	@Override
	public void filterChanged(String[] path, boolean value) {
		TreeNode<BooleanProperty> node = theaterFilter;
		int i = 0;
		boolean sucess = false;
		while (i != path.length) {
			sucess = false;
			for (TreeNode<BooleanProperty> son : node.getChildren())
				if (son.getValue().name.equals(path[i])) {
					node = son;
					i++;
					sucess = true;
					break;
				}
			if (!sucess)
				break;
		}
		if (sucess)
			node.getValue().value = value;
	}

	private void firePaint() {
		for (PaintListener listener : listeners.getListeners(PaintListener.class))
			listener.paint(this, roi);
	}

	protected void fireTheaterFilterPropertyChanged(String[] path) {
		for (TheaterFilterPropertyChange listener : listeners.getListeners(TheaterFilterPropertyChange.class))
			listener.theaterFilterPropertyChanged(path);
	}

	protected void fireTheaterFilterStructChanged() {
		for (TheaterFilterStructChange listener : listeners.getListeners(TheaterFilterStructChange.class))
			listener.theaterFilterStructChanged();
	}

	public abstract void fitDocument();

	protected Object[] getAdditionalDrawableElement() {
		return additionalDrawableElement;
	}

	public abstract Dimension2D getDimension();

	public Object getDrawableElement() {
		return drawableElement;
	}

	public String geticonName() {// TODO faire les icones pour les autres
		return "/scenarium_icon_viewer.png";
	}

	public Dimension2D getPreferredSize() {
		return new Dimension2D(getWidth(), getHeight());
	}

	public abstract String[] getStatusBarInfo();

	public int getTheaterEditorSelection() {
		return theaterEditorSelection;
	}

	public TreeRoot<BooleanProperty> getTheaterFilter() {
		return theaterFilter;
	}

	protected TreeNode<BooleanProperty> getTheaterSon(TreeNode<BooleanProperty> father, String propName) {
		for (TreeNode<BooleanProperty> son : father.getChildren())
			if (son.getValue().name.equals(propName))
				return son;
		return null;
	}

	public ArrayList<ToolBoxItem> getToolBoxItems() {
		return new ArrayList<>();
	}

	public boolean hasTheaterFilterStructChanged() {
		return listeners.getListeners(TheaterFilterStructChange.class).length != 0;
	}

	public void initialize(ScenariumContainer container, Scheduler scheduler, Object drawableElement, boolean autoFitIfResize) {
		if (drawableElement == null)
			throw new IllegalArgumentException("drawableElement must be non null");
		setDrawableElement(drawableElement);
		this.scheduler = scheduler;
		this.container = container;
		setFocusTraversable(true);
		// Redraw canvas when size changes.
		ChangeListener<Number> es = (a, b, c) -> {
			if (autoFitIfResize)
				fitDocument();
			else
				repaint(false);
		};
		widthProperty().addListener(es);
		heightProperty().addListener(es);
		addEventHandler(MouseEvent.MOUSE_MOVED, e -> {
			if (container != null && container.isStatusBar())
				container.updateStatusBar(getStatusBarInfo());
		});
		addEventHandler(MouseEvent.MOUSE_PRESSED, e -> {
			if (!e.isConsumed())
				requestFocus();
		}); // Bug de focus sur rename block sinon -> Handler pour corriger, Bug de focus info chartdrawer si pas
		addEventFilter(ScrollEvent.SCROLL, e -> {
			if (e.getDeltaY() != 0)
				if (this.scheduler != null)
					if (e.isShiftDown()) {
						this.scheduler.jump(e.getDeltaY() < 0);
						e.consume();
					} else if (!e.isControlDown()) {
						this.scheduler.moveSpeed(e.getDeltaY() > 0);
						e.consume();
					}
		});
		addEventHandler(KeyEvent.KEY_PRESSED, e -> {
			if (e.isConsumed())
				return;
			KeyCode keyCode = e.getCode();
			switch (keyCode) {
			case SPACE:
				if (this.scheduler != null)
					if (e.isControlDown())
						new Thread(() -> TheaterPanel.this.scheduler.stop()).start();
					else if (this.scheduler.isRunning())
						this.scheduler.pause();
					else
						new Thread(() -> TheaterPanel.this.scheduler.start()).start();
				// this.scheduler.setRunning(e.isControlDown() ? false : !this.scheduler.isRunning(), e.isControlDown());
				break;
			case ADD:
				if (this.scheduler != null)
					this.scheduler.moveSpeed(true);
				break;
			case SUBTRACT:
				if (this.scheduler != null)
					this.scheduler.moveSpeed(false);
				break;
			case ENTER:
				if (e.isAltDown())
					changeFullScreen();
				break;
			default:
				return;
			}
			e.consume();
		});
		roi[2] = 50;
		roi[3] = 50;
		if (theaterFilter == null) {
			theaterFilter = new TreeRoot<>(BooleanProperty.class);
			populateTheaterFilter();
			updateTheaterFilter();
		}
	}

	public void initStruct(Viewer viewer) {}

	@Override
	public boolean isResizable() {
		return true;
	}

	protected boolean isRunning() {
		return scheduler != null && scheduler.isRunning();
	}

	protected boolean isStopped() {
		return scheduler == null || scheduler.isStopped();
	}

	protected abstract void paint(Object dataElement);

	public void paintImmediately(boolean autoFitSize) { // only for viewer
		needToBeRefresh = false;
		Dimension2D dim = getDimension();
		if (oldDataSize == null || !oldDataSize.equals(dim)) {
			oldDataSize = dim;
			if (autoFitSize)
				container.adaptSizeToDrawableElement();
			else
				fitDocument();
		}
		Object _drawableElement = drawableElement;
		if (_drawableElement == null)
			return;
		if (!(_drawableElement instanceof BufferedStrategy)) // ATTENTION rajout de synchro aurel qui correspont au cas sans flip sur la synchro data de update scenario
			synchronized (_drawableElement) {
				FxUtils.runLaterIfNeeded(() -> paint(_drawableElement));
			}
		else
			FxUtils.runLaterIfNeeded(() -> paint(_drawableElement));
		firePaint();
	}

	public void populateExclusiveFilter(ArrayList<List<String>> exclusiveFilters) {}

	protected void populateTheaterFilter() {}

	public void removePaintListener(PaintListener listener) {
		listeners.remove(PaintListener.class, listener);
	}

	public void removeTheaterFilterPropertyChangeListener(TheaterFilterPropertyChange listener) {
		listeners.remove(TheaterFilterPropertyChange.class, listener);
	}

	public void removeTheaterFilterStructChangeListener(TheaterFilterStructChange listener) {
		listeners.remove(TheaterFilterStructChange.class, listener);
	}

	public void repaint(boolean autoFitSize) {
		if (ignoreRepaint)
			return;
		if (animated) {
			needToBeRefresh = true;
			return;
		}
		paintImmediately(autoFitSize);
	}

	@Override
	public void resize(double width, double height) {
		super.resize(width, height);
	}

	protected void saveScenario() {
		container.saveScenario();
	}

	public void setAdditionalDrawableElement(Object[] additionalDrawableElement) {
		this.additionalDrawableElement = additionalDrawableElement;
	}

	public void setAnimated(boolean animated, AnimationTimerConsumer animationTimerConsumer) {}

	public void setDrawableElement(Object drawableElement) {
		this.drawableElement = drawableElement;
		Dimension2D dim = getDimension();
		if (dim != null) {
			sWid = (int) dim.getWidth();
			sHei = (int) dim.getHeight();
			if (sHei == -1)
				getDimension();
		} else {
			sWid = 800;
			sHei = 600;
		}
	}

	public void setScheduler(Scheduler scheduler) {
		this.scheduler = scheduler;
	}

	public void setTheaterEditorSelection(int theaterEditorSelection) {
		this.theaterEditorSelection = theaterEditorSelection;
	}

	public void setTheaterFilter(TreeRoot<BooleanProperty> theaterFilter) {
		if (this.theaterFilter == null) {
			this.theaterFilter = new TreeRoot<>(BooleanProperty.class);
			populateTheaterFilter();

			if (theaterFilter != null && theaterFilter.getChildren() != null) { // Chargement
				LinkedList<TreeNode<BooleanProperty>> todoListOri = new LinkedList<>();
				LinkedList<TreeNode<BooleanProperty>> todoListSave = new LinkedList<>();
				todoListOri.push(this.theaterFilter);
				todoListSave.push(theaterFilter);
				while (!todoListOri.isEmpty()) {
					TreeNode<BooleanProperty> oriNode = todoListOri.pop();
					TreeNode<BooleanProperty> saveNode = todoListSave.pop();
					if (oriNode.getChildren() == null && saveNode.getChildren() == null && saveNode.getValue() != null)
						oriNode.setValue(saveNode.getValue());
					else if (oriNode.getChildren() != null && saveNode.getChildren() != null)
						for (TreeNode<BooleanProperty> oriTreeNode : oriNode.getChildren()) {
							String oriSonName = oriTreeNode.getValue().name;
							TreeNode<BooleanProperty> correspondingSaveNode = null;
							for (TreeNode<BooleanProperty> seekNode : saveNode.getChildren())
								if (oriSonName.equals(seekNode.getValue().name)) {
									correspondingSaveNode = seekNode;
									break;
								}
							if (correspondingSaveNode != null) {
								todoListOri.push(oriTreeNode);
								todoListSave.push(correspondingSaveNode);
							}
						}
				}
			}
			updateTheaterFilter();
		}
	}

	public void showMessage(String message, boolean error) {
		container.showMessage(message, error);
	}

	protected void showTool(Class<? extends Tool> toolClass) {
		container.showTool(toolClass);
	}

	protected abstract boolean updateFilterWithPath(String[] filterPath, boolean value);

	final public void updateFilterWithPath(String[] path, boolean value, boolean updateTheater) {
		if (updateTheater)
			filterChanged(path, value);
		if (!updateFilterWithPath(path, value))
			return;
		fireTheaterFilterPropertyChanged(path);
	}

	public void updateTheaterFilter() {
		if (theaterFilter.getChildren() == null)
			return;
		LinkedList<TreeNode<BooleanProperty>> todoList = new LinkedList<>();
		LinkedList<ArrayList<String>> todoListPath = new LinkedList<>();
		todoList.add(theaterFilter);
		todoListPath.add(new ArrayList<>());
		while (!todoList.isEmpty()) {
			TreeNode<BooleanProperty> node = todoList.pop();
			ArrayList<String> nodePath = todoListPath.pop();
			for (TreeNode<BooleanProperty> treeNode : node.getChildren())
				if (treeNode.getChildren() != null) {
					ArrayList<String> path = new ArrayList<>(nodePath);
					path.add(treeNode.getValue().name);
					todoList.push(treeNode);
					todoListPath.push(path);
				} else {
					ArrayList<String> path = new ArrayList<>(nodePath);
					path.add(treeNode.getValue().name);
					updateFilterWithPath(path.toArray(new String[0]), treeNode.getValue().value, false);
				}
		}
	}

	// protected void updateToolView(int toolId, boolean isVisible) {
	// container.updateToolView(toolId, isVisible);
	// }

	public static boolean canAddInputToRenderer(Class<? extends StackableDrawer> type, Class<?>[] class1) {
		try {
			return type.getConstructor().newInstance().canAddInputToRenderer(class1);
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
			System.err.println("No default protected constructor for the class: " + type.getSimpleName() + "\nCannot check add input possibilities");
			return false;
		}
	}

	public static Object getImageFromData(Object scenarioData) {
		return null;
	}

	public static boolean isValidAdditionalInput(Class<? extends StackableDrawer> type, Class<?>[] inputsType, Class<?> additionalInput) {
		try {
			return additionalInput == null ? false : type.getConstructor().newInstance().isValidAdditionalInput(inputsType, additionalInput);
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
			System.err.println("No default protected constructor for the class: " + type.getSimpleName() + "\nCannot check input validities");
			return false;
		}
	}

	@Override
	public String toString() {
		return getClass().getSimpleName();
	}
}
