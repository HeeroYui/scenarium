/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.display;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Objects;

import javax.swing.event.EventListenerList;
import javax.vecmath.Point2i;

import org.beanmanager.BeanDesc;
import org.beanmanager.BeanManager;
import org.beanmanager.editors.container.BeanEditor;
import org.scenarium.Scenarium;
import org.scenarium.display.drawer.DrawerManager;
import org.scenarium.display.drawer.TheaterPanel;
import org.scenarium.display.toolBar.Console;
import org.scenarium.display.toolBar.DrawerProperties;
import org.scenarium.display.toolBar.Operators;
import org.scenarium.display.toolBar.PlayList;
import org.scenarium.display.toolBar.Player;
import org.scenarium.display.toolBar.StatusBar;
import org.scenarium.display.toolBar.TheaterFilter;
import org.scenarium.display.toolBar.Values;
import org.scenarium.display.toolBar.receditor.LogMerger;
import org.scenarium.display.toolbarclass.ExternalTool;
import org.scenarium.display.toolbarclass.InternalTool;
import org.scenarium.display.toolbarclass.Tool;
import org.scenarium.filemanager.DataLoader;
import org.scenarium.filemanager.scenariomanager.Scenario;
import org.scenarium.struct.BufferedStrategy;
import org.scenarium.timescheduler.Scheduler;
import org.scenarium.timescheduler.VisuableSchedulable;

import javafx.application.Platform;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.collections.ObservableList;
import javafx.geometry.Dimension2D;
import javafx.geometry.Orientation;
import javafx.geometry.Point2D;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.Window;

public class RenderPane implements VisuableSchedulableContainer, LoadToolBarListener {
	private static ArrayList<ToolBarInfo> toolBarsInfo = new ArrayList<>();
	private static final EventListenerList toolBarListeners = new EventListenerList();

	public static final String drawerPropertiesDir = Scenarium.getLocalDir() + File.separator + "DrawerProperties" + File.separator;
	static {
		toolBarsInfo.add(new ToolBarInfo(Console.class, KeyCode.C, true));
		toolBarsInfo.add(new ToolBarInfo(Values.class, KeyCode.V, false));
		// toolBarsInfo.add(new ToolBarInfoFx(TheaterEditorFx.class, KeyCode.E, false));
		toolBarsInfo.add(new ToolBarInfo(TheaterFilter.class, KeyCode.F, false));
		toolBarsInfo.add(new ToolBarInfo(Player.class, KeyCode.P, true));
		toolBarsInfo.add(new ToolBarInfo(StatusBar.class, KeyCode.S, false));
		toolBarsInfo.add(new ToolBarInfo(PlayList.class, KeyCode.L, true));
		toolBarsInfo.add(new ToolBarInfo(Operators.class, KeyCode.O, true));
		toolBarsInfo.add(new ToolBarInfo(DrawerProperties.class, KeyCode.D, false));
		toolBarsInfo.add(new ToolBarInfo(LogMerger.class, KeyCode.M, true));
		File dpd = new File(drawerPropertiesDir);
		if (!new File(drawerPropertiesDir).exists())
			dpd.mkdirs();
	}

	public static boolean addToolBarDescs(ToolBarInfo toolBarDesc) {
		Objects.requireNonNull(toolBarDesc);
		for (ToolBarInfo tbi : toolBarsInfo)
			if (tbi.keyCode == toolBarDesc.keyCode) {
				System.err.println("Cannot add toolBarDesc: " + toolBarDesc + " tbd has the same accelerator");
				return false;
			}
		toolBarsInfo.add(toolBarDesc);
		fireToolBarLoaded(toolBarDesc);
		return true;
	}
	
	public static void purgeToolBars(Module module) {
		for (Iterator<ToolBarInfo> iterator = toolBarsInfo.iterator(); iterator.hasNext();) {
			ToolBarInfo toolBarInfo = iterator.next();
			if (toolBarInfo._class.getModule().equals(module)) {
				iterator.remove();
				fireToolBarUnloaded(toolBarInfo);
			}
		}
	}
	
	public static void addLoadToolBarListener(LoadToolBarListener listener) {
		toolBarListeners.add(LoadToolBarListener.class, listener);
	}

	public static void removeLoadToolBarListener(LoadToolBarListener listener) {
		toolBarListeners.remove(LoadToolBarListener.class, listener);
	}
	
	private static void fireToolBarLoaded(ToolBarInfo toolBarDesc) {
		for (LoadToolBarListener listener : toolBarListeners.getListeners(LoadToolBarListener.class))
			listener.loaded(toolBarDesc);
	}

	private static void fireToolBarUnloaded(ToolBarInfo toolBarInfo) {
		for (LoadToolBarListener listener : toolBarListeners.getListeners(LoadToolBarListener.class))
			listener.unloaded(toolBarInfo);
	}

	public static TheaterPanel createTheaterPanel(Object de) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, NullPointerException {
		return DrawerManager.getRenderPanelClass(de.getClass()).getConstructor().newInstance();
	}

	public static ArrayList<ToolBarInfo> getToolBarDescs() {
		return toolBarsInfo;
	}
	
	private static ToolBarInfo getToolBarDesc(Class<? extends Tool> toolClass) {
		for (ToolBarInfo tbi : toolBarsInfo)
			if(tbi._class.equals(toolClass))
				return tbi;
		return null;
	}

	private final EventListenerList listeners = new EventListenerList();
	private final boolean isMainFrame;
	private DataLoader dataLoader;
	private TheaterPanel theaterPane;
	private StackPane stackPane;
	private HashMap<Class<? extends Tool>, Tool> displayedTools = new HashMap<>();
	private HashMap<InternalTool, Region> internalToolParents = new HashMap<>();
	private ScenariumContainer scenariumContainer;
	private BorderPane toolPane;
	private FlowPane centerPane;
	private AnimationTimerConsumer animationTimerConsumer;
	private HashSet<Class<? extends Tool>> excludedTools = new HashSet<>();

	public RenderPane(DataLoader dataLoader, Object de, ScenariumContainer scenariumContainer, TheaterPanel theaterPanel, boolean isMainFrame, boolean autoFitIfResize) {
		this(scenariumContainer.getScheduler(), de, scenariumContainer, theaterPanel, isMainFrame, autoFitIfResize);
		this.dataLoader = dataLoader;
	}

	public RenderPane(DataLoader dataLoader, ScenariumContainer scenariumContainer, boolean isMainFrame, boolean autoFitIfResize) {
		this(dataLoader, getScenarioData(dataLoader.getScenario()), scenariumContainer, null, isMainFrame, autoFitIfResize);
	}

	private static Object getScenarioData(Scenario scenario) {
		Object scenarioData = scenario.getScenarioData();
		if(scenarioData instanceof BufferedStrategy<?>)
			scenarioData = ((BufferedStrategy<?>) scenarioData).getElement();
		return scenarioData;
	}

	public RenderPane(Scheduler scheduler, Object de, ScenariumContainer scenariumContainer, TheaterPanel _theaterPanel, boolean isMainFrame, boolean autoFitIfResize) {
		// if (de == null)
		// de = dataLoader.getScenario().getScenarioData();
		this.scenariumContainer = scenariumContainer;
		if (_theaterPanel == null)
			try {
				theaterPane = createTheaterPanel(de);
			} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				// JOptionPane.showMessageDialog((Component) (scenariumContainer instanceof Component ? scenariumContainer : null), "Cannot create a renderPanel for the scenario", "RenderPanel error", JOptionPane.ERROR_MESSAGE);
				System.exit(-1);
			}
		else
			theaterPane = _theaterPanel;
		if (isMainFrame) { // rajout, les renderpane des viewer ne doivent pas charger les propriétées par default
			String tpName = theaterPane.getClass().getSimpleName();
			File drawerConfigFile = new File(drawerPropertiesDir + tpName + BeanDesc.SEPARATOR + tpName + ".txt");
			if (drawerConfigFile.exists() && !new BeanManager(theaterPane, drawerPropertiesDir).load(drawerConfigFile)) {
				BeanEditor.registerCreatedBeanAndCreateSubBean(theaterPane, tpName, drawerPropertiesDir);
				System.err.println("pas de fichier");
			}
		}
		theaterPane.initialize(scenariumContainer, scheduler, de, autoFitIfResize);// déplacement ici pour theaterfilter

		stackPane = new StackPane();
		theaterPane.prefWidthProperty().bind(stackPane.widthProperty());
		theaterPane.prefHeightProperty().bind(stackPane.heightProperty());
		stackPane.setMinSize(0, 0);
		adaptSizeToDrawableElement();
		stackPane.getChildren().add(theaterPane);

		toolPane = new BorderPane();
		toolPane.setFocusTraversable(false);
		toolPane.setPickOnBounds(false);

		centerPane = new FlowPane(Orientation.VERTICAL);
		centerPane.setMinSize(0, 0);
		centerPane.setFocusTraversable(false);
		centerPane.setPickOnBounds(false);
		centerPane.setVisible(false);
		toolPane.setCenter(centerPane);
		toolPane.setVisible(false);
		stackPane.getChildren().add(toolPane);

		if (!scenariumContainer.isManagingAccelerator()) {
			ArrayList<ToolBarInfo> toolBarInfos = new ArrayList<>();
			for (ToolBarInfo toolBarInfo : toolBarsInfo)
				if (isMainFrame || !toolBarInfo.onlyMainFrame)
					toolBarInfos.add(toolBarInfo);
			// if (!scenariumContainer.isManagingAccelerator()) {
			stackPane.addEventFilter(KeyEvent.KEY_PRESSED, e -> {
				if (e.isControlDown() || e.isAltDown() || e.isShiftDown())
					return;
				Object keyCode = e.getCode();
				for (int i = 0; i < toolBarInfos.size(); i++) {
					ToolBarInfo tbi = toolBarInfos.get(i);
					if (tbi.keyCode == keyCode) {
						updateToolView(tbi._class);
						e.consume();
						return;
					}
				}
			});
			// }
		}
		RenderPane.addLoadToolBarListener(this);
		this.isMainFrame = isMainFrame;
	}

	public void adaptSizeToDrawableElement() {
		Dimension2D dim = theaterPane.getDimension();
		double width = dim.getWidth();
		double height = dim.getHeight();
		if (stackPane.getWidth() == width && stackPane.getHeight() == height)
			return;
		Scene scene = theaterPane.getScene();
		if (scene != null) {
			Window windows = scene.getWindow();
			if (windows instanceof Stage) {
				Stage stage = (Stage) windows;

				stackPane.setPrefSize(1, 1); // Ajout sinon bug de taille si taille > screensize (voir FxTestSizeToScene)
				stage.sizeToScene();

				stackPane.setPrefSize(width, height);
				stage.sizeToScene();

				final boolean resizable = stage.isResizable();
				stage.setResizable(!resizable);
				stage.setResizable(resizable);
			}
		} else
			stackPane.setPrefSize(width, height); // ici avant
	}

	public void addPaneToRenderPane(Pane pane) {
		pane.setFocusTraversable(false);
		pane.setPickOnBounds(false);
		stackPane.getChildren().add(stackPane.getChildren().size(), pane);
	}

	public void addToolVisibleListener(final ToolVisibleListener listener) {
		listeners.add(ToolVisibleListener.class, listener);
	}

	public void close() {
		RenderPane.removeLoadToolBarListener(this);
		if (isMainFrame)
			saveDrawerProperties();
		for (Tool tb : displayedTools.values())
			if (tb != null)
				tb.dispose();
		theaterPane.death();
	}

	public boolean closeTool(Class<? extends Tool> _class) {
		Tool tool = displayedTools.get(_class);
		if(tool == null)
			return false;
		updateToolView(_class);
		return true;
	}

	public void excludeToolBar(Class<? extends Tool> toolToRemove) {
		excludedTools.add(toolToRemove);
	}

	private void fireToolVisibleChanged(Class<? extends Tool> toolBarClass, boolean visible) {
		for (ToolVisibleListener listener : listeners.getListeners(ToolVisibleListener.class))
			listener.toolVisibleChanged(toolBarClass, visible);
	}

	public Region getCenterPane() {
		return centerPane;
	}

	public ScenariumContainer getContainer() {
		return scenariumContainer;
	}

	public DataLoader getDataLoader() {
		return dataLoader;
	}

	public StackPane getPane() {
		return stackPane;// theaterPanel instanceof PointCloudDrawer || theaterPanel instanceof MeshDrawer ? theaterPanel : stackPane;
	}

	public Dimension2D getPreferredSize() {
		return theaterPane.getPreferredSize();
	}

	public TheaterPanel getTheaterPane() {
		return theaterPane;
	}

	public boolean isStatusBar() {
		return isDisplayed(StatusBar.class);
	}

	public boolean isDisplayed(Class<? extends Tool> toolClass) {
		return displayedTools.get(toolClass) != null;
	}

	public void loadingOperation(ReadOnlyDoubleProperty progressProperty) {
		StatusBar statusBar = (StatusBar) displayedTools.get(StatusBar.class);
		if (statusBar != null)
			Platform.runLater(() -> statusBar.update(progressProperty, false));
		else {
			progressProperty.addListener((a, b, c) -> {
				if (c.doubleValue() == 1)
					Platform.runLater(() -> closeTool(StatusBar.class));
			});
			Platform.runLater(() -> {
				if (progressProperty.getValue().doubleValue() == 1)
					return;
				updateToolView(StatusBar.class);
				((StatusBar) displayedTools.get(StatusBar.class)).update(progressProperty, true);
			});
		}
	}

	public void openTool(Class<? extends Tool> toolClass) {
		if (displayedTools.get(toolClass) != null)
			return;
		updateToolView(toolClass);
	}

	public void reload(boolean resize) {
		Object de = dataLoader.getScenario().getScenarioData();
		if (de == null)
			return;
		if (DrawerManager.isCompatibleRenderPanel(theaterPane, de.getClass())) {
			theaterPane.setDrawableElement(de);
			if (resize)
				/* if ( */adaptSizeToDrawableElement()/* ) */;
			// theaterPane.sizeToScene();
		} else {
			stackPane.getChildren().remove(theaterPane);
			theaterPane.prefWidthProperty().unbind();
			theaterPane.prefHeightProperty().unbind();
			theaterPane.death();
			theaterPane = null;
			try {
				theaterPane = createTheaterPanel(de);
			} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				// JOptionPane.showMessageDialog(theaterPanel.getParent(), "Cannot create a renderPanel for the scenario", "RenderPanel error", JOptionPane.ERROR_MESSAGE);
				System.exit(-1);
			}
			theaterPane.initialize(scenariumContainer, scenariumContainer.getScheduler(), de, true);
			theaterPane.prefWidthProperty().bind(stackPane.widthProperty());
			theaterPane.prefHeightProperty().bind(stackPane.heightProperty());
			stackPane.getChildren().add(0, theaterPane);
			adaptSizeToDrawableElement();
			// theaterPane.sizeToScene();
			theaterPane.ignoreRepaint = false;
			theaterPane.fitDocument();
		}
	}

	public void removePaneToLayeredPane(Pane pane) {
		stackPane.getChildren().remove(pane);
	}

	public void removeToolVisibleListener(final ToolVisibleListener listener) {
		listeners.remove(ToolVisibleListener.class, listener);
	}

	public void saveDrawerProperties() {
		File drawerPropertiesFile = new File(drawerPropertiesDir);
		if (!drawerPropertiesFile.exists())
			drawerPropertiesFile.mkdir();
		String tpn = theaterPane.getClass().getSimpleName();
		new BeanManager(theaterPane, drawerPropertiesDir).save(new File(drawerPropertiesDir + tpn + BeanDesc.SEPARATOR + tpn + ".txt"), true);
	}

	@Override
	public void setAnimated(boolean animated, AnimationTimerConsumer animationTimerConsumer) {
		this.animationTimerConsumer = animationTimerConsumer;
		theaterPane.animated = animated;
		if (animated) {
			for (Tool toolBar : displayedTools.values())
				if (toolBar != null && toolBar instanceof VisuableSchedulable)
					animationTimerConsumer.register((VisuableSchedulable) toolBar);
			theaterPane.setAnimated(animated, animationTimerConsumer);
		}
	}

	public void setCursor(Cursor cursor) {
		stackPane.setCursor(cursor);
	}

	public void showMessage(String message, boolean error) {
		StatusBar statusBar = (StatusBar) displayedTools.get(StatusBar.class);
		if (statusBar != null)
			statusBar.update(message, error, false);
		else {
			updateToolView(StatusBar.class);
			((StatusBar) displayedTools.get(StatusBar.class)).update(message, error, true);
		}
	}

	public void showTool(Class<? extends Tool> toolClass) {
		if (excludedTools.contains(toolClass))
			return;
		Tool toolBar = displayedTools.get(toolClass);
		if (toolBar == null)
			updateToolView(toolClass);
		else if (toolBar instanceof ExternalTool)
			((ExternalTool) toolBar).requestFocus();
	}

	public void updateStatusBar(String... infos) {
		StatusBar statusBar = (StatusBar) displayedTools.get(StatusBar.class);
		if (statusBar != null)
			statusBar.update(infos);
	}

	void updateToolView(Class<? extends Tool> toolClass) {
		if (excludedTools.contains(toolClass))
			return;
		Tool toolBar = displayedTools.get(toolClass);
		scenariumContainer.updateToolView(toolClass, toolBar == null);
		if (toolBar == null) {
			ToolBarInfo tbd = getToolBarDesc(toolClass);
			if(tbd == null) {
				System.err.println("ToolBar: " + toolClass.getSimpleName() + " not registered");
				return;
			}
			try {
				toolBar = toolClass.getConstructor().newInstance();
				if (!toolBar.instantiate(this, tbd.keyCode))
					return;
				if (toolBar instanceof ExternalTool) {
					Point2i toolPos = scenariumContainer.getDefaultToolBarLocation(toolClass.getSimpleName());
					if (toolPos != null)
						((ExternalTool) toolBar).setPosition(new Point2D(Math.max(toolPos.x, 0), Math.max(toolPos.y, 0)));
					((ExternalTool) toolBar).setAlwaysOnTop(scenariumContainer.isDefaultToolBarAlwaysOnTop(toolClass.getSimpleName()));
					((ExternalTool) toolBar).show();
				}
				if (toolBar instanceof VisuableSchedulable && animationTimerConsumer != null)
					animationTimerConsumer.register((VisuableSchedulable) toolBar);
				displayedTools.put(toolClass, toolBar);
				fireToolVisibleChanged(toolClass, true);
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e1) {
				e1.printStackTrace();
			}
			if (toolBar instanceof InternalTool) {
				Region region = toolBar.getRegion();
				if (region == null)
					throw new IllegalArgumentException("getValue on the class : " + toolBar.getClass() + " return forbidden value null");
				if (!toolPane.isVisible())
					toolPane.setVisible(true);
				switch (((InternalTool) toolBar).getBorder()) {
				case CENTER:
					centerPane.getChildren().add(region);
					if (!centerPane.isVisible())
						centerPane.setVisible(true);
					internalToolParents.put((InternalTool) toolBar, region);
					break;
				case TOP:
					toolPane.setTop(region);
					break;
				case RIGHT:
					toolPane.setRight(region);
					break;
				case BOTTOM:
					toolPane.setBottom(region);
					break;
				case LEFT:
					toolPane.setLeft(region);
					break;
				default:
					break;
				}
			}
		} else {
			if (toolBar instanceof InternalTool) {
				switch (((InternalTool) toolBar).getBorder()) {
				case CENTER:
					Region internalToolParent = internalToolParents.get(toolBar);
					internalToolParents.remove(toolBar);
					ObservableList<Node> children = centerPane.getChildren();
					children.remove(internalToolParent);
					if (children.isEmpty())
						centerPane.setVisible(false);
					break;
				case TOP:
					toolPane.setTop(null);
					break;
				case RIGHT:
					toolPane.setRight(null);
					break;
				case BOTTOM:
					toolPane.setBottom(null);
					break;
				case LEFT:
					toolPane.setLeft(null);
					break;
				default:
					break;
				}
				if (toolPane.getChildren().size() == 1 && centerPane.getChildren().isEmpty() && toolPane.isVisible())
					toolPane.setVisible(false);
			}
			if (toolBar instanceof VisuableSchedulable && animationTimerConsumer != null)
				animationTimerConsumer.unRegister((VisuableSchedulable) toolBar);
			displayedTools.remove(toolClass);
			fireToolVisibleChanged(toolClass, false);
			toolBar.dispose();
		}
	}

	Tool getDisplayedTool(Class<? extends Tool> toolClass) {
		return displayedTools.get(toolClass);
	}

	@Override
	public void loaded(ToolBarInfo toolBarInfo) {}

	@Override
	public void unloaded(ToolBarInfo toolBarInfo) {
		closeTool(toolBarInfo._class);
	}
}
