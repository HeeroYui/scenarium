/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.display.toolbarclass;

import javax.vecmath.Point2d;

import org.beanmanager.tools.FxUtils;

import javafx.geometry.Point2D;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Region;
import javafx.stage.Stage;

public abstract class ExternalTool extends Tool {
	protected Stage stage;

	@Override
	public void dispose() {
		stage.close();
	}

	protected String getIconName() {
		return "scenarium_icon.png";
	}

	public Point2d getPosition() {
		return new Point2d(stage.getX(), stage.getY());
	}

	@Override
	public boolean instantiate() {
		stage = new Stage();
		stage.setTitle(getClass().getSimpleName());
		stage.getIcons().add(new Image(ExternalTool.class.getResourceAsStream("/" + getIconName())));
		stage.addEventHandler(KeyEvent.KEY_PRESSED, e -> { // Handler -> sauf si quelqu'un a consumed
			if (!e.isConsumed() && !e.isControlDown() && !e.isShiftDown() && e.getCode() == keycode && renderPane != null)
				renderPane.closeTool(getClass());
		});
		Region parent = getRegion();
		if (parent == null)
			throw new IllegalArgumentException("getValue on the class : " + getClass() + " return forbidden value null");
		Scene scene = new Scene(parent, -1, -1);
		stage.setScene(scene);
		stage.sizeToScene();
		// parent.widthProperty().addListener(e -> Platform.runLater(() -> stage.sizeToScene())); //Les enfant peuvent pas s'adapter sinon
		// parent.heightProperty().addListener(e -> Platform.runLater(() -> stage.sizeToScene()));
		stage.setOnCloseRequest(e -> renderPane.closeTool(getClass()));
		// ScenicView.show(scene);
		return true;
	}

	public boolean isAlwaysOnTop() {
		return stage.isAlwaysOnTop();
	}

	public void setAlwaysOnTop(boolean alwaysOnTop) {
		stage.setAlwaysOnTop(alwaysOnTop);
	}

	public void setPosition(Point2D toolPos) {
		if (FxUtils.isLocationInScreenBounds(toolPos)) {
			stage.setX(toolPos.getX());
			stage.setY(toolPos.getY());
		}
	}

	public void show() {
		stage.show();
	}

	public void requestFocus() {
		stage.requestFocus();
	}
}
