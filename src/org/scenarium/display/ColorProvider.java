/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.display;

import java.util.Objects;

import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

public class ColorProvider {
	private final Color[] colors;
	private int colorIndex = -1;

	public ColorProvider() {
		this.colors = new Color[] { Color.RED, Color.BLUE, Color.LIME, Color.FUCHSIA, Color.CYAN, Color.YELLOW, new Color(0.5, 0, 1, 1), new Color(1, 0.5, 0, 1) };
	}
	
	public ColorProvider(Color[] colors) {
		Objects.requireNonNull(colors);
		this.colors = colors;
	}
	
	public Paint getCurrentColor() {
		return colors[colorIndex];
	}
	
	public Color getNextColor() {
		incrementColorIndex();
		return colors[colorIndex];
	}
	
	public void incrementColorIndex() {
		colorIndex++;
		if (colorIndex > colors.length)
			colorIndex = 0;
	}
	
	public void resetIndex() {
		colorIndex = -1;
	}

	public int getCurrentIndex() {
		return colorIndex;
	}
	
	public Color[] getColors() {
		return colors;
	}
}
