/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.display.toolBar;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.beanmanager.editors.primitive.number.ControlType;
import org.beanmanager.editors.primitive.number.IncrementMode;
import org.beanmanager.editors.primitive.number.IntegerEditor;
import org.beanmanager.editors.time.DateEditor;
import org.beanmanager.tools.FxUtils;
import org.scenarium.display.ToolVisibleListener;
import org.scenarium.display.toolbarclass.ExternalTool;
import org.scenarium.display.toolbarclass.Tool;
import org.scenarium.filemanager.DataLoader;
import org.scenarium.filemanager.OpenListener;
import org.scenarium.filemanager.scenariomanager.LocalScenario;
import org.scenarium.filemanager.scenariomanager.MetaScenario;
import org.scenarium.filemanager.scenariomanager.RecordingListener;
import org.scenarium.filemanager.scenariomanager.Scenario;
import org.scenarium.filemanager.scenariomanager.StartStopChangeListener;
import org.scenarium.timescheduler.Scheduler;
import org.scenarium.timescheduler.SchedulerPropertyChangeListener;
import org.scenarium.timescheduler.SchedulerState;
import org.scenarium.timescheduler.VisuableSchedulable;

import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBase;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Paint;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Line;
import javafx.scene.shape.StrokeLineCap;
import javafx.scene.text.Text;

public class Player extends ExternalTool implements VisuableSchedulable, SchedulerPropertyChangeListener, ToolVisibleListener, EventHandler<ActionEvent>, OpenListener, StartStopChangeListener, RecordingListener {
	private static final int PLAY = 0;
	private static final int STOP = 1;
	private static final int REVERSE = 2;
	private static final int PREVIOUS = 3;
	private static final int SLOWER = 4;
	private static final int FASTER = 5;
	private static final int NEXT = 6;
	private static final int STEPBACKWARD = 7;
	private static final int REFRESH = 8;
	private static final int STEPFORWARD = 9;
	private static final int BOUCLEAB = 10;
	private static final int PLAYLIST = 11;
	private static final int LOOPOREPEATMODE = 12;
	private static final int RECORDOFF = 13;
	private static final int[] speedPallier = new int[] { 1, 2, 5, 20, 100, 400, 1000, 5000, 10000, 100000 };
	// private static final String DATEFORMAT = "HH:mm:ss.SSS";
	private ButtonBase[] buttons;
	private Label endTimelabel;
	private String timePattern;
	private Slider timeSlider;
	private DateEditor timeEditor;
	private IntegerEditor speedEditor;
	private boolean dead = false;
	private TextField scenarioNameLabel;
	private HBox timeBox;
	private double halfImageAWidth;
	private ImageView imageViewA;
	private ImageView imageViewB;
	private ArrayList<Interval> intervals = new ArrayList<>();
	private boolean isUpdating;
	private boolean isUpdatingAB;
	private boolean isSchedulerBusy;
	// private boolean useTimeRepresentation = true;

	private boolean canReverse;
	private boolean canModulateSpeed;
	private int speed;
	private long beginTime;
	private long endTime;
	private long nbGap;
	private long timePointer;
	private Long timeA;
	private Long timeB;
	private boolean playing;
	private VBox intervalBox;

	@Override
	public void dispose() {
		super.dispose();
		if (dead == false) {
			Scheduler scheduler = getScheduler();
			// new Thread(() -> scheduler.removeVisualScheduleElement(this)).start(); //Pk le thread
			if (scheduler != null) {
				scheduler.removeVisualScheduleElement(this);
				scheduler.removePropertyChangeListener(this);
			}
			renderPane.removeToolVisibleListener(this);
			DataLoader dataLoader = renderPane.getDataLoader();
			dataLoader.removeOpenListener(this);
			Scenario scenario = dataLoader.getScenario();
			if (scenario instanceof LocalScenario)
				((LocalScenario) scenario).removeStartStopChangeListener(this);
			if(scenario.canRecord())
				scenario.removeRecordingListener(this);
		}
		dead = true;
	}

	@Override
	protected String getIconName() {
		return "scenarium_icon_player.png";
	}

	@Override
	public VBox getRegion() {
		DataLoader dataLoader = renderPane.getDataLoader();
		Scheduler scheduler = renderPane.getContainer().getScheduler();
		Scenario scenario = dataLoader.getScenario();

		speed = scheduler.getSpeed();
		playing = scheduler.isRunning();
		timePointer = scheduler.getTimePointer();
		timeA = scheduler.getTimeA();
		timeB = scheduler.getTimeB();

		HBox buttonBox = new HBox(2);
		buttonBox.setPadding(new Insets(2, 0, 5, 0));
		buttonBox.setAlignment(Pos.CENTER_LEFT);
		String[] buttonName = new String[] { "Play", "Stop", "Reverse", "Previous", "Slower", "Faster", "Next", "Step_backward", "Refresh", "Step_forward", "Boucle_A_B", "PlayList",
				"Loop_or_Repeat_mode", "RecordOff" };
		buttons = new ButtonBase[buttonName.length];
		int[] space = new int[] { 3, 7, 10, 13 };
		int indexSpace = 0;
		for (int i = 0; i < buttonName.length; i++) {
			String name = buttonName[i];
			String imgName = "/" + name;
			ImageView iv = new ImageView(new Image(getClass().getResourceAsStream(imgName + ".png")));
			ButtonBase button = i == 11 || i == 12 ? new ToggleButton(null, iv) : new Button(null, iv);
			int size = i == 0 ? 34 : 26;
			button.setMaxSize(size, size);
			button.setMinSize(size, size);
			if (indexSpace < space.length && i == space[indexSpace]) {
				HBox.setMargin(button, new Insets(0, 0, 0, 10));
				indexSpace++;
			}
			button.setTooltip(new Tooltip(name.replace("_or_", "/").replace("_", " ")));
			button.setOnAction(this);
			buttonBox.getChildren().add(button);
			buttons[i] = button;
			if (i == PLAYLIST)
				((ToggleButton) button).setSelected(renderPane.isDisplayed(PlayList.class));
			else if (i == LOOPOREPEATMODE)
				((ToggleButton) button).setSelected(scheduler.isRepeat());
		}
		String controlStyle = "-fx-background-color: transparent;-fx-border-width: 1px;-fx-border-color: #969696 #DCDCDC #DCDCDC #969696;";
		HBox boxBottom = new HBox(1);
		boxBottom.setPadding(new Insets(5, 0, 0, 0));
		scenarioNameLabel = new TextField();
		scenarioNameLabel.setStyle(controlStyle);
		scenarioNameLabel.setMinHeight(0);
		scenarioNameLabel.setPadding(new Insets(0));
		scenarioNameLabel.setMaxWidth(Double.MAX_VALUE);
		scenarioNameLabel.setEditable(false);
		scenarioNameLabel.setPrefWidth(1);
		int maxSpeed = scheduler.getMaxSpeed();
		HBox speedBox = new HBox();
		speedEditor = new IntegerEditor(-maxSpeed, maxSpeed, ControlType.TEXTFIELD);
		// speedEditor.setValue(scheduler.getSpeed());
		speedEditor.setIncrementMode(IncrementMode.GEOMETRICPROGRESSION);
		speedEditor.setIncrement(1.1);
		speedEditor.setStyle("-fx-background-color: transparent;");
		speedEditor.addPropertyChangeListener(() -> getScheduler().setSpeed(speedEditor.getValue()));
		TextField speedControl = (TextField) ((HBox) speedEditor.getNoSelectionEditor()).getChildren().get(0);
		speedControl.addEventFilter(KeyEvent.KEY_PRESSED, e -> {
			if (e.getCode() == KeyCode.UP) {
				faster();
				e.consume();
			} else if (e.getCode() == KeyCode.DOWN) {
				slower();
				e.consume();
			}
		});
		speedControl.setMinHeight(0);
		speedControl.setPadding(new Insets(0));
		speedControl.setAlignment(Pos.CENTER_RIGHT);
		speedControl.setPrefWidth(new Text(Integer.toString(-maxSpeed)).getLayoutBounds().getWidth() + 2);
		Label percentLabel = new Label("%");
		percentLabel.setMaxHeight(Double.MAX_VALUE);
		speedBox.getChildren().addAll(speedControl, percentLabel);
		speedBox.setStyle(controlStyle);
		endTimelabel = new Label();
		timeSlider = new Slider();
		timeSlider.setPadding(new Insets(0, 0, 4, 0));
		timeSlider.setOnScroll(e -> {
			if (e.getDeltaY() != 0)
				getScheduler().jump(e.getDeltaY() > 0);
		});
		timeSlider.setBlockIncrement(0);
		timeSlider.setOnKeyReleased(e -> e.consume());
		timeSlider.setOnKeyTyped(e -> e.consume());
		timeSlider.setOnKeyPressed(e -> {
			if (e.getCode() == KeyCode.RIGHT) {
				Scheduler _scheduler = getScheduler();
				long newVal = _scheduler.getTimePointer() + 1;
				if (newVal < _scheduler.getEffectiveStop())
					_scheduler.setTimePointer(newVal);
				e.consume();
			} else if (e.getCode() == KeyCode.LEFT) {
				Scheduler _scheduler = getScheduler();
				long newVal = _scheduler.getTimePointer() - 1;
				if (newVal > _scheduler.getEffectiveStart())
					_scheduler.setTimePointer(newVal);
				e.consume();
			}
		});
		timeSlider.setMaxWidth(Double.MAX_VALUE);
		timeSlider.valueProperty().addListener((ov, oldVal, newVal) -> {
			if (!isUpdating) {
				long lNewVal = (long) (newVal.doubleValue() + 0.5);
				long lOldVal = (long) (oldVal.doubleValue() + 0.5);
				if (lNewVal != lOldVal) {
					Scheduler _scheduler = getScheduler();
					_scheduler.setTimePointer(lNewVal);
					if (lNewVal != _scheduler.getTimePointer())
						paint();
				}
			}
		});

		timeBox = new HBox();
		timeBox.getChildren().addAll(new Label("/"), endTimelabel);
		timeBox.setStyle(controlStyle);
		HBox.setHgrow(scenarioNameLabel, Priority.ALWAYS);
		boxBottom.getChildren().addAll(scenarioNameLabel, speedBox, timeBox);
		Image image1 = new Image(getClass().getResourceAsStream("/A.png"));
		halfImageAWidth = image1.getWidth() / 2.0;
		imageViewA = new ImageView(image1);
		imageViewA.setOnMouseDragged(e -> {
			if (e.getButton() == MouseButton.PRIMARY && !isUpdatingAB)
				scheduler.setTimeA(getTimeForMarker(e.getSceneX()));
		});
		Image image2 = new Image(getClass().getResourceAsStream("/B.png"));
		imageViewB = new ImageView(image2);
		imageViewB.setOnMouseDragged(e -> {
			if (e.getButton() == MouseButton.PRIMARY && !isUpdatingAB)
				scheduler.setTimeB(getTimeForMarker(e.getSceneX()));
		});
		timeSlider.boundsInParentProperty().addListener((a, b, c) -> {
			updateABPosition();
			updateIntervalsPosition();
		});
		imageViewA.setManaged(false);
		imageViewB.setManaged(false);
		StackPane aBBox = new StackPane(imageViewA, imageViewB);
		aBBox.setPrefHeight(Math.max(image1.getHeight(), image2.getHeight()));
		intervalBox = new VBox(0);
		intervalBox.setPadding(new Insets(0, 0, 2, 0));
		VBox vbox = new VBox(timeSlider, intervalBox, aBBox, buttonBox, boxBottom);
		vbox.setPadding(new Insets(5, 5, 5, 5));
		vbox.setFillWidth(true);
		scheduler.addVisualScheduleElement(this);
		scheduler.addPropertyChangeListener(this);
		renderPane.addToolVisibleListener(this);
		dataLoader.addOpenListener(this);
		if (scenario instanceof LocalScenario)
			((LocalScenario) scenario).addStartStopChangeListener(this);
		opened(null, scenario);
		startStopChanged();
		updateBoucle();
		updateSpeed();
		updatePlayPause();
		updateHMIIntervalTime();
		recordingPropertyChanged(scenario.isRecording());
		return vbox;
	}

	private Scheduler getScheduler() {
		return renderPane.getContainer().getScheduler();
	}

	private long getTimeForMarker(double sceneX) {
		Node thumb = timeSlider.lookup(".thumb");
		// Scheduler scheduler = getScheduler();
		double thumbWidth = thumb.getLayoutBounds().getWidth();
		long time = (long) (beginTime + (sceneX - (timeSlider.getLayoutX() + thumbWidth / 2.0)) / (timeSlider.getWidth() - thumbWidth) * (endTime - beginTime));
		if (time < beginTime)
			time = beginTime;
		else if (time > endTime)
			time = endTime;
		return time;
	}

	@Override
	public void handle(ActionEvent e) {
		ButtonBase button = (ButtonBase) e.getSource();
		Scheduler scheduler = getScheduler();
		if (renderPane != null) {
			int id = -1;
			for (int i = 0; i < buttons.length; i++)
				if (buttons[i] == button) {
					id = i;
					break;
				}
			switch (id) {
			case PLAY:
				if (scheduler.isRunning())
					scheduler.pause();
				else {
					isSchedulerBusy = true;
					updateEnableButtons();
					new Thread(() -> {
						if (!scheduler.start()) {
							isSchedulerBusy = false;
							updateEnableButtons();
						}
					}).start();
				}
				break;
			case STOP:
				isSchedulerBusy = true;
				updateEnableButtons();
				new Thread(() -> {
					if (!scheduler.stop()) {
						isSchedulerBusy = false;
						updateEnableButtons();
					}
				}).start();
				break;
			case REVERSE:
				scheduler.setSpeed(-speed); // Opération potentiellement bloquante...
				break;
			case PREVIOUS:
				new Thread(() -> renderPane.getDataLoader().goToNextScenario(true)).start();
				break;
			case SLOWER:
				slower();
				break;
			case FASTER:
				faster();
				break;
			case NEXT:
				renderPane.getDataLoader().goToNextScenario(false);
				break;
			case STEPBACKWARD:
				scheduler.previousStep();
				break;
			case REFRESH:
				getScheduler().refresh();
				break;
			case STEPFORWARD:
				scheduler.nextStep();
				break;
			case BOUCLEAB:
				if (scheduler.getTimeA() == null)
					if (scheduler.getTimeB() == null)
						scheduler.setTimeA(timePointer);
					else
						scheduler.setTimeB(null);
				else if (scheduler.getTimeB() == null)
					scheduler.setTimeB(timePointer);
				else
					scheduler.setTimeA(null);
				break;
			case PLAYLIST:
				if (renderPane.isDisplayed(PlayList.class))
					renderPane.closeTool(PlayList.class);
				else
					renderPane.openTool(PlayList.class);
				break;
			case LOOPOREPEATMODE:
				scheduler.setRepeat(((ToggleButton) button).isSelected());
				break;
			case RECORDOFF:
				Scenario scenario = renderPane.getDataLoader().getScenario();
				scenario.setRecording(!scenario.isRecording());
				break;
			default:
				break;
			}
		}
	}

	private void slower() {
		for (int i = 0; i < speedPallier.length - 1; i++)
			if (Math.abs(speed) > speedPallier[speedPallier.length - i - 2]) {
				int newSpeed = speedPallier[speedPallier.length - i - 2];
				getScheduler().setSpeed(speed < 0 ? -newSpeed : newSpeed);
				break;
			}
	}

	private void faster() {
		for (int i = 0; i < speedPallier.length; i++)
			if (Math.abs(speed) < speedPallier[i]) {
				int newSpeed = speedPallier[i];
				getScheduler().setSpeed(speed < 0 ? -newSpeed : newSpeed);
				break;
			}
	}

	private void updateSlider() {
		timeSlider.setMin(beginTime);
		timeSlider.setMax(beginTime >= endTime ? beginTime + 1 : endTime); // Bug fx, si je mets max plus petit que min il mouline a 15% du proc...
		timeSlider.setValue(timePointer);
		timeSlider.setDisable(nbGap <= 0);
	}

	private void updateTimeLabel() {
		if (timeEditor != null)
			timeBox.getChildren().remove(timeEditor.getNoSelectionEditor());
		DateEditor _timeEditor = new DateEditor(beginTime, endTime, timePattern);
		_timeEditor.setStyle("-fx-background-color: transparent;");
		Region editor = _timeEditor.getNoSelectionEditor();
		MenuItem timeContexItem = new MenuItem();
		timeContexItem.setOnAction(e -> timeEditor.setIgnoreTimePattern(!timeEditor.isIgnoreTimePattern()));
		endTimelabel.setContextMenu(new ContextMenu(timeContexItem));
		Runnable updateEndLabel = () -> {
			endTimelabel.setText(endTime == -1 ? "-" : timePattern == null || timeEditor.isIgnoreTimePattern() ? Long.toString(endTime) : new SimpleDateFormat(timePattern).format(endTime));
			timeContexItem.setText(timeEditor.isIgnoreTimePattern() ? "As Time" : "As Integer");
		};

		InvalidationListener il = e -> {
			Node box = ((HBox) timeEditor.getNoSelectionEditor()).getChildrenUnmodifiable().get(0);
			TextField timeControl = box instanceof HBox ? (TextField) ((HBox) box).getChildren().get(0) : (TextField) box;
			timeControl.setMinHeight(0);
			timeControl.setPadding(new Insets(0));
			timeControl.setAlignment(Pos.CENTER_RIGHT);
			timeControl.setMinWidth(0);
			SimpleDateFormat formatter = timeEditor.getDateFormat();
			timeControl.setPrefWidth(new Text(formatter == null ? Long.toString(timeEditor.getMax()) : formatter.format(timeEditor.getMax())).getLayoutBounds().getWidth() + 2);
			updateEndLabel.run();
		};
		timeEditor = _timeEditor;
		editor.getChildrenUnmodifiable().addListener(il);
		timeEditor.setValue(new Date(timePointer));
		timeEditor.addPropertyChangeListener(() -> {
			if (!isUpdating) {
				Scheduler _scheduler = getScheduler();
				long timeFromEditor = timeEditor.getValue().getTime();
				if (_scheduler.getTimePointer() != timeFromEditor) {
					_scheduler.setTimePointer(timeFromEditor);
					paint();
				}
			}
		});
		il.invalidated(null);
		updateEndLabel.run();
		timeBox.getChildren().add(0, timeEditor.getNoSelectionEditor());
	}

	@Override
	public void opened(Scenario oldScenario, Scenario scenario) {
		if (oldScenario != null && oldScenario instanceof LocalScenario)
			((LocalScenario) oldScenario).removeStartStopChangeListener(this);
		if(oldScenario != null && oldScenario.canRecord())
			oldScenario.removeRecordingListener(this);
		if (scenario == null)
			return;
		timePattern = scenario.getTimePattern();
		canReverse = scenario.canReverse();
		canModulateSpeed = scenario.canModulateSpeed();
		String scenarioName = scenario.toString();
		FxUtils.runLaterIfNeeded(() -> {
			scenarioNameLabel.setText(scenarioName);
			updateTimeLabel();
		});
		if (scenario instanceof LocalScenario)
			((LocalScenario) scenario).addStartStopChangeListener(this);
		buttons[RECORDOFF].setDisable(!scenario.canRecord());
		if(scenario.canRecord())
			scenario.addRecordingListener(this);
	}

	@Override
	public void startStopChanged() {
		ArrayList<Interval> oldIntervals = intervals;
		intervals = new ArrayList<>();
		Scheduler scheduler = getScheduler();
		beginTime = scheduler.getBeginningTime();
		endTime = scheduler.getEndTime();
		nbGap = scheduler.getNbGap();
		Scenario scenario = renderPane.getDataLoader().getScenario();
		if (scenario instanceof MetaScenario) {
			Scenario[] subScenarios = ((MetaScenario) scenario).getAdditionalScenario();
			for (Scenario ss : subScenarios) {
				if (ss instanceof LocalScenario) {
					Date sta = ss.getStartTime();
					Date sto = ss.getStopTime();
					intervals.add(new Interval(ss.getBlockName(), ((LocalScenario) ss).getColor(), ss.getBeginningTime(), ss.getEndTime(), sta == null ? null : sta.getTime(),
							sto == null ? null : sto.getTime()));
				}
			}
		}
		FxUtils.runLaterIfNeeded(() -> {
			oldIntervals.forEach(interval -> intervalBox.getChildren().remove(interval.fullLine));
			updateIntervalsPosition();
			if (oldIntervals.size() != intervals.size())
				Platform.runLater(() -> stage.sizeToScene());
		});
	}

	@Override
	public void paint() {
		Scheduler scheduler = getScheduler();
		if (scheduler != null) {
			long timeFromEditor = timeEditor.getValue().getTime();
			timePointer = scheduler.getTimePointer();
			if (timeFromEditor != scheduler.getTimePointer() || timePointer != timeSlider.getValue())
				FxUtils.runLaterIfNeeded(() -> {
					isUpdating = true;
					if (timeFromEditor != scheduler.getTimePointer())
						timeEditor.setValue(new Date(timePointer));
					if (timePointer != timeSlider.getValue())
						timeSlider.setValue(timePointer);
					isUpdating = false;
				});
		}
	}

	@Override
	public void setAnimated(boolean animated) {}

	@Override
	public void stateChanged(final SchedulerState state) {
		Scheduler scheduler = getScheduler();
		if (scheduler == null)
			return;
		switch (state) {
		case STARTED:
		case STOPPED:
		case SUSPENDED:
		case UNSUSPENDED: {
			playing = state == SchedulerState.STARTED || state == SchedulerState.UNSUSPENDED;
			FxUtils.runLaterIfNeeded(() -> {
				isSchedulerBusy = false;
				updateEnableButtons();
				updatePlayPause();
			});
		}
			break;
		case PRESTART:
		case PRESTOP:
			FxUtils.runLaterIfNeeded(() -> {
				isSchedulerBusy = true;
				updateEnableButtons();
			});
			break;
		case SPEEDCHANGED:
			speed = scheduler.getSpeed();
			FxUtils.runLaterIfNeeded(() -> updateSpeed());
			break;
		case BOUCLE:
			timeA = scheduler.getTimeA();
			timeB = scheduler.getTimeB();
			FxUtils.runLaterIfNeeded(() -> updateBoucle());
			break;
		case INTERVALTIMECHANGED:
			startStopChanged();
			FxUtils.runLaterIfNeeded(() -> updateHMIIntervalTime());
			break;
		default:
			break;
		}
	}

	@Override
	public void toolVisibleChanged(Class<? extends Tool> toolBarClass, boolean visible) {
		if (toolBarClass == PlayList.class)
			((ToggleButton) buttons[PLAYLIST]).setSelected(visible);
	}

	private void updateABPosition() {
		isUpdatingAB = true;
		Node thumb = timeSlider.lookup(".thumb");
		if (thumb == null)
			return;
		double thumbWidth = thumb.getLayoutBounds().getWidth();
		double trackLength = timeSlider.getWidth() - thumbWidth;
		double trackStart = timeSlider.getTranslateX() + thumbWidth / 2.0;
		long timeInterval = endTime - beginTime;
		if (timeA != null) {
			imageViewA.setTranslateX(trackStart - halfImageAWidth + trackLength * (timeA - beginTime) / timeInterval);
			imageViewA.setVisible(true);
		} else
			imageViewA.setVisible(false);
		if (timeB != null) {
			imageViewB.setTranslateX(trackStart - halfImageAWidth + trackLength * (timeB - beginTime) / timeInterval);
			imageViewB.setVisible(true);
		} else
			imageViewB.setVisible(false);
		isUpdatingAB = false;
	}

	private void updateIntervalsPosition() {
		if (timeSlider == null)
			return;
		Node thumb = timeSlider.lookup(".thumb");
		if (thumb == null)
			return;
		double thumbWidth = thumb.getLayoutBounds().getWidth();
		double trackLength = timeSlider.getWidth() - thumbWidth;
		double trackStart = timeSlider.getTranslateX() + thumbWidth / 2.0;
		for (Interval interval : intervals) {
			if (interval.fullLine == null)
				interval.createLine();
			interval.updateLine(beginTime, endTime, trackStart, trackLength);
			if (!intervalBox.getChildren().contains(interval.fullLine))
				intervalBox.getChildren().add(interval.fullLine);
		}
	}

	private void updateBoucle() {
		String boucleIconName = timeA != null && timeB != null ? "Point_A_B" : timeA != null ? "Point_A" : timeB != null ? "Point_B" : "Boucle_A_B";
		buttons[BOUCLEAB].setGraphic(new ImageView(new Image(getClass().getResourceAsStream("/" + boucleIconName + ".png"))));
		buttons[BOUCLEAB].getTooltip().setText(boucleIconName);
		updateABPosition();
	}

	private void updateEnableButtons() {
		if (isSchedulerBusy)
			for (ButtonBase buttonBase : buttons)
				buttonBase.setDisable(true);
		else {
			buttons[PLAY].setDisable(false);
			buttons[STOP].setDisable(false);
			buttons[PREVIOUS].setDisable(false);
			buttons[NEXT].setDisable(false);
			buttons[PLAYLIST].setDisable(false);
			Scheduler scheduler = renderPane.getContainer().getScheduler();
			if (scheduler == null || scheduler.isOfKind(Scheduler.STREAMSCHEDULER) || timeSlider.getMax() - timeSlider.getMin() <= 1) {
				buttons[REVERSE].setDisable(true);
				buttons[SLOWER].setDisable(true);
				buttons[FASTER].setDisable(true);
				buttons[STEPBACKWARD].setDisable(true);
				buttons[REFRESH].setDisable(true);
				buttons[STEPFORWARD].setDisable(true);
				buttons[LOOPOREPEATMODE].setDisable(true);
				buttons[BOUCLEAB].setDisable(true);
			} else {
				buttons[REVERSE].setDisable(!canReverse);
				buttons[SLOWER].setDisable(!canModulateSpeed);
				buttons[FASTER].setDisable(!canModulateSpeed);
				buttons[STEPBACKWARD].setDisable(!canReverse);
				buttons[REFRESH].setDisable(false);
				buttons[STEPFORWARD].setDisable(false);
				buttons[LOOPOREPEATMODE].setDisable(false);
				buttons[BOUCLEAB].setDisable(false);
			}
		}
	}

	private void updateHMIIntervalTime() {
		isUpdating = true;
		updateSlider();
		updateTimeLabel();
		isUpdating = false;
		updateEnableButtons();
	}

	private void updatePlayPause() {
		String playIconName = playing ? "Pause" : "Play";
		((ImageView)buttons[PLAY].getGraphic()).setImage(new Image(getClass().getResourceAsStream("/" + playIconName + ".png")));
		buttons[PLAY].getTooltip().setText(playIconName);
	}

	private void updateSpeed() {
		speedEditor.setValue(speed);
		String speedIconName = speed > 0 ? "Reverse" : "Verse";
		((ImageView)buttons[REVERSE].getGraphic()).setImage(new Image(getClass().getResourceAsStream("/" + speedIconName + ".png")));
		buttons[REVERSE].getTooltip().setText(speedIconName);
	}

	@Override
	public void recordingPropertyChanged(boolean recording) {
		String recordingIconName = "Record" + (recording ? "On" : "Off");
		((ImageView)buttons[RECORDOFF].getGraphic()).setImage(new Image(getClass().getResourceAsStream("/" + recordingIconName + ".png")));
	}
}

class Interval {
	private static final Color grey = Color.GREY.brighter();
	public final String name;
	private final Color color;
	public final long beginning;
	public final long end;
	public final Long start;
	public final Long stop;
	public StackPane fullLine;
	public Line beginLine;
	public Line line;
	public Line endLine;

	public Interval(String name, Color color, long beginning, long end, Long start, Long stop) {
		this.color = color;
		this.name = name;
		this.beginning = beginning;
		this.end = end;
		this.start = start;
		this.stop = stop;
	}

	public void updateLine(long beginTime, long endTime, double trackStart, double trackLength) {
		long timeInterval = endTime - beginTime;
		if(timeInterval == 0)		//avoid infinite width when scenario change due to division with zero. Happens when scheduler have no scenario and so beginTime and endTime == 0
			return;
		long begin = this.start != null ? this.start : this.beginning;
		long end = this.stop != null ? this.stop : this.end;
		line.setTranslateX(trackStart + (trackLength * (this.beginning - beginTime) / timeInterval));
		beginLine.setTranslateX(line.getTranslateX());
		line.setEndX((this.end - this.beginning) / (double) timeInterval * trackLength);
		if (line.getStroke() == null || ((LinearGradient) line.getStroke()).getEndX() != line.getEndX())
			line.setStroke(getLinearGradient((begin - beginning) / (double) (this.end - this.beginning), (end - this.beginning) / (double) (this.end - this.beginning)));
		endLine.setTranslateX(line.getTranslateX() + line.getEndX());
	}

	private Paint getLinearGradient(double beginRatio, double endRatio) {
		return new LinearGradient(0, 0, line.getEndX(), 0, false, CycleMethod.REPEAT, new Stop(0, grey), new Stop(beginRatio, grey), new Stop(beginRatio, color), new Stop(endRatio, color),
				new Stop(endRatio, grey), new Stop(1, grey));
	}

	public void createLine() {
		line = new Line();
		line.setStrokeWidth(2);
		line.setStrokeLineCap(StrokeLineCap.BUTT);
		line.setStroke(null);
		fullLine = new StackPane(line);
		beginLine = new Line();
		beginLine.setStrokeWidth(2);
		beginLine.setStrokeLineCap(StrokeLineCap.BUTT);
		beginLine.setStroke(start == null ? color : grey);
		beginLine.setStartY(-2);
		beginLine.setEndY(2);
		fullLine.getChildren().add(beginLine);
		endLine = new Line();
		endLine.setStrokeWidth(2);
		endLine.setStrokeLineCap(StrokeLineCap.BUTT);
		endLine.setStroke(stop == null ? color : grey);
		endLine.setStartY(-2);
		endLine.setEndY(2);
		fullLine.getChildren().add(endLine);
		fullLine.setAlignment(Pos.CENTER_LEFT);
		Tooltip.install(fullLine, new Tooltip(name));
	}
}
