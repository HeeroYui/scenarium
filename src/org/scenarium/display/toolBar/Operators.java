/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.display.toolBar;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.beanmanager.BeanDesc;
import org.beanmanager.BeanManager;
import org.beanmanager.BeanRegisterListener;
import org.beanmanager.BeanRenameListener;
import org.beanmanager.BeanUnregisterListener;
import org.beanmanager.LoadModuleListener;
import org.beanmanager.editors.SubBeanChangeListener;
import org.beanmanager.editors.container.BeanEditor;
import org.beanmanager.tools.FxUtils;
import org.scenarium.ModuleManager;
import org.scenarium.Scenarium;
import org.scenarium.display.toolbarclass.ExternalTool;
import org.scenarium.filemanager.scenario.dataflowdiagram.operator.OperatorManager;
import org.scenarium.test.fx.FxTest;

import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DataFormat;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

public class Operators extends ExternalTool implements SubBeanChangeListener, BeanRegisterListener, BeanUnregisterListener, BeanRenameListener, LoadModuleListener {
	public static final DataFormat beanFormat = new DataFormat("ScenariumBean");
	public static final DataFormat operatorFormat = new DataFormat("ScenariumOperator");
	public static final DataFormat operatorClassFormat = new DataFormat("ScenariumOperatorClass");
	private static final Image OPENFOLDERICON = new Image(Operators.class.getResourceAsStream("/open_folder.png"));
	private static final Image CLOSEDFOLDERICON = new Image(Operators.class.getResourceAsStream("/closed_folder.png"));
	static final Image OPERATORINSTICON = new Image(BeanTreeItem.class.getResourceAsStream("/Operator.gif"));
	static final Image BEANICON = new Image(Operators.class.getResourceAsStream("/bean.gif"));
	private static final String OPERATORPACKAGENAME = ".operator.";

	private TreeView<Object> tree;
	private BorderPane operatorPane;
	private BeanManager beanManager;
	private ObservableList<BeanDesc<?>> beanList;
	private ListView<BeanDesc<?>> beanListView;

	public static void main(String[] args) {
		Operators ofx = new Operators();
		new FxTest().launchIHM(args, () -> ofx.getRegion(), e -> ofx.dispose());
	}

	@Override
	public void beanRegister(BeanDesc<?> beanDesc) {
		FxUtils.runLaterIfNeeded(() -> {
			if (OperatorManager.isOperator(beanDesc.bean)) {
				LinkedList<FilterableTreeItem<Object>> nodeToTest = new LinkedList<>();
				nodeToTest.add((FilterableTreeItem<Object>) tree.getRoot());
				while (!nodeToTest.isEmpty()) {
					FilterableTreeItem<Object> node = nodeToTest.pop();
					for (TreeItem<Object> treeItem : node.getInternalChildren()) {
						if (treeItem instanceof OperatorClassTreeItem)
							((OperatorClassTreeItem<?>) treeItem).reset();
						if (!treeItem.isLeaf())
							nodeToTest.push((FilterableTreeItem<Object>) treeItem);
					}
				}
			}
			beanList.add(beanDesc);
		});
	}

	@Override
	public void beanRename(BeanDesc<?> oldBeanDesc, BeanDesc<?> beanDesc) {
		FxUtils.runLaterIfNeeded(() -> {
			beanUnregister(oldBeanDesc, true);
			beanRegister(beanDesc);
		});
	}

	@Override
	public void beanUnregister(BeanDesc<?> beanDesc, boolean delete) { // TODO on relache pas immédiatement alors...
		FxUtils.runLaterIfNeeded(() -> {
			LinkedList<FilterableTreeItem<Object>> nodeToTest = new LinkedList<>();
			nodeToTest.add((FilterableTreeItem<Object>) tree.getRoot());
			while (!nodeToTest.isEmpty()) {
				FilterableTreeItem<Object> node = nodeToTest.pop();
				ArrayList<TreeItem<?>> nodesToReset = new ArrayList<>();
				for (TreeItem<Object> treeItem : node.getInternalChildren()) { // TODO ConcurrentModificationException
					if (treeItem instanceof BeanTreeItem && ((BeanTreeItem) treeItem).beanDesc == beanDesc)
						nodesToReset.add(treeItem.getParent());
					if (!treeItem.isLeaf())
						nodeToTest.push((FilterableTreeItem<Object>) treeItem);
				}
				for (TreeItem<?> nodeToRest : nodesToReset)
					if (nodeToRest instanceof BeanTreeItem)
						((BeanTreeItem) nodeToRest).reset();
					else if (nodeToRest instanceof OperatorClassTreeItem)
						((OperatorClassTreeItem<?>) nodeToRest).reset();
			}
			beanList.remove(beanDesc);
		});
	}

	@Override
	public void dispose() {
		ModuleManager.removeLoadModuleListener(this);
		super.dispose();
		BeanEditor.removeStrongRefBeanRegisterListener(this);
		BeanEditor.removeStrongRefBeanUnregisterListener(this);
		BeanEditor.removeStrongRefBeanRenameListener(this);
		BeanManager.removeSubBeanChangeListener(this);
		if (beanManager != null)
			beanManager.saveIfChanged();
	}

	private static void expandTreeView(TreeItem<Object> node) {
		LinkedList<TreeItem<Object>> todo = new LinkedList<>();
		todo.add(node);
		while (!todo.isEmpty()) {
			TreeItem<Object> ti = todo.pop();
			ti.setExpanded(true);
			todo.addAll(ti.getChildren());
		}
	}

	@Override
	public SplitPane getRegion() {
		ModuleManager.addLoadModuleListener(this);
		SplitPane splitPane1 = new SplitPane();
		splitPane1.setOrientation(Orientation.VERTICAL);
		splitPane1.setDividerPositions(0.6);
		FilterableTreeItem<Object> rootNode = new FilterableTreeItem<>("rootNode");
		populateInternOperatorNode(rootNode);
		tree = new TreeView<>(rootNode);
		tree.setShowRoot(false);
		tree.getSelectionModel().selectedItemProperty().addListener((a, b, op) -> updatePanelOperator(op instanceof BeanTreeItem ? ((BeanTreeItem) op).beanDesc.bean : null));
		tree.setEditable(true);
		tree.setCellFactory(p -> {
			TreeCell<Object> cell = new TreeCell<>() {
				private void commitEdit(TextField textField, BeanDesc<?> beanDesc) {
					cancelEdit();
					if (changeBeanName(beanDesc.bean, textField.getText()))
						getTreeView().refresh();
				}

				@Override
				public void startEdit() {
					if (!isEditable() || !getTreeView().isEditable() || !(getItem() instanceof BeanDesc))
						return;
					BeanDesc<?> beanDesc = (BeanDesc<?>) getItem();
					setText(null);
					TextField textField = new TextField(beanDesc.name);
					textField.setPadding(new Insets(0));
					textField.setPrefHeight(0);
					textField.setStyle("-fx-background-color: white;");
					textField.focusedProperty().addListener((a, b, c) -> {
						if (!c)
							commitEdit(textField, beanDesc);
					});
					textField.setOnKeyPressed(e -> {
						if (e.getCode() == KeyCode.ENTER)
							commitEdit(textField, beanDesc);
					});
					HBox.setHgrow(textField, Priority.ALWAYS);
					setGraphic(new HBox(3, new ImageView(OperatorManager.isOperator(beanDesc.bean) ? OPERATORINSTICON : BEANICON), textField));
					textField.requestFocus();
				};

				@Override
				protected void updateItem(Object item, boolean empty) {
					super.updateItem(item, empty);
					if (item != null) {
						TreeItem<Object> ti = getTreeItem();
						setGraphic(ti.getGraphic());
						setText(item.toString());
						String toolTipText = ti instanceof ModuleTreeItem ? "Path: " + ModuleManager.getModulePath(((ModuleTreeItem) ti).module) : ti instanceof OperatorClassTreeItem ? "Name: " + ((OperatorClassTreeItem<?>) ti).type.getName() : null;
						if (toolTipText != null) {
							Tooltip tt = getTooltip();
							if (tt == null)
								setTooltip(new Tooltip(toolTipText));
							else
								tt.setText(toolTipText);
						} else
							setTooltip(null);

					} else {
						setGraphic(null);
						setText(null);
						setTooltip(null);
					}
				}
			};
			cell.setOnContextMenuRequested(e -> {
				if (cell.isEmpty()) {
					MenuItem rmmi = new MenuItem("Register module");
					rmmi.setOnAction(e1 -> {
						FileChooser fc = new FileChooser();
						fc.setSelectedExtensionFilter(new ExtensionFilter("Java module", "*.jar"));
						fc.setTitle("Register new module in Scenarium");
						File moduleFile = fc.showOpenDialog(stage);
						if (moduleFile != null)
							ModuleManager.registerExternModules(moduleFile.toPath());
					});
					new ContextMenu(rmmi).show(cell, e.getScreenX(), e.getScreenY());
				} else {
					TreeItem<Object> treeItem = cell.getTreeItem();
					if (treeItem == null)
						return;
					ArrayList<MenuItem> menus = new ArrayList<>();
					if (treeItem instanceof BeanTreeItem) {
						MenuItem deleteMenu = new MenuItem("Delete");
						menus.add(deleteMenu);
						deleteMenu.setOnAction(e11 -> removeBean(((BeanTreeItem) treeItem).beanDesc, true, false));
						deleteMenu.setAccelerator(new KeyCodeCombination(KeyCode.DELETE));
						MenuItem deleteAndPurgeMenu = new MenuItem("Delete and delete sub beans");
						menus.add(deleteAndPurgeMenu);
						deleteAndPurgeMenu.setOnAction(e12 -> removeBean(((BeanTreeItem) treeItem).beanDesc, true, true));
						deleteAndPurgeMenu.setAccelerator(new KeyCodeCombination(KeyCode.DELETE, KeyCombination.SHIFT_DOWN));
					} else if (treeItem instanceof OperatorClassTreeItem) {
						MenuItem newInstMenu = new MenuItem("New instance...");
						newInstMenu.setAccelerator(new KeyCodeCombination(KeyCode.N));
						menus.add(newInstMenu);
						newInstMenu.setOnAction(e13 -> {
							try {
								BeanEditor.registerCreatedBeanAndCreateSubBean(((OperatorClassTreeItem<?>) treeItem).type.getConstructor().newInstance(), BeanManager.DEFAULTDIR);
							} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e21) {
								e21.printStackTrace();
							}
						});
					} else if (treeItem instanceof ModuleTreeItem && !ModuleManager.isInternModule(((ModuleTreeItem) treeItem).module)) {
						MenuItem unregisterMenu = new MenuItem("Unregister");
						unregisterMenu.setAccelerator(new KeyCodeCombination(KeyCode.DELETE));
						menus.add(unregisterMenu);
						unregisterMenu.setOnAction(e13 -> {
							ModuleManager.unregisterExternModule(((ModuleTreeItem) treeItem).module);
							System.gc();
							new Thread(() -> {
								while (true) {
									try {
										Thread.sleep(1000);
									} catch (InterruptedException e1) {
										e1.printStackTrace();
									}
									System.out.println("gc");
									System.gc();
								}
							}).start();
						});
					}
					if (!treeItem.isLeaf()) {
						MenuItem eaMenu = new MenuItem("Expand All");
						eaMenu.setAccelerator(new KeyCodeCombination(KeyCode.E));
						eaMenu.setOnAction(e22 -> expandTreeView(treeItem));
						menus.add(eaMenu);
					}
					if (!menus.isEmpty())
						new ContextMenu(menus.toArray(new MenuItem[menus.size()])).show(cell, e.getScreenX(), e.getScreenY());
				}
				e.consume();
			});
			return cell;
		});
		tree.setOnKeyPressed(e -> {
			if (e.getCode() == KeyCode.DELETE) {
				TreeItem<Object> selectedItem = tree.getSelectionModel().getSelectedItem();
				if (selectedItem instanceof BeanTreeItem)
					removeBean(((BeanTreeItem) selectedItem).beanDesc, true, e.isShiftDown());
				else if (selectedItem instanceof ModuleTreeItem)
					ModuleManager.unregisterExternModule(((ModuleTreeItem) selectedItem).module);
				// tree.getRoot().getChildren().remove(selectedItem);
			} else if (e.getCode() == KeyCode.N) {
				TreeItem<Object> selectedItem = tree.getSelectionModel().getSelectedItem();
				if (selectedItem instanceof OperatorClassTreeItem)
					try {
						BeanEditor.registerCreatedBeanAndCreateSubBean(((OperatorClassTreeItem<?>) selectedItem).type.getConstructor().newInstance(), BeanManager.DEFAULTDIR);
					} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e2) {
						e2.printStackTrace();
					}
			} else if (e.getCode() == KeyCode.E) {
				TreeItem<Object> selectedItem = tree.getSelectionModel().getSelectedItem();
				expandTreeView(selectedItem == null ? tree.getRoot() : selectedItem);
			}
		});
		tree.setOnDragDetected(e -> {
			ClipboardContent content = new ClipboardContent();
			TreeItem<Object> item = tree.getSelectionModel().getSelectedItem();
			if (item instanceof BeanTreeItem) {
				BeanDesc<?> beanDesc = ((BeanTreeItem) item).beanDesc;
				content.put(OperatorManager.isOperator(beanDesc.bean) ? operatorFormat : beanFormat, beanDesc.getCompleteDescriptor());
				tree.startDragAndDrop(TransferMode.MOVE).setContent(content);
				e.consume();
			} else if (item instanceof OperatorClassTreeItem) {
				content.put(operatorClassFormat, BeanManager.getDescriptorFromClass(((OperatorClassTreeItem<?>) item).type));
				tree.startDragAndDrop(TransferMode.MOVE).setContent(content);
				System.out.println("dragg: " + ((OperatorClassTreeItem<?>) item).type);
				e.consume();
			}
		});
		// expandTreeView(); // a garder pour étendre l'arbre
		// for (TreeItem<Object> treeItem : rootNode.getChildren())
		// treeItem.setExpanded(true);

		beanList = FXCollections.observableArrayList();
		beanListView = new ListView<>();
		beanListView.setEditable(true);
		for (BeanDesc<?> beanDesc : BeanEditor.getAllBeans())
			beanList.add(beanDesc);
		SortedList<BeanDesc<?>> sortedBeanList = beanList.sorted((o1, o2) -> o1.name.compareTo(o2.name));
		beanListView.setCellFactory(e -> {
			return new ListCell<>() {

				private void commitEdit(TextField textField, BeanDesc<?> beanDesc) {
					cancelEdit();
					if (changeBeanName(beanDesc.bean, textField.getText()))
						getListView().refresh();
				}

				@Override
				public void startEdit() {
					if (!isEditable() || !getListView().isEditable())
						return;
					setText(null);
					BeanDesc<?> beanDesc = getItem();
					TextField textField = new TextField(beanDesc.name);
					textField.setPadding(new Insets(0));
					textField.setPrefHeight(0);
					textField.setStyle("-fx-background-color: white;");
					textField.focusedProperty().addListener((a, oldValue, newValue) -> {
						if (!newValue)
							commitEdit(textField, beanDesc);
					});
					textField.setOnKeyPressed(e -> {
						if (e.getCode() == KeyCode.ENTER)
							commitEdit(textField, beanDesc);
					});
					HBox.setHgrow(textField, Priority.ALWAYS);
					setGraphic(new HBox(3, new ImageView(OperatorManager.isOperator(beanDesc.bean) ? OPERATORINSTICON : BEANICON), textField));
					textField.requestFocus();
				};

				@Override
				protected void updateItem(BeanDesc<?> item, boolean empty) {
					super.updateItem(item, empty);
					if (item == null || empty) {
						setText(null);
						setGraphic(null);
					} else {
						setText(item.name);
						setGraphic(new ImageView(OperatorManager.isOperator(item.bean) ? OPERATORINSTICON : BEANICON));
					}
				}
			};
		});
		beanListView.setItems(sortedBeanList);
		beanListView.getSelectionModel().selectedItemProperty().addListener((a, b, op) -> updatePanelOperator(op == null ? null : op.bean));
		beanListView.setOnDragDetected(e -> {
			BeanDesc<?> beanDesc = beanListView.getSelectionModel().getSelectedItem();
			if (beanDesc != null) {
				ClipboardContent content = new ClipboardContent();
				content.put(OperatorManager.isOperator(beanDesc.bean) ? operatorFormat : beanFormat, beanDesc.getCompleteDescriptor());
				beanListView.startDragAndDrop(TransferMode.MOVE).setContent(content);
				e.consume();
			}
		});
		beanListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		beanListView.setOnKeyPressed(e -> {
			if (e.getCode() == KeyCode.DELETE)
				for (BeanDesc<?> beanDesc : new ArrayList<>(beanListView.getSelectionModel().getSelectedItems()))
					removeBean(beanDesc, true, e.isShiftDown());
		});
		TextField searchField = new TextField();
		searchField.setPromptText("search");
		searchField.textProperty().addListener(e -> beanListView.setItems(sortedBeanList.filtered(a -> a.name.toLowerCase().contains(searchField.getText().toLowerCase()))));
		VBox.setVgrow(tree, Priority.ALWAYS);
		VBox.setVgrow(beanListView, Priority.ALWAYS);

		TextField searchFieldTree = new TextField();
		rootNode.predicateProperty().bind(Bindings.createObjectBinding(() -> {
			if (searchFieldTree.getText() == null || searchFieldTree.getText().isEmpty())
				return null;
			return TreeItemPredicate.create(actor -> actor.toString().toLowerCase().contains(searchFieldTree.getText().toLowerCase()));
		}, searchFieldTree.textProperty()));

		splitPane1.getItems().addAll(new VBox(searchFieldTree, tree), new VBox(searchField, beanListView));
		SplitPane splitPane2 = new SplitPane();
		splitPane2.setOrientation(Orientation.HORIZONTAL);
		splitPane2.setDividerPositions(0.3);
		splitPane2.setPrefSize(800, 600);
		operatorPane = new BorderPane();
		splitPane2.getItems().addAll(splitPane1, operatorPane);

		BeanEditor.addStrongRefBeanRegisterListener(this);
		BeanEditor.addStrongRefBeanUnregisterListener(this);
		BeanEditor.addStrongRefBeanRenameListener(this);
		BeanManager.addSubBeanChangeListener(this);
		return splitPane2;
	}

	private static boolean changeBeanName(Object bean, String newName) {
		BeanDesc<?> oldBeanDesc = BeanEditor.getBeanDesc(bean);
		if (oldBeanDesc != null) {
			String oldName = oldBeanDesc.name;
			if (!oldName.equals(newName))
				try {
					BeanEditor.renameBean(oldBeanDesc, newName);
					return true;
				} catch (IllegalArgumentException e) {
					System.err.println(e.getMessage());
				}
		}
		return false;
	}

	private static void populateInternOperatorNode(FilterableTreeItem<Object> rootNode) {
		// Get a list of internal operators and a map of external classes for each module
		ClassLoader dcl = Scenarium.class.getClassLoader();
		HashMap<Module, ArrayList<Class<?>>> externModules = new HashMap<>();
		ArrayList<Class<?>> internalOperators = new ArrayList<>();
		for (Class<?> operator : OperatorManager.getInternOperators()) {
			// System.out.println(operator.getSimpleName() + "\t" + operator.getModule() + "\t" + (operator.getClassLoader() == dcl ? " is intern" : "isExtern"));
			if (operator.getClassLoader() != dcl) {
				Module module = operator.getModule();
				ArrayList<Class<?>> operators = externModules.get(operator.getModule());
				if (operators == null) {
					operators = new ArrayList<>();
					externModules.put(module, operators);
				}
				operators.add(operator);
			} else
				internalOperators.add(operator);
		}
		rootNode.getInternalChildren().sort((r1, r2) -> r1.toString().compareTo(r2.toString()));
		// populate intern operators and extern modules
		populateOperatorsTreeItems(rootNode, internalOperators, operator -> {
			String className = operator.getName();
			if (className.contains(OPERATORPACKAGENAME))
				return className.substring(className.lastIndexOf(OPERATORPACKAGENAME) + OPERATORPACKAGENAME.length());
			int index1 = className.lastIndexOf(".") - 1;
			int index2 = className.lastIndexOf(".", index1);
			return className.substring((index2 != -1 ? index2 : index1 + 1) + 1); // attention index1 + 1 ajout + 1 car bug operator racine sinon
		});
		externModules.forEach((module, operators) -> rootNode.getInternalChildren().add(new ModuleTreeItem(module, operators)));
	}

	private void removeBean(BeanDesc<?> beanDescToRemove, boolean delete, boolean deleteSubBeans) {
		BeanManager.purgeBean(beanDescToRemove.bean, delete, deleteSubBeans);
		beanList.remove(beanDescToRemove);
		LinkedList<TreeItem<Object>> nodeToTest = new LinkedList<>();
		nodeToTest.add(tree.getRoot());
		while (!nodeToTest.isEmpty()) {
			FilterableTreeItem<Object> node = (FilterableTreeItem<Object>) nodeToTest.pop();
			node.getInternalChildren().removeIf(sonNode -> {
				boolean toRemove = sonNode instanceof BeanTreeItem && ((BeanTreeItem) sonNode).beanDesc == beanDescToRemove;
				if (!toRemove && !sonNode.isLeaf())
					nodeToTest.push(sonNode);
				return toRemove;
			});
		}
		if (beanManager != null && beanDescToRemove.bean == beanManager.getBean()) {
			beanManager = null;
			updatePanelOperator(null);
		}
		// Remove pk avant??? BeanManager.purgeBean le détruit déja via BeanManager.link...
		// new File(BeanManager.DEFAULTDIR + beanDescToRemove.bean.getClass().getSimpleName() + "-" + beanDescToRemove.name + BeanManager.SERIALIZEEXT).delete(); //Remove pk avant???
	}

	@Override
	public void subBeanChange(BeanDesc<?> owner) {
		LinkedList<FilterableTreeItem<Object>> nodeToTest = new LinkedList<>();
		nodeToTest.add((FilterableTreeItem<Object>) tree.getRoot());
		while (!nodeToTest.isEmpty()) {
			FilterableTreeItem<Object> node = nodeToTest.pop();
			for (TreeItem<Object> treeItem : node.getChildren()) {
				if (treeItem instanceof BeanTreeItem && ((BeanTreeItem) treeItem).beanDesc == owner)
					((BeanTreeItem) treeItem).reset();
				if (!treeItem.isLeaf())
					nodeToTest.push((FilterableTreeItem<Object>) treeItem);
			}
		}

	}

	private void updatePanelOperator(Object bean) {
		if (beanManager != null) {
			beanManager.saveIfChanged();
			beanManager = null;
		}
		if (bean != null) {
			beanManager = new BeanManager(bean, BeanManager.DEFAULTDIR);
			GridPane editor = beanManager.getEditor();
			ScrollPane sp = new ScrollPane(editor);
			sp.setFitToWidth(true);
			sp.setHbarPolicy(ScrollBarPolicy.NEVER);
			operatorPane.setCenter(sp);
			Button resetButton = new Button("Reset");
			resetButton.setOnAction(e -> {
				if (beanManager != null) {
					beanManager.reset();
					updatePanelOperator(beanManager.getBean());
				}
			});
			Button refreshButton = new Button("Refresh");
			refreshButton.setOnAction(e -> {
				if (beanManager != null)
					updatePanelOperator(beanManager.getBean());
			});
			Button saveButton = new Button("Save");
			saveButton.setOnAction(e -> {
				if (beanManager != null)
					beanManager.saveIfChanged();
			});
			Button cancelButton = new Button("Cancel");
			cancelButton.setOnAction(e -> {
				if (beanManager != null) {
					beanManager.load();
					beanManager.isEdited = false;
					BeanManager.createSubBeans(beanManager.getBean(), BeanManager.DEFAULTDIR);
					updatePanelOperator(beanManager.getBean());
				}
			});
			HBox vBox = new HBox(10, resetButton, refreshButton, saveButton, cancelButton);
			vBox.setAlignment(Pos.CENTER);
			vBox.setPadding(new Insets(3, 3, 3, 3));
			operatorPane.setBottom(vBox);
		} else
			operatorPane.setCenter(null);
	}

	@SuppressWarnings("unchecked")
	static void populateOperatorsTreeItems(FilterableTreeItem<Object> rootItem, List<Class<?>> operators, Function<Class<?>, String> nameProvider) {
		// Build a map of operators given the root package
		LinkedHashMap<String, Object> nodeMenuItemTools = new LinkedHashMap<>();
		for (Class<?> type : operators) {
			String typeName = nameProvider.apply(type);
			LinkedHashMap<String, Object> currentNode = nodeMenuItemTools;
			while (typeName.contains(".")) {
				int fi = typeName.indexOf(".");
				String parentName = typeName.substring(0, fi);
				parentName = parentName.replaceFirst(".", Character.toString(Character.toUpperCase(parentName.charAt(0))));
				typeName = typeName.substring(fi + 1);
				if (currentNode.get(parentName) == null) {
					LinkedHashMap<String, Object> sonNode = new LinkedHashMap<>();
					currentNode.put(parentName, sonNode);
					currentNode = sonNode;
				} else if (!(currentNode.get(parentName) instanceof HashMap)) {
					System.err.println("A tool have the same name than a tool package");
					System.exit(-1);
				} else
					currentNode = (LinkedHashMap<String, Object>) currentNode.get(parentName);
			}
			currentNode.put(typeName, type);
		}
		// Get a list of all bean files
		ArrayList<String> beansPath = new ArrayList<>();
		for (File beanFile : new File(BeanManager.DEFAULTDIR).listFiles())
			if (beanFile.getName().endsWith(BeanManager.SERIALIZEEXT))
				beansPath.add(beanFile.getName());
		// Build the tree of TreeItem and load files corresponding to operators
		LinkedList<HashMap<?, ?>> stTool = new LinkedList<>();
		LinkedList<ObservableList<TreeItem<Object>>> stDMTN = new LinkedList<>();
		stTool.push(nodeMenuItemTools);
		stDMTN.push(rootItem.getInternalChildren());
		while (!stTool.isEmpty()) {
			HashMap<?, ?> currToolNode = stTool.pop();
			ObservableList<TreeItem<Object>> currToolMenu = stDMTN.pop();
			currToolNode.forEach((tool, element) -> {
				if (element instanceof Class<?>) {
					currToolMenu.add(new OperatorClassTreeItem<>((Class<?>) element));
					loadSubBeanOperatorNode((Class<?>) element, beansPath);
				} else {
					FilterableTreeItem<Object> toolMenu = new FilterableTreeItem<>(tool, new ImageView(CLOSEDFOLDERICON));
					toolMenu.expandedProperty().addListener(e -> toolMenu.setGraphic(new ImageView(toolMenu.isExpanded() ? OPENFOLDERICON : CLOSEDFOLDERICON)));
					currToolMenu.add(toolMenu);
					stTool.push((HashMap<?, ?>) element);
					stDMTN.push(toolMenu.getInternalChildren());
				}
			});
		}

		// Sort tree to get firstly the package and then the operators
		LinkedList<FilterableTreeItem<Object>> todo = new LinkedList<>();
		todo.add(rootItem);
		while (!todo.isEmpty()) {
			FilterableTreeItem<Object> ti = todo.pop();
			ObservableList<TreeItem<Object>> children = ti.getInternalChildren();
			children.sort((r1, r2) -> {
				boolean isR1Op = r1 instanceof OperatorClassTreeItem;
				boolean isR2Op = r2 instanceof OperatorClassTreeItem;
				if (isR1Op && !isR2Op)
					return 1;
				else if (!isR1Op && isR2Op)
					return -1;
				return r1.toString().compareTo(r2.toString());
			});
			children.forEach(c -> todo.add((FilterableTreeItem<Object>) c));
		}
	}

	private static void loadSubBeanOperatorNode(Class<?> beanClass, ArrayList<String> beansPath) {
		String baseName = BeanDesc.getSimpleBaseDescriptor(beanClass);
		for (String fileName : beansPath)
			if (fileName.startsWith(baseName)) {
				String brotherName = fileName.substring(baseName.length());
				try {
					String brotherBeanName = brotherName.substring(0, brotherName.lastIndexOf("."));
					if (BeanEditor.getRegisterBean(beanClass, brotherBeanName) == null) {
						Object brotherBean = beanClass.getConstructor().newInstance();
						BeanDesc<?> beanDesc = BeanEditor.registerBean(brotherBean, brotherBeanName, BeanManager.DEFAULTDIR);
						if (!new BeanManager(brotherBean, BeanManager.DEFAULTDIR).load(new File(BeanManager.DEFAULTDIR + fileName)))
							BeanEditor.unregisterBean(beanDesc);
					}
				} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
					e.printStackTrace();
				}
			}
	}

	@Override
	public void loaded(Module module) {
		TreeView<Object> _tree = tree;
		if (_tree != null)
			FxUtils.runLaterIfNeeded(() -> ((FilterableTreeItem<Object>)_tree.getRoot()).getInternalChildren().add(new ModuleTreeItem(module, OperatorManager.getInternOperators().stream().filter(o -> o.getModule().equals(module)).collect(Collectors.toList()))));
	}
	
	@Override
	public Runnable modified(Module module) {
		return null;
	}

	@Override
	public void unloaded(Module module) {
		Platform.runLater(() -> {
			TreeView<Object> _tree = tree;
			if (_tree != null)
				((FilterableTreeItem<Object>)_tree.getRoot()).getInternalChildren().removeIf(child -> child instanceof ModuleTreeItem && ((ModuleTreeItem) child).module.equals(module));
			// TODO Bug javafx SelectedItemsReadOnlyObservableList itemsListChange = c; sert à rien et créer un leak quand je retire un module
			beanList.add(new BeanDesc<>(null, "", ""));
			beanList.remove(beanList.size() - 1);
		});
	}
}

class BeanTreeItem extends FilterableTreeItem<Object> {
	private boolean isFirstTime = true;
	public final BeanDesc<?> beanDesc;
	private boolean isResetting;

	@Override
	public ObservableList<TreeItem<Object>> getInternalChildren() {
		if (isFirstTime)
			populate();
		return super.getInternalChildren();
	}

	@Override
	public boolean isLeaf() {
		if (isFirstTime && !isResetting)
			populate();
		return super.isLeaf();
	}

	private void populate() {
		LinkedList<Object> bi = BeanManager.getSubBeans(beanDesc.bean, BeanManager.DEFAULTDIR);
		if (bi != null)
			for (Object subBean : bi) {
				TreeItem<Object> parent = this;
				boolean isInParents = false;
				try { // l'arbre peut étre en cours de chargement -> getBeanDesc renvoie null -> cancel
					BeanDesc<?> subBeanDesc = BeanEditor.getBeanDesc(subBean);
					while (parent instanceof BeanTreeItem) {
						if (((BeanTreeItem) parent).beanDesc == subBeanDesc) {
							isInParents = true;
							break;
						}
						parent = parent.getParent();
					}
					isFirstTime = false;
					if (!isInParents) {
						super.getInternalChildren().add(new BeanTreeItem(BeanEditor.getBeanDesc(subBean)));
						super.getInternalChildren().sort((r1, r2) -> r1.toString().compareTo(r2.toString()));
					}
				} catch (NullPointerException e) {
					isFirstTime = true;
					return;
				}
			}
	}

	public void reset() {
		isResetting = true;
		isFirstTime = true;
		super.getInternalChildren().clear();
		isResetting = false;
//		}
	}

	public BeanTreeItem(BeanDesc<?> beanDesc) {
		super(beanDesc, new ImageView(OperatorManager.isOperator(beanDesc.bean) ? Operators.OPERATORINSTICON : Operators.BEANICON));
		this.beanDesc = beanDesc;
	}
}

class OperatorClassTreeItem<T> extends FilterableTreeItem<Object> {
	private static final Image OPERATORCLASSICON = new Image(OperatorClassTreeItem.class.getResourceAsStream("/class.png"));
	public final Class<T> type;
	private boolean isFirstTime = true;
	private boolean isResetting;

	public OperatorClassTreeItem(Class<T> type) {
		super(type.getSimpleName(), new ImageView(OPERATORCLASSICON));
		this.type = type;
	}

	@Override
	public ObservableList<TreeItem<Object>> getInternalChildren() {
		if (isFirstTime)
			populate();
		return super.getInternalChildren();
	}

	@Override
	public boolean isLeaf() {
		if (isFirstTime && !isResetting)
			populate();
		return super.isLeaf();
	}

	private void populate() {
		isFirstTime = false;
		List<BeanDesc<T>> bi = BeanEditor.getBeanInstance(type);
		if (bi != null) {
			ArrayList<BeanTreeItem> beanTreeItems = new ArrayList<>();
			for (BeanDesc<T> beanDesc : bi)
				beanTreeItems.add(new BeanTreeItem(beanDesc));
			super.getInternalChildren().addAll(beanTreeItems);
			super.getInternalChildren().sort((r1, r2) -> r1.toString().compareTo(r2.toString()));
		}
	}

	public void reset() {
		isResetting = true;
		isFirstTime = true;
		super.getInternalChildren().clear();
		isResetting = false;
//		}
	}
}

class ModuleTreeItem extends FilterableTreeItem<Object> {
	private static final Image MODULECLASSICON = new Image(OperatorClassTreeItem.class.getResourceAsStream("/module.png"));
	public final Module module;

	public ModuleTreeItem(Module module, List<Class<?>> operators) {

		super(module.getName(), new ImageView(MODULECLASSICON));
		this.module = module;
		// Get the root package
		if (!operators.isEmpty()) {
			String name = operators.get(0).getName();
			name = name.substring(0, name.lastIndexOf("."));
			String[] operatorPath = name.split("\\.");
			for (int i = 1; i < operators.size(); i++) {
				String[] os = operators.get(i).getName().split("\\.");
				int max = Math.min(operatorPath.length, os.length);
				for (int j = 0; j < max; j++)
					if (!operatorPath[j].equals(os[j])) {
						String[] newOperatorPath = new String[j];
						System.arraycopy(operatorPath, 0, newOperatorPath, 0, newOperatorPath.length);
						operatorPath = newOperatorPath;
						break;
					}
			}
			
			int roorPackageNameSize = String.join(".", operatorPath).length() + 1;
			Operators.populateOperatorsTreeItems(this, operators, type -> type.getName().substring(roorPackageNameSize));
		}
	}
}

// Source: https://github.com/eclipse-efx/efxclipse-rt/blob/3.x/modules/ui/org.eclipse.fx.ui.controls/src/main/java/org/eclipse/fx/ui/controls/tree/FilterableTreeItem.java
class FilterableTreeItem<T> extends TreeItem<T> {
	private final ObservableList<TreeItem<T>> sourceList = FXCollections.observableArrayList();
	private final FilteredList<TreeItem<T>> filteredList = new FilteredList<>(this.sourceList);

	private final ObjectProperty<TreeItemPredicate<T>> predicate = new SimpleObjectProperty<>();

	public FilterableTreeItem(T value) {
		this(value, null);
	}

	public FilterableTreeItem(T value, Node graphic) {
		super(value, graphic);
		this.filteredList.predicateProperty().bind(Bindings.createObjectBinding(() -> {
			Predicate<TreeItem<T>> p = child -> {
				if (child instanceof FilterableTreeItem) {
					FilterableTreeItem<T> filterableChild = (FilterableTreeItem<T>) child;
					filterableChild.setPredicate(this.predicate.get());
				}
				if (this.predicate.get() == null)
					return true;
				if (child.getChildren().size() > 0)
					return true;
				return this.predicate.get().test(this, child.getValue());
			};
			return p;
		}, this.predicate));
		Bindings.bindContent(super.getChildren(), getBackingList());
	}
	
	@Override
	public ObservableList<TreeItem<T>> getChildren() {
		return super.getChildren();
	}

	protected ObservableList<TreeItem<T>> getBackingList() {
		return this.filteredList;
	}

	public ObservableList<TreeItem<T>> getInternalChildren() {
		return this.sourceList;
	}

	public final ObjectProperty<TreeItemPredicate<T>> predicateProperty() {
		return this.predicate;
	}

	public final TreeItemPredicate<T> getPredicate() {
		return this.predicate.get();
	}

	public final void setPredicate(TreeItemPredicate<T> predicate) {
		this.predicate.set(predicate);
	}
}

@FunctionalInterface
interface TreeItemPredicate<T> {
	boolean test(TreeItem<T> parent, T value);

	static <T> TreeItemPredicate<T> create(Predicate<T> predicate) {
		return (parent, value) -> predicate.test(value);
	}

}