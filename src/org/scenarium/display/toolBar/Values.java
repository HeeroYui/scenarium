/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.display.toolBar;

import javax.vecmath.Point2i;

import org.scenarium.display.PaintListener;
import org.scenarium.display.RenderPane;
import org.scenarium.display.ScenariumContainer;
import org.scenarium.display.drawer.GeographicalDrawer;
import org.scenarium.display.drawer.GeometricDrawer;
import org.scenarium.display.drawer.TheaterPanel;
import org.scenarium.display.toolbarclass.ExternalTool;
import org.scenarium.display.toolbarclass.Tool;
import org.scenarium.timescheduler.Scheduler;

import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;

public class Values extends ExternalTool implements PaintListener, ScenariumContainer {
	private RenderPane renderPaneValues;

	@Override
	public void adaptSizeToDrawableElement() {
		renderPaneValues.adaptSizeToDrawableElement();
	}

	@Override
	public void dispose() {
		super.dispose();
		TheaterPanel theaterPanel = renderPane.getTheaterPane();
		if (theaterPanel instanceof GeometricDrawer)
			((GeometricDrawer) theaterPanel).filterROI = false;
		theaterPanel.removePaintListener(this);
		if (renderPaneValues != null) {
			renderPaneValues.getTheaterPane().removePaintListener(this);
			renderPaneValues.close();
		}
		theaterPanel.repaint(false);
	}

	@Override
	public Point2i getDefaultToolBarLocation(String simpleName) {
		return null;
	}

	@Override
	public Region getRegion() {
		if (renderPane.getDataLoader() == null) {
			renderPaneValues = new RenderPane((Scheduler) null, renderPane.getTheaterPane().getDrawableElement(), this, null, false, false);
			renderPane.getTheaterPane().setDrawableElement(renderPane.getTheaterPane().getDrawableElement());
		} else
			renderPaneValues = new RenderPane(renderPane.getDataLoader(), this, false, false);
		renderPaneValues.excludeToolBar(Values.class);
		// renderPane.getDataLoader() != null ? renderPane.getTheaterPane().getDrawableElement() : renderPane.getDataLoader();
		TheaterPanel tpv = renderPaneValues.getTheaterPane();
		TheaterPanel tp = renderPane.getTheaterPane();
		if (tpv instanceof GeometricDrawer) {
			GeometricDrawer gtpv = (GeometricDrawer) tpv;
			GeometricDrawer gtp = (GeometricDrawer) tp;
			gtpv.filterROI = false;
			gtpv.addPaintListener(this);
			float scale = gtpv.getZoomScale();
			float[] roi = gtpv.getRoi();
			gtpv.setTransfom(roi[0] * scale, roi[1] * scale, scale);
			roi[2] = (float) (gtpv.getWidth() / scale);
			roi[3] = (float) (gtpv.getHeight() / scale);
			gtp.filterROI = true;
		} else if (tpv instanceof GeographicalDrawer) {
			GeographicalDrawer gtpv = (GeographicalDrawer) tpv;
			GeographicalDrawer gtp = (GeographicalDrawer) tp;
			gtpv.filterROI = false;
			gtpv.addPaintListener(this);
			gtpv.setZoom(Integer.MAX_VALUE);
			// float zoom = gtpv.getZoom();
			// float[] roi = gtpv.getRoi();
			// roi[2] = (float) (gtpv.getWidth() / scale);
			// roi[3] = (float) (gtpv.getHeight() / scale);
			gtp.filterROI = true;
		}
		StackPane sp = renderPaneValues.getPane();
		sp.setPrefSize(600, 600);
		tpv.ignoreRepaint = false;
		return sp;
	}

	@Override
	public Scheduler getScheduler() {
		return null;
	}

	@Override
	public int getSelectedElementFromTheaterEditor() {
		return -1;
	}

	@Override
	public boolean isDefaultToolBarAlwaysOnTop(String simpleName) {
		return false;
	}

	@Override
	public boolean isManagingAccelerator() {
		return false;
	}

	@Override
	public boolean isStatusBar() {
		return renderPaneValues.isStatusBar();
	}

	@Override
	public void paint(TheaterPanel source, float[] roi) {
		TheaterPanel theaterPanelValues = renderPaneValues.getTheaterPane();
		boolean forceToRedraw = false;
		if (renderPaneValues.getTheaterPane().getDrawableElement() != renderPane.getTheaterPane().getDrawableElement()) {
			renderPaneValues.getTheaterPane().setDrawableElement(renderPane.getTheaterPane().getDrawableElement());
			forceToRedraw = true;
		}
		if (theaterPanelValues instanceof GeometricDrawer) {
			final GeometricDrawer gtpv = (GeometricDrawer) theaterPanelValues;
			double scale = gtpv.getScale();
			GeometricDrawer toRedraw = source != gtpv ? gtpv : (GeometricDrawer) renderPane.getTheaterPane();
			toRedraw.removePaintListener(this);
			if (gtpv != source)
				toRedraw.setTransfom(-roi[0] * scale, -roi[1] * scale, scale);
			else
				toRedraw.setRoi((float) (-gtpv.getxTranslate() / scale), (float) (-gtpv.getyTranslate() / scale), (float) (source.getWidth() / scale), (float) (source.getHeight() / scale));
			if (forceToRedraw)
				toRedraw.repaint(false);
			toRedraw.addPaintListener(this);
		} else if (theaterPanelValues instanceof GeographicalDrawer) {
			final GeographicalDrawer gtpv = (GeographicalDrawer) theaterPanelValues;
			GeographicalDrawer toRedraw = source != gtpv ? gtpv : (GeographicalDrawer) renderPane.getTheaterPane();
			toRedraw.removePaintListener(this);
			// if (gtpv != source)
			// ;//toRedraw.setZoom(Integer.MAX_VALUE);
			// else
			// toRedraw.setRoi(0f, 0f, 0f, 0f);
			if (forceToRedraw)
				toRedraw.repaint(false);
			toRedraw.addPaintListener(this);
		}
	}

	@Override
	public void saveScenario() {}

	@Override
	public void showMessage(String message, boolean error) {
		renderPaneValues.showMessage(message, error);
	}

	@Override
	public void updateStatusBar(String... infos) {
		renderPaneValues.updateStatusBar(infos);
	}

	@Override
	public void updateToolView(Class<? extends Tool> toolClass, boolean isVisible) {}

	@Override
	public void showTool(Class<? extends Tool> toolClass) {}
}
