/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.display.toolBar;

import java.io.File;

import org.beanmanager.BeanDesc;
import org.beanmanager.BeanManager;
import org.scenarium.display.RenderPane;
import org.scenarium.display.drawer.TheaterPanel;
import org.scenarium.display.toolbarclass.ExternalTool;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;

public class DrawerProperties extends ExternalTool/* implements PropertyChangeFromBeanManagerListener */ {
	private BeanManager beanManager;

	@Override
	public void dispose() {
		super.dispose();
		if (beanManager.isEdited)
			renderPane.saveDrawerProperties();
	}

	@Override
	public Region getRegion() {
		TheaterPanel tp = renderPane.getTheaterPane();
		// TODO enregistrer bonne endroit
		beanManager = new BeanManager(tp, RenderPane.drawerPropertiesDir);
		File drawerFile = new File(RenderPane.drawerPropertiesDir + tp.getClass().getSimpleName() + BeanDesc.SEPARATOR + tp.getClass().getSimpleName() + ".txt");
		BorderPane bp = new BorderPane(beanManager.getEditor());
		Button okButton = new Button("Ok");
		BorderPane.setAlignment(okButton, Pos.CENTER);
		okButton.setOnAction(e1 -> {
			beanManager.saveIfChanged(drawerFile);
			renderPane.closeTool(DrawerProperties.class);
			dispose();
		});

		Button resetButton = new Button("Reset");
		resetButton.setOnAction(e1 -> {
			beanManager.reset();
			bp.setCenter(beanManager.getEditor());
		});
		Button refreshButton = new Button("Refresh");
		refreshButton.setOnAction(e1 -> bp.setCenter(beanManager.getEditor()));
		Button saveButton = new Button("save");
		saveButton.setOnAction(e1 -> beanManager.saveIfChanged(drawerFile));
		Button cancelButton = new Button("Cancel");
		cancelButton.setOnAction(e1 -> {
			beanManager.load(drawerFile);
			beanManager.isEdited = false;
			BeanManager.createSubBeans(beanManager.getBean(), BeanManager.DEFAULTDIR);
			bp.setCenter(beanManager.getEditor());
		});
		HBox vBox = new HBox(10, resetButton, refreshButton, saveButton, cancelButton, okButton);
		vBox.setAlignment(Pos.CENTER);
		vBox.setPadding(new Insets(3, 3, 3, 3));
		bp.setBottom(vBox);
		return bp;
	}

	// public void propertyChangeFromBeanManager(Object bean) {
	// renderPane.getTheaterPane().repaint(true);
	// }
}
