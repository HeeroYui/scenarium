/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.test.fx;

import javafx.application.Application;
import javafx.concurrent.Worker;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebHistory;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

public class FxWeb extends Application {
	private static final double ratio = 0.75;
	private static final String NEWTAB = "Nouvel onglet";

	public static void main(String[] args) {
		launch(args);
	}

	private TabPane tabPane;
	private ProgressIndicator progressIndicator;
	private TextField adressTextField;
	private Stage stage;
	private Button nextButton;

	private Button previousButton;

	private Tab createWebTab() {
		WebView wv = new WebView();
		WebEngine engine = wv.getEngine();
		engine.getLoadWorker().stateProperty().addListener((obs2, oldVal, newVal) -> {
			if (newVal == Worker.State.SUCCEEDED) {
				progressIndicator.setVisible(false);
				String title = engine.getTitle();
				tabPane.getSelectionModel().getSelectedItem().setText(title == null || title.isEmpty() ? "Sans nom" : title);
				stage.setTitle(title == null || title.isEmpty() ? "Navigateur Internet" : title);
				adressTextField.setText(engine.getLocation());
			} else {
				progressIndicator.setVisible(true);
				progressIndicator.progressProperty().bind(engine.getLoadWorker().progressProperty());
			}
		});
		WebHistory history = engine.getHistory();
		history.currentIndexProperty().addListener(e -> updateButton());
		return new Tab(NEWTAB, wv);
	}

	private WebEngine getTabWebViewEngine() {
		return ((WebView) tabPane.getSelectionModel().getSelectedItem().getContent()).getEngine();
	}

	@Override
	public void start(Stage primaryStage) {
		this.stage = primaryStage;
		Tab addTab = new Tab("+");
		tabPane = new TabPane(createWebTab(), addTab);
		tabPane.setTabMaxWidth(250);
		tabPane.getSelectionModel().selectedItemProperty().addListener((obs, oldTab, newTab) -> {
			if (newTab == addTab) {
				Tab createdTab = createWebTab();
				tabPane.getTabs().add(tabPane.getTabs().size() - 1, createdTab);
				tabPane.getSelectionModel().select(createdTab);
			}
			updateButton();
			adressTextField.setText(getTabWebViewEngine().getLocation());
		});

		adressTextField = new TextField();
		adressTextField.setPromptText("Saisir une adresse");
		TextField rf = new TextField();
		rf.setPromptText("Rechercher");
		adressTextField.setOnAction(e -> {
			String url = adressTextField.getText();
			if (!url.startsWith("http://") && !url.startsWith("https://")) {
				url = "https://" + url;
				adressTextField.setText(url);
			}
			nextButton.setDisable(false);
			getTabWebViewEngine().load(url);
		});
		rf.setOnAction(e -> getTabWebViewEngine().load("http://www.google.fr/search?q=" + rf.getText()));
		progressIndicator = new ProgressBar();
		progressIndicator.setVisible(false);
		progressIndicator.minWidthProperty().bind(adressTextField.heightProperty());
		progressIndicator.prefWidthProperty().bind(progressIndicator.minWidthProperty());
		progressIndicator.setPadding(new Insets(0, 0, 0, 4));
		progressIndicator.prefHeightProperty().bind(progressIndicator.widthProperty());
		HBox boxText = new HBox(adressTextField, rf, progressIndicator);
		boxText.setSpacing(5);
		adressTextField.prefWidthProperty().bind(boxText.widthProperty().multiply(ratio));
		rf.prefWidthProperty().bind(boxText.widthProperty().multiply(1 - ratio));
		HBox.setHgrow(boxText, Priority.ALWAYS);

		nextButton = new Button("", new ImageView(new Image(getClass().getResourceAsStream("/forward.png"))));
		previousButton = new Button("", new ImageView(new Image(getClass().getResourceAsStream("/backward.png"))));
		nextButton.setOnAction(e -> {
			WebHistory history = getTabWebViewEngine().getHistory();
			nextButton.setDisable(history.getCurrentIndex() == history.getEntries().size() - 2);
			previousButton.setDisable(false);
			history.go(1);
		});
		previousButton.setOnAction(e -> {
			WebHistory history = getTabWebViewEngine().getHistory();
			previousButton.setDisable(history.getCurrentIndex() == 1);
			nextButton.setDisable(false);
			history.go(-1);
		});
		nextButton.prefHeightProperty().bind(boxText.heightProperty());
		previousButton.prefHeightProperty().bind(boxText.heightProperty());
		updateButton();

		VBox.setVgrow(tabPane, Priority.ALWAYS);
		HBox headerBox = new HBox(new HBox(previousButton, nextButton), boxText, progressIndicator);
		headerBox.setSpacing(4);
		VBox.setVgrow(tabPane, Priority.ALWAYS);
		primaryStage.setTitle("Navigateur");
		primaryStage.setScene(new Scene(new VBox(headerBox, tabPane), 1024, 768));
		primaryStage.show();
	}

	private void updateButton() {
		WebHistory history = getTabWebViewEngine().getHistory();
		previousButton.setDisable(history.getCurrentIndex() == 0);
		nextButton.setDisable(history.getCurrentIndex() >= history.getEntries().size() - 1);
	}
}
