/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.test.fx;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;

public class Size extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage arg0) throws Exception { // Bug 9049350
		new Thread(() -> {
			try {
				Thread.sleep(100);
				Platform.runLater(() -> {
					arg0.setWidth(640);
					arg0.setHeight(480);
				});
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}).start();

		arg0.setWidth(640);
		arg0.setHeight(480);
		arg0.heightProperty().addListener((p, old, _new) -> System.out.println(_new));
		arg0.show();
	}
}
