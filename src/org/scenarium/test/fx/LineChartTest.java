/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.test.fx;

import java.text.DecimalFormat;
import java.util.Set;

import javax.vecmath.Point2i;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.control.Label;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.stage.Stage;

public class LineChartTest extends Application {
	private static double[][] datas;

	static {
		datas = new double[3][10000];
		for (int n = 0; n < datas[0].length; n++) {
			datas[0][n] = n / 1000.0;
			datas[1][n] = Math.exp(n / 1000.0);
			datas[2][n] = n / 1000.0;
		}
	}

	public static void main(String[] args) {
		launch(args);
	}

	private Data<Number, Number> first;
	private Data<Number, Number> last;
	private Canvas chartCanvas;
	private Canvas selectionCanvas;
	private Region drawRegion;
	private double ratioX;
	private double ratioY;
	private double offsetX;
	private double offsetY;
	private Point2i selectedPoint;
	private Point2i draggPoint;
	private NumberAxis xAxis;
	private NumberAxis yAxis;
	private double minX;
	private double maxX;
	private double minY;
	private double maxY;
	private double minZ;

	private double maxZ;

	private void computeBounds() {
		double[] xs = datas[0];
		double[] ys = datas[1];
		double minX = xs[0];
		double maxX = minX;
		double minY = ys[0];
		double maxY = minY;
		if (datas.length == 2)
			for (int i = xs.length - 1; i-- != 0;) {
				double val = xs[i];
				if (val < minX)
					minX = val;
				else if (val > maxX)
					maxX = val;
				val = ys[i];
				if (val < minY)
					minY = val;
				else if (val > maxY)
					maxY = val;
			}
		else {
			double[] zs = datas[2];
			double minZ = zs[0];
			double maxZ = minZ;
			for (int i = xs.length - 1; i-- != 0;) {
				double val = xs[i];
				if (val < minX)
					minX = val;
				else if (val > maxX)
					maxX = val;
				val = ys[i];
				if (val < minY)
					minY = val;
				else if (val > maxY)
					maxY = val;
				val = zs[i];
				if (val < minZ)
					minZ = val;
				else if (val > maxZ)
					maxZ = val;
			}
			this.minZ = minZ;
			this.maxZ = maxZ;
		}
		this.minX = minX;
		this.maxX = maxX;
		this.minY = minY;
		this.maxY = maxY;
	}

	private void drawChart() {
		GraphicsContext g = chartCanvas.getGraphicsContext2D();
		g.clearRect(0, 0, chartCanvas.getWidth(), chartCanvas.getHeight());
		double[] xDatas = datas[0];
		double[] yDatas = datas[1];
		Color color = Color.LIME;
		double oldX = (xDatas[xDatas.length - 1] - offsetX) * ratioX;
		double oldY = (yDatas[xDatas.length - 1] - offsetY) * ratioY;
		if (datas.length == 2) {
			g.setStroke(Color.LIME);
			for (int i = xDatas.length - 1; i-- != 0;) {
				double x = (xDatas[i] - offsetX) * ratioX;
				double y = (yDatas[i] - offsetY) * ratioY;
				g.strokeLine(oldX, oldY, x, y);
				oldX = x;
				oldY = y;
			}
		} else {
			// float value = (float) values[row][column];
			// if (value < 0)
			// value = 0;
			// return new Color(color.getRed() / 255.0f * value, color.getGreen() / 255.0f * value, color.getBlue() / 255.0f * value);
			double[] zDatas = datas[2];
			double offsetZ = minZ;
			double ratioZ = 1 / (maxZ - minZ);
			double red = color.getRed();
			double green = color.getGreen();
			double blue = color.getBlue();
			for (int i = xDatas.length - 1; i-- != 0;) {
				double x = (xDatas[i] - offsetX) * ratioX;
				double y = (yDatas[i] - offsetY) * ratioY;
				double z = (zDatas[i] - offsetZ) * ratioZ;
				if (z < 0)
					z = 0;
				else if (z > 1)
					z = 1;
				g.setStroke(new Color(z * red, z * green, z * blue, 1));
				g.strokeLine(oldX, oldY, x, y);
				oldX = x;
				oldY = y;
			}
		}
	}

	private void drawSelection() {
		GraphicsContext g = selectionCanvas.getGraphicsContext2D();
		g.clearRect(0, 0, selectionCanvas.getWidth(), selectionCanvas.getHeight());
		if (selectedPoint != null) {
			g.setStroke(Color.BLACK);
			g.strokeRect(selectedPoint.getX() - 0.5, selectedPoint.getY() - 0.5, draggPoint.getX() - selectedPoint.getX(), draggPoint.getY() - selectedPoint.getY());
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void start(Stage primaryStage) {
		computeBounds();
		// Création des séries.
		final LineChart.Series<Number, Number> series = new LineChart.Series<>();
		series.setName("cool");
		first = new LineChart.Data<>(minX, minY);
		last = new LineChart.Data<>(minX == maxX ? maxX + 1 : maxX, minY == maxY ? maxY + 1 : maxY);
		series.getData().addAll(first, last);
		// Création du graphique.
		xAxis = new NumberAxis(first.getXValue().doubleValue(), last.getXValue().doubleValue(), 1);
		// xAxis.setAutoRanging(true);
		xAxis.setLabel("x");
		xAxis.setLowerBound(first.getXValue().doubleValue());
		xAxis.setUpperBound(last.getXValue().doubleValue());
		yAxis = new NumberAxis(first.getYValue().doubleValue(), last.getYValue().doubleValue(), 1);
		// yAxis.setAutoRanging(true);
		yAxis.setLabel("y");
		yAxis.setLowerBound(first.getYValue().doubleValue());
		yAxis.setUpperBound(last.getYValue().doubleValue());
		updateTickUnit();

		final LineChart<Number, Number> chart = new LineChart<>(xAxis, yAxis);
		chart.setAnimated(false);
		// chart.setCreateSymbols(false);
		chart.setTitle("Fonction puissance");
		chart.getData().add(series);
		chart.setStyle("CHART_COLOR_1: transparent;");

		Set<Node> items = chart.lookupAll("Label.chart-legend-item");
		for (Node item : items) {
			Label label = (Label) item;
			Line line = new Line(0, 0, 6, 0);
			line.setStroke(Color.MEDIUMSEAGREEN);
			line.setStrokeWidth(2);
			label.setGraphic(line);
		}
		Pane drawPane = (Pane) chart.lookup(".chart-content");
		drawPane.setPadding(new Insets(5, 20, 0, 0));
		drawRegion = (Region) drawPane.getChildren().get(0);

		chart.setPadding(new Insets(0));
		final StackPane root = new StackPane();
		chartCanvas = new Canvas();
		chartCanvas.setManaged(false);
		chartCanvas.widthProperty().bind(drawRegion.widthProperty());
		chartCanvas.heightProperty().bind(drawRegion.heightProperty());
		selectionCanvas = new Canvas();
		selectionCanvas.setManaged(false);
		selectionCanvas.widthProperty().bind(drawRegion.widthProperty());
		selectionCanvas.heightProperty().bind(drawRegion.heightProperty());
		selectionCanvas.setVisible(false);
		selectionCanvas.setFocusTraversable(false);
		selectionCanvas.setPickOnBounds(false);
		root.getChildren().addAll(chart, chartCanvas, selectionCanvas);
		final Scene scene = new Scene(root, 640, 480);

		chartCanvas.setOnMousePressed(e -> {
			if (e.getButton() == MouseButton.PRIMARY)
				selectedPoint = new Point2i((int) e.getX(), (int) e.getY());
			selectionCanvas.setVisible(true);
		});
		chartCanvas.setOnMouseDragged(e -> {
			draggPoint = new Point2i((int) e.getX(), (int) e.getY());
			drawSelection();
		});
		chartCanvas.setOnMouseReleased(e -> {
			if (selectedPoint != null && draggPoint != null) {
				double beginX = selectedPoint.x / ratioX + offsetX;
				double endX = draggPoint.x / ratioX + offsetX;
				double beginY = draggPoint.y / ratioY + offsetY;
				double endY = selectedPoint.y / ratioY + offsetY;
				boolean isReinit = false;
				if (endX - beginX < 0) {
					first.setXValue(minX);
					last.setXValue(maxX);
					isReinit = true;
				}
				if (endY - beginY < 0) {
					first.setYValue(minY);
					last.setYValue(maxY);
					isReinit = true;
				}
				if (!isReinit) {
					if (endX - beginX > 1E-5) {
						first.setXValue(beginX);
						last.setXValue(endX);
					} else
						System.err.println("Too small X");
					if (endY - beginY > 1E-5) {
						first.setYValue(beginY);
						last.setYValue(endY);
					} else
						System.err.println("Too small Y");
				}
				updateTickUnit();
				Platform.runLater(() -> updateCanvasParam());
			}
			selectedPoint = null;
			draggPoint = null;
			drawSelection();
			selectionCanvas.setVisible(false);
		});
		drawRegion.localToSceneTransformProperty().addListener(e -> Platform.runLater(() -> updateCanvasPos()));
		chartCanvas.widthProperty().addListener(e -> Platform.runLater(() -> updateCanvasPos()));
		chartCanvas.heightProperty().addListener(e -> Platform.runLater(() -> updateCanvasPos()));
		primaryStage.setTitle("Test de LineChart");
		primaryStage.setScene(scene);
		primaryStage.show();
		// ScenicView.show(scene);
	}

	private void updateCanvasParam() {
		xAxis.setLowerBound(first.getXValue().doubleValue());
		xAxis.setUpperBound(last.getXValue().doubleValue());
		yAxis.setLowerBound(first.getYValue().doubleValue());
		yAxis.setUpperBound(last.getYValue().doubleValue());

		// Node node = first.getNode();
		// Point2D pos = node.localToScene(new Point2D(0, 0));
		// Bounds bounds = node.getBoundsInLocal();
		// double xBegin = pos.getX() + bounds.getMaxX() / 2.0;
		// double yBegin = pos.getY() + bounds.getMaxY() / 2.0;
		// node = last.getNode();
		// bounds = node.getBoundsInLocal();
		// pos = node.localToScene(new Point2D(0, 0));
		// double xEnd = pos.getX() + bounds.getMaxX() / 2.0;
		// double yEnd = pos.getY() + bounds.getMaxY() / 2.0;

		boolean needToRepaint = false;
		double oldVal = ratioX;
		ratioX = drawRegion.getWidth() / (last.getXValue().doubleValue() - first.getXValue().doubleValue());
		if (oldVal != ratioX)
			needToRepaint = true;
		oldVal = ratioY;
		ratioY = -drawRegion.getHeight() / (last.getYValue().doubleValue() - first.getYValue().doubleValue());
		if (oldVal != ratioY)
			needToRepaint = true;
		oldVal = offsetX;
		offsetX = first.getXValue().doubleValue();
		if (oldVal != offsetX)
			needToRepaint = true;
		oldVal = offsetY;
		offsetY = first.getYValue().doubleValue() - chartCanvas.getHeight() / ratioY;
		if (oldVal != offsetY)
			needToRepaint = true;
		if (needToRepaint)
			drawChart();
	}

	private void updateCanvasPos() {
		Point2D _pos = drawRegion.localToScene(new Point2D(0, 0));
		if (chartCanvas.getLayoutX() != _pos.getX())
			chartCanvas.setLayoutX(_pos.getX());
		if (chartCanvas.getLayoutY() != _pos.getY())
			chartCanvas.setLayoutY(_pos.getY());
		if (selectionCanvas.getLayoutX() != _pos.getX())
			selectionCanvas.setLayoutX(_pos.getX());
		if (selectionCanvas.getLayoutY() != _pos.getY())
			selectionCanvas.setLayoutY(_pos.getY());
		updateCanvasParam();
	}

	private void updateTickUnit() {
		double distX = Math.abs(last.getXValue().doubleValue() - first.getXValue().doubleValue());
		double distY = Math.abs(last.getYValue().doubleValue() - first.getYValue().doubleValue());
		xAxis.setTickUnit(distX / 10.0);
		yAxis.setTickUnit(distY / 10.0);
		xAxis.setTickLabelFormatter(new NumberAxis.DefaultFormatter(xAxis) {
			@Override
			public String toString(Number object) {
				double value = object.doubleValue();
				double d = distX;
				String pat = "###";
				if (d < 10)
					pat += ".";
				while (d < 10) {
					d *= 10;
					pat += "0";
				}
				double absValue = Math.abs(object.doubleValue());
				if (absValue > 100000)
					pat += "E0";
				return new DecimalFormat(pat).format(value);
			}
		});
		yAxis.setTickLabelFormatter(new NumberAxis.DefaultFormatter(xAxis) {
			@Override
			public String toString(Number object) {
				double value = object.doubleValue();
				double d = distY;
				String pat = "###";
				if (d < 10)
					pat += ".";
				while (d < 10) {
					d *= 10;
					pat += "#";
				}
				double absValue = Math.abs(object.doubleValue());
				if (absValue > 100000)
					pat += "E0";
				return new DecimalFormat(pat).format(value);
			}
		});
	}
}