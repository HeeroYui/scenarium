/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.test.fx.fx3D;

import javafx.application.Application;
import javafx.scene.DepthTest;
import javafx.scene.Group;
import javafx.scene.PerspectiveCamera;
import javafx.scene.Scene;
import javafx.scene.SceneAntialiasing;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Box;
import javafx.stage.Stage;

/** @author cmcastil */
public class MoleculeSampleApp extends Application {

	private static final double CAMERA_INITIAL_DISTANCE = -450;
	private static final double CAMERA_INITIAL_X_ANGLE = 70.0;
	private static final double CAMERA_INITIAL_Y_ANGLE = 320.0;
	private static final double CAMERA_NEAR_CLIP = 0.1;
	private static final double CAMERA_FAR_CLIP = 10000.0;
	private static final double AXIS_LENGTH = 250.0;
	private static final double CONTROL_MULTIPLIER = 0.1;
	private static final double SHIFT_MULTIPLIER = 10.0;
	private static final double MOUSE_SPEED = 0.1;
	private static final double ROTATION_SPEED = 2.0;
	private static final double TRACK_SPEED = 0.3;

	/** The main() method is ignored in correctly deployed JavaFX application. main() serves only as fallback in case the application can not be launched through deployment artifacts, e.g., in IDEs
	 * with limited FX support. NetBeans ignores main().
	 *
	 * @param args the command line arguments */
	public static void main(String[] args) {
		launch(args);
	}

	final Group root = new Group();
	final Xform axisGroup = new Xform();
	final Xform world = new Xform();
	final PerspectiveCamera camera = new PerspectiveCamera(true);
	final Xform cameraXform = new Xform();
	final Xform cameraXform2 = new Xform();

	final Xform cameraXform3 = new Xform();
	double mousePosX;
	double mousePosY;
	double mouseOldX;
	double mouseOldY;
	double mouseDeltaX;

	double mouseDeltaY;

	private void buildAxes() {
		System.out.println("buildAxes()");
		final PhongMaterial redMaterial = new PhongMaterial();
		redMaterial.setDiffuseColor(Color.DARKRED);
		redMaterial.setSpecularColor(Color.RED);

		final PhongMaterial greenMaterial = new PhongMaterial();
		greenMaterial.setDiffuseColor(Color.DARKGREEN);
		greenMaterial.setSpecularColor(Color.GREEN);

		final PhongMaterial blueMaterial = new PhongMaterial();
		blueMaterial.setDiffuseColor(Color.DARKBLUE);
		blueMaterial.setSpecularColor(Color.BLUE);

		final Box xAxis = new Box(AXIS_LENGTH, 1, 1);
		final Box yAxis = new Box(1, AXIS_LENGTH, 1);
		final Box zAxis = new Box(1, 1, AXIS_LENGTH);

		xAxis.setMaterial(redMaterial);
		yAxis.setMaterial(greenMaterial);
		zAxis.setMaterial(blueMaterial);

		axisGroup.getChildren().addAll(xAxis, yAxis, zAxis);
		axisGroup.setVisible(true);
		world.getChildren().addAll(axisGroup);
	}

	// private void buildScene() {
	// root.getChildren().add(world);
	// }
	private void buildCamera() {
		System.out.println("buildCamera()");
		root.getChildren().add(cameraXform);
		cameraXform.getChildren().add(cameraXform2);
		cameraXform2.getChildren().add(cameraXform3);
		cameraXform3.getChildren().add(camera);
		cameraXform3.setRotateZ(180.0);

		camera.setNearClip(CAMERA_NEAR_CLIP);
		camera.setFarClip(CAMERA_FAR_CLIP);
		camera.setTranslateZ(CAMERA_INITIAL_DISTANCE);
		cameraXform.ry.setAngle(CAMERA_INITIAL_Y_ANGLE);
		cameraXform.rx.setAngle(CAMERA_INITIAL_X_ANGLE);
	}

	private void handleKeyboard(Scene scene) {
		scene.setOnKeyPressed(event -> {
			switch (event.getCode()) {
			case Z:
				cameraXform2.t.setX(0.0);
				cameraXform2.t.setY(0.0);
				camera.setTranslateZ(CAMERA_INITIAL_DISTANCE);
				cameraXform.ry.setAngle(CAMERA_INITIAL_Y_ANGLE);
				cameraXform.rx.setAngle(CAMERA_INITIAL_X_ANGLE);
				break;
			case X:
				axisGroup.setVisible(!axisGroup.isVisible());
				break;
			case V:
				// moleculeGroup.setVisible(!moleculeGroup.isVisible());
				break;
			default:
				break;
			}
		});
	}

	private void handleMouse(Scene scene) {
		scene.setOnMousePressed(me -> {
			mousePosX = me.getSceneX();
			mousePosY = me.getSceneY();
			mouseOldX = me.getSceneX();
			mouseOldY = me.getSceneY();
		});
		scene.setOnMouseDragged(ev -> {
			mouseOldX = mousePosX;
			mouseOldY = mousePosY;
			mousePosX = ev.getSceneX();
			mousePosY = ev.getSceneY();
			mouseDeltaX = mousePosX - mouseOldX;
			mouseDeltaY = mousePosY - mouseOldY;

			double modifier = ev.isControlDown() ? CONTROL_MULTIPLIER : ev.isShiftDown() ? SHIFT_MULTIPLIER : 1.0;

			if (ev.isPrimaryButtonDown()) {
				cameraXform.ry.setAngle(cameraXform.ry.getAngle() - mouseDeltaX * MOUSE_SPEED * modifier * ROTATION_SPEED);
				cameraXform.rx.setAngle(cameraXform.rx.getAngle() + mouseDeltaY * MOUSE_SPEED * modifier * ROTATION_SPEED);
			} else if (ev.isSecondaryButtonDown()) {
				cameraXform2.t.setX(cameraXform2.t.getX() + mouseDeltaX * MOUSE_SPEED * modifier * TRACK_SPEED);
				cameraXform2.t.setY(cameraXform2.t.getY() + mouseDeltaY * MOUSE_SPEED * modifier * TRACK_SPEED);
			}
		});

		scene.setOnScroll((ev) -> {
			if (ev.getDeltaY() != 0) {
				double modifier = ev.isControlDown() ? CONTROL_MULTIPLIER : ev.isShiftDown() ? SHIFT_MULTIPLIER : 1.0;
				if (ev.getDeltaY() < 0)
					camera.setTranslateZ(camera.getTranslateZ() - modifier * 10);
				else
					camera.setTranslateZ(camera.getTranslateZ() + modifier * 10);
			}
		});
	}

	@Override
	public void start(Stage primaryStage) {
		System.out.println("start()");

		root.getChildren().add(world);
		root.setDepthTest(DepthTest.ENABLE);

		buildCamera();
		buildAxes();

		Scene scene = new Scene(root, 1024, 768, true, SceneAntialiasing.BALANCED);
		scene.setFill(Color.GREY);
		handleKeyboard(scene);
		handleMouse(scene);

		primaryStage.setTitle("Molecule Sample Application");
		primaryStage.setScene(scene);
		primaryStage.show();

		scene.setCamera(camera);
	}
}
