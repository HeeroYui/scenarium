/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.test.fx.fx3D;

import java.io.File;
import java.util.Random;

import org.scenarium.display.drawer.TheaterPanel;
import org.scenarium.test.fx.FxTest;

import com.interactivemesh.jfx.importer.obj.ObjModelImporter;

import javafx.geometry.Dimension2D;
import javafx.scene.DepthTest;
import javafx.scene.Group;
import javafx.scene.PerspectiveCamera;
import javafx.scene.SceneAntialiasing;
import javafx.scene.SubScene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Box;
import javafx.scene.shape.MeshView;
import javafx.scene.shape.TriangleMesh;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Scale;
import javafx.scene.transform.Translate;

public class PointCloudDrawer3DFx extends TheaterPanel {
	private static final double CAMERA_INITIAL_DISTANCE = -450;
	private static final double CAMERA_INITIAL_X_ANGLE = 70.0;
	private static final double CAMERA_INITIAL_Y_ANGLE = 320.0;
	private static final double CAMERA_NEAR_CLIP = 0.1;
	private static final double CAMERA_FAR_CLIP = 10000.0;
	private static final double AXIS_LENGTH = 100.0;
	private static final double CONTROL_MULTIPLIER = 0.1;
	private static final double SHIFT_MULTIPLIER = 10.0;

	private static final double MOUSE_SPEED = 0.1;
	private static final double ROTATION_SPEED = 2.0;
	private static final double TRACK_SPEED = 0.3;

	public static void main(String[] args) {
		new FxTest().launchIHM(args, () -> {
			PointCloudDrawer3DFx cfx = new PointCloudDrawer3DFx();
			return cfx.getRegion();
		});
	}

	final Group root = new Group();
	final Xform axisGroup = new Xform();
	final Xform world = new Xform();
	final PerspectiveCamera camera = new PerspectiveCamera(true);
	final Xform cameraXform = new Xform();
	final Xform cameraXform2 = new Xform();
	final Xform cameraXform3 = new Xform();

	final Xform sceneXform = new Xform();
	double mousePosX;
	double mousePosY;
	double mouseOldX;
	double mouseOldY;
	double mouseDeltaX;
	double mouseDeltaY;

	private ObjModelImporter omi;

	private void buildAxes() {
		System.out.println("buildAxes()");
		final PhongMaterial redMaterial = new PhongMaterial();
		redMaterial.setDiffuseColor(Color.DARKRED);
		redMaterial.setSpecularColor(Color.RED);

		final PhongMaterial greenMaterial = new PhongMaterial();
		greenMaterial.setDiffuseColor(Color.DARKGREEN);
		greenMaterial.setSpecularColor(Color.GREEN);

		final PhongMaterial blueMaterial = new PhongMaterial();
		blueMaterial.setDiffuseColor(Color.DARKBLUE);
		blueMaterial.setSpecularColor(Color.BLUE);

		final PhongMaterial blackMaterial = new PhongMaterial();
		blackMaterial.setDiffuseColor(Color.BLACK);
		blackMaterial.setSpecularColor(Color.BLACK);

		final Box xAxis = new Box(AXIS_LENGTH, 1, 1);
		final Box yAxis = new Box(1, AXIS_LENGTH, 1);
		final Box zAxis = new Box(1, 1, AXIS_LENGTH);
		final Box cAxis = new Box(1, 1, 1);

		xAxis.setMaterial(redMaterial);
		yAxis.setMaterial(greenMaterial);
		zAxis.setMaterial(blueMaterial);
		cAxis.setMaterial(blackMaterial);

		xAxis.setTranslateX(-AXIS_LENGTH / 2 - 0.5);
		yAxis.setTranslateY(-AXIS_LENGTH / 2 - 0.5);
		zAxis.setTranslateZ(-AXIS_LENGTH / 2 - 0.5);

		axisGroup.getChildren().addAll(xAxis, yAxis, zAxis, cAxis);
		// axisGroup.setTranslateX(100);
		axisGroup.setVisible(false);
		world.getChildren().addAll(axisGroup);
	}

	private void buildCamera() {
		System.out.println("buildCamera()");
		root.getChildren().add(cameraXform);
		cameraXform.getChildren().add(cameraXform2);
		cameraXform2.getChildren().add(cameraXform3);
		cameraXform3.getChildren().add(camera);
		cameraXform3.setRotateZ(180.0);

		camera.setNearClip(CAMERA_NEAR_CLIP);
		camera.setFarClip(CAMERA_FAR_CLIP);
		camera.setTranslateZ(CAMERA_INITIAL_DISTANCE);
		cameraXform.ry.setAngle(CAMERA_INITIAL_Y_ANGLE);
		cameraXform.rx.setAngle(CAMERA_INITIAL_X_ANGLE);
	}

	private void buildScene() {
		// TODO Auto-generated method stub
		Box[] boxs = new Box[5000];
		Random rand = new Random();
		for (int i = 0; i < boxs.length; i++) {
			Box point = new Box(5, 5, 5);
			// Sphere point = new Sphere(5);
			point.setTranslateX(-rand.nextDouble() * 500);
			point.setTranslateY(-rand.nextDouble() * 500);
			point.setTranslateZ(-rand.nextDouble() * 500);

			final PhongMaterial redMaterial = new PhongMaterial();
			redMaterial.setDiffuseColor(Color.YELLOW);
			redMaterial.setSpecularColor(Color.RED);

			point.setMaterial(redMaterial);
			boxs[i] = point;
			// rects[i] = new Rectangle(-rand.nextDouble() * 500, -rand.nextDouble() * 500, 6, 6);

		}
		sceneXform.getChildren().addAll(boxs);
		for (MeshView box : omi.getImport())
			sceneXform.getChildren().add(box);

		int size = 20;
		float dx = -50;
		float dy = -30;
		float dz = 20;
		float[] points = { -size / 2 + dx, size / 2 + dy, 0 - dz, -size / 2 + dx, -size / 2 + dy, 0 - dz, size / 2 + dx, size / 2 + dy, 0 - dz, size / 2 + dx, -size / 2 + dy, 0 - dz };
		float[] texCoords = { 1, 1, 1, 0, 0, 1, 0, 0 };
		int[] faces = { 2, 2, 1, 1, 0, 0, 2, 2, 3, 3, 1, 1 };
		TriangleMesh tm = new TriangleMesh();
		tm.getPoints().setAll(points);
		tm.getTexCoords().setAll(texCoords);
		tm.getFaces().setAll(faces);

		final MeshView rect = new MeshView(tm);
		rect.setMaterial(new PhongMaterial(Color.LIME));
		// rect.setCullFace(CullFace.NONE);
		sceneXform.getChildren().add(rect);
		// sceneXform.setRotate(cameraXform.rx.getAngle(), cameraXform.ry.getAngle(), 0);
		// AmbientLight ambient = new AmbientLight(); //Couleur simple
		// ambient.setColor(Color.rgb(0, 255, 0, 0.6));
		world.getChildren().addAll(sceneXform);
	}

	@Override
	public void fitDocument() {
		// TODO Auto-generated method stub

	}

	@Override
	public Dimension2D getDimension() {
		return new Dimension2D(1024, 768);
	}

	private Region getRegion() {
		omi = new ObjModelImporter();
		omi.read(new File("/home/revilloud/stilo.obj"));
		root.getChildren().add(world);
		root.setDepthTest(DepthTest.ENABLE);

		buildCamera();
		buildAxes();
		buildScene();

		SubScene subScene = new SubScene(root, 800, 600, true, SceneAntialiasing.BALANCED);
		subScene.setFill(Color.WHITE);
		handleKeyboard(subScene);
		handleMouse(subScene);

		subScene.setCamera(camera);
		return new Pane(subScene);
	}

	@Override
	public String[] getStatusBarInfo() {
		// TODO Auto-generated method stub
		return null;
	}

	private void handleKeyboard(SubScene subScene) {
		subScene.setOnKeyPressed(event -> {
			switch (event.getCode()) {
			case Z:
				cameraXform2.t.setX(0.0);
				cameraXform2.t.setY(0.0);
				camera.setTranslateZ(CAMERA_INITIAL_DISTANCE);
				cameraXform.ry.setAngle(CAMERA_INITIAL_Y_ANGLE);
				cameraXform.rx.setAngle(CAMERA_INITIAL_X_ANGLE);
				break;
			case X:
				axisGroup.setVisible(!axisGroup.isVisible());
				break;
			case V:
				// moleculeGroup.setVisible(!moleculeGroup.isVisible());
				break;
			default:
				break;
			}
		});
	}

	private void handleMouse(SubScene subScene) {
		subScene.setOnMousePressed(me -> {
			mousePosX = me.getSceneX();
			mousePosY = me.getSceneY();
			mouseOldX = me.getSceneX();
			mouseOldY = me.getSceneY();
		});
		subScene.setOnMouseDragged(ev -> {
			mouseOldX = mousePosX;
			mouseOldY = mousePosY;
			mousePosX = ev.getSceneX();
			mousePosY = ev.getSceneY();
			mouseDeltaX = mousePosX - mouseOldX;
			mouseDeltaY = mousePosY - mouseOldY;

			double modifier = ev.isControlDown() ? CONTROL_MULTIPLIER : ev.isShiftDown() ? SHIFT_MULTIPLIER : 1.0;

			if (ev.isPrimaryButtonDown()) {
				cameraXform.ry.setAngle(cameraXform.ry.getAngle() - mouseDeltaX * MOUSE_SPEED * modifier * ROTATION_SPEED);
				cameraXform.rx.setAngle(cameraXform.rx.getAngle() + mouseDeltaY * MOUSE_SPEED * modifier * ROTATION_SPEED);
			} else if (ev.isSecondaryButtonDown()) {
				cameraXform2.t.setX(cameraXform2.t.getX() + mouseDeltaX * MOUSE_SPEED * modifier * TRACK_SPEED);
				cameraXform2.t.setY(cameraXform2.t.getY() + mouseDeltaY * MOUSE_SPEED * modifier * TRACK_SPEED);
			}
			// sceneXform.setRotate(cameraXform.rx.getAngle(), cameraXform.ry.getAngle(), 0);

		});
		;
		subScene.setOnScroll((ev) -> {
			double modifier = ev.isControlDown() ? CONTROL_MULTIPLIER : ev.isShiftDown() ? SHIFT_MULTIPLIER : 1.0;
			if (ev.getDeltaY() < 0)
				camera.setTranslateZ(camera.getTranslateZ() - modifier * 10);
			else
				camera.setTranslateZ(camera.getTranslateZ() + modifier * 10);
		});
	}

	@Override
	public void paint(Object dataElement) {
		// TODO Auto-generated method stub
	}

	@Override
	protected boolean updateFilterWithPath(String[] filterPath, boolean value) {
		return false;
	}
}

class Shape3DRectangle extends TriangleMesh {

	public Shape3DRectangle(float Width, float Height) {
		float[] points = { -Width / 2, Height / 2, 0, // idx p0
				-Width / 2, -Height / 2, 0, // idx p1
				Width / 2, Height / 2, 0, // idx p2
				Width / 2, -Height / 2, 0 // idx p3
		};
		float[] texCoords = { 1, 1, // idx t0
				1, 0, // idx t1
				0, 1, // idx t2
				0, 0 // idx t3
		};
		/** points: 1 3 ------- texture: |\ | 1,1 1,0 | \ | ------- | \ | | | | \ | | | | \| ------- ------- 0,1 0,0 0 2
		 *
		 * texture[3] 0,0 maps to vertex 2 texture[2] 0,1 maps to vertex 0 texture[0] 1,1 maps to vertex 1 texture[1] 1,0 maps to vertex 3
		 *
		 * Two triangles define rectangular faces: p0, t0, p1, t1, p2, t2 // First triangle of a textured rectangle p0, t0, p2, t2, p3, t3 // Second triangle of a textured rectangle */

		// if you use the co-ordinates as defined in the above comment, it will be all messed up
		// int[] faces = {
		// 0, 0, 1, 1, 2, 2,
		// 0, 0, 2, 2, 3, 3
		// };

		// try defining faces in a counter-clockwise order to see what the difference is.
		// int[] faces = {
		// 2, 2, 1, 1, 0, 0,
		// 2, 2, 3, 3, 1, 1
		// };

		// try defining faces in a clockwise order to see what the difference is.
		int[] faces = { 2, 3, 0, 2, 1, 0, 2, 3, 1, 0, 3, 1 };

		this.getPoints().setAll(points);
		this.getTexCoords().setAll(texCoords);
		this.getFaces().setAll(faces);
	}
}

class Xform extends Group {

	public enum RotateOrder {
		XYZ, XZY, YXZ, YZX, ZXY, ZYX
	}

	public Translate t = new Translate();
	public Translate p = new Translate();
	public Translate ip = new Translate();
	public Rotate rx = new Rotate();

	{
		rx.setAxis(Rotate.X_AXIS);
	}

	public Rotate ry = new Rotate();

	{
		ry.setAxis(Rotate.Y_AXIS);
	}

	public Rotate rz = new Rotate();

	{
		rz.setAxis(Rotate.Z_AXIS);
	}

	public Scale s = new Scale();

	public Xform() {
		super();
		getTransforms().addAll(t, rz, ry, rx, s);
	}

	public Xform(RotateOrder rotateOrder) {
		super();
		// choose the order of rotations based on the rotateOrder
		switch (rotateOrder) {
		case XYZ:
			getTransforms().addAll(t, p, rz, ry, rx, s, ip);
			break;
		case XZY:
			getTransforms().addAll(t, p, ry, rz, rx, s, ip);
			break;
		case YXZ:
			getTransforms().addAll(t, p, rz, rx, ry, s, ip);
			break;
		case YZX:
			getTransforms().addAll(t, p, rx, rz, ry, s, ip); // For Camera
			break;
		case ZXY:
			getTransforms().addAll(t, p, ry, rx, rz, s, ip);
			break;
		case ZYX:
			getTransforms().addAll(t, p, rx, ry, rz, s, ip);
			break;
		}
	}

	public void reset() {
		t.setX(0.0);
		t.setY(0.0);
		t.setZ(0.0);
		rx.setAngle(0.0);
		ry.setAngle(0.0);
		rz.setAngle(0.0);
		s.setX(1.0);
		s.setY(1.0);
		s.setZ(1.0);
		p.setX(0.0);
		p.setY(0.0);
		p.setZ(0.0);
		ip.setX(0.0);
		ip.setY(0.0);
		ip.setZ(0.0);
	}

	public void resetTSP() {
		t.setX(0.0);
		t.setY(0.0);
		t.setZ(0.0);
		s.setX(1.0);
		s.setY(1.0);
		s.setZ(1.0);
		p.setX(0.0);
		p.setY(0.0);
		p.setZ(0.0);
		ip.setX(0.0);
		ip.setY(0.0);
		ip.setZ(0.0);
	}

	public void setPivot(double x, double y, double z) {
		p.setX(x);
		p.setY(y);
		p.setZ(z);
		ip.setX(-x);
		ip.setY(-y);
		ip.setZ(-z);
	}

	public void setRotate(double x, double y, double z) {
		rx.setAngle(x);
		ry.setAngle(y);
		rz.setAngle(z);
	}

	public void setRotateX(double x) {
		rx.setAngle(x);
	}

	public void setRotateY(double y) {
		ry.setAngle(y);
	}

	public void setRotateZ(double z) {
		rz.setAngle(z);
	}

	public void setRx(double x) {
		rx.setAngle(x);
	}

	public void setRy(double y) {
		ry.setAngle(y);
	}

	public void setRz(double z) {
		rz.setAngle(z);
	}

	public void setScale(double scaleFactor) {
		s.setX(scaleFactor);
		s.setY(scaleFactor);
		s.setZ(scaleFactor);
	}

	public void setScale(double x, double y, double z) {
		s.setX(x);
		s.setY(y);
		s.setZ(z);
	}

	public void setSx(double x) {
		s.setX(x);
	}

	public void setSy(double y) {
		s.setY(y);
	}

	public void setSz(double z) {
		s.setZ(z);
	}

	public void setTranslate(double x, double y) {
		t.setX(x);
		t.setY(y);
	}

	public void setTranslate(double x, double y, double z) {
		t.setX(x);
		t.setY(y);
		t.setZ(z);
	}

	// Cannot override these methods as they are final:
	// public void setTranslateX(double x) { t.setX(x); }
	// public void setTranslateY(double y) { t.setY(y); }
	// public void setTranslateZ(double z) { t.setZ(z); }
	// Use these methods instead:
	public void setTx(double x) {
		t.setX(x);
	}

	public void setTy(double y) {
		t.setY(y);
	}

	public void setTz(double z) {
		t.setZ(z);
	}

	@Override
	public String toString() {
		return "Xform[t = (" + t.getX() + ", " + t.getY() + ", " + t.getZ() + ")  " + "r = (" + rx.getAngle() + ", " + ry.getAngle() + ", " + rz.getAngle() + ")  " + "s = (" + s.getX() + ", " + s.getY() + ", " + s.getZ() + ")  " + "p = (" + p.getX()
				+ ", " + p.getY() + ", " + p.getZ() + ")  " + "ip = (" + ip.getX() + ", " + ip.getY() + ", " + ip.getZ() + ")]";
	}
}
