/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.test.fx;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class ResizableCanvas extends Canvas {

	public ResizableCanvas() {
		// Redraw canvas when size changes.
		widthProperty().addListener(evt -> draw());
		heightProperty().addListener(evt -> draw());
		minWidth(0);
		maxWidth(Double.MAX_VALUE);
		minHeight(0);
		maxHeight(Double.MAX_VALUE);
	}

	private void draw() {
		double width = getWidth();
		double height = getHeight();
		GraphicsContext gc = getGraphicsContext2D();
		gc.clearRect(0, 0, width, height);
		gc.setStroke(Color.RED);
		gc.strokeLine(0, 0, width, height);
		gc.strokeLine(0, height, width, 0);
	}

	@Override
	public boolean isResizable() {
		return true;
	}

	@Override
	public double prefHeight(double width) {
		return getHeight();
	}

	@Override
	public double prefWidth(double height) {
		return getWidth();
	}

	@Override
	public void resize(double width, double height) {
		super.resize(width, height);
		System.out.println("resize: " + width);
	}
}
