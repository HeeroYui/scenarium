/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.test;

import java.util.Arrays;
import java.util.List;

import com.sun.jna.Pointer;
import com.sun.jna.Structure;

public class ExtU_test_T extends Structure {
	public int ours_int32;
	public byte chien_boolean;
	public double vache_double;
	public double[] moule_1000_double = new double[1000];

	public ExtU_test_T() {
		super();
	}

	public byte getChien_boolean() {
		return chien_boolean;
	}

	@Override
	protected List<String> getFieldOrder() {
		return Arrays.asList("ours_int32", "chien_boolean", "vache_double", "moule_1000_double");
	}

	public double[] getMoule_1000_double() {
		return moule_1000_double;
	}

	public int getOurs_int32() {
		return ours_int32;
	}

	public double getVache_double() {
		return vache_double;
	}

	public void setChien_boolean(byte chien_boolean) {
		this.chien_boolean = chien_boolean;
		writeField("chien_boolean");
	}

	public void setMoule_1000_double(double[] moule_1000_double) {
		this.moule_1000_double = moule_1000_double;
		writeField("moule_1000_double");
	}

	public void setOurs_int32(int ours_int32) {
		this.ours_int32 = ours_int32;
		writeField("ours_int32");
	}

	public void setPointer(Pointer p) {
		useMemory(p);
	}

	public void setVache_double(double vache_double) {
		this.vache_double = vache_double;
		writeField("vache_double");
	}
}