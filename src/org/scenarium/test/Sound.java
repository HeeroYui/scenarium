/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.test;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

public class Sound {
	public static float SAMPLE_RATE = 44100;
	public static int hz = 2093;
	public static double freq = 1;
	public static double vol = 1;
	public static boolean left = true;

	public static void main(String[] args) throws Exception {
		tone();
	}

	public static void tone() throws LineUnavailableException {
		byte[] buf = new byte[2];
		AudioFormat af = new AudioFormat(SAMPLE_RATE, 8, 2, true, false);
		SourceDataLine sdl = AudioSystem.getSourceDataLine(af);
		sdl.open(af);
		sdl.start();
		int i = 0;
		while (true) {
			i++;
			double time = freq * SAMPLE_RATE;
			double angle = i / (SAMPLE_RATE / hz) * 2.0 * Math.PI;
			if (left)
				if (i % time < time / 2)
					buf[0] = (byte) (Math.sin(angle) * 127.0 * vol);
				else
					buf[0] = 0;
			sdl.write(buf, 0, 2);
		}
		// sdl.drain();
		// sdl.stop();
		// sdl.close();
	}
}