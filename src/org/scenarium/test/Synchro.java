/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.test;

import java.util.concurrent.locks.LockSupport;

public class Synchro {
	public static void main(String[] args) {
		Thread t = new Thread(() -> {
			while (true) {
				LockSupport.park();
				System.out.println("cool");
			}
		});
		t.start();

		for (int i = 0; i < 1000; i++) {
			try {
				Thread.sleep((long) (Math.random() * 1000));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			for (int j = 0; j < 100; j++)
				LockSupport.unpark(t);
		}

	}
}
