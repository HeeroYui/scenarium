/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.test.bugSize;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.stage.Stage;

public class TestGridPane extends Application {
	public static void main(String[] args) {
		launch(args);
	}

	private boolean firstTime = true;

	@Override
	public void start(Stage primaryStage) throws Exception {
		Button b = new Button("up");
		b.setPrefSize(250, 250);
		b.setOnMousePressed(e -> {
			if (firstTime) {
				b.setPrefHeight(500);
				firstTime = false;
			}
		});
		b.heightProperty().addListener(e -> {
			// new Thread(() -> {
			// Platform.runLater(() -> ((Stage) b.getScene().getWindow()).sizeToScene());
			// }).start();
			((Stage) b.getScene().getWindow()).sizeToScene();
		});
		primaryStage.setScene(new Scene(new ScrollPane(b), -1, -1));
		primaryStage.show();
	}
}
