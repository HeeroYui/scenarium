/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.test.versionconverter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

public class PsfConverter {
	public static File compactToNormalPsf(File inputFile) throws IOException {
		try (FileInputStream fis = new FileInputStream(inputFile)) {
			if (fis.read() == 0) // Lecture normale
				return null;
			try (FileChannel inChannel = fis.getChannel()) {
				MappedByteBuffer buffer = inChannel.map(FileChannel.MapMode.READ_ONLY, 0, inChannel.size());
				buffer.load();
				File outFile = new File(inputFile.getAbsolutePath() + "n");
				if (outFile.exists())
					outFile.delete();
				try (RandomAccessFile outBuffer = new RandomAccessFile(outFile, "rw")) {
					buffer.get();
					outBuffer.writeByte(0);
					int nbInput = buffer.getInt();
					outBuffer.writeBytes(Integer.toString(nbInput));
					for (int i = 0; i < nbInput; i++)
						outBuffer.writeBytes(System.lineSeparator() + getString(buffer) + ":" + i);
					while (buffer.hasRemaining())
						outBuffer.writeBytes(System.lineSeparator() + buffer.getLong() + ":" + buffer.getInt() + ":" + buffer.getLong());
				}
				return outFile;
			}
		}
	}

	private static String getString(ByteBuffer bb) {
		byte[] stringBytes = new byte[bb.getInt()];
		bb.get(stringBytes);
		return new String(stringBytes);
	}

	public static void main(String[] args) {
		FilesGrabber.getAllFiles(new File("/mnt/DATA1/Log/Scenarium"), fileName -> fileName.endsWith(".psf") && fileName.startsWith("Rec")).stream().forEach(inputFile -> {
			System.out.println("Converting psf file: " + inputFile);
			try {
				try (FileInputStream fis = new FileInputStream(inputFile)) {
					boolean compactFile = fis.read() == 1;
					if (compactFile) {
						File tempFile = compactToNormalPsf(inputFile);
						normalToCompactPsf(tempFile, true);
						tempFile.delete();
					} else
						normalToCompactPsf(inputFile, true);
				}
				System.out.println("Converting done");
			} catch (Exception e) {
				System.err.println("failed to process: " + inputFile);
				e.printStackTrace();
			}
		});
	}

	public static void normalToCompactPsf(File inputFile, boolean timeOfIssue) throws IOException {
		try (BufferedReader br = new BufferedReader(new FileReader(inputFile))) {
			if (br.read() != 0)
				return;
			try (RandomAccessFile raf = new RandomAccessFile(new File(inputFile.getAbsolutePath() + "c"), "rw")) {
				int nbScenario = Integer.parseInt(br.readLine());
				raf.write(1);
				raf.writeInt(nbScenario);
				for (int i = 0; i < nbScenario; i++) {
					String line = br.readLine();
					int id = line.indexOf(":");
					putString(raf, line.substring(0, id));
				}
				String line = null;
				while ((line = br.readLine()) != null)
					try {
						int i1 = line.indexOf(":");
						int i2 = line.lastIndexOf(":");
						long ts = Long.parseLong(line.substring(0, i1));
						int scenarioId = Integer.parseInt(line.substring(i1 + 1, i2));
						long seekIndex = Long.parseLong(line.substring(i2 + 1));
						raf.writeLong(ts);
						if (timeOfIssue)
							raf.writeLong(ts);
						raf.writeInt(scenarioId);
						raf.writeLong(seekIndex);
					} catch (NumberFormatException e) {
						br.close();
						throw new IllegalArgumentException(inputFile + " is corrupted\n" + "Line: " + line);
					}
			}
		}
	}

	private static void putString(RandomAccessFile raf, String value) throws IOException {
		byte[] nameb = value.getBytes();
		raf.writeInt(nameb.length);
		raf.write(nameb);
	}
}
