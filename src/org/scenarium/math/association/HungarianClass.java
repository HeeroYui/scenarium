/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.math.association;

public class HungarianClass {
	private static final int INFINITY_HUNGARIAN = 100000000;
	private int n;
	private int[][] cost;
	private int[] lx;
	private int[] ly;
	private int[] xy;
	private int[] yx;
	private boolean[] s;
	private boolean[] t;
	private int[] slack;
	private int[] slackx;
	private int[] prev;
	private int max_match;

	public HungarianClass(int Nbelem) {
		n = Nbelem;
		cost = new int[n][n];
		lx = new int[n];
		ly = new int[n];
		xy = new int[n];
		yx = new int[n];
		s = new boolean[n];
		t = new boolean[n];
		slack = new int[n];
		slackx = new int[n];
		prev = new int[n];
	}

	private void add_to_tree(int x, int prevx) {
		// x - current vertex,prevx - vertex from X before x in the alternating path,
		// so we add edges (prevx, xy[x]), (xy[x], x)
		s[x] = true; // add x to S
		prev[x] = prevx; // we need this when augmenting
		for (int y = 0; y < n; y++) // update slacks, because we add new vertex to S
			if (lx[x] + ly[y] - cost[x][y] < slack[y]) {
				slack[y] = lx[x] + ly[y] - cost[x][y];
				slackx[y] = x;
			}
	}

	private void augment() {
		if (max_match == n)
			return; // check wether matching is already perfect
		int x, y, root = 0; // just counters and root vertex
		int wr = 0, rd = 0; // wr,rd - write and read
		int[] q = new int[n]; // q - queue for bfs, pos in queue
		for (x = 0; x < n; x++)
			s[x] = false;

		for (x = 0; x < n; x++)
			t[x] = false;

		for (x = 0; x < n; x++)
			prev[x] = -1;

		for (x = 0; x < n; x++)
			// finding root of the tree
			if (xy[x] == -1) {
				q[wr++] = root = x;
				prev[x] = -2;
				s[x] = true;
				break;
			}

		for (y = 0; y < n; y++) { // initializing slack array
			slack[y] = lx[root] + ly[y] - cost[root][y];
			slackx[y] = root;
		}
		// second part of augment() function
		while (true) { // main cycle
			while (rd < wr) { // building tree with bfs cycle
				x = q[rd++]; // current vertex from X part
				for (y = 0; y < n; y++)
					// iterate through all edges in equality graph
					if (cost[x][y] == lx[x] + ly[y] && !t[y]) {
						if (yx[y] == -1)
							break; // an exposed vertex in Y found, so
						// augmenting path exists!
						t[y] = true; // else just add y to T,
						q[wr++] = yx[y]; // add vertex yx[y], which is matched
						// with y, to the queue
						add_to_tree(yx[y], x); // add edges (x,y) and (y,yx[y]) to the tree
					}
				if (y < n)
					break; // augmenting path found!
			}
			if (y < n)
				break; // augmenting path found!
			update_labels(); // augmenting path not found, so improve labeling
			wr = rd = 0;
			for (y = 0; y < n; y++)
				// in this cycle we add edges that were added to the equality graph as a
				// result of improving the labeling, we add edge (slackx[y], y) to the tree if
				// and only if !T[y] && slack[y] == 0, also with this edge we add another one
				// (y, yx[y]) or augment the matching, if y was exposed
				if (!t[y] && slack[y] == 0) {
					if (yx[y] == -1) // exposed vertex in Y found - augmenting path exists!
					{
						x = slackx[y];
						break;
					}
					t[y] = true; // else just add y to T,
					if (!s[yx[y]]) {
						q[wr++] = yx[y]; // add vertex yx[y], which is matched with
						// y, to the queue
						add_to_tree(yx[y], slackx[y]); // and add edges (x,y) and (y,
						// yx[y]) to the tree
					}
				}
			if (y < n)
				break; // augmenting path found!
		}

		if (y < n) { // we found augmenting path!
			max_match++; // increment matching
			// in this cycle we inverse edges along augmenting path
			for (int cx = x, cy = y, ty; cx != -2; cx = prev[cx], cy = ty) {
				ty = xy[cx];
				yx[cy] = cx;
				xy[cx] = cy;
			}
			augment(); // recall function, go to step 1 of the algorithm
		}
	}

	public void evaluateAlgorithm(int[][] dataMat) {
		cost = dataMat.clone();
		max_match = 0; // number of vertices in current matching
		for (int x = 0; x < n; x++)
			xy[x] = -1;
		for (int x = 0; x < n; x++)
			yx[x] = -1;
		init_labels(); // step 0
		augment(); // steps 1-3
	}

	public int[] getxyIn() {
		return xy.clone();
	}

	public int[] getyxIn() {
		return yx.clone();
	}

	private void init_labels() {
		for (int x = 0; x < n; x++)
			lx[x] = 0;
		for (int x = 0; x < n; x++)
			ly[x] = 0;

		for (int x = 0; x < n; x++)
			for (int y = 0; y < n; y++)
				lx[x] = lx[x] > cost[x][y] ? lx[x] : cost[x][y];
	}

	private void update_labels() {
		int x, y, delta = INFINITY_HUNGARIAN; // init delta as infinity
		for (y = 0; y < n; y++) // calculate delta using slack
			if (!t[y])
				delta = delta < slack[y] ? delta : slack[y];
		for (x = 0; x < n; x++) // update X labels
			if (s[x])
				lx[x] -= delta;
		for (y = 0; y < n; y++) // update Y labels
			if (t[y])
				ly[y] += delta;
		for (y = 0; y < n; y++) // update slack array
			if (!t[y])
				slack[y] -= delta;
	}
}
