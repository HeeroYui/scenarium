/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.math.association;

import static java.lang.Math.PI;
import static java.lang.Math.abs;
import static java.lang.Math.pow;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AssocOdaHung<T extends Piste> {
	private static boolean comparePisteList(ArrayList<PisteTest> pistes, List<Piste> pistes2) {
		if (pistes.size() != pistes2.size())
			return false;
		for (int i = 0; i < pistes.size(); i++)
			if (!pistes.get(i).equals(pistes2.get(i)))
				return false;
		return true;
	}

	public static void main(String[] args) {
		AssocOdaHung<PisteTest> aoh = new AssocOdaHung<>(0.5f, 0.9f, 3, true);
		aoh.update(new ArrayList<>(Arrays.asList(new CibleTest[] { new CibleTest(1), new CibleTest(2), new CibleTest(4) })));
		if (aoh.pistes.size() != 3 || !comparePisteList(aoh.pistes, Arrays.asList(new Piste[] { new PisteTest(0, 1f, 0.5f), new PisteTest(1, 2f, 0.5f), new PisteTest(2, 4f, 0.5f) }))) {
			System.err.println("Regression!!!");
			System.exit(-1);
		}
		System.out.println();
		aoh.update(new ArrayList<>(Arrays.asList(new CibleTest[] { new CibleTest(1.1f), new CibleTest(2.3f), new CibleTest(4.2f) })));
		if (aoh.pistes.size() != 3 || !comparePisteList(aoh.pistes, Arrays.asList(new Piste[] { new PisteTest(0, 1.1f, 0.84669334f), new PisteTest(1, 2.3f, 0.84669334f), new PisteTest(2, 4.2f, 0.84669334f) })))
			System.err.println("Regression!!!");
		// System.exit(-1);
		System.out.println();
		aoh.update(new ArrayList<>(Arrays.asList(new CibleTest[] { new CibleTest(1.3f), new CibleTest(2.0f), new CibleTest(4.7f), new CibleTest(6.7f) })));
		if (aoh.pistes.size() != 4 || !comparePisteList(aoh.pistes, Arrays.asList(new Piste[] { new PisteTest(0, 1.3f, 0.9728101f), new PisteTest(1, 2f, 0.9728101f), new PisteTest(2, 4.7f, 0.9728101f), new PisteTest(3, 6.7f, 0.5f) })))
			System.err.println("Regression!!!");
		// System.exit(-1);
		System.out.println();
		aoh.update(new ArrayList<>(Arrays.asList(new CibleTest[] { new CibleTest(0.0f), new CibleTest(2.0f), new CibleTest(4.7f) })));
		if (aoh.pistes.size() != 4 || !comparePisteList(aoh.pistes, Arrays.asList(new Piste[] { new PisteTest(0, 1.3f, 0.8468071f), new PisteTest(1, 2f, 1), new PisteTest(2, 4.7f, 1), new PisteTest(4, 0f, 0.5f) })))
			System.err.println("Regression!!!");
		// System.exit(-1);
		System.out.println();
		aoh.update(new ArrayList<>(Arrays.asList(new CibleTest[] { new CibleTest(1.1f), new CibleTest(2.0f), new CibleTest(4.7f) })));
		if (aoh.pistes.size() != 3 || !comparePisteList(aoh.pistes, Arrays.asList(new Piste[] { new PisteTest(0, 1.1f, 0.9728615f), new PisteTest(1, 2f, 1), new PisteTest(2, 4.7f, 1) })))
			System.err.println("Regression!!!");
		// System.exit(-1);
		System.out.println();
		aoh.update(new ArrayList<>(Arrays.asList(new CibleTest[] { new CibleTest(1.3f), new CibleTest(2.0f), new CibleTest(4.7f) })));
		if (aoh.pistes.size() != 3 || !comparePisteList(aoh.pistes, Arrays.asList(new Piste[] { new PisteTest(0, 1.3f, 1), new PisteTest(1, 2.0f, 1), new PisteTest(2, 4.7f, 1) })))
			System.err.println("Regression!!!");
		// System.exit(-1);
		System.out.println();
		aoh.update(new ArrayList<>(Arrays.asList(new CibleTest[] { new CibleTest(1.3f), new CibleTest(2.0f), new CibleTest(4.7f) })));
		if (aoh.pistes.size() != 3 || !comparePisteList(aoh.pistes, Arrays.asList(new Piste[] { new PisteTest(0, 1.3f, 1), new PisteTest(1, 2f, 1), new PisteTest(2, 4.7f, 1) })))
			System.err.println("Regression!!!");
		// System.exit(-1);
		System.out.println();
		aoh.update(new ArrayList<>(Arrays.asList(new CibleTest[] { new CibleTest(1.1f), new CibleTest(1.49f), new CibleTest(2.0f), new CibleTest(4.7f) })));
		if (aoh.pistes.size() != 4 || !comparePisteList(aoh.pistes, Arrays.asList(new Piste[] { new PisteTest(0, 1.49f, 1), new PisteTest(1, 2f, 1), new PisteTest(2, 4.7f, 1), new PisteTest(5, 1.1f, 0.5f) })))
			System.err.println("Regression!!!");
		// System.exit(-1);
		System.out.println();
	}

	private float taux_massecroyance;
	private float alphaEvolConfidence;
	private int nbIterConf;

	private ArrayList<T> pistes = new ArrayList<>();

	private boolean verbose;

	public AssocOdaHung(float taux_massecroyance, float alphaEvolConfidence, int nbIterConf, boolean verbose) {
		this.taux_massecroyance = taux_massecroyance;
		this.alphaEvolConfidence = alphaEvolConfidence;
		this.nbIterConf = nbIterConf;
		this.verbose = verbose;
	}

	@SuppressWarnings("unchecked")
	@Override
	public AssocOdaHung<T> clone() {
		AssocOdaHung<T> a = new AssocOdaHung<>(taux_massecroyance, alphaEvolConfidence, nbIterConf, verbose);
		ArrayList<T> pistes = new ArrayList<>();
		for (T piste : this.pistes)
			pistes.add((T) piste.clone());
		a.pistes = pistes;
		return a;
	}

	public ArrayList<T> getPistes() {
		return pistes;
	}

	@SuppressWarnings("unchecked")
	public int update(ArrayList<Cible> cibles) {
		if (cibles.isEmpty()) {
			// On diminue les croyances
			for (Piste piste : pistes) {
				piste.evolutionConfidence(alphaEvolConfidence, false, nbIterConf);
				piste.setStatus(PisteStatus.MAINTAINED);
			}
			pistes.removeIf(piste -> {
				if (piste.getConfidence() <= 0) {
					if (verbose)
						System.out.println("Remove Piste: " + piste);
					return true;
				}
				return false;
			});
			return pistes.size();
		}
		if (pistes.isEmpty()) {
			for (Cible cible : cibles) {
				Piste newPiste = cible.createPisteFromCible();
				pistes.add((T) newPiste);
				if (verbose)
					System.out.println("Create piste: " + newPiste);
			}
			return pistes.size();
		}

		// Calcul de la distance et des masses de croyance initiale entre cibles et pistes
		int nbCible = cibles.size();
		int nbPiste = pistes.size();
		float[][] M_Y_Mat = new float[nbCible][nbPiste];
		float[][] M_notY_Mat = new float[nbCible][nbPiste];

		for (int icib = 0; icib < nbCible; icib++) {
			float[] currM_notY_Mat = M_notY_Mat[icib];
			float[] currM_Y_Mat = M_Y_Mat[icib];
			for (int ipis = 0; ipis < nbPiste; ipis++) {
				// Calcul des masses de croyance initiale
				float disimalarity = 1 - pistes.get(ipis).matchScore(cibles.get(icib));
				if (disimalarity < taux_massecroyance) {
					currM_notY_Mat[ipis] = 0;
					currM_Y_Mat[ipis] = (float) ((1 - sin(PI * (disimalarity / taux_massecroyance - 0.5f))) / 2.0f); // PK????
				} else {
					currM_Y_Mat[ipis] = 0;
					currM_notY_Mat[ipis] = (float) ((1 - sin(PI * ((1.0 - disimalarity) / (1.0 - taux_massecroyance) - 0.5f))) / 2.0f);
				}
			}
		}

		// %==================================================
		// %
		// % PREMIERE ETAPE : Xi en relation avec Yi
		// % Association des cibles vers les pistes
		// %==================================================
		float[][] m_cr_sub_xy = new float[nbCible][nbPiste];
		float[][] m_cr_xy = new float[nbCible][nbPiste + 2];

		for (int icib = 0; icib < nbCible; icib++) { // ** Calcul des masse de croyance
			float[] currM_Y_Mat = M_Y_Mat[icib];
			float m_Vide = 1;
			// float m_Omega = 1;
			float[] m_Y = new float[nbPiste];
			for (int ipis = 0; ipis < nbPiste; ipis++) {
				m_Y[ipis] = currM_Y_Mat[ipis];
				for (int ipis2 = 0; ipis2 < nbPiste; ipis2++)
					if (ipis2 != ipis)
						m_Y[ipis] *= 1 - currM_Y_Mat[ipis2];
				m_Vide *= M_notY_Mat[icib][ipis];
				// m_Omega *= 1.0 - currM_Y_Mat[ipis];
			}
			// m_Omega -= m_Vide;
			// ** Normalisation
			float K;
			K = m_Vide + 0;
			for (int ipis = 0; ipis < nbPiste; ipis++)
				K += m_Y[ipis];

			if (K != 0) {
				m_Vide /= K;
				// m_Omega /= K;
				for (int ipis = 0; ipis < nbPiste; ipis++)
					m_Y[ipis] /= K;
			}

			// Affectation des résultat dans les matrices dédiées :
			float[] currM_cr_sub_xy = m_cr_sub_xy[icib];
			float[] currM_cr_xy = m_cr_xy[icib];
			for (int ipis = 0; ipis < nbPiste; ipis++) {
				currM_cr_sub_xy[ipis] = m_Y[ipis];
				currM_cr_xy[ipis] = m_Y[ipis];
			}
			currM_cr_xy[nbPiste] = m_Vide;
			// currM_cr_xy[nbPiste + 1] = m_Omega;
		}

		// %==================================================
		// %
		// % DEUXIEME ETAPE : Yi en relation avec Xi
		// % association des pistes vers les cibles
		// %==================================================

		float[][] m_cr_sub_yx = new float[nbPiste][nbCible];
		float[][] m_cr_yx = new float[nbPiste][nbCible + 2];
		for (int ipis = 0; ipis < nbPiste; ipis++) { // ** Calcul des masse de croyance
			float m_Vide = 1;
			// float m_Omega = 1;
			float[] m_Y = new float[nbCible];
			for (int icib = 0; icib < nbCible; icib++) {
				m_Y[icib] = M_Y_Mat[icib][ipis];
				for (int icib2 = 0; icib2 < nbCible; icib2++)
					if (icib2 != icib)
						m_Y[icib] *= 1 - M_Y_Mat[icib2][ipis];
				m_Vide *= M_notY_Mat[icib][ipis];
				// m_Omega *= 1.0 - M_Y_Mat[icib][ipis];
			}
			// m_Omega -= m_Vide;
			// ** Normalisation
			float K;
			K = m_Vide + 0;
			for (int icib = 0; icib < nbCible; icib++)
				K += m_Y[icib];
			if (K != 0) {
				m_Vide /= K;
				// m_Omega /= K;
				for (int icib = 0; icib < nbCible; icib++)
					m_Y[icib] /= K;
			}
			// Affectation des résultat dans les matrices dédiées :
			float[] currM_cr_sub_yx = m_cr_sub_yx[ipis];
			float[] currM_cr_yx = m_cr_yx[ipis];
			for (int icib = 0; icib < nbCible; icib++) {
				currM_cr_sub_yx[icib] = m_Y[icib];
				currM_cr_yx[icib] = m_Y[icib];
			}
			currM_cr_yx[nbCible] = m_Vide;
			// currM_cr_yx[nbCible + 1] = m_Omega;
		}

		// DECISION X->Y :
		int[] decision_x = new int[nbCible]; // matrice de décision: X(i) est associé à la piste j

		for (int icib = 0; icib < nbCible; icib++) {
			float massmax = m_cr_xy[icib][nbPiste]; // par défaut on débute sur vide;
			int decindx = nbPiste;
			for (int ipis = 0; ipis < nbPiste; ipis++) { // On parcourt tous les Y, Vide est fait, omega on ne le prend pas en compte dans cette version
				float masstmp = m_cr_xy[icib][ipis];
				if (masstmp > massmax) {
					decindx = ipis;
					massmax = masstmp;
				}
			}
			decision_x[icib] = decindx;
		}

		// DECISION Y->X :
		int[] decision_y = new int[nbPiste]; // matrice de décision: Y(i) est associé à la cible i
		for (int ipis = 0; ipis < nbPiste; ipis++) {
			float[] currM_cr_yx = m_cr_yx[ipis];
			float massmax = currM_cr_yx[nbCible]; // par défaut on débute sur vide;
			int decindx = nbCible;
			for (int icib = 0; icib < nbCible; icib++) { // On parcourt tous les X, Vide est fait, omega on ne le prend pas en compte dans cette version
				float masstmp = currM_cr_yx[icib];
				if (masstmp > massmax) {
					decindx = icib;
					massmax = masstmp;
				}
			}
			decision_y[ipis] = decindx;
		}

		// soit X -> Y et Y -> X concordent alors c'est bonheur
		// sinon il faut faire plus compliqué pour lever l'ambiguité
		boolean isconflit = false;
		for (int icib = 0; icib < nbCible; icib++)
			if (decision_x[icib] != nbPiste) // si X(icib) correspond à vide, il n'y a pas de conflit
				if (decision_y[decision_x[icib]] != icib) {
					isconflit = true;
					break;
				}
		if (!isconflit)
			for (int ipis = 0; ipis < nbPiste; ipis++)
				if (decision_y[ipis] != nbCible) // si Y(icib) correspond à vide, il n'y a pas de conflit
					if (decision_x[decision_y[ipis]] != ipis) {
						isconflit = true;
						break;
					}

		if (isconflit) { // Algorithme Hongrois :
			// Cet algorithme travaille avec des entiers, donc on convertit nos flotants en x1000
			// creation de la matrice de taille nbCible+nbPiste
			int NbHung = nbCible + nbPiste;
			int[][] combinedMass_Mat = new int[NbHung][NbHung];
			// remplissage de CombinedMass_Mat:

			for (int icib = 0; icib < nbCible; icib++)
				// remplissage de [1:nbCible]x[1:nbPiste]
				for (int ipis = 0; ipis < nbPiste; ipis++)
					combinedMass_Mat[icib][ipis] = (int) (500 * (m_cr_xy[icib][ipis] + m_cr_yx[ipis][icib]));

			for (int icib = 0; icib < nbCible; icib++)
				combinedMass_Mat[icib][icib + nbPiste] = (int) (1000 * m_cr_xy[icib][nbPiste]);

			for (int ipis = 0; ipis < nbPiste; ipis++)
				combinedMass_Mat[ipis + nbCible][ipis] = (int) (1000 * m_cr_yx[ipis][nbCible]);

			HungarianClass _HungClass = new HungarianClass(NbHung); // Initialisation
			_HungClass.evaluateAlgorithm(combinedMass_Mat); // lancement de l'algo
			decision_x = _HungClass.getxyIn(); // extraction des résultats
			decision_y = _HungClass.getyxIn(); // extraction des résultats
		}

		// %==================================================
		// %
		// % TROISIEME ETAPE : Mise à jour des pistes
		// %
		// %==================================================
		// Mise à jour des pistes
		for (int ipis = 0; ipis < pistes.size(); ipis++) {
			Piste piste = pistes.get(ipis);
			if (decision_y[ipis] < nbCible/* && piste.matchScore(cibles.get(decision_y[ipis])) != 0 */) { // la piste est associée à une cible //TODO bug!!! one ne respecte pas le critère de 0.5 de similarity avec assoc odaHong
				Cible cible = cibles.get(decision_y[ipis]);// CurrentCibles[Decision_y[ipis]];
				piste.evolutionConfidence(alphaEvolConfidence, true, nbIterConf);
				piste.setStatus(PisteStatus.ASSOCIATED);
				if (verbose) {
					float score = piste.matchScore(cible);
					piste.update(cible);
					System.out.println("Associate piste: " + piste + " with cible: " + cible + " score: " + score);
				} else
					piste.update(cible);
			} else {
				piste.evolutionConfidence(alphaEvolConfidence, false, nbIterConf);
				piste.setStatus(PisteStatus.MAINTAINED);
				if (verbose)
					System.out.println("Maintain piste: " + piste);
			}
		}
		// Nouvelle piste
		for (int icib = 0; icib < nbCible; icib++)
			if (decision_x[icib] >= nbPiste) { // la cible n'est pas associée à une piste
				T newPiste = (T) cibles.get(icib).createPisteFromCible();
				newPiste.setStatus(PisteStatus.CREATED);
				pistes.add(newPiste);
				if (verbose)
					System.out.println("Create piste: " + newPiste);
			}
		// Suppression des pistes avec une confiance à 0
		if (verbose)
			System.out.println("lipi: " + pistes.toString());
		pistes.removeIf(piste -> {
			if (piste.getConfidence() <= 0) {
				if (verbose)
					System.out.println("Remove Piste: " + piste);
				return true;
			}
			return false;
		});
		return 0;
	}
}

class CibleTest implements Cible {

	public float x;

	public CibleTest(float x) {
		this.x = x;
	}

	@Override
	public Piste createPisteFromCible() {
		return new PisteTest(x, 0.5f);
	}

	@Override
	public String toString() {
		return "x: " + x;
	}
}

class PisteTest implements Piste {
	private static int alphaDis = 1;
	private static int idCpt = 0;
	public PisteStatus status = PisteStatus.CREATED;
	private int id;
	private float x;
	public float confidence;

	public PisteTest(float x) {
		this.x = x;
		id = idCpt++;
	}

	public PisteTest(float x, float confidence) {
		this(x);
		this.confidence = confidence;
	}

	public PisteTest(int id, float x, float confidence) {
		this(x, confidence);
		idCpt--;
		this.id = id;
	}

	@Override
	public PisteTest clone() {
		try {
			return (PisteTest) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof PisteTest))
			return false;
		PisteTest p = (PisteTest) obj;
		return id == p.id && x == p.x && confidence == p.confidence;
	}

	@Override
	public void evolutionConfidence(float alpha, boolean sens, int nbItertotal) {
		if (alpha > 0.99)
			alpha = 0.99f;

		double a = -3 * pow(alpha, 2) / 2 + confidence / 2 + pow(alpha, 3) + sqrt(4 * pow(alpha, 3) - 3 * pow(alpha, 4) - 6 * pow(alpha, 2) * confidence + pow(confidence, 2) + 4 * pow(alpha, 3) * confidence) / 2;
		double t = pow(a, 0.3333) - (alpha - pow(alpha, 2)) / pow(a, 0.3333) + alpha; // Abscisse courante
		t = (t * nbItertotal + (sens ? 1 : -1)) / nbItertotal; // décalage de l'abscisse
		t = t > 1 ? 1 : t;
		t = t < 0 ? 0 : t;
		confidence = (float) ((3 * t * pow(1 - t, 2) + 3 * pow(t, 2) * (1 - t)) * alpha + pow(t, 3));
	}

	@Override
	public float getConfidence() {
		return confidence;
	}

	@Override
	public float matchScore(Cible cible) {
		return 1 / (abs(((CibleTest) cible).x - x) / alphaDis + 1);
	}

	@Override
	public void setStatus(PisteStatus status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "id: " + id + " x: " + x + " confidence: " + confidence + " status: " + status;
	}

	@Override
	public void update(Cible cible) {
		this.x = ((CibleTest) cible).x;
	}
}