/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.math;

public class AlgebraicDerivator implements FilterFunction {
	public static void main(String[] args) {
		AlgebraicDerivator ad = new AlgebraicDerivator();
		ad.setWindowsSize(10);
		System.out.println(ad);
	}

	double[] coeff;
	double[] res;

	private int windowsSize;

	public double compute(double[] signal, int index) {
		double[] s = new double[windowsSize];
		for (int i = 0; i < windowsSize; i++) {
			s[i] = coeff[i] * signal[index];
			if (index > 0)
				index--;
			else
				index = windowsSize - 1;
		}
		double output = 0.0;
		// integrale par methode des trapezes
		for (int i = 1; i < windowsSize - 1; i++)
			output += s[i];
		output = output + 0.5 * (s[0] + s[windowsSize - 1]);
		return output;
	}

	@Override
	public double evaluate(double z) {
		return 0;
	}

	public int getWindowsSize() {
		return windowsSize;
	}

	@Override
	public void resolve(double[] x, double[] y) {
		res = new double[x.length];
		for (int i = 0; i < x.length; i++)
			res[i] = compute(y, i);
	}

	public void setWindowsSize(int windowsSize) {
		if (windowsSize <= 2)
			throw new IllegalArgumentException("windowsSize <= 2");
		this.windowsSize = windowsSize;
		coeff = new double[windowsSize];
		double windowLength = windowsSize - 1;
		for (int i = 0; i < windowsSize; i++)
			coeff[i] = 6 / (windowLength * windowLength * windowLength) * (windowLength - 2 * i);
	}
}
