/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.math;

public class CorrelationDerivator {
	private int windowsSize;
	private double[] y;

	public double evaluate(int index) {
		if (index < windowsSize - 1 || index > y.length - windowsSize - 1)
			return 0;
		double value = 0;
		for (int i = index; i > index - windowsSize; i--)
			value -= y[i];
		for (int i = index + 1; i < index + windowsSize + 1; i++)
			value += y[i];
		return value;
	}

	public int getWindowsSize() {
		return windowsSize;
	}

	public void resolve(double[] x, double[] y) {
		this.y = y;
	}

	public void setWindowsSize(int windowsSize) {
		if (windowsSize <= 2)
			throw new IllegalArgumentException("windowsSize <= 2");
		this.windowsSize = windowsSize;
	}
}
