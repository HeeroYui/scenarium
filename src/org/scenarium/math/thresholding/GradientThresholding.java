/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.math.thresholding;

import java.util.Arrays;

public class GradientThresholding implements Thresholder {
	private int windowsSize = 10;

	@Override
	public Thresholder clone() {
		GradientThresholding gt = new GradientThresholding();
		gt.windowsSize = windowsSize;
		return gt;
	}

	@Override
	public int computeThreshold(int[] srcData) {
		int[] src = srcData.clone();
		Arrays.sort(src);
		int bestGrad = 0;
		int iBestGrad = -1;
		int end = src.length - windowsSize;
		for (int i = windowsSize; i < end; i++) {
			int grad = src[i + windowsSize] - src[i - windowsSize];
			if (grad > bestGrad) {
				bestGrad = grad;
				iBestGrad = i;
			}
		}
		return src[iBestGrad];
	}

	public int getWindowsSize() {
		return windowsSize;
	}

	public void setWindowsSize(int windowsSize) {
		this.windowsSize = windowsSize;
	}

	@Override
	public String toString() {
		return "Gradient: " + windowsSize + " windowsSize";
	}
}
