/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium;

import java.util.function.Function;

import org.scenarium.filemanager.scenario.dataflowdiagram.scheduler.DiagramScheduler;

public class ClonerConsumer {
	public <T extends Object> boolean accept(Class<T> _class, Function<T, T> cloneFunction) {
		return DiagramScheduler.addCloner(_class, cloneFunction);
	}
}
