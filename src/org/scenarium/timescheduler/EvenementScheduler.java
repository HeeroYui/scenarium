/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.timescheduler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.function.BiConsumer;

import org.beanmanager.editors.TransientProperty;

public class EvenementScheduler extends Scheduler {
	// private ArrayList<ScheduleTask> taskStack = new ArrayList<>(); //JDK12-13 Value Type
	private Schedulable[] taskStack = new Schedulable[0];
	private long[] timeStampStack = new long[0];
	private long[] timeOfIssueStack = new long[0];
	private long[] seekIndexStack = new long[0];
	private SchedulerThread scheduler;
	@TransientProperty
	private int index = 0;
	private volatile long restTime = 0;
	private long timeStamp;

	private TriggerMode triggerMode;
	// private Semaphore startLock = new Semaphore(1);

	public EvenementScheduler(boolean canReverse) {
		super(canReverse);
	}

	public void addTasks(ArrayList<ScheduleTask> schedulableTasks, BiConsumer<Long, Long> minMaxSupplier) {
		if (timeStampStack.length != 0)
			triggerMode = TriggerMode.TIMEOFISSUE;
		if (minMaxSupplier != null) {
			long min;
			long max;
			if (schedulableTasks.isEmpty()) {
				min = 0;
				max = 0;
			} else if (triggerMode == TriggerMode.TIMESTAMP) {
				min = schedulableTasks.get(0).timeStamp;
				max = schedulableTasks.get(0).timeStamp;
				for (int i = 0; i < schedulableTasks.size(); i++) {
					long time = schedulableTasks.get(i).timeStamp;
					if (time < min)
						min = time;
					else if (time > max)
						max = time;
				}
			} else if (triggerMode == TriggerMode.TIMEOFISSUE) {
				min = schedulableTasks.get(0).timeofIssue;
				max = schedulableTasks.get(0).timeofIssue;
				for (int i = 0; i < schedulableTasks.size(); i++) {
					long time = schedulableTasks.get(i).timeofIssue;
					if (time < min)
						min = time;
					else if (time > max)
						max = time;
				}
			} else {
				min = schedulableTasks.get(0).timeofIssue;
				max = schedulableTasks.get(schedulableTasks.size() - 1).timeofIssue;
			}
			minMaxSupplier.accept(min, max);
		}
		ArrayList<ScheduleTask> tasks = new ArrayList<>();
		if (timeStampStack.length != 0) {
			for (int i = 0; i < timeStampStack.length; i++)
				tasks.add(new ScheduleTask(timeOfIssueStack[i], timeStampStack[i], taskStack[i], seekIndexStack[i]));
			triggerMode = TriggerMode.TIMEOFISSUE; // Si on mélange deux log il faut bien les réordonner
		}
		tasks.addAll(schedulableTasks);
		setTasks(tasks);
	}

	@Override
	public void clean() {
		stopAndWait();
		// setRunning(false, false);
		timeStampStack = new long[0];
		timeOfIssueStack = new long[0];
		taskStack = new Schedulable[0];
		seekIndexStack = new long[0];
		updateBeginAndEndTime(); // Attention, j'efface timeA et timeB si je fais ca...
	}

	@Override
	public long getTimeStamp() {
		return timeStamp;
	}

	@Override
	public long getIndex() {
		return index;
	}

	private int getIndexFromTimePointer(long timePointer) {
		long[] timeStack = triggerMode == TriggerMode.TIMESTAMP ? timeStampStack : timeOfIssueStack;
		if (timePointer == -1)
			return -1;
		int ts = timeStack.length;
		if (timePointer == beginTime)
			return 0;
		else if (timePointer == endTime)
			return ts - 1;
		double ratio = ts / (double) (endTime - beginTime);
		int index = (int) ((timePointer - beginTime) * ratio);
		long a = index == 0 ? 0 : timeStack[index - 1];
		long b = timeStack[index];
		int nbIt = 0;
		while (timePointer < a || timePointer > b) {
			double delta = timePointer - (a + b) * 0.5;
			if (nbIt < 5) {
				index += (int) (delta * ratio);
				nbIt++;
			} else
				index += delta < 0 ? -1 : 1;
			if (index < 1)
				index = 1;
			if (index >= ts)
				index = ts - 1;
			a = timeStack[index - 1];
			b = timeStack[index];
		}
		while (timeStack[index - 1] == timeStack[index])
			index--;
		return index;
	}

	@Override
	public long getNbGap() {
		return timeStampStack.length;
	}

	@Override
	public boolean isRunning() {
		return scheduler != null && !scheduler.refreshAndDie;
	}

	@Override
	public void nextStep() {
		if (index >= timeStampStack.length - 1)
			return;
		index++;
		long[] timeStack = triggerMode == TriggerMode.TIMESTAMP ? timeStampStack : timeOfIssueStack;
		while (index != timeStack.length - 1 && timeStack[index - 1] == timeStack[index])
			index++;
		super.setTimePointer(timeStack[this.index]);
	}

	@Override
	public void previousStep() {
		if (index == 0)
			return;
		long[] timeStack = triggerMode == TriggerMode.TIMESTAMP ? timeStampStack : timeOfIssueStack;
		while (index != 0 && timeStack[index - 1] == timeStack[index])
			index--;
		if (index != 0)
			index--;
		while (index != 0 && timeStack[index - 1] == timeStack[index])
			index--;
		super.setTimePointer(timeStack[index]);
	}

	@Override
	public void setIndex(long index) {
		this.index = (int) index;
		long[] timeStack = triggerMode == TriggerMode.TIMESTAMP ? timeStampStack : timeOfIssueStack;
		try {
			super.setTimePointer(timeStack[this.index]);
		} catch (IndexOutOfBoundsException e) {

		}
	}

	public void setTasks(ArrayList<ScheduleTask> schedulableTasks) {
		if (schedulableTasks == null) {
			taskStack = new Schedulable[0];
			timeStampStack = new long[0];
			timeOfIssueStack = new long[0];
			seekIndexStack = new long[0];
		} else {
			if (triggerMode != TriggerMode.NATURALORDER)
				Collections.sort(schedulableTasks, triggerMode == TriggerMode.TIMESTAMP ? (a, b) -> Long.compare(a.timeStamp, b.timeStamp) : (a, b) -> Long.compare(a.timeofIssue, b.timeofIssue)); // Il fallait bien trier...
			taskStack = new Schedulable[schedulableTasks.size()];
			timeStampStack = new long[taskStack.length];
			timeOfIssueStack = new long[taskStack.length];
			seekIndexStack = new long[taskStack.length];
			for (int i = 0; i < schedulableTasks.size(); i++) {
				ScheduleTask task = schedulableTasks.get(i);
				taskStack[i] = task.task;
				timeStampStack[i] = task.timeStamp;
				timeOfIssueStack[i] = task.timeofIssue;
				seekIndexStack[i] = task.seekIndex;
			}
		}
		updateBeginAndEndTime();
		setIndex(0);
	}

	@Override
	public boolean setTimePointer(long timePointer) {
		if (timePointer <= getEffectiveStart())
			timePointer = getEffectiveStart();
		else if (timePointer >= getEffectiveStop())
			timePointer = getEffectiveStop();
		index = getIndexFromTimePointer(timePointer);
		return super.setTimePointer(timePointer);
	}

	public void setTriggerMode(TriggerMode triggerMode) {
		this.triggerMode = triggerMode;
	}

	@Override
	public void start(final boolean refresh) {
		start(refresh, false, false);
	}

	private void start(boolean refreshAtStartup, boolean ignoreRestTime, boolean refreshAndDie) {
		SchedulerThread oldScheduler = scheduler;
		if (oldScheduler != null)
			oldScheduler.interrupt();
		scheduler = new SchedulerThread(oldScheduler, refreshAtStartup, ignoreRestTime, refreshAndDie);
		if (speed != 0)
			scheduler.start();
		else
			restTime = 0;
	}

	@Override
	protected void suspend(Runnable taskWhenSuspented) {
		// System.err.println("suspend: " + SchedulerThread.currentThread().getId());
		SchedulerThread _scheduler = scheduler;
		if (_scheduler != null) {
			// System.err.println("suspend _scheduler " + SchedulerThread.currentThread().getId());
			_scheduler.interrupt();
			if (taskWhenSuspented == null)
				scheduler = null;
			else
				new Thread(() -> {
					try {
						_scheduler.join();
						scheduler = null;
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					taskWhenSuspented.run();
				}).start();
		} else if (taskWhenSuspented != null)
			taskWhenSuspented.run();
	}

	@Override
	protected void suspendAndWait() {
		if (scheduler != null) {
			scheduler.interrupt();
			try {
				scheduler.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			scheduler = null;
		}
	}

	@Override
	protected void timePointerChanged() {
		if (isRunning())
			start(true, true, false); // redémarre le scheduler
		else if (!stop && !prestop)
			start(true, true, true); // Créer beaucoup de thread....
		for (VisuableSchedulable visuableSchedulable : visualScheduleElements)
			visuableSchedulable.paint();

		// else
		// ;//updateSchedulableElement(); //Ici ok on run pas... le thread javafx ici...
		// long[] timeStack = triggerMode == TriggerMode.TimeStamp ? timeStampStack : timeOfIssueStack;
		// if (!stop && !isRunning()) {
		// int nextIndex = speed > 0 ? index + 1 : index - 1;
		// while (nextIndex >= 0 && nextIndex <= timeStack.length - 1 && timeStack[index] == timeStack[nextIndex]) {
		// index = nextIndex;
		// //updateSchedulableElement(); //Il faut pas le faire ici, seul ce scheduler devrait pouvoir faire ca, sinon apelle de updateSchedulableElement asynchrone... le thread javafx ici...
		// nextIndex = speed > 0 ? index + 1 : index - 1;
		// }
		// }
	}

	@Override
	public void updateBeginAndEndTime() {
		startTime = Long.MIN_VALUE;
		stopTime = Long.MAX_VALUE;
		long[] timeStack = triggerMode == TriggerMode.TIMESTAMP ? timeStampStack : timeOfIssueStack;
		if (timeStack.length == 0) {
			beginTime = 0; // -1 avant mais pose problème car sinon on fait un setTimePointer à -1 à la fin et le scheduler se lance pas
			endTime = 0;
		} else {
			beginTime = timeStack.length == 0 ? 0 : timeStack[0];
			endTime = timeStack[timeStack.length - 1];
		}
		if (getTimeA() != null && (getTimeA() < beginTime || getTimeA() > endTime))
			setTimeA(null);
		if (getTimeB() != null && (getTimeB() < beginTime || getTimeB() > endTime))
			setTimeB(null);
		if (timePointer < getBeginningTime())
			setTimePointer(getBeginningTime());
		else if (timePointer > getEndTime())
			setTimePointer(getEndTime());
		firePropertyChangeEvent(SchedulerState.INTERVALTIMECHANGED);
	}

	@Override
	protected void updateSpeed() {
		if (scheduler != null && !scheduler.refreshAndDie)
			start(false);
	}
	
	class SchedulerThread extends Thread {
		private SchedulerThread oldScheduler;
		private boolean refreshAtStartup;
		private boolean ignoreRestTime;
		private boolean refreshAndDie;

		public SchedulerThread(SchedulerThread oldScheduler, boolean refreshAtStartup, boolean ignoreRestTime, boolean refreshAndDie) {
			this.oldScheduler = oldScheduler;
			this.refreshAtStartup = refreshAtStartup;
			this.ignoreRestTime = ignoreRestTime;
			this.refreshAndDie = refreshAndDie;
		}

		@Override
		public void run() {
			if (oldScheduler != null)
				try {
					oldScheduler.join();
				} catch (InterruptedException e) {
					return;
				}
			oldScheduler = null;
			try {
				waitStarted();
			} catch (InterruptedException e) {
				return;
			}
			long[] timeStack = triggerMode == TriggerMode.TIMESTAMP ? timeStampStack : timeOfIssueStack;
			long beginTime = System.currentTimeMillis();
			if (index == -1 || timeStack.length == 0)
				return;
			if ((refreshAtStartup || refreshAndDie) && timeStack[index] == timePointer) { // Ajout 18/09 il ne faut pas rafraichir si la donné n'est pas au temps voulu
				updateSchedulableElement();
				int nextIndex = speed > 0 ? index + 1 : index - 1;
				while (nextIndex >= 0 && nextIndex <= timeStack.length - 1 && timeStack[index] == timeStack[nextIndex]) {
					index = nextIndex;
					updateSchedulableElement(); // Close de temps en temps...
					nextIndex = speed > 0 ? index + 1 : index - 1;
				}
			}
			if (refreshAndDie)
				return;
			long firstTaskTime = timeStack[index];
			int speed = EvenementScheduler.this.speed;
			long timeToReach = System.currentTimeMillis();
			if (isInterrupted())
				return;
			try {
				if (restTime != 0 && !ignoreRestTime) {
					long timeTowait = (long) (restTime * 100.0 / Math.abs(speed));
					timeToReach = System.currentTimeMillis() + timeTowait;
					if (timeTowait > 0)
						synchronized (this) {
							wait(timeTowait);
						}
					restTime = 0;
				}
				while (!isInterrupted()) {
					int newIndex = speed > 0 ? index + 1 : index - 1;
					if (speed > 0 && (newIndex >= timeStack.length || timeStack[newIndex] > getEffectiveStop()) || speed < 0 && (newIndex < 0 || timeStack[newIndex] < getEffectiveStart()))
						if (repeat) {
							timePointer = speed > 0 ? getEffectiveStart() : getEffectiveStop();
							newIndex = getIndexFromTimePointer(timePointer);
							beginTime = System.currentTimeMillis();
							firstTaskTime = timeStack[newIndex];
						} else { 
							endReached();
							return;
						}
					long timetask = timeStack[newIndex];
					if (taskStack[newIndex].canTrigger(timetask)) {
						if (timetask != timePointer) {
							long timeGap = (long) ((timetask - firstTaskTime) * 100.0 / Math.abs(speed));
							timeToReach = beginTime + (speed > 0 ? timeGap : -timeGap);
							long timeTowait = timeToReach - System.currentTimeMillis();
							if (timeTowait > 0)
								synchronized (this) {
									wait(timeTowait);
								}
							timePointer = timetask;
						}
						index = newIndex;
						updateSchedulableElement();
					} else
						index = newIndex;
				}
			} catch (InterruptedException e1) {}
			long time = System.currentTimeMillis();
			restTime = !isRunning() ? 0 : speed * EvenementScheduler.this.speed > 0 ? (long) ((timeToReach - time) * Math.abs(speed) / 100.0) : 0;
		}

		private void updateSchedulableElement() {
			if (index == -1 || timeStampStack.length == 0)
				return;
			if (Thread.currentThread().isInterrupted())
				return;
			// System.out.println("index: " + index + " at: " + timePointer);
			timeStamp = timeStampStack[index];
			taskStack[index].update(seekIndexStack[index]);
			if (Thread.currentThread().isInterrupted())
				return;
			if (!isRunning())
				for (VisuableSchedulable visuableSchedulable : visualScheduleElements)
					visuableSchedulable.paint();
		}
	}
}
