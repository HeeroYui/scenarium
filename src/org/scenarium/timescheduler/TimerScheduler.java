/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.timescheduler;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import org.scenarium.filemanager.scenariomanager.LocalScenario;
import org.scenarium.filemanager.scenariomanager.PeriodListener;
import org.scenarium.filemanager.scenariomanager.Scenario;
import org.scenarium.filemanager.scenariomanager.TimedScenario;

public class TimerScheduler extends Scheduler implements PeriodListener {
	protected ArrayList<Schedulable> scheduleElements = new ArrayList<>();
	private Timer simulationTimer;
	private int period;
	private LocalScenario mainSchedulable;
	private Thread timerThread;

	public TimerScheduler(boolean canReverse) {
		super(canReverse);
	}

	public void addScheduleElement(Schedulable scheduleElement, int index) {
		boolean isRunning = isRunning();
		if (isRunning)
			suspendAndWait();
		ArrayList<Schedulable> newScheduleElements = new ArrayList<>(scheduleElements);
		if (!newScheduleElements.contains(scheduleElement)) {
			newScheduleElements.add(index == -1 ? newScheduleElements.size() : index, scheduleElement);
			if (scheduleElement instanceof Scenario)
				updateBeginAndEndTime(newScheduleElements);
		}
		scheduleElements = newScheduleElements;
		if (isRunning)
			start(true);
	}

	@Override
	public void clean() {
		if (mainSchedulable != null)
			mainSchedulable.removePeriodListener(this);
	}

	@Override
	public long getTimeStamp() {
		return timePointer * period;
	}

	@Override
	public long getIndex() {
		return timePointer;
	}

	@Override
	public long getNbGap() {
		return endTime - beginTime;
	}

	@Override
	public boolean isRunning() {
		return simulationTimer != null;
	}

	@Override
	public void nextStep() {
		setTimePointer(timePointer + 1);
	}

	@Override
	public void periodChanged(int period) {
		this.period = period;
		if (simulationTimer != null)
			start(false);
	}

	@Override
	public void previousStep() {
		setTimePointer(timePointer - 1);
	}

	public void removeScheduleElement(Schedulable scheduleElement) {
		boolean isRunning = isRunning();
		if (isRunning)
			suspendAndWait();
		ArrayList<Schedulable> newScheduleElements = new ArrayList<>(scheduleElements);
		if (newScheduleElements.contains(scheduleElement)) {
			newScheduleElements.remove(scheduleElement);
			if (scheduleElement instanceof Scenario)
				updateBeginAndEndTime(newScheduleElements);
		}
		scheduleElements = newScheduleElements;
		if (isRunning)
			start(true);
	}

	public void resetScheduleElement() {
		scheduleElements = new ArrayList<>();
	}

	public void run() {
		if (timePointer >= getEffectiveStop() && speed > 0 || timePointer <= getEffectiveStart() && speed < 0) {
			if (repeat)
				timePointer = speed > 0 ? getEffectiveStart() : getEffectiveStop();
			else {
				pause();		//Pour ne rien faire apèrs
				endReached();
				return;
			}
		} else
			timePointer = speed > 0 ? timePointer + 1 : timePointer - 1;
		updateSchedulableElement();
	}

	@Override
	public void setIndex(long index) {
		setTimePointer(index);
	}

	@Override
	public void start(final boolean refresh) {
		Thread oldTimerThread = timerThread;
		stopTimer(null);
		simulationTimer = new Timer();
		timerThread = new Thread() {
			@Override
			public void run() {
				if (oldTimerThread != null)
					try {
						oldTimerThread.join();
					} catch (InterruptedException e) {
						return;
					}
				try {
					waitStarted();
				} catch (InterruptedException e1) {
					return;
				}
				if (refresh)
					updateSchedulableElement();
				int ms = (int) (100.0 / Math.abs(speed) * period);
				if (ms == 0)
					ms = 1;
				if (simulationTimer == null)
					return;
				try {
					simulationTimer.scheduleAtFixedRate(new TimerTask() {
						@Override
						public void run() {
							TimerScheduler.this.run();
						}
					}, ms / 2, ms);
				} catch (IllegalStateException e) {} // Le timer est peut �tre d�ja cancel
			};
		};
		timerThread.start();
	}

	private void stopTimer(Runnable taskWhenSuspented) {
		if (simulationTimer != null) {
			simulationTimer.cancel();
			simulationTimer.purge();
			simulationTimer = null;
			if (taskWhenSuspented == null)
				timerThread = null;
			else
				new Thread(() -> {
					try {
						timerThread.join();
						timerThread = null;
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					taskWhenSuspented.run();
				}).start();
		} else if (taskWhenSuspented != null)
			taskWhenSuspented.run();
	}

	@Override
	public void suspend(Runnable run) {
		stopTimer(run);
	}

	@Override
	protected void suspendAndWait() {
		Thread _timerThread = timerThread;
		stopTimer(null);
		try {
			_timerThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void timePointerChanged() {
		updateSchedulableElement();
	}

	@Override
	public void updateBeginAndEndTime() {
		updateBeginAndEndTime(scheduleElements);
	}

	private void updateBeginAndEndTime(ArrayList<Schedulable> scheduleElements) {
		beginTime = 0;
		endTime = 0;
		if (mainSchedulable != null)
			mainSchedulable.removePeriodListener(this);
		mainSchedulable = null;
		for (Schedulable schedulable : scheduleElements)
			if (schedulable instanceof Scenario) {
				long beginTimeScenario = ((Scenario) schedulable).getBeginningTime();
				long endTimeScenario = ((Scenario) schedulable).getEndTime();
				if (this.beginTime == -1 || beginTimeScenario < this.beginTime)
					this.beginTime = beginTimeScenario;
				if (endTimeScenario != -1) {
					endTimeScenario--;
					if (endTimeScenario > this.endTime)
						this.endTime = endTimeScenario;
				}
				if (schedulable instanceof TimedScenario)
					mainSchedulable = (TimedScenario) schedulable;
			}
		if (mainSchedulable != null) {
			period = mainSchedulable.getPeriod();
			mainSchedulable.addPeriodListener(this);
		}
		firePropertyChangeEvent(SchedulerState.INTERVALTIMECHANGED);
	}

	private void updateSchedulableElement() {
		try {
			for (Schedulable so : scheduleElements) // TODO updateSchedulableElement...
				so.update(timePointer);
			if (!isRunning())
				for (VisuableSchedulable visuableSchedulable : visualScheduleElements)
					visuableSchedulable.paint();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void updateSpeed() {
		if (simulationTimer != null)
			start(false);
	}
}
