package org.scenarium.timescheduler;

import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.function.BiConsumer;

import org.scenarium.filemanager.scenario.dataflowdiagram.Block;
import org.scenarium.filemanager.scenario.dataflowdiagram.operator.rmi.ScenarioRemoteOperator;
import org.scenarium.filemanager.scenariomanager.LocalScenario;

public class SchedulerInterface {
	private Scheduler scheduler;
	private Block remoteBlock;

	public SchedulerInterface(Scheduler scheduler) {
		this.scheduler = scheduler;
	}

	public SchedulerInterface(Scheduler scheduler, Block remoteBlock) {
		this(scheduler);
		this.remoteBlock = remoteBlock;
	}

	public void addPropertyChangeListener(SchedulerPropertyChangeListener schedulerPropertyChangeListener) {
		scheduler.addPropertyChangeListener(schedulerPropertyChangeListener);
	}

	public void addTasks(ArrayList<ScheduleTask> schedulableTasks, BiConsumer<Long, Long> minMaxSupplier) {
		if (remoteBlock != null)
			for (int i = 0; i < schedulableTasks.size(); i++) {
				ScheduleTask st = schedulableTasks.get(i);
				schedulableTasks.set(i, new ScheduleTask(st.timeofIssue, st.timeStamp, new Schedulable() {

					@Override
					public void update(long timePointer) {
						ScenarioRemoteOperator _remoteOp = (ScenarioRemoteOperator) remoteBlock.getRemoteOperator();
						if (_remoteOp != null)
							_remoteOp.update(((LocalScenario) remoteBlock.getOperator()).getTaskId(st.task), timePointer, scheduler.getTimeStamp());
					}

					@Override
					public boolean canTrigger(long time) {
						return ((LocalScenario) remoteBlock.getOperator()).canTrigger(time);
					}
				}, st.seekIndex));
			}
		if (scheduler instanceof EvenementScheduler)
			((EvenementScheduler) scheduler).addTasks(schedulableTasks, minMaxSupplier);
	}

	public void clean() {
		scheduler.clean();
	}

	public SchedulerInterface derivateAsRemote(Block remoteBlock) {
		return new SchedulerInterface(scheduler, remoteBlock);
	}

	public long getBeginTime() {
		return scheduler.getBeginningTime();
	}

	public long getTimeStamp() {
		return scheduler.getTimeStamp();
	}

	public long getEndTime() {
		return scheduler.getEndTime();
	}

	public void removePropertyChangeListener(SchedulerPropertyChangeListener schedulerPropertyChangeListener) {
		scheduler.addPropertyChangeListener(schedulerPropertyChangeListener);
	}

	public void removeScheduleElement(Schedulable schedulable) {
		if (scheduler instanceof TimerScheduler)
			((TimerScheduler) scheduler).removeScheduleElement(schedulable);
	}

	public void setStartLock(CountDownLatch startLock) {
		scheduler.setStartLock(startLock);
	}

	public void setTriggerMode(TriggerMode triggerMode) {
		if (scheduler instanceof EvenementScheduler)
			((EvenementScheduler) scheduler).setTriggerMode(triggerMode);
	}

	public boolean stop() {
		return scheduler.stop();
	}

	public boolean stopAndWait() {
		return scheduler.stopAndWait();
	}

	public void setStartTime(long startTime) {
		scheduler.setStartTime(startTime);
	}

	public void setStopTime(long stopTime) {
		scheduler.setStopTime(stopTime);
	}

	public <T> T synchronisedCall(Callable<T> callable) throws Exception {
		synchronized (scheduler) {
			return callable.call();
		}
	}
	
	public boolean isRunning() {
		return scheduler.isRunning();
	}
}