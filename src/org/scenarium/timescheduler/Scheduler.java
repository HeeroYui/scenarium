/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.timescheduler;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import javax.swing.event.EventListenerList;

import org.beanmanager.BeanPropertiesInheritanceLimit;
import org.beanmanager.editors.TransientProperty;

@BeanPropertiesInheritanceLimit
public abstract class Scheduler {
	public static final int TIMERSCHEDULER = 0;
	public static final int EVENTSCHEDULER = 1;
	public static final int STREAMSCHEDULER = 2;
	protected List<VisuableSchedulable> visualScheduleElements = new ArrayList<>(); // Remove Collections.synchronizedList because of lock
	private EventListenerList listeners = new EventListenerList();
	protected long timePointer = 0;
	protected long beginTime;
	protected long endTime;
	@TransientProperty
	protected long startTime = Long.MIN_VALUE; // Ajout
	@TransientProperty
	protected long stopTime = Long.MAX_VALUE; // Ajout
	private Long timeA = null;
	private Long timeB = null;
	protected int speed = 100;
	private int maxSpeed = 100000;
	protected boolean repeat = false;
	protected boolean prestop = false;
	protected boolean stop = true;
	private boolean canReverse;
	private CountDownLatch startLock;
	protected Runnable stopTimeOutTask;

	public static Scheduler createScheduler(int schedulerType, boolean canReverse) {
		if (schedulerType == TIMERSCHEDULER)
			return new TimerScheduler(canReverse);
		else if (schedulerType == EVENTSCHEDULER)
			return new EvenementScheduler(canReverse);
		else if (schedulerType == STREAMSCHEDULER)
			return new StreamScheduler(canReverse);
		return null;
	}

	public Scheduler(boolean canReverse) {
		this.canReverse = canReverse;
	}

	public void addPropertyChangeListener(SchedulerPropertyChangeListener listener) {
		listeners.add(SchedulerPropertyChangeListener.class, listener);
	}

	public void addVisualScheduleElement(VisuableSchedulable visuableSchedulable) {
		visualScheduleElements.add(visuableSchedulable);
	}

	public abstract void clean();

	public void copyProperty(Scheduler scheduler) {
		// scheduleElements = scheduler.scheduleElements;
		visualScheduleElements = scheduler.visualScheduleElements; // Pour garder le listener du player
		listeners = scheduler.listeners;
		timeA = scheduler.timeA;
		timeB = scheduler.timeB;
		speed = scheduler.speed;
		repeat = scheduler.repeat;
		maxSpeed = scheduler.maxSpeed;
		if (timeA != null && timePointer < timeA)
			timePointer = timeA;
		if (timeB != null && timePointer > timeB)
			timePointer = timeB;
	}

	public void death() {
		suspend(null);
	}

	protected void endReached() {
		firePropertyChangeEvent(SchedulerState.ENDREACHED);
	}

	protected void firePropertyChangeEvent(SchedulerState state) {
		for (SchedulerPropertyChangeListener listener : listeners.getListeners(SchedulerPropertyChangeListener.class))
			listener.stateChanged(state);
	}

	public long getBeginningTime() {
		return beginTime;
	}

	public abstract long getTimeStamp();

	public long getEndTime() {
		return endTime;
	}

	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(long time) {
		if (time == startTime || time < beginTime || time > endTime - 1)
			return;
		if (timePointer < time)
			setTimePointer(time);
		this.startTime = time;
	}

	public long getStopTime() {
		return stopTime;
	}

	public void setStopTime(long time) {
		if (time == stopTime || time > endTime || time < beginTime)
			return;
		if (timePointer > time)
			setTimePointer(time);
		this.stopTime = time;
	}

	public long getEffectiveStop() {
		return Math.min(timeB != null ? timeB : endTime, stopTime);
	}

	public abstract long getIndex();

	public int getMaxSpeed() {
		return maxSpeed;
	}

	public abstract long getNbGap();

	public int getSpeed() {
		return speed;
	}

	public long getEffectiveStart() {
		return Math.max(timeA != null ? timeA : beginTime, startTime);
	}

	public Long getTimeA() {
		return timeA;
	}

	public Long getTimeB() {
		return timeB;
	}

	public long getTimePointer() {
		return timePointer;
	}

	public boolean isOfKind(int schedulerType) {
		if (schedulerType == TIMERSCHEDULER && this instanceof TimerScheduler)
			return true;
		else if (schedulerType == EVENTSCHEDULER && this instanceof EvenementScheduler)
			return true;
		return schedulerType == STREAMSCHEDULER && this instanceof StreamScheduler;
	}

	public boolean isRepeat() {
		return repeat;
	}

	public abstract boolean isRunning();

	public boolean isStopped() {
		return stop;
	}

	public void jump(boolean forward) {
		long startTime = getEffectiveStart();
		long newTimePointer;
		if (forward) {
			newTimePointer = timePointer + (endTime - beginTime) / 50;
			if (newTimePointer > endTime)
				newTimePointer = endTime;
		} else {
			newTimePointer = timePointer - (endTime - beginTime) / 50;
			if (newTimePointer < startTime)
				newTimePointer = startTime;
		}
		setTimePointer(newTimePointer);
	}

	public void moveSpeed(boolean inc) {
		int oldSimulationSpeed = speed;
		int newSpeed;
		if (inc) {
			newSpeed = speed + Math.abs(speed) / 10;
			if (oldSimulationSpeed == speed)
				newSpeed++;
		} else {
			newSpeed = speed - Math.abs(speed) / 10;
			if (oldSimulationSpeed == speed)
				newSpeed--;
		}
		setSpeed(newSpeed);
	}

	public abstract void nextStep();

	synchronized public void pause() {
		pause(false);
	}

	private void pause(boolean wait) {
		if (isRunning())
			if (wait)
				suspendAndWait();
			else
				suspend(() -> {
					stop = false;
					firePropertyChangeEvent(SchedulerState.SUSPENDED);
				});

	}

	synchronized public void pauseAndWait() {
		pause(true);
	}

	public abstract void previousStep();

	public void refresh() {
		timePointerChanged();
	}

	public void removePropertyChangeListener(SchedulerPropertyChangeListener listener) {
		listeners.remove(SchedulerPropertyChangeListener.class, listener);
	}

	public void removeVisualScheduleElement(VisuableSchedulable visuableSchedulable) {
		if (visualScheduleElements.contains(visuableSchedulable))
			visualScheduleElements.remove(visuableSchedulable);
	}

	public void reset() {
		setTimeA(null);
		setTimeB(null);
		setTimePointer(beginTime);
	}

	protected abstract void setIndex(long index);

	public void setRepeat(boolean repeat) {
		this.repeat = repeat;
		firePropertyChangeEvent(SchedulerState.REPEAT);
	}

	public void setSpeed(int speed) {
		int oldSimulationSpeed = this.speed;
		int minSpeed = canReverse ? -maxSpeed : 0;
		if (speed > maxSpeed)
			this.speed = maxSpeed;
		else if (speed < minSpeed)
			this.speed = minSpeed;
		else
			this.speed = speed;
		if (oldSimulationSpeed != this.speed) {
			updateSpeed();
			firePropertyChangeEvent(SchedulerState.SPEEDCHANGED);
		}
	}

	public void setStartLock(CountDownLatch startLock) {
		this.startLock = startLock;
	}

	public void setTimeA(Long time) {
		if (time == timeA)
			return;
		if (time != null) {
			if (time < beginTime || timeB != null && time >= timeB)
				return;
			else if (time > endTime - 1)
				return;
			if (time == beginTime) {
				time = null;
				if (timeB != null && timeB == beginTime + 1)
					timeB = null;
			} else if (timePointer < time)
				setTimePointer(time);
		}
		timeA = time;
		firePropertyChangeEvent(SchedulerState.BOUCLE);
	}

	public void setTimeB(Long time) {
		if (time == timeB)
			return;
		if (time != null) {
			if (time > endTime || timeA != null && time <= timeA)
				return;
			else if (time < beginTime)
				return;
			if (time == endTime) {
				time = null;
				if (timeA != null && timeA == endTime - 1)
					timeA = null;
			} else if (timePointer > time)
				setTimePointer(time);
		}
		timeB = time;
		firePropertyChangeEvent(SchedulerState.BOUCLE);
	}

	public boolean setTimePointer(long timePointer) {
		if (this.timePointer == timePointer) // en plus
			return false;
		long startTime = getEffectiveStart();
		long finishTime = getEffectiveStop();
		if (timePointer < startTime)
			timePointer = startTime;
		else if (timePointer > finishTime)
			timePointer = finishTime;
		this.timePointer = timePointer;
		timePointerChanged();
		return true;
	}

	public void setToIndex(long index) {
		if (index >= 0 && index < getNbGap())
			setIndex(index);
	}

	// private static volatile int cpts = 0;
	synchronized public boolean start() {
		// int c = cpts++;
		// System.err.println("start diagram: " + c);
		if (!isRunning()) {
			if (stop) {
				// System.err.println("PRESTART diagram: " + c);
				firePropertyChangeEvent(SchedulerState.PRESTART); // lance le load, le pb c'est que le mono il peut prendre du temps...
				// System.err.println("STARTING diagram: " + c);
				firePropertyChangeEvent(SchedulerState.STARTING); // lance le load, le pb c'est que le mono il peut prendre du temps...
				start(true); // lance le scheduler, peut être avant la fin du mono...
				stop = false;
				// System.err.println("STARTED diagram: " + c);
				firePropertyChangeEvent(SchedulerState.STARTED);
			} else {
				start(false); // lance le scheduler, peut être avant la fin du mono...
				// System.err.println("UNSUSPENDED diagram: " + c);
				firePropertyChangeEvent(SchedulerState.UNSUSPENDED);
			}
			return true;
		}
		return false;
	}

	protected abstract void start(boolean refresh);

	synchronized public boolean stop() {
		return stop(false);
	}

	synchronized public boolean stopAndWait() {
		return stop(true);
	}

	// private static volatile int cpt = 0;
	private boolean stop(boolean wait) {
		// System.err.println("scheduler stopped ask " + ProcessHandle.current().pid() + " wait: " + wait);
		// if (wait == false)
		// System.err.println();
		// int c = cpt++;
		// System.err.println("stop diagram: " + c);
		if (!stop) {
			prestop = true;
			// System.err.println("PRESTOP diagram: " + c);
			firePropertyChangeEvent(SchedulerState.PRESTOP);
			// System.err.println("event PRESTOP done: " + c);
			Runnable stopTask = () -> {
				if (timePointer != getEffectiveStart()) // On a stopper, on doit refresh apres
					setTimePointer(getEffectiveStart());
				// System.err.println("STOPPING diagram: " + c);
				firePropertyChangeEvent(SchedulerState.STOPPING); /// Tue les opérateurs...
				// System.err.println("scheduler stopped");
				// System.err.println("STOPPED diagram: " + c);
				firePropertyChangeEvent(SchedulerState.STOPPED);
			};
			if (wait) {
				suspendAndWait(); // Bloqué ici dans certain cas
				stopTask.run();
			} else
				suspend(stopTask); // Tue le scheduler dans une tache
			stop = true;
			prestop = false;
			return true;
		}
		return false;
	}

	protected abstract void suspend(Runnable taskWhenSuspented);

	protected abstract void suspendAndWait();

	// public void addStartStopListener(StartStopListener listener) {
	// listeners.add(StartStopListener.class, listener);
	// }

	// public void removeStartStopListener(StartStopListener listener) {
	// listeners.remove(StartStopListener.class, listener);
	// }
	//
	// protected void fireStartStop(boolean start) {
	// for (StartStopListener listener : listeners.getListeners(StartStopListener.class))
	// if (start)
	// listener.start();
	// else
	// listener.stop();
	// }

	protected abstract void timePointerChanged();

	@Override
	public String toString() {
		return "Time: " + getTimeStamp();
	}

	public abstract void updateBeginAndEndTime();

	protected abstract void updateSpeed();

	protected void waitStarted() throws InterruptedException {
		if (startLock != null) {
			startLock.await();
			startLock = null;
		}
	}
}
