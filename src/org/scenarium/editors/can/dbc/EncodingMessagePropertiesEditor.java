/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.editors.can.dbc;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.StringTokenizer;

import org.beanmanager.editors.DynamicSizeEditor;
import org.beanmanager.editors.PropertyEditor;
import org.beanmanager.editors.basic.EnumEditor;
import org.beanmanager.editors.basic.StringEditor;
import org.beanmanager.editors.primitive.BooleanEditor;
import org.beanmanager.editors.primitive.number.ControlType;
import org.beanmanager.editors.primitive.number.DoubleEditor;
import org.beanmanager.editors.primitive.number.IncrementMode;
import org.beanmanager.editors.primitive.number.IntegerEditor;
import org.beanmanager.tools.FxUtils;
import org.scenarium.communication.can.dbc.CanDBC;
import org.scenarium.communication.can.dbc.EncodingMessageProperties;
import org.scenarium.communication.can.dbc.EncodingSignalProperties;
import org.scenarium.communication.can.dbc.MessageIdentifier;
import org.scenarium.communication.can.dbc.SignalIdentifier;
import org.scenarium.editors.can.SendMode;
import org.scenarium.test.fx.FxTest;

import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class EncodingMessagePropertiesEditor extends PropertyEditor<EncodingMessageProperties[]> implements DynamicSizeEditor {

	private static final String NEWTRIGGER = "New trigger...";

	public static void main(String[] args) {
		CanDBC canDBC = new CanDBC();
		// canDBC.setCanDBCFile(new File("/home/revilloud/Téléchargements/Fwd TR Mobileye Extended Protocol documentation + dbc files(1)/ExtLogData2.dbc"));
		canDBC.setCanDBCFile(new File("/home/revilloud/workspace/Fabric/GUI_CAN-C-E-Q.dbc"));
		ArrayList<MessageIdentifier> mis = canDBC.getMessagesIdentifier();
		EncodingMessagePropertiesEditor ee = new EncodingMessagePropertiesEditor();
		ee.setMessageIdentifier(mis.toArray(new MessageIdentifier[mis.size()]));
		ee.setAsText(
				"384 INFRA_CONTROL ONNEWDATA _ {INFRA_PowerLimitVeh1,0.0,false,INFRA_EnableSystem,0.0,false,INFRA_PowerLimitVeh2,0.0,false} 385 INFRA_STATUS ONNEWDATA _ {INFRA_SystemEnabled,10.0,false} 386 INFRA_AUTHORIZATION ONNEWDATA _ {INFRA_AuthVeh1,0.0,false,INFRA_AuthVeh2,0.0,false} 387 SECC_BASE_STATUS1 ONNEWDATA _ {SECC_BanBA,0.0,false,SECC_BanBB,0.0,false,SECC_BanBC,0.0,false,SECC_BanBD,0.0,false,SECC_BanA9,0.0,false,SECC_BanA8,0.0,false,SECC_BanA5,0.0,false,SECC_BanA4,0.0,false,SECC_BanA7,0.0,false,SECC_BanA6,0.0,false,SECC_BanA1,0.0,false,SECC_BanA0,0.0,false,SECC_BanA3,0.0,false,SECC_BanA2,0.0,false,SECC_BanAD,0.0,false,SECC_BanAA,0.0,false,SECC_BanAC,0.0,false,SECC_BanAB,0.0,false,SECC_BanB0,0.0,false,SECC_BanB1,0.0,false,SECC_BanB2,0.0,false,SECC_BanB3,0.0,false,SECC_BanB4,0.0,false,SECC_BanB5,0.0,false,SECC_BanB6,0.0,false,SECC_BanB7,0.0,false,SECC_BanB8,0.0,false,SECC_BanB9,0.0,false} 388 INFRA_DISABLE ONNEWDATA _ {INFRA_DisableBanA0,0.0,false,INFRA_DisableBanA1,0.0,false,INFRA_DisableBanA2,0.0,false,INFRA_DisableBanA3,0.0,false,INFRA_DisableBanA4,0.0,false,INFRA_DisableBanA5,0.0,false,INFRA_DisableBanA6,0.0,false,INFRA_DisableBanA7,0.0,false,INFRA_DisableBanA8,0.0,false,INFRA_DisableBanA9,0.0,false,INFRA_DisableBanAA,0.0,false,INFRA_DisableBanAB,0.0,false,INFRA_DisableBanAC,0.0,false,INFRA_DisableBanAD,0.0,false,INFRA_DisableStubA,0.0,false,INFRA_DisableBanB0,0.0,false,INFRA_DisableBanB1,0.0,false,INFRA_DisableBanB2,0.0,false,INFRA_DisableBanB3,0.0,false,INFRA_DisableBanB4,0.0,false,INFRA_DisableBanB5,0.0,false,INFRA_DisableBanB6,0.0,false,INFRA_DisableBanB7,0.0,false,INFRA_DisableBanB8,0.0,false,INFRA_DisableBanB9,0.0,false,INFRA_DisableBanBA,0.0,false,INFRA_DisableBanBB,0.0,false,INFRA_DisableBanBC,0.0,false,INFRA_DisableBanBD,0.0,false,INFRA_DisableStubB,0.0,false,INFRA_DisableBanC0,0.0,false,INFRA_DisableBanC1,0.0,false,INFRA_DisableBanC2,0.0,false,INFRA_DisableBanC3,0.0,false,INFRA_DisableBanC4,0.0,false,INFRA_DisableBanC5,0.0,false,INFRA_DisableBanC6,0.0,false,INFRA_DisableBanC7,0.0,false,INFRA_DisableBanC8,0.0,false,INFRA_DisableBanC9,0.0,false,INFRA_DisableBanCA,0.0,false,INFRA_DisableBanCB,0.0,false,INFRA_DisableBanCC,0.0,false,INFRA_DisableBanCD,0.0,false,INFRA_DisableStubC,0.0,false,INFRA_DisableBanD0,0.0,false,INFRA_DisableBanD1,0.0,false,INFRA_DisableBanD2,0.0,false,INFRA_DisableBanD3,0.0,false,INFRA_DisableBanD4,0.0,false,INFRA_DisableBanD5,0.0,false,INFRA_DisableBanD6,0.0,false,INFRA_DisableBanD7,0.0,false,INFRA_DisableBanD8,0.0,false,INFRA_DisableBanD9,0.0,false,INFRA_DisableBanDA,0.0,false,INFRA_DisableBanDB,0.0,false,INFRA_DisableBanDC,0.0,false,INFRA_DisableBanDD,0.0,false,INFRA_DisableStubS,0.0,false,INFRA_DisableStubD,0.0,false} 389 SECC_BASE_STATUS2 ONNEWDATA _ {SECC_BanD2,0.0,false,SECC_BanD3,0.0,false,SECC_BanD0,0.0,false,SECC_BanD1,0.0,false,SECC_BanD6,0.0,false,SECC_BanD7,0.0,false,SECC_BanD4,0.0,false,SECC_BanD5,0.0,false,SECC_BanD8,0.0,false,SECC_BanD9,0.0,false,SECC_BanC7,0.0,false,SECC_BanC6,0.0,false,SECC_BanC5,0.0,false,SECC_BanC4,0.0,false,SECC_BanC3,0.0,false,SECC_BanC2,0.0,false,SECC_BanC1,0.0,false,SECC_BanC0,0.0,false,SECC_BanC9,0.0,false,SECC_BanC8,0.0,false,SECC_BanCD,0.0,false,SECC_BanCC,0.0,false,SECC_BanCB,0.0,false,SECC_BanCA,0.0,false,SECC_BanDB,0.0,false,SECC_BanDC,0.0,false,SECC_BanDA,0.0,false,SECC_BanDD,0.0,false} 391 SECC_BASE_FAULT ONEXTERNALTRIGGER patate {SECC_Segment,4.0,false,SECC_BaseDTC_Priority,1.0,true,SECC_BaseTemperature,25.0,false,SECC_BaseDTC,2560.0,true,SECC_BaseDev,11.0,false} 393 SECC_PSUA_STATUS ONNEWDATA _ {} 394 SECC_PSUB_STATUS ONNEWDATA _ {} 395 SECC_PSUC_STATUS ONNEWDATA _ {} 396 SECC_PSUD_STATUS ONNEWDATA _ {} 397 SECC_PSUS_STATUS ONNEWDATA _ {} 398 SECC_ACDC_STATUS ONNEWDATA _ {} 1947 DTOOL_to_LBC ONNEWDATA _ {} 415 PEB_CANHS_R_03 ONNEWDATA _ {} 1695 VehicleID_CANHS_R_01 ONNEWDATA _ {} 928 VEH2_WEVC_CONTROL ONNEWDATA _ {} 672 VEH1_WEVC_CONTROL ONNEWDATA _ {} 416 WEVC_CONTROL ONNEWDATA _ {} 929 VEH2_WEVC_STATUS ONNEWDATA _ {} 417 WEVC_STATUS ONNEWDATA _ {} 673 VEH1_WEVC_STATUS ONNEWDATA _ {} 418 WEVC_HMI_REQUEST ONNEWDATA _ {} 1059 SCH_CANHS_R_02 ONNEWDATA _ {} 1060 LBC_CANHS_R_01 ONNEWDATA _ {} 420 WEVC_VEH_IDENTIFICATION ONNEWDATA _ {} 1061 LBC_CANHS_R_02 ONNEWDATA _ {} 421 WEVC_CHARACTERISTICS ONNEWDATA _ {} 1062 SCH_CANHS_R_03 ONNEWDATA _ {} 424 WEVC_VEH_SPEED_ODO ONNEWDATA _ {} 1198 LBC_CANHS_R_08 ONNEWDATA _ {} 1199 LBC_CANHS_R_09 ONNEWDATA _ {} 431 WEVC_DEBUG_LOG ONNEWDATA _ {} 1712 GPS_COORDINATES ONNEWDATA _ {} 1716 GPS_TIME ONNEWDATA _ {} 1077 SCH_CANHS_R_12 ONNEWDATA _ {} 1082 LBC_CANHS_R_07 ONNEWDATA _ {} 1979 LBC_to_DTOOL ONNEWDATA _ {} 1093 LBC2_CANHS_R_01 ONNEWDATA _ {} 204 OpenRelays ONNEWDATA _ {} 1365 FABRIC_01 ONNEWDATA _ {} 341 LBC_CANHS_R_03 ONNEWDATA _ {} 1366 FABRIC_02 ONNEWDATA _ {} 1625 LBC_CANHS_R_04 ONNEWDATA _ {} 229 EmergencyStop1 ONNEWDATA _ {} 230 EmergencyStop2 ONNEWDATA _ {} 231 EmergencyStop3 ONNEWDATA _ {} 2032 LOG_COMMAND ONNEWDATA _ {} 2033 LOG_STATUS ONNEWDATA _ {} 2034 LOG_TEST_CASE ONNEWDATA _ {} 2036 LOG_TESTER_NAME ONNEWDATA _ {} 2038 LOG_COMMENT ONNEWDATA _ {}");
		// ee.getAsText();
		new FxTest().launchIHM(args, () -> new VBox(ee.getEditor()), e -> System.out.println(ee.getAsText()));
	}

	private Accordion messages;
	private MessageIdentifier[] possibleMessages;
	private HashSet<String> triggers = new HashSet<>();

	@Override
	public String getAsText() {
		EncodingMessageProperties[] dbcemp = getValue();
		if (dbcemp == null)
			return "";
		StringBuilder sb = new StringBuilder();
		for (int j = 0; j < dbcemp.length; j++) {
			EncodingMessageProperties mi = dbcemp[j];
			sb.append((j == 0 ? "" : " ") + mi.getId() + " " + mi.getName() + " " + mi.getSendMode() + " " + (mi.getAdditionalInfo() == null ? "_ {" : mi.getAdditionalInfo() + " {"));
			ArrayList<EncodingSignalProperties> signals = mi.getSignalProperties();
			for (int i = 0; i < signals.size() - 1; i++) {
				EncodingSignalProperties signal = signals.get(i);
				sb.append(signal.getName() + "," + signal.getValue() + "," + signal.isAsInput() + ",");
			}
			if (!signals.isEmpty()) {
				EncodingSignalProperties signal = signals.get(signals.size() - 1);
				sb.append(signal.getName() + "," + signal.getValue() + "," + signal.isAsInput());
			}
			sb.append("}");
		}
		return sb.toString();
	}

	@Override
	protected Region getCustomEditor() {
		if (messages != null)
			return messages;
		messages = new Accordion();
		populateAccordion();
		messages.heightProperty().addListener(e -> {
			new Thread(() -> { // BUG Javafx RT-? Erreur de visu sans le thread, version simplifiée: test.bugSize.TestGridPane
				Platform.runLater(() -> fireSizeChanged());
			}).start();
		});
		messages.setPrefWidth(540);
		return messages;
	}

	private static ArrayList<EncodingSignalProperties> getEncodingSignals(String signals) {
		StringTokenizer sts = new StringTokenizer(signals.substring(1, signals.length() - 1), ",");
		ArrayList<EncodingSignalProperties> encodingSignalPropertiesSet = new ArrayList<>();
		while (sts.hasMoreTokens())
			encodingSignalPropertiesSet.add(new EncodingSignalProperties(sts.nextToken(), Double.parseDouble(sts.nextToken()), Boolean.parseBoolean(sts.nextToken())));
		return encodingSignalPropertiesSet;
	}

	private Node getMessageContent(MessageIdentifier canMessageIdentifier, EncodingMessageProperties emp) {
		Label tml = new Label("Trigger Mode: ");
		Font titleColumnFont = Font.font(tml.getFont().getFamily(), FontWeight.BOLD, tml.getFont().getSize());
		tml.setFont(titleColumnFont);
		EnumEditor<SendMode> sendModeEditor = new EnumEditor<>(SendMode.class);
		sendModeEditor.setValue(emp.getSendMode());
		HBox triggerBox = new HBox(tml, sendModeEditor.getEditor());// enum triggermode, valeur time,
		triggerBox.setAlignment(Pos.CENTER);
		triggerBox.setSpacing(5);
		Runnable updateControl = () -> {
			SendMode sendMode = sendModeEditor.getValue();
			ObservableList<Node> child = triggerBox.getChildren();
			if (sendMode == SendMode.TIMER) {
				IntegerEditor te = new IntegerEditor(0, Integer.MAX_VALUE, ControlType.SPINNER);
				te.addPropertyChangeListener(() -> {
					emp.setAdditionalInfo(te.getValue().toString());
					firePropertyChangeListener();
				});
				// if (needToUpdateValue)
				String addInfo = emp.getAdditionalInfo();
				int val = 100;
				try {
					val = Integer.parseInt(addInfo);
				} catch (NumberFormatException e) {}
				te.setValue(val);
				child.add(te.getEditor());
				child.add(new Label("ms"));
			} else if (sendMode == SendMode.ONEXTERNALTRIGGER) {
				StringEditor te = new StringEditor(updateExternalTrigger());
				te.addPropertyChangeListener(() -> {
					String triggerName = te.getValue();
					if (triggerName == NEWTRIGGER) {
						Stage dialog = new Stage(StageStyle.DECORATED);
						dialog.initOwner(triggerBox.getScene().getWindow());
						dialog.setTitle("External Trigger Name");
						TextField extf = new TextField();
						BorderPane bp = new BorderPane(extf);
						Button okButton = new Button("Ok");
						BorderPane.setAlignment(okButton, Pos.CENTER);
						okButton.setOnAction(e1 -> {
							String text = extf.getText();
							if (!text.isEmpty() && !text.equals("_")) {
								triggers.add(text);
								te.setPossibilities(updateExternalTrigger());
								te.setValue(text);
								emp.setAdditionalInfo(text);
								firePropertyChangeListener();
								dialog.close();
							}
						});
						bp.setBottom(okButton);
						dialog.setScene(new Scene(bp, -1, -1));
						dialog.sizeToScene();
						dialog.show();
					} else {
						emp.setAdditionalInfo(triggerName);
						firePropertyChangeListener();
					}
				});
				String trigger = emp.getAdditionalInfo();
				try {
					Integer.parseInt(trigger);
					trigger = null;
					emp.setAdditionalInfo(null);
				} catch (NumberFormatException e) {}
				if (trigger != null && !triggers.contains(trigger))
					triggers.add(trigger); // Je dois charger tout les triggers...
				te.setValue(trigger);
				child.add(te.getEditor());
			}
			emp.setSendMode(sendMode);
			firePropertyChangeListener();
		};
		updateControl.run();
		sendModeEditor.addPropertyChangeListener(() -> {
			ObservableList<Node> child = triggerBox.getChildren();
			while (child.size() != 2)
				child.remove(child.size() - 1);
			updateControl.run();
		});
		GridPane gp = new GridPane();
		gp.setBorder(new Border(new BorderStroke(Color.DARKGREY, BorderStrokeStyle.SOLID, new CornerRadii(3), null, null)));
		gp.setPadding(new Insets(5));
		gp.setAlignment(Pos.BASELINE_CENTER);
		gp.setHgap(10);
		ColumnConstraints columnConstraints = new ColumnConstraints();
		columnConstraints.setHgrow(Priority.NEVER);
		gp.getColumnConstraints().add(columnConstraints);
		ColumnConstraints columnConstraints2 = new ColumnConstraints();
		columnConstraints2.setHgrow(Priority.ALWAYS);
		gp.getColumnConstraints().add(columnConstraints2);
		ColumnConstraints columnConstraints3 = new ColumnConstraints();
		columnConstraints3.setFillWidth(false);
		columnConstraints3.setHalignment(HPos.CENTER);
		columnConstraints3.setHgrow(Priority.NEVER);
		gp.getColumnConstraints().add(columnConstraints3);
		Label snl = new Label("Signal Name");
		snl.setFont(titleColumnFont);
		snl.setPadding(new Insets(0, 0, 5, 0));
		gp.add(snl, 0, 0);
		Label vall = new Label("Value");
		vall.setFont(titleColumnFont);
		vall.setPadding(new Insets(0, 0, 5, 0));
		gp.add(vall, 1, 0);
		Label ail = new Label("AsInput");
		ail.setFont(titleColumnFont);
		ail.setPadding(new Insets(0, 0, 5, 0));
		gp.add(ail, 2, 0);
		ArrayList<SignalIdentifier> signals = canMessageIdentifier.getSignals();
		ArrayList<EncodingSignalProperties> signalsProperties = emp.getSignalProperties();
		ArrayList<EncodingSignalProperties> newSignalProperties = new ArrayList<>();
		int marked = 0;
		for (int i = 0; i < signals.size(); i++) {
			SignalIdentifier signal = signals.get(i);
			EncodingSignalProperties _esp = null;
			for (EncodingSignalProperties encodingSignalProperties : signalsProperties)
				if (encodingSignalProperties.getName().equals(signal.getName())) {
					_esp = encodingSignalProperties;
					marked++;
					break;
				}
			if (_esp == null)
				_esp = new EncodingSignalProperties(signal.getName());
			EncodingSignalProperties esp = _esp;
			newSignalProperties.add(esp);
			Label l = new Label(signal.getName());
			if (signal.getMin() != -Float.MAX_VALUE || signal.getMax() != Float.MAX_VALUE || signal.getEnumList() != null) {
				StringBuilder sb = new StringBuilder();
				if (signal.getEnumList() != null) {
					signal.getEnumList().forEach((val, name) -> sb.append(val + ": " + name + "\n"));
					sb.substring(0, sb.length() - 1);
				} else {
					if (signal.getMin() != -Float.MAX_VALUE) {
						sb.append("Min: " + signal.getMin());
						if (signal.getMax() != Float.MAX_VALUE)
							sb.append("\nMax: " + signal.getMax());
					} else
						sb.append("Max: " + signal.getMax());
					if (signal.getUnit() != null && !signal.getUnit().isEmpty())
						sb.append("\nunit: " + signal.getUnit());
				}
				l.setTooltip(new Tooltip(sb.toString()));
			}
			gp.add(l, 0, i + 1);
			PropertyEditor<?> valueEditor;
			if (signal.getEnumList() != null) {
				Collection<String> values = signal.getEnumList().values();
				String[] aValues = values.toArray(new String[values.size()]);
				StringEditor se = new StringEditor(aValues);
				String sv = signal.getEnumList().get((int) esp.getValue());
				se.setValue(sv == null ? signal.getEnumList().values().iterator().next() : sv);
				valueEditor = se;
			} else {
				DoubleEditor de = new DoubleEditor(signal.getMin(), signal.getMax(), 1, IncrementMode.LINEAR, ControlType.SPINNER);
				de.setValue(esp.getValue());
				valueEditor = de;
			}
			gp.add(valueEditor.getEditor(), 1, i + 1);

			valueEditor.addPropertyChangeListener(() -> {
				Object value = valueEditor.getValue();
				if (value instanceof Double)
					esp.setValue((double) value);
				else {
					HashMap<Integer, String> enumList = signal.getEnumList();
					for (Integer enumValue : enumList.keySet()) {
						String enumName = enumList.get(enumValue);
						if (enumName.equals(value)) {
							esp.setValue(enumValue);
							break;
						}
					}
				}
				firePropertyChangeListener();
			});
			BooleanEditor cb = new BooleanEditor();
			cb.setValue(esp.isAsInput());
			gp.add(cb.getEditor(), 2, i + 1);
			cb.addPropertyChangeListener(() -> {
				esp.setAsInput(cb.getValue());
				firePropertyChangeListener();
			});
		}
		if (signals.size() != signalsProperties.size() || signals.size() != marked) {
			signalsProperties.clear();
			signalsProperties.addAll(newSignalProperties);
		}
		VBox mesBox = new VBox(triggerBox, gp);
		mesBox.setSpacing(5);
		return mesBox;
	}

	@Override
	public boolean hasCustomEditor() {
		return true;
	}

	private static boolean isEquals(MessageIdentifier[] mi1, MessageIdentifier[] mi2) {
		if (mi1 == null)
			return mi2 == null;
		if (mi1.length == mi2.length) {
			for (int i = 0; i < mi1.length; i++)
				if (!mi1[i].equals(mi2[i]))
					return false;
			return true;
		}
		return false;
	}

	@Override
	public boolean isFixedControlSized() {
		return false;
	}

	private void populateAccordion() {
		ArrayList<TitledPane> panes = new ArrayList<>();
		if (possibleMessages != null)
			for (int i = 0; i < possibleMessages.length; i++) {
				MessageIdentifier possibleMessage = possibleMessages[i];
				EncodingMessageProperties emp = getValue()[i];
				TitledPane tp = new TitledPane(possibleMessage.getId() + " " + possibleMessage.getName(), null);
				if (emp.getSendMode() == SendMode.ONEXTERNALTRIGGER && emp.getAdditionalInfo() != null)
					triggers.add(emp.getAdditionalInfo());
				EncodingMessageProperties _emp = emp;
				tp.setOnMousePressed(e -> {
					if (tp.getContent() == null)
						tp.setContent(getMessageContent(possibleMessage, _emp));
				});
				tp.setAnimated(false);
				panes.add(tp);
			}
		if (panes.isEmpty())
			panes.add(new TitledPane("No message selected", null));
		messages.getPanes().setAll(panes); // BUG Javafx RT-? , version simplifiée: test.bugSize.AccordionHeightBug
	}

	@Override
	public void setAsText(String text) {
		StringTokenizer stm = new StringTokenizer(text, " ");
		ArrayList<EncodingMessageProperties> encodingMessagesProperties = new ArrayList<>();
		while (stm.hasMoreTokens())
			encodingMessagesProperties
					.add(new EncodingMessageProperties(Integer.parseInt(stm.nextToken()), stm.nextToken(), SendMode.valueOf(stm.nextToken()), stm.nextToken(), getEncodingSignals(stm.nextToken())));
		setValue(encodingMessagesProperties.toArray(new EncodingMessageProperties[encodingMessagesProperties.size()]));
	}

	public void setMessageIdentifier(MessageIdentifier[] possibilities) {
		if (isEquals(this.possibleMessages, possibilities))
			return;
		this.possibleMessages = possibilities;
		EncodingMessageProperties[] newValue = new EncodingMessageProperties[possibilities.length];
		EncodingMessageProperties[] oldValues = getValue();
		nextMessage: for (int i = 0; i < possibleMessages.length; i++) {
			MessageIdentifier mi = possibleMessages[i];
			if (oldValues != null)
				for (int j = 0; j < oldValues.length; j++) {
					EncodingMessageProperties oldValue = oldValues[j];
					if (oldValue.getId() == mi.getId() && oldValue.getName().equals(mi.getName())) {
						newValue[i] = oldValue;
						continue nextMessage;
					}
				}
			newValue[i] = new EncodingMessageProperties(mi.getId(), mi.getName());
		}
		setValue(newValue);
		if (messages != null)
			FxUtils.runLaterIfNeeded(() -> populateAccordion());
	}

	private String[] updateExternalTrigger() {
		String[] triggersNames = new String[triggers.size() + 1];
		int index = 0;
		for (String triggerName : triggers)
			triggersNames[index++] = triggerName;
		triggersNames[triggersNames.length - 1] = NEWTRIGGER;
		return triggersNames;
	}
}