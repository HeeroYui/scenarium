/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.editors.can.dbc;

import org.beanmanager.editors.PropertyEditor;
import org.scenarium.communication.can.dbc.MessageIdentifier;

public class MessageIdentifierEditor extends PropertyEditor<MessageIdentifier> {

	@Override
	public String getAsText() {
		MessageIdentifier cvi = getValue();
		return cvi.getId() + " " + cvi.getName() + " " + cvi.getSize();
	}

	@Override
	public void setAsText(String text) {
		int i1 = text.indexOf(" ");
		int i2 = text.lastIndexOf(" ");
		setValue(new MessageIdentifier(Integer.parseInt(text.substring(0, i1)), text.substring(i1 + 1, i2), Integer.parseInt(text.substring(i2 + 1)), null));
	}
}
