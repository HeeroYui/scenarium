/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.basic;

import org.scenarium.filemanager.scenario.dataflowdiagram.operator.EvolvedVarArgsOperator;
import org.scenarium.operator.AbstractViewer;

import javafx.scene.Scene;
import javafx.scene.control.TextArea;

public class FlowViewer extends AbstractViewer implements EvolvedVarArgsOperator{

	private TextArea ta;

	@Override
	protected void closeViewer() {
		super.closeViewer();
		ta = null;
	}

	@Override
	protected void paintDatas(Object[] objs) {
		if (stage == null) {
			initStage();
			if (stage == null)
				return;
			ta = new TextArea();
			stage.setScene(new Scene(ta));
			stage.show();
		}
		String text = objs[0].toString();
		if (!text.endsWith("\n"))
			text += "\n";
		ta.appendText(text);
	}

	@Override
	public boolean canAddInput(Class<?>[] inputsType) {
		return false;
	}

	@Override
	public boolean isValidInput(Class<?>[] inputsType, Class<?> additionalInput) {
		return true;
	}

}
