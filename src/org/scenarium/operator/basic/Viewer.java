/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.basic;

import javax.vecmath.Point2i;

import org.beanmanager.editors.DynamicEnableBean;
import org.beanmanager.editors.PropertyEditor;
import org.beanmanager.editors.PropertyEditorManager;
import org.beanmanager.editors.PropertyInfo;
import org.beanmanager.editors.UpdatableViewBean;
import org.beanmanager.editors.container.BeanEditor;
import org.beanmanager.editors.container.BeanInfo;
import org.beanmanager.editors.container.DynamicBeanInfo;
import org.beanmanager.tools.FxUtils;
import org.scenarium.display.RenderPane;
import org.scenarium.display.ScenariumContainer;
import org.scenarium.display.StackableDrawer;
import org.scenarium.display.drawer.DrawerManager;
import org.scenarium.display.drawer.PrimitiveDrawer;
import org.scenarium.display.drawer.TheaterPanel;
import org.scenarium.display.toolbarclass.Tool;
import org.scenarium.filemanager.scenario.dataflowdiagram.operator.EvolvedVarArgsOperator;
import org.scenarium.operator.AbstractViewer;
import org.scenarium.timescheduler.Scheduler;

import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextInputControl;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Viewer extends AbstractViewer implements ScenariumContainer, EvolvedVarArgsOperator, DynamicEnableBean, UpdatableViewBean {
	private Region renderComponent;
	private Object editor;
	@PropertyInfo(index = 4, info = "Define if the viewer size is calculated automatically or defined manually by the user")
	private boolean autoSize = true;
	@PropertyInfo(index = 5, info = "Refresh on each ne additional inputs")
	private boolean refreshOnNewAdditionalInputs = false;
	@PropertyInfo(index = 6, info = "Properties of the theater panel generated during the last run"/* , readOnly = true */)
	@BeanInfo(inline = true, alwaysExtend = true)
	@DynamicBeanInfo(possibleSubclassesMethodName = "getTypes")
	public TheaterPanel theaterPane;
	private Class<?> oldDrawClass;
	private Class<?>[] oldDrawedObjs = new Class<?>[0];

	@Override
	public void initStruct() {
		super.initStruct();
		if (theaterPane != null)
			theaterPane.initStruct(this);
	}

	@Override
	public void paintDatas(Object[] objs) {
		Stage _stage = stage;
		Region _renderComponent = renderComponent;
		Class<?> _oldDrawClass = oldDrawClass;
		// Object _editor = editor;
		Object obj = objs[0];
		if (obj == null || objs.length == 0)
			throw new IllegalArgumentException("Cannot paint datas without the first data");
		Class<?> objClass = obj.getClass();
		boolean needToReset;
		if (_oldDrawClass == null)
			needToReset = true;
		else {
			if (((editor == null || editor instanceof PropertyEditor) && objs.length != 1) || (editor instanceof PropertyEditor[] && objs.length == 1))
				needToReset = true;
			else {
				needToReset = !_oldDrawClass.isAssignableFrom(objClass);
				if (needToReset)
					if (theaterPane != null && DrawerManager.getSpecializedDrawableObjectClass(oldDrawClass).isAssignableFrom(objClass)) // Inversion dans getRenderPanelParentObject
						needToReset = false;
					else if (editor != null) {
						PropertyEditor<?> _editor = PropertyEditorManager.findEditor(objClass, "");
						if (_editor != null && _editor.getClass() == editor.getClass())
							needToReset = false;
					}
			}
		}
		if (needToReset) {
			oldDrawedObjs = new Class<?>[0];
			editor = null;
			_renderComponent = null;
			String beanName = getBlockName();
			Class<? extends TheaterPanel> type = DrawerManager.getRenderPanelClass(objClass);
			if (type != null && !type.isAssignableFrom(PrimitiveDrawer.class)) { // Drawer view
				RenderPane renderpanel;
				try {
					renderpanel = theaterPane == null || theaterPane.getClass() != type ? new RenderPane((Scheduler) null, obj, Viewer.this, null, false, true) : new RenderPane((Scheduler) null, obj, Viewer.this, theaterPane, false, true);
				} catch (SecurityException | IllegalArgumentException e) {
					e.printStackTrace();
					if (!isDefaulting) {
						isDefaulting = true;
						System.err.println("Cannot create a renderPanel for the View for the drawable element of type: " + objClass.getSimpleName());
					}
					return;
				}
				_renderComponent = renderpanel.getPane();
				editor = renderpanel;
				if (theaterPane != renderpanel.getTheaterPane()) {
					theaterPane = renderpanel.getTheaterPane();
					propertyChanged = true;
				}
				theaterPane.updateTheaterFilter();
				if (objs.length > 1) {
					Object[] des = new Object[objs.length - 1];
					System.arraycopy(objs, 1, des, 0, des.length);
					theaterPane.setAdditionalDrawableElement(des);
				}
				theaterPane.animated = animated;
				theaterPane.addTheaterFilterPropertyChangeListener(e -> propertyChanged = true);
			} else { // Property view
				if (objs.length == 1) { // Single property view
					PropertyEditor<?> propEditor = PropertyEditorManager.findEditor(objClass, "");
					if (propEditor != null && !(propEditor instanceof BeanEditor) && propEditor.hasCustomView()) {
						propEditor.setValueFromObj(obj);
						_renderComponent = propEditor.getView();
						if (_renderComponent instanceof TextInputControl)
							((TextInputControl) _renderComponent).setEditable(false);
						editor = propEditor;
					}
					if (_renderComponent == null)
						_renderComponent = new Label(obj.toString());
				} else {
					_renderComponent = new GridPane();
					updateGridPane(objs, (GridPane) _renderComponent);
				}
				theaterPane = null;
			}
			if (_stage == null) {
				_stage = initStage();
				if (_stage == null)
					return;
				_stage.setScene(new Scene(_renderComponent));
			} else
				_stage.getScene().setRoot(_renderComponent);
			_stage.getIcons().add(new Image(getClass().getResourceAsStream(theaterPane == null ? "/scenarium_icon_viewer.png" : theaterPane.geticonName()))); // TODO cr�er icone
			_renderComponent.setMinWidth(170 + new Text(beanName).getBoundsInLocal().getWidth());
			_stage.setTitle(beanName);
			renderComponent = _renderComponent;
			if (autoSize)
				_stage.sizeToScene();
			// ScenicView.show(stage.getScene());
			stage = _stage;
			if (theaterPane != null) {
				theaterPane.ignoreRepaint = false;
				theaterPane.repaint(false);
			}
			// renderComponent = _renderComponent; //Remonter sinon _stage.sizeToScene ne prend pas en compte la modif
			if (!_stage.isShowing()) { // Relance une boucle de draw dans le draw..., c'est le bordel après si j'ai pas finis la boucle
				oldDrawClass = objClass;
				_stage.show();
				return;
			}
		} else if (editor instanceof RenderPane) {
			TheaterPanel tp = ((RenderPane) editor).getTheaterPane();
			if (tp.getDrawableElement() != obj)
				tp.setDrawableElement(obj);
			if (objs.length >= 1) {
				Object[] des = new Object[objs.length - 1];
				System.arraycopy(objs, 1, des, 0, des.length);
				tp.setAdditionalDrawableElement(des);
			}
			tp.paintImmediately(false); // TODO si pas true, pas de rafraichissement des images si elles changent de taille
		} else if (editor instanceof PropertyEditor) {
			((PropertyEditor<?>) editor).setValueFromObj(obj);
		} else if (editor instanceof PropertyEditor[]) {
			if (updateGridPane(objs, (GridPane) _renderComponent))
				_stage.sizeToScene();
		} else {
			Label label = (Label) _renderComponent;
			String objAsString = obj.toString();
			label.setText(objAsString);
			if (label.getWidth() < new Text(objAsString).getLayoutBounds().getWidth())
				_stage.sizeToScene();
		}
		oldDrawClass = objClass;
	}

	private boolean updateGridPane(Object[] objs, GridPane gridPane) {
		boolean sizeToScene = false;
		PropertyEditor<?>[] editors = (PropertyEditor<?>[]) editor;
		String[] propertyNames = getOutputLinkToInputName();
		if (oldDrawedObjs.length != objs.length) {
			oldDrawedObjs = new Class<?>[objs.length];
			editors = new PropertyEditor<?>[objs.length];
			editor = editors;
			gridPane.getChildren().clear();
			for (int i = 0; i < editors.length; i++) {
				gridPane.add(new Label(propertyNames[i]), 0, i);
				gridPane.add(new Label(": "), 1, i);
			}
			sizeToScene = true;
		}
		for (int j = 0; j < editors.length; j++) {
			Object subObj = objs[j];
			if (subObj != null)
				if (oldDrawedObjs[j] != subObj.getClass()) { // changement de class
					Region subRenderComponent = null;
					PropertyEditor<?> propEditor = PropertyEditorManager.findEditor(subObj.getClass(), "");
					if (propEditor != null && !(propEditor instanceof BeanEditor) && propEditor.hasCustomView()) {
						propEditor.setValueFromObj(subObj);
						subRenderComponent = propEditor.getView();
						if (subRenderComponent instanceof TextInputControl)
							((TextInputControl) subRenderComponent).setEditable(false);
						editors[j] = propEditor;
					}
					if (subRenderComponent == null)
						subRenderComponent = new Label(subObj.toString());
					Node renderComponent = getNodeFromGridPane(gridPane, 2, j);
					if (renderComponent != null)
						gridPane.getChildren().remove(renderComponent);
					gridPane.add(subRenderComponent, 2, j);
					oldDrawedObjs[j] = subObj.getClass();
				} else {
					// if (true)
					// return false;
					PropertyEditor<?> subEditor = editors[j];
					Label subLabelPropertyName = ((Label) getNodeFromGridPane((gridPane), 0, j));
					if (subLabelPropertyName.getText() != propertyNames[j])
						subLabelPropertyName.setText(propertyNames[j]);
					if (editors[j] == null) {
						Label subLabelValue = ((Label) getNodeFromGridPane((gridPane), 2, j));
						String objAsString = subObj.toString();
						subLabelValue.setText(objAsString);
						if (subLabelValue.getWidth() < new Text(objAsString).getLayoutBounds().getWidth())
							sizeToScene = true;
					} else
						subEditor.setValueFromObj(subObj);
				}
		}
		return sizeToScene;
	}

	private static Node getNodeFromGridPane(GridPane gridPane, int col, int row) {
		for (Node node : gridPane.getChildren())
			if (GridPane.getColumnIndex(node) == col && GridPane.getRowIndex(node) == row)
				return node;
		return null;
	}

	@Override
	protected void closeViewer() {
		Object _editor = editor;
		editor = null;
		if (_editor != null && _editor instanceof RenderPane)
			((RenderPane) _editor).close();
		oldDrawClass = null;
		renderComponent = null;
	}

	@Override
	public boolean canAddInput(Class<?>[] inputsType) {
		if (editor == null || editor instanceof PropertyEditor<?> || editor instanceof PropertyEditor<?>[]) {
			Class<? extends TheaterPanel> type = DrawerManager.getRenderPanelClass(inputsType[0]);
			if (type == PrimitiveDrawer.class)
				return true;
			if (type != null && StackableDrawer.class.isAssignableFrom(type))
				return TheaterPanel.canAddInputToRenderer(type.asSubclass(StackableDrawer.class), inputsType);
		}
		if (editor instanceof RenderPane) {
			TheaterPanel tp = ((RenderPane) editor).getTheaterPane();
			if (tp instanceof StackableDrawer)
				return ((StackableDrawer) tp).canAddInputToRenderer(inputsType);
		}
		return false;
	}

	@Override
	public boolean isValidInput(Class<?>[] inputsType, Class<?> additionalInput) {
		if (inputsType.length == 0 || inputsType.length == 1 && inputsType[0] == null)
			return true;
		if (editor == null) {
			Class<? extends TheaterPanel> mainType = DrawerManager.getRenderPanelClass(inputsType[0]);
			if (mainType == PrimitiveDrawer.class)
				return DrawerManager.getRenderPanelClass(additionalInput) == PrimitiveDrawer.class;
			if (mainType != null && StackableDrawer.class.isAssignableFrom(mainType))
				return TheaterPanel.isValidAdditionalInput(mainType.asSubclass(StackableDrawer.class), inputsType, additionalInput);
		}
		if (editor instanceof RenderPane) {
			TheaterPanel tp = ((RenderPane) editor).getTheaterPane();
			if (tp instanceof StackableDrawer)
				return ((StackableDrawer) tp).isValidAdditionalInput(inputsType, additionalInput);
		}
		return true;
	}

	@Override
	protected boolean needToForceRefresh() {
		return theaterPane != null && theaterPane.needToBeRefresh;
	}

	@Override
	protected boolean needToRefreshForNewAdditionalInputs() {
		return refreshOnNewAdditionalInputs;
	}

	@Override
	public void setAnimated(boolean animated) {
		super.setAnimated(animated);
		if (theaterPane != null)
			theaterPane.animated = animated;
	}

	public boolean isAutoSize() {
		return autoSize;
	}

	public void setAutoSize(boolean autoSize) {
		this.autoSize = autoSize;
		Stage _stage = stage;
		if (_stage != null)
			FxUtils.runLaterIfNeeded(() -> {
				if (autoSize)
					stage.sizeToScene();
				setEnable();
				updateView();
			});
	}

	@Override
	public void setEnable() {
		fireSetPropertyEnable(this, "dimension", !autoSize);
	}

	public boolean isRefreshOnNewAdditionalInputs() {
		return refreshOnNewAdditionalInputs;
	}

	public void setRefreshOnNewAdditionalInputs(boolean refreshOnNewAdditionalInputs) {
		this.refreshOnNewAdditionalInputs = refreshOnNewAdditionalInputs;
	}

	public TheaterPanel getTheaterPane() {
		return theaterPane;
	}

	// Introspection pour la liste des types possible de drawer
	public Class<?>[] getTypes() {
		return DrawerManager.getDrawers().toArray(new Class<?>[0]);
	}

	public void setTheaterPane(TheaterPanel theaterPane) {
		this.theaterPane = theaterPane;
	}

	// public boolean triggerOutput(String outputName, Object outputValue) {
	// Object[] vec = generateOuputsVector();
	// vec[getOutputIndex(outputName)] = outputValue;
	// return super.triggerOutput(vec, System.currentTimeMillis());
	// }

	@Override
	public void updateView() {
		fireUpdateView(this, "dimension", false);
	}

	@Override
	public void adaptSizeToDrawableElement() {
		if (editor instanceof RenderPane)
			((RenderPane) editor).adaptSizeToDrawableElement();
		if (!autoSize) {
			autoSize = true;
			propertyChanged = true;
		}
	}

	@Override
	public Point2i getDefaultToolBarLocation(String simpleName) {
		return null;
	}

	@Override
	public Scheduler getScheduler() {
		return null;
	}

	@Override
	public int getSelectedElementFromTheaterEditor() {
		return 0;
	}

	@Override
	public boolean isDefaultToolBarAlwaysOnTop(String simpleName) {
		return false;
	}

	@Override
	public boolean isManagingAccelerator() {
		return false;
	}

	@Override
	public boolean isStatusBar() {
		return editor instanceof RenderPane ? ((RenderPane) editor).isStatusBar() : false;
	}

	@Override
	public void saveScenario() {}

	@Override
	public void showMessage(String message, boolean error) {}

	@Override
	public void updateStatusBar(String... infos) {
		if (editor instanceof RenderPane)
			((RenderPane) editor).updateStatusBar(infos);
	}

	@Override
	public void updateToolView(Class<? extends Tool> toolClass, boolean isVisible) {}

	@Override
	public void showTool(Class<? extends Tool> toolClass) {}
}
