/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.basic;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

import org.beanmanager.editors.PropertyInfo;
import org.beanmanager.editors.primitive.number.ControlType;
import org.beanmanager.editors.primitive.number.NumberInfo;
import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;

public class Beep extends EvolvedOperator {
	class BeepThread extends Thread {
		@Override
		public void run() {
			byte[] buf = new byte[2];
			SourceDataLine sdl = null;
			while (sdl == null) {
				AudioFormat af = new AudioFormat(SAMPLE_RATE, 8, 2, true, false);
				try {
					sdl = AudioSystem.getSourceDataLine(af);
					sdl.open(af);
					sdl.start();
				} catch (LineUnavailableException e) {
					if (sdl != null) {
						sdl.stop();
						sdl.close();
						sdl = null;
					}
					e.printStackTrace();
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e1) {
						e1.printStackTrace();
						return;
					}
				}
			}
			int i = 0;
			double _periode = periode;
			int _hz = hz;

			while (!isInterrupted() && isAlive) {
				i++;
				double time = _periode * SAMPLE_RATE;
				double angle = i / (SAMPLE_RATE / _hz) * 2.0 * Math.PI;
				buf[0] = i % time < time / 2 ? (byte) (Math.sin(angle) * 127.0 * volume * (balance < 0 ? 1 : 1 - balance)) : 0;
				buf[1] = i % time < time / 2 ? (byte) (Math.sin(angle) * 127.0 * volume * (balance > 0 ? 1 : 1 + balance)) : 0;
				sdl.write(buf, 0, 2);
				if (i >= time) {
					i = 0;
					_periode = periode;
					_hz = hz;
				}
			}
		}
	}

	public static float SAMPLE_RATE = 44100;
	@PropertyInfo(index = 0, info = "frequence du signal sonor")
	private int hz = 2000;
	@PropertyInfo(index = 1, info = "période des beeps")
	private double periode = 1;
	@PropertyInfo(index = 2)
	@NumberInfo(min = 0, max = 1, controlType = ControlType.SPINNERANDSLIDER)
	private double volume = 1;
	@PropertyInfo(index = 3)
	@NumberInfo(min = -1, max = 1, controlType = ControlType.SPINNERANDSLIDER)
	private double balance = 0;
	private Thread t;

	private volatile boolean isAlive = false;

	@Override
	public void birth() {
		isAlive = true;
		t = new BeepThread();
		t.start();
	}

	@Override
	public void death() {
		if (t != null) {
			isAlive = false;
			t.interrupt();
			t = null;
		}
	}

	public double getBalance() {
		return balance;
	}

	public int getHz() {
		return hz;
	}

	public double getPeriode() {
		return periode;
	}

	public double getVolume() {
		return volume;
	}

	public void process() {}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public void setHz(int hz) {
		this.hz = hz;
	}

	public void setPeriode(double periode) {
		this.periode = periode;
	}

	public void setVolume(double volume) {
		this.volume = volume;
	}
}
