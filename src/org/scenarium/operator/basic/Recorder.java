/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.basic;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;

import org.beanmanager.editors.PropertyInfo;
import org.scenarium.editors.NotChangeableAtRuntime;
import org.scenarium.filemanager.filerecorder.FileStreamRecorder;
import org.scenarium.filemanager.scenario.dataflowdiagram.ParamInfo;
import org.scenarium.filemanager.scenario.dataflowdiagram.VarArgsInputChangeListener;
import org.scenarium.filemanager.scenario.dataflowdiagram.operator.EvolvedVarArgsOperator;
import org.scenarium.operator.AbstractRecorder;

public class Recorder extends AbstractRecorder implements VarArgsInputChangeListener, EvolvedVarArgsOperator {
	public static final String recorIndexSeparator = "_";
	private String[] outputLinkToInputName;
	private Class<?>[] outputLinkToInputType;
	private FileStreamRecorder[] streamRecorders;
	private RandomAccessFile recFos; // Le passer en dataoutputstream
	private String recordedPath;
	private int recordId[];
	private int recordIdCpt = 0;
	private File recFile;
	@PropertyInfo(index = 10, info = "Store the scheduling file in text format, use more space but can be read and modify easily")
	@NotChangeableAtRuntime
	private boolean textMode = false;

	private int recordPt;
	private boolean isWarning;

	// TODO sceptique..., ca marche en compact?
	private void addToHeader(int indexOfInput) {
		try {
			recFos.close();
			File tempFile = new File(recFile.getAbsolutePath() + "-");
			recFile.renameTo(tempFile);
			recFos = new RandomAccessFile(recFile, "rw");
			try (RandomAccessFile rtemp = new RandomAccessFile(tempFile, "rw")) {
				recFos.writeByte(rtemp.readByte());
				if (textMode) {
					int nbInput = Integer.parseInt(rtemp.readLine());
					recFos.writeBytes(Integer.toString(nbInput + 1));
					for (int i = 0; i < nbInput; i++)
						recFos.writeBytes(System.lineSeparator() + rtemp.readLine());
					recFos.writeBytes(System.lineSeparator() + streamRecorders[indexOfInput].getFile().getName() + ":" + recordId[indexOfInput] + System.lineSeparator());
				} else {
					int nbInput = rtemp.readInt();
					recFos.writeInt(nbInput + 1);
					for (int i = 0; i < nbInput; i++)
						putString(recFos, getString(rtemp));
					putString(recFos, streamRecorders[indexOfInput].getFile().getName());
				}
				long offset = rtemp.getFilePointer();
				rtemp.getChannel().transferTo(offset, rtemp.length() - offset, recFos.getChannel());
			}
			tempFile.delete();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void birth() {
		isWarning = false;
		if (!isRecording())
			return;
		recordIdCpt = 0;
		outputLinkToInputName = getOutputLinkToInputName();
		HashMap<String, ArrayList<Integer>> similar = new HashMap<>();
		for (int i = 0; i < outputLinkToInputName.length; i++) {
			String name = outputLinkToInputName[i];
			if (name != null) {
				ArrayList<Integer> groupName = similar.get(name);
				if (groupName == null) {
					groupName = new ArrayList<>();
					similar.put(name, groupName);
				}
				groupName.add(i);
			}
		}
		for (ArrayList<Integer> groupName : similar.values())
			if (groupName.size() != 1) {
				int i = 0;
				for (Integer id : groupName)
					outputLinkToInputName[id] += "-" + i++;
			}
		outputLinkToInputType = getOutputLinkToInputType();
		int nbRecorder = 0;
		for (Class<?> type : outputLinkToInputType)
			if (type != null)
				nbRecorder++;
		streamRecorders = new FileStreamRecorder[nbRecorder];
		recordId = new int[nbRecorder];
		recordPt = 0;
		String recordName = "Rec_" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(System.currentTimeMillis());
		recordedPath = getRecordDirectory() + File.separator + recordName;
		int nbStreamRecorder = 0;
		for (int i = 0; i < streamRecorders.length; i++) {
			FileStreamRecorder sr = FileStreamRecorder.getRecorder(outputLinkToInputType[i]);
			if (sr == null)
				continue;
			nbStreamRecorder++;
			sr.setRecordPath(recordedPath);
			sr.setRecordName(recordPt++ + recorIndexSeparator + outputLinkToInputName[i]);
			streamRecorders[i] = sr;
			recordId[i] = recordIdCpt++;
		}
		try {
			recFile = new File(recordedPath + File.separator + recordName + ".psf");
			recFile.getParentFile().mkdirs();
			recFos = new RandomAccessFile(recFile, "rw");
			// TODO Indice de version, version compacte en 1
			if (textMode) {
				recFos.writeByte(0);
				recFos.writeBytes(Integer.toString(nbStreamRecorder));
				for (int i = 0; i < streamRecorders.length; i++)
					if (streamRecorders[i] != null)
						recFos.writeBytes(System.lineSeparator() + streamRecorders[i].getFile().getName() + ":" + recordId[i]);
			} else {
				recFos.writeByte(1);
				recFos.writeInt(nbStreamRecorder);
				for (int i = 0; i < streamRecorders.length; i++)
					if (streamRecorders[i] != null)
						putString(recFos, streamRecorders[i].getFile().getName());
			}
		} catch (IOException e) {
			if (e instanceof FileNotFoundException)
				System.err.println("The file: " + new File(recordedPath).getParent() + " does not exist");
			else
				e.printStackTrace();
			setDefaulting(true);
			return;
		}
		addVarArgsInputChangeListener(this);
	}

	@Override
	public boolean canAddInput(Class<?>[] inputsType) {
		return true;
	}

	@Override
	public void death() {
		if (recFos != null) {
			try {
				recFos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			recFos = null;
		}
		if (streamRecorders != null) {
			for (FileStreamRecorder sr : streamRecorders)
				if (sr != null)
					try {
						sr.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
			streamRecorders = null;
		}
		removeVarArgsInputChangeListener(this);
	}

	public String getRecordedPath() {
		return recordedPath;
	}

	public boolean isTextMode() {
		return textMode;
	}

	@Override
	public boolean isValidInput(Class<?>[] inputsType, Class<?> additionalInput) {
		return FileStreamRecorder.hasStreamRecorder(additionalInput);
	}

	@ParamInfo(in = { "r" })
	public void process(final Object... objs) {
		if (recFos == null) {
			if (isRecording() && !isWarning) {
				setWarning("Unable to record probably due to some errors during birth");
				isWarning = true;
			}
			return;
		} else if (isWarning) {
			setWarning(null);
			isWarning = false;
		}
		for (int i = 0; i < objs.length; i++) {
			Object obj = objs[i];
			if (obj != null) {
				FileStreamRecorder sr = streamRecorders[i];
				if (sr == null)
					continue;
				try {
					if (textMode)
						recFos.writeBytes(System.lineSeparator() + getTimeOfIssue(i) + ":" + getTimeStamp(i) + ":" + recordId[i] + ":" + sr.getTimePointer());
					else {
						recFos.writeLong(getTimeOfIssue(i));
						recFos.writeLong(getTimeStamp(i));
						recFos.writeInt(recordId[i]);
						recFos.writeLong(sr.getTimePointer());
					}
					sr.pop(obj);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private static void putString(RandomAccessFile raf, String value) throws IOException {
		byte[] nameb = value.getBytes(StandardCharsets.UTF_8);
		raf.writeInt(nameb.length);
		raf.write(nameb);
	}

	public void setTextMode(boolean textMode) {
		this.textMode = textMode;
	}

	@Override
	public void varArgsInputChanged(int indexOfInput, int type) {
		outputLinkToInputName = getOutputLinkToInputName();
		outputLinkToInputType = getOutputLinkToInputType();
		if (type == VarArgsInputChangeListener.CHANGED) {
			try {
				streamRecorders[indexOfInput].close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			streamRecorders[indexOfInput] = FileStreamRecorder.getRecorder(outputLinkToInputType[indexOfInput]);
			streamRecorders[indexOfInput].setRecordPath(recordedPath);
			streamRecorders[indexOfInput].setRecordName(recordPt++ + recorIndexSeparator + outputLinkToInputName[indexOfInput]);
			recordId[indexOfInput] = recordIdCpt++;
			addToHeader(indexOfInput);
		} else if (type == VarArgsInputChangeListener.NEW) {
			FileStreamRecorder[] newStreamRecorders = new FileStreamRecorder[indexOfInput];
			System.arraycopy(streamRecorders, 0, newStreamRecorders, 0, streamRecorders.length);
			int indexOfNewInput = newStreamRecorders.length - 1;
			int[] newRecordId = new int[newStreamRecorders.length];
			System.arraycopy(recordId, 0, newRecordId, 0, recordId.length);
			newStreamRecorders[indexOfNewInput] = FileStreamRecorder.getRecorder(outputLinkToInputType[indexOfNewInput]);
			newStreamRecorders[indexOfNewInput].setRecordPath(recordedPath);
			newStreamRecorders[indexOfNewInput].setRecordName(recordPt++ + recorIndexSeparator + outputLinkToInputName[indexOfNewInput]);
			newRecordId[indexOfNewInput] = recordIdCpt++;
			streamRecorders = newStreamRecorders;
			recordId = newRecordId;
			addToHeader(indexOfNewInput);
		} else if (type == VarArgsInputChangeListener.REMOVE) {
			try {
				streamRecorders[indexOfInput].close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			FileStreamRecorder[] newStreamRecorders = new FileStreamRecorder[streamRecorders.length - 1];
			System.arraycopy(streamRecorders, 0, newStreamRecorders, 0, indexOfInput);
			int[] newRecordId = new int[newStreamRecorders.length];
			System.arraycopy(recordId, 0, newRecordId, 0, newRecordId.length);
			for (int i = indexOfInput + 1; i < streamRecorders.length; i++) {
				newStreamRecorders[i - 1] = streamRecorders[i];
				newRecordId[i - 1] = recordId[i];
			}
			streamRecorders = newStreamRecorders;
			recordId = newRecordId;
		}
	}

	private static String getString(RandomAccessFile raf) throws IOException {
		int nbBytes = raf.readInt();
		byte[] data = new byte[nbBytes];
		raf.readFully(data);
		return new String(data, StandardCharsets.UTF_8);
	}
}
