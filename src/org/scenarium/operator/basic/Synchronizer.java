/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.basic;

import java.util.Arrays;

import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;
import org.scenarium.filemanager.scenario.dataflowdiagram.operator.LinkChangeListener;

public class Synchronizer extends EvolvedOperator implements LinkChangeListener {
	private int nbData;
	private Object[] tempBuff;

	@Override
	public void birth() throws Exception {}

	@Override
	public void death() throws Exception {}

	@Override
	public void initStruct() {
		String[] oltin = getOutputLinkToInputName();
		Class<?>[] oltit = getOutputLinkToInputType();
		nbData = oltin.length - 1;
		String[] names = new String[nbData];
		Class<?>[] types = new Class<?>[nbData];
		for (int i = 0; i < types.length; i++) {
			names[i] = oltin[i] + "-" + i;
			types[i] = oltit[i];
		}
		updateOutputs(names, types);
	}

	@Override
	public void linkChanged(int indexOfInput) {
		initStruct();
	}

	public void process(Object... data) {
		if (tempBuff == null || tempBuff.length != nbData)
			tempBuff = new Object[nbData];
		boolean full = true;
		for (int i = 0; i < data.length; i++) {
			if (data[i] != null)
				tempBuff[i] = data[i];
			if (tempBuff[i] == null)
				full = false;
		}
		if (full) {
			triggerOutput(tempBuff);
			Arrays.fill(tempBuff, null);
		}
	}
}
