/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.basic;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.beanmanager.editors.PropertyEditor;
import org.beanmanager.editors.PropertyEditorManager;
import org.beanmanager.editors.primitive.number.NumberEditor;
import org.scenarium.filemanager.scenario.dataflowdiagram.ParamInfo;
import org.scenarium.filemanager.scenario.dataflowdiagram.VarArgsInputChangeListener;
import org.scenarium.filemanager.scenario.dataflowdiagram.operator.EvolvedVarArgsOperator;
import org.scenarium.operator.AbstractRecorder;

public class CSVRecorder extends AbstractRecorder implements VarArgsInputChangeListener, EvolvedVarArgsOperator {
	private String[] outputLinkToInputName;
	private Class<?>[] outputLinkToInputType;
	private RecordDescriptor[] records;
	private Runnable varArgsInputChangedTask;
	private int recordPt;

	@Override
	public void birth() {
		if (!isRecording())
			return;
		outputLinkToInputName = getOutputLinkToInputName();
		HashMap<String, ArrayList<Integer>> similar = new HashMap<>();
		for (int i = 0; i < outputLinkToInputName.length; i++) {
			String name = outputLinkToInputName[i];
			if (name != null) {
				ArrayList<Integer> groupName = similar.get(name);
				if (groupName == null) {
					groupName = new ArrayList<>();
					similar.put(name, groupName);
				}
				groupName.add(i);
			}
		}
		for (ArrayList<Integer> groupName : similar.values())
			if (groupName.size() != 1) {
				int i = 0;
				for (Integer id : groupName)
					outputLinkToInputName[id] += "_" + i++;
			}
		outputLinkToInputType = getOutputLinkToInputType();
		int nbRecorder = 0;
		for (Class<?> type : outputLinkToInputType)
			if (type != null)
				nbRecorder++;
		File rd = getRecordDirectory();
		if (rd == null || !rd.exists()) {
			setWarning(rd == null || rd.getName().isEmpty() ? "Empty record directory" : "The record directory: " + rd + " does not exists");
			return;
		}
		records = new RecordDescriptor[nbRecorder];
		recordPt = 0;
		for (int i = 0; i < nbRecorder; i++) {
			PropertyEditor<?> editor = PropertyEditorManager.findEditor(outputLinkToInputType[i], null);
			if (editor == null)
				continue;
			try {
				records[i] = new RecordDescriptor(editor, rd, recordPt++ + "_" + outputLinkToInputName[i]);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		addVarArgsInputChangeListener(this);
	}

	@Override
	public boolean canAddInput(Class<?>[] inputsType) {
		return true;
	}

	@Override
	public void death() {
		removeVarArgsInputChangeListener(this);
		if (records != null) {
			for (RecordDescriptor rd : records)
				try {
					rd.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			records = null;
		}
	}

	@Override
	public boolean isValidInput(Class<?>[] inputsType, Class<?> additionalInput) {
		return PropertyEditorManager.findEditor(additionalInput, "") != null;
	}

	@ParamInfo(in = { "r" })
	public void process(final Object... objs) {
		if (records == null)
			return;
		if (varArgsInputChangedTask != null) {
			varArgsInputChangedTask.run();
			varArgsInputChangedTask = null;
		}
		for (int i = 0; i < objs.length; i++) {
			Object obj = objs[i];
			if (obj != null)
				try {
					records[i].pop(getTimeOfIssue(i), getTimeStamp(i), obj);
				} catch (IOException | ArrayIndexOutOfBoundsException e) {
					System.err.println(records.length);
					e.printStackTrace();
				}
		}
	}

	@Override
	public void varArgsInputChanged(int indexOfInput, int type) {
		outputLinkToInputName = getOutputLinkToInputName();
		outputLinkToInputType = getOutputLinkToInputType();
		if (type == VarArgsInputChangeListener.CHANGED) {
			try {
				records[indexOfInput].close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			PropertyEditor<?> editor = PropertyEditorManager.findEditor(outputLinkToInputType[indexOfInput], null);
			try {
				records[indexOfInput] = editor == null ? null : new RecordDescriptor(editor, getRecordDirectory(), recordPt++ + outputLinkToInputName[indexOfInput]);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if (type == VarArgsInputChangeListener.NEW) {
			RecordDescriptor[] newRecords = new RecordDescriptor[indexOfInput];
			System.arraycopy(records, 0, newRecords, 0, records.length);
			int indexOfNewInput = newRecords.length - 1;
			PropertyEditor<?> editor = PropertyEditorManager.findEditor(outputLinkToInputType[indexOfNewInput], null);
			try {
				newRecords[indexOfNewInput] = editor == null ? null : new RecordDescriptor(editor, getRecordDirectory(), recordPt++ + "_" + outputLinkToInputName[indexOfNewInput]);
			} catch (IOException e) {
				e.printStackTrace();
			}
			records = newRecords;
		} else if (type == VarArgsInputChangeListener.REMOVE) {
			try {
				records[indexOfInput].close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			RecordDescriptor[] newRecords = new RecordDescriptor[records.length - 1];
			System.arraycopy(records, 0, newRecords, 0, newRecords.length);
			for (int i = indexOfInput + 1; i < records.length; i++)
				newRecords[i - 1] = records[i];
			records = newRecords;
		}
	}
}

class RecordDescriptor {
	private final boolean needQuotationMark;
	private final PropertyEditor<?> editor;
	private final FileWriter writter;

	public RecordDescriptor(PropertyEditor<?> editor, File recordDirectory, String name) throws IOException {
		needQuotationMark = !(editor instanceof NumberEditor);
		this.editor = editor;
		this.writter = new FileWriter(recordDirectory.toPath().resolve(name + ".csv").toFile()); //new RandomAccessFile(recordFile, "rw");// Files.newBufferedWriter(recordDirectory.toPath().resolve(name + ".csv"), CHARSET, StandardOpenOption.SYNC, StandardOpenOption.CREATE);
		writter.write("TimeOfIssue,TimeStamp," + name + "\n");
	}

	public void close() throws IOException {
		writter.close();
	}

	public void pop(long timeOfIssue, long timeStamp, Object obj) throws IOException {
		editor.setValueFromObj(obj);
		writter.write(timeOfIssue + "," + timeStamp + (needQuotationMark ? ",\"" + editor.getAsText() + "\"\n" : "," + editor.getAsText() + "\n"));
	}
}
