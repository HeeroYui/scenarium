/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.image;

import java.awt.image.BufferedImage;

import javax.vecmath.Point2i;

import org.beanmanager.editors.PropertyInfo;
import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;

public class Crop extends EvolvedOperator {
	@PropertyInfo(index = 0, nullable = false)
	private Point2i position = new Point2i(0, 0);
	@PropertyInfo(index = 1, nullable = false, info = "")
	private Point2i size = new Point2i(640, 480);
	private BufferedImage dst;

	@Override
	public void birth() {}

	@Override
	public void death() {
		dst = null;
	}

	public Point2i getPosition() {
		return position;
	}

	public Point2i getSize() {
		return size;
	}

	public BufferedImage process(BufferedImage img) {
		if (dst == null || img.getType() != dst.getType() || dst.getWidth() != size.x || dst.getHeight() != size.y)
			dst = new BufferedImage(size.x, size.y, img.getType());
		dst.getGraphics().drawImage(img, 0, 0, size.x, size.y, position.x, position.y, position.x + size.x, position.y + size.y, null);
		return dst;
	}

	public void setPosition(Point2i position) {
		this.position = position;
	}

	public void setSize(Point2i size) {
		this.size = size;
	}
}
