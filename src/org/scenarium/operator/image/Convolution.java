/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.image;

import java.awt.RenderingHints;
import java.awt.RenderingHints.Key;
import java.awt.image.BufferedImage;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;
import java.util.HashMap;

import javax.vecmath.Matrix3f;

import org.beanmanager.editors.PropertyInfo;
import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;

public class Convolution extends EvolvedOperator {
	@PropertyInfo(index = 0, info = "EDGE_NO_OP : Pixels at the edge of the source image are copied to the corresponding pixels in the destination without modification.\nEDGE_ZERO_FILL : Pixels at the edge of the destination image are set to zero.")
	private EdgeCondition edgeCondition = EdgeCondition.EDGE_NO_OP;
	@PropertyInfo(index = 1, nullable = false)
	private Matrix3f kernel = new Matrix3f(1f, 0f, -1f, 2f, 0f, -2f, 1f, 0f, -1f);
	private ConvolveOp convolveOp;
	private BufferedImage dst;

	@Override
	public void birth() {
		HashMap<Key, Object> init = new HashMap<>();
		// init.put(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		convolveOp = new ConvolveOp(new Kernel(3, 3, new float[] { kernel.m00, kernel.m01, kernel.m02, kernel.m10, kernel.m11, kernel.m12, kernel.m20, kernel.m21, kernel.m22 }),
				edgeCondition == EdgeCondition.EDGE_NO_OP ? ConvolveOp.EDGE_NO_OP : ConvolveOp.EDGE_ZERO_FILL, new RenderingHints(init));
	}

	@Override
	public void death() {
		convolveOp = null;
		dst = null;
	}

	public EdgeCondition getEdgeCondition() {
		return edgeCondition;
	}

	public Matrix3f getKernel() {
		return kernel;
	}

	public BufferedImage process(BufferedImage img) {
		if (dst == null || img.getType() != dst.getType() || img.getWidth() != dst.getWidth() || img.getHeight() != dst.getHeight())
			dst = new BufferedImage(img.getWidth(), img.getHeight(), img.getType());
		convolveOp.filter(img, dst);
		return dst;
	}

	public void setEdgeCondition(EdgeCondition edgeCondition) {
		this.edgeCondition = edgeCondition;
	}

	public void setKernel(Matrix3f kernel) {
		this.kernel = kernel;
		restart();
	}
}
