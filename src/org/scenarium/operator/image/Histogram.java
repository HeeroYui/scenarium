/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.image;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferUShort;
import java.util.Arrays;

import javax.vecmath.Point2i;

import org.beanmanager.BeanDesc;
import org.beanmanager.BeanManager;
import org.beanmanager.editors.container.BeanEditor;
import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;
import org.scenarium.operator.basic.Viewer;
import org.scenarium.operator.image.conversion.TypeConverter;
import org.scenarium.struct.curve.Curvei;
import org.scenarium.struct.curve.EvolvedCurveSeries;
import org.scenarium.timescheduler.VisuableSchedulable;

public class Histogram extends EvolvedOperator implements VisuableSchedulable {
	int[][][] datas = new int[1][2][0];
	private Viewer viewer;

	@Override
	public void birth() throws Exception {
		if (viewer == null) {
			String beanName = BeanEditor.getBeanDesc(this).name + "-Viewer";
			BeanDesc<?> beanDesc = BeanEditor.getRegisterBean(Viewer.class, beanName);
			if (beanDesc == null) {
				viewer = new Viewer();
				viewer.setPosition(new Point2i(0, 0));
				viewer.setDimension(new Point2i(640, 480));
				BeanEditor.registerBean(viewer, BeanEditor.getBeanDesc(this).name + "-Viewer", BeanManager.DEFAULTDIR);
			} else
				viewer = (Viewer) beanDesc.bean;
		}
		viewer.birth();
		// ((ChartDrawer) viewer.getTheaterPane()).setColors(new Color[] { Color.RED, Color.LIME, Color.BLUE });//que si tp sinon null...
	}

	@Override
	public void death() throws Exception {
		datas = new int[1][2][0];
		if (viewer != null)
			viewer.death();
	}

	public Viewer getViewer() {
		return viewer;
	}

	@Override
	public boolean needToBeSaved() {
		return viewer == null ? true : viewer.needToBeSaved();
	}

	@Override
	public void paint() {
		viewer.paint();
	}

	public void process(BufferedImage img) {
		int size = (img.getType() == BufferedImage.TYPE_USHORT_GRAY ? Short.MAX_VALUE : Byte.MAX_VALUE) * 2 + 2;
		int nbChannel = img.getType() == BufferedImage.TYPE_USHORT_GRAY || img.getType() == BufferedImage.TYPE_BYTE_GRAY ? 1 : 3;

		if (datas.length != nbChannel)
			datas = new int[nbChannel][2][0];
		if (datas[0].length != size)
			for (int i = 0; i < nbChannel; i++) {
				int[][] channel = datas[i];
				if (channel[0].length != size) {
					channel[0] = new int[size];
					channel[1] = new int[size];
					int[] x = datas[i][0];
					for (int j = 0; j < x.length; j++)
						x[j] = j;
				}
			}
		for (int[][] channel : datas)
			Arrays.fill(channel[1], 0);

		if (img.getType() == BufferedImage.TYPE_USHORT_GRAY || img.getType() == BufferedImage.TYPE_BYTE_GRAY) {
			int[] y = datas[0][1];
			if (img.getType() == BufferedImage.TYPE_USHORT_GRAY)
				for (short pixel : ((DataBufferUShort) img.getRaster().getDataBuffer()).getData())
					y[Short.toUnsignedInt(pixel)]++;
			else
				for (byte pixel : ((DataBufferByte) img.getRaster().getDataBuffer()).getData())
					y[pixel & 0xFF]++;
		} else {
			if (img.getType() != BufferedImage.TYPE_3BYTE_BGR)
				img = new TypeConverter(ImageType.TYPE_3BYTE_BGR).process(img);

			byte[] pixels = ((DataBufferByte) img.getRaster().getDataBuffer()).getData();
			int[][] b = datas[0];
			int[][] g = datas[1];
			int[][] r = datas[2];
			for (int i = 0; i < pixels.length;) {
				b[1][pixels[i++] & 0xFF]++;
				g[1][pixels[i++] & 0xFF]++;
				r[1][pixels[i++] & 0xFF]++;
			}
		}
		try {
			EvolvedCurveSeries ecs = nbChannel == 1 ? new EvolvedCurveSeries(new Curvei(datas[0], "Gray Levels"))
					: new EvolvedCurveSeries("Histogram", "Height", "Frequency", new Curvei(datas[2], "Red Levels"), new Curvei(datas[1], "Green Levels"), new Curvei(datas[0], "Blue Levels"));
			viewer.process(ecs);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void setAnimated(boolean animated) {
		viewer.setAnimated(animated);
	}

	public void setViewer(Viewer viewer) {
		if (this.viewer != null)
			BeanEditor.unregisterBean(this.viewer, true);
		this.viewer = viewer;
		if (viewer != null)
			BeanEditor.registerBean(viewer, BeanEditor.getBeanDesc(this).name + "-Viewer", BeanManager.DEFAULTDIR);
	}
}
