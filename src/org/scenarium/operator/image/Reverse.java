/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.image;

import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;

import org.beanmanager.editors.PropertyInfo;
import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;
import org.scenarium.filemanager.scenario.dataflowdiagram.ParamInfo;

public class Reverse extends EvolvedOperator {
	@PropertyInfo(index = 0)
	private boolean vertical = true;
	private boolean horizontal = true;
	private BufferedImage outRaster;

	public Reverse() {}

	public Reverse(boolean vertical, boolean horizontal) {
		this.vertical = vertical;
		this.horizontal = horizontal;
	}

	@Override
	public void birth() {}

	@Override
	public void death() {
		outRaster = null;
	}

	public boolean isHorizontal() {
		return horizontal;
	}

	public boolean isVertical() {
		return vertical;
	}

	@ParamInfo(in = { "In" }, out = "Out")
	public BufferedImage process(BufferedImage image) {
		int width = image.getWidth();
		int height = image.getHeight();
		AffineTransform tx = AffineTransform.getScaleInstance(vertical ? -1 : 1, horizontal ? -1 : 1);
		tx.translate(vertical ? -image.getWidth() : 0, horizontal ? -image.getHeight() : 0);
		AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
		if (outRaster == null || outRaster.getType() != image.getType() || outRaster.getWidth() != width || outRaster.getHeight() != height)
			outRaster = new BufferedImage(width, height, image.getType());
		op.filter(image, outRaster);
		return outRaster;
	}

	public void setHorizontal(boolean horizontal) {
		this.horizontal = horizontal;
	}

	public void setVertical(boolean vertical) {
		this.vertical = vertical;
	}
}
