/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.image;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import javax.imageio.ImageIO;

import org.beanmanager.editors.PropertyInfo;
import org.beanmanager.editors.basic.PathInfo;
import org.beanmanager.editors.primitive.number.NumberInfo;
import org.scenarium.editors.NotChangeableAtRuntime;
import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;

public class ImageToVideo extends EvolvedOperator {
	@PropertyInfo(index = 0, info = "Image folder. Images must have an index in their names and no other numeric characters")
	@PathInfo(directory = true)
	private File imagePath;
	@PropertyInfo(index = 3, unit = "ms")
	@NumberInfo(min = 0)
	@NotChangeableAtRuntime
	private int stepTime = 50;
	private ArrayList<Path> imgs;
	private int index;
	private Timer timer;

	@Override
	public void birth() throws Exception {
		imgs = getImages(imagePath.getAbsolutePath());
		index = 0;
		onStart(() -> {
			timer = new Timer();
			timer.scheduleAtFixedRate(new TimerTask() {

				@Override
				public void run() {
					try {
						triggerOutput(ImageIO.read(imgs.get(index++).toFile()));
//						index = 1700;
						if(index == imgs.size())
							index = 0;
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}, stepTime, stepTime);
		});
	}

	public BufferedImage process() {
		return null;
	}

	private static ArrayList<Path> getImages(String path) throws IOException {
		Path _path = Paths.get(path);
		ArrayList<Path> list = new ArrayList<>();
		Files.newDirectoryStream(_path).forEach(p -> {
			try {
				getIndex(p);
				list.add(p);
			} catch (NumberFormatException e) {

			}
		});
		list.sort((a, b) -> Integer.compare(getIndex(a), getIndex(b)));
		return list;
	}

	private static int getIndex(Path a) {
		String fileName = a.getFileName().toString();
		fileName = fileName.replaceAll("[^\\d]", "");
		return Integer.parseInt(fileName);
	}

	@Override
	public void death() throws Exception {
		if(timer != null) {
			timer.cancel();
			timer.purge();
			timer = null;
		}
		imgs = null;
		index = 0;
	}

	public File getImagePath() {
		return imagePath;
	}

	public void setImagePath(File imagePath) {
		this.imagePath = imagePath;
	}

	public int getStepTime() {
		return stepTime;
	}

	public void setStepTime(int stepTime) {
		this.stepTime = stepTime;
	}
}
