/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.image;

import java.awt.image.BufferedImage;
import java.awt.image.ConvolveOp;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferUShort;
import java.awt.image.Kernel;

import org.beanmanager.editors.primitive.number.NumberInfo;
import org.scenarium.filemanager.scenario.dataflowdiagram.ParamInfo;

public class Gain {
	public static void main(String[] args) {
		BufferedImage img = new BufferedImage(640, 480, BufferedImage.TYPE_USHORT_GRAY);
		DataBuffer db = img.getData().getDataBuffer();
		System.out.println(db);
	}

	@NumberInfo(min = 0)
	private double gain;

	private BufferedImage outRaster;

	public void birth() {}

	public void death() {
		outRaster = null;
	}

	public double getGain() {
		return gain;
	}

	@ParamInfo(in = { "In" }, out = "Out")
	public BufferedImage process(BufferedImage img) {
		if (outRaster == null || outRaster.getType() != img.getType() || outRaster.getWidth() != img.getWidth() || outRaster.getHeight() != img.getHeight())
			outRaster = new BufferedImage(img.getWidth(), img.getHeight(), img.getType());
		DataBuffer db = img.getRaster().getDataBuffer();
		if (db instanceof DataBufferByte) {
			byte[] dataIn = ((DataBufferByte) db).getData();
			byte[] dataOut = ((DataBufferByte) outRaster.getRaster().getDataBuffer()).getData();
			for (int i = 0; i < dataIn.length; i++) {
				int val = (int) ((dataIn[i] & 0xFF) * gain);
				if (val > Byte.MAX_VALUE * 2 + 1)
					val = Byte.MAX_VALUE * 2 + 1;
				dataOut[i] = (byte) val;
			}
		} else if (db instanceof DataBufferUShort) {
			short[] dataIn = ((DataBufferUShort) db).getData();
			short[] dataOut = ((DataBufferUShort) outRaster.getRaster().getDataBuffer()).getData();
			for (int i = 0; i < dataIn.length; i++) {
				int val = (int) ((dataIn[i] & 0xFFFF) * gain);
				if (val > Short.MAX_VALUE * 2 + 1)
					val = Short.MAX_VALUE * 2 + 1;
				dataOut[i] = (short) val;
			}
		} else {
			Kernel kernel = new Kernel(1, 1, new float[] { (float) gain });
			ConvolveOp cop = new ConvolveOp(kernel);
			cop.filter(img, outRaster);
		}
		return outRaster;
	}

	public void setGain(double gain) {
		this.gain = gain;
	}
}
