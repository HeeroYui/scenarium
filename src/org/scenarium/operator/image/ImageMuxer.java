/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.image;

import java.awt.image.BufferedImage;

import javax.vecmath.Point2i;

import org.beanmanager.editors.PropertyInfo;
import org.beanmanager.editors.primitive.number.ControlType;
import org.beanmanager.editors.primitive.number.NumberInfo;
import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;

public class ImageMuxer extends EvolvedOperator {
	@PropertyInfo(index = 0, info = "Width of the output image")
	@NumberInfo(min = 0, controlType = ControlType.SPINNER)
	private int width;
	@PropertyInfo(index = 1, info = "Height of the output image")
	@NumberInfo(min = 0, controlType = ControlType.SPINNER)
	private int height;
	@PropertyInfo(index = 2, info = "Array of coordinates of each input, must be equal to the number of input")
	private Point2i coord[];
	private BufferedImage[] imgBuffer;

	@Override
	public void birth() throws Exception {}

	public BufferedImage process(BufferedImage... imgs) {
		if (imgBuffer == null || imgBuffer.length != imgs.length) 
			imgBuffer = new BufferedImage[imgs.length];
		if(coord == null || coord.length != imgs.length)
			coord = new Point2i[imgs.length];
		for (int i = 0; i < imgs.length; i++)
			if (imgs[i] != null)
				imgBuffer[i] = imgs[i];
		BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_3BYTE_BGR);
		for (int i = 0; i < imgBuffer.length; i++) {
			if(coord[i] == null)
				coord[i] = new Point2i();
			if (imgBuffer[i] != null)
				img.getGraphics().drawImage(imgBuffer[i], coord[i].x, coord[i].y, null);
		}
		if(imgs[0] != null)
			return img;
		return null;
	}

	@Override
	public void death() throws Exception {
		imgBuffer = null;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public Point2i[] getCoord() {
		return coord;
	}

	public void setCoord(Point2i[] coord) {
		this.coord = coord;
	}
}
