/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.image.conversion;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.plugins.jpeg.JPEGImageWriteParam;
import javax.imageio.stream.ImageOutputStream;

import org.beanmanager.editors.DynamicEnableBean;
import org.beanmanager.editors.DynamicPossibilities;
import org.beanmanager.editors.PropertyInfo;
import org.beanmanager.editors.primitive.number.ControlType;
import org.beanmanager.editors.primitive.number.NumberInfo;
import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;
import org.scenarium.filemanager.scenario.dataflowdiagram.ParamInfo;

public class ImgEncoder extends EvolvedOperator implements DynamicEnableBean {
	@PropertyInfo(index = 0, info = "Image writter format name")
	@DynamicPossibilities(possibleChoicesMethod = "getWriterFormatNames")
	private String imgFormatName;
	@PropertyInfo(index = 1, info = "Compression quality for jpeg images")
	@NumberInfo(min = 0, max = 1, controlType = ControlType.TEXTFIELDANDSLIDER)
	private float quality = 0.5f;

	private ByteArrayOutputStream stream;
	private ImageOutputStream ios;
	private ImageWriter writer;
	private JPEGImageWriteParam iwparam;
	private IIOImage iioi;
	private boolean illegalArgument;

	public ImgEncoder() {}

	public ImgEncoder(String imgFormatName, float quality) {
		this.imgFormatName = imgFormatName;
		this.quality = quality;
	}

	@Override
	public void birth() {
		illegalArgument = false;
		if (imgFormatName == null || imgFormatName.isEmpty()) {
			System.err.println(getBlockName() + ": No image writter format name");
			return;
		}
		Iterator<ImageWriter> iter = ImageIO.getImageWritersByFormatName(imgFormatName);
		if (iter.hasNext())
			writer = iter.next();
		if (isJpgFormat()) {
			iwparam = new JPEGImageWriteParam(Locale.getDefault());
			iwparam.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
			iwparam.setCompressionQuality(quality);
		}
		stream = new ByteArrayOutputStream();
		iioi = new IIOImage(new BufferedImage(1, 1, BufferedImage.TYPE_3BYTE_BGR), null, null);
		try {
			ios = ImageIO.createImageOutputStream(stream);
			writer.setOutput(ios);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void death() {
		if (writer != null) {
			writer.dispose();
			writer = null;
		}
		try {
			if (ios != null) {
				ios.close();
				ios = null;
			}
			if (stream != null) {
				stream.close();
				stream = null;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		iwparam = null;
		iioi = null;
	}

	public String getImgFormatName() {
		return imgFormatName;
	}

	public float getQuality() {
		return quality;
	}

	public String[] getWriterFormatNames() {
		ArrayList<String> writterformats = new ArrayList<>();
		HashSet<Class<?>> set = new HashSet<>();
		for (String writterFormatName : ImageIO.getWriterFormatNames()) {
			Iterator<ImageWriter> it = ImageIO.getImageWritersByFormatName(writterFormatName);
			while (it.hasNext()) {
				Class<?> writterClass = it.next().getClass();
				if (!set.contains(writterClass)) {
					set.add(writterClass);
					writterformats.add(writterFormatName.toUpperCase());
				}
			}
		}
		return writterformats.toArray(new String[writterformats.size()]);
	}

	private boolean isJpgFormat() {
		return imgFormatName != null && (imgFormatName.equals("JPG") || imgFormatName.equals("JPEG"));
	}

	@ParamInfo(in = { "img" }, out = "encodedData")
	public byte[] process(BufferedImage img) {
		if (ios != null)
			try {
				stream.reset();
				iioi.setRenderedImage(img);
				writer.write(null, iioi, iwparam);
				ios.flush();
				return stream.toByteArray();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				if (illegalArgument == false) {
					illegalArgument = true;
					System.err.println(getBlockName() + ": " + e.getMessage());
				}
			}
		return null;
	}

	@Override
	public void setEnable() {
		fireSetPropertyEnable(this, "quality", isJpgFormat());
	}

	public void setImgFormatName(String imgFormatName) {
		this.imgFormatName = imgFormatName;
		setEnable();
		restartLater();
	}

	public void setQuality(float quality) {
		this.quality = quality;
		JPEGImageWriteParam _iwparam = iwparam;
		if (_iwparam != null)
			_iwparam.setCompressionQuality(quality);
	}

}
