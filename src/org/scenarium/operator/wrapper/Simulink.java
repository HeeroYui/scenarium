/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.wrapper;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.Timer;
import java.util.TimerTask;

import javax.tools.Diagnostic;
import javax.tools.JavaFileObject;

import org.beanmanager.BeanManager;
import org.beanmanager.editors.DynamicEnableBean;
import org.beanmanager.editors.PropertyInfo;
import org.beanmanager.editors.container.BeanEditor;
import org.beanmanager.editors.container.BeanInfo;
import org.beanmanager.editors.container.DynamicBeanInfo;
import org.beanmanager.editors.primitive.number.NumberInfo;
import org.scenarium.filemanager.scenario.dataflowdiagram.BlockInfo;
import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;
import org.scenarium.tools.dynamiccompilation.CompilationException;
import org.scenarium.tools.dynamiccompilation.InlineCompiler;

import com.sun.jna.Function;
import com.sun.jna.NativeLibrary;
import com.sun.jna.Pointer;
import com.sun.jna.Structure;

class CStructDescriptor {
	public final String structName;
	public final String variablename;
	public final ArrayList<PrimitiveVariable> params;

	public CStructDescriptor(String structName, ArrayList<PrimitiveVariable> params) {
		this.structName = structName;
		this.variablename = null;
		this.params = params;
	}

	public CStructDescriptor(String structName, String variableName, ArrayList<PrimitiveVariable> params) {
		this.structName = structName;
		this.variablename = variableName;
		this.params = params;
	}

	@Override
	public String toString() {
		return structName + " " + variablename + " " + params;
	}
}

class IOPDescriptor {
	public String initMethodName;
	public final String initializeMethodName;
	public final String stepMethodName;
	public final String terminateMethodName;
	public String destroyMethodName;
	public final CStructDescriptor inputs;
	public final CStructDescriptor outputs;
	public final CStructDescriptor parameters;

	public IOPDescriptor(String initializeMethodName, String stepMethodName, String terminateMethodName, CStructDescriptor inputs, CStructDescriptor outputs, CStructDescriptor parameters) {
		if (initializeMethodName == null)
			throw new IllegalArgumentException("No initialize method");
		if (stepMethodName == null)
			throw new IllegalArgumentException("No step method");
		if (terminateMethodName == null)
			throw new IllegalArgumentException("No terminate method");
		if (inputs == null)
			throw new IllegalArgumentException("No input struct");
		if (outputs == null)
			throw new IllegalArgumentException("No output struct");
		// if (parameters == null)
		// throw new IllegalArgumentException("No parameters struct");
		this.initMethodName = null;
		this.initializeMethodName = initializeMethodName;
		this.stepMethodName = stepMethodName;
		this.terminateMethodName = terminateMethodName;
		this.destroyMethodName = null;
		this.inputs = inputs;
		this.outputs = outputs;
		this.parameters = parameters;
	}

	public IOPDescriptor(String initMethodName, String birthMethodName, String processMethodName, String deathMethodName, String destroyMethodName, CStructDescriptor inputs, CStructDescriptor outputs,
			CStructDescriptor parameters) {
		this(birthMethodName, processMethodName, deathMethodName, inputs, outputs, parameters);
		this.initMethodName = initMethodName;
		this.destroyMethodName = destroyMethodName;
	}

	@Override
	public String toString() {
		return "inputs: " + inputs + "outputs: " + outputs + "parameters: " + parameters;
	}
}

class PrimitiveVariable {
	public static final int NotAnArray = -1;
	public static final int DynamicArraySize = -2;
	public static final int DynamicArray = -3;
	public final String name;
	public Class<?> type;
	public int size;

	public PrimitiveVariable(String name, Class<?> type, int size) {
		this.name = name;
		this.type = type;
		this.size = size;
	}

	public void setAsArraySize() {
		size = DynamicArraySize;
	}

	public void setAsDynamicArray() {
		size = DynamicArray;
	}

	@Override
	public String toString() {
		return name + (size == -1 ? "" : "[" + size + "]") + " " + type;
	}
}

@BlockInfo(info = "This block can execute simulink diagram compiled with Matlab Embedded coder")
public class Simulink extends EvolvedOperator implements DynamicEnableBean {
	private static final String GROUPDELIMITER = "_";

	private static Class<?> getType(String matlabType) {
		switch (matlabType) {
		case "int8_T":
			return byte.class;
		case "uint8_T":
			return byte.class;
		case "int16_T":
			return short.class;
		case "uint16_T":
			return short.class;
		case "int32_T":
			return int.class;
		case "uint32_T":
			return int.class;
		case "int64_T":
			return long.class;
		case "uint64_T":
			return long.class;
		case "real32_T":
			return float.class;
		case "real64_T":
			return double.class;
		case "real_T":
			return double.class;// isArray ? double[].class : double.class;
		case "time_T":
			return double.class;
		case "boolean_T":
			return byte.class;
		case "int_T":
			return int.class;
		case "uint_T":
			return int.class;
		case "ulong_T":
			return long.class;
		case "char_T":
			return byte.class;
		case "uchar_T":
			return byte.class;
		case "byte_T":
			return byte.class;
		default:
			return null;
		}
	}

	public static void main(String[] args) {
		Simulink simulink = new Simulink();
		simulink.header = new File("/home/revilloud/Dropbox/TestSimulink/test_ert_shrlib_rtw/test.h");
		simulink.library = new File("/home/revilloud/Dropbox/TestSimulink/test.so");
		try {
			simulink.initStruct();
			simulink.birth();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static Variable processExtern(String extern) {
		int indexOfEquals = extern.indexOf("=");
		if (indexOfEquals != -1)
			extern = extern.substring(0, indexOfEquals - 1).trim();
		int indexOfParamName = extern.lastIndexOf(" ");
		String name = extern.substring(indexOfParamName + 1);
		String type = extern.substring(0, indexOfParamName);
		int indexOfParamType = type.lastIndexOf(" ");
		type = type.substring(indexOfParamType + 1);
		int indexOfArray = name.indexOf("[");
		int size;
		if (indexOfArray != -1) {
			String sSize = name.substring(indexOfArray + 1);
			size = Integer.parseInt(sSize.substring(0, sSize.indexOf("]")));
			name = name.substring(0, indexOfArray);
		} else
			size = -1;
		return new Variable(name, type, size);
	}

	private static CStructDescriptor processTypedefStruct(StringBuilder struct) {
		int indexOfBeginHeader = struct.indexOf("{");
		int indexOfEndHeader = struct.indexOf("}");
		String header = struct.substring(0, indexOfBeginHeader).trim();
		String parameters = struct.substring(indexOfBeginHeader + 1, indexOfEndHeader).trim();
		String foot = struct.substring(indexOfEndHeader + 1).trim();
		String structName = header.startsWith("typedef") ? foot : header.substring(header.lastIndexOf(" ")).trim();
		StringTokenizer st = new StringTokenizer(parameters, ";");
		ArrayList<PrimitiveVariable> params = new ArrayList<>();
		while (st.hasMoreTokens()) {
			String param = st.nextToken().trim();
			int indexOfEquals = param.indexOf("=");
			if (indexOfEquals != -1)
				param = param.substring(0, indexOfEquals - 1).trim();
			int indexOfParamName = param.lastIndexOf(" ");
			String name = param.substring(indexOfParamName + 1);
			param = param.substring(0, indexOfParamName);
			int indexOfParamType = param.lastIndexOf(" ");
			int indexOfArray = name.indexOf("[");
			int size;
			if (indexOfArray != -1) {
				String sSize = name.substring(indexOfArray + 1);
				size = Integer.parseInt(sSize.substring(0, sSize.indexOf("]")));
				name = name.substring(0, indexOfArray);
			} else
				size = PrimitiveVariable.NotAnArray;
			Class<?> type = getType(indexOfParamType == -1 ? param : param.substring(0, indexOfParamType + 1));
			if (type != null)
				params.add(new PrimitiveVariable(name, type, size));
		}
		return new CStructDescriptor(structName, params);
	}

	@PropertyInfo(index = 0, info = "header (*.h) of the generated diaram")
	private File header;
	@PropertyInfo(index = 1, info = "library (*.so, *.dll) of the generated diaram")
	private File library;
	@PropertyInfo(index = 2, info = "FIXED: The block is schedule at fixed time rate defined by the parameter fixedStepSize.\nVARIABLEWITHALLENTRIES: The block wait all inputs.\nVARIABLEWITHONEENTRIES: The block wait for at least one input.\n")
	private StepMode stepMode = StepMode.VARIABLEWITHONEENTRIES;
	@PropertyInfo(index = 3, unit = "ms")
	@NumberInfo(min = 0)
	private int fixedStepSize = 50;
	@PropertyInfo(index = 4, info = "Prefix for IHM parameters")
	private String parameterPrefix = "Gain5";
	@PropertyInfo(index = 5, nullable = false, info = "Parameters of the diaram")
	@BeanInfo(alwaysExtend = true)
	@DynamicBeanInfo(possibleSubclassesMethodName = "getStructType")
	private JNAStructure parameters;
	private InlineCompiler ic;
	private IOPDescriptor iop;
	private Structure in;
	private Structure out;
	private NativeLibrary lib;

	private Function test_step;

	private Object[] oldInputs;

	private long maxTs = -1;

	private Timer fixedModeTimer;

	@Override
	public void birth() throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, IOException {
		load();
		lib.getFunction(iop.initializeMethodName).invokeVoid(null);
		test_step = lib.getFunction(iop.stepMethodName);
		oldInputs = new Object[iop.outputs.params.size()];
		if (stepMode == StepMode.FIXED) {
			fixedModeTimer = new Timer();
			fixedModeTimer.scheduleAtFixedRate(new TimerTask() {

				@Override
				public void run() {
					if (stepMode != StepMode.FIXED) {
						cancel();
						return;
					}
					nextStep();
				}
			}, 0, fixedStepSize);
		}
	}

	@SuppressWarnings("unchecked")
	private ArrayList<Class<? extends Structure>> createAndLoadJNAJavaClass(IOPDescriptor iOPDescriptor) {
		String[] inCode = generateIOCode(iOPDescriptor.inputs);
		String[] outCode = generateIOCode(iOPDescriptor.outputs);
		String[] paramCode = iOPDescriptor.parameters != null ? generateParameterCode(iOPDescriptor.parameters) : null;

		ArrayList<Class<? extends Structure>> c = new ArrayList<>();
		try {
			ic = paramCode == null ? new InlineCompiler(new String[] { inCode[0], outCode[0] }, new String[] { inCode[1], outCode[1] })
					: new InlineCompiler(new String[] { inCode[0], outCode[0], paramCode[0] }, new String[] { inCode[1], outCode[1], paramCode[1] });
			for (Class<?> class1 : ic.getCompileClass())
				c.add((Class<? extends Structure>) class1);
		} catch (CompilationException e) {
			for (Diagnostic<? extends JavaFileObject> error : e.getErrors())
				System.err.println("Line: " + error.getLineNumber() + ": " + error.getMessage(Locale.getDefault()));
		}
		return c;
	}

	@Override
	public void death() throws Exception {
		lib.getFunction(iop.terminateMethodName).invokeVoid(null);
		oldInputs = null;
		maxTs = -1;
		stopTimer();
	}

	private void dispose() {
		if (lib != null) {
			lib.dispose();
			lib = null;
		}
		if (ic != null) {
			ic.close();
			ic = null;
		}
		iop = null;
		parameters = null;
		in = null;
		out = null;
	}

	private static String[] generateIOCode(CStructDescriptor struct) {
		ArrayList<PrimitiveVariable> params = struct.params;
		StringBuilder sb = new StringBuilder();
		String pName = "simulink";
		String className = pName + "." + struct.structName;
		sb.append("package " + pName + ";\n");
		sb.append("import java.util.Arrays;\n");
		sb.append("import java.util.List;\n");
		sb.append("import com.sun.jna.Pointer;\n");
		sb.append("import com.sun.jna.Structure;\n");

		sb.append("public class " + struct.structName + " extends Structure{\n");
		for (PrimitiveVariable var : params) {
			sb.append("\tpublic " + var.type);
			if (var.size == -1)
				sb.append(" " + var.name + ";\n");
			else
				sb.append("[] " + var.name + " = new " + var.type + "[" + var.size + "];\n");
		}
		sb.append("\tpublic " + struct.structName + "() {\n");
		sb.append("\t\tsuper();\n");
		sb.append("\t}\n");

		sb.append("\tpublic void setPointer(Pointer p) {\n");
		sb.append("\t\tuseMemory(p);\n");
		sb.append("\t}\n");

		HashSet<String> visited = new HashSet<>();
		for (PrimitiveVariable var : params) {
			String upName = var.name.substring(0, 1).toUpperCase() + var.name.substring(1);
			if (!visited.add(upName))
				continue;
			String type = var.type + (var.size == -1 ? "" : "[]");
			sb.append("\tpublic " + type + " get" + upName + "() {\n");
			sb.append("\t\treturn " + var.name + ";\n");
			sb.append("\t}\n");

			sb.append("\tpublic void set" + upName + "(" + type + " " + var.name + ") {\n");
			sb.append("\t\tthis." + var.name + " = " + var.name + ";\n");
			sb.append("\t\twriteField(\"" + var.name + "\");\n");
			sb.append("\t}\n");
		}

		sb.append("\tprotected List<String> getFieldOrder() {\n");
		sb.append("\t\treturn Arrays.asList(");
		if (!params.isEmpty()) {
			for (int i = 0; i < params.size() - 1; i++)
				sb.append("\"" + params.get(i).name + "\", ");
			sb.append("\"" + params.get(params.size() - 1).name + "\"");
		}
		sb.append(");\n");
		sb.append("\t}\n");

		sb.append("}\n");
		return new String[] { className, sb.toString() };
	}

	@SuppressWarnings("unchecked")
	private String[] generateParameterCode(CStructDescriptor struct) {
		ArrayList<PrimitiveVariable> params = struct.params;
		HashMap<String, Object> paramMap = new HashMap<>();
		for (PrimitiveVariable param : params) {
			String name = param.name;
			String[] arbo = name.split(GROUPDELIMITER);
			for (int i = 0; i < arbo.length - 1; i++)
				arbo[i] += GROUPDELIMITER;
			if (name.endsWith(GROUPDELIMITER))
				arbo[arbo.length - 1] += GROUPDELIMITER;
			HashMap<String, Object> node = paramMap;
			Object nodeLvl = null;
			for (int i = 0; i < arbo.length - 1; i++) {
				String lvl = arbo[i];
				nodeLvl = node.get(lvl);
				if (nodeLvl == null) {
					nodeLvl = new HashMap<>();
					node.put(lvl, nodeLvl);
				} else if (nodeLvl instanceof String) {
					System.err.println("conflit: " + nodeLvl + " is a group and a value...");
					nodeLvl = new HashMap<>();
					node.put(lvl, nodeLvl);
				}
				node = (HashMap<String, Object>) nodeLvl;
			}
			node.put(arbo[arbo.length - 1], param);
		}
		LinkedList<HashMap<String, Object>> todoNodes = new LinkedList<>();
		LinkedList<String> todoNames = new LinkedList<>();
		LinkedList<HashMap<String, Object>> todoFatherNode = new LinkedList<>();
		LinkedList<String> todoFatherName = new LinkedList<>();
		for (String objName : paramMap.keySet()) {
			Object obj = paramMap.get(objName);
			if (obj instanceof HashMap) {
				todoFatherNode.add(paramMap);
				todoFatherName.add("");
				todoNodes.add((HashMap<String, Object>) obj);
				todoNames.add(objName);
			}
		}
		while (!todoNodes.isEmpty()) {
			HashMap<String, Object> grandFatherNode = todoFatherNode.pop();
			String grandFatherName = todoFatherName.pop();
			HashMap<String, Object> fatherNode = todoNodes.pop();
			String fatherName = todoNames.pop();
			Set<String> sonNodesName = fatherNode.keySet();
			if (sonNodesName.size() == 1 || grandFatherName.equals(fatherName)) {
				String sonName = fatherNode.keySet().iterator().next();
				Object son = fatherNode.get(sonName);
				String newNodeName = fatherName + sonName;
				grandFatherNode.remove(fatherName);
				grandFatherNode.put(newNodeName, son);
				if (son instanceof HashMap) {
					todoFatherNode.push(grandFatherNode);
					todoFatherName.push(fatherName);
					todoNodes.push((HashMap<String, Object>) son);
					todoNames.push(newNodeName);
				}
			} else
				for (String sonName : sonNodesName) {
					Object sonNode = fatherNode.get(sonName);
					if (sonNode instanceof HashMap) {
						todoFatherNode.push(fatherNode);
						todoFatherName.push(fatherName);
						todoNodes.push((HashMap<String, Object>) sonNode);
						todoNames.push(sonName);
					}
				}
		}
		// displayMap(paramMap, 0);
		StringBuilder sb = new StringBuilder();
		String pName = "simulink";
		String className = pName + "." + struct.structName;
		sb.append("package " + pName + ";\n");
		sb.append("import java.util.Arrays;\n");
		sb.append("import java.util.List;\n");
		sb.append("import com.sun.jna.Pointer;\n");
		sb.append("import operator.internoperator.wrapper.JNAStructure;\n");
		sb.append("import beanUtils.PropertyInfo;\n");
		sb.append("public class " + struct.structName + " extends JNAStructure {\n");
		for (PrimitiveVariable var : params) { // déclaration des variable, on change rien
			sb.append("\tpublic " + var.type);
			if (var.size == -1)
				sb.append(" " + var.name + ";\n");
			else
				sb.append("[] " + var.name + " = new " + var.type + "[" + var.size + "];\n");
		}
		sb.append("\tpublic " + struct.structName + "() {\n");
		sb.append("\t\tsuper();\n");
		sb.append("\t}\n");
		sb.append("\tpublic void setPointer(Pointer p) {\n");
		sb.append("\t\tuseMemory(p);\n");
		sb.append("\t}\n");
		HashSet<String> visited = new HashSet<>();
		paramMap.forEach((name, element) -> {
			if (element instanceof HashMap) {
				if (parameterPrefix == null || name.startsWith(parameterPrefix))
					writeClass(sb, struct.structName, (HashMap<String, Object>) element, name, 1);
			} else {
				PrimitiveVariable var = (PrimitiveVariable) element;
				String upName = var.name.substring(0, 1).toUpperCase() + var.name.substring(1);
				if (!visited.contains(upName) && (parameterPrefix == null || var.name.startsWith(parameterPrefix))) {
					visited.add(upName);
					String type = var.type + (var.size == -1 ? "" : "[]");
					sb.append("\tpublic " + type + " get" + upName + "() {\n");
					sb.append("\t\treturn " + var.name + ";\n");
					sb.append("\t}\n");
					sb.append("\tpublic void set" + upName + "(" + type + " " + var.name + ") {\n");
					sb.append("\t\tthis." + var.name + " = " + var.name + ";\n");
					sb.append("\t\twriteField(\"" + var.name + "\");\n");
					sb.append("\t}\n");
				}
			}
		});
		visited.clear();
		sb.append("\tprotected List<String> getFieldOrder() {\n");
		sb.append("\t\treturn Arrays.asList(");
		for (int i = 0; i < params.size() - 1; i++)
			sb.append("\"" + params.get(i).name + "\", ");
		sb.append("\"" + params.get(params.size() - 1).name + "\"");
		sb.append(");\n");
		sb.append("\t}\n");
		sb.append("}\n");
		return new String[] { className, sb.toString() };
	}

	public int getFixedStepSize() {
		return fixedStepSize;
	}

	public File getHeader() {
		return header;
	}

	public File getLibrary() {
		return library;
	}

	// @SuppressWarnings("unchecked")
	// private void displayMap(HashMap<String, Object> paramMap, int i) {
	// String indent = "";
	// for (int j = 0; j < i; j++) {
	// indent += "\t";
	// }
	// for (String nodeName : paramMap.keySet()) {
	// Object node = paramMap.get(nodeName);
	// if (node instanceof HashMap) {
	// System.out.println(indent + nodeName + ": ");
	// displayMap((HashMap<String, Object>) node, i + 1);
	// } else
	// System.out.println(indent + nodeName);
	// }
	// }

	public String getParameterPrefix() {
		return parameterPrefix;
	}

	public JNAStructure getParameters() {
		try {
			load();
		} catch (InstantiationException | UnsatisfiedLinkError | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException
				| NullPointerException | IOException e) {
			if (!(e instanceof UnsatisfiedLinkError) && library != null && library.exists())
				System.err.println("Error while loading the library: " + e.getMessage());
		}
		if (parameters != null) {
			if (BeanEditor.getBeanDesc(parameters) == null)
				BeanEditor.registerBean(parameters, BeanManager.DEFAULTDIR);
			parameters.read();
		}
		return parameters;
	}

	public StepMode getStepMode() {
		return stepMode;
	}

	public Class<?>[] getStructType() {
		try {
			if (iop == null)
				iop = parseHeader(header);
			load();
			if (parameters != null)
				return new Class<?>[] { parameters.getClass() };
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException | IOException e) {
			if (!(e instanceof NoSuchFileException))
				e.printStackTrace();
		}
		return new Class<?>[0];
	}

	@Override
	public void initStruct() throws IOException {
		if (header == null || !header.exists() || library == null || !library.exists()) {
			updateOutputs(new String[0], new Class<?>[0]);
			return;
		}
		if (iop == null)
			iop = parseHeader(header);
		ArrayList<String> names = new ArrayList<>();
		ArrayList<Class<?>> types = new ArrayList<>();
		for (PrimitiveVariable var : iop.inputs.params) {
			names.add(var.name);
			types.add(var.size == -1 ? var.type : var.type.arrayType());
		}
		updateInputs(names.toArray(new String[0]), types.toArray(new Class<?>[0]));

		names.clear();
		types.clear();
		for (PrimitiveVariable var : iop.outputs.params) {
			names.add(var.name);
			types.add(var.size == -1 ? var.type : var.type.arrayType());
		}
		updateOutputs(names.toArray(new String[0]), types.toArray(new Class<?>[0]));
	}

	private void load() throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, IOException {
		if (lib == null && library != null && !library.getPath().isBlank()) {
			try {
				lib = NativeLibrary.getInstance(library.getAbsolutePath()); // Ajouter class Loader pour décharger?
			} catch (Exception | UnsatisfiedLinkError e) {
				System.err.println("Not able to load library: " + library.getAbsolutePath() + e.getMessage());
				return;
			}
			if (iop == null)
				iop = parseHeader(header);
			if (iop == null)
				return;
			ArrayList<Class<? extends Structure>> c = createAndLoadJNAJavaClass(iop);
			in = c.get(0).getConstructor().newInstance();
			out = c.get(1).getConstructor().newInstance();
			in.getClass().getMethod("setPointer", Pointer.class).invoke(in, lib.getGlobalVariableAddress(iop.inputs.variablename));
			out.getClass().getMethod("setPointer", Pointer.class).invoke(out, lib.getGlobalVariableAddress(iop.outputs.variablename));
			if (parameters != null) {
				parameters = (JNAStructure) c.get(2).getConstructor().newInstance();
				parameters.getClass().getMethod("setPointer", Pointer.class).invoke(parameters, lib.getGlobalVariableAddress(iop.parameters.variablename));
				parameters.read();
			}
		}
	}

	private void nextStep() {
//		System.out.println("invoke");
//		long time = System.currentTimeMillis();
		test_step.invokeVoid(null);
//		System.out.println("invoke done, execution time: " + (System.currentTimeMillis() - time));
		ArrayList<PrimitiveVariable> outParams = iop.outputs.params;
		Object[] outputs = new Object[outParams.size()];
		for (int i = 0; i < outputs.length; i++)
			outputs[i] = out.readField(outParams.get(i).name);
		triggerOutput(outputs, maxTs);
	}

	private static IOPDescriptor parseHeader(File header) throws IOException {
		if (header != null && !header.getPath().isBlank()) {
			if (!header.isDirectory() || header.exists()) {
				System.err.println("Simulink header: " + header + " is not a valid file");
				return null;
			}

			ArrayList<CStructDescriptor> structs = new ArrayList<>();
			ArrayList<Variable> externVariable = new ArrayList<>();

			String initializeMethodName = null;
			String stepMethodName = null;
			String terminateMethodName = null;

			List<String> lines = Files.readAllLines(header.toPath());
			StringBuilder typedefStructLines = null;
			StringBuilder extern = null;
			boolean isEndOfStructReach = false;
			boolean isInCommentMode = false;
			nextLine: for (String line : lines) {
				line = line.trim();
				int beginCommentInLine = 0;
				nextVerif: while (true) {
					if (isInCommentMode) {
						int indexOfCommentEnd = line.indexOf("*/");
						if (indexOfCommentEnd == -1) {
							if (beginCommentInLine == 0)
								continue nextLine;
							line = line.substring(0, beginCommentInLine).trim();
							break nextVerif;
						}
						String part1 = line.substring(0, beginCommentInLine);
						line = (part1 + line.substring(indexOfCommentEnd + 2)).trim();
					}
					int indexOfComment = line.indexOf("/*");
					if (indexOfComment != -1) {
						isInCommentMode = true;
						beginCommentInLine = indexOfComment;
						continue;
					}
					isInCommentMode = false;
					break;
				}
				if (line.isEmpty())
					continue;
				int indexOfComment = line.indexOf("//"); // Remove // comment
				if (indexOfComment != -1)
					line = line.substring(0, indexOfComment);

				if (typedefStructLines != null) {
					if (!isEndOfStructReach && line.contains("}"))
						isEndOfStructReach = true;
					if (isEndOfStructReach && line.contains(";")) {
						line = line.substring(0, line.indexOf(";"));
						typedefStructLines.append(" " + line);
						structs.add(processTypedefStruct(typedefStructLines));
						isEndOfStructReach = false;
						typedefStructLines = null;
					} else
						typedefStructLines.append(" " + line);
				} else if (extern != null) {
					if (line.contains(";")) {
						extern.append(" " + line.substring(0, line.indexOf(";")));
						if (extern.indexOf("(") == -1)
							externVariable.add(processExtern(extern.toString().trim()));
						extern = null;
					} else
						extern.append(" " + line);
				} else if (line.startsWith("typedef struct") || line.startsWith("struct")) {
					typedefStructLines = new StringBuilder();
					typedefStructLines.append(line);
					if (!isEndOfStructReach && line.contains("}"))
						isEndOfStructReach = true;
					if (isEndOfStructReach && line.contains(";")) {
						line = line.substring(0, line.indexOf(";") + 1);
						structs.add(processTypedefStruct(typedefStructLines));
						isEndOfStructReach = false;
						typedefStructLines = null;
					}
				} else if (line.startsWith("extern")) {
					extern = new StringBuilder(line.substring(0, line.indexOf(";")).replaceAll("__declspec\\(dllexport\\)", ""));
					if (line.contains(";")) {
						if (extern.indexOf("(") == -1)
							externVariable.add(processExtern(extern.toString().trim()));
						else {
							StringTokenizer st = new StringTokenizer(extern.toString(), " ");
							if (st.hasMoreTokens() && st.nextToken().equals("extern") && st.hasMoreTokens() && st.nextToken().equals("void") && st.hasMoreTokens()) {
								String funcName = extern.substring(extern.indexOf("void") + 4).trim();
								int indexOfParan = funcName.indexOf("(");
								if (indexOfParan != -1) {
									String parameters = funcName.substring(indexOfParan + 1, funcName.lastIndexOf(")")).trim();
									if (parameters.equals("void") || parameters.isEmpty()) {
										String methodeName = funcName.substring(0, indexOfParan);
										if (methodeName.endsWith("_initialize"))
											initializeMethodName = methodeName;
										else if (methodeName.endsWith("_step"))
											stepMethodName = methodeName;
										else if (methodeName.endsWith("_terminate"))
											terminateMethodName = methodeName;
									}
								}
							}
						}
						extern = null;
					} else
						extern.append(line);
				}
			}

			CStructDescriptor inputs = null;
			CStructDescriptor outputs = null;
			CStructDescriptor parameters = null;
			for (CStructDescriptor struct : structs)
				if (struct.structName.startsWith("ExtU_")) {
					for (Variable variable : externVariable)
						if (variable.type.equals(struct.structName))
							inputs = new CStructDescriptor(struct.structName, variable.name, struct.params);
				} else if (struct.structName.startsWith("ExtY_")) {
					for (Variable variable : externVariable)
						if (variable.type.equals(struct.structName))
							outputs = new CStructDescriptor(struct.structName, variable.name, struct.params);
				} else if (struct.structName.startsWith("P_"))
					for (Variable variable : externVariable)
						if (variable.type.equals(struct.structName))
							parameters = new CStructDescriptor(struct.structName, variable.name, struct.params);
			return new IOPDescriptor(initializeMethodName, stepMethodName, terminateMethodName, inputs, outputs, parameters);
		}
		return null;
	}

	public void process() {
		Object[] addInput = getAdditionalInputs();
		if (addInput == null)
			return;
		ArrayList<PrimitiveVariable> inParams = iop.inputs.params;
		maxTs = Long.MIN_VALUE;
		for (int i = 0; i < inParams.size(); i++) {
			Object input = addInput[i];
			if (input != null) {
				in.writeField(inParams.get(i).name, addInput[i]);
				long ts = getTimeStamp(i);
				if (ts > maxTs)
					maxTs = ts;
			} else
				addInput[i] = oldInputs[i];
		}
		if (stepMode != StepMode.FIXED)
			nextStep();
		oldInputs = addInput;
	}

	@Override
	public void setEnable() {
		fireSetPropertyEnable(this, "fixedStepSize", stepMode == StepMode.FIXED);
	}

	public void setFixedStepSize(int fixedStepSize) {
		boolean isRunning = stopTimer();
		this.fixedStepSize = fixedStepSize;
		if (isRunning && stepMode == StepMode.FIXED)
			try {
				birth();
			} catch (Exception e) {
				e.printStackTrace();
			}
	}

	public void setHeader(File header) {
		if (header.equals(this.header))
			return;
		dispose();
		this.header = header;
		restartAndReloadStructLater();
	}

	public void setLibrary(File library) {
		if (library.equals(this.library))
			return;
		dispose();
		this.library = library;
		restartLater();
	}

	public void setParameterPrefix(String parameterPrefix) {
		this.parameterPrefix = parameterPrefix;
		dispose();
	}

	public void setParameters(JNAStructure parameters) {
		this.parameters = parameters;
		if (parameters != null) {
			try {
				parameters.getClass().getMethod("setPointer", Pointer.class).invoke(parameters, lib.getGlobalVariableAddress(iop.parameters.variablename));
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
				e.printStackTrace();
			}
			parameters.write();
		}
	}

	public void setStepMode(StepMode stepMode) {
		boolean isRunning = stopTimer();
		this.stepMode = stepMode;
		if (isRunning && stepMode == StepMode.FIXED)
			try {
				birth();
			} catch (Exception e) {
				e.printStackTrace();
			}
		setEnable();
	}

	private boolean stopTimer() {
		if (fixedModeTimer != null) {
			fixedModeTimer.cancel();
			fixedModeTimer.purge();
			fixedModeTimer = null;
			return true;
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	private void writeClass(StringBuilder sb, String structName, HashMap<String, Object> node, String className, int lvl) {
		// className = className.substring(0, className.length() - 1); //A voir
		String indent = "";
		for (int i = 0; i < lvl; i++)
			indent += "\t";
		sb.append(indent + "class " + className + "{\n");
		String _indent = indent + "\t";

		sb.append(_indent + "public " + className + "(){}\n");

		HashSet<String> visited = new HashSet<>();
		node.forEach((name, element) -> {
			if (element instanceof HashMap)
				writeClass(sb, structName, (HashMap<String, Object>) element, name, lvl + 1);
			else {
				PrimitiveVariable var = (PrimitiveVariable) element;
				String upName = var.name.substring(0, 1).toUpperCase() + var.name.substring(1);
				if (!visited.contains(upName) && (parameterPrefix == null || var.name.startsWith(parameterPrefix))) {
					visited.add(upName);
					String upSimpleName = name.substring(0, 1).toUpperCase() + name.substring(1); // A voir
					String type = var.type + (var.size == -1 ? "" : "[]");
					sb.append(_indent + "public " + type + " get" + upSimpleName + "() {\n");
					sb.append(_indent + "\treturn " + var.name + ";\n");
					sb.append(_indent + "}\n");
					sb.append(_indent + "public void set" + upSimpleName + "(" + type + " " + var.name + ") {\n");
					sb.append(_indent + "\t" + structName + ".this." + var.name + " = " + var.name + ";\n");
					sb.append(_indent + "\twriteField(\"" + var.name + "\");\n");
					sb.append(_indent + "}\n");
				}
			}
		});
		visited.clear();
		sb.append(_indent + "public String toString() {\n");
		sb.append(_indent + "\treturn \"\";\n");
		sb.append(_indent + "}\n");
		sb.append(indent + "}\n");
		sb.append(indent + "private " + className + " " + className + " = new " + className + "();\n");
		String upName = className.substring(0, 1).toUpperCase() + className.substring(1);
		sb.append(indent + "@PropertyInfo(nullable = false)\n");
		sb.append(indent + "public " + className + " get" + upName + "() {\n");
		sb.append(indent + "\treturn " + className + ";\n");
		sb.append(indent + "}\n");
		sb.append(indent + "public void set" + upName + "(" + className + " " + className + ") {\n");
		sb.append(indent + "\tthis." + className + " = " + className + ";\n");
		sb.append(indent + "}\n");
	}

	// @Override
	// public boolean isBlockReady(boolean[] input) {
	// if (stepMode == StepMode.VARIABLEWITHALLENTRIES) {
	// for (boolean b : input)
	// if (!b)
	// return false;
	// return true;
	// } else {
	// for (boolean b : input)
	// if (b)
	// return true;
	// return false;
	// }
	// }
}

class Variable {
	public final String name;
	public final String type;
	public final int size;

	public Variable(String name, String type, int size) {
		this.name = name;
		if (type.startsWith("P_") && type.endsWith("_T"))
			type = type + "_";
		this.type = type;
		this.size = size;
	}

	@Override
	public String toString() {
		return name + (size == -1 ? "" : "[" + size + "]") + " " + type;
	}
}
