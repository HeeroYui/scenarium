/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.wrapper;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.concurrent.ConcurrentHashMap;

import javax.tools.Diagnostic;
import javax.tools.JavaFileObject;

import org.beanmanager.editors.PropertyInfo;
import org.beanmanager.editors.container.BeanInfo;
import org.beanmanager.editors.container.DynamicBeanInfo;
import org.scenarium.filemanager.scenario.dataflowdiagram.BlockInfo;
import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;
import org.scenarium.tools.dynamiccompilation.CompilationException;
import org.scenarium.tools.dynamiccompilation.InlineCompiler;

import com.sun.jna.Function;
import com.sun.jna.Library;
import com.sun.jna.NativeLibrary;
import com.sun.jna.Pointer;
import com.sun.jna.Structure;

@BlockInfo(info = "This block can execute C or C++ code embedded into a static library")
public class COrCpp extends EvolvedOperator {
	private static ConcurrentHashMap<COrCpp, File> loadedLib = new ConcurrentHashMap<>();

	// TODO pas faire de reset ou cancel sinon ca crash
	public static void main(String[] args) {
		// System.getProperties().forEach((a, b) -> System.out.println(a + " " + b));
		System.out.println(System.getProperty("java.library.path"));
		try {

			// System.load("/home/revilloud/detrolidep/libpng12.so.0");
			// System.load("/home/revilloud/detrolidep/libopencv_core.so.3.2");
			System.load("/home/revilloud/detrolidep/libopencv_imgproc.so.3.2");
			// System.load("/home/revilloud/detrolidep/libopencv_imgcodecs.so.3.2");
			// System.load("/home/revilloud/detrolidep/libopencv_highgui.so.3.2");
			// System.load("/home/revilloud/detrolidep/libwrapper.so");

			System.loadLibrary("wrapper");

			// System.load("/home/revilloud/detrolidep/libwrapper.so");
		} catch (UnsatisfiedLinkError e) {
			System.err.println(e.getMessage());
		}
		// System.setProperty("jna.library.path", "/home/revilloud/detrolidep/");
		// System.setProperty("jna.platform.library.path", "/home/revilloud/detrolidep/");
		// NativeLibrary lib2 = NativeLibrary.getInstance("/home/revilloud/detrolidep/libwrapper.so"); // Ajouter class Loader pour décharger?
		// System.out.println("good");
		// Pointer f = lib2.getGlobalVariableAddress("detroli_inputs");
		// System.out.println(f);
		// // System.out.println(Arrays.toString(new String("int []patate").split("(?<=[{\\[\\]* ])(?=[^{\\[\\]* ])")));
		//
		// COrCpp simulink = new COrCpp();
		// simulink.header = new File("/home/revilloud/workspace/JnaC/src/JnaC.h");
		// simulink.library = new File("/home/revilloud/workspace/JnaC/Release/libJnaC.so");
		// try {
		// simulink.initStruct();
		// simulink.birth();
		// } catch (Exception e) {
		// e.printStackTrace();
		// }
	}

	private static Variable processExtern(String extern) {
		int indexOfEquals = extern.indexOf("=");
		if (indexOfEquals != -1)
			extern = extern.substring(0, indexOfEquals - 1).trim();
		int indexOfParamName = extern.lastIndexOf(" ");
		String name = extern.substring(indexOfParamName + 1);
		String type = extern.substring(0, indexOfParamName);
		int indexOfParamType = type.lastIndexOf(" ");
		type = type.substring(indexOfParamType + 1);
		int indexOfArray = name.indexOf("[");
		int size;
		if (indexOfArray != -1) {
			String sSize = name.substring(indexOfArray + 1);
			size = Integer.parseInt(sSize.substring(0, sSize.indexOf("]")));
			name = name.substring(0, indexOfArray);
		} else
			size = -1;
		return new Variable(name, type, size);
	}

	private static CStructDescriptor processTypedefStruct(StringBuilder struct) {
		int indexOfBeginHeader = struct.indexOf("{");
		int indexOfEndHeader = struct.indexOf("}");
		String header = struct.substring(0, indexOfBeginHeader).trim();
		String parameters = struct.substring(indexOfBeginHeader + 1, indexOfEndHeader).trim();
		String foot = struct.substring(indexOfEndHeader + 1).trim();
		String structName = header.startsWith("typedef") ? foot : header.substring(header.lastIndexOf(" ")).trim();
		StringTokenizer st = new StringTokenizer(parameters, ";");
		ArrayList<PrimitiveVariable> params = new ArrayList<>();
		while (st.hasMoreTokens()) {
			String param = st.nextToken().trim();
			int indexOfEquals = param.indexOf("=");
			if (indexOfEquals != -1)
				param = param.substring(0, indexOfEquals - 1).trim();
			String[] s = param.split("(?<=[{\\[\\]* ])(?=[^{\\[\\]* ])");
			param = s[0];
			if (s.length >= 2 && param.trim().equals("long") && (s[1].trim().equals("long") || s[1].trim().equals("long*") || s[1].trim().equals("long *"))) {
				String[] s2 = new String[s.length - 1];
				s2[0] = s[0] + s[1];
				for (int i = 2; i < s.length; i++)
					s2[i - 1] = s[i];
				param = s2[0];
				s = s2;
			}
			int indexOfArray = param.indexOf("[");
			int size;
			if (indexOfArray != -1) {
				String sSize = param.substring(indexOfArray + 1);
				size = Integer.parseInt(sSize.substring(0, sSize.indexOf("]")));
				param = param.substring(0, indexOfArray);
			} else
				size = PrimitiveVariable.NotAnArray;
			Class<?> type = getJavaTypeFromCType(indexOfArray == -1 ? param : param.substring(0, indexOfArray + 1));
			if (type != null)
				params.add(new PrimitiveVariable(s[1], type, size));
		}
		return new CStructDescriptor(structName, params);
	}

	@PropertyInfo(index = 0, info = "header (*.h) of the generated diaram")
	private File header;
	@PropertyInfo(index = 1, info = "library (*.so, *.dll) of the generated diaram")
	private File library;
	@PropertyInfo(index = 2, nullable = false, info = "Parameters of the diaram")
	@BeanInfo(alwaysExtend = true, inline = true)
	@DynamicBeanInfo(possibleSubclassesMethodName = "getStructType")
	private JNAStructure parameters;
	private NativeLibrary lib;

	private IOPDescriptor iop;

	private JNAInput in;

	private JNAOutput out;

	private Function processMethod;

	@Override
	public void birth() {
		if (lib == null)
			return;
		try {
			load();
			if (iop.initializeMethodName != null)
				lib.getFunction(iop.initializeMethodName).invokeVoid(null);
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException | IOException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	private static ArrayList<Class<? extends Structure>> createAndLoadJNAJavaClass(IOPDescriptor iOPDescriptor) {
		String[] inCode = generateIOCode(iOPDescriptor.inputs, true);
		String[] outCode = generateIOCode(iOPDescriptor.outputs, false);
		String[] paramCode = generateParameterCode(iOPDescriptor.parameters);

		ArrayList<Class<? extends Structure>> c = new ArrayList<>();
		try {
			InlineCompiler ic = new InlineCompiler(new String[] { inCode[0], outCode[0], paramCode[0] }, new String[] { inCode[1], outCode[1], paramCode[1] });
			for (Class<?> class1 : ic.getCompileClass())
				c.add((Class<? extends Structure>) class1);
		} catch (CompilationException e) {
			e.printStackTrace();
			for (Diagnostic<? extends JavaFileObject> error : e.getErrors())
				System.err.println("Line: " + error.getLineNumber() + ": " + error.getMessage(Locale.getDefault()));
		}
		return c;
	}

	@Override
	public void death() {
		if (iop != null && iop.terminateMethodName != null && lib != null)
			lib.getFunction(iop.terminateMethodName).invokeVoid(null);
	}

	private void dispose() {
		if (lib != null) {
			if (iop.terminateMethodName != null)
				lib.getFunction(iop.terminateMethodName).invokeVoid(null);
			if (iop.destroyMethodName != null)
				lib.getFunction(iop.destroyMethodName).invokeVoid(null);
			lib.dispose();
			if (loadedLib.get(this) != null)
				loadedLib.remove(this);
			lib = null;
			iop = null;
			in = null;
			out = null;
			processMethod = null;
		}
	}

	private static String[] generateIOCode(CStructDescriptor struct, boolean input) {
		ArrayList<PrimitiveVariable> params = struct.params;
		StringBuilder sb = new StringBuilder();
		String pName = "simulink";
		String className = pName + "." + struct.structName;
		sb.append("package " + pName + ";\n");
		sb.append("import java.util.List;\n");
		sb.append("import com.sun.jna.Native;\n");
		sb.append("import com.sun.jna.Pointer;\n");
		sb.append("import com.sun.jna.Structure;\n");
		sb.append("import org.scenarium.operator.wrapper." + (input ? "JNAInput" : "JNAOutput") + ";\n");

		sb.append("public class " + struct.structName + " extends " + (input ? "JNAInput" : "JNAOutput") + "{\n");
		for (PrimitiveVariable var : params) {
			sb.append("\tpublic " + getCTypeFromJavaType(var.type, var.size));
			if (var.size < 0)
				sb.append(" " + var.name + ";\n");
			else
				sb.append("[] " + var.name + " = new " + var.type + "[" + var.size + "];\n");
		}

		sb.append("\tpublic " + struct.structName + "() {\n");
		sb.append("\t\tsuper();\n");
		if (input)
			for (PrimitiveVariable var : params)
				if (var.type.isArray() && var.size < 0)
					sb.append("\t\twriteField(\"" + var.name + "\", null);\n");
		sb.append("\t}\n");
		sb.append("\tpublic void setPointer(Pointer p) {\n");
		sb.append("\t\tuseMemory(p);\n");
		sb.append("\t}\n");

		if (input) {
			sb.append("\tpublic void write(int index, Object value) {\n");
			sb.append("\t\tswitch (index) {\n");
			int indexInput = 0;
			for (int i = 0; i < params.size(); i++) {
				PrimitiveVariable param = params.get(i);
				if (param.size == PrimitiveVariable.DynamicArraySize)
					continue;
				sb.append("\t\tcase " + indexInput++ + ": \n");

				if (param.size == PrimitiveVariable.DynamicArray) {
					String type = param.type.getComponentType().getCanonicalName();
					String paramSizeName = param.name + "_length";
					sb.append("\t\t\tif(value == null) {\n");
					sb.append("\t\t\t\t" + paramSizeName + " = -1;\n");
					sb.append("\t\t\t\tif(" + param.name + " != null) {\n");
					sb.append("\t\t\t\t\tNative.free(Pointer.nativeValue(" + param.name + "));\n");
					sb.append("\t\t\t\t\t" + param.name + " = null;\n");
					sb.append("\t\t\t\t}\n");
					sb.append("\t\t\t}else {\n");
					sb.append("\t\t\t\t" + type + "[] array = (" + type + "[])value;\n");
					sb.append("\t\t\t\tif(" + param.name + " == null || " + paramSizeName + " != array.length) {\n");
					sb.append("\t\t\t\t\tif(" + param.name + " != null)\n");
					sb.append("\t\t\t\t\t\tNative.free(Pointer.nativeValue(" + param.name + "));\n");
					sb.append("\t\t\t\t\t" + param.name + " = new Pointer(Native.malloc(array.length * " + getTypeSize(param.type.getComponentType()) + "));\n");
					sb.append("\t\t\t\t\t" + paramSizeName + " = array.length;\n");
					sb.append("\t\t\t\t}\n");
					sb.append("\t\t\t\t" + param.name + ".write(0, array, 0, array.length);\n");
					sb.append("\t\t\t}\n");
				} else
					sb.append("\t\t\t" + param.name + " = value == null ? null : new " + getCTypeFromJavaType(param.type, param.size) + "((" + getWrapperName(param.type.getComponentType()) + ") value);\n");
				sb.append("\t\t\tbreak;\n");
			}
			sb.append("\t\t}\n");
			sb.append("\t}\n");
		} else {
			sb.append("\tpublic Object read(int index) {\n");
			sb.append("\t\tswitch (index) {\n");
			int indexOutput = 0;
			for (int i = 0; i < params.size(); i++) {
				PrimitiveVariable param = params.get(i);
				if (param.size == PrimitiveVariable.DynamicArraySize)
					continue;
				sb.append("\t\tcase " + indexOutput++ + " :\n");
				if (param.size == PrimitiveVariable.DynamicArray)
					sb.append("\t\t\treturn " + param.name + " == null ? null : ((Pointer) " + param.name + ")." + getArrayGetter(param.type.getComponentType()) + "(0, " + param.name + "_length);\n");
				else
					sb.append("\t\t\treturn " + param.name + " == null ? null : ((" + getCTypeFromJavaType(param.type, param.size) + ")" + param.name + ").getValue();\n");
			}
			sb.append("\t\t}\n");
			sb.append("\t\treturn null;\n");
			sb.append("\t}\n");
		}

		sb.append("\tprotected List<String> getFieldOrder() {\n");
		sb.append("\t\treturn List.of(");
		for (int i = 0; i < params.size() - 1; i++)
			sb.append("\"" + params.get(i).name + "\", ");
		sb.append("\"" + params.get(params.size() - 1).name + "\"");
		sb.append(");\n");
		sb.append("\t}\n");
		if (input) {
			sb.append("\tpublic void realease() {\n");
			for (PrimitiveVariable var : params)
				if (var.type.isArray() && var.size < 0) {
					sb.append("\t\tif(this." + var.name + " != null) {\n");
					sb.append("\t\t\tNative.free(Pointer.nativeValue(this." + var.name + (var.size != PrimitiveVariable.DynamicArray ? ".getPointer()" : "") + "));\n");
					sb.append("\t\t\tthis." + var.name + " = null;\n");
					sb.append("\t\t}\n");
				}
			sb.append("\t}\n");
		}
		sb.append("}\n");
		return new String[] { className, sb.toString() };
	}

	private static String[] generateParameterCode(CStructDescriptor struct) {
		ArrayList<PrimitiveVariable> params = struct.params;
		// displayMap(paramMap, 0);
		StringBuilder sb = new StringBuilder();
		String pName = "simulink";
		String className = pName + "." + struct.structName;
		sb.append("package " + pName + ";\n");
		sb.append("import java.util.List;\n");
		sb.append("import com.sun.jna.Native;\n");
		sb.append("import com.sun.jna.Pointer;\n");
		sb.append("import com.sun.jna.Structure;\n");
		sb.append("import org.scenarium.operator.wrapper.JNAStructure;\n");
		sb.append("public class " + struct.structName + " extends JNAStructure {\n");
		for (PrimitiveVariable var : params) { // déclaration des variable, on change rien
			sb.append("\tpublic " + (var.size == PrimitiveVariable.DynamicArray ? "Pointer" : var.type.getCanonicalName()));
			if (var.size < 0) {
				sb.append(" " + var.name + ";\n");
			} else
				sb.append("[] " + var.name + " = new " + var.type + "[" + var.size + "];\n");
		}
		sb.append("\tpublic " + struct.structName + "() {\n");
		sb.append("\t\tsuper();\n");
		sb.append("\t}\n");

		sb.append("\tpublic void setPointer(Pointer p) {\n");
		sb.append("\t\tuseMemory(p);\n");
		sb.append("\t}\n");

		HashSet<String> visited = new HashSet<>();
		for (PrimitiveVariable param : params) {
			PrimitiveVariable var = param;
			String upName = var.name.substring(0, 1).toUpperCase() + var.name.substring(1);
			if (!visited.contains(upName)) {
				visited.add(upName);
				boolean isBoolean = var.type == int.class && var.name.endsWith("_boolean");
				if (var.size >= PrimitiveVariable.NotAnArray) {
					if (isBoolean) {
						upName = var.name.substring(0, var.name.length() - "_boolean".length());
						upName = upName.substring(0, 1).toUpperCase() + upName.substring(1);
						String type = boolean.class.getCanonicalName() + (var.size == -1 ? "" : "[]");
						sb.append("\tpublic " + type + " get" + upName + "() {\n");
						sb.append("\t\treturn ((int) readField(\"" + var.name + "\")) != 0" + ";\n");
						sb.append("\t}\n");
						sb.append("\tpublic void set" + upName + "(" + type + " " + var.name + ") {\n");
						sb.append("\t\twriteField(\"" + var.name + "\", " + var.name + " ? 1 : 0);\n");
						sb.append("\t}\n");
					} else {
						String type = (isBoolean ? boolean.class : var.type).getCanonicalName() + (var.size == -1 ? "" : "[]");
						sb.append("\tpublic " + type + " get" + upName + "() {\n");
						sb.append("\t\treturn (" + type + ") readField(\"" + var.name + "\")" + ";\n");
						sb.append("\t}\n");
						sb.append("\tpublic void set" + upName + "(" + type + " " + var.name + ") {\n");
						sb.append("\t\twriteField(\"" + var.name + "\", " + var.name + ");\n");
						sb.append("\t}\n");
					}
				} else if (var.size == PrimitiveVariable.DynamicArray) {
					String type = var.type.getCanonicalName();
					String ctype = getCTypeFromJavaType(var.type, var.size);
					String varSizeName = var.name + "_length";
					sb.append("\tpublic " + type + " get" + upName + "() {\n");
					sb.append("\t\t" + var.name + " = (" + ctype + ") readField(\"" + var.name + "\");\n");
					sb.append("\t\t" + varSizeName + " = (int) readField(\"" + varSizeName + "\");\n");
					sb.append("\t\treturn " + var.name + " == null ? null : " + var.name + "." + getArrayGetter(var.type.getComponentType()) + "(0, " + varSizeName + ")" + ";\n");
					sb.append("\t}\n");
					sb.append("\tpublic void set" + upName + "(" + type + " " + var.name + ") {\n");
					sb.append("\t\tthis." + var.name + " = (" + ctype + ") readField(\"" + var.name + "\");\n");
					sb.append("\t\t" + varSizeName + " = (int) readField(\"" + varSizeName + "\");\n");
					sb.append("\t\tif(" + var.name + " == null) {\n");
					sb.append("\t\t\tif(this." + var.name + " != null) {\n");
					sb.append("\t\t\t\tNative.free(Pointer.nativeValue(this." + var.name + "));\n");
					sb.append("\t\t\t\tthis." + var.name + " = null;\n");
					sb.append("\t\t\t\twriteField(\"" + var.name + "\", null);\n");
					sb.append("\t\t\t}\n");
					sb.append("\t\t}else {\n");
					sb.append("\t\t\tif(this." + var.name + " == null || " + varSizeName + " != " + var.name + ".length){\n");
					sb.append("\t\t\t\tif(this." + var.name + " != null)\n");
					sb.append("\t\t\t\t\tNative.free(Pointer.nativeValue(this." + var.name + "));\n");
					sb.append("\t\t\t\tthis." + var.name + " = new Pointer(Native.malloc(" + var.name + ".length * " + getTypeSize(var.type.getComponentType()) + "));\n");
					sb.append("\t\t\t\t" + varSizeName + " = " + var.name + ".length;\n");
					sb.append("\t\t\t\twriteField(\"" + var.name + "\", this." + var.name + ");\n");
					sb.append("\t\t\t\twriteField(\"" + varSizeName + "\", this." + varSizeName + ");\n");
					sb.append("\t\t\t}\n");
					sb.append("\t\t\tthis." + var.name + ".write(0, " + var.name + ", 0, " + varSizeName + ");\n");
					sb.append("\t\t}\n");
					sb.append("\t}\n");
				}
			}
		}
		sb.append("\tpublic String toString() {\n");
		String blockName = className.substring(className.lastIndexOf(".") + 1);
		blockName = blockName.substring(0, blockName.lastIndexOf("_"));
		sb.append("\t\treturn \"" + blockName + " parameters\";\n");
		sb.append("\t}\n");
		visited.clear();
		sb.append("\tprotected List<String> getFieldOrder() {\n");
		sb.append("\t\treturn List.of(");
		if (!params.isEmpty()) {
			for (int i = 0; i < params.size() - 1; i++)
				sb.append("\"" + params.get(i).name + "\", ");
			sb.append("\"" + params.get(params.size() - 1).name + "\"");
		}
		sb.append(");\n");
		sb.append("\t}\n");
		sb.append("}\n");
		return new String[] { className, sb.toString() };
	}

	public File getHeader() {
		return header;
	}

	public File getLibrary() {
		return library;
	}

	public JNAStructure getParameters() {
		try {
			load();
		} catch (InstantiationException | UnsatisfiedLinkError | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException | NullPointerException | IOException e) {
			if (!(e instanceof UnsatisfiedLinkError) && library != null && library.exists())
				System.err.println("Error while loading the library: " + e.getMessage());
		}
		if (parameters != null)
			parameters.read();
		return parameters;
	}

	public Class<?>[] getStructType() {
		try {
			if (iop == null)
				iop = parseHeader(header);
			load();
			return parameters == null ? new Class<?>[0] : new Class<?>[] { parameters.getClass() };
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException | IOException e) {
			if (!(e instanceof NoSuchFileException) && !header.getAbsolutePath().isEmpty())
				e.printStackTrace();
		}
		return new Class<?>[0];
	}

	@Override
	public void initStruct() throws IOException {
		if (header == null || !header.exists()) {
			updateOutputs(new String[0], new Class<?>[0]);
			return;
		}
		if (iop == null)
			iop = parseHeader(header);
		ArrayList<String> names = new ArrayList<>();
		ArrayList<Class<?>> types = new ArrayList<>();
		for (PrimitiveVariable var : iop.inputs.params)
			if (var.size != PrimitiveVariable.DynamicArraySize) {
				Class<?> type = var.size > 0 ? var.type.arrayType() : var.size == PrimitiveVariable.NotAnArray ? var.type.isArray() ? var.type.getComponentType() : var.type : var.type;
				if (type == int.class && var.name.endsWith("_boolean")) {
					names.add(var.name.substring(0, var.name.length() - "_boolean".length()));
					types.add(boolean.class);
				} else {
					names.add(var.name);
					types.add(type);
				}
			}
		updateInputs(names.toArray(new String[0]), types.toArray(new Class<?>[0]));

		names.clear();
		types.clear();
		for (PrimitiveVariable var : iop.outputs.params)
			if (var.size != PrimitiveVariable.DynamicArraySize) {
				Class<?> type = var.size > 0 ? var.type.arrayType() : var.size == PrimitiveVariable.NotAnArray ? var.type.isArray() ? var.type.getComponentType() : var.type : var.type;
				if (type == int.class && var.name.endsWith("_boolean")) {
					names.add(var.name.substring(0, var.name.length() - "_boolean".length()));
					types.add(boolean.class);
				} else {
					names.add(var.name);
					types.add(type);
				}
			}
		updateOutputs(names.toArray(new String[0]), types.toArray(new Class<?>[0]));
	}

	private void load() throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, IOException {
		if (lib == null && library != null && !library.getPath().isEmpty() && library.exists()) {
			File fileToLoad;
			synchronized (this) {
				if (!loadedLib.values().contains(library)) {
					fileToLoad = library;
					loadedLib.put(this, library);
				} else {
					String libExt = System.mapLibraryName("");
					libExt = libExt.substring(libExt.indexOf("."), libExt.length());
					fileToLoad = File.createTempFile("lib", libExt);
					fileToLoad.deleteOnExit();
					Files.copy(library.toPath(), fileToLoad.toPath(), StandardCopyOption.REPLACE_EXISTING);
				}
			}
			try {
				lib = NativeLibrary.getInstance(fileToLoad.getAbsolutePath(), Map.of(Library.OPTION_OPEN_FLAGS, 1)); // Ajouter class Loader pour décharger?
				if (fileToLoad != library)
					fileToLoad.delete();
			} catch (UnsatisfiedLinkError e) {
				try {
					System.load(library.getAbsolutePath());
					e.printStackTrace();
				} catch (UnsatisfiedLinkError e2) {
					System.err.println(e2.getMessage());
				}
			}
			if (iop == null)
				iop = parseHeader(header);
			if (iop.initMethodName != null)
				lib.getFunction(iop.initMethodName).invokeVoid(null);
			ArrayList<Class<? extends Structure>> c = createAndLoadJNAJavaClass(iop);
			in = (JNAInput) c.get(0).getConstructor().newInstance();
			out = (JNAOutput) c.get(1).getConstructor().newInstance();
			in.getClass().getMethod("setPointer", Pointer.class);
			lib.getGlobalVariableAddress(iop.inputs.variablename);
			in.getClass().getMethod("setPointer", Pointer.class).invoke(in, lib.getGlobalVariableAddress(iop.inputs.variablename));
			out.getClass().getMethod("setPointer", Pointer.class).invoke(out, lib.getGlobalVariableAddress(iop.outputs.variablename));
			in.setAutoRead(false);
			in.setAutoWrite(false);
			in.setAutoSynch(false);
			out.setAutoRead(false);
			out.setAutoWrite(false);
			out.setAutoSynch(false);
			processMethod = lib.getFunction(iop.stepMethodName);
			parameters = (JNAStructure) c.get(2).getConstructor().newInstance();
			parameters.getClass().getMethod("setPointer", Pointer.class).invoke(parameters, lib.getGlobalVariableAddress(iop.parameters.variablename));
			parameters.setAutoRead(false);
			parameters.setAutoWrite(false);
			parameters.setAutoSynch(false);
			parameters.read();
		}
	}

	private static IOPDescriptor parseHeader(File header) throws IOException {
		if (header == null || !header.exists())
			return null;
		ArrayList<CStructDescriptor> structs = new ArrayList<>();
		ArrayList<Variable> externVariable = new ArrayList<>();

		String initMethodName = null;
		String birthMethodName = null;
		String processMethodName = null;
		String deathMethodName = null;
		String destroyMethodName = null;

		List<String> lines = Files.readAllLines(header.toPath());
		StringBuilder typedefStructLines = null;
		StringBuilder extern = null;
		boolean isEndOfStructReach = false;
		boolean isInCommentMode = false;
		nextLine: for (String line : lines) {
			line = line.trim();
			line = line.replace("extern \"C\" ", "extern ");
			line = line.replace("__declspec(dllexport)", "");
			line = line.replaceAll(" +", " ");
			line = line.trim();

			int beginCommentInLine = 0;
			nextVerif: while (true) {
				if (isInCommentMode) {
					int indexOfCommentEnd = line.indexOf("*/");
					if (indexOfCommentEnd == -1) {
						if (beginCommentInLine == 0)
							continue nextLine;
						line = line.substring(0, beginCommentInLine).trim();
						break nextVerif;
					}
					String part1 = line.substring(0, beginCommentInLine);
					line = (part1 + line.substring(indexOfCommentEnd + 2)).trim();
				}
				int indexOfComment = line.indexOf("/*");
				if (indexOfComment != -1) {
					isInCommentMode = true;
					beginCommentInLine = indexOfComment;
					continue;
				}
				isInCommentMode = false;
				break;
			}
			if (line.isEmpty())
				continue;
			int indexOfComment = line.indexOf("//"); // Remove // comment
			if (indexOfComment != -1)
				line = line.substring(0, indexOfComment);

			if (typedefStructLines != null) {
				if (!isEndOfStructReach && line.contains("}"))
					isEndOfStructReach = true;
				if (isEndOfStructReach && line.contains(";")) {
					line = line.substring(0, line.indexOf(";"));
					typedefStructLines.append(" " + line);
					CStructDescriptor struct = processTypedefStruct(typedefStructLines);
					if (!typedefStructLines.substring(typedefStructLines.indexOf("{") + 1).startsWith("typedef struct")) {
						String foot = typedefStructLines.substring(typedefStructLines.indexOf("}") + 1).trim();
						if (!foot.isEmpty()) {
							int indexEqual = foot.indexOf("=");
							if (indexEqual >= 0)
								foot = foot.substring(0, indexEqual);
							externVariable.add(new Variable(foot.trim(), struct.structName, -1));
						}
					}
					structs.add(struct);
					isEndOfStructReach = false;
					typedefStructLines = null;
				} else
					typedefStructLines.append(" " + line);
			} else if (extern != null) {
				if (line.contains(";")) {
					extern.append(" " + line.substring(0, line.indexOf(";")));
					if (extern.indexOf("(") == -1)
						externVariable.add(processExtern(extern.toString().trim()));
					extern = null;
				} else
					extern.append(" " + line);
			} else if (line.startsWith("extern struct") || line.startsWith("typedef struct") || line.startsWith("struct") || line.startsWith("extern \"C\" struct")) {
				// line = line.replace("\"C\" struct __declspec(dllexport)", "struct");
				typedefStructLines = new StringBuilder();
				typedefStructLines.append(line);
				if (!isEndOfStructReach && line.contains("}"))
					isEndOfStructReach = true;
				if (isEndOfStructReach && line.contains(";")) {
					line = line.substring(0, line.indexOf(";") + 1);
					CStructDescriptor struct = processTypedefStruct(typedefStructLines);
					if (!typedefStructLines.substring(typedefStructLines.indexOf("{") + 1).startsWith("typedef struct")) {
						String foot = typedefStructLines.substring(typedefStructLines.indexOf("}") + 1).trim();
						if (!foot.isEmpty()) {
							int indexEqual = foot.indexOf("=");
							if (indexEqual >= 0)
								foot = foot.substring(0, indexEqual);
							externVariable.add(new Variable(foot.trim(), struct.structName, -1));
						}
					}
					structs.add(struct);
					structs.add(processTypedefStruct(typedefStructLines));
					isEndOfStructReach = false;
					typedefStructLines = null;
				}
			} else if (line.startsWith("extern") || line.startsWith("void")) {
				// line = line.replace("\"", "");
				// line = line.replace("\"C\" __declspec(dllexport)", "");
				extern = new StringBuilder(line.substring(0, line.indexOf(";")));
				if (line.contains(";")) {
					if (extern.indexOf("(") == -1)
						externVariable.add(processExtern(extern.toString().trim()));
					else {
						StringTokenizer st = new StringTokenizer(extern.toString(), " ");
						if (st.hasMoreTokens()) {
							String nextElement = st.nextToken();
							if ((nextElement.equals("void") || nextElement.equals("extern") && st.hasMoreTokens() && st.nextToken().equals("void")) && st.hasMoreTokens()) {
								String funcName = extern.substring(extern.indexOf("void") + 4).trim();
								int indexOfParan = funcName.indexOf("(");
								if (indexOfParan != -1) {
									String parameters = funcName.substring(indexOfParan + 1, funcName.lastIndexOf(")")).trim();
									if (parameters.equals("void") || parameters.isEmpty()) {
										String methodeName = funcName.substring(0, indexOfParan);
										if (methodeName.endsWith("_init"))
											initMethodName = methodeName;
										else if (methodeName.endsWith("_birth"))
											birthMethodName = methodeName;
										else if (methodeName.endsWith("_process"))
											processMethodName = methodeName;
										else if (methodeName.endsWith("_death"))
											deathMethodName = methodeName;
										else if (methodeName.endsWith("_destroy"))
											destroyMethodName = methodeName;
									}
								}
							}
						}
					}
					extern = null;
				} else
					extern.append(line);
			}
		}

		CStructDescriptor inputs = null;
		CStructDescriptor outputs = null;
		CStructDescriptor parameters = null;
		for (CStructDescriptor struct : structs)
			if (struct.structName.endsWith("_inputs")) {
				for (Variable variable : externVariable)
					if (variable.type.equals(struct.structName))
						inputs = new CStructDescriptor(struct.structName, variable.name, struct.params);
			} else if (struct.structName.endsWith("_outputs")) {
				for (Variable variable : externVariable)
					if (variable.type.equals(struct.structName))
						outputs = new CStructDescriptor(struct.structName, variable.name, struct.params);
			} else if (struct.structName.endsWith("_properties"))
				for (Variable variable : externVariable)
					if (variable.type.equals(struct.structName))
						parameters = new CStructDescriptor(struct.structName, variable.name, struct.params);
		if (inputs == null) {
			System.err.println("cannot find inputs in header file: " + header);
			return null;
		}
		if (outputs == null) {
			System.err.println("cannot find outputs in header file: " + header);
			return null;
		}
		if (parameters == null) {
			System.err.println("cannot find parameters in header file: " + header);
			return null;
		}
		for (CStructDescriptor cStruct : List.of(inputs, outputs, parameters))
			for (PrimitiveVariable var : cStruct.params)
				if (var.type.isArray())
					for (PrimitiveVariable ovar : cStruct.params)
						if (ovar.type == int.class && ovar.name.equals(var.name + "_length")) {
							ovar.setAsArraySize();
							var.setAsDynamicArray();
							break;
						}

		// les char* sans taille deviennent des String
		for (PrimitiveVariable pv : parameters.params)
			if (pv.type == byte[].class) {
				String pvLengthVariableName = pv.name + "_length";
				boolean hasLengthVariable = false;
				for (PrimitiveVariable otherPv : parameters.params)
					if (pv != otherPv && otherPv.type == int.class && otherPv.name.equals(pvLengthVariableName)) {
						hasLengthVariable = true;
						break;
					}
				if (!hasLengthVariable) {
					pv.type = String.class;
					pv.size = PrimitiveVariable.NotAnArray;
				}
			}

		return new IOPDescriptor(initMethodName, birthMethodName, processMethodName, deathMethodName, destroyMethodName, inputs, outputs, parameters);
	}

	private static Class<?> getJavaTypeFromCType(String cType) {
		cType = cType.replaceAll(" ", "");
		switch (cType) {
		case "char":
			return byte.class;
		case "short":
			return short.class;
		case "wchar_t":
			return char.class;
		case "int":
			return int.class;
		case "long":
			return int.class;
		case "longlong":
			return long.class;
		case "float":
			return float.class;
		case "double":
			return double.class;
		case "short*":
			return short[].class;
		case "int*":
			return int[].class;
		case "long*":
			return int[].class;
		case "longlong*":
			return long[].class;
		case "float*":
			return float[].class;
		case "double*":
			return double[].class;
		case "char*":
			return byte[].class; // String.class
		default:
			return null;
		}
	}

	private static Object getArrayGetter(Class<?> componentType) {
		if (componentType == byte.class)
			return "getByteArray";
		else if (componentType == short.class)
			return "getShortArray";
		else if (componentType == int.class)
			return "getIntArray";
		else if (componentType == long.class)
			return "getLongArray";
		else if (componentType == float.class)
			return "getFloatArray";
		else if (componentType == double.class)
			return "getDoubleArray";
		return null;
	}

	private static String getCTypeFromJavaType(Class<?> type, int size) {
		if (type == byte.class)
			return "char";
		else if (type == short.class)
			return "short";
		else if (type == char.class)
			return "wchar_t";
		else if (type == int.class)
			return "int";
		else if (type == long.class)
			return "long long";
		else if (type == float.class)
			return "float";
		else if (type == double.class)
			return "double";
		else if (type == String.class) // String.class
			return "char*";
		else if (type.isArray()) {
			if (size == -3)
				return "Pointer";
			else if (type == byte[].class)
				return "com.sun.jna.ptr.ByteByReference";
			else if (type == short[].class)
				return "com.sun.jna.ptr.ShortByReference";
			else if (type == int[].class)
				return "com.sun.jna.ptr.IntByReference";
			else if (type == long[].class)
				return "com.sun.jna.ptr.LongByReference";
			else if (type == float[].class)
				return "com.sun.jna.ptr.FloatByReference";
			else if (type == double[].class)
				return "com.sun.jna.ptr.DoubleByReference";
			else
				return null;
		} else
			return type.toString();
	}

	private static int getTypeSize(Class<?> type) {
		if (type == byte.class)
			return Byte.SIZE;
		else if (type == short.class)
			return Short.SIZE;
		else if (type == int.class)
			return Integer.SIZE;
		else if (type == long.class)
			return Long.SIZE;
		else if (type == float.class)
			return Float.SIZE;
		else if (type == double.class)
			return Double.SIZE;
		return -1;
	}

	private static Object getWrapperName(Class<?> componentType) {
		if (componentType == byte.class)
			return Byte.class.getCanonicalName();
		else if (componentType == short.class)
			return Short.class.getCanonicalName();
		else if (componentType == char.class)
			return Character.class.getCanonicalName();
		else if (componentType == int.class)
			return Integer.class.getCanonicalName();
		else if (componentType == long.class)
			return Long.class.getCanonicalName();
		else if (componentType == float.class)
			return Float.class.getCanonicalName();
		else if (componentType == double.class)
			return Double.class.getCanonicalName();
		return null;
	}

	public void process() {
		if (lib == null)
			return;
		Object[] addInput = getAdditionalInputs();
		if (addInput == null)
			return;
		ArrayList<PrimitiveVariable> inParams = iop.inputs.params;
		int indexInput = 0;
		for (int i = 0; i < inParams.size(); i++) {
			PrimitiveVariable var = inParams.get(i);
			if (var.size == PrimitiveVariable.DynamicArraySize)
				continue;
			// if(indexInput == 0)
			// in.write(indexInput, new float[] {0, 0, 0, 0, 0.1f, 0, 0.1f, 0, 0});
			// else
			Object val = addInput[indexInput];
			if (val instanceof Boolean)
				val = ((Boolean) val == true) ? 1 : 0;
			in.write(indexInput++, val);
		}
		in.write();
		processMethod.invokeVoid(null);
		out.read();
		int indexOutput = 0;
		Object[] outputs = new Object[getNbOutput()];
		long[] ts = new long[outputs.length];
		long maxTs = getMaxTimeStamp();
		ArrayList<PrimitiveVariable> outParams = iop.outputs.params;
		boolean isOutput = false;
		for (int i = 0; i < outParams.size(); i++) {
			PrimitiveVariable var = outParams.get(i);
			if (var.size == PrimitiveVariable.DynamicArraySize)
				continue;
			Object output = out.read(indexOutput);
			if (output != null) {
				if (var.name.endsWith("_boolean") && output instanceof Integer)
					output = (Integer) output != 0;
				outputs[indexOutput] = output;
				ts[indexOutput] = maxTs;
				isOutput = true;
			}
			indexOutput++;
		}
		if (isOutput)
			triggerOutput(outputs, ts);
	}

	public void setHeader(File header) {
		dispose();
		this.header = header;
		try {
			initStruct();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void setLibrary(File library) {
		dispose();
		this.library = library;
		try {
			initStruct();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void setParameters(JNAStructure parameters) {
		this.parameters = parameters;
		if (parameters != null) {
			parameters.setAutoRead(false);
			parameters.setAutoWrite(false);
			parameters.setAutoSynch(false);
			try {
				parameters.getClass().getMethod("setPointer", Pointer.class).invoke(parameters, lib.getGlobalVariableAddress(iop.parameters.variablename));
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
				e.printStackTrace();
			}
			parameters.write();
		}
	}
}
