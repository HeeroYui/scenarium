/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.dataprocessing;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;

import net.objecthunter.exp4j.ExpressionBuilder;
import net.objecthunter.exp4j.function.Function;
import net.objecthunter.exp4j.operator.Operator;
import net.objecthunter.exp4j.tokenizer.UnknownFunctionOrVariableException;

public class Expression extends EvolvedOperator {
	private static final ArrayList<Function> functions = new ArrayList<>(Arrays.asList(new Function("atan2", 2) {

		@Override
		public double apply(double... arg0) {
			return Math.atan2(arg0[0], arg0[1]);
		}

	}, new Function("min", 2) {

		@Override
		public double apply(double... arg0) {
			return Math.min(arg0[0], arg0[1]);
		}

	}, new Function("max", 2) {

		@Override
		public double apply(double... arg0) {
			return Math.max(arg0[0], arg0[1]);
		}

	}));

	private static final ArrayList<Operator> operators = new ArrayList<>(Arrays.asList(new Operator("<", 2, true, 0) {

		@Override
		public double apply(double... arg0) {
			return arg0[0] < arg0[1] ? 1 : 0;
		}

	}, new Operator(">", 2, true, 0) {

		@Override
		public double apply(double... arg0) {
			return arg0[0] > arg0[1] ? 1 : 0;
		}

	}, new Operator("<=", 2, true, 0) {

		@Override
		public double apply(double... arg0) {
			return arg0[0] <= arg0[1] ? 1 : 0;
		}

	}, new Operator(">=", 2, true, 0) {

		@Override
		public double apply(double... arg0) {
			return arg0[0] >= arg0[1] ? 1 : 0;
		}

	}, new Operator("!", 1, true, 0) {

		@Override
		public double apply(double... arg0) {
			return arg0[0] == 0 ? 1 : 0;
		}

	}));

	private static ExpressionBuilder getExpressionBuilder(String expression) {
		return new ExpressionBuilder(expression).operator(operators).functions(functions);
	}

	private static String[] getVariables(String expression) {
		HashSet<String> variables = new HashSet<>();
		ArrayList<String> variablesList = new ArrayList<>();
		ExpressionBuilder e2 = getExpressionBuilder(expression);// new ExpressionBuilder(expression).operator(operators).functions(functions);
		while (true) {
			e2.variables(variables);
			try {
				e2.build();
				return variablesList.toArray(new String[variablesList.size()]);
			} catch (UnknownFunctionOrVariableException exc) {
				variables.add(exc.getToken());
				variablesList.add(exc.getToken());
			}
		}
	}

	private String expression;
	private String[] variables;

	private net.objecthunter.exp4j.Expression exp;

	private Double[] tempInputs;

	@Override
	public void birth() throws Exception {
		tempInputs = new Double[getNbInput()];
	}

	@Override
	public void death() throws Exception {
		tempInputs = null;
	}

	public String getExpression() {
		return expression;
	}

	@Override
	public void initStruct() {
		if (exp != null) {
			Class<?>[] types = new Class<?>[variables.length];
			Arrays.fill(types, Double.class);
			updateInputs(variables, types);
		} else
			updateInputs(new String[0], new Class<?>[0]);
	}

	public Double process() {
		if (exp == null)
			return null;
		Object[] inputs = getAdditionalInputs();
		if (inputs.length != tempInputs.length)
			tempInputs = new Double[getNbInput()];
		boolean isFull = true;
		for (int i = 0; i < tempInputs.length; i++)
			if (inputs[i] != null)
				tempInputs[i] = (Double) inputs[i];
			else if (tempInputs[i] == null)
				isFull = false;

		if (isFull) {
			for (int i = 0; i < variables.length; i++)
				exp.setVariable(variables[i], tempInputs[i]);
			return exp.evaluate();
		}
		return null;
	}

	public void setExpression(String expression) {
		this.expression = expression;
		if (expression != null && !expression.isEmpty())
			try {
				variables = getVariables(expression);
				exp = getExpressionBuilder(expression).variables(new HashSet<>(Arrays.asList(variables))).build();
				setWarning(null);
				initStruct();
				return;
			} catch (Exception e) {
				setWarning(e.getMessage());
			}
		initStruct();
		variables = null;
		exp = null;
	}
}
