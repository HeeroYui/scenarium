/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.dataprocessing;

import org.beanmanager.editors.PropertyInfo;
import org.beanmanager.editors.container.BeanInfo;
import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;
import org.scenarium.math.noise.PerlinNoise1D;

public class PerlinNoise extends EvolvedOperator {
	@PropertyInfo(nullable = false)
	@BeanInfo(alwaysExtend = true, inline = true)
	private PerlinNoise1D perlinNoise = new PerlinNoise1D(1, 1, 2, 6, 0.2, 0);
	private long firstT;

	@Override
	public void birth() {
		firstT = -1;
	}

	@Override
	public void death() {}

	public Double process(Double value) {
		long t = getTimeStamp(0);
		if (firstT == -1)
			firstT = t;
		return value + perlinNoise.getValue((t - firstT) / 1000.0);
	}
	
	public PerlinNoise1D getPerlinNoise() {
		return perlinNoise;
	}

	public void setPerlinNoise(PerlinNoise1D perlinNoise) {
		this.perlinNoise = perlinNoise;
	}
}
