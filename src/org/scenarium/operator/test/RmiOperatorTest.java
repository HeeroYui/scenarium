/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.test;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;

public class RmiOperatorTest extends EvolvedOperator {
	private int patate;
	private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		// System.out.println("addPropertyChangeListener from " + ProcessHandle.current().pid());
		pcs.addPropertyChangeListener(listener);
	}

	@Override
	public void birth() {
		System.out.println("birth, pid: " + ProcessHandle.current().pid() + " patate: " + patate);
		setPatate(8);
	}

	@Override
	public void death() {
		System.out.println("death, pid: " + ProcessHandle.current().pid() + " patate: " + patate);
	}

	public int getPatate() {
		return patate;
	}

	public Integer process(Byte i, Byte j, Byte... k) {
		// System.out.println("TimeStamp: " + getTimeStamp(0));
		// System.out.println("process, pid: " + ProcessHandle.current().pid() + " patate: " + patate);
		int res = i + j;
		for (int l = 0; l < k.length; l++)
			res += k[l] == null ? 0 : k[l];
		return res;
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		pcs.removePropertyChangeListener(listener);
	}

	public void setPatate(int patate) {
		// System.out.println("setPatate, pid: " + ProcessHandle.current().pid() + " patate: " + patate);
		int oldPatate = this.patate;
		this.patate = patate;
		pcs.firePropertyChange("Patate", oldPatate, patate);
	}
}
