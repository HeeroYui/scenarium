/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.test;

public class CompA {
	static Integer res = Integer.valueOf(0);

	public void birth() {

	}

	public void death() {

	}

	public Integer process(Integer d) {
		return res;
	}
}
