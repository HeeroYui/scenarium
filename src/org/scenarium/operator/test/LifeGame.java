/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.test;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;

public class LifeGame {
	private BufferedImage[] lifeRaster;
	private int width = 300;
	private int height = 300;
	private BufferedImage nextRaster;
	private BufferedImage currentRaster;
	private double ratio = 0.5;
	private boolean hasChanged;
	private double patate;

	public void birth() {
		hasChanged = false;
		lifeRaster = new BufferedImage[2];
		lifeRaster[0] = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
		lifeRaster[1] = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
		BufferedImage raster = lifeRaster[0];
		byte[] bytes = ((DataBufferByte) raster.getRaster().getDataBuffer()).getData();
		for (int i = 0; i < bytes.length; i++)
			bytes[i] = (byte) (Math.random() < ratio ? 0 : 255);
		currentRaster = null;
	}

	public void death() {}

	public int getHeight() {
		return height;
	}

	public double getPatate() {
		return patate;
	}

	public double getRatio() {
		return ratio;
	}

	public int getWidth() {
		return width;
	}

	private int isAlive(byte[] bytesCurr, int x, int y) {
		if (x >= 0 && x < width && y >= 0 && y < height)
			return bytesCurr[x + y * width] != 0 ? 1 : 0;
		return 0;
	}

	public BufferedImage process(BufferedImage raster) {
		if (hasChanged)
			birth();
		boolean isFirstProcess = currentRaster == null;
		currentRaster = lifeRaster[0] == currentRaster ? lifeRaster[1] : lifeRaster[0];
		nextRaster = currentRaster == lifeRaster[0] ? lifeRaster[1] : lifeRaster[0];
		byte[] bytesCurr = ((DataBufferByte) currentRaster.getRaster().getDataBuffer()).getData();
		byte[] bytesNext = ((DataBufferByte) nextRaster.getRaster().getDataBuffer()).getData();
		if (isFirstProcess) {
			for (int i = 0; i < bytesCurr.length; i++)
				bytesNext[i] = bytesCurr[i];
			return currentRaster;
		}
		for (int x = 0; x < width; x++)
			for (int y = 0; y < height; y++) {
				int nbrNeighbours = isAlive(bytesCurr, x - 1, y - 1) + isAlive(bytesCurr, x - 1, y + 1) + isAlive(bytesCurr, x + 1, y - 1) + isAlive(bytesCurr, x + 1, y + 1) + isAlive(bytesCurr, x, y + 1) + isAlive(bytesCurr, x + 1, y)
						+ isAlive(bytesCurr, x - 1, y) + isAlive(bytesCurr, x, y - 1);
				if (nbrNeighbours == 2)
					bytesNext[x + y * width] = bytesCurr[x + y * width];// nextRaster.set(x, y, 0, currentRaster.get(x, y, 0));
				else if (nbrNeighbours == 3)
					bytesNext[x + y * width] = (byte) 255;// nextRaster.set(x, y, 0, (byte) 255);
				else if (nbrNeighbours < 2)
					bytesNext[x + y * width] = 0;// nextRaster.set(x, y, 0, (byte) 0);
				else if (nbrNeighbours > 3)
					bytesNext[x + y * width] = 0;// nextRaster.set(x, y, 0, (byte) 0);
			}
		return nextRaster;
	}

	public void setHeight(int height) {
		this.height = height;
		hasChanged = true;
	}

	public void setPatate(double patate) {
		this.patate = patate;
	}

	public void setRatio(double ratio) {
		this.ratio = ratio;
		hasChanged = true;
	}

	public void setWidth(int width) {
		this.width = width;
		hasChanged = true;
	}
}
