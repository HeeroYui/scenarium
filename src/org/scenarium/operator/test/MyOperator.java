/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.test;

import java.awt.image.BufferedImage;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Arrays;

import org.beanmanager.BeanDesc;
import org.beanmanager.BeanRenameListener;
import org.beanmanager.editors.PropertyInfo;
import org.beanmanager.editors.container.BeanInfo;
import org.beanmanager.editors.primitive.number.ControlType;
import org.beanmanager.editors.primitive.number.NumberInfo;
import org.beanmanager.struct.TreeRoot;
import org.scenarium.communication.can.CanTrame;
import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;
import org.scenarium.filemanager.scenario.dataflowdiagram.VarArgsInputChangeListener;
import org.scenarium.filemanager.scenario.dataflowdiagram.operator.DeclaredInputChangeListener;
import org.scenarium.filemanager.scenario.dataflowdiagram.operator.DeclaredOutputChangeListener;
import org.scenarium.filemanager.scenariomanager.StructChangeListener;

public class MyOperator extends EvolvedOperator implements BeanRenameListener, StructChangeListener, DeclaredInputChangeListener, DeclaredOutputChangeListener, VarArgsInputChangeListener {
	private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);
	@PropertyInfo(info = "cool")
	@NumberInfo(controlType = ControlType.SPINNER, min = 0)
	private int nbIn = 1;
	private SubMyOp sub = new SubMyOp();

	private int patate = 1;
	private int prop0 = 7;
	private Legume choix;
	@BeanInfo(possibleSubclasses = { PropertySet1.class, PropertySet2.class })
	private PropertySet set;
	@BeanInfo(possibleSubclasses = { PropertySet1.class, PropertySet2.class })
	private PropertySet set2;
	private int cpt;
	@BeanInfo(possibleSubclasses = { PropertySet1.class, PropertySet2.class })
	private PropertySet[][] setArray;
	@BeanInfo(possibleSubclasses = { PropertySet1.class, PropertySet2.class })
	private TreeRoot<PropertySet> tree;

	public void addPropertyChangeListener(PropertyChangeListener listener) {
		pcs.addPropertyChangeListener(listener);
	}

	@Override
	public void beanRename(BeanDesc<?> oldBeanDesc, BeanDesc<?> beanDesc) {
		System.out.println("new name: " + beanDesc.name);
	}

	@Override
	public void birth() {
//		System.out.println("je birth");
		addBlockNameChangeListener(this);
		addStructChangeListener(this);
		addDeclaredInputChangeListener(this);
		addDeclaredOutputChangeListener(this);
		addVarArgsInputChangeListener(this);
		cpt = 0;
	}

	@Override
	public void death() {
		removeBlockNameChangeListener(this);
	}

	@Override
	public void declaredInputChanged(String[] names, Class<?>[] types) {
		System.out.println("declaredInputChanged: " + Arrays.toString(names) + " " + Arrays.toString(types));
	}

	@Override
	public void declaredOutputChanged(String[] names, Class<?>[] types) {
		System.out.println("declaredOutputChanged: " + Arrays.toString(names) + " " + Arrays.toString(types));
	}

	public Legume getChoix() {
		return choix;
	}

	public int getNbIn() {
		return nbIn;
	}

	public String getOutputCarotte() {
		return new String("carotte");
	}

	public int getPatate() {
		return patate;
	}

	public int getProp0() {
		return prop0;
	}

	public PropertySet getSet() {
		return set;
	}

	public PropertySet getSet2() {
		return set2;
	}

	public SubMyOp getSub() {
		return sub;
	}

	@Override
	public void initStruct() {
		String[] names = new String[nbIn];
		Class<?>[] types = new Class<?>[nbIn];
		for (int i = 0; i < types.length; i++) {
			names[i] = "addIn-" + i;
			types[i] = Double.class;
		}
		updateInputs(names, types);
		types = new Class<?>[prop0];
		names = new String[types.length];
		for (int i = 0; i < types.length; i++) {
			names[i] = "addOut-" + i;
			types[i] = Double.class;
		}
		updateOutputs(names, types);
	}

	public Double process(Integer patate, CanTrame canTrame, BufferedImage img, Integer... ours) {
		// System.out.println(this);
		// System.out.println(canTrame);
		// cpt++;
		// if (cpt == 5)
		// initStruct();
		// if (cpt % 20 == 0) {
		// removeBlockNameChangeListener(this);
		// removeStructChangeListener(this);
		// removeDeclaredInputChangeListener(this);
		// removeDeclaredOutputChangeListener(this);
		// removeVarArgsInputChangeListener(this);
		// System.out.println("setPropertyAsInput: " + setPropertyAsInput("nbIn", true));
		// setDefaulting(true);
		// setWarning("warning ok");
		setPatate(cpt);
		if (set != null)
			set.setX(-cpt);

		// SubMyOp newsub = new SubMyOp();
		// newsub.setVache(18);
		// setSub(newsub);
		// } else if (cpt % 30 == 0) {
		// System.out.println("setPropertyAsInput: " + setPropertyAsInput("nbIn", false));
		// setDefaulting(false);
		// setWarning(null);
		// }
		// System.out.println("exclusiveObjectInput: " + isExclusiveObjectInput());
		// System.out.println("in: " + patate + " varargs: " + Arrays.toString(ours) + " addIn: " + Arrays.toString(getAdditionalInputs()));
		// System.out.println("ts of first: " + getTimeStamp(0));
		// System.out.println("toi of first: " + getTimeOfIssue(0));
		// System.out.println("max ts: " + getMaxTimeStamp());
		// System.out.println("block name: " + getBlockName());
		// System.out.println("outputIndex: " + getOutputIndex("Carotte"));
		// System.out.println("inputIndex: " + getInputIndex("patate"));
		// System.out.println("inputsName: " + Arrays.toString(getInputsName()));
		// System.out.println("outputsName: " + Arrays.toString(getOutputsName()));
		// System.out.println("nbInput: " + getNbInput());
		// System.out.println("nbOutput: " + getNbOutput());
		// System.out.println("outputLinkToInputName: " + Arrays.toString(getOutputLinkToInputName()));
		// System.out.println("outputLinkToInputType: " + Arrays.toString(getOutputLinkToInputType()));
		// System.out.println("isPropertyAsInput: " + isPropertyAsInput("nbIn"));
		// setPropertyAsInput("nbIn", false);
		// new Thread(() -> {
		// System.out.println("start run later from other thread");
		// runLater(() -> System.out.println("run later from other thread ok"));
		// }).start();
		// runLater(() -> System.out.println("run later from process thread ok"));

		// System.out.println(getBlockName() + ": " + this);
		double sum = 0;
		Object[] addInputs = getAdditionalInputs();
		if (addInputs != null)
			for (Object val : addInputs)
				if (val != null)
					sum += (Double) val;
		if (ours != null)
			for (Integer val : ours)
				if (val != null)
					sum += val;
		if (patate != null)
			sum += patate;
		cpt++;
		if (cpt % 10000 == 0)
			System.out.println(cpt);
		Object[] outVec = generateOuputsVector();
		outVec[2] = Double.valueOf(cpt);
		triggerOutput(outVec, -1);
		return sum;
	}

	public void removePropertyChangeListener(PropertyChangeListener listener) {
		pcs.removePropertyChangeListener(listener);
	}

	public void setChoix(Legume choix) {
		this.choix = choix;
	}

	public void setNbIn(int nbIn) {
		this.nbIn = nbIn;
		initStruct();
		// runLater(() -> {
		// initStruct();
		// });
	}

	public void setPatate(int patate) {
		int oldPatate = this.patate;
		this.patate = patate;
		pcs.firePropertyChange("patate", oldPatate, patate);
	}

	public void setProp0(int prop0) {
		this.prop0 = prop0;
		initStruct();
	}

	public void setSet(PropertySet set) {
		PropertySet oldValue = this.set;
		this.set = set;
		pcs.firePropertyChange("set", oldValue, set);
	}

	public void setSet2(PropertySet set2) {
		PropertySet oldValue = this.set2;
		this.set2 = set2;
		pcs.firePropertyChange("set2", oldValue, set2);
	}

	public void setSub(SubMyOp sub) {
		SubMyOp oldValue = this.sub;
		this.sub = sub;
		pcs.firePropertyChange("sub", oldValue, sub);
	}

//	public PropertySet[][] getSetArray() {
//		return setArray;
//	}
//
//	public void setSetArray(PropertySet[][] setArray) {
//		this.setArray = setArray;
//	}

	@Override
	public void structChanged() {
		System.out.println("structChanged");
	}

	@Override
	public String toString() {
		return "MyOperator [nbIn=" + nbIn + ", sub=" + sub + ", patate=" + patate + ", prop0=" + prop0 + ", choix=" + choix + ", set=" + set + ", cpt=" + cpt + "]";
	}

	@Override
	public void varArgsInputChanged(int indexOfInput, int typeOfChange) {
		System.out.println("varArgsInputChanged: " + indexOfInput + " " + typeOfChange);
	}

	public TreeRoot<PropertySet> getTree() {
		return tree;
	}

	public void setTree(TreeRoot<PropertySet> tree) {
		this.tree = tree;
	}
}