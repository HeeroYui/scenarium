/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.test;

import java.io.IOException;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;

import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;
import org.scenarium.struct.raster.ByteRaster;
import org.scenarium.struct.raster.Raster;

public class RasterGenerator extends EvolvedOperator{
	private int width;
	private int height;	
	private Timer timer;

	public void birth() throws IOException {
		onStart(() -> {
			timer = new Timer();
			timer.scheduleAtFixedRate(new TimerTask() {
				
				@Override
				public void run() {
					byte[] data = new byte[width * height * 3];
					Arrays.fill(data, (byte)255);
					triggerOutput(new ByteRaster(width, height, Raster.BGR, data));
				}
			}, 1000, 1000);
		});
	}
	
	public ByteRaster process() {
		return null;
	}

	public void death() {
		if(timer != null) {
			timer.cancel();
			timer.purge();
			timer = null;
		}
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}
}
