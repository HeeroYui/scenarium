/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.test;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.locks.ReentrantLock;

import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;

public class B1 extends EvolvedOperator {
	private Timer timer;
	private ReentrantLock lock;

	@Override
	public void birth() {
		timer = new Timer();
		lock = new ReentrantLock();
		triggerOutput(new Object[] { new byte[] { 1, 2, 3, 4, 5 } }, new long[] { System.currentTimeMillis() });
		timer.scheduleAtFixedRate(new TimerTask() {

			@Override
			public void run() {
				lock.lock();
				try {
					triggerOutput(new Object[] { new byte[] { 1, 2, 3, 4, 5 } }, new long[] { System.currentTimeMillis() });
				} finally {
					lock.unlock();
				}
			}
		}, 0, 1);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			System.out.println("le block " + getBlockName() + " est interrompu");
			// while(true);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

	@Override
	public void death() {
		lock.lock();
		try {
			timer.cancel();
			timer = null;
		} finally {
			lock.unlock();
		}
		try {
			System.out.println("begin wait");
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			System.out.println("le block " + getBlockName() + " est interrompu");
			// while(true);
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

	public int getOutputA() {
		return 6;
	}

	public byte[] process() {
		return null;
	}
}
