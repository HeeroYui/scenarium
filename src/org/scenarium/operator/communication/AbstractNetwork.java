/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.communication;

import org.beanmanager.BeanDesc;
import org.beanmanager.BeanPropertiesInheritanceLimit;
import org.beanmanager.BeanRenameListener;
import org.beanmanager.editors.PropertyInfo;
import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;
import org.scenarium.operator.communication.serial.DataType;

@BeanPropertiesInheritanceLimit
public abstract class AbstractNetwork extends EvolvedOperator implements BeanRenameListener {
	@PropertyInfo(index = 100, info = "Specify the data input type")
	protected DataType inDataType = DataType.BYTE;
	@PropertyInfo(index = 101, info = "Specify the data output type")
	protected DataType outDataType = DataType.BYTE;
	@PropertyInfo(index = 102, info = "Generate an output that send true when the connection is established and false when the connection is lost")
	private boolean connectionFlag = false;
	@PropertyInfo(index = 103, info = "Explicitly displays in the console all operations performed")
	protected boolean verbose = false;

	private Object[] outputValues;
	private long[] timeStamps;

	public AbstractNetwork() {}

	public AbstractNetwork(DataType inDataType, DataType outDataType, boolean connectionFlag, boolean verbose) {
		this.inDataType = inDataType;
		this.outDataType = outDataType;
		this.connectionFlag = connectionFlag;
		this.verbose = verbose;
	}

	protected void connectionEstablished(boolean established) {
		if (verbose && established)
			System.out.println(getBlockName() + ": Connection " + (established ? "established" : "lost"));
		if (connectionFlag) {
			int flagIndex = getOutputIndex("flag");
			if (flagIndex != -1) {
				for (int i = 0; i < outputValues.length; i++)
					outputValues[i] = null;
				outputValues[flagIndex] = established;
				timeStamps[flagIndex] = System.currentTimeMillis();
				triggerOutput(outputValues, timeStamps);
				outputValues[flagIndex] = null;
			}
		}
	}

	public DataType getInDataType() {
		return inDataType;
	}

	public DataType getOutDataType() {
		return outDataType;
	}

	@Override
	public void initStruct() {
		if (inDataType != DataType.DISABLE && isInputAvailable())
			updateInputs(new String[] { "in" }, new Class[] { inDataType == DataType.BYTE ? byte[].class : inDataType == DataType.STRING ? String.class : Object.class });
		else
			updateInputs(new String[0], new Class[0]);

		int nbOutput;
		if (outDataType != DataType.DISABLE && isOutputAvailable()) {
			if (connectionFlag) {
				updateOutputs(new String[] { getBlockName(), "flag" }, new Class[] { outDataType == DataType.BYTE ? byte[].class : outDataType == DataType.STRING ? String.class : Object.class, Boolean.class });
				nbOutput = 2;
			} else {
				updateOutputs(new String[] { getBlockName() }, new Class[] { outDataType == DataType.BYTE ? byte[].class : outDataType == DataType.STRING ? String.class : Object.class });
				nbOutput = 1;
			}
		} else if (connectionFlag) {
			updateOutputs(new String[] { "flag" }, new Class[] { Boolean.class });
			nbOutput = 1;
		} else {
			updateOutputs(new String[0], new Class[0]);
			nbOutput = 0;
		}
		if (nbOutput != 0) {
			outputValues = new Object[nbOutput];
			timeStamps = new long[nbOutput];
		}
		removeBlockNameChangeListener(this);
		addBlockNameChangeListener(this);
	}

	public boolean isConnectionFlag() {
		return connectionFlag;
	}

	protected boolean isInputAvailable() {
		return true;
	}

	protected boolean isOutputAvailable() {
		return true;
	}

	public boolean isVerbose() {
		return verbose;
	}

	public void setConnectionFlag(boolean connectionFlag) {
		this.connectionFlag = connectionFlag;
		restartAndReloadStructLater();
	}

	public void setInDataType(DataType inDataType) {
		this.inDataType = inDataType;
		restartAndReloadStructLater();
	}

	public void setOutDataType(DataType outDataType) {
		this.outDataType = outDataType;
		restartAndReloadStructLater();
	}

	public void setVerbose(boolean verbose) {
		this.verbose = verbose;
	}

	protected void triggerData(Object data) {
		outputValues[0] = data;
		timeStamps[0] = System.currentTimeMillis();
		triggerOutput(outputValues, timeStamps);
	}

	@Override
	public void beanRename(BeanDesc<?> oldBeanDesc, BeanDesc<?> beanDesc) {
		initStruct();
	}
}
