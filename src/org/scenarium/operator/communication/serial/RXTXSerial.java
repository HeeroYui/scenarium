/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.communication.serial;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;

import org.beanmanager.BeanDesc;
import org.beanmanager.BeanRenameListener;
import org.beanmanager.editors.DynamicPossibilities;
import org.beanmanager.editors.PropertyInfo;
import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;
import org.scenarium.filemanager.scenario.dataflowdiagram.ParamInfo;

import gnu.io.CommPortIdentifier;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;

public class RXTXSerial extends EvolvedOperator implements BeanRenameListener {
	// private static final String[] RXTXLIB = {/* "rxtxParallel", */"rxtxSerial" };
	// private static boolean isAvailable = true;

	public static void main(String[] args) {
		System.out.println(Arrays.toString(new RXTXSerial().getAvailablePort()));
	}

	@PropertyInfo(index = 0)
	@DynamicPossibilities(possibleChoicesMethod = "getAvailablePort", useSwingWorker = true)
	private String commPort;
	@PropertyInfo(index = 1)
	private int baudrate = 4800;
	@PropertyInfo(index = 2)
	private DataType inDataType = DataType.STRING;
	@PropertyInfo(index = 3)
	private DataType outDataType = DataType.STRING;
	@PropertyInfo(index = 4)
	private RXTXDataBits dataBits = RXTXDataBits.DATABITS_8;
	@PropertyInfo(index = 5)
	private RXTXStopBits stopBits = RXTXStopBits.STOPBITS_1;
	@PropertyInfo(index = 6)
	private RXTXParity parity = RXTXParity.PARITY_NONE;

	@PropertyInfo(index = 7)
	private boolean verbose = false;
	private Thread sor;
	private boolean isRunning = false;
	private SerialPort sp = null;
	private OutputStream os = null;

	private ObjectOutputStream objectOutputStream;

	@Override
	public void birth() throws Exception {
		// if (isAvailable)
		onStart(() -> startReceiver());
		isRunning = true;
	}

	@Override
	public void death() throws Exception {
		isRunning = false;
		stopReceiver();
	}

	public String[] getAvailablePort() {
		Enumeration<?> ports = CommPortIdentifier.getPortIdentifiers();
		ArrayList<CommPortIdentifier> commPorts = new ArrayList<>();
		while (ports.hasMoreElements()) {
			Object e = ports.nextElement();
			if (e instanceof CommPortIdentifier)
				commPorts.add((CommPortIdentifier) e);
		}
		String[] availablePorts = new String[commPorts.size()];
		for (int i = 0; i < commPorts.size(); i++)
			availablePorts[i] = commPorts.get(i).getName();
		return availablePorts;
	}

	public int getBaudrate() {
		return baudrate;
	}

	public String getCommPort() {
		return commPort;
	}

	public RXTXDataBits getDataBits() {
		return dataBits;
	}

	public DataType getInDataType() {
		return inDataType;
	}

	public DataType getOutDataType() {
		return outDataType;
	}

	public RXTXParity getParity() {
		return parity;
	}

	public RXTXStopBits getStopBits() {
		return stopBits;
	}

	@Override
	public void initStruct() {
		if (inDataType != DataType.DISABLE)
			updateInputs(new String[] { "in" }, new Class[] { inDataType == DataType.BYTE ? byte[].class : String.class });
		else
			updateInputs(new String[0], new Class[0]);
		if (outDataType != DataType.DISABLE)
			updateOutputs(new String[] { getBlockName() }, new Class[] { outDataType == DataType.BYTE ? byte[].class : String.class });
		else
			updateOutputs(new String[0], new Class[0]);
		removeBlockNameChangeListener(this);
		addBlockNameChangeListener(this);
	}

	public boolean isVerbose() {
		return verbose;
	}

	@ParamInfo(in = "in", out = "Out")
	public void process() {
		Object in = getAdditionalInputs()[0];
		OutputStream _os = os;
		if (_os != null)
			try {
				if (inDataType == DataType.OBJECT) {
					if (objectOutputStream == null)
						objectOutputStream = new ObjectOutputStream(_os);
					objectOutputStream.writeObject(in);
					objectOutputStream.flush();
				} else if (in instanceof byte[] && inDataType == DataType.BYTE)
					_os.write((byte[]) in);
				else if (in instanceof String && inDataType == DataType.STRING)
					_os.write(((String) in).getBytes());
			} catch (IOException e) {
				if (verbose)
					System.err.println(getBlockName() + ": " + e.getMessage());
				startReceiver();
			}
	}

	public void setBaudrate(int baudrate) {
		this.baudrate = baudrate;
		if (isRunning)
			startReceiver();
	}

	public void setCommPort(String commPort) {
		this.commPort = commPort;
		if (isRunning)
			startReceiver();
	}

	public void setDataBits(RXTXDataBits dataBits) {
		this.dataBits = dataBits;
		if (isRunning)
			startReceiver();
	}

	public void setInDataType(DataType inDataType) {
		this.inDataType = inDataType;
		initStruct();
		restart();
	}

	public void setOutDataType(DataType outDataType) {
		this.outDataType = outDataType;
		initStruct();
		restart();
	}

	public void setParity(RXTXParity parity) {
		this.parity = parity;
		if (isRunning)
			startReceiver();
	}

	public void setStopBits(RXTXStopBits stopBits) {
		this.stopBits = stopBits;
		if (isRunning)
			startReceiver();
	}

	public void setVerbose(boolean verbose) {
		this.verbose = verbose;
	}

	private void startReceiver() {
		stopReceiver();
		sor = new Thread(() -> {
			InputStream is = null;
			while (isRunning) {
				try {
					Enumeration<CommPortIdentifier> ports = CommPortIdentifier.getPortIdentifiers();
					CommPortIdentifier id = null;
					while (ports.hasMoreElements()) {
						CommPortIdentifier e = ports.nextElement();
						if (e.getName().equals(commPort)) {
							id = e;
							break;
						}
					}
					if (id == null) {
						if (verbose)
							System.err.println(getBlockName() + ": The port: " + commPort + " is not available");
						return;
					}
					while (sp == null)
						try {
							synchronized (RXTXSerial.class) {
								sp = (SerialPort) id.open(getBlockName(), 1000);
							}
						} catch (PortInUseException e) {
							if (verbose)
								System.err.println("Port: " + commPort + " is in used");
							Thread.sleep(1000);
						}
					sp.setSerialPortParams(baudrate, dataBits.getValue(), stopBits.getValue(), parity.getValue());
					sp.enableReceiveTimeout(1000);
					os = sp.getOutputStream();
					if (outDataType == DataType.DISABLE)
						return;
					is = sp.getInputStream();
					if (outDataType == DataType.STRING) {
						try (BufferedReader buf = new BufferedReader(new InputStreamReader(sp.getInputStream()))) {
							while (isRunning)
								try {
									triggerOutput(buf.readLine());
								} catch (IOException e) {
									if (verbose)
										System.err.println(getBlockName() + ": no serial data received");
								}
						}
					} else if (outDataType == DataType.BYTE) {
						byte[] readBuff = new byte[40000];
						while (isRunning()) {
							int nbBytes = is.read(readBuff);
							if (nbBytes > 0) {
								byte[] datas = new byte[nbBytes];
								System.arraycopy(readBuff, 0, datas, 0, datas.length);
								triggerOutput(datas);
							}
						}
					} else if (outDataType == DataType.OBJECT) {
						ObjectInputStream objectOutput = new ObjectInputStream(is);
						while (isRunning())
							triggerOutput(objectOutput.readObject());
					}
				} catch (Exception e) {
					if (sp != null) {
						sp.close();
						sp = null;
					}
					if (!(e instanceof InterruptedException)) {
						System.err.println(getBlockName() + ": Error: " + e.getClass().getSimpleName());
						e.printStackTrace();
					}
				}
				if (sp != null) {
					sp.close();
					sp = null;
				}
				if (os != null) {
					try {
						os.close();
					} catch (IOException e) {}
					os = null;
				}
				if (objectOutputStream != null) {
					try {
						objectOutputStream.close();
					} catch (IOException e) {}
					objectOutputStream = null;
				}
				if (is != null)
					try {
						is.close();
					} catch (Exception e) {
						System.err.println(getBlockName() + ": Cannot close the stream: " + e.getClass().getSimpleName() + ": " + e.getMessage());
					}
			}
		});
		sor.start();
	}

	private void stopReceiver() {
		if (sor != null) {
			sor.interrupt();
			try {
				sor.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			sor = null;
		}
	}

	@Override
	public void beanRename(BeanDesc<?> oldBeanDesc, BeanDesc<?> beanDesc) {
		initStruct();
	}
}