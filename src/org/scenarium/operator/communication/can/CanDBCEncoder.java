/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.communication.can;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import org.beanmanager.editors.DynamicChoiceBox;
import org.beanmanager.editors.DynamicEnableBean;
import org.beanmanager.editors.PropertyInfo;
import org.beanmanager.editors.UpdatableViewBean;
import org.beanmanager.editors.container.BeanInfo;
import org.beanmanager.struct.Selection;
import org.scenarium.communication.can.CanTrame;
import org.scenarium.communication.can.LoadedListener;
import org.scenarium.communication.can.dbc.CanDBC;
import org.scenarium.communication.can.dbc.DBCMessage;
import org.scenarium.communication.can.dbc.DBCSignal;
import org.scenarium.communication.can.dbc.EncodingMessageProperties;
import org.scenarium.communication.can.dbc.EncodingSignalProperties;
import org.scenarium.communication.can.dbc.MessageIdentifier;
import org.scenarium.editors.NotChangeableAtRuntime;
import org.scenarium.editors.can.SendMode;
import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;

public class CanDBCEncoder extends EvolvedOperator implements LoadedListener, DynamicEnableBean, UpdatableViewBean {
	// TODO prend pas en compte le changement de timer temps réel d'un signal ou le changement de filters, rien du tout en faite
	@BeanInfo(alwaysExtend = true)
	@PropertyInfo(index = 0, info = "DBC database for encoding datas")
	@NotChangeableAtRuntime
	private CanDBC canDBC;
	@PropertyInfo(index = 1, info = "Input filters based on available signal in the DBC database")
	@DynamicChoiceBox(possibleChoicesMethod = "getVariablesIdentifier")
	private Selection<MessageIdentifier> filters;
	@PropertyInfo(index = 2, info = "Input enum as string or as double value")
	private boolean enumAsString;
	@PropertyInfo(index = 3, info = "Properties for CAN encoding and triggering")
	@DynamicChoiceBox(possibleChoicesMethod = "getPossibleMessages")
	@NotChangeableAtRuntime
	private EncodingMessageProperties[] messagesProperties = null;
	private HashMap<String, int[]> triggerMap = new HashMap<>();
	private HashMap<Integer, InputData> inputOnNewDataMap = new HashMap<>(); // Map index entré vers InputData
	private HashMap<Integer, byte[]> dataMap = new HashMap<>(); // Map id vers data pour external trigget et timer

	private ArrayList<CanTrame> canTramesToTrigger = new ArrayList<>();
	private MessageTimer signalTimer;
	private ArrayList<String> triggers = new ArrayList<>();

	private EncodingMessageProperties[] oldMessagesProperties;

	private void addMessage(EncodingMessageProperties prop) {
		if (prop.getSendMode() == SendMode.TIMER) {
			if (signalTimer == null) {
				signalTimer = new MessageTimer();
				signalTimer.add(prop);
				signalTimer.start();
			} else
				signalTimer.add(prop);
		} else if (prop.getSendMode() == SendMode.ONEXTERNALTRIGGER)
			addToTriggerMap(prop);
	}

	private void addToTriggerMap(EncodingMessageProperties messagesPropertie) {
		String name = messagesPropertie.getAdditionalInfo();
		if (name != null && !name.isEmpty()) {
			int[] triggerList = triggerMap.get(name);
			if (triggerList == null)
				triggerList = new int[] { messagesPropertie.getId() };
			else {
				int[] newTriggerList = new int[triggerList.length + 1];
				System.arraycopy(triggerList, 0, newTriggerList, 0, triggerList.length);
				newTriggerList[triggerList.length] = messagesPropertie.getId();
				triggerList = newTriggerList;
			}
			triggerMap.put(name, triggerList);
		}
		triggerMap.forEach((triggerName, ids) -> System.out.println(triggerName + ": " + Arrays.toString(ids)));
	}

	@Override
	public void birth() {
		launch();
	}

	@Override
	public void death() {
		if (signalTimer != null) {
			signalTimer.stop();
			signalTimer = null;
		}
		dataMap.clear();
		triggerMap.clear();
	}

	public CanDBC getCanDBC() {
		return canDBC;
	}

	public Selection<MessageIdentifier> getFilters() {
		return filters;
	}

	public EncodingMessageProperties[] getMessagesProperties() {
		return messagesProperties;
	}

	public MessageIdentifier[] getPossibleMessages() {
		return filters == null ? null : filters.getSelected().toArray(new MessageIdentifier[filters.getSelected().size()]);
	}

	public Object[] getVariablesIdentifier() {
		return canDBC == null ? new MessageIdentifier[0] : canDBC.getMessagesIdentifier().toArray();
	}

	@Override
	public void initStruct() {
		inputOnNewDataMap.clear();
		ArrayList<String> names = new ArrayList<>();
		ArrayList<Class<?>> types = new ArrayList<>();
		triggers.clear();
		int inputMapIndex = 0;
		if (canDBC != null && messagesProperties != null && messagesProperties.length != 0) {
			canDBC.loadIfNeeded();
			ArrayList<EncodingMessageProperties> selectedOutputs = new ArrayList<>(Arrays.asList(messagesProperties));
			selectedOutputs.sort((a, b) -> Integer.compare(a.getId(), b.getId()));
			for (EncodingMessageProperties emp : selectedOutputs) {
				if (emp.getSendMode() == SendMode.ONEXTERNALTRIGGER && emp.getAdditionalInfo() != null && !triggers.contains(emp.getAdditionalInfo()))
					triggers.add(emp.getAdditionalInfo());
				DBCMessage message = canDBC.getPropById(emp.getId());
				if (message == null || !message.getName().equals(emp.getName()))
					continue;
				boolean[] updatedData = null;
				if (emp.getSendMode() == SendMode.ONNEWSETOFDATAS)
					updatedData = new boolean[message.getSignals().size()];
				for (EncodingSignalProperties esp : emp.getSignalProperties())
					if (esp.isAsInput()) {
						DBCSignal signal = message.getSignal(esp.getName());
						names.add(emp.getId() + "_" + signal.name);
						types.add(enumAsString ? signal.getDataType() : Double.class);
						inputOnNewDataMap.put(inputMapIndex++, new InputData(message.getId(), signal, new byte[message.getSize()], updatedData));
					}
			}
		}
		for (String triggerName : triggers) {
			names.add(triggerName);
			types.add(Boolean.class);
		}
		updateInputs(names.toArray(new String[0]), types.toArray(new Class<?>[0]));
	}

	private void launch() {
		dataMap.clear();
		triggerMap.clear();
		if (signalTimer != null) {
			signalTimer.stop();
			signalTimer = null;
		}
		if (messagesProperties == null)
			return;
		Collection<InputData> inputDatas = inputOnNewDataMap.values();
		for (EncodingMessageProperties emp : messagesProperties) {
			DBCMessage message = canDBC.getPropById(emp.getId());
			byte[] trameData = null;
			for (InputData inputData : inputDatas)
				if (inputData.getId() == message.getId())
					trameData = inputData.getCanData();
			if (trameData == null)
				trameData = new byte[message.getSize()];
			byte[] _trameData = trameData;
			emp.getSignalProperties().forEach(esp -> message.getSignal(esp.getName()).encode(_trameData, esp.getValue())); // Initialisation des valeurs par default
			if (emp.getSendMode() == SendMode.ONEXTERNALTRIGGER)
				addToTriggerMap(emp);
			else if (emp.getSendMode() == SendMode.TIMER) {
				if (signalTimer == null)
					signalTimer = new MessageTimer();
				signalTimer.add(emp);
			}
			dataMap.put(emp.getId(), trameData);
		}
		canTramesToTrigger = new ArrayList<>(messagesProperties.length);
		if (signalTimer != null)
			signalTimer.start();
	}

	@Override
	public void loaded() {
		initStruct();
		// fireStructChanged();
		if (canDBC != null && canDBC.hasMessage() && filters != null) { // || filters.getSelected().isEmpty() si je mets ca pas d'update quad je vide la liste
			ArrayList<Object> toRemove = new ArrayList<>();
			ArrayList<MessageIdentifier> variablesIdentifier = canDBC.getMessagesIdentifier();
			for (MessageIdentifier selection : filters.getSelected())
				if (!variablesIdentifier.contains(selection))
					toRemove.add(selection);
			if (!toRemove.isEmpty()) {
				HashSet<MessageIdentifier> sel = filters.getSelected();
				sel.removeAll(toRemove);
				filters = new Selection<>(sel, MessageIdentifier.class);
			}
			EncodingMessageProperties[] newMessagesProperties = new EncodingMessageProperties[filters.getSelected().size()];
			int index = 0;
			for (MessageIdentifier encodingMessageProperties : filters.getSelected())
				newMessagesProperties[index++] = new EncodingMessageProperties(encodingMessageProperties.getId(), encodingMessageProperties.getName());
			messagesProperties = newMessagesProperties;
		} else
			filters = null;
		setEnable();
		updateView();
	}

	public CanTrame process() {
		Object[] inputs = getAdditionalInputs();
		synchronized (this) {
			for (int i = 0; i < inputs.length; i++) {
				Object input = inputs[i];
				if (input != null)
					if (input instanceof Boolean) { // C'est un trigger, voir la liste pour avoir son index dedans, attention synchro lors du changement
						String trigger = triggers.get(inputs.length - triggers.size() + i);
						for (int id : triggerMap.get(trigger))
							canTramesToTrigger.add(new CanTrame(id, true, dataMap.get(id)));
					} else {
						InputData inputData = inputOnNewDataMap.get(i); // les signaux sont toujours avant et les triggers après, donc pas de problème avec i
						if (inputData != null) { // null si pas dans un mode de trigger sur data
							if (input instanceof Double)
								inputData.getSignal().encode(inputData.getCanData(), (Double) input);
							else
								inputData.getSignal().encodeAsString(inputData.getCanData(), (String) input);
							boolean[] updatedData = inputData.getUpdatedData();
							if (updatedData == null)
								canTramesToTrigger.add(new CanTrame(inputData.getId(), true, inputData.getCanData()));
							else {
								boolean ready = true;
								ArrayList<DBCSignal> signals = canDBC.getPropById(inputData.getId()).getSignals();
								for (int j = 0; j < signals.size(); j++)
									if (inputData.getSignal() == signals.get(j))
										updatedData[j] = true;
									else if (!updatedData[j])
										ready = false;
								if (ready) {
									canTramesToTrigger.add(new CanTrame(inputData.getId(), true, inputData.getCanData()));
									Arrays.fill(updatedData, false);
								}
							}
						}
					}
			}
		}
		canTramesToTrigger.forEach(canTrame -> System.out.println("Trigger output: " + canTrame));
		canTramesToTrigger.forEach(canTrame -> triggerOutput(canTrame));
		canTramesToTrigger.clear();
		return null;
	}

	private void purgeMessage(EncodingMessageProperties prop) {
		if (prop.getSendMode() == SendMode.TIMER && signalTimer.remove(prop))
			signalTimer = null;
		else if (prop.getSendMode() == SendMode.ONEXTERNALTRIGGER)
			removeToTriggerMap(prop);
	}

	private void removeToTriggerMap(EncodingMessageProperties messagesPropertie) {
		ArrayList<String> toRemove = new ArrayList<>();
		triggerMap.forEach((triggerName, ids) -> {
			for (int id : ids)
				if (id == messagesPropertie.getId()) {
					if (ids.length == 1) {
						toRemove.add(triggerName);
						return;
					}
					int[] newTriggerList = new int[ids.length - 1];
					int index = 0;
					for (int i = 0; i < ids.length; i++)
						if (ids[i] != messagesPropertie.getId())
							newTriggerList[index++] = ids[i];
					triggerMap.put(triggerName, newTriggerList);
				}
		});
		toRemove.forEach(e -> triggerMap.remove(e));
		triggerMap.forEach((triggerName, ids) -> System.out.println(triggerName + ": " + Arrays.toString(ids)));
	}

	public void setCanDBC(CanDBC canDBC) {
		this.canDBC = canDBC;
		loaded();
	}

	@Override
	public void setEnable() {
		fireSetPropertyEnable(this, "filters", canDBC != null && canDBC.hasMessage());
		fireSetPropertyEnable(this, "messagesProperties", canDBC != null && canDBC.hasMessage());
	}

	public void setFilters(Selection<MessageIdentifier> filters) {
		this.filters = new Selection<>(canDBC == null || filters == null ? new HashSet<>() : canDBC.getMessagesIdentifier().stream().filter(e -> filters.getSelected().contains(e)).collect(Collectors.toCollection(HashSet::new)),
				MessageIdentifier.class);
		loaded();
	}

	public void setMessagesProperties(EncodingMessageProperties[] messagesProperties) {
		if (isRunning()) {
			ArrayList<EncodingMessageProperties> oldMessages = new ArrayList<>(Arrays.asList(oldMessagesProperties));
			ArrayList<EncodingMessageProperties> messages = new ArrayList<>(Arrays.asList(messagesProperties));
			this.messagesProperties = messagesProperties;
			initStruct();
			for (int i = 0; i < messagesProperties.length; i++)
				for (int j = 0; j < oldMessagesProperties.length; j++) {
					EncodingMessageProperties prop = messagesProperties[i];
					EncodingMessageProperties oldProp = oldMessagesProperties[j];
					if (prop.equals(oldProp)) { // Ok c'est les mêmes avant et après, je regarde les signaux
						oldMessages.remove(oldProp);
						messages.remove(prop);
						ArrayList<EncodingSignalProperties> signalsProp = prop.getSignalProperties();
						ArrayList<EncodingSignalProperties> oldSignalsProp = oldProp.getSignalProperties();
						for (int k = 0; k < signalsProp.size(); k++)
							for (int l = 0; l < oldSignalsProp.size(); l++) {
								EncodingSignalProperties signalProp = signalsProp.get(k);
								EncodingSignalProperties oldSignalProp = oldSignalsProp.get(l);
								if (signalProp.equals(oldSignalProp)) {
									if (signalProp.isAsInput() != oldSignalProp.isAsInput())
										System.out.println(signalProp.getName() + " changed asinput");
									else if (signalProp.getValue() != oldSignalProp.getValue()) {
										canDBC.getPropById(prop.getId()).getSignal(signalProp.getName()).encode(dataMap.get(prop.getId()), signalProp.getValue());
										DBCSignal.showBytes(dataMap.get(prop.getId()));
										System.out.println(signalProp.getName() + " changed value");
									}
									break;
								}
							}
						if (oldProp.getSendMode() != prop.getSendMode()) {
							purgeMessage(oldProp);
							addMessage(prop);
						}
						try {
							if ((prop.getSendMode() == SendMode.TIMER || prop.getSendMode() == SendMode.ONEXTERNALTRIGGER) && (oldProp.getAdditionalInfo() == null || !oldProp.getAdditionalInfo().equals(prop.getAdditionalInfo())))
								if (prop.getSendMode() == SendMode.TIMER)
									signalTimer.changePeriod(prop);
								else if (prop.getSendMode() == SendMode.ONEXTERNALTRIGGER) {
									removeToTriggerMap(prop);
									addToTriggerMap(prop);
									System.out.println("changement de trigger");
								}
						} catch (NullPointerException e) {
							e.printStackTrace();
						}
						break;
					}
				}
			oldMessages.forEach(oldProp -> {
				purgeMessage(oldProp);
				dataMap.remove(oldProp.getId());
			});
			messages.forEach(emp -> {
				addMessage(emp);
				dataMap.put(emp.getId(), new byte[canDBC.getPropById(emp.getId()).getSize()]);
				// Collection<InputData> inputDatas = inputOnNewDataMap.values();
				// DBCMessage message = canDBC.getPropById(emp.getId());
				// byte[] trameData = null;
				// for (InputData inputData : inputDatas)
				// if (inputData.getId() == message.getId())
				// trameData = inputData.getCanData();
				// if (trameData == null)
				// trameData = new byte[message.getSize()];
			});
			if (!oldMessages.isEmpty())
				System.out.println("old remove message: " + oldMessages.toString());
			if (!messages.isEmpty())
				System.out.println("new add message: " + messages.toString());
		} else {
			ArrayList<EncodingMessageProperties> filteredEMP = new ArrayList<>();
			for (EncodingMessageProperties encodingMessageProperties : messagesProperties)
				if (canDBC.getPropById(encodingMessageProperties.getId()) != null)
					filteredEMP.add(encodingMessageProperties);
			this.messagesProperties = filteredEMP.size() == messagesProperties.length ? messagesProperties : filteredEMP.toArray(new EncodingMessageProperties[filteredEMP.size()]);
			initStruct();
		}
		oldMessagesProperties = Arrays.stream(messagesProperties).map(e -> e.clone()).toArray(EncodingMessageProperties[]::new);
	}

	@Override
	public void updateView() {
		fireUpdateView(this, "filters", false);
		fireUpdateView(this, "messagesProperties", false);
	}
	
	class MessageTimer {
		class MessageTimerInfo {
			private int nextRun;
			private final int period;
			private final ArrayList<DBCMessage> messages;

			public MessageTimerInfo(int period) {
				this.period = period;
				this.messages = new ArrayList<>();
				this.nextRun = period;
			}

			public void addMessage(DBCMessage message) {
				messages.add(message);
			}

			public ArrayList<DBCMessage> getMessages() {
				return messages;
			}

			public int getNextRun() {
				return nextRun;
			}

			public int getPeriod() {
				return period;
			}

			public void incNextRun() {
				this.nextRun += period;
			}

			public void incNextRun(long nbPeriod) {
				this.nextRun += nbPeriod * period;
			}

			@Override
			public String toString() {
				return "nextRun: " + nextRun + " period: " + period;// + " messages: " + messages;
			}
		}

		private ArrayList<EncodingMessageProperties> encodingMessageProperties = new ArrayList<>();
		private long restTime = 0;
		private Thread timerThread;
		private MessageTimerInfo group = null;
		private LinkedList<MessageTimerInfo> timeTasks;
		long startTime = 0;

		long timeToReach;

		public void add(EncodingMessageProperties emp) {
			if (startTime == 0)
				encodingMessageProperties.add(emp);
			else {
				stop();
				encodingMessageProperties.add(emp);
				addAtRuntime(emp);
				start();
			}
		}

		private void addAtRuntime(EncodingMessageProperties emp) {
			try {
				int period = Integer.parseInt(emp.getAdditionalInfo());
				DBCMessage message = canDBC.getPropById(emp.getId());
				for (MessageTimerInfo mti : timeTasks)
					if (mti.getPeriod() == period) {
						mti.messages.add(message);
						return;
					}
				MessageTimerInfo mti = new MessageTimerInfo(period);
				mti.addMessage(message);
				mti.incNextRun((timeToReach - startTime - restTime) / period);
				ListIterator<MessageTimerInfo> it = timeTasks.listIterator();
				while (it.hasNext()) {
					MessageTimerInfo element = it.next();
					if (element.getNextRun() > mti.getNextRun()) {
						it.previous();
						it.add(mti);
						return;
					}
				}
				it.add(mti);
			} catch (NumberFormatException e) {}
		}

		public void changePeriod(EncodingMessageProperties emp) {
			stop();
			removeAtRuntime(emp);
			addAtRuntime(emp);
			start();

		}

		public boolean remove(EncodingMessageProperties emp) {
			stop();
			encodingMessageProperties.remove(emp);
			if (encodingMessageProperties.isEmpty())
				return true;
			removeAtRuntime(emp);
			start();
			return false;
		}

		private void removeAtRuntime(EncodingMessageProperties emp) {
			DBCMessage messageToChange = canDBC.getPropById(emp.getId());
			Iterator<MessageTimerInfo> iterator = timeTasks.iterator();
			while (iterator.hasNext()) {
				MessageTimerInfo mti = iterator.next();
				ArrayList<DBCMessage> messages = mti.messages;
				int indexOfMessage = messages.indexOf(messageToChange);
				if (indexOfMessage != -1) {
					messages.remove(indexOfMessage);
					if (messages.isEmpty())
						iterator.remove();
					return;
				}
			}
			if (group != null) {
				ArrayList<DBCMessage> messages = group.messages;
				int indexOfMessage = messages.indexOf(messageToChange);
				if (indexOfMessage != -1) {
					messages.remove(indexOfMessage);
					if (messages.isEmpty())
						group = null;
					return;
				}
			}
		}

		public void start() {
			if (timerThread != null)
				stop();
			timerThread = new Thread() {

				@Override
				public void run() {
					if (timeTasks == null) {
						HashMap<Integer, MessageTimerInfo> messageIndex = new HashMap<>();
						for (int i = 0; i < encodingMessageProperties.size(); i++) {
							EncodingMessageProperties emp = encodingMessageProperties.get(i);
							int period = Integer.parseInt(emp.getAdditionalInfo());
							MessageTimerInfo mti = messageIndex.get(period);
							if (mti == null) {
								mti = new MessageTimerInfo(period);
								messageIndex.put(period, mti);
							}
							mti.addMessage(canDBC.getPropById(emp.getId()));
						}
						timeTasks = new LinkedList<>(messageIndex.values());
						timeTasks.sort((a, b) -> Integer.compare(a.getPeriod(), b.getPeriod()));
						startTime = System.currentTimeMillis();
					}
					timeToReach = System.currentTimeMillis();
					try {
						next: while (true) {
							long timeTowait;
							if (restTime != -1) {
								if (group != null)
									restTime = 0;
								timeTowait = restTime;
								timeToReach += timeTowait;
								restTime = -1;
							} else {
								group = timeTasks.pop();
								timeToReach = startTime + group.getNextRun();
								timeTowait = timeToReach - System.currentTimeMillis();
							}
							if (timeTowait > 0)
								synchronized (this) {
									wait(timeTowait);
								}
							if (group != null) {
								ArrayList<DBCMessage> messages = group.getMessages();
								ArrayList<byte[]> datas = new ArrayList<>();
								synchronized (CanDBCEncoder.this) {
									messages.forEach(message -> datas.add(dataMap.get(message.getId())));
								}
								for (int i = 0; i < messages.size(); i++) {
									DBCMessage message = messages.get(i);
									triggerOutput(new CanTrame(message.getId(), true, datas.get(i)));
								}
								group.incNextRun();
								ListIterator<MessageTimerInfo> it = timeTasks.listIterator();
								while (it.hasNext()) {
									MessageTimerInfo element = it.next();
									if (element.getNextRun() > group.getNextRun()) {
										it.previous();
										it.add(group);
										continue next;
									}
								}
								if (group.messages.isEmpty())
									System.err.println("error empty");
								it.add(group);
								group = null;
							}
						}
					} catch (InterruptedException e) {
						restTime = timeToReach - System.currentTimeMillis();
					} catch (NoSuchElementException e) {
						timerThread = null;
						return;
					}
				}
			};
			timerThread.start();
		}

		public void stop() {
			if (timerThread != null) {
				timerThread.interrupt();
				try {
					timerThread.join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
}

class InputData {
	private final int id;
	private final DBCSignal signal;
	private final byte[] canData;
	private final boolean[] updatedData;

	public InputData(int id, DBCSignal signal, byte[] canData, boolean[] updatedData) {
		this.id = id;
		this.signal = signal;
		this.canData = canData;
		this.updatedData = updatedData;
	}

	public byte[] getCanData() {
		return canData;
	}

	public int getId() {
		return id;
	}

	public DBCSignal getSignal() {
		return signal;
	}

	public boolean[] getUpdatedData() {
		return updatedData;
	}
}
