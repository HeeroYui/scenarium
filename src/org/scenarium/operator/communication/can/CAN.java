/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.communication.can;

import java.util.ArrayList;

import org.beanmanager.BeanDesc;
import org.beanmanager.BeanRenameListener;
import org.beanmanager.editors.PropertyInfo;
import org.beanmanager.editors.container.BeanInfo;
import org.scenarium.communication.can.CANPeak;
import org.scenarium.communication.can.CANReceiver;
import org.scenarium.communication.can.CANVector;
import org.scenarium.communication.can.CanException;
import org.scenarium.communication.can.CanProvider;
import org.scenarium.communication.can.CanTrame;
import org.scenarium.communication.can.dbc.DBCSignal;
import org.scenarium.filemanager.scenario.dataflowdiagram.BlockInfo;
import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;
import org.scenarium.filemanager.scenario.dataflowdiagram.ParamInfo;
import org.scenarium.filemanager.scenario.dataflowdiagram.operator.DeclaredOutputChangeListener;

@BlockInfo(info = "Block for the communication with an CAN bus")
public class CAN extends EvolvedOperator implements CANReceiver, DeclaredOutputChangeListener, BeanRenameListener {
	@PropertyInfo(index = 0)
	@BeanInfo(possibleSubclasses = { CANVector.class, CANPeak.class }, alwaysExtend = true)
	private CanProvider canProvider;
	@PropertyInfo(index = 1)
	private boolean generatingCanRawOutput = false;
	@PropertyInfo(index = 2)
	@BeanInfo(alwaysExtend = true)
	private CanDBCDecoder canDBCDecoder;
	private boolean isRunning = false;
	private boolean isAvailable = false;
	private String[] decoderOutputNames;
	private Class<?>[] decoderOutputTypes;

	@Override
	public void birth() throws Exception {
		if (canProvider == null)
			return;
		isRunning = true;
		onStart(() -> {
			try {
				canProvider.init(this);
				isAvailable = true;
			} catch (CanException e) {
				System.err.println(getBlockName() + ": " + e.getMessage());
				setDefaulting(true);
			}
		});
		if (canDBCDecoder != null)
			canDBCDecoder.birth();
	}

	@Override
	public void death() throws Exception {
		if (canProvider != null)
			canProvider.close();
		if (canDBCDecoder != null)
			canDBCDecoder.death();
		isRunning = false;
	}

	@Override
	public void declaredOutputChanged(String[] names, Class<?>[] types) {
		this.decoderOutputNames = names;
		this.decoderOutputTypes = types;
		initStruct();
	}

	public CanDBCDecoder getCanDBCDecoder() {
		return canDBCDecoder;
	}

	public CanProvider getCanProvider() {
		return canProvider;
	}

	@Override
	public void initStruct() {
		ArrayList<String> names = new ArrayList<>();
		ArrayList<Class<?>> types = new ArrayList<>();
		if (generatingCanRawOutput) {
			names.add(getBlockName());
			types.add(CanTrame.class);
		}
		if (decoderOutputNames != null)
			for (int i = 0; i < decoderOutputNames.length; i++) {
				names.add(decoderOutputNames[i]);
				types.add(decoderOutputTypes[i]);
			}
		updateOutputs(names.toArray(new String[0]), types.toArray(new Class<?>[0]));
		removeBlockNameChangeListener(this);
		addBlockNameChangeListener(this);
	}

	public boolean isGeneratingCanRawOutput() {
		return generatingCanRawOutput;
	}

	@ParamInfo(in = "in", out = "out")
	public void process(CanTrame... canTrames) throws Exception {
		if (canProvider != null && isAvailable)
			for (CanTrame canTrame : canTrames)
				canProvider.sendTrame(canTrame);
	}

	@Override
	public void receive(CanTrame canTrame, long timestamp) {
		Object[] outputs = generateOuputsVector();
		boolean isEmpty = generatingCanRawOutput;
		if (generatingCanRawOutput) {
			int idOfOut = getOutputIndex(getBlockName());
			outputs[idOfOut] = canTrame;
			isEmpty = false;
		}
		if (canDBCDecoder != null) {
			ArrayList<DBCSignal> datasProp = canDBCDecoder.getDatasProp(canTrame);
			if (datasProp != null)
				for (DBCSignal canDBCProp : datasProp) {
					outputs[getOutputIndex(canTrame.getId() + "_" + canDBCProp.name)] = canDBCDecoder.isEnumAsString() && canDBCProp.isEnum() ? canDBCProp.decodeAsString(canTrame.getData()) : canDBCProp.decode(canTrame.getData());
					isEmpty = false;
				}
		}
		if (!isEmpty)
			triggerOutput(outputs, timestamp);
	}

	public void setCanDBCDecoder(CanDBCDecoder canDBCDecoder) {
		if (this.canDBCDecoder != null) {
			this.canDBCDecoder.removeDeclaredOutputChangeListener(this);
			this.canDBCDecoder.setTrigger(null);
			declaredOutputChanged(null, null);
			if (isRunning)
				this.canDBCDecoder.death();
		}
		this.canDBCDecoder = canDBCDecoder;
		if (this.canDBCDecoder != null) {
			this.canDBCDecoder.addDeclaredOutputChangeListener(this);
			this.canDBCDecoder.initStruct();
			if (isRunning)
				this.canDBCDecoder.birth();
		}
	}

	public void setCanProvider(CanProvider canProvider) {
		if (isRunning) {
			isAvailable = false;
			CanProvider _canProvider = this.canProvider;
			this.canProvider = null;
			_canProvider.close();
			this.canProvider = canProvider;
			try {
				birth();
				isAvailable = true;
			} catch (Exception e) {
				System.err.println(e.getMessage());
			}
		} else
			this.canProvider = canProvider;
	}

	public void setGeneratingCanRawOutput(boolean generatingCanRawOutput) {
		this.generatingCanRawOutput = generatingCanRawOutput;
		initStruct();
	}

	@Override
	public void beanRename(BeanDesc<?> oldBeanDesc, BeanDesc<?> beanDesc) {
		initStruct();
	}
}
