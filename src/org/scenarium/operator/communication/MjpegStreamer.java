/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.communication;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

import org.beanmanager.editors.PropertyInfo;
import org.beanmanager.editors.primitive.number.ControlType;
import org.beanmanager.editors.primitive.number.NumberInfo;
import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;
import org.scenarium.operator.image.conversion.ImgEncoder;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpServer;

public class MjpegStreamer extends EvolvedOperator {
	@PropertyInfo(index = 0, info = "Port of the jpeg http server")
	@NumberInfo(min = 0, max = Short.MAX_VALUE * 2 + 1, controlType = ControlType.TEXTFIELD)
	private int port = 8080;
	@PropertyInfo(index = 1, info = "Compression quality for the jpeg transmitted image")
	@NumberInfo(min = 0, max = 1, controlType = ControlType.TEXTFIELDANDSLIDER)
	private float quality = 0.5f;
	@PropertyInfo(index = 2, info = "Explicitly displays in the console connection status")
	private boolean verbose = false;

	private HttpServer server;
	private List<Function<byte[], Boolean>> clients = Collections.synchronizedList(new ArrayList<>());
	private ArrayList<Function<byte[], Boolean>> deadClients = new ArrayList<>();
	private ImgEncoder imgToJpeg;

	public MjpegStreamer() {}

	public MjpegStreamer(int port, float quality) {
		this.port = port;
		this.quality = quality;
	}

	@Override
	public void birth() {
		clients.clear();
		try {
			server = HttpServer.create(new InetSocketAddress(port), 0);
			server.createContext("/", t -> {
				Headers h = t.getResponseHeaders();
				h.set("Cache-Control", "no-cache, private");
				h.set("Content-Type", "multipart/x-mixed-replace;boundary=--boundary");
				t.sendResponseHeaders(200, 0);

				OutputStream os = t.getResponseBody();
				InetSocketAddress remoteAddress = t.getRemoteAddress();
				if (verbose)
					System.out.println(getBlockName() + " connected with: " + remoteAddress);
				clients.add(new Function<byte[], Boolean>() {
					private boolean isConnected = true;
					private int deadCpt = 0;

					@Override
					public Boolean apply(byte[] jpegData) {
						try {
							os.write(("\r\n\r\n--boundary\r\nContent-Type: image/jpeg\r\nContent-Length: " + jpegData.length + "\r\n\r\n").getBytes());
							os.write(jpegData);
							if (!isConnected) {
								if (verbose)
									System.out.println(getBlockName() + ": Connection established");
								isConnected = true;
								deadCpt = 0;
							}
						} catch (IOException e) {
							deadCpt++;
							if (isConnected) {
								if (verbose)
									System.out.println(getBlockName() + ": Connection lost");
								isConnected = false;
							}
						}
						return deadCpt >= 10;
					}
				});
			});
			server.setExecutor(null);
			server.start();
		} catch (IOException e) {
			System.err.println(getBlockName() + ": Cannot create the serveur. Cause: " + e.getMessage());
		}
		imgToJpeg = new ImgEncoder();
		imgToJpeg.setImgFormatName("jpg");
		imgToJpeg.setQuality(quality);
		imgToJpeg.birth();
	}

	@Override
	public void death() {
		if (server != null) {
			server.stop(1);
			server = null;
		}
		imgToJpeg.death();
		imgToJpeg = null;
		clients.clear();
	}

	public int getPort() {
		return port;
	}

	public float getQuality() {
		return quality;
	}

	public boolean isVerbose() {
		return verbose;
	}

	public void process(BufferedImage img) {
		if (!clients.isEmpty()) {
			byte[] jpegData = imgToJpeg.process(img);
			clients.forEach(e -> {
				if (e.apply(jpegData))
					deadClients.add(e);
			});
			if (!deadClients.isEmpty()) {
				clients.removeAll(deadClients);
				deadClients.clear();
			}
		}
	}

	public void setPort(int port) {
		this.port = port;
		restart();
	}

	public void setQuality(float quality) {
		this.quality = quality;
		ImgEncoder _imgToJpeg = imgToJpeg;
		if (_imgToJpeg != null)
			_imgToJpeg.setQuality(quality);
	}

	public void setVerbose(boolean verbose) {
		this.verbose = verbose;
	}
}