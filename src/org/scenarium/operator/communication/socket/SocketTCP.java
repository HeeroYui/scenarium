/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.communication.socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import org.beanmanager.editors.PropertyInfo;
import org.scenarium.filemanager.scenario.dataflowdiagram.ParamInfo;
import org.scenarium.operator.communication.serial.DataType;

public class SocketTCP extends AbstractEthernet {
	@PropertyInfo(index = 0, nullable = false, info = "The desired ethernet adresse to use\nIf no ip is specified, this block will be considered as the server")
	private InetSocketAddress address;

	private Socket socket;
	private ServerSocket serveurSocket;
	private ObjectOutputStream objectOutputStream;

	public SocketTCP() {
		try {
			address = new InetSocketAddress(InetAddress.getByName("0.0.0.0"), 0);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}

	public SocketTCP(DataType dataTypeIn, DataType dataTypeOut, boolean connectionFlag, boolean verbose, int timeOut, int bufferSize, InetSocketAddress inetSocketAddress) {
		super(dataTypeIn, dataTypeOut, connectionFlag, verbose, timeOut, bufferSize);
		if (inetSocketAddress == null)
			throw new IllegalArgumentException("the adress cannot be null");
		this.address = inetSocketAddress;
	}

	public InetSocketAddress getAddress() {
		return address;
	}

	@ParamInfo(out = "Out")
	public void process() throws Exception {
		Object in = getAdditionalInputs()[0];
		java.net.Socket _socket = socket;
		if (_socket != null)
			try {
				if (inDataType == DataType.OBJECT) {
					if (objectOutputStream == null)
						objectOutputStream = new ObjectOutputStream(_socket.getOutputStream());
					objectOutputStream.writeObject(in);
					objectOutputStream.flush();
				} else if (in instanceof byte[] && inDataType == DataType.BYTE)
					_socket.getOutputStream().write((byte[]) in);
				else if (in instanceof String && inDataType == DataType.STRING)
					_socket.getOutputStream().write(((String) in).getBytes());
			} catch (IOException e) {
				if (verbose)
					System.err.println(getBlockName() + ": " + e.getMessage());
				start();
			}
	}

	public void setAddress(InetSocketAddress address) {
		if (address != null) {
			this.address = address;
			restartLater();
		}
	}

	@Override
	synchronized protected void start() {
		death();
		ethernetThread = new Thread(() -> {
			while (isRunning()) {
				if (Thread.currentThread().isInterrupted()) {
					stop();
					return;
				}
				InputStream is = null;
				do {
					try {
						if (socket == null) {
							if (address.getAddress().equals(InetAddress.getByName("0.0.0.0"))) {
								serveurSocket = new ServerSocket(address.getPort());
								serveurSocket.setSoTimeout(timeOut);
								socket = serveurSocket.accept();
							} else
								socket = new Socket(address.getAddress(), address.getPort());
							socket.setSoTimeout(timeOut);
							socket.setReceiveBufferSize(receiveBufferSize);
							connectionEstablished(true);
						}
						if (outDataType == DataType.DISABLE)
							return;
						is = socket.getInputStream();
					} catch (IOException e) {
						if (verbose)
							System.err.println(getBlockName() + ": " + e.getMessage());
						if (!isRunning())
							return;
						if (socket == null) {
							if (serveurSocket != null) {
								try {
									serveurSocket.close();
								} catch (IOException e1) {
									e1.printStackTrace();
								}
							}
							try {
								Thread.sleep(timeOut);
							} catch (InterruptedException e1) {
								return;
							}
						}
					}
					if (Thread.currentThread().isInterrupted()) {
						stop();
						return;
					}
				} while (is == null && isRunning());
				if (socket == null)
					return;
				try {
					if (outDataType == DataType.STRING) {
						try (BufferedReader buf = new BufferedReader(new InputStreamReader(socket.getInputStream()))) {
							while (isRunning()) {
								if (Thread.currentThread().isInterrupted()) {
									stop();
									return;
								}
								triggerData(buf.readLine());
							}
						}
					} else if (outDataType == DataType.BYTE) {
						byte[] readBuff = new byte[receiveBufferSize];
						while (isRunning()) {
							if (Thread.currentThread().isInterrupted()) {
								stop();
								return;
							}
							int nbBytes = is.read(readBuff);
							if (nbBytes > 0)
								triggerByteArrayData(readBuff, nbBytes);
						}
					} else if (outDataType == DataType.OBJECT) {
						ObjectInputStream objectOutput = new ObjectInputStream(socket.getInputStream());
						while (isRunning()) {
							if (Thread.currentThread().isInterrupted()) {
								stop();
								return;
							}
							triggerData(objectOutput.readObject());
						}
					}
					stop();
				} catch (IOException | ClassNotFoundException e) {
					if (isRunning()) {
						if (verbose)
							System.err.println(getBlockName() + ": " + e.getMessage());
						if (!e.getClass().equals(SocketTimeoutException.class))
							stop();
					}
				}
			}
		});
		ethernetThread.start();
	}

	@Override
	synchronized protected void stop() {
		if (socket != null) {
			try {
				socket.close();
			} catch (IOException e) {}
			connectionEstablished(false);
			socket = null;
		}
		if (serveurSocket != null) {
			try {
				serveurSocket.close();
			} catch (IOException e) {}
			serveurSocket = null;
		}
		if (objectOutputStream != null) {
			try {
				objectOutputStream.close();
			} catch (IOException e) {}
			objectOutputStream = null;
		}
	}

	private void triggerByteArrayData(byte[] readBuff, int nbBytes) {
		byte[] datas = new byte[nbBytes];
		System.arraycopy(readBuff, 0, datas, 0, datas.length);
		triggerData(datas);
	}
}
