/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.operator.communication.socket;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import org.beanmanager.editors.DynamicEnableBean;
import org.beanmanager.editors.PropertyInfo;
import org.scenarium.operator.communication.serial.DataType;

public class SocketUDP extends AbstractEthernet implements DynamicEnableBean {
	@PropertyInfo(index = 0, nullable = false, info = "The desired ethernet adresse to use\nIf no ip is specified, this block will be considered as the client")
	private InetSocketAddress address = new InetSocketAddress(0);

	private byte[] data = new byte[receiveBufferSize];
	private byte[] dpData = new byte[1];

	private DatagramSocket datagramSocket;

	public SocketUDP() {
		try {
			address = new InetSocketAddress(InetAddress.getByName("0.0.0.0"), 0);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}

	public SocketUDP(DataType dataTypeIn, DataType dataTypeOut, boolean connectionFlag, boolean verbose, int timeOut, int bufferSize, InetSocketAddress inetSocketAddress) {
		super(dataTypeIn, dataTypeOut, connectionFlag, verbose, timeOut, bufferSize);
		if (inetSocketAddress == null)
			throw new IllegalArgumentException("the adress cannot be null");
		this.address = inetSocketAddress;
	}

	public InetSocketAddress getAddress() {
		return address;
	}

	@Override
	protected boolean isInputAvailable() {
		try {
			return address.getAddress() == null ? false : !address.getAddress().equals(InetAddress.getByName("0.0.0.0"));
		} catch (UnknownHostException e) {
			return false;
		}
	}

	@Override
	protected boolean isOutputAvailable() {
		try {
			return address.getAddress() == null ? false : address.getAddress().equals(InetAddress.getByName("0.0.0.0"));
		} catch (UnknownHostException e) {
			return false;
		}
	}

	public void process() {
		Object in = getAdditionalInputs()[0];
		DatagramSocket _datagramSocket = datagramSocket;
		if (_datagramSocket != null) {
			try {
				if (address.getAddress().equals(InetAddress.getByName("0.0.0.0")))
					if (verbose)
						System.err.println(getBlockName() + ": cannot send data in client mode");
			} catch (UnknownHostException e1) {
				return;
			}
			try {
				byte[] sendBuf = null;
				if (inDataType == DataType.OBJECT) {
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					try(ObjectOutputStream os = new ObjectOutputStream(baos)){
						os.writeObject(in);
						os.flush();
						sendBuf = baos.toByteArray();
					}
				} else if (in instanceof byte[] && inDataType == DataType.BYTE)
					sendBuf = (byte[]) in;
				else if (in instanceof String && inDataType == DataType.STRING)
					sendBuf = ((String) in).getBytes();
				if (sendBuf != null) {
					DatagramPacket pack = new DatagramPacket(sendBuf, sendBuf.length, address);
					pack.setData(sendBuf);
					_datagramSocket.send(pack);
				}
			} catch (IOException e) {
				if (verbose)
					System.err.println(getBlockName() + ": " + e.getMessage());
				start();
			}
		}
	}

	public void setAddress(InetSocketAddress address) {
		if (address != null) {
			this.address = address;
			setEnable();
			restartAndReloadStructLater();
		}
	}

	@Override
	public void setEnable() {
		try {
			boolean isClient = address.getAddress() == null ? true : address.getAddress().equals(InetAddress.getByName("0.0.0.0"));
			fireSetPropertyEnable(this, "inDataType", address == null ? false : !isClient);
			fireSetPropertyEnable(this, "outDataType", address == null ? false : isClient);
		} catch (UnknownHostException e) {}
	}

	@Override
	synchronized protected void start() {
		death();
		ethernetThread = new Thread(() -> {
			data = new byte[receiveBufferSize];
			while (isRunning()) {
				if (Thread.currentThread().isInterrupted()) {
					stop();
					return;
				}
				do {
					try {
						if (datagramSocket == null) {
							datagramSocket = address.getAddress().equals(InetAddress.getByName("0.0.0.0")) ? new DatagramSocket(address.getPort()) : new DatagramSocket();
							datagramSocket.setSoTimeout(timeOut);
							connectionEstablished(true);
						}
						if (outDataType == DataType.DISABLE)
							return;
					} catch (IOException e) {
						if (verbose)
							System.err.println(getBlockName() + ": " + e.getMessage());
						if (!isRunning())
							return;
						if (datagramSocket == null)
							try {
								Thread.sleep(timeOut);
							} catch (InterruptedException e1) {}
					}
					if (Thread.currentThread().isInterrupted()) {
						stop();
						return;
					}
				} while (datagramSocket == null && isRunning());
				while (isRunning() && datagramSocket != null) {
					if (Thread.currentThread().isInterrupted()) {
						stop();
						return;
					}
					try {
						DatagramPacket dp = new DatagramPacket(data, data.length);
						datagramSocket.receive(dp);
						if (dpData.length != dp.getLength())
							dpData = new byte[dp.getLength()];
						System.arraycopy(dp.getData(), dp.getOffset(), dpData, 0, dpData.length);
						if (outDataType == DataType.STRING)
							triggerData(new String(dpData));
						else if (outDataType == DataType.BYTE)
							triggerData(dpData);
						else if (outDataType == DataType.OBJECT) {
							try(ObjectInputStream iStream = new ObjectInputStream(new ByteArrayInputStream(dpData))){
								Object data = iStream.readObject();
								triggerData(data);
							}
						}
					} catch (IOException | NullPointerException | ClassNotFoundException e) {
						if (isRunning()) {
							if (verbose)
								System.err.println(getBlockName() + ": " + e.getMessage());
							if (!e.getClass().equals(SocketTimeoutException.class))
								stop();
						}
					}
				}
			}
		});
		ethernetThread.start();
	}

	@Override
	synchronized protected void stop() {
		dpData = new byte[1];
		if (datagramSocket != null) {
			datagramSocket.close();
			connectionEstablished(false);
			datagramSocket = null;
		}
	}
}
