/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium;

import javax.vecmath.Point2i;

import org.scenarium.display.toolbarclass.ToolBarDescriptor;
import org.scenarium.filemanager.scenariomanager.Scenario;

import javafx.beans.property.ReadOnlyDoubleProperty;

public interface MainApp {

	void addToScheduleAndShow(Scenario scenario);

	Point2i getMainFrameDimension();

	Point2i getMainFramePosition();

	ToolBarDescriptor[] getVisibleToolsDesc();

	void loadingOperation(ReadOnlyDoubleProperty progressProperty);

	void removeToSchedule(Scenario scenario);

	void updateRecentScenario();
//
//	void registerExternModules(Path modulePath);
//
//	void unregisterExternModule(Module module);
//
//	boolean isInternModule(Module module);
//
//	String getModulePath(Module module);
}
