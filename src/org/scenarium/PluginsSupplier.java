/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

import org.scenarium.display.ToolBarInfo;
import org.scenarium.display.drawer.TheaterPanel;
import org.scenarium.filemanager.filerecorder.FileStreamRecorder;
import org.scenarium.filemanager.scenariomanager.Scenario;

public interface PluginsSupplier {
	default public void populateOperators(Consumer<Class<?>> operatorConsumer) {}

	default public void populateCloners(ClonerConsumer clonerConsumer) {}

	default public void populateEditors(EditorConsumer editorConsumer) {}

	default public void populateDrawers(BiConsumer<Class<? extends Object>, Class<? extends TheaterPanel>> drawersConsumer) {}

	default public void populateToolBars(Consumer<ToolBarInfo> toolBarConsumer) {}

	default public void populateScenarios(BiConsumer<Class<? extends Scenario>, String[]> scenarioConsumer) {}
	
	default public void populateDataStream(DataStreamConsumer dataStreamConsumer) {};
	
	default public void populateFileRecorder(BiConsumer<Class<?>, Class<? extends FileStreamRecorder>> fileRecorderConsumer) {};
}