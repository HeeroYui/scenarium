/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium;

import java.beans.IntrospectionException;
import java.beans.Introspector;
//import java.io.File;
import java.io.IOException;
import java.lang.module.FindException;
import java.lang.module.ModuleDescriptor;
import java.lang.module.ModuleDescriptor.Requires;
import java.lang.module.ModuleFinder;
import java.lang.module.ModuleReference;
import java.lang.module.ResolutionException;
import java.lang.reflect.Field;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchEvent.Kind;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.ServiceLoader;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.beanmanager.BeanManager;
import org.beanmanager.LoadModuleListener;
import org.beanmanager.editors.PropertyEditorManager;
import org.beanmanager.tools.EventListenerList;
import org.beanmanager.tools.Logger;
import org.scenarium.communication.can.CanTrame;
import org.scenarium.communication.can.dbc.EncodingMessageProperties;
import org.scenarium.communication.can.dbc.MessageIdentifier;
import org.scenarium.communication.can.dbc.SignalIdentifier;
import org.scenarium.display.RenderPane;
import org.scenarium.display.drawer.DrawerManager;
import org.scenarium.display.toolbarclass.ToolBarDescriptor;
import org.scenarium.editors.CurveSeriesEditor;
import org.scenarium.editors.CurvedEditor;
import org.scenarium.editors.CurveiEditor;
import org.scenarium.editors.GeographicCoordinateEditor;
import org.scenarium.editors.GeographicalPoseEditor;
import org.scenarium.editors.NMEA0183Editor;
import org.scenarium.editors.ScenarioDescriptorEditor;
import org.scenarium.editors.ToolDescriptorEditor;
import org.scenarium.editors.can.CanTrameEditor;
import org.scenarium.editors.can.dbc.EncodingMessagePropertiesEditor;
import org.scenarium.editors.can.dbc.MessageIdentifierEditor;
import org.scenarium.editors.can.dbc.SignalIdentifierEditor;
import org.scenarium.filemanager.datastream.DataStreamManager;
import org.scenarium.filemanager.filerecorder.FileStreamRecorder;
import org.scenarium.filemanager.scenario.dataflowdiagram.operator.OperatorManager;
import org.scenarium.filemanager.scenario.dataflowdiagram.scheduler.DiagramScheduler;
import org.scenarium.filemanager.scenariomanager.ScenarioDescriptor;
import org.scenarium.filemanager.scenariomanager.ScenarioManager;
import org.scenarium.struct.GeographicCoordinate;
import org.scenarium.struct.GeographicalPose;
import org.scenarium.struct.ScenariumProperties;
import org.scenarium.struct.curve.CurveSeries;
import org.scenarium.struct.curve.Curved;
import org.scenarium.struct.curve.Curvei;

import net.sf.marineapi.nmea.sentence.Sentence;

// TODO Ne pas charger les modules dépendances
// TODO Construire un index moduleName -> Path pour les dépendance
// TODO à chaque chargement d'un module, utiliser un moduleFinder à partir de l'index des modules sur les dépendance

// TODO Si changement module, mettre avant tout à jour avec l'ensemble des modifs, puis charger les modules
// TODO Si plusieurs module même nom, voir version -> Charger que la dernière et préciser qu'il existe une version obsoléte à supprimer? ou pas? Sinon, erreur deux modules même nom et version?

public class ModuleManager {
	private static final PathMatcher jarPathMatcher = FileSystems.getDefault().getPathMatcher("glob:**.jar");
	private static String EXTERNPLUGINFOLDERNAME = "modules";
	private static final HashMap<Path, List<Module>> internModules = new HashMap<>();
	private static final HashMap<String, Path> internModuleDependencySupplier = new HashMap<>();
	private static final HashMap<Module, Path> externModulePaths = new HashMap<>();
	private static final EventListenerList<LoadModuleListener> listeners = new EventListenerList<>();

	static {
		// Forces static methods to be called from Scenarim module and not from another module. Otherwise, it prevents the release of module resources. See on RenderPane
		try {
			Class.forName(OperatorManager.class.getName());
			Class.forName(DiagramScheduler.class.getName());
			Class.forName(PropertyEditorManager.class.getName());
			Class.forName(DrawerManager.class.getName());
			Class.forName(RenderPane.class.getName());
			Class.forName(ScenarioManager.class.getName());
			Class.forName(DataStreamManager.class.getName());
			Class.forName(FileStreamRecorder.class.getName());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	private ModuleManager() {}

	public static void main(String[] args) throws IOException {
		Path oriTestFile = Path.of("C:\\Users\\marcr\\Workspace\\Scenarium\\Sample.jar");
		Path copyTestFile = Path.of("C:\\Users\\marcr\\Workspace\\Scenarium\\Sample2.jar");
		Files.copy(oriTestFile, copyTestFile, StandardCopyOption.REPLACE_EXISTING);
		loadAndUnloadTest(copyTestFile);
		System.gc();
		try {
			Files.delete(copyTestFile);
			System.out.println("Good");
		} catch (Exception e) {
			System.err.println("Bad");
		}
	}

	private static void loadAndUnloadTest(Path testFile) {
		List<Module> newModules = registerModules(testFile);
		try {
			Class<?> c = Class.forName("org.operators.SampleOperator", true, newModules.get(0).getClassLoader());
			Introspector.getBeanInfo(c);
			Introspector.flushCaches();
			Introspector.flushFromCaches(c);
			Class<?> ci = Class.forName("com.sun.beans.introspect.ClassInfo");
			Field cache = ci.getDeclaredField("CACHE");
			/* Object val = */cache.get(null);
			// cache.setAccessible(true);
			// cache.set(null, null);
			System.out.println(c);
			// flushCachesWithOOME();
		} catch (ClassNotFoundException | SecurityException | IllegalArgumentException | IntrospectionException | NoSuchFieldException | IllegalAccessException e) {
			e.printStackTrace();
		}
		System.out.println(newModules);
		newModules.forEach(ModuleManager::unregisterModule);
	}

	public static void loadEmbeddedAndInternalModules() {
		Logger.log(Logger.MODULE, "Load embedded and internal modules");
		loadScenariumPlugins();
		loadPlugins(ServiceLoader.load(PluginsSupplier.class)); // Built-in modules. 200ms for the first one, 40ms for the following
		loadInternalModules();
	}

	static void loadExternalModules() {
		Logger.log(Logger.MODULE, "Load external modules");
		String[] modules = ScenariumProperties.get().getExternModules();
		ArrayList<String> invalidModules = new ArrayList<>();
		if (modules != null)
			for (String module : modules)
				try {
					if (!registerExternModules(Paths.get(module)))
						invalidModules.add(module);
				} catch (InvalidPathException e) {
					System.err.println("Module path: " + module + "is an invalid path: " + e.getMessage());
				}
		if (!invalidModules.isEmpty())
			ScenariumProperties.get().setExternModules(Arrays.asList(modules).stream().filter(m -> !invalidModules.contains(m)).toArray(size -> new String[size]));
	}

	static void loadScenariumPlugins() {
		PropertyEditorManager.registerEditor(ScenarioDescriptor.class, ScenarioDescriptorEditor.class);
		PropertyEditorManager.registerEditor(ToolBarDescriptor.class, ToolDescriptorEditor.class);
		PropertyEditorManager.registerEditor(Curved.class, CurvedEditor.class);
		PropertyEditorManager.registerEditor(Curvei.class, CurveiEditor.class);
		PropertyEditorManager.registerEditor(CurveSeries.class, CurveSeriesEditor.class);
		PropertyEditorManager.registerEditor(CanTrame.class, CanTrameEditor.class);
		PropertyEditorManager.registerEditor(Sentence.class, NMEA0183Editor.class);
		PropertyEditorManager.registerEditor(SignalIdentifier.class, SignalIdentifierEditor.class);
		PropertyEditorManager.registerEditor(MessageIdentifier.class, MessageIdentifierEditor.class);
		PropertyEditorManager.registerEditor(EncodingMessageProperties[].class, EncodingMessagePropertiesEditor.class);
		PropertyEditorManager.registerEditor(GeographicCoordinate.class, GeographicCoordinateEditor.class);
		PropertyEditorManager.registerEditor(GeographicalPose.class, GeographicalPoseEditor.class);
	}

	static void loadInternalModules() {
		Logger.log(Logger.MODULE, "Load Internal modules");
		Path pluginFolder = Paths.get(Scenarium.getLocalDir() + FileSystems.getDefault().getSeparator() + EXTERNPLUGINFOLDERNAME);
		if (!Files.exists(pluginFolder))
			try {
				Files.createDirectories(pluginFolder);
			} catch (IOException e) {
				Logger.logError(Logger.MODULE, "Cannot create module directory: " + pluginFolder + " due to: " + e.getMessage());
				return;
			}
		WatchService watchService = null;
		try {
			watchService = FileSystems.getDefault().newWatchService();
			pluginFolder.register(watchService, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_DELETE, StandardWatchEventKinds.ENTRY_MODIFY);
		} catch (IOException e1) {
			Logger.logError(Logger.MODULE, "Cannot create watch service for plugin folter: " + pluginFolder);
			e1.printStackTrace();
		}
		try {
			ArrayList<Path> moduleToLoad = new ArrayList<>();
			Files.walk(pluginFolder, Integer.MAX_VALUE).filter(ModuleManager::isJarFile).forEach(path -> ModuleFinder.of(path).findAll().stream().map(mr -> mr.descriptor()).forEach(descriptor -> {
				internModuleDependencySupplier.put(descriptor.name(), path);
				if (ModuleManager.isScenariumPlugin(descriptor))
					moduleToLoad.add(path);
			}));
			moduleToLoad.forEach(ModuleManager::registerInternModules);
		} catch (IOException e) {
			e.printStackTrace();
		}
		WatchService _watchService = watchService;
		if (watchService != null)
			new Thread(() -> {
				try {
					WatchKey key;
					while ((key = _watchService.take()) != null) {
						ArrayList<Path> createdModulePath = new ArrayList<>();
						for (WatchEvent<?> event : key.pollEvents()) {
							Kind<?> kind = event.kind();
							Path path = pluginFolder.resolve((Path) event.context());
							List<Module> modules = internModules.get(path);
							if (kind == StandardWatchEventKinds.ENTRY_CREATE) {
								if (isJarFile(path) && modules == null) {
									Logger.log(Logger.MODULE, "Intern module(s) file created: " + path);
									ModuleFinder.of(path).findAll().forEach(moduleReference -> internModuleDependencySupplier.put(moduleReference.descriptor().name(), path));
									registerInternModules(path);
									createdModulePath.add(path);
								}
							} else if (kind == StandardWatchEventKinds.ENTRY_DELETE) {
								if (jarPathMatcher.matches(path) && modules != null) {
									Logger.log(Logger.MODULE, "Intern module(s) file deleted: " + path);
									internModuleDependencySupplier.values().removeIf(f -> f.equals(path));
									unregisterInternModule(path);
								}
							} else if (kind == StandardWatchEventKinds.ENTRY_MODIFY) {
								if (isJarFile(path) && modules != null && !createdModulePath.contains(path)) {
									Logger.log(Logger.MODULE, "Intern module(s) file modified: " + path);
									List<Runnable> tasks = modules.stream().flatMap(ModuleManager::fireModuleModified).collect(Collectors.toList());
									unregisterInternModule(path);
									registerInternModules(path);
									tasks.forEach(Runnable::run);
								}
							}

						}
						key.reset();
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}).start();
	}

	private static void loadPlugins(ServiceLoader<PluginsSupplier> serviceLoader) {
		serviceLoader.forEach(pl -> {
			pl.populateOperators(OperatorManager::addInternOperator);
			pl.populateCloners(new ClonerConsumer());
			pl.populateEditors(new EditorConsumer());
			pl.populateDrawers(DrawerManager::registerDrawer);
			pl.populateToolBars(RenderPane::addToolBarDescs); // Leak here JDK-8116412 workaround: weakReference in RenderFrame
			pl.populateScenarios(ScenarioManager::registerScenario);
			pl.populateDataStream(new DataStreamConsumer());
			pl.populateFileRecorder(FileStreamRecorder::registerStreamRecorder);
		});
	}

	private static boolean registerInternModules(Path path) {
		Logger.log(Logger.MODULE, "Register intern module from: " + path);
		List<Module> newModules = registerModules(path);
		if (newModules != null) {
			internModules.put(path, newModules);
			newModules.forEach(ModuleManager::fireModuleLoaded);
			return true;
		}
		return false;
	}

	private static boolean unregisterInternModule(Path path) {
		Logger.log(Logger.MODULE, "Unregister intern module from: " + path);
		List<Module> modules = internModules.remove(path);
		if (modules == null || modules.isEmpty())
			return false;
		modules.forEach(m -> ModuleManager.unregisterModule(m));
		return true;
	}

	// private static void flushCachesWithOOME() {
	// try {
	// final List<long[]> memhog = new LinkedList<long[]>();
	// while(true)
	// memhog.add(new long[102400]);
	// }
	// catch(final OutOfMemoryError e) {}
	// }

	public static boolean registerExternModules(Path modulePath) {
		Logger.log(Logger.MODULE, "Register extern module from: " + modulePath);
		List<Module> newModules = registerModules(modulePath);
		if (newModules != null) {
			ScenariumProperties.get().addExternModule(modulePath);
			newModules.forEach(nm -> externModulePaths.put(nm, modulePath));
			newModules.forEach(ModuleManager::fireModuleLoaded);
			return true;
		}
		return false;
	}

	public static boolean unregisterExternModule(Module module) {
		Logger.log(Logger.MODULE, "Unregister extern module: " + module);
		for (List<Module> modules : internModules.values())
			if (modules.contains(module))
				return false;
		Path path = externModulePaths.remove(module);
		if (path == null)
			return false;
		if (!externModulePaths.values().contains(path))
			ScenariumProperties.get().removeModule(path);
		unregisterModule(module);
		return true;
	}

	synchronized private static List<Module> registerModules(Path path) {
		Logger.log(Logger.MODULE, "Register module(s) from: " + path);
		ModuleFinder finder = ModuleFinder.of(path);
		ModuleLayer parent = ModuleLayer.boot();
		List<String> modulesName = parent.modules().stream().map(Module::getName).collect(Collectors.toList());
		try {
			ArrayList<Module> moduleLoaded = new ArrayList<>();
			nextModule: for (ModuleReference mr : finder.findAll()) {
				ModuleDescriptor descriptor = mr.descriptor();
				String moduleName = descriptor.name();
				if (isScenariumPlugin(descriptor)) {
					// System.out.println(descriptor.version() + " " +descriptor.rawVersion() + " " + descriptor.toNameAndVersion() );// version: Si jar modulaire: --module-version Sinon: nom du
					// fichier jar (jar --update --file a.jar --module-version 1.0.1)
					if (!BeanManager.isModuleRegistered(moduleName)) {
						try {
							ArrayList<Path> resuiresModulesPath = findRequireModules(descriptor, modulesName);
							resuiresModulesPath.add(path);
							finder = ModuleFinder.of(resuiresModulesPath.toArray(new Path[resuiresModulesPath.size()]));
						} catch (FindException e) {
							Logger.logError(Logger.MODULE, e.getMessage());
							continue nextModule;
						}
						try {
							ModuleLayer moduleLayer = parent.defineModulesWithOneLoader(parent.configuration().resolve(finder, ModuleFinder.of(), Set.of(moduleName)),
									ClassLoader.getSystemClassLoader());
							ClassLoader classLoader = moduleLayer.findLoader(moduleName);
							ServiceLoader<PluginsSupplier> sl = ServiceLoader.load(PluginsSupplier.class, classLoader);
							if (!sl.findFirst().isEmpty()) {
								loadPlugins(sl);
								sl.reload();
								Set<Module> modules = moduleLayer.modules();
								modules.forEach(m -> BeanManager.registerModule(m, classLoader));
								moduleLoaded.addAll(modules);
							}
						} catch (FindException | ResolutionException | SecurityException | IllegalArgumentException | IllegalStateException | Error | LayerInstantiationException e) {
							Logger.logError(Logger.MODULE, "Cannot register module: " + moduleName + " due to: " + e.getClass().getSimpleName() + ": " + e.getMessage());
							return null;
						}
					} else {
						Path pathOfOtherModule = null;
						for (Entry<Path, List<Module>> entry : internModules.entrySet()) {
							Optional<Module> module = entry.getValue().stream().filter(m -> m.getName().equals(moduleName)).findFirst();
							if (module.isPresent()) {
								pathOfOtherModule = entry.getKey();
								break;
							}
						}
						boolean isInternModule;
						if (pathOfOtherModule == null) {
							isInternModule = false;
							for (Entry<Module, Path> entry : externModulePaths.entrySet()) {
								if (entry.getKey().getName().equals(moduleName)) {
									pathOfOtherModule = entry.getValue();
									break;
								}
							}
						} else
							isInternModule = true;
						Logger.logError(Logger.MODULE,
								"Cannot load module: " + moduleName + ", another " + (isInternModule ? "intern" : "extern") + " located at: " + pathOfOtherModule + " have the same name");
						return null;
					}
				} else
					Logger.logError(Logger.MODULE,
							"Cannot load module: " + moduleName + ", this is not a Scenarium plugin. A Scenarium plugin uses " + PluginsSupplier.class.getName() + " and provides at least one plugin");
			}
			Logger.log(Logger.MODULE, "register module: " + moduleLoaded);
			// System.out.println("register module: " + moduleLoaded);
			// if (!moduleLoaded.isEmpty())
			// new Thread(() -> {
			// try {
			// Thread.sleep(5000);
			// } catch (InterruptedException e) {
			// e.printStackTrace();
			// }
			// System.out.println("unregister module");
			// ModuleManager.unregisterExternModule(moduleLoaded.get(0));
			// new Thread(() -> {
			// while (true) {
			// try {
			// Thread.sleep(1000);
			// } catch (InterruptedException e) {
			// e.printStackTrace();
			// }
			// System.out.println("gc");
			// System.gc();
			// }
			//
			// }).start();
			// }).start();
			return List.copyOf(moduleLoaded);
		} catch (FindException | SecurityException e) {
			System.err.println("Cannot find modules in file: " + path + " due to: " + e.getClass().getSimpleName() + ": " + e.getMessage());
			return null;
		}
	}

	private static ArrayList<Path> findRequireModules(ModuleDescriptor descriptor, List<String> availableModule) throws FindException {
		ArrayList<Path> requiresModulesPath = new ArrayList<>();
		String moduleName = descriptor.name();
		for (Requires requireModule : descriptor.requires()) {
			String requireModuleName = requireModule.name();
			if (!availableModule.contains(requireModuleName)) {
				Path requireModulePath = internModuleDependencySupplier.get(requireModuleName);
				if (requireModulePath != null) {
					requiresModulesPath.add(requireModulePath);
					availableModule.add(requireModuleName);
					ModuleFinder.of(requireModulePath).findAll().stream().flatMap(mr -> findRequireModules(mr.descriptor(), availableModule).stream())
							.collect(Collectors.toCollection(() -> requiresModulesPath));
				} else
					throw new FindException("Cannot register module: " + moduleName + ". Module: " + requireModuleName + " not found, required by " + moduleName);
			}
		}
		return requiresModulesPath;
	}

	public static boolean isScenariumPlugin(ModuleDescriptor descriptor) {
		return descriptor.uses().contains(PluginsSupplier.class.getName()) && descriptor.provides().stream().anyMatch(p -> p.service().equals(PluginsSupplier.class.getName()));
	}

	synchronized private static void unregisterModule(Module module) {
		Logger.log(Logger.MODULE, "Unregister module: " + module);
		Scenarium.runInMainThreadAndWait(() -> {
			// I) Opérateurs
			OperatorManager.purgeOperators(module);
			BeanManager.unregisterModule(module);
			// II) Les cloners
			DiagramScheduler.purgeCloners(module);
			// III) Les editeurs
			PropertyEditorManager.purgeEditors(module);
			// V) Les toolBars //Listener mainframe
			RenderPane.purgeToolBars(module);
			// IV) Les Drawers //Listener Scenarium
			DrawerManager.purgeDrawers(module);
			// VI) Les scénarios (et si je suis sur le scénario en question? repasse sur le scénario par default)
			ScenarioManager.purgeScenarios(module);
			// VII) Les streamer //Listener Scenarium
			DataStreamManager.purgeDataStream(module);
			// VIII) Les file recorder
			FileStreamRecorder.purgeStreamRecorder(module);
			fireModuleUnloaded(module);
		});
	}

	public static String getModulePath(Module module) {
		Path path = externModulePaths.get(module);
		return path == null ? "intern module" : path.toString();
	}

	private static boolean isJarFile(Path path) {
		return Files.isRegularFile(path) && jarPathMatcher.matches(path);
	}

	public static boolean isInternModule(Module module) {
		for (List<Module> modules : internModules.values())
			if (modules.contains(module))
				return true;
		return false;
	}

	public static void addLoadModuleListener(LoadModuleListener listener) {
		listeners.addStrongRef(listener);
	}

	public static void removeLoadModuleListener(LoadModuleListener listener) {
		listeners.removeStrongRef(listener);
	}

	private static void fireModuleLoaded(Module module) {
		listeners.forEach(l -> l.loaded(module));
	}

	private static Stream<Runnable> fireModuleModified(Module module) {
		ArrayList<Runnable> tasks = new ArrayList<>();
		listeners.forEach(l -> {
			Runnable runnable = l.modified(module);
			if (runnable != null)
				tasks.add(l.modified(module));
		});
		return tasks.stream();
	}

	private static void fireModuleUnloaded(Module module) {
		listeners.forEach(l -> l.unloaded(module));
	}
}
