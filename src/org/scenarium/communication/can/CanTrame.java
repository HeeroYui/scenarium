/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.communication.can;

import java.io.Serializable;
import java.util.Arrays;

public class CanTrame implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final int ADRESSEMAXCANEXT = 0b11111111111111111111111111111;
	public static final int ADRESSEMAXCANSTD = 0b11111111111;
	public static final int CANAIDLENGHT = 2;
	public static final int CANBIDLENGHT = 4;
	public static final int CANDATAMAXLENGHT = 8;
	public static final int TRAMEMAXSIZE = CANBIDLENGHT + CANDATAMAXLENGHT;
	private final byte[] trame;

	public static void main(String[] args) {
		CanTrame ct = new CanTrame(ADRESSEMAXCANEXT, false, new byte[] { 0x76, 0x76, (byte) 0xFF, 0x76, 0x62 });
		System.out.println(ct.getId() + " " + ct.isExtendedFrame());
	}

	public CanTrame(byte[] trame) {
		if (trame.length < CANAIDLENGHT)
			throw new IllegalArgumentException("Size of trame must be at least 11 bits (2 byte)");
		else if (trame.length > CANBIDLENGHT + CANDATAMAXLENGHT)
			throw new IllegalArgumentException("Size of trame must be at most 29 + 8*8 bits (29 + 64 byte)");
		this.trame = trame;
		if (!isExtendedFrame()) {
			if (getId() > ADRESSEMAXCANSTD)
				throw new IllegalArgumentException("Id: " + getId() + " is greater than the maximum size for standard frame: " + ADRESSEMAXCANSTD);
		} else {
			if (getId() > ADRESSEMAXCANEXT)
				throw new IllegalArgumentException("Id: " + getId() + " is greater than the maximum size for extended frame: " + ADRESSEMAXCANEXT);
		}
	}

	public CanTrame(int id, boolean extendedFrame, byte[] data) {
		if (id < 0 || id > (extendedFrame ? ADRESSEMAXCANEXT : ADRESSEMAXCANSTD))
			throw new IllegalArgumentException("Id: " + id + " larger than " + (extendedFrame ? "29" : "11") + " bits specified in " + (extendedFrame ? "CAN2.0B" : "CAN2.0A"));
		if (data.length < 0 || data.length > CANDATAMAXLENGHT)
			throw new IllegalArgumentException("Size of data must be 64 bits (" + CANDATAMAXLENGHT + " bytes)");
		int index = 0;
		byte[] _trame;
		if (extendedFrame) {
			_trame = new byte[CANBIDLENGHT + data.length];
			_trame[index++] = (byte) ((id >> 24 & 0x7F) + (extendedFrame ? 1 << 7 : 0));
			_trame[index++] = (byte) (id >> 16 & 0xFF);
		} else
			_trame = new byte[CANAIDLENGHT + data.length];
		_trame[index++] = (byte) (id >> 8 & 0XFF);
		_trame[index++] = (byte) (id & 0XFF);
		for (int i = 0; i < data.length; i++)
			_trame[index++] = data[i];
		trame = _trame;
	}

	@Override
	public CanTrame clone() {
		return new CanTrame(trame.clone());
	}

	@Override
	public int hashCode() {
		return Arrays.hashCode(trame);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof CanTrame)
			return Arrays.equals(trame, ((CanTrame) obj).getTrame());
		return false;
	}

	public byte[] getData() {
		int idLength = isExtendedFrame() ? CANBIDLENGHT : CANAIDLENGHT;
		byte[] datas = new byte[trame.length - idLength];
		System.arraycopy(trame, idLength, datas, 0, datas.length);
		return datas;
	}

	public int getId() {
		return isExtendedFrame() ? (trame[3] & 0xFF) << 0 | (trame[2] & 0xFF) << 8 | (trame[1] & 0xFF) << 16 | (trame[0] & 0x7F) << 24 : (trame[1] & 0xFF) << 0 | (trame[0] & 0x7F) << 8;
	}

	public byte[] getTrame() {
		return trame;
	}

	public boolean isExtendedFrame() {
		return (trame[0] & 0x80) != 0;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Id: ");
		int i = 0;
		sb.append(getId());
		sb.append("\tData: ");
		byte[] data = getData();
		for (; i < data.length; i++)
			sb.append(String.format("%02X", data[i]));
		return sb.toString();
	}
}
