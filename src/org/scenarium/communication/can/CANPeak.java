/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.communication.can;

import java.util.ArrayList;

import org.beanmanager.editors.DynamicChoiceBox;
import org.beanmanager.editors.DynamicEnableBean;
import org.beanmanager.editors.PropertyInfo;
import org.beanmanager.editors.primitive.number.NumberInfo;
import org.scenarium.tools.LibraryUtils;

import peak.can.basic.PCANBasic;
import peak.can.basic.TPCANBaudrate;
import peak.can.basic.TPCANHandle;
import peak.can.basic.TPCANMessageType;
import peak.can.basic.TPCANMode;
import peak.can.basic.TPCANMsg;
import peak.can.basic.TPCANStatus;
import peak.can.basic.TPCANTimestamp;
import peak.can.basic.TPCANType;

public class CANPeak extends CanProvider implements DynamicEnableBean {
	private static boolean isAvailable = LibraryUtils.loadLibraryFromRessource(CANPeak.class,e -> "\nDrivers of the Peak's card probably not installed", new String[] { "PCANBasic", "PCANBasic_JNI" });

	public static void sample() {
		PCANBasic can = null;
		TPCANMsg msg = null;
		TPCANStatus status = null;
		can = new PCANBasic();
		if (!can.initializeAPI()) {
			System.out.println("Unable to initialize the API");
			System.exit(0);
		}
		status = can.Initialize(TPCANHandle.PCAN_PCIBUS1, TPCANBaudrate.PCAN_BAUD_1M, TPCANType.PCAN_TYPE_NONE, 0, (short) 0);
		msg = new TPCANMsg();
		while (true)
			while (can.Read(TPCANHandle.PCAN_PCIBUS1, msg, null) == TPCANStatus.PCAN_ERROR_OK) {
				status = can.Write(TPCANHandle.PCAN_PCIBUS1, msg);
				if (status != TPCANStatus.PCAN_ERROR_OK) {
					System.out.println("Unable to write the CAN message");
					System.exit(0);
				}
			}

	}

	private PCANBasic can;
	@PropertyInfo(index = 0)
	@DynamicChoiceBox(possibleChoicesMethod = "getAvailableChannel", useSwingWorker = true)
	private TPCANHandle channel;
	@PropertyInfo(index = 1)
	private TPCANBaudrate baudRate = TPCANBaudrate.PCAN_BAUD_500K;
	@PropertyInfo(index = 2)
	private FilterType filterType = FilterType.STANDARD;
	@PropertyInfo(index = 3)
	@NumberInfo(min = 0, max = CanTrame.ADRESSEMAXCANEXT)
	private int from = 0;
	@PropertyInfo(index = 4)
	@NumberInfo(min = 0, max = CanTrame.ADRESSEMAXCANEXT)
	private int to = CanTrame.ADRESSEMAXCANEXT;
	@PropertyInfo(index = 5)
	private TPCANType hardwareType = TPCANType.PCAN_TYPE_NONE;
	@PropertyInfo(index = 6)
	private int ioport = 100;

	@PropertyInfo(index = 7)
	private short interrupt = 3;
	private boolean isInitialize = false;
	// Variable tampon
	private TPCANMsg sendMsgBuff = new TPCANMsg();

	private TPCANMsg rcvMsgBuff = new TPCANMsg();

	private TPCANTimestamp rcvtTsBuff = new TPCANTimestamp();

	@Override
	public void connect() throws CanException {
		if (!isAvailable)
			throw new CanException("Can provider not available");

		CanPeakManager cpm = CanPeakManager.getInstance();
		if (can == null) {
			can = cpm.getPCANBasic();
			if (!can.initializeAPI()) {
				can = null;
				throw new CanException("Cannot initialize API");
			}
		}

		if (channel == null)
			throw new CanException("No channel selected");
		String error = (isNonPnP() ? can.Initialize(channel, baudRate, hardwareType, ioport, interrupt) : can.Initialize(channel, baudRate, TPCANType.PCAN_TYPE_NONE, 0, (short) 0)).getMessage();
		if (error != null)
			throw new CanException("Cannot init channel: " + channel + ", error code: " + error);
		isInitialize = true;
		if ((from != 0 || to != CanTrame.ADRESSEMAXCANEXT) && to > from) {
			error = can.FilterMessages(channel, from, to, filterType == FilterType.STANDARD ? TPCANMode.PCAN_MODE_STANDARD : TPCANMode.PCAN_MODE_EXTENDED).getMessage();
			if (error != null)
				throw new CanException("Cannot set filtering, error code: " + error);
		}
		error = can.SetRcvEvent(channel).getMessage();
		if (error != null) {
			System.err.println("Cannot set the RcvEvent, error code: " + error);
			return;
		}
		cpm.setrcvEventDispatcher(channel, this);
	}

	@Override
	public TPCANHandle[] getAvailableChannel() {
		if (!isAvailable)
			return new TPCANHandle[0];
		if (this.can == null) {
			try {
				can = CanPeakManager.getInstance().getPCANBasic();
			} catch (CanException e) {
				return new TPCANHandle[0];
			}
			if (!can.initializeAPI()) {
				can = null;
				return new TPCANHandle[0];
			}
		}
		ArrayList<TPCANHandle> availableChannel = new ArrayList<>();
		for (TPCANHandle canHandle : TPCANHandle.initializableChannels())
			// StringBuffer sb = new StringBuffer();
			// can.GetValue(canHandle, TPCANParameter.PCAN_CHANNEL_CONDITION, sb, 1);
			// if ((canHandle == channel && can != null) || (sb.length() != 0 && ((int) sb.charAt(0)) == TPCANParameterValue.PCAN_CHANNEL_AVAILABLE.getValue())) {
			availableChannel.add(canHandle);
		// String error = (isNonPnP() ? can.Initialize(canHandle, baudRate, hardwareType, ioport, interrupt) : can.Initialize(channel, baudRate, TPCANType.PCAN_TYPE_NONE, 0, (short) 0)).getMessage();
		// if (error != null){
		// can.Uninitialize(canHandle)
		// }
		// }
		// sb.setLength(0);
		// if (this.can == null) {
		// String error = can.Uninitialize(TPCANHandle.PCAN_NONEBUS).getMessage();
		// if (error != null)
		// System.err.println(error);
		// }
		can = null;
		return availableChannel.toArray(new TPCANHandle[0]);
	}

	public TPCANBaudrate getBaudRate() {
		return baudRate;
	}

	public TPCANHandle getChannel() {
		return channel;
	}

	public FilterType getFilterType() {
		return filterType;
	}

	public int getFrom() {
		return from;
	}

	public TPCANType getHardwareType() {
		return hardwareType;
	}

	public short getInterrupt() {
		return interrupt;
	}

	public int getIoport() {
		return ioport;
	}

	public int getTo() {
		return to;
	}

	private boolean isNonPnP() {
		return channel == null ? false : TPCANHandle.isPCANUSBHardware(channel) || channel == TPCANHandle.PCAN_DNGBUS1;
	}

	public void processRcvEvent(TPCANHandle _channel) {
		while (true) {
			TPCANStatus status = can.Read(_channel, rcvMsgBuff, rcvtTsBuff);
			if (status != TPCANStatus.PCAN_ERROR_OK) {
				if (status != TPCANStatus.PCAN_ERROR_QRCVEMPTY)
					System.err.println("Error while reading trame, error code: " + status.getMessage());
				return;
			}
			if (rcvMsgBuff.getType() == TPCANMessageType.PCAN_MESSAGE_EXTENDED.getValue())
				receiver.receive(new CanTrame(rcvMsgBuff.getID(), true, rcvMsgBuff.getData()), System.currentTimeMillis()/* ts.getMillis() */);
			else if (rcvMsgBuff.getType() == TPCANMessageType.PCAN_MESSAGE_STANDARD.getValue())
				receiver.receive(new CanTrame(rcvMsgBuff.getID(), false, rcvMsgBuff.getData()), System.currentTimeMillis()/* ts.getMillis() */);
		}
	}

	@Override
	public void release() {
		if (can == null || !isInitialize)
			return;
		if (can.GetStatus(channel) != TPCANStatus.PCAN_ERROR_INITIALIZE) {
			String error = can.ResetRcvEvent(channel).getMessage();
			if (error != null)
				System.err.println(error);
			error = can.Uninitialize(channel).getMessage();
			if (error != null)
				System.err.println("Cannot close the driver, error code: " + error);
		}
		can = null;
	}

	@Override
	public void sendTrame(CanTrame canTrame) {
		if (filterType == FilterType.STANDARD && canTrame.isExtendedFrame()) {
			System.err.println("cannot send trame: " + canTrame + "\nId larger than 11 bits specified in standard can 2.0A");
			return;
		}
		byte[] datas = canTrame.getData();
		sendMsgBuff.setID(canTrame.getId());
		sendMsgBuff.setType(filterType == FilterType.STANDARD ? TPCANMessageType.PCAN_MESSAGE_STANDARD : TPCANMessageType.PCAN_MESSAGE_EXTENDED);
		sendMsgBuff.setLength((byte) datas.length);
		sendMsgBuff.setData(datas, (byte) datas.length);
		can.Write(channel, sendMsgBuff).getMessage();
	}

	public void setBaudRate(TPCANBaudrate baudRate) {
		this.baudRate = baudRate;
		restart();
	}

	public void setChannel(TPCANHandle channel) {
		boolean needToRestart = can != null;
		CANReceiver oldReceiver = null;
		if (needToRestart) {
			oldReceiver = receiver;
			close();
		}
		this.channel = channel;
		setEnable();
		if (needToRestart)
			try {
				init(oldReceiver);
			} catch (CanException e) {
				System.err.println(e.getMessage());
			}
	}

	@Override
	public void setEnable() {
		boolean isNonPnP = isNonPnP();
		fireSetPropertyEnable(this, "hardwareType", isNonPnP);
		fireSetPropertyEnable(this, "ioport", isNonPnP);
		fireSetPropertyEnable(this, "interrupt", isNonPnP);
	}

	public void setFilterType(FilterType filterType) {
		this.filterType = filterType;
		restart();
	}

	public void setFrom(int from) {
		this.from = from;
		restart();
	}

	public void setHardwareType(TPCANType hardwareType) {
		this.hardwareType = hardwareType;
		restart();
	}

	public void setInterrupt(short interrupt) {
		this.interrupt = interrupt;
		restart();
	}

	public void setIoport(int ioport) {
		this.ioport = ioport;
		restart();
	}

	public void setTo(int to) {
		this.to = to;
		restart();
	}
}
