/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.communication.can.dbc;

import java.util.ArrayList;

import org.scenarium.communication.can.CanTrame;

public class DBCMessage {
	private final ArrayList<DBCSignal> signals;
	private final int id;
	private final String name;
	private final int size;
	private final String sender;

	public DBCMessage(int id, String name, int size, String sender, ArrayList<DBCSignal> signals) {
		this.id = id;
		this.name = name;
		this.size = size;
		this.sender = sender;
		this.signals = signals;
	}

	public CanTrame createTrame() {
		return new CanTrame(id, true, new byte[size]);
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getSender() {
		return sender;
	}

	public DBCSignal getSignal(String name) {
		for (DBCSignal dbcSignal : signals)
			if (dbcSignal.name.equals(name))
				return dbcSignal;
		return null;
	}

	public ArrayList<DBCSignal> getSignals() {
		return signals;
	}

	public int getSize() {
		return size;
	}

	@Override
	public String toString() {
		return "id: " + id + " name: " + name;
	}
}
