/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.communication.can.dbc;

import java.util.ArrayList;
import java.util.Objects;

import org.beanmanager.editors.basic.DescriptionProvider;

public class MessageIdentifier implements DescriptionProvider, Comparable<MessageIdentifier> {

	private final int id;
	private final String name;
	private final int size;
	private final ArrayList<SignalIdentifier> signals;

	public MessageIdentifier(int id, String name, int size, ArrayList<SignalIdentifier> signals) {
		this.id = id;
		this.name = name;
		this.size = size;
		this.signals = signals;
	}

	@Override
	public int compareTo(MessageIdentifier o) {
		int c = name.compareTo(o.name);
		return c != 0 ? c : id < o.id ? -1 : id == o.id ? 0 : 1;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof MessageIdentifier) {
			MessageIdentifier cvi = (MessageIdentifier) obj;
			return cvi.id == id && cvi.name.equals(name);
		}
		return false;
	}

	@Override
	public String getDescription() {
		StringBuilder sb = new StringBuilder(Integer.toString(id));
		sb.append(" ");
		sb.append(name);
		sb.append(":");
		for (SignalIdentifier signal : signals) {
			sb.append("\n\t-");
			sb.append(signal.getName());
		}
		return sb.toString();
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public ArrayList<SignalIdentifier> getSignals() {
		return signals;
	}

	public int getSize() {
		return size;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, name);
	}

	@Override
	public String toString() {
		return id + " " + name;
	}
}
