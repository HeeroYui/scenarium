/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.communication.can.dbc;

public class EncodingSignalProperties {
	private final String name;
	private double value;
	private boolean asInput;

	public EncodingSignalProperties(String name) {
		this.name = name;
	}

	public EncodingSignalProperties(String name, double value, boolean asInput) {
		this(name);
		this.value = value;
		this.asInput = asInput;
	}

	@Override
	protected EncodingSignalProperties clone() {
		return new EncodingSignalProperties(name, value, asInput);
	}

	@Override
	public boolean equals(Object emp) {
		return emp instanceof EncodingSignalProperties ? ((EncodingSignalProperties) emp).name.equals(name) : false;
	}

	public String getName() {
		return name;
	}

	public double getValue() {
		return value;
	}

	public boolean isAsInput() {
		return asInput;
	}

	public void setAsInput(boolean asInput) {
		this.asInput = asInput;
	}

	public void setValue(double value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "Name: " + name + " Value: " + value + " AsInput: " + asInput;
	}
}
