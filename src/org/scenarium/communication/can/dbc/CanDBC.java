/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.communication.can.dbc;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

import javax.swing.event.EventListenerList;

import org.scenarium.communication.can.LoadedListener;

public class CanDBC {
	private static final String BOBALISE = "BO_ ";
	private static final String SGBALISE = "SG_ ";
	private static final String SIG_VALTYPEBALISE = "SIG_VALTYPE_ ";
	private static final String VALBALISE = "VAL_ ";

	public static void main(String[] args) {
		CanDBC canDBC = new CanDBC();
		canDBC.setCanDBCFile(new File("/home/revilloud/Téléchargements/Fwd TR  Mobileye Extended Protocol documentation + dbc files(1)/ExtLogData2.dbc"));
		DBCMessage m = canDBC.getPropById(1616);
		byte[] data = new byte[m.getSize()];
		m.getSignal("Fixed_Horizon").encode(data, 1.256864f);
		m.getSignal("Fixed_Yaw").encode(data, 16.256864f);
		DBCSignal.showBytesCompact(data);
		System.out.println(m.getSignal("Fixed_Horizon").decode(data));
		System.out.println(m.getSignal("Fixed_Yaw").decode(data));
	}

	private File canDBCFile;
	private final EventListenerList listeners = new EventListenerList();

	private HashMap<Integer, DBCMessage> messages;

	public CanDBC() {}

	public CanDBC(File file) {
		setCanDBCFile(file);
	}

	public void addOpenListener(LoadedListener listener) {
		listeners.add(LoadedListener.class, listener);
	}

	private void fireLoaded() {
		for (LoadedListener listener : listeners.getListeners(LoadedListener.class))
			listener.loaded();
	}

	public File getCanDBCFile() {
		return canDBCFile;
	}

	public ArrayList<DBCSignal> getSignals() {
		ArrayList<DBCSignal> signals = new ArrayList<>();
		if (messages == null) {
			loadIfNeeded();
			if (messages == null)
				return null;
		}
		messages.forEach((id, message) -> message.getSignals().forEach(signal -> signals.add(signal)));
		return signals;
	}

	public ArrayList<MessageIdentifier> getMessagesIdentifier() {
		ArrayList<MessageIdentifier> _messages = new ArrayList<>();
		loadIfNeeded();
		if (messages != null)
			messages.forEach((id, message) -> _messages.add(toMessagesIdentifier(message)));
		return _messages;
	}

	public DBCMessage getPropById(int id) {
		if (messages == null)
			return null;
		return messages.get(id);
	}

	public ArrayList<SignalIdentifier> getSignalsIdentifier() {
		ArrayList<SignalIdentifier> signals = new ArrayList<>();
		loadIfNeeded();
		if (messages != null)
			messages.forEach((id, message) -> message.getSignals().forEach(signal -> signals.add(toSignalIdentifier(message, signal))));
		return signals;
	}

	public boolean hasMessage() {
		return messages != null && !messages.isEmpty();
	}

	synchronized public void loadIfNeeded() {
		if (messages == null)
			reload();
	}

	synchronized private void reload() {
		if (canDBCFile == null)
			return;
		messages = new HashMap<>();
		try {
			try (BufferedReader br = new BufferedReader(new FileReader(canDBCFile))) {
				String line = null;
				int nbLine = 0;
				while (line != null || (line = br.readLine()) != null) {
					nbLine++;
					line = line.trim();
					if (line.endsWith(";"))
						line = line.substring(0, line.length() - 1);
					if (line.startsWith(BOBALISE))
						try {
							StringTokenizer boSt = new StringTokenizer(line, " ");
							boSt.nextToken();
							long id = Long.parseLong(boSt.nextToken());
							String boName = boSt.nextToken();
							boName = boName.substring(0, boName.length() - 1);
							int size = Integer.parseInt(boSt.nextToken());
							String sender = boSt.nextToken();
							ArrayList<DBCSignal> signals = new ArrayList<>();
							line = null;
							String subLine;
							int nbSubLine = 0;
							while ((subLine = br.readLine()) != null) {
								nbLine++;
								subLine = subLine.trim();
								if (!subLine.startsWith(SGBALISE)) {
									line = subLine;
									nbLine--;
									break;
								}
								nbSubLine++;
								try {
									StringTokenizer st = new StringTokenizer(subLine);
									st.nextToken();
									String name = st.nextToken();
									if (!st.nextToken().equals(":"))
										throw new IllegalArgumentException("Do not find ':' separator in line: " + subLine);
									String formatInfo = st.nextToken();
									int idFirstSeparator = formatInfo.indexOf("|");
									if (idFirstSeparator == -1)
										throw new IllegalArgumentException("Do not find '|' separator in line: " + subLine);
									byte firstBit = Byte.parseByte(formatInfo.substring(0, idFirstSeparator));
									int idSecondSeparator = formatInfo.indexOf("@");
									if (idSecondSeparator == -1)
										throw new IllegalArgumentException("Do not find '@' separator in line: " + subLine);
									byte msgSize = Byte.parseByte(formatInfo.substring(idFirstSeparator + 1, idSecondSeparator));
									char charCodage = formatInfo.charAt(++idSecondSeparator);
									Boolean isIntel;
									if (charCodage == '1')
										isIntel = true;
									else if (charCodage == '0')
										isIntel = false;
									else
										throw new IllegalArgumentException("The codage: " + charCodage + " is unknown in line: " + subLine);
									char charFormat = formatInfo.charAt(++idSecondSeparator);
									int format = charFormat == '+' ? DBCSignal.UNSIGNED : charFormat == '-' ? DBCSignal.SIGNED : -1;
									if (format == -1)
										throw new IllegalArgumentException("The format: " + charFormat + " is unknown in line: " + subLine);
									String scaleOffsetInfo = st.nextToken();
									if (scaleOffsetInfo.charAt(0) != '(' || scaleOffsetInfo.charAt(scaleOffsetInfo.length() - 1) != ')')
										throw new IllegalArgumentException("The scale and offset properties: " + scaleOffsetInfo + " do not begin with ( and finish with ) in line: " + subLine);
									scaleOffsetInfo = scaleOffsetInfo.substring(1, scaleOffsetInfo.length() - 1);
									int idSeparator = scaleOffsetInfo.indexOf(",");
									if (idSeparator == -1)
										throw new IllegalArgumentException("Do not find ',' separator in line: " + subLine);
									double scale = Double.parseDouble(scaleOffsetInfo.substring(0, idSeparator));
									double offset = Double.parseDouble(scaleOffsetInfo.substring(idSeparator + 1, scaleOffsetInfo.length()));
									String minMaxInfo = st.nextToken();
									if (minMaxInfo.charAt(0) != '[' || minMaxInfo.charAt(minMaxInfo.length() - 1) != ']')
										throw new IllegalArgumentException("The min and max properties: " + minMaxInfo + " do not begin with [ and finish with ] in line: " + subLine);
									minMaxInfo = minMaxInfo.substring(1, minMaxInfo.length() - 1);
									idSeparator = minMaxInfo.indexOf("|");
									if (idSeparator == -1)
										throw new IllegalArgumentException("Do not find '|' separator in line: " + subLine);
									double min = Double.parseDouble(minMaxInfo.substring(0, idSeparator));
									double max = Double.parseDouble(minMaxInfo.substring(idSeparator + 1, minMaxInfo.length()));
									if (min == 0 && max == 0) {
										min = -Double.MAX_VALUE;
										max = Double.MAX_VALUE;
									}
									String unit = st.nextToken();
									if (unit.charAt(0) == '"')
										while (unit.charAt(unit.length() - 1) != '"')
											unit += " " + st.nextToken();
									else
										throw new IllegalArgumentException("The unit properties: " + minMaxInfo + " do not begin with \" and finish with \" in line: " + subLine);
									// if (unit.charAt(0) != '"' || unit.charAt(unit.length() - 1) != '"')
									// throw new IllegalArgumentException("The unit properties: " + minMaxInfo + " do not begin with \" and finish with \" in line: " + subLine);
									signals.add(new DBCSignal(name, size, isIntel, format, firstBit, msgSize, scale, offset, min, max, unit.substring(1, unit.length() - 1), st.nextToken()));
								} catch (Exception e) {
									if (e instanceof IllegalArgumentException)
										System.err.println(e.getClass().getSimpleName() + ": " + e.getMessage());
									else
										System.err.println("Bad formatted " + SGBALISE + ": " + nbSubLine + " of block: " + id);
								}
							}
							if (id > Integer.MIN_VALUE || id < Integer.MAX_VALUE)
								messages.put((int) id, new DBCMessage((int) id, boName, size, sender, signals));
						} catch (Exception e) {
							e.printStackTrace();
							System.err.println("Bad formatted " + BOBALISE + ": in line: " + line);
							line = null;
						}
					else if (line.startsWith(SIG_VALTYPEBALISE)) {
						int i = SIG_VALTYPEBALISE.length();
						if (i != line.length())
							try {
								StringTokenizer st = new StringTokenizer(line);
								st.nextToken();
								long id = Long.parseLong(st.nextToken());
								String sigName = st.nextToken();
								if (!st.nextToken().equals(":"))
									throw new IllegalArgumentException("Do not find ':' separator in line: " + line);
								int typei = Integer.parseInt(st.nextToken());
								int format = typei == 0 ? DBCSignal.IEEEDOUBLE : typei == 1 ? DBCSignal.IEEEFLOAT : -1;
								if (format == -1)
									new IllegalArgumentException(typei + " is not a valid type in line: " + line);
								DBCMessage message = messages.get((int) id);
								DBCSignal signal = message.getSignal(sigName);
								signal.format = format;
							} catch (Exception e) {
								System.err.println("Bad formatted " + SIG_VALTYPEBALISE + ": in line: " + nbLine + "\nCause by: " + e.getClass().getSimpleName() + ": " + e.getMessage());
								line = null;
							}
						line = null;
					} else if (line.startsWith(VALBALISE)) {
						int i = VALBALISE.length();
						if (i != line.length())
							try {
								StringTokenizer st = new StringTokenizer(line);
								st.nextToken();
								DBCSignal signal = messages.get((int) Long.parseLong(st.nextToken())).getSignal(st.nextToken());
								HashMap<Integer, String> enumMap = new HashMap<>();
								while (st.hasMoreTokens()) {
									int value = Integer.parseInt(st.nextToken());
									String enumName = st.nextToken();
									if (enumName.charAt(0) == '"')
										while (enumName.charAt(enumName.length() - 1) != '"')
											enumName += " " + st.nextToken();
									else
										throw new IllegalArgumentException("The val properties do not begin with \" and finish with \" in line: " + line);
									enumMap.put(value, enumName.substring(1, enumName.length() - 1));
								}
								signal.setEnumMap(enumMap);
							} catch (Exception e) {
								System.err.println("Bad formatted " + SIG_VALTYPEBALISE + ": in line: " + nbLine + "\nCause by: " + e.getClass().getSimpleName() + ": " + e.getMessage());
								line = null;
							}
						line = null;
					} else
						line = null;
				}
			}
		} catch (IOException e) {
			if (canDBCFile.getAbsolutePath().isBlank())
				System.err.println("Error while loading file: " + e.getMessage());
			messages = null;
		}
		fireLoaded();
	}

	public void removeOpenListener(LoadedListener listener) {
		listeners.remove(LoadedListener.class, listener);
	}

	public void setCanDBCFile(File canDBCFile) {
		this.canDBCFile = canDBCFile;
		reload();
	}

	private static MessageIdentifier toMessagesIdentifier(DBCMessage message) {
		return new MessageIdentifier(message.getId(), message.getName(), message.getSize(),
				message.getSignals().stream().map(e -> toSignalIdentifier(message, e)).collect(Collectors.toCollection(ArrayList::new)));
	}

	private static SignalIdentifier toSignalIdentifier(DBCMessage message, DBCSignal signal) {
		return new SignalIdentifier(message.getId(), message.getName(), signal.name, signal.min, signal.max, signal.unit, signal.getEnumMap());
	}

	@Override
	public String toString() {
		return canDBCFile == null ? "No DBC" : canDBCFile.toString() + " Nb Messages: " + (messages == null ? 0 : messages.size());
	}
}
