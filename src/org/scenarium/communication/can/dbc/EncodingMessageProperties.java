/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.communication.can.dbc;

import java.util.ArrayList;
import java.util.Objects;
import java.util.stream.Collectors;

import org.scenarium.editors.can.SendMode;

public class EncodingMessageProperties {
	private int id;
	private String name;
	private SendMode sendMode;
	private String additionalInfo;
	private ArrayList<EncodingSignalProperties> signalProperties;

	public EncodingMessageProperties(int id, String name) {
		this.id = id;
		this.name = name;
		this.sendMode = SendMode.ONNEWDATA;
		this.signalProperties = new ArrayList<>();
	}

	public EncodingMessageProperties(int id, String name, SendMode sendMode, String additionalInfo, ArrayList<EncodingSignalProperties> signalProperties) {
		this.id = id;
		this.name = name;
		this.sendMode = sendMode;
		this.additionalInfo = additionalInfo == null || additionalInfo.equals("_") ? null : additionalInfo;
		this.signalProperties = signalProperties;
	}

	@Override
	public EncodingMessageProperties clone() {
		return new EncodingMessageProperties(id, name, sendMode, additionalInfo, signalProperties.stream().map(e -> e.clone()).collect(Collectors.toCollection(ArrayList::new)));
	}

	@Override
	public boolean equals(Object emp) {
		return emp instanceof EncodingMessageProperties ? ((EncodingMessageProperties) emp).id == id && ((EncodingMessageProperties) emp).name.equals(name) : false;
	}

	public String getAdditionalInfo() {
		return additionalInfo;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public SendMode getSendMode() {
		return sendMode;
	}

	public ArrayList<EncodingSignalProperties> getSignalProperties() {
		return signalProperties;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, name);
	}

	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	public void setSendMode(SendMode sendMode) {
		if (this.sendMode == sendMode)
			return;
		this.sendMode = sendMode;
		if (sendMode == SendMode.TIMER && additionalInfo.isEmpty())
			additionalInfo = "100";
	}

	public void setSignalProperties(ArrayList<EncodingSignalProperties> signalProperties) {
		this.signalProperties = signalProperties;
	}

	@Override
	public String toString() {
		return "Id: " + id + " Name: " + name + " SendMode: " + sendMode + " SignalProperties: " + signalProperties;
	}

}
