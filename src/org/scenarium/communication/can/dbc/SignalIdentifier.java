/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.communication.can.dbc;

import java.util.HashMap;
import java.util.Objects;

import org.beanmanager.editors.basic.DescriptionProvider;

public class SignalIdentifier implements DescriptionProvider, Comparable<SignalIdentifier> {
	private final int id;
	private final String messageName;
	private final String name;
	private final double min;
	private final double max;
	private final String unit;
	private final HashMap<Integer, String> enumMap;

	public SignalIdentifier(int id, String messageName, String name, double min, double max, String unit, HashMap<Integer, String> enumMap) {
		this.id = id;
		this.messageName = messageName;
		this.name = name;
		this.unit = unit;
		this.min = min;
		this.max = max;
		this.enumMap = enumMap;
	}

	@Override
	public int compareTo(SignalIdentifier o) {
		int c = name.toLowerCase().compareTo(o.name.toLowerCase());
		return c != 0 ? c : id < o.id ? -1 : id == o.id ? 0 : 1;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof SignalIdentifier) {
			SignalIdentifier cvi = (SignalIdentifier) obj;
			return cvi.id == id && cvi.messageName.equals(messageName) && cvi.name.equals(name);
		}
		return false;
	}

	@Override
	public String getDescription() {
		String desc = id + " " + messageName + " " + name;
		return unit == null || unit.isEmpty() ? desc : desc + " unit: " + unit;
	}

	public HashMap<Integer, String> getEnumList() {
		return enumMap;
	}

	public int getId() {
		return id;
	}

	public double getMax() {
		return max;
	}

	public String getMessageName() {
		return messageName;
	}

	public double getMin() {
		return min;
	}

	public String getName() {
		return name;
	}

	public String getUnit() {
		return unit;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, name);
	}

	@Override
	public String toString() {
		return id + " " + name;
	}
}