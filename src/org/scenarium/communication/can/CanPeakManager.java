/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package org.scenarium.communication.can;

import java.util.ArrayList;

import peak.can.basic.IRcvEventProcessor;
import peak.can.basic.PCANBasic;
import peak.can.basic.RcvEventDispatcher;
import peak.can.basic.TPCANHandle;

class CanPeakChannelDispatcher {
	public final TPCANHandle channel;
	public final CANPeak canPeak;

	public CanPeakChannelDispatcher(TPCANHandle channel, CANPeak canPeak) {
		this.channel = channel;
		this.canPeak = canPeak;
	}
}

public class CanPeakManager implements IRcvEventProcessor {
	private static CanPeakManager instance;

	public static CanPeakManager getInstance() {
		if (instance == null)
			synchronized (CanPeakManager.class) {
				if (instance == null)
					instance = new CanPeakManager();
			}
		return instance;
	}

	private PCANBasic can;

	private ArrayList<CanPeakChannelDispatcher> rcvEventDispatcher;

	private CanPeakManager() {

	}

	public PCANBasic getPCANBasic() throws CanException {
		if (can == null) {
			can = new PCANBasic();
			try {
				if (!can.initializeAPI())
					throw new CanException("Unable to initialize the API");
			} catch (UnsatisfiedLinkError e) {
				throw new CanException("Unable to initialize the API, UnsatisfiedLinkError");
			}
			RcvEventDispatcher.setListener(this);
		}
		return can;
	}

	@Override
	public void processRcvEvent(TPCANHandle channel) {
		for (CanPeakChannelDispatcher canPeakChannelDispatcher : rcvEventDispatcher)
			if (canPeakChannelDispatcher.channel == channel) {
				canPeakChannelDispatcher.canPeak.processRcvEvent(channel);
				return;
			}
	}

	public void removeRcvEventDispatcher(TPCANHandle channel) {
		for (int i = 0; i < rcvEventDispatcher.size(); i++)
			if (rcvEventDispatcher.get(i).channel == channel) {
				rcvEventDispatcher.remove(i);
				return;
			}
	}

	public void setrcvEventDispatcher(TPCANHandle channel, CANPeak canPeak) {
		if (rcvEventDispatcher == null)
			rcvEventDispatcher = new ArrayList<>();
		removeRcvEventDispatcher(channel);
		rcvEventDispatcher.add(new CanPeakChannelDispatcher(channel, canPeak));
	}
}
