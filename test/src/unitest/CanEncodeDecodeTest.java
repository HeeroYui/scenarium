/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package unitest;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.util.Arrays;
import java.util.Random;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;
import org.scenarium.communication.can.dbc.CanDBC;
import org.scenarium.communication.can.dbc.DBCSignal;

import com.googlecode.junittoolbox.ParallelParameterized;

@RunWith(value = ParallelParameterized.class)
public class CanEncodeDecodeTest {
	private static final DBCSignal[] testedDBCFiles = Arrays.stream(new File(CanEncodeDecodeTest.class.getResource("/DBFFiles/").getFile()).listFiles()).flatMap(file -> new CanDBC(file).getSignals().stream()).toArray(DBCSignal[]::new);

	@Parameter
	public DBCSignal signal;

	@Parameters(name = "{index}: type - {0}")
	public static DBCSignal[] data() {
		return testedDBCFiles;
	}

	@Test
	public void checkEncodeDecode() {
		Random rand = new Random(0);
		int nbErrors = testRandomValuesInSignal(signal, 100000, rand);
		assertTrue(nbErrors == 0 || nbErrors == -1);
//		nbErrors = testAllPossibleValuesInSignal(signal);
//		assertTrue(nbErrors == 0 || nbErrors == -1);
	}

	@SuppressWarnings("unused")
	private static int testAllPossibleValuesInSignal(DBCSignal signal) {
//		if (!signal.name.equals("GPS_Latitude"))
//			return -1;
		double min;
		double max;
		if (signal.min == -Double.MAX_VALUE) {
			min = signal.offset;
			max = signal.maxEffectiveValue();
		} else {
			min = signal.min;
			max = signal.max;
		}
		int nbError = 0;
		long index = 0;
		while (true) {
			double value = min + index * signal.scale;
			if (value > max)
				break;
			byte[] encodeArray = new byte[signal.messageSize];
			if (encode(signal, encodeArray, value)) {
				double decoded = signal.decode(encodeArray);
				if (Math.abs(value - decoded) > signal.scale / 2.0) {
					System.err.println("Error " + signal.name + " with value: " + value + " get value: " + decoded);
					encode(signal, encodeArray, value);
					DBCSignal.toHexStringCompact(encodeArray);
					DBCSignal.showBytes(encodeArray);
					nbError++;
				}
			}
			index++;
		}
		return nbError;
	}

	private static boolean encode(DBCSignal signal, byte[] encodeArray, double value) {
		try {
			signal.encode(encodeArray, value);
			return true;
		} catch (IllegalArgumentException e) {
			System.out.println(e.getMessage());
			return false;
		}
	}

	private static int testRandomValuesInSignal(DBCSignal signal, int nbIteration, Random rand) {
		double min;
		double max;
		if (signal.min == -Double.MAX_VALUE) {
			min = signal.offset;
			max = signal.maxEffectiveValue();
			return -1;
		}
		min = signal.min;
		max = signal.max;
		int nbError = 0;
		for (double i = 0; i < nbIteration; i++) {
			double value;
			if(i == 0)
				value = signal.min;
			else if(i == nbIteration - 1)
				value = Math.min(signal.maxEffectiveValue(), signal.max) ;
			else
				value = min + nextLong(rand, Math.round((max - min) / signal.scale)) * signal.scale;
			byte[] encodeArray = new byte[signal.messageSize];
			if (encode(signal, encodeArray, value)) {
				double decoded = signal.decode(encodeArray);
				if (Math.abs(value - decoded) > signal.scale / 2.0) {
					System.out.println("value: " + (long) value);
					decoded = signal.decode(encodeArray);
					System.err.println("Error " + signal.name + " with value: " + value + " get value: " + decoded);
					DBCSignal.toHexStringCompact(encodeArray);
					DBCSignal.showBytes(encodeArray);
					nbError++;
				}
			}
		}
		return nbError;
	}

	// https://en.wikipedia.org/wiki/Two%27s_complement
	public static int twoComplement(int base, int value) {
		return -((1 << base) - value);
	}

	public static int invertedTwoComplement(int base, int value) {
		return (1 << base) + value;
	}
	
	private static long nextLong(Random rng, long n) {
		long bits, val;
		do {
			bits = (rng.nextLong() << 1) >>> 1;
			val = bits % n;
		} while (bits - val + (n - 1) < 0L);
		return val;
	}
}
