/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 * 
 * Contributors:
 *     Revilloud Marc - initial API and implementation
 ******************************************************************************/
package unitest;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.lang.reflect.Array;
import java.util.Random;
import java.util.function.Supplier;

import org.beanmanager.editors.container.ArrayEditor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;
import org.scenarium.ModuleManager;
import org.scenarium.communication.can.CanTrame;

import com.googlecode.junittoolbox.ParallelParameterized;

@RunWith(value = ParallelParameterized.class)
public class StreamAndTextTest {
	private static final int nbRun = 10;
	private static final int seed = 369;
	private static final int maxlevel = 3;
	private static final int maxSize = 10;
	private static final double nullRatio = 0.1;

	private static final Random rand = new Random(seed);

	private static final Class<?>[] testedTypes = new Class<?>[] {CanTrame.class};
	static {
		ModuleManager.loadEmbeddedAndInternalModules();
	}

	@Parameter
	public Class<?> type;

	// Single parameter, use Object[]
	@Parameters(name = "{index}: type - {0}")
	public static Class<?>[] data() {
		return testedTypes;
	}

	@Test
	public void test_type() {
		assertTrue(checkPropertyArray(rand, maxlevel, maxSize, nullRatio, type, () -> generateRandomValue(type, rand, maxSize, testedTypes)));
	}

	public static Object generateRandomValue(Class<?> type, Random rand, int maxSize, Class<?>[] testedTypes) {
		if (type.equals(CanTrame.class)) {
			boolean ex = rand.nextBoolean();
			byte[] data = new byte[rand.nextInt(8)];
			for (int i = 0; i < data.length; i++)
				data[i] = (byte) rand.nextInt();
			return new CanTrame(rand.nextInt(ex ? CanTrame.ADRESSEMAXCANEXT : CanTrame.ADRESSEMAXCANSTD), ex, data);
		}
		throw new IllegalArgumentException("No generator for type: " + type);
	}

	private static boolean checkPropertyArray(Random rand, int maxlevel, int maxSize, double nullRatio, Class<?> arrayType, Supplier<?> supplier) {
		try {
			int textError = 0;
			for (int k = 0; k < nbRun; k++) {
				Class<?> type = arrayType;
				for (int i = 0; i < maxlevel; i++)
					type = Array.newInstance(type, 0).getClass();
				Object array = Array.newInstance(type, (int) (rand.nextDouble() * maxSize));
				populateArray(array, rand, supplier, 0.01);
				// Text test
				ArrayEditor<?> ed = new ArrayEditor<>(array.getClass());
				ed.setValueFromObj(array);
				String text = ed.getAsText();
				ed.setAsText(text);
				Object resValue = ed.getValue();
				if (!compareArray(array, resValue)) {
					textError++;
					ed.setAsText(text);
					ed.setValueFromObj(array);
					text = ed.getAsText();
					ed.setAsText(text);
					Object v = ed.getValue();
					System.out.println(v);
					System.out.println(compareArray(array, resValue));
				}
			}
			int streamError = 0;
			for (int k = 0; k < nbRun; k++) {
				int level = 1 + (int) (rand.nextDouble() * maxlevel);
				Class<?> type = arrayType;
				for (int i = 0; i < level - 1; i++)
					type = Array.newInstance(type, 0).getClass();
				Object array = Array.newInstance(type, (int) (rand.nextDouble() * maxSize));
				populateArray(array, rand, supplier, 0.01);
				ArrayEditor<?> ed = new ArrayEditor<>(array.getClass());
				// Flow test
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				try (DataOutputStream dos = new DataOutputStream(baos)) {
					ed.writeValueFromObj(dos, array);
				}
				try (DataInputStream dis = new DataInputStream(new ByteArrayInputStream(baos.toByteArray()))) {
					Object array2 = ed.readValue(dis);
					if (!compareArray(array, array2))
						streamError++;
				}
			}
			if (textError == 0 && streamError == 0)
				;// System.out.println(arrayType.getSimpleName() + " Ok");
			else if (textError != 0)
				System.err.println(arrayType.getSimpleName() + ": Text fail, Stream ok");
			else if (streamError != 0)
				System.err.println(arrayType.getSimpleName() + ": Text ok, Stream fail");
			else
				System.err.println(arrayType.getSimpleName() + ": both failed");
			return textError == 0 && streamError == 0;
		} catch (Exception e) {
			System.err.println(arrayType.getSimpleName() + ": Exception");
			e.printStackTrace();
			return false;
		}
	}

	private static boolean compareArray(Object a1, Object a2) {
		if (a1 == null ^ a2 == null)
			return false;
		if (a1 == null && a2 == null)
			return true;
		if (a1.getClass().isArray() ^ a2.getClass().isArray())
			return false;
		if (a1.getClass().isArray()) {
			if (Array.getLength(a1) != Array.getLength(a2))
				return false;
			for (int i = 0; i < Array.getLength(a1); i++)
				if (!compareArray(Array.get(a1, i), Array.get(a2, i)))
					return false;
		} else
			return a1.equals(a2);
		return true;

	}

	private static void populateArray(Object array, Random rand, Supplier<?> supplier, double nullRatio) {
		int size = Array.getLength(array);
		Class<?> subType = array.getClass().getComponentType();
		for (int i = 0; i < size; i++) {
			if (!subType.isArray()) {
				Array.set(array, i, (!subType.isPrimitive() && rand.nextDouble() < nullRatio) ? null : supplier.get());
			} else {
				if (rand.nextDouble() < nullRatio)
					Array.set(array, i, null);
				else {
					Class<?> subsubType = subType.getComponentType();
					Object subArray = Array.newInstance(subsubType, (int) (rand.nextDouble() * 10));
					populateArray(subArray, rand, supplier, nullRatio);
					Array.set(array, i, subArray);
				}
			}
		}
	}
}
